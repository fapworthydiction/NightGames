package global;

import java.nio.charset.CharacterCodingException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import characters.*;
import characters.Character;
import com.google.gson.JsonObject;
import daytime.Daytime;
import sun.security.jca.GetInstance;

public class Scheduler {
	private static Scheduler instance;
	private int date;
	private int matchnum;
	private LocalTime time;
	private Daytime day;
	private Match match;
	private HashMap<ID, Integer> score;

	private Scheduler(){
		date = 1;
		time = LocalTime.of(22,0);
		matchnum = 0;
		score = new HashMap<ID, Integer>();
	}

	public static Scheduler getInstance(){
		if(instance==null){
			instance = new Scheduler();
		}
		return instance;
	}

	public static void reset(){
		instance = new Scheduler();
	}
	
	public static void dawn(){
		Scheduler now = getInstance();
        now.time = LocalTime.of(9,0);
		now.match=null;
        if(getMatchNumber()>Constants.SEASONLENGTH && getDate()%7==0){
            now.matchnum=0;
            Global.unflag(Flag.OffSeason);
        }
		for(Character player: Roster.getExisting()){
			player.getStamina().fill();
			player.getArousal().empty();
			player.getMojo().empty();
			player.change(Modifier.normal);
		}
		now.date++;
		now.day=new Daytime(Global.getPlayer());
		Global.gui().refresh();
	}

	public static void dusk(){
		Scheduler now = getInstance();
		now.day=null;
        now.time = LocalTime.of(22,0);
		Player player = (Player)Roster.get(ID.PLAYER);
        if(Scheduler.hasMatch(player)){
            new Prematch(player);
        }else if(isMatchNight()){
            createMatch(Modifier.quiet);
            new NightOff(player);
            getMatch().automate(LocalTime.of(00,00));
        }else{
            new NightOff(player);
        }

	}
	public static void createMatch(Modifier matchmod){
        ArrayList<Character> lineup = new ArrayList<Character>();
        ArrayList<Character> available = Roster.getCombatants();
        if(matchmod==Modifier.maya){
            lineup.add(Roster.get(ID.PLAYER));
            Collections.shuffle(available);
            for(Character player: available){
                if(!lineup.contains(player)&&!player.human()&&lineup.size()<4&&!player.has(Trait.event)){
                    lineup.add(player);
                }
            }
            if(!Global.checkFlag(Flag.Maya)){
                Roster.scaleOpponent(ID.MAYA);
                Global.flag(Flag.Maya);
            }
            Character maya = Roster.get(ID.MAYA);
            maya.scaleLevel(Roster.get(ID.PLAYER).getLevel()+20);
            lineup.add(maya);
            getInstance().match=new Match(lineup,matchmod);
        }
        else if(available.size()>5){
            getInstance().match=new Match(calculateLineup(getMatchNumber()),matchmod);
        }
        else{
            getInstance().match=new Match(available,matchmod);
        }
        Global.buildActionPool();
        Global.populateTargetedActions(lineup);
        if(matchmod!=Modifier.quiet) {
            getInstance().match.round();
        }
    }
	
	public static boolean isAvailable(ID npc){
		boolean available = Roster.exists(npc);
		if(isMatchNight() && getTime().getHour() >= 10){
			if(getMatch().combatants.contains(Roster.get(npc))){
				available = false;
			}
		}
		return available;
	}
    public static boolean isAvailable(ID npc, LocalTime time){
        boolean available = Roster.exists(npc);
        if(isMatchNight() && time.getHour() >= 10){
            if(getMatch().combatants.contains(Roster.get(npc))){
                available = false;
            }
        }
        return available;
    }

	public static ArrayList<Character> getAvailable(){
		ArrayList<Character> available = new ArrayList<Character>();
		for(Character person: Roster.getExisting()){
			if(isAvailable(person.id())){
				available.add(person);
			}
		}
		return available;
	}
    public static ArrayList<Character> getAvailable(LocalTime time){
        ArrayList<Character> available = new ArrayList<Character>();
        for(Character person: Roster.getExisting()){
            if(isAvailable(person.id(),time)){
                available.add(person);
            }
        }
        return available;
    }
	public static int getMatchNumber(){
		return getInstance().matchnum;
	}

    public static void setMatchNum(int match) {
        getInstance().matchnum = match;
    }

    public static void incMatchNum() {
        getInstance().matchnum++;
    }

	public static Match getMatch(){
		return getInstance().match;
	}

	public static Daytime getDay(){
		return getInstance().day;
	}

	public static int getDate(){
		return getInstance().date;
	}

	public static boolean isMatchNight(){
		return getDate()%7 != 0 && getMatchNumber()<=Constants.SEASONLENGTH+1;
	}

	public static void setDate(int day){
		Scheduler.getInstance().date = day;
	}

	public static LocalTime getTime(){return getInstance().time;}
	public static void setTime(LocalTime time){getInstance().time = time;}
	public static void advanceTime(LocalTime diff){
		getInstance().time = getInstance().time.plusHours(diff.getHour());
        getInstance().time = getInstance().time.plusMinutes(diff.getMinute());
	}

	public static String displayScores(){
	    StringBuilder display = new StringBuilder();
	    ArrayList<Character> ordered = rankParticipants();
	    for(Character c:ordered){
	        display.append(c.name()+": "+getScore(c.id())+" points.");
	        display.append("<br>");
        }
        return display.toString();
    }

    public static ArrayList<Character> rankParticipants() {
        ArrayList<Character> result = new ArrayList<Character>();
        boolean inserted;
        for(Character combatant: Roster.getCombatants()){
            inserted = false;
            for(int i=0;i<result.size();i++){
                if(getScore(combatant.id()) > getScore(result.get(i).id())){
                    result.add(i,combatant);
                    inserted = true;
                    break;
                }
            }
            if(!inserted){
                result.add(combatant);
            }
        }
        return result;
    }

	public static Collection<Character> calculateLineup(int round){
		ArrayList<Character> lineup = new ArrayList<Character>();
		ArrayList<Character> available = Roster.getCombatants();
		lineup.add(available.remove(round % available.size()));
		lineup.add(available.remove( (available.size()-1)-(round % available.size())));
		lineup.add(available.remove(round % available.size()));
		lineup.add(available.remove( (available.size()-1)-(round % available.size())));
		lineup.add(available.remove(round % available.size()));
		return lineup;
	}

	public static boolean hasMatch(Character player){
		if(!isMatchNight()){
			return false;
		}
		if(getMatchNumber() > Constants.SEASONLENGTH){
			return player.human();
		}else{
			return calculateLineup(getMatchNumber()).contains(player);
		}
	}

	public static int getScore(ID player){
		HashMap<ID, Integer> score = getInstance().score;
		if(score.containsKey(player)){
		    return score.get(player);
        }else{
		    return 0;
        }
	}

	public static void addScore(ID player, int points){
		setScore(player,getScore(player)+points);
	}

	public static void setScore(ID player, int points){
		getInstance().score.put(player, points);
	}

	public static String getTimeString(){
		return LocalTime.parse(getTime().toString(), DateTimeFormatter.ofPattern("HH:mm")).format(DateTimeFormatter.ofPattern("hh:mm a"));
	}

	public static String getDayString(int day){
		switch(day%7){
		case 1:
			return "Monday";
		case 2:
			return "Tuesday";
		case 3:
			return "Wednesday";
		case 4:
			return "Thursday";
		case 5:
			return "Friday";
		case 6:
			return "Saturday";
		default:
			return "Sunday";
		}
	}

    public static void clearScores() {
	    getInstance().score.clear();
    }
}
