package global;

import characters.*;
import characters.Character;
import gui.GUI;
import gui.Title;
import items.Attachment;
import items.Component;
import items.Consumable;
import items.Flask;
import items.Item;
import items.Potion;
import items.Toy;
import items.Trophy;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import pet.Ptype;


import skills.*;
import status.Disobedient;
import trap.Alarm;
import trap.AphrodisiacTrap;
import trap.Decoy;
import trap.DissolvingTrap;
import trap.EnthrallingTrap;
import trap.IllusionTrap;
import trap.Snare;
import trap.Spiderweb;
import trap.SpringTrap;
import trap.StripMine;
import trap.TentacleTrap;
import trap.Trap;
import trap.Tripline;

import actions.Action;
import actions.Bathe;
import actions.Craft;
import actions.Drink;
import actions.Energize;
import actions.Hide;
import actions.JerkOff;
import actions.Locate;
import actions.Movement;
import actions.Recharge;
import actions.Resupply;
import actions.Scavenge;
import actions.SetTrap;
import actions.Use;
import actions.Wait;
import areas.Area;
import areas.MapDrawHint;
import daytime.Daytime;
import daytime.Scene;


public class Global {
	private static Random rng;
	private static GUI gui;
	private static HashSet<Skill> skillPool;
	private static ArrayList<Action> actionPool;
	private static ArrayList<Trap> trapPool;
	private static HashSet<Trait> featPool;
	private static HashSet<Flag> flags;
	private static HashMap<Flag,Float> counters;
	private static HashMap<Attribute,HashMap<Integer,String>> skillList;
	private static Player human;
	private static Match match;
	private static Daytime day;
	public static Scene current;
	public static boolean debug = false;
	
	public Global(GUI gui){
		rng=new Random();
		flags = new HashSet<Flag>();
		
		counters = new HashMap<Flag,Float>();
		current=null;
		this.gui = gui;
		this.gui.BuildGUI();
		buildActionPool();
		buildFeatPool();
		buildSkillList();
	}
	public static void newGame(Player one, boolean tutorial){
		human = one;
		gui.populatePlayer(human);
		buildSkillPool(human);
		Global.gainSkills(human,false);
        Roster.reset(one);
		Scheduler.reset();
		if(tutorial){
			new Tutorial(one).play("");
		}else{
			Scheduler.dusk();
		}
	}
	
	public static int random(int d){
		if(d<=0){
			return 0;
		}
		return rng.nextInt(d);
	}

	public static GUI gui(){
		return gui;
	}

	public static Player getPlayer(){
		return human;
	}
    public static void setPlayer(Character player){
        human = (Player) player;
    }


	public static void buildSkillPool(Character p){
		skillPool=new HashSet<Skill>();
		
		//Seduction
		skillPool.add(new Kiss(p));
		skillPool.add(new FondleBreasts(p));
		skillPool.add(new Fuck(p));
		skillPool.add(new Tickle(p));
		skillPool.add(new Blowjob(p));
		skillPool.add(new Cunnilingus(p));
		skillPool.add(new LickNipples(p));
		skillPool.add(new Paizuri(p));
		skillPool.add(new SuckNeck(p));
		skillPool.add(new Whisper(p));
		skillPool.add(new Footjob(p));
		skillPool.add(new Handjob(p));
		skillPool.add(new Finger(p));
		skillPool.add(new Piston(p));
		skillPool.add(new Grind(p));
		skillPool.add(new Thrust(p));
		skillPool.add(new Carry(p));
		skillPool.add(new Tighten(p));
		skillPool.add(new AssFuck(p));
		skillPool.add(new Frottage(p));
		skillPool.add(new FingerAss(p));
		
		//Cunning
		skillPool.add(new StripTop(p));
		skillPool.add(new StripBottom(p));
		skillPool.add(new Escape(p));
		skillPool.add(new Maneuver(p));
		skillPool.add(new Reversal(p));
		skillPool.add(new Taunt(p));
		skillPool.add(new Trip(p));
		skillPool.add(new Turnover(p));
		
		//Power
		skillPool.add(new Shove(p));
		skillPool.add(new Slap(p));
		skillPool.add(new ArmBar(p));		
		skillPool.add(new Flick(p));
		skillPool.add(new Knee(p));
		skillPool.add(new LegLock(p));
		skillPool.add(new Restrain(p));
		skillPool.add(new Spank(p));
		skillPool.add(new Stomp(p));
		skillPool.add(new Tackle(p));
		skillPool.add(new Kick(p));
		skillPool.add(new Squeeze(p));
		skillPool.add(new Nurple(p));
		skillPool.add(new Tear(p));
		
		//Arcane
		skillPool.add(new MagicMissile(p));
		skillPool.add(new Binding(p));
		skillPool.add(new Illusions(p));
		skillPool.add(new NakedBloom(p));
		skillPool.add(new Barrier(p));
		skillPool.add(new MageArmor(p));
		skillPool.add(new ManaFortification(p));
		
		//Dark
		skillPool.add(new LustAura(p));
		skillPool.add(new TailPeg(p));
		skillPool.add(new Dominate(p));
		skillPool.add(new DarkTendrils(p));
		skillPool.add(new Sacrifice(p));
		skillPool.add(new Fly(p));
		skillPool.add(new Drain(p));
		skillPool.add(new LustOverflow(p));
		
		//Ki
		skillPool.add(new WaterForm(p));
		skillPool.add(new FlashStep(p));
		skillPool.add(new FlyCatcher(p));
		skillPool.add(new StoneForm(p));
		skillPool.add(new FireForm(p));
		skillPool.add(new IceForm(p));
		skillPool.add(new PleasureBomb(p));
		
		//Science
		skillPool.add(new StunBlast(p));
		skillPool.add(new ShrinkRay(p));
		skillPool.add(new ShortCircuit(p));
		skillPool.add(new Defabricator(p));
		skillPool.add(new Fabricator(p));
		skillPool.add(new MatterConverter(p));
		
		//Fetish
		skillPool.add(new Masochism(p));
		skillPool.add(new Bondage(p));
		skillPool.add(new TentaclePorn(p));
		skillPool.add(new FaceFuck(p));
		skillPool.add(new BondageStraps(p));
		skillPool.add(new TortoiseWrap(p));
		skillPool.add(new Nymphomania(p));
		
		//Animism
		skillPool.add(new CatsGrace(p));
		skillPool.add(new TailJob(p));
		skillPool.add(new Purr(p));
		skillPool.add(new PheromoneOverdrive(p));
		skillPool.add(new BeastTF(p));
		
		//Ninjutsu
		skillPool.add(new StealClothes(p));
		skillPool.add(new BunshinAssault(p));
		skillPool.add(new BunshinService(p));
		skillPool.add(new Substitute(p));
		skillPool.add(new GoodnightKiss(p));
		skillPool.add(new KoushoukaBurst(p));
		
		//Submissive
		skillPool.add(new Dive(p));
		skillPool.add(new Cowardice(p));
		skillPool.add(new Stumble(p));
		skillPool.add(new Invite(p));
		skillPool.add(new Beg(p));
		skillPool.add(new ShamefulDisplay(p));
		skillPool.add(new Buck(p));
		skillPool.add(new HoneyTrap(p));
		skillPool.add(new PleasureSlave(p));

		//Discipline
        skillPool.add(new WarningCrop(p));
        skillPool.add(new MastersOrder(p));
        skillPool.add(new DominatingGaze(p));
        skillPool.add(new BerserkerBarrage(p));
        skillPool.add(new RegainComposure(p));
        skillPool.add(new Punishment(p));
		
		//Professional
		skillPool.add(new Blindside(p));
		
		//Hypnosis
		skillPool.add(new Suggestion(p));
		skillPool.add(new HeightenSenses(p));
		skillPool.add(new Humiliate(p));
		skillPool.add(new EnflameLust(p));
		skillPool.add(new CrushPride(p));
		
		//Temporal
		skillPool.add(new WindUp(p));
		skillPool.add(new Haste(p));
		skillPool.add(new CheapShot(p));
		skillPool.add(new AttireShift(p));
		skillPool.add(new EmergencyJump(p));
		skillPool.add(new Unstrip(p));
		skillPool.add(new Rewind(p));
		
		//Special
		skillPool.add(new PerfectTouch(p));
		skillPool.add(new HipThrow(p));
		skillPool.add(new SpiralThrust(p));
		skillPool.add(new Bravado(p));
		skillPool.add(new Diversion(p));
		
		//Item
		skillPool.add(new Tie(p));
		skillPool.add(new UseDildo(p));
		skillPool.add(new UseOnahole(p));
		skillPool.add(new UseCrop(p));
		skillPool.add(new Undress(p));
		skillPool.add(new Strapon(p));
		skillPool.add(new VibroTease(p));
		skillPool.add(new Scroll(p));
		skillPool.add(new DarkTalisman(p));
		skillPool.add(new Needle(p));
		skillPool.add(new UsePowerband(p));
		skillPool.add(new UseExcalibur(p));
		skillPool.add(new UseClamps(p));
		skillPool.add(new InsertBead(p));
		skillPool.add(new PullBeads(p));
		
		//Misc
		skillPool.add(new Release(p));
		skillPool.add(new Masturbate(p));
		skillPool.add(new FaceSit(p));
		skillPool.add(new GoldCock(p));
		skillPool.add(new LowBlow(p));
		skillPool.add(new KittyKick(p));
		skillPool.add(new Rotate(p));
		
		//Pets
		skillPool.add(new SpawnFaerie(p,Ptype.fairyfem));
		skillPool.add(new SpawnImp(p,Ptype.impfem));
		skillPool.add(new SpawnFaerie(p,Ptype.fairymale));
		skillPool.add(new SpawnImp(p,Ptype.impmale));
		skillPool.add(new SpawnSlime(p));
		skillPool.add(new SpawnFGoblin(p));		
		
		//Commands
		skillPool.add(new Command(p));
		skillPool.add(new Obey(p));
		skillPool.add(new CommandDismiss(p));
		skillPool.add(new CommandDown(p));
		skillPool.add(new CommandGive(p));
		skillPool.add(new CommandHurt(p));
		skillPool.add(new CommandInsult(p));
		skillPool.add(new CommandMasturbate(p));
		skillPool.add(new CommandOral(p));
		skillPool.add(new CommandStrip(p));
		skillPool.add(new CommandStripPlayer(p));
		skillPool.add(new CommandUse(p));		
		
		//EX
		skillPool.add(new KissEX(p));
		skillPool.add(new KneeEX(p));
		skillPool.add(new HandjobEX(p));
		skillPool.add(new StripTopEX(p));
		skillPool.add(new StripBottomEX(p));
		skillPool.add(new FingerEX(p));
		skillPool.add(new ThrustEX(p));
	}

	public static void buildActionPool(){
		actionPool=new ArrayList<Action>();
		actionPool.add(new Resupply());
		actionPool.add(new Bathe());
		actionPool.add(new Scavenge());
		actionPool.add(new Craft());
		actionPool.add(new Use(Flask.Lubricant));
		actionPool.add(new Recharge());
		actionPool.add(new JerkOff());
		actionPool.add(new Energize());
		actionPool.add(new Hide());
		for(Potion p:Potion.values()){
			actionPool.add(new Drink(p));
		}
		buildTrapPool();
		for(Trap t:trapPool){
			actionPool.add(new SetTrap(t));
		}
		actionPool.add(new Wait());
	}
	
	public static void buildSkillList(){
		skillList = new HashMap<Attribute,HashMap<Integer,String>>();
		HashMap<Integer,String> powerlist = new HashMap<Integer,String>();
		powerlist.put(6, "Turnover");
		powerlist.put(7, "Slap");
		powerlist.put(8, "Pin");
		powerlist.put(9, "Squeeze Balls");
		powerlist.put(10, "Knee");
		powerlist.put(13, "Twist Nipple");
		powerlist.put(16, "Stomp");
		powerlist.put(17, "Kick");
		powerlist.put(20, "Armbar");
		powerlist.put(24, "Leg Lock");
		powerlist.put(26, "Tackle");
		powerlist.put(30, "Carry");
		powerlist.put(32, "Tear Clothes");
		skillList.put(Attribute.Power, powerlist);
		
		HashMap<Integer,String> seductionlist = new HashMap<Integer,String>();
		seductionlist.put(8, "Spank");
		seductionlist.put(10, "Lick Pussy");
		seductionlist.put(12, "Suck Neck");
		seductionlist.put(14, "Lick Nipples");
		seductionlist.put(17, "Flick");
		seductionlist.put(18, "Piston");
		seductionlist.put(20, "Ass Fuck");
		seductionlist.put(22, "Footjob");
		seductionlist.put(24, "Strip Tease");
		seductionlist.put(26, "Frottage");
        seductionlist.put(28, "Finger Ass");
		seductionlist.put(32, "Whisper");
		skillList.put(Attribute.Seduction, seductionlist);
		
		HashMap<Integer,String> cunninglist = new HashMap<Integer,String>();
		cunninglist.put(6, "Alarm");
		cunninglist.put(7, "Decoy");
		cunninglist.put(8, "Taunt");
		cunninglist.put(11, "Snare");
		cunninglist.put(12, "Escape");
		cunninglist.put(14, "Aphrodisiac Trap");
		cunninglist.put(15, "Focus");
		cunninglist.put(16, "Trip");
		cunninglist.put(17, "Dissolving Trap");
		cunninglist.put(20, "Maneuver");
		cunninglist.put(24, "Reversal");
		cunninglist.put(28, "Shortcuts");
		cunninglist.put(30, "Double Strip");
		skillList.put(Attribute.Cunning, cunninglist);
		
		HashMap<Integer,String> arcanelist = new HashMap<Integer,String>();
		arcanelist.put(1, "Magic Missile");
		arcanelist.put(3, "Faerie Familiar");
		arcanelist.put(6, "Illusion Trap");
		arcanelist.put(9, "Binding");
		arcanelist.put(12, "Illusions");
		arcanelist.put(15, "Naked Bloom");
		arcanelist.put(18, "Barrier");
		arcanelist.put(21, "Mage Armor");
		arcanelist.put(24, "Royal Faeries");
		arcanelist.put(30, "Mana Fortification");
		skillList.put(Attribute.Arcane, arcanelist);
		
		HashMap<Integer,String> darklist = new HashMap<Integer,String>();
		darklist.put(1, "Energy Drain");
		darklist.put(3, "Lust Aura");
		darklist.put(6, "Summon Imp");
		darklist.put(9, "Domination");
		darklist.put(12, "Dark Tendrils");
		darklist.put(15, "Sacrifice");
		darklist.put(18, "Fly");
		darklist.put(21, "Drain");
		darklist.put(24, "Shadow Fingers");
		skillList.put(Attribute.Dark, darklist);
		
		HashMap<Integer,String> kilist = new HashMap<Integer,String>();
		kilist.put(1, "Shredding Palm");
		kilist.put(3, "Water Form");
		kilist.put(6, "Flash Step");
		kilist.put(9, "Fly Catcher");
		kilist.put(12, "Stone Form");
		kilist.put(15, "Fire Form");
		kilist.put(18, "Ice Form");
		kilist.put(21, "Shatter Kick");
		kilist.put(30, "Pleasure Bomb");
		skillList.put(Attribute.Ki, kilist);
		
		HashMap<Integer,String> sciencelist = new HashMap<Integer,String>();
		sciencelist.put(1, "Shock Glove");
		sciencelist.put(3, "Slime");
		sciencelist.put(6, "Aerosolizer");
		sciencelist.put(9, "Stun Blast");
		sciencelist.put(12, "Shrink Ray");
		sciencelist.put(15, "Short Circuit");
		sciencelist.put(18, "Defabricator");
		sciencelist.put(21, "Fabricator");
		sciencelist.put(30, "Matter Converter");
		skillList.put(Attribute.Science, sciencelist);
		
		HashMap<Integer,String> fetishlist = new HashMap<Integer,String>();
		fetishlist.put(1, "Exhibitionism");
		fetishlist.put(3, "Bondage");
		fetishlist.put(6, "Fetish Goblin");
		fetishlist.put(9, "Masochism");
		fetishlist.put(12, "Tentacle Porn");
		fetishlist.put(15, "Face Fuck");
		fetishlist.put(18, "Bondage Straps");
		fetishlist.put(21, "Tortoise Wrap");
		fetishlist.put(30, "Nymphomania");
		skillList.put(Attribute.Fetish, fetishlist);
		
		HashMap<Integer,String> animismlist = new HashMap<Integer,String>();
		animismlist.put(1, "Pounce");
		animismlist.put(3, "Cat's Grace");
		animismlist.put(4, "Feral Power");
		animismlist.put(6, "Tail Job");
		animismlist.put(9, "Purr");
		animismlist.put(12, "Tiger Claw");
		animismlist.put(15, "Shred Clothes");
		animismlist.put(30, "Pheromone Pink Overdrive");
		skillList.put(Attribute.Animism, animismlist);
		
		HashMap<Integer,String> ninjutsulist = new HashMap<Integer,String>();
		ninjutsulist.put(1, "Needle");
		ninjutsulist.put(3, "Smoke Bomb");
		ninjutsulist.put(5, "Enhanced Mobility");
		ninjutsulist.put(6, "Bunshin Assault");
		ninjutsulist.put(9, "Stash");
		ninjutsulist.put(12, "Bunshin Service");
		ninjutsulist.put(15, "Steal Clothes");
		ninjutsulist.put(18, "Goodnight Kiss");
		ninjutsulist.put(21, "Substitution");
		ninjutsulist.put(30, "Fertility Ritual");
		skillList.put(Attribute.Ninjutsu, ninjutsulist);

        HashMap<Integer,String> disciplinelist = new HashMap<Integer,String>();
        disciplinelist.put(1, "Composure");
        disciplinelist.put(3, "Warning Crop");
        disciplinelist.put(6, "Master's Order");
        disciplinelist.put(9, "Confident Stance");
        disciplinelist.put(12, "Dominating Gaze");
        disciplinelist.put(15, "Reward Strike");
        disciplinelist.put(18, "Berserker Barrage");
        disciplinelist.put(21, "Regain Composure");
        disciplinelist.put(30, "Punishment");
        skillList.put(Attribute.Discipline,disciplinelist);
		
		HashMap<Integer,String> submissivelist = new HashMap<Integer,String>();
		submissivelist.put(1, "Dive");
		submissivelist.put(3, "Cowardice");
		submissivelist.put(6, "Invite");
		submissivelist.put(9, "Stumble");
		submissivelist.put(12, "Beg");
		submissivelist.put(15, "Shameful Display");
		submissivelist.put(18, "Buck");
		submissivelist.put(21, "Honey Trap");
		submissivelist.put(30, "Pleasure Slave");
		skillList.put(Attribute.Submissive, submissivelist);
		
		HashMap<Integer,String> professionallist = new HashMap<Integer,String>();
		professionallist.put(1, "Tempting Whisper");
		professionallist.put(3, "Pro Handjob");
		professionallist.put(5, "Pro Oral");
		professionallist.put(7, "Charming Strip Tease");
		professionallist.put(9, "Blindside");
		professionallist.put(11, "Pro Thrust");
		skillList.put(Attribute.Professional, professionallist);
		
		HashMap<Integer,String> temporallist = new HashMap<Integer,String>();
		temporallist.put(1, "Haste");
		temporallist.put(2, "Cheap Shot");
		temporallist.put(4, "Emergency Jump");
		temporallist.put(6, "Attire Shift");
		temporallist.put(8, "Unstrip");
		temporallist.put(10, "Rewind");
		skillList.put(Attribute.Temporal, temporallist);
		
		HashMap<Integer,String> hypnosislist = new HashMap<Integer,String>();
		hypnosislist.put(1, "Suggestion");
		hypnosislist.put(2, "Enflame Lust");
		hypnosislist.put(4, "Heighten Senses");
		hypnosislist.put(6, "Humiliate");
		hypnosislist.put(8, "Crush Pride");
		skillList.put(Attribute.Hypnosis, hypnosislist);
	}
	
	public static void populateTargetedActions(Collection<Character> combatants){
		for(Character active: combatants){
			actionPool.add(new Locate(active));
		}
	}

	public static void buildTrapPool(){
		trapPool=new ArrayList<Trap>();
		trapPool.add(new Alarm());
		trapPool.add(new Tripline());
		trapPool.add(new Snare());
		trapPool.add(new SpringTrap());
		trapPool.add(new AphrodisiacTrap());
		trapPool.add(new DissolvingTrap());
		trapPool.add(new Decoy());
		trapPool.add(new Spiderweb());
		trapPool.add(new EnthrallingTrap());
		trapPool.add(new IllusionTrap());
		trapPool.add(new StripMine());
		trapPool.add(new TentacleTrap());
	}
	public static void buildFeatPool(){
		featPool = new HashSet<Trait>();
		featPool.add(Trait.sprinter);
		featPool.add(Trait.QuickRecovery);
		featPool.add(Trait.Sneaky);
		featPool.add(Trait.Confident);
		featPool.add(Trait.SexualGroove);
		featPool.add(Trait.BoundlessEnergy);
		featPool.add(Trait.Unflappable);
		featPool.add(Trait.resourceful);
		featPool.add(Trait.treasureSeeker);
		featPool.add(Trait.sympathetic);
		featPool.add(Trait.leadership);
		featPool.add(Trait.tactician);
		featPool.add(Trait.PersonalInertia);
		featPool.add(Trait.fitnessNut);
		featPool.add(Trait.expertGoogler);
		featPool.add(Trait.mojoMaster);
		featPool.add(Trait.fastLearner);
		featPool.add(Trait.veryfastLearner);
		featPool.add(Trait.cautious);
		featPool.add(Trait.responsive);
		featPool.add(Trait.assmaster);
		featPool.add(Trait.Clingy);
		featPool.add(Trait.houdini);
		featPool.add(Trait.coordinatedStrikes);
		featPool.add(Trait.evasiveManuevers);
		featPool.add(Trait.handProficiency);
		featPool.add(Trait.handExpertise);
		featPool.add(Trait.handMastery);
		featPool.add(Trait.oralProficiency);
		featPool.add(Trait.oralExpertise);
		featPool.add(Trait.oralMastery);
		featPool.add(Trait.intercourseProficiency);
		featPool.add(Trait.intercourseExpertise);
		featPool.add(Trait.intercourseMastery);
		featPool.add(Trait.footloose);
		featPool.add(Trait.amateurMagician);
		featPool.add(Trait.bountyHunter);
		featPool.add(Trait.challengeSeeker);
		featPool.add(Trait.showoff);
		featPool.add(Trait.tieGuy);
	}

	public static ArrayList<Action> getActions(){
		return actionPool;
	}
	public static HashSet<Trait> getFeats(){
		return featPool;
		
	}
	
	public static String getUpcomingSkills(Attribute att, int level){
		String list = att.name()+"("+level+"): ";
		int found = 0;
		if(skillList.containsKey(att)){
			HashMap<Integer, String> skills = skillList.get(att);
			ArrayList<Integer> levels = new ArrayList<Integer>();
			levels.addAll(skills.keySet());
			levels.sort(null);
			for(int i: levels){
				if(level < i){
					list += String.format("%s (%d), ",skills.get(i),i);
					found++;
					if(found >= 5){
						break;
					}
				}
			}
		}
		if(found > 0 ){
			return list;
		}else{
			return "";
		}
	}
	public static void gainSkills(Character c){
		gainSkills(c,true);
	}
	public static void gainSkills(Character c, boolean display){
		for(Skill skill:skillPool){
			if(skill.requirements(c)&&!c.knows(skill.copy(c))){
				c.learn(skill.copy(c));
				if(display && c.human()){
					gui().message("You've learned "+skill.toString()+".");
				}
			}
		}
		if(c.getPure(Attribute.Dark)>=6&&!c.has(Trait.darkpromises)){
			c.add(Trait.darkpromises);
		}
		if(c.getPure(Attribute.Animism)>=2&&!c.has(Trait.pheromones)){
			if(c.human()){
				gui().message("You've gained the passive skill Pheromones.");
			}
			c.add(Trait.pheromones);
		}
		if(c.getPure(Attribute.Fetish)>=1&&!c.has(Trait.exhibitionist)){
			if(c.human()){
				gui().message("You've gained the passive skill Exhibitionist.");
			}
			c.add(Trait.exhibitionist);
		}
	}

	public static void learnSkills(Player p){
		for(Skill skill:skillPool){
			if(skill.requirements()&&!p.knows(skill)){
				p.learn(skill);
			}
		}
		if(p.getPure(Attribute.Dark)>=6&&!p.has(Trait.darkpromises)){
			p.add(Trait.darkpromises);
		}
		if(p.getPure(Attribute.Animism)>=2&&!p.has(Trait.pheromones)){
			p.add(Trait.pheromones);
		}
		if(p.getPure(Attribute.Fetish)>=3&&!p.has(Trait.exhibitionist)){
			p.add(Trait.exhibitionist);
		}
	}


	public static void flag(Flag f){
		flags.add(f);
	}

	public static void unflag(Flag f){
		flags.remove(f);
	}

	public static boolean checkFlag(Flag f){
		return flags.contains(f);
	}

	public static HashSet<Flag> getFlags(){
		return flags;
	}

	public static float getValue(Flag f){
		if(!counters.containsKey(f)){
			return 0;
		}
		else{
			return counters.get(f);
		}
	}
	public static void modCounter(Flag f, float inc){
		if(!counters.containsKey(f)){
			setCounter(f,inc);
		}
		else{
			counters.put(f, getValue(f)+inc);
		}
	}
	public static void setCounter(Flag f, float val){
		counters.put(f,val);
	}

	public static HashMap<Flag,Float> getCounters(){
		return counters;
	}

	public static Item getItem(String name){
		for(Flask f: Flask.values()){
			if(f.toString().equalsIgnoreCase(name)){
				return f;
			}
		}
		for(Potion p: Potion.values()){
			if(p.toString().equalsIgnoreCase(name)){
				return p;
			}
		}
		for(Component c: Component.values()){
			if(c.toString().equalsIgnoreCase(name)){
				return c;
			}
		}
		for(Consumable c: Consumable.values()){
			if(c.toString().equalsIgnoreCase(name)){
				return c;
			}
		}
		for(Trophy t: Trophy.values()){
			if(t.toString().equalsIgnoreCase(name)){
				return t;
			}
		}
		for(Toy t: Toy.values()){
			if(t.toString().equalsIgnoreCase(name)){
				return t;
			}
		}
		for(Attachment a: Attachment.values()){
			if(a.toString().equalsIgnoreCase(name)){
				return a;
			}
		}
		return null;
	}

	public static void main(String[] args){
		new GUI();
	}


	public static String getIntro() {
		return "Warning This game contains depictions of explicit sexual content. Do not proceed unless you are at least 18 and are comfortable with such content. "
				+ "All characters depicted are over 18 years of age.\n\n"
				+ "You don't really know why you're going to the Student Union in the middle of the night. You'd have to be insane to accept the invitation you received this afternoon. Seriously, someone " +
			"is offering you money to sexfight a bunch of girls? You're more likely to get mugged (though you're not carrying any money) or murdered if you show up. Best case scenario, it's probably a prank " +
			"for gullible freshmen. You have no good reason to believe the invitation is on the level, but here you are, walking into the empty Student Union.\n\n"
			+ "Not quite empty, it turns out. The same " +
			"woman who approached you this afternoon greets you and brings you to a room near the back of the building. Inside, you're surprised to find three quite attractive girls. After comparing notes, " +
			"you confirm they're all freshmen like you and received the same invitation today. You're surprised, both that these girls would agree to such an invitation, and that you're the only guy here. For " +
			"the first time, you start to believe that this might actually happen.\n\n"
			+ "After a few minutes of awkward small talk (though none of these girls seem self-conscious about being here), the woman walks " +
			"in again leading another girl. Embarrassingly you recognize the girl, named Cassie, who is a classmate of yours, and who you've become friends with over the past couple weeks. She blushes when she " +
			"sees you and the two of you consciously avoid eye contact while the woman explains the rules of the competition.\n\n"
			+ "There are a lot of specific points, but the rules basically boil down to this:\n" +
			"*Competitors move around the empty areas of the campus and engage each other in sexfights.\n"
			+ "*When one competitor orgasms, the other gets a point and can claim their clothes.\n"
			+ "*Additional orgasms between those two players are not worth any points until the loser gets a replacement set of clothes at either the Student Union or the first floor of the dorm building.\n "
			+ "*It seems to be customary, but not required, for the loser to get the winner off after a fight, when it doesn't count.\n"
			+ "*After three hours, the match ends and each player is paid for each opponent they defeat, each set of clothes taken, and a bonus for whoever scores the most points.\n\n"
			+ "After the explanation, she confirms with each participant whether they are still interested in participating.\n\n"
			+ "Everyone agrees. The first match starts at exactly 10:00.";
	}

	public static void reset(){
		flags.clear();
		human=new Player("Dummy");
		gui.purgePlayer();
		Global.gui().clearText();

	}
	public static String pickRandom(Object[] array) {		
		return array[Global.random(array.length)].toString();
	}
	public static String pickRandom(Object[] array,Integer[] weights){
		int total=0;
		int choice;
		int count = 0;
		for(Integer w : weights){
			total+=w;
		}
		choice = random(total);
		for(int i=0; i<array.length; i++){
			count += weights[i];
			if(choice <= count){
				return array[i].toString();
			}
		}
		return array[0].toString();
	}
}
