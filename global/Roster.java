package global;

import characters.*;
import characters.Character;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashSet;

public class Roster {
    private static Roster instance;
    private ArrayList<Character> players;
    private HashSet<Relationship> affection;
    private HashSet<Relationship> attraction;

    public static Roster getInstance(){
        if(instance==null){
            instance = new Roster();
        }
        return instance;
    }

    private Roster(){
        players = new ArrayList<Character>();
        affection = new HashSet<Relationship>();
        attraction = new HashSet<Relationship>();
        players.add(Global.getPlayer());
        players.add(new Cassie().character);
        players.add(new Mara().character);
        players.add(new Angel().character);
        players.add(new Jewel().character);
        players.add(new Yui().character);
        players.add(new Kat().character);
        players.add(new Reyka().character);
        players.add(new Eve().character);
        players.add(new Samantha().character);
        players.add(new Valerie().character);
        players.add(new Maya().character);
    }
    public static void reset(Player p){
        instance = new Roster();
        instance.players.remove(get(ID.PLAYER));
        instance.players.add(p);
    }

    public static NPC getNPC(String name){
        for( Character c : getInstance().players){
            if(c.name().equalsIgnoreCase(name)){
                return (NPC)c;
            }
        }
        return null;
    }
    public static Character get(ID identity){
        for( Character c : getInstance().players){
            if(identity == c.id()){
                return c;
            }
        }
        return null;
    }

    public static Character get(String name){
        for( Character c : getInstance().players){
            if(c.name().equalsIgnoreCase(name)){
                return c;
            }
        }
        return null;
    }

    public static boolean exists(ID npc){
        switch (npc){
            case EVE:
                return Global.checkFlag(Flag.Eve);
            case KAT:
                return Global.checkFlag(Flag.Kat);
            case REYKA:
                return Global.checkFlag(Flag.Reyka);
            case YUI:
                return Global.checkFlag(Flag.metYui);
            case SAMANTHA:
                return Global.checkFlag(Flag.Samantha);
            case VALERIE:
                return Global.checkFlag(Flag.Valerie);
        }
        return true;
    }

    public static void scaleOpponent(ID npc){
        int level = get(ID.PLAYER).getLevel();
        get(npc).scaleLevel(level);
    }

    public static int getAttraction(ID p1, ID p2){
        if(!exists(p1) || !exists(p2)){
            return 0;
        }
        for( Relationship r: getInstance().attraction){
            if(r.is(p1,p2)){
                return r.get();
            }
        }
        return 0;
    }

    public static void gainAttraction(ID p1, ID p2, int magnitude){
        for( Relationship r: getInstance().attraction) {
            if (r.is(p1, p2)) {
                r.add(magnitude);
                return;
            }
        }
        Relationship meeting = new Relationship(p1,p2);
        meeting.set(magnitude);
        getInstance().attraction.add(meeting);
    }

    public static void setAttraction(ID p1, ID p2, int magnitude){
        for( Relationship r: getInstance().attraction) {
            if (r.is(p1, p2)) {
                r.set(magnitude);
                return;
            }
        }
        Relationship meeting = new Relationship(p1,p2);
        meeting.set(magnitude);
        getInstance().attraction.add(meeting);
    }

    public static int getAffection(ID p1, ID p2){
        if(!exists(p1) || !exists(p2)){
            return 0;
        }
        for( Relationship r: getInstance().affection){
            if(r.is(p1,p2)){
                return r.get();
            }
        }
        return 0;
    }

    public static void gainAffection(ID p1, ID p2, int magnitude){
        for( Relationship r: getInstance().affection) {
            if (r.is(p1, p2)) {
                r.add(magnitude);
                return;
            }
        }
        Relationship meeting = new Relationship(p1,p2);
        meeting.set(magnitude);
        getInstance().affection.add(meeting);
    }

    public static void setAffection(ID p1, ID p2, int magnitude){
        for( Relationship r: getInstance().affection) {
            if (r.is(p1, p2)) {
                r.set(magnitude);
                return;
            }
        }
        Relationship meeting = new Relationship(p1,p2);
        meeting.set(magnitude);
        getInstance().affection.add(meeting);
    }

    public static ArrayList<Character> getExisting(){
        ArrayList<Character> group = new ArrayList<Character>();
        for(Character c: getInstance().players){
            if(exists(c.id())){
                group.add(c);
            }
        }
        return group;
    }
    public static ArrayList<Character> getCombatants(){
        ArrayList<Character> group = new ArrayList<Character>();
        boolean canCompete = true;
        for(Character c: getInstance().players){
            canCompete = true;
            if(!exists(c.id())){
                canCompete = false;
            }
            switch(c.id()){
                case MAYA:
                    canCompete = false;
                    break;
                case YUI:
                    if(!Global.checkFlag(Flag.Yui)){
                        canCompete = false;
                    }
                    break;
            }
            if(canCompete) {
                group.add(c);
            }
        }
        return group;
    }

    public static JsonObject Save(){
        JsonObject saver = new JsonObject();
        for(Character c: Roster.getInstance().players){
            saver.add(c.id().name(),c.save());
        }
        ArrayList<ID> saved = new ArrayList<ID>();
        JsonObject current;
        boolean needed;
        JsonObject affections = new JsonObject();
        for(ID c:ID.values()){
            needed = false;
            current = new JsonObject();
            for(Relationship r: getInstance().affection){
                if(r.contains(c)){
                    if(!saved.contains(r.getPartner(c))){
                        needed = true;
                        current.addProperty(r.getPartner(c).name(),r.get());
                    }
                }
            }
            if(needed) {
                affections.add(c.name(), current);
            }
            saved.add(c);
        }
        saver.add("Affection",affections);
        saved.clear();
        JsonObject attractions = new JsonObject();
        for(ID c:ID.values()){
            needed = false;
            current = new JsonObject();
            for(Relationship r: getInstance().attraction){
                if(r.contains(c)){
                    if(!saved.contains(r.getPartner(c))){
                        needed = true;
                        current.addProperty(r.getPartner(c).name(),r.get());
                    }
                }
            }
            if(needed) {
                attractions.add(c.name(), current);
            }
            saved.add(c);
        }
        saver.add("Attraction",attractions);
        return saver;
    }

    public static void Load(JsonObject loader){
        for(Character c: Roster.getInstance().players){
            if(loader.has(c.id().name())){
                c.load(loader.get(c.id().name()).getAsJsonObject());
            }
        }
        getInstance().attraction.clear();
        getInstance().affection.clear();
        ID p1;
        ID p2;
        JsonObject affections = loader.getAsJsonObject("Affection");
        for(String name: affections.keySet()){
            p1 = ID.valueOf(name);
            JsonObject rel = affections.getAsJsonObject(name);
            for(String name2:rel.keySet()){
                p2 = ID.valueOf(name2);
                setAffection(p1,p2,rel.get(name2).getAsInt());
            }
        }
        JsonObject attractions = loader.getAsJsonObject("Attraction");
        for(String name: attractions.keySet()){
            p1 = ID.valueOf(name);
            JsonObject rel = attractions.getAsJsonObject(name);
            for(String name2:rel.keySet()){
                p2 = ID.valueOf(name2);
                setAttraction(p1,p2,rel.get(name2).getAsInt());
            }
        }
    }

}
