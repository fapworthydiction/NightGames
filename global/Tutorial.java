package global;

import java.util.HashMap;

import characters.Anatomy;
import characters.Dummy;
import characters.Emotion;
import characters.Player;
import daytime.Scene;

public class Tutorial implements Scene {
	private int page;
	private Player player;
	private Dummy cassie;
	private Dummy mara;
	private Dummy angel;
	private Dummy jewel;
	public Tutorial(Player player){
		this.player=player;
		Global.current=this;
		cassie = new Dummy("Cassie",1,true);
		mara = new Dummy("Mara",1,true);
		jewel = new Dummy("Jewel",1,true);
		angel = new Dummy("Angel",1,false);
		page = 1; 
	}

	@Override
	public void respond(String response) {
		Global.gui().clearCommand();
		Global.gui().clearText();
		if(page >= 9){
			player.rest();
			Scheduler.getInstance().dusk();
		}
		page++;
		play(response);
		Global.gui().refresh();
	}

	@Override
	public boolean play(String response) {
		switch(page){
		case 1:
			cassie.setBlush(2);
			cassie.setMood(Emotion.nervous);
			Global.gui().loadPortrait(player, cassie);
			Global.gui().message("<i>\"Most nights, we will begin the match immediately, but since it's your first match, I want to spend "
					+ "some time going over the basics of sexual combat.\"</i><p>"
					+ "The woman who brought you here, Maya, has you pair up with Cassie, while two of the other girls are paired together. You suspect she put you together "
					+ "to break the awkward air between you.<br>"
					+ "Cassie finally makes eye contact with you, though she still looks pretty shy. <i>\"I wasn't really expecting to see "
					+ "anyone I knew here.\"</i> You nod empathetically. Having a friend here makes this all feel a bit too real. Fortunately "
					+ "Cassie is really cute. You could certainly do worse for a sex-fighting partner.<p>"
					+ "Your thoughts are interrupted as you realize Maya is speaking. <i>\"<b>The goal of a fight is of course to make your opponent "
					+ "orgasm.</b> This is in some ways easier and more difficult than it sounds. Fortunately it's pretty easy to arouse young men and "
					+ "women like you. See how much you can turn on your partner in the next 30 seconds or so.\"</i><p>"
					+ "Well, this is a more intimate icebreaker than usual. You step toward Cassie a bit self-consciously and confirm she's ready. "
					+ "<i>\"Yeah, go ahead.\"</i> She wets her lips subtly with her tongue and tilts her head invitingly.");
			Global.gui().choose("Kiss");
			break;
		case 2:
			cassie.setBlush(3);
			cassie.setMood(Emotion.confident);
			Global.gui().loadPortrait(player, cassie);			
			Global.gui().message("You press your lips against Cassie's and she responds by pressing her body against yours. You're not the "
					+ "most experienced kisser, but you know the basics. You part your lips and tease out her tongue with yours. She responds "
					+ "enthusiastically, so you take things a step further and fondle her modest breast through her blouse. She lets out a "
					+ "quiet approving noise as you feel her up.<p>");
			Global.gui().displayImage("Cassie Kiss.jpg", "Art by AimlessArt");
			Global.gui().message( "When Maya tells you to stop, Cassie is flushed and breathing heavily. Any awkwardness between you has been replaced "
					+ "with lust. She pushes her hips against you and smiles when she feels your erection. You reluctantly separate as Maya "
					+ "resumes her lecture.<p>"
					+ "<i>\"So as you have surely noticed, clothing isn't much of an obstacle for a little foreplay. However, <b>if you want to "
					+ "make your opponent orgasm, you will probably want to strip them first.</b> Undressing a resisting opponent can be tricky, "
					+ "so let's trade partners and try another exercise.");
			player.pleasure(15, Anatomy.mouth);
			player.buildMojo(5);
			Global.gui().choose("Next");
			break;
		case 3:
			Global.gui().loadPortrait(player, mara);
			Global.gui().message("You are paired with the shortest of the competitors this time, an energetic girl named Mara. She sat out "
					+ "during the last exercise since you have an odd number of people, and she seems pretty eager to get busy.<p>"
					+ "<i>\"As I was saying, it's not that easy to undress a wary and active opponent.\"</i> Maya gestures to Mara's "
					+ "tight-fitting T-shirt. <i>\"You might be able to strip a simple shirt without any preparation, but more difficult "
					+ "undergarments and fastened pants will require softening up your opponent first. <b>Weakening or arousing your opponent "
					+ "will make stripping them more likely to succeed.</b> For this next exercise, see if you can overpower your partner "
					+ "and strip off their top.\"</i><p>"
					+ "You turn your attention back to Mara, who bats her eyes at you cutely. <i>\"You wouldn't hit a little girl would you?\"</i>"
					+ " She's clearly not being sincere, but you are legitimately concerned about attacking her. She's almost a head shorter than "
					+ "you and you really don't want to hurt her accidently.<p> "
					+ "Maya notices your hesitation and approaches, though she speaks loud enough for the other girls to hear. <i>\"This is a "
					+ "full-contact sport. Staff will be overseeing the match to prevent injuries and I know everyone here can take some punishment. You "
					+ "don't need to hold back for the sake of chivalry.\"</i><p>"
					+ "You take her words to heart and focus on Mara again. It's not like you're trying to knock her out, you just need to pin her.");
			Global.gui().choose("Shove");
			break;
		case 4:
			mara.setMood(Emotion.nervous);
			Global.gui().loadPortrait(player, mara);
			Global.gui().message("As Mara cautiously approaches you, you lunge forward and use your superior mass to shove her on her butt. "
					+ "She lands on the floor and winces slightly. You feel a twinge of guilt, but she doesn't seem seriously hurt. Before "
					+ "she can recover, you press your advantage and straddle her waist. She tries to push you off, but she doesn't really "
					+ "have the strength to move you. You are able to catch her hands and pin her helplessly to the floor.<p>"
					+ "Maya gives you a nod of approval. <i>\"Well done. <b>It's significantly easier to strip an opponent when you are in "
					+ "a dominant position.</b> You shouldn't have much trouble stripping her now.\"</i>");
			player.buildMojo(5);
			Global.gui().choose("Strip Top");
			break;
		case 5:
			mara.setBlush(1);
			mara.setMood(Emotion.dominant);
			mara.setTop(false);
			mara.setTopInner(false);
			Global.gui().loadPortrait(player, mara);
			Global.gui().message("You have to release Mara's hands to take off her shirt, but she can't put up much resistance. You pull her "
					+ "T-shirt over her head and toss it aside. Her bra is your next target. You try to reach the clasp, but with her back to "
					+ "the floor, you can't manage it.<p>"
					+ "<i>\"Do you need some help with that?\"</i> Mara taunts you as you struggle with the undergarment. You give up on the "
					+ "clasp and just yank the bra up, exposing her small breasts. She sticks her tongue out at you cutely.<br>"
					+ "She's being pretty cheeky for someone topless and pinned. You decide to tease her newly exposed nipples, pinching them "
					+ "with each hand. She lets out a soft whimper and squirms submissively at your touch.<p>"
					+ "In a moment, her attitude suddenly changes. <i>\"I was waiting for that.\"</i> She winks at you and slips a zip-tie around "
					+ "your wrists before you can react, binding your hands tight. Without your hands, she's easily able to get on top of you and "
					+ "remove your shirt.<p>");
			Global.gui().displayImage("Zip Tie.jpg", "Art by AimlessArt");
			Global.gui().message("<i>\"How's that? I didn't exactly overpower my opponent, but I got his shirt.\"</i> <br>"
					+ "Maya applauds her quietly. <i>\"Well done. It's often a good idea to carry around useful items like that to turn the tide "
					+ "in critical moments.\"</i> She directs her attention to you, still trying to free your hands. <i>\"<b>If you find yourself "
					+ "bound or pinned, you'll need to struggle your way out. Even if it seems impossible at first, the binding will loosen up as "
					+ "you work at it and it will become easier to escape.</b>");
			player.shred(Player.OUTFITTOP);
			Global.gui().choose("Struggle");
			break;
		case 6:
			jewel.setMood(Emotion.dominant);
			jewel.setTop(false);
			jewel.setTopInner(false);
			Global.gui().loadPortrait(player, jewel);
			Global.gui().message("You manage to break free of the zip tie while Maya rearranges the pairs again. This time you are paired with "
					+ "Jewel, the beautiful and extremely fit redhead. She adopts a casual fighting stance and seems to radiate confidence. Her "
					+ "perky breasts are completely exposed, but she doesn't seem the slightest bit embarrassed about it. You can't help watching "
					+ "them jiggle enticingly as she shifts her weight.<p>"
					+ "You're so busy staring that you almost fail to notice Maya speaking again. <i>\"We are going to repeat the previous exercise, "
					+ "focusing on the lower body this time. We will continue until everyone is nude. You may begin.\"</i><p>"
					+ "You focus on Jewel again and are again distracted by her gorgeous boobs. You try to direct your brain to strategizing. She's "
					+ "clearly a lot more physically capable than the small girl you just faced, so you aren't going to simply overpower her. "
					+ "Maybe you can wait for her to attack and find a way to counter her, like Mara did to you...<p>"
					+ "You're still only halfway through formulating a plan when Jewel lunges toward you. She grabs your head and pulls you face-first "
					+ "into her soft cleavage. You're so surprise by this sudden development that you freeze completely. Before you recover from your "
					+ "surprise, her knee shoot up between your legs and slams into your groin. The intense pain robs you of motor control and you "
					+ "slump to the floor pathetically. You instinctively try to curl up in the fetal position, but Jewel pins you down and strips off "
					+ "your pants and underwear.");
			Global.gui().displayImage("Jewel Tutorial.jpg", "Art by AimlessArt");
			player.getStamina().empty();
			player.getArousal().empty();
			player.nudify();
			Global.gui().choose("Stunned");
			break;
		case 7:
			jewel.undress();
			jewel.setBlush(2);
			Global.gui().loadPortrait(player, jewel);
			Global.gui().message("The pain keeps you from focusing on your surroundings, but you hear Maya talking to Jewel. She sounds slightly conflicted. "
					+ "<i>\"That was quite an impressive knee strike. It seems like you have some practice at that. <b>Intense pain, especially to "
					+ "the genitals will reduce your opponent's arousal, but stripping or pinning a stunned opponent is child's play.</b> Pain can be a "
					+ "potent weapon if used sparingly.\"</i><p>"
					+ "You open your eyes and see her face surprising close to yours. She looks genuinely sympathetic. <i>\""+player.name()+", are you "
					+ "OK to continue? If you need to rest until the start of the match, that's fine.\"</i> You take a deep breath before reassuring her "
					+ "that you are alright. The initial pain was pretty overwhelming, but you've already mostly recovered. Maya looks relieved. Her eyes "
					+ "drift down your body and she frowns slightly. You blush as you realize she's looking at your flaccid dick. She taps Jewel on the "
					+ "shoulder and gestures toward your groin. <i>\"He needs an erection for the next lesson, do you mind giving him a hand?\"</i><p>"
					+ "Jewel sits on the floor next to you and starts to gently tease your soft penis. <i>\"Sorry about the nut-shot. It was nothing personal, "
					+ "you were just looking at my breasts so much that you left yourself wide open.\"</i> She doesn't sound sorry, but she does sound "
					+ "more friendly than you expected. <i>\"You should pay less attention to staring at my fun parts and more to protecting your own. Here, I'll "
					+ "show you something good to make us even.\"</i><p>"
					+ "She takes a break from playing with your dick to doff her jeans and panties. She opens her legs to give you a good look at her "
					+ "visibly flushed vulva. <i>\"I don't know if you've ever been in a brawl with a girl, but a solid kick to the clit or pussy lips "
					+ "will put a girl out of commission, just like a boy.\"</i> She resumes stroking your cock with one hand, while the other points "
					+ "out sensitive areas of her groin. <i>\"It's a smaller target than the jewels dangling between your legs, but you aren't completely "
					+ "helpless against female opponents. Now, it looks like you've gotten nice and hard again. I'm looking forward to seeing what the next "
					+ "lesson is.");
			player.heal(20);
			player.pleasure(20, Anatomy.genitals);
			Global.gui().choose("Next");
			break;
		case 8:
			angel.setBlush(1);
			angel.setMood(Emotion.dominant);
			Global.gui().loadPortrait(player, angel);
			Global.gui().message("For the final lesson, you're paired up with the fourth and final girl, Angel. She's a blonde bombshell with a great "
					+ "figure and huge breasts. You get to appreciate her in all her glory, since she's naked like you. Her expression is a mix of "
					+ "confidence and open desire. At Maya's direction, she lies on the floor and spreads her legs invitingly.<p>"
					+ "<i>\"The final lesson is on the subject of sexual intercourse. Now any lesbian will tell you it is possible to accomplish this "
					+ "without a penis involved, but that's a more advanced technique. For now, I'm just going to have the rest of you girls watch.\"</i> "
					+ "she turns to address you directly. <i>\"You may start whenever you are ready.\"</i><p>"
					+ "As the only male present, you are suddenly the center of attention. You're a little nervous about performing in front of all "
					+ "these girls, but Angel is really sexy and you are really horny. You position yourself between her thighs and rub the head of "
					+ "your cock against her wet entrance. Angel licks her lips lustily and smiles at you. <i>\"Come on, fill me with your seed! You "
					+ "probably won't be able to satisfy me, but try not to cum right away.\"</i>");
			Global.gui().choose("Fuck");
			break;
		case 9:
			angel.setBlush(3);
			angel.setMood(Emotion.desperate);
			Global.gui().loadPortrait(player, angel);
			Global.gui().displayImage("Angel Sex.jpg", "Art by AimlessArt");
			Global.gui().message("You thrust your cock into Angel's wonderful pussy. She lets out a sigh of satisfaction and gives you a confident smile. Her "
					+ "insides feel amazing, but you're pretty confident with your technique too. You move your hips steadily at a pace you can sustain for a long "
					+ "time. You adjust the angle gradually, looking for her most sensitive spots. Soon Angel's grin is wiped away and she can't restrain her "
					+ "moans. The girls looking on seem pretty impressed.<p>"
					+ "<i>\"T-That's... Oh Fuck! mmm-!\"</i> Angel bites her lip and her body clenches up, trying to contain her pleasure. You rub your thumb over "
					+ "her engorged clit and feel her shudder.<p>"
					+ "Maya looks on with an amused smile. <i>\"There's no point trying to stay quiet. <b>Regardless "
					+ "of sexual experience, the person on top has a major advantage during sex. If you can't struggle your way on top, you'll end up losing most "
					+ "of the time.</b>\"</i> Angel arches her back and moans with abandon, no longer "
					+ "able to contain her voice. Maya gives you an approving nod. <i>\"Feel free to cum inside her whenever you're ready. She's been finished for a "
					+ "while. Our judges never miss an orgasm, male or female.\"</i><p>"
					+ "You speed up your thrusts and let yourself indulge in the feel of Angel's insides. Soon you've hit your peak and shoot a hot load of cum into "
					+ "her womb. The onlooking girls murmur excitedly as you pull out. They sound eager to give your cock a try. You can feel your pride swell a bit "
					+ "at overcoming your most experienced opponent so intimately. <b>It seems that making your opponent cum during penetration has permanently "
					+ "boosted your maximum Mojo.</b><p>"
					+ "Maya claps a couple times to get everyone's attention. <i>\"I think everyone gets the basic understanding. Tomorrow I'll arrange a mentor for each of "
					+ "you to help explain some more advanced strategies. As a final piece of advice, let me just say, don't worry if you have a bad night or two. Even if "
					+ "you fall behind a bit, You're sure to catch up in no time. For now, everyone get dressed. Your first match is about to start.\"</i>");
			player.getArousal().max();
			Global.gui().choose("Start the Match");
		}
		return true;
	}

	@Override
	public String morning() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String mandatory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		// TODO Auto-generated method stub
		
	}

}
