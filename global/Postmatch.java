package global;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;

import gui.KeyableButton;
import gui.SaveButton;
import gui.SceneButton;
import characters.Character;
import characters.Dummy;
import characters.Emotion;
import characters.Player;
import characters.Yui;
import daytime.Scene;
import daytime.YuiEvent;

public class Postmatch implements Scene {
	private Character player;
	private ArrayList<Character> combatants;
	private boolean normal;
	public Postmatch(Character player,ArrayList<Character> combatants){
		this.player = player;
		this.combatants = combatants;
		normal = true;
		for(Character self: combatants){
			for(Character other: combatants){
				if(self.getAffection(other)>=1){
					if(self.getAttraction(other)>=20){
						self.gainAttraction(other, -20);
						self.gainAffection(other, 2);
					}
				}
			}
		}
		Global.current= this;
		String message = "";
		ArrayList<KeyableButton> choice = new ArrayList<KeyableButton>();
		choice.add(new SceneButton("Collect Payment"));
		Global.gui().prompt(message, choice);
	}
	@Override
	public void respond(String response) {
		if(response.startsWith("Collect")){
			Global.gui().clearText();
			events();
			if(normal){
				normal();
			}	
		}
		else if(response.startsWith("Next")){
			normal();
		}else if(response.startsWith("Take her back to your dorm")){
			Global.gui().message("");
			if(Global.checkFlag(Flag.autosave)){
				SaveManager.save(true);
			}
			Global.gui().endMatch();
		}else if(response.startsWith("Take Yui")){
			Global.current = new YuiEvent(player);
			Global.current.play("VirginityUndressing");
		}

	}
	private void events(){
		String message = "";
		ArrayList<KeyableButton> choice = new ArrayList<KeyableButton>();
		if(Global.checkFlag(Flag.MaraDayOff)){
			message+="";
			normal = false;
			choice.add(new SceneButton("Take her back to your dorm"));
		}
		else if(Global.checkFlag(Flag.YuiUnlocking) && !Global.checkFlag(Flag.Yui)){
			Dummy yui = new Dummy("Yui",1,true);
			yui.setMood(Emotion.nervous);
			Global.gui().loadPortrait(player, yui);
			message+="After the match, Lilly calls you over. <i>\"You have a guest.\"</i> You're confused for a moment, until you see Yui, looking extremely "
					+ "guilty.<p>"
					+ "<i>\"I'm sorry, Master. I happened to see you leaving your dorm late at night, and I was curious.\"</i> Oh shit. You know Yui "
					+ "didn't mean any harm, but the girl's curiosity has really caused trouble this time. You can only assume she saw something she "
					+ "wasn't suppose to.<p>"
					+ "Lilly nods, her expression grave. <i>\"Despite our safeguards, she saw almost half the match before we caught her. I heard she's "
					+ "your friend, so I'm giving you a chance to take responsibility for letting yourself be followed and accept the consequence in her place.\"</i><p>"
					+ "You nod without hesitation. Whatever punishment is forthcoming should fall on you, not Yui. Yui, however, immediately protests.<br>"
					+ "<i>\"No Master! This isn't your fault! I'm the one who followed you! I'm the one who should be punished!\"</i> She looks close to tears. "
					+ "You put your hand on her shoulder to try to calm her. You may have agreed to be her master on a whim, but you did agree. This "
					+ "is exactly the sort of situation where a master should protect his retainer.<p>"
					+ "Lilly holds up her hands to stop you from talking. <i>\"Uh, wait. Sorry.\"</i> Now she looks guilty. <i>\"I was just messing with you "
					+ "two. I didn't mean for this to turn into a melodrama. Sorry.\"</i> She fidgets with her hair awkwardly. <i>\"This isn't a common situation, "
					+ "but it's far from unprecedented. It's not actually a big deal. This girl promised not to gossip about the Games, and that's good enough "
					+ "for me.\"</i><p>"
					+ "You let out a sigh of relief and pat Yui on the back. Lilly continues, directing her attention to the kunoichi. <i>\"Actually, "
					+ "I'm kinda impressed. We take measures to keep outsiders from wandering into the match area, but you were able to sneak around "
					+ "for a solid hour without getting spotted. If you want to watch matches in the future, you should just come talk to me. That is "
					+ "assuming, of course, "+player.name()+" doesn't mind you watching his fights.\"</i> Yui blushes bright red and doesn't look you "
					+ "in the eye. If she was watching the match, she probably saw a very different side of you than usual.<p>"
					+ "Come to think of it, Yui can probably do a lot more than just watch the matches. In all the excitement you forgot that you were "
					+ "suppose to invite her to join the Games. You explain what Aesop told you to both girls. Lilly nods in sudden comprehension. "
					+ "<i>\"Ah, so she's the new transfer student Maya was talking about. That explains a lot.\"</i> She looks Yui over with an approving smile. "
					+ "<i>\"You got a sneak peek of how the games work, so I don't need to give you the pitch. So, are you interested in participating? "
					+ "You'll be very well paid.\"</i><p>"
					+ "Yui immediately perks up. <i>\"I can have sex with Master?!\"</i> She immediately blushes at her own words, as Lilly bursts out "
					+ "laughing.<br>"
					+ "<i>\"Yes. "+player.name()+" is a regular participant, so you will be engaging in 'intimate' competition with him frequently. However, "
					+ "you can't just cherry-pick your partners. You'll be fair game for the other competitors, who are female. Are you comfortable with that?\"</i><p>"
					+ "Yui considers this for a moment and nods. <i>\"You're talking about the girls I saw tonight? They're all pretty cute.\"</i> She glances at "
					+ "you shyly before she continues. <i>\"I prefer men, but I'm a lot more... open to idea than most girls, I think.\"</i> She glances at "
					+ "you again, fidgeting awkwardly. It seems like your presence is making her uncomfortable talking about this sort of things. "
					+ "She might be more open talking girl-to-girl.<p>"
					+ "You try to excuse yourself, but Yui grabs your hand before you can walk away. She doesn't say anything, but the gesture requires no "
					+ "explanation. Lilly looks at your clasped hands, but continues without commenting on it. <i>\"I don't want to be rude, but I have to "
					+ "ask before you can join. How much sexual experience do you have?\"</i><p>"
					+ "Yui turns bright red and looks at the floor. She's obvious incredibly embarrassed to answer in front of you, but she's still holding "
					+ "your hand. <i>\"I've been trained in a wide variety of sexual techniques to pleasure male or female partners with my hands, mouth, "
					+ "feet or... other parts. I only turned 18 a couple weeks ago, so before that I was only allowed to practice on toys, anatomical models, "
					+ "and... myself. As far as doing things with a real person... I haven't...\"</i> She trails off, overwhelmed by embarrassment.<p>"
					+ "<i>\"A virgin, huh?\"</i> Lilly considers this, idly toying with her pigtail. <i>\"Your training should offset your lack of experience, and I "
					+ "assume if you're training with toys, your hymen must be broken. However, I still don't want a repeat of last year. You should have at "
					+ "least a night of normal sex before you start doing it competitively.\"</i><p>"
					+ "Lilly turns her attention toward you. <i>\"Can I ask you to show the girl a good time tonight? I'd love to take her to bed myself, but she "
					+ "seems pretty attached already. I'm sure she'd rather have you as her first.\"</i><p>"
					+ "If Yui's firm grip on your hand is any indication, that's probably true, but it's important to ask these things properly. You gently place "
					+ "two fingers under Yui's chin and tilt her head up to face you. When she doesn't resist, you kiss her tenderly on the lips and invite her "
					+ "to your room. She nods happily, her face red as a tomato.<p>"
					+ "<i>\"Quite the Casanova.\"</i> Lilly sounds almost impressed. <i>\"I think I see why the Benefactor picked you. Have fun tonight and she can "
					+ "start competing tomorrow night.\"</i>";
			
			normal = false;
			choice.add(new SceneButton("Take Yui to your room"));
		}
		else if(Global.checkFlag(Flag.metLilly)&&!Global.checkFlag(Flag.challengeAccepted)&&Global.random(10)>=7){
			Dummy jewel = new Dummy("Jewel",1,true);
			jewel.setMood(Emotion.angry);
			Global.gui().loadPortrait(player, jewel);
			message+="When you gather after the match to collect your reward money, you notice Jewel is holding a crumpled up piece of paper and ask about it. " +
					"<i>\"This? I found it lying on the ground during the match. It seems to be a worthless piece of trash, but I didn't want to litter.\"</i> Jewel's face is expressionless, " +
					"but there's a bitter edge to her words that makes you curious. You uncrumple the note and read it.<p>"
					+ "'Jewel always acts like the dominant, always-on-top tomboy, " +
					"but I bet she loves to be held down and fucked hard.'<p>"
					+ "<i>\"I was considering finding whoever wrote the note and tying his penis in a knot,\"</i> Jewel says, still " +
					"impassive. <i>\"But I decided to just throw it out instead.\"</i> It's nice that she's learning to control her temper, but you're a little more concerned with the note. " +
					"It mentions Jewel by name and seems to be alluding to the Games. You doubt one of the other girls wrote it. You should probably show it to Lilly.<p>"
					+ "...<p>"
					+ "<i>\"Oh for fuck's " +
					"sake..\"</i> Lilly sighs, exasperated. <i>\"I thought we'd seen the last of these. I don't know who writes them, but they showed up last year too. I'll have to do a second " +
					"sweep of the grounds each night to make sure they're all picked up by morning. They have competitors' names on them, so we absolutely cannot let a normal student find " +
					"one.\"</i> She toys with a pigtail idly while looking annoyed. <i>\"For what it's worth, <b>they do seem to pay well if you do what the note says that night.</b> Do with them what " +
					"you will.\"</i><br>";
			Global.flag(Flag.challengeAccepted);
			choice.add(new SceneButton("Next"));
		}
		Global.gui().prompt(message, choice);
	}
	private void normal(){
		Character closest=null;
		int maxaffection=0;
		for(Character rival:combatants){
			if(rival!=player && rival.getAffection(player)>maxaffection){
				closest=rival;
				maxaffection=rival.getAffection(player);
			}
		}
		
		if(maxaffection>=15&&closest!=null){
			closest.afterParty();
		}
		else{
			Global.gui().message("You walk back to your dorm and get yourself cleaned up.");
		}
		if(Global.checkFlag(Flag.autosave)){
			SaveManager.save(true);
		}
		Global.gui().endMatch();
	}
	@Override
	public boolean play(String response) {
		return true;
		
	}
	@Override
	public String morning() {
		return "";
	}
	@Override
	public String mandatory() {
		return "";
	}
	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		return;
	}

}
