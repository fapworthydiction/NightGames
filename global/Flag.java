package global;

public enum Flag {
	//Introductions
	metBroker,
	metAisha,
	metRin,
	metJett,
	metYui,
	metAlice,
	metSuzume,
	metLilly,
	metGinette,
    metDanielle,
	
	//Information
	basicStores,
	blackMarket,
	meditation,
	girlAdvice,
	CassieKnown,
	JewelKnown,
	AngelKnown,
	MaraKnown,
	magicstore,
	blackMarketPlus,
	dojo,
	workshop,
	darkness,
	fetishism,
	excalibur,
	goldcockstart,
	
	//Story
	Cassievg1,
	Cassievg2,
	CassieMaravg,
	challengeAccepted,
	Maravg1,
	maravg2,
	MaraDayOff,
	MaraRoute,
	rank1,
	furry,
	catspirit,
	YuiAvailable,
	YuiUnlocking,
	YuiLoyalty,
	Clue1,
	Mayabeaten,
	Lillyphone,
	GinetteHelper,
	ValerieUnlock,
	JustineSub,
	ValerieSub,

	//Bonuses
	MoreCache1,
	CacheNoBasics,
	CacheFinder,
	
	//Unlocked NPCs
	Eve,
	Kat,
	Reyka,
	Samantha,
	Maya,
	Yui,
	Valerie,
	
	//NPC Counters
	CassieLoneliness,
	CassieDWV,	
	JewelDWV,
	AngelDWV,
	MaraDWV,
	KatDWV,
	ReykaDWV,
	YuiAffection,
	CarolineAffection,
	YuiDWV,
	ChallengesCompleted,
	HandicapMatches,
	PlayerAssLosses,
	ThreesomeCnt,
	MatchWins,
	IntercourseWins,
	LillyFriendship,
	GangRank,
	
	//Options
	autosave,
	noimage,
	exactimages,
	noportraits,
	portraits2,
	altcolors,
	largefonts,
	noReports,
	statussprites,
	
	//Difficulty
	hardmode,
	challengemode,
	doublexp,
	shortmatches,
	
	//Daily
	threesome,
	victory,
	AliceAvailable,
	night,
    OffSeason,

	//Progress
	VGskill,
	SeasonMatches;

}
