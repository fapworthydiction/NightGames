package skills;

import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class FingerEX extends Skill {

	public FingerEX(Character self) {
		super("Shining Finger", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachBottom(self)&&(target.pantsless()||(self.has(Trait.dexterous)&&target.bottom.size()<=1))&&target.hasPussy()&&
				self.canAct()&&(!c.stance.penetration(target)||c.stance.en==Stance.anal)&&
				self.canSpend(20);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		Result style = Result.seductive;
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			style = Result.powerful;
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			style = Result.clever;
		}
		self.spendMojo(20);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(style == Result.powerful){
				m = 4 + Global.random(self.get(Attribute.Power)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
			}else if(style == Result.clever){
				m = 4 + Global.random(self.get(Attribute.Cunning)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
			}else{
				m = 4 + Global.random(self.get(Attribute.Seduction)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
				m*=1.3f;
			}

			if(self.getPure(Attribute.Professional)>=3){
				if(self.has(Trait.sexuallyflexible)){
					self.add(new ProfessionalMomentum(self,self.get(Attribute.Professional)*5),c);
				}else{
					self.add(new ProfMod("Dexterous Momentum",self,Anatomy.fingers,self.get(Attribute.Professional)*5),c);
				}
			}
			if(self.human()){
				c.write(self,deal(m,style,target));
				c.offerImage("Fingering.jpg", "Art by AimlessArt");
			}
			else if(target.human()){
				c.write(self,receive(m,style,target));
			}
			m = self.bonusProficiency(Anatomy.fingers, m);
			target.pleasure(m,Anatomy.genitals,Result.finisher,c);
			if(self.has(Trait.roughhandling)){
				target.weaken(m/2,c);
				if(self.human()){
					c.write(self,deal(m,Result.unique,target));
				}
				else if(target.human()){
					c.write(self,receive(m,Result.unique,target));
				}
			}

		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}
	@Override
	public boolean requirements() {
		return self.get(Attribute.Seduction)>=8;
	}
	public int accuracy(){
		return 7;
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Seduction)>=8;
	}

	@Override
	public Skill copy(Character user) {
		return new FingerEX(user);
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.unique){
			return "You opportunistically give her clit a sharp pinch.";
		}
		if(modifier == Result.miss){
			if(Global.random(2)==0){
				return "You try to slide your hand down to "+target.name()+"'s pussy, but she bats your hand away.";
			}else{
				return "You grope at "+target.name()+"'s pussy, but miss.";

			}
		}
		if(modifier==Result.powerful){
			return String.format("You plunge two fingers deep into %s's wet pussy. She gasps at the strong penetration, and can barely stay "
					+ "standing as you jackhammer your fingers inside her.",target.name());
		}
		else if(modifier==Result.clever){
			return String.format("You explore %s's feminine garden with your finger, watching her reactions closely. Whenever you find a spot "
					+ "that provokes a stronger reaction, you focus on rubbing and teasing it until she's trembling with pleasure.",target.name());
		}
		else{
			switch(Global.random(2)){
			case 1:
				return "Your hand practically glows with an awesome power. You slip two fingers into " + target.name() + "'s pussy. You pour your love, your anger, even your sorrow into her, leaving her a quivering mess when you're done.";
			default:
				return String.format("You skillfully pleasure %s's eager vagina with your best fingering technique. You cycle between tracing her labia, "
						+ "teasing her clit, and rubbing her G-spot. You switch targets frequently enough that she never gets used to the sensation.", target.name());
			}

		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.unique){
			return String.format("%s gives your poor clit a painful pinch.",
                    self.name());
		}
		if (modifier == Result.miss) {
            return String.format("%s gropes at your pussy, but misses the mark.",
                            self.name());
        }else if(modifier == Result.critical){
        	return String.format("%s slides her shadow tentacles between your legs. The tendrils delve into your slick hole, overwhelming "
        			+ "you with a strange pleasure", self.name());
        }
        else {
            if (target.getArousal().get() <= 15) {
                return String.format("%s softly rubs your sensitive lower lips. You aren't very aroused yet, but %s gentle touch "
                		+ "gives you a ticklish pleasure",
                		self.name(),self.possessive(false));
            } else if (target.getArousal().percent() < 50) {
                return String.format("%s skillfully fingers your sensitive pussy. You bite your lip and try to ignore the pleasure, "
                		+ "but despite your best efforts, you feel yourself growing wet.",
                		self.name());
            } else if (target.getArousal().percent() < 80) {
                return String.format("%s locates your clitoris and caress it directly, causing"
                                + " you to tremble from the powerful stimulation.",
                                self.name());
            } else {
                return String.format("%s stirs your increasingly soaked pussy with %s fingers and "
                                + "rubs your clit directly with %s thumb.",
                                self.name(),self.possessive(false),self.possessive(false));
            }
        }
	}
	
	@Override
	public String toString(){
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Intense Finger";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Probing Finger";
		}
		return name;
	}


	@Override
	public String describe() {
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Strong forceful fingering: 20 Mojo";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Exploratory fingering to identify sensitive areas: 20 Mojo";
		}else{
			return "Mojo boosted shining finger: 20 Mojo";
		}
	}
}
