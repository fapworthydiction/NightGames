package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Hypersensitive;
import status.Stsflag;

public class HeightenSenses extends Skill {

	public HeightenSenses(Character self) {
		super("Heighten Senses", self);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Hypnosis)>=5;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis)>=5;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.behind(self)&&
				!c.stance.behind(target)&&!c.stance.sub(self)&&(target.is(Stsflag.charmed)||target.is(Stsflag.enthralled))&&
				(!target.is(Stsflag.hypersensitive)||target.getPure(Attribute.Perception)<9);
	}

	@Override
	public String describe() {
		return "Hypnotize the target to temporarily become more sensitive";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.is(Stsflag.hypersensitive)&&Global.random(2)==0){
			if(self.human()){
				c.write(self,deal(0,Result.strong,target));
			}else{
				c.write(self,receive(0,Result.strong,target));
			}
			target.mod(Attribute.Perception,1);
		}else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}else{
				c.write(self,receive(0,Result.normal,target));
			}
			target.add(new Hypersensitive(target),c);
		}
		target.emote(Emotion.nervous, 20);

	}

	@Override
	public Skill copy(Character user) {
		return new HeightenSenses(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.strong){
			return String.format("You plant a suggestion in %s's head to increase %s sensitivity. %s accepts the suggestion so easily and "
					+ "strongly that you suspect it may have had a permanent effect.",target.name(),target.possessive(false),target.pronounSubject(false));
		}else{
			return String.format("You plant a suggestion in %s's head to increase %s sensitivity. %s shivers as %s sense of touch is "
					+ "amplified",self.name(),target.possessive(false),target.pronounSubject(false),target.possessive(false));
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.strong){
			return String.format("%s explains to you that your body, especially your erogenous zones, have become more sensitive. %s's right. "
					+ "All your senses feel heightened. You feel almost like a superhero. It's ok if this is permanent, right?", 
					self.name(), self.pronounSubject(false));
		}else{
			return String.format("%s explains to you that your body, especially your erogenous zones, have become more sensitive. "
					+ "You feel goosebumps cover your skin as if you've been hit by a Sensitivity Flask. Maybe you were and just "
					+ "didn't notice", self.name());
		}
	}

}
