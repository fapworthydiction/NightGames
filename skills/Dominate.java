package skills;

import stance.StandingOver;
import characters.Attribute;
import characters.Character;
import characters.Emotion;

import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class Dominate extends Skill {

	public Dominate(Character self) {
		super("Dominate", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Dark)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>=9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !c.stance.sub(self)&&!c.stance.prone(self)&&!c.stance.prone(target)&&self.canAct();
	}

	@Override
	public String describe() {
		return "Overwhelm your opponent to force her to lie down: 10 Arousal";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendArousal(10);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
			if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Cassie")){
				c.offerImage("Dominating.jpg", "Art by AimlessArt");
			}
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
			if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Angel")){
				c.offerImage("Dominated.jpg", "Art by AimlessArt");
			}
		}
		c.stance = new StandingOver(self,target);
		self.emote(Emotion.dominant, 20);
		target.emote(Emotion.nervous,20);
	}

	@Override
	public Skill copy(Character user) {
		return new Dominate(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You take a deep breath, gathering dark energy into your lungs. You expend the power to command "+target.name()+" to submit. The demonic command renders her " +
				"unable to resist and she drops to floor, spreading her legs open to you. As you approach, she comes to her senses and quickly closes her legs. Looks like her " +
				"will is still intact.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" forcefully orders you to \"Kneel!\" Your body complies without waiting for your brain and you drop to your knees in front of her. She smiles and " +
				"pushes you onto your back. By the time you break free of her suggestion, you're flat on the floor with her foot planted on your chest.";
	}

}
