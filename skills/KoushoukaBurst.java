package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Horny;

public class KoushoukaBurst extends Skill {

	public KoushoukaBurst(Character self) {
		super("Fertility Rite", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Ninjutsu)>=30;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=30;
	}


	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct() && !target.canAct() && target.pantsless() && self.canSpend(25) && c.stance.reachBottom(self);
	}

	@Override
	public String describe() {
		return "A lengthy ritual that causes an overwhelming urge to have sex: 25 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(25);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.add(new Horny(target,20,10),c);

	}

	@Override
	public Skill copy(Character user) {
		return new KoushoukaBurst(user);
	}

	public int speed(){
		return 1;
	}
	
	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(target.hasBalls()){
			return String.format("You carefully grab %s balls between your fingers and quickly complete the gestures Yui showed you. This "
					+ "should briefly boost %s sperm and testosterone production, giving %s an uncontrollable need to reproduce.",
					target.name(),target.possessive(false),target.pronounTarget(false));
		}else{
			return String.format("You make a series of gestures on %s bare abdomen, which should, in theory, stimulate the pressure points "
					+ "around her ovaries and womb. You see her face quickly flush as she starts to tremble with an intense need for sex.",
					target.name());
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s kneels between your legs and carefully holds your bare balls. %s makes a series of hand movements, "
				+ "while muttering something under %s breath. When %s finishes, a desperate heat starts to grow in your testicles, "
				+ "and you feel overwhelmed by a desire to cum.",
				self.name(),self.pronounSubject(true),self.possessive(false),self.pronounSubject(false));
	}

}
