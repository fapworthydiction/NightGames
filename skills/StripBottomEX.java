package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;

import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class StripBottomEX extends Skill {

	public StripBottomEX(Character self) {
		super("Tricky Strip Bottoms", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachBottom(self)&&!target.pantsless()&&self.canAct()&&self.canSpend(15);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int strip = 0;
		Result style = Result.clever;
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			style = Result.powerful;
		}else if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning)){
			style = Result.seductive;
		}
		self.spendMojo(15);
		if(style == Result.powerful){
			strip = self.get(Attribute.Power);
		}else if(style == Result.clever){
			strip = self.get(Attribute.Cunning);
			strip *= 3;
		}else{
			strip = self.get(Attribute.Seduction);
		}
		if(target.stripAttempt(strip,self, c, target.bottom.peek())){
			if(self.human()){
				c.write(self,deal(0,style,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Yui")){
					c.offerImage("Strip Bottom Female.jpg", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(0,style,target));
				if(!Global.checkFlag(Flag.exactimages)||self.id()== ID.MARA){
					c.offerImage("Strip Bottom Male.jpg", "Art by AimlessArt");
				}
			}
			target.strip(1, c);
			if(self.getPure(Attribute.Cunning)>=30 &&! target.pantsless()){
				if(target.stripAttempt(strip,self, c, target.bottom.peek())){
					if(self.human()){
						c.write(self,deal(0,Result.strong,target));
						
					}
					else if(target.human()){
						c.write(self,receive(0,Result.strong,target));
					}
					target.strip(1, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if(self.human()&&target.nude()){
				c.write(target,target.nakedLiner());
			}
			if(target.human()&&target.pantsless()){
				if(target.getArousal().get()>=15){
					c.write("Your boner springs out, no longer restrained by your pants.");
				}
				else{
					c.write(self.name()+" giggles as your flacid dick is exposed");
				}
			}
			target.emote(Emotion.nervous, 10);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripBottomEX(user);
	}
	public int speed(){
		return 3;
	}
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You grab %s's %s, but %s scrambles away before you can strip %s.",
					target.name(),target.bottom.peek().getName(),target.pronounSubject(false),target.pronounTarget(false));
		}else if(modifier==Result.strong){
			return String.format("Taking advantage of the situation, you also manage to snag %s %s.", target.possessive(false), target.bottom.peek().getName());
		}
		if(modifier==Result.powerful){
			return String.format("You forcefully rip %s's %s down to the floor before %s can resist.",
					target.name(),target.bottom.peek().getName(),target.pronounSubject(false));
		}
		else if(modifier==Result.clever){
			return String.format("You playfully give %s a sharp pinch on the butt. When %s yelps and reflexively covers %s bottom, you switch to your real target and "
					+ "pull down %s %s.",
					target.name(),target.pronounSubject(false),target.possessive(false),target.possessive(false),target.bottom.peek().getName());
		}
		else{
			return String.format("With a smile and a wink, you manage to literally charm the %s off of %s.",
					target.bottom.peek().getName(),target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to pull down your "+target.bottom.peek().getName()+", but you hold them up.";
		}else if(modifier==Result.strong){
			return String.format("Before you can react, %s also strips off your %s.", self.pronounSubject(false), target.bottom.peek().getName());
		}
		if(modifier==Result.powerful){
			return String.format("%s lunges at you and yanks down your %s with unexpected force.",
					self.name(),target.bottom.peek().getName());
		}
		else if(modifier==Result.clever){
			return String.format("%s slips a hand between your legs to lightly pinch your balls. You reflexively flinch and move to protect your delicate parts, but %s "
					+ "takes advantage of your momentary panic to pull down your %s.",
					self.name(),self.pronounSubject(false),target.bottom.peek().getName());
		}
		else{
			return String.format("%s gives you a dazzling smile and suggestively tugs at the waistband of your %s. It would be trivially easy to stop %s, "
					+ "but your desire gets the better of you, and you let yourself be stripped.",
					self.name(),target.bottom.peek().getName(),self.pronounTarget(false));
		}
	}
	
	@Override
	public String toString(){
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Forceful Strip Bottoms";
		}else if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning)){
			return "Charming Strip Bottoms";
		}
		return name;
	}
	
	@Override
	public String describe() {
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Remove opponent's pants with great force: 15 Mojo";
		}else if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning)){
			return "Attempt to remove opponent's pants by seduction: 15 Mojo";
		}
		return "Attempt to remove opponent's pants with extra Mojo: 15 Mojo";
	}
}
