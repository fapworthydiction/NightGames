package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class PullBeads extends Skill {
    public PullBeads(Character self) {
        super("Pull Out Beads", self);
    }

    @Override
    public boolean requirements() {
        return true;
    }

    @Override
    public boolean requirements(Character user) {
        return true;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct()&&c.stance.reachBottom(self)&&target.pantsless()&&target.is(Stsflag.beads);
    }

    @Override
    public String describe() {
        return "Pull all the anal beads out of your opponent, dealing pleasure based on how many are inside";
    }

    @Override
    public void resolve(Combat c, Character target) {
        int beads = target.getStatus(Stsflag.beads).mag();
        int m = (10+(target.get(Attribute.Perception)/2)+(self.get(Attribute.Science)/2))*beads;
        m = self.bonusProficiency(Anatomy.toy,m);
        if(self.human()){
            c.write(self,deal(beads,Result.normal,target));
        }else if(target.human()){
            c.write(self,receive(beads,Result.normal,target));
        }
        target.removeStatus(Stsflag.beads,c);
        target.pleasure(m,Anatomy.ass,c);
    }

    @Override
    public Skill copy(Character user) {
        return new PullBeads(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(damage>1){
            return "You pull the string of "+damage+" beads out of "+target.name()+"'s anus.";
        } else{
            return "You yank the bead out of "+target.name()+"'s ass with an audible 'pop'.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(damage>1){
            return self.name()+" grabs the string of anal beads sticking out of your ass and yanks them all out at once. Your " +
                    "mind goes blank ass you're assaulted by the sensation of your asshole repeatedly getting forced open.";
        } else{
            return self.name()+" pulls the anal bead out of your ass, causing an indescribable sensation.";
        }
    }
}
