package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import global.Modifier;
import global.Scheduler;
import status.Primed;

public class Unstrip extends Skill {

	public Unstrip(Character self) {
		super("Unstrip", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Temporal)>=8;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal)>=8;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&!c.stance.prone(self)&&self.canAct()&&self.getStatusMagnitude("Primed")>=6&&self.nude()&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.nudist)&&!c.stance.penetration(self)&&!c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Rewinds your clothing's time to when you were still wearing it: 6 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.change(null);
		self.add(new Primed(self,-6));
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.emote(Emotion.confident, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new Unstrip(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("It's tricky, but with some clever calculations, you restore the state of your outfit. Your outfit from the "
				+ "start of the night reappears on your body.");
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("You lose sight of %s for just a moment and almost do a double-take when you see %s again, fully dressed. "
				+ "In the second you looked away, how did %s find the time to put %s clothes on?!",
				self.name(),self.pronounTarget(false),self.pronounSubject(false),self.possessive(false));
	}

}
