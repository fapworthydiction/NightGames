package skills;

import stance.ReverseMount;
import stance.SixNine;
import status.Enthralled;
import status.ProfMod;
import status.ProfessionalMomentum;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Cunnilingus extends Skill {

	public Cunnilingus(Character self) {
		super("Lick Pussy", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless()&&target.hasPussy()&&c.stance.oral(self)&&self.canAct()&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			int m=0;
			if(self.getPure(Attribute.Professional)>=5){
				if(self.has(Trait.silvertongue)){
					m = Global.random(8)+self.get(Attribute.Professional)+(2*self.get(Attribute.Seduction)/3)+target.get(Attribute.Perception);
					if(self.human()){
						c.write(self,deal(m, Result.special, target));
					}
				}
				else{
					m = Global.random(6)+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception);
					if(self.human()){
						c.write(self,deal(m, Result.normal, target));
					}
				}
				if(self.has(Trait.sexuallyflexible)){
					self.add(new ProfessionalMomentum(self,self.get(Attribute.Professional)*5),c);
				}else{
					self.add(new ProfMod("Oral Momentum",self,Anatomy.mouth,self.get(Attribute.Professional)*5),c);
				}
			}
			else if(self.has(Trait.silvertongue)){
				m += Global.random(8)+(2*self.get(Attribute.Seduction)/3)+target.get(Attribute.Perception);
				if(self.human()){
					if (!target.has(Trait.succubus))
						c.write(self,deal(m, Result.special, target));
					else {
						if (Global.random(5) == 0) {
							this.self.tempt(5,c);
							if(target.get(Attribute.Dark)>=6&&Global.random(2)==0){
								c.write(self,deal(-2, Result.special, target));
								this.self.add(new Enthralled(self,target),c);
							}
							else{
								c.write(self,deal(-1, Result.special, target));
							}
						}
						else{
							c.write(self,deal(m, Result.special, target));
						}
					}
				}
			}
			else{
				m = Global.random(6)+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception);
				if(self.human()){
					if (!target.has(Trait.succubus))
						c.write(self,deal(m, Result.normal, target));
					else {
						if (Global.random(5) == 0) {
							this.self.tempt(5,c);
							if(target.get(Attribute.Dark)>=6&&Global.random(2)==0){
								c.write(self,deal(-2, Result.normal, target));
								this.self.add(new Enthralled(self,target),c);
							}
							else{
								c.write(self,deal(-1, Result.normal, target));
							}
						}
						else{
							c.write(self,deal(m, Result.normal, target));
						}
					}
				}
			}
			if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Yui")){
				c.offerImage("Cunnilingus.jpg", "Art by AimlessArt");
			}
			m += self.getMojo().get()/10;
			if(target.has(Trait.lickable)){
				m*=1.5;
			}
			m = self.bonusProficiency(Anatomy.mouth, m);
			target.pleasure(m,Anatomy.genitals,c);
			if(ReverseMount.class.isInstance(c.stance)){
				c.stance=new SixNine(self,target);
			}
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=10;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=10;
	}

	@Override
	public Skill copy(Character user) {
		return new Cunnilingus(user);
	}
	public int speed(){
		return 2;
	}
	public int accuracy(){
		return 6;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			if(Global.random(2)==0){
				return "You try to get between "+target.name()+"'s legs to use your tongue but she kicks you away.";
			}else{
				return "You try to eat out "+target.name()+", but she pushes your head away.";
			}
			
		}
		if(target.getArousal().get()<10){
			if(Global.random(2)==0){
				return "You part "+target.name()+"'s dry lips with your wet tongue and lap between them until her own lubrication starts to mix with yours.";
			}else{
				return "You run your tongue over "+target.name()+"'s dry vulva, lubricating it with your saliva.";
			}
			
		}
		if (modifier == Result.special) {
			return "Your skilled tongue explores "
					+ target.name()
					+ "'s pussy, finding and pleasuring her more sensitive areas. You frequently tease her clitoris until she "
					+ "can't suppress her pleasured moans."
					+ (damage == -1 ? " Under your skilled ministrations, her juices flow freely, and they have an unmistakable"
							+ " effect on you"
							: "")
					+ (damage == -2 ? " You feel a strange pull on you mind,"
							+ " somehow she has managed to enthrall you with her juices"
							: "");
		}
		if (target.getArousal().percent() > 80) {
			return "You relentlessly lick and suck the lips of "
					+ target.name()
					+ "'s pussy as she squirms in pleasure. You let up just for a second before kissing her"
					+ " swollen clit, eliciting a cute gasp."
					+ (damage == -1 ? " The highly aroused succubus' vulva is dripping with her "
							+ "aphrodisiac juices and you consume generous amounts of them"
							: "")
					+ (damage == -2 ? " You feel a strange pull on you mind,"
							+ " somehow she has managed to enthrall you with her juices"
							: "");
		}
		int r = Global.random(4);
		if (r == 0) {
			return "You gently lick "
					+ target.name()
					+ "'s pussy and sensitive clit."
					+ (damage == -1 ? " As you drink down her juices, they seem to flow "
							+ "straight down to your crotch, lighting fires when they arrive"
							: "")
					+ (damage == -2 ? " You feel a strange pull on you mind,"
							+ " somehow she has managed to enthrall you with her juices"
							: "");
		}
		if (r == 1) {
			return "You thrust your tongue into "
					+ target.name()
					+ "'s hot vagina and lick the walls of her pussy."
					+ (damage == -1 ? " Your tongue tingles with her juices,"
							+ " clouding your mind with lust"
							: "")
					+ (damage == -2 ? " You feel a strange pull on you mind,"
							+ " somehow she has managed to enthrall you with her juices"
							: "");
		}
		if (r==2){
			return "Your own enjoyment of pleasing your partner feeds in to "+target.name()+"'s reactions as she moans with every flick of your tongue."
					+ (damage == -1 ? " Your tongue tingles with her juices,"
							+ " clouding your mind with lust"
							: "")
					+ (damage == -2 ? " You feel a strange pull on you mind,"
							+ " somehow she has managed to enthrall you with her juices"
							: "");
		}
		return "You locate and capture "
				+ target.name()
				+ "'s clit between your lips and attack it with your tongue"
				+ (damage == -1 ? " Her juices taste wonderful and you cannot"
						+ " help but desire more" : "")
				+ (damage == -2 ? " You feel a strange pull on you mind,"
						+ " somehow she has managed to enthrall you with her juices"
						: "");
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
        String special;
        switch (damage) {
            case -1:
                special = String.format(" Your aphrodisiac juices manage to arouse %s as much as %s aroused you.", 
                                self.name(), self.pronounSubject(false));
                break;
            case -2:
                special = String.format(" Your tainted juices quickly reduce %s into a willing thrall.",
                		self.name());
                break;
            default:
                special = "";
        }
        if (modifier == Result.miss) {
            return String.format("%s tries to tease your cunt with %s mouth, but you push %s face away from you.",
                            self.name(), self.possessive(false),self.possessive(false));
        } else if (modifier == Result.special) {
            return String.format("%s skilled tongue explores your pussy, finding and pleasuring your more sensitive areas. "
                            + "%s repeatedly attacks your clitoris until you can't suppress your pleasured moans.%s",
                            self.name(),self.pronounSubject(true), special);
        } 
        return String.format("%s locates and captures your clit between %s lips and attacks it with %s tongue.%s", 
                        self.name(),self.possessive(false),self.possessive(false), special);

	}

	@Override
	public String describe() {
		return "Perform cunnilingus on opponent, more effective at high Mojo";
	}
}
