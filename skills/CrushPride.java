package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Shamed;
import status.Status;
import status.Stsflag;

public class CrushPride extends Skill {

	public CrushPride(Character self) {
		super("Crush Pride", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Hypnosis)>=8;
	}

	@Override
	public boolean requirements(Character user) {
		return self.getPure(Attribute.Hypnosis)>=8;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&!c.stance.behind(self)&&
				!c.stance.behind(target)&&(target.is(Stsflag.charmed)||target.is(Stsflag.enthralled))&&
				target.is(Stsflag.shamed);
	}

	@Override
	public String describe() {
		return "Exploit opponent's shame to give them extreme masochistic pleasure";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		if(target.is(Stsflag.shamed)){
			Status sts = target.getStatus(Stsflag.shamed);
			if(sts!=null){
				m = target.getStatusMagnitude("Shamed");
				m+= target.getStatusMagnitude("Horny");
				target.tempt(m*self.get(Attribute.Hypnosis)*2, c);
				target.pleasure(1, Anatomy.head,c);
			}
			target.removeStatus(sts,c);
		}
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.emote(Emotion.horny, 30);
		target.emote(Emotion.desperate, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new CrushPride(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("%s is so embarrassed and horny, that it only takes a little push to link the two emotions. You place a "
				+ "simple suggestion in %s head that %s gets off on humiliation. In seconds, %s expression shifts from shame to pure lust.",
				target.name(),target.possessive(false),target.pronounSubject(false),target.possessive(false));
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("As your shame threatens to overwhelm you, %s laughs mockingly and points out that your cock is responding "
				+ "eagerly. Your humiliation fetish is getting the better of you, and %s scornful look is almost enough to make you cum. "
				+ "Since when have you gotten off on humiliation? Your mind is too foggy right now. You'll think about it later.",
				self.name(),self.possessive(false));
	}

}
