package skills;

import items.Component;
import items.Consumable;
import items.Item;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;
import global.Global;

public class Defabricator extends Skill {

	public Defabricator(Character self) {
		super("Defabricator", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Science)>=18;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&!target.nude()&&self.has(Component.Battery,8);
	}

	@Override
	public String describe() {
		return "Does what it says on the tin.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(!target.check(Attribute.Speed, 5+self.get(Attribute.Science)/2)){
		target.nudify();
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
			c.write(target,target.nakedLiner());
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		}else{
			if(self.human()){
				c.write(self,deal(0,Result.weak,target));
				if(target.nude()){
					c.write(target,target.nakedLiner());
				}
			}
			else if(target.human()){
				c.write(self,receive(0,Result.weak,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Defabricator(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.weak){
			return String.format("You charge up your Defabricator and point it in %s's general direction. "
					+ "%s jumps away just as it activates and you're only able to get %s %s.",
					target.name(),target.pronounSubject(true),target.possessive(false),target.shredRandom().getName());

		}
		if(Global.random(2)==0){
			return "You ready your Defabricator then take aim at "+target.name()+", who tries to dash away. You're too fast for her "
					+ "and she soon finds herself basking in the light then completely naked. Isn't technology awesome?";
		}else{
			return String.format("You charge up your Defabricator and point it in %s's general direction. "
					+ "A bright light engulfs %s and %s clothes are disintegrated in moment.",target.name(),target.pronounTarget(false),target.possessive(false));
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.weak){
			return String.format("%s points a device at you and light shines from it like it's a simple flashlight. Instinct causes you to dive out of the way "
					+ "at the last moment. When you land, you realize your %s is gone.",self.name(),target.shredRandom());
		}
		if(Global.random(2)==0){
			return self.name()+" fidgets with a device on her arm, the extends it towards you. You are immediately engulfed in a "
					+ "bright that that leaves you without any clothing, much to her pleasure.";
		}else{
			return String.format("%s points a device at you and light shines from it like it's a simple flashlight. The device's function is immediately revealed as your clothes just vanish " +
					"in the light. You're left naked in seconds.",self.name());
		}
		
	}

}
