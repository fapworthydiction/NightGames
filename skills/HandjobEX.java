package skills;

import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class HandjobEX extends Skill {

	public HandjobEX(Character self) {
		super("Sensual Handjob", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachBottom(self)&&(target.pantsless()||(self.has(Trait.dexterous)&&target.bottom.size()<=1))&&
				target.hasDick()&&self.canAct()&&(!c.stance.penetration(target)||c.stance.en==Stance.anal)&&
				self.canSpend(20);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		Result style = Result.seductive;
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			style = Result.powerful;
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			style = Result.clever;
		}
		self.spendMojo(20);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(style == Result.powerful){
				m = 4+ Global.random(self.get(Attribute.Power)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
			}else if(style == Result.clever){
				m = 4+ Global.random(self.get(Attribute.Cunning)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
			}else{
				m = 4+ Global.random(self.get(Attribute.Seduction)+self.get(Attribute.Professional))+target.get(Attribute.Perception);
				m*=1.3f;
			}

			if(self.getPure(Attribute.Professional)>=3){
				if(self.has(Trait.sexuallyflexible)){
					self.add(new ProfessionalMomentum(self,self.get(Attribute.Professional)*5),c);
				}else{
					self.add(new ProfMod("Dexterous Momentum",self,Anatomy.fingers,self.get(Attribute.Professional)*5),c);
				}
			}
			if(self.human()){
				c.write(self,deal(m,style,target));
			}
			else if(target.human()){
				c.write(self,receive(m,style,target));
				if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Jewel")){
					c.offerImage("Handjob.png", "Art by Fujin Hitokiri");
				}
			}
			m = self.bonusProficiency(Anatomy.fingers, m);
			target.pleasure(m,Anatomy.genitals,Result.finisher,c);
			if(self.has(Trait.roughhandling)){
				target.weaken(m/2,c);
				if(self.human()){
					c.write(self,deal(m,Result.unique,target));
				}
				else if(target.human()){
					c.write(self,receive(m,Result.unique,target));
				}
			}
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.get(Attribute.Seduction)>=8;
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Seduction)>=8;
	}

	@Override
	public Skill copy(Character user) {
		return new HandjobEX(user);
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.unique){
			return "You opportunistically give her balls some rough treatment.";
		}
		if(modifier==Result.miss){
			return "You reach for "+target.name()+"'s dick but miss.";
		}
		if(modifier==Result.powerful){
			return String.format("You forcefully grab %s's cock and jerk it with all your power. %s moans loudly as the intense sensation hits %s",
					target.name(),target.pronounSubject(true),target.pronounTarget(false));
		}
		else if(modifier==Result.clever){
			return String.format("You gently tease %s's dick with your fingertips. Your featherlight strokes probably don't provide much actual "
					+ "stimulation, but you effectively stoke %s arousal by making %s yearn for more.",
					target.name(),target.possessive(false),target.pronounTarget(false));
		}
		else{
			return String.format("You skillfully service %s's penis with slow, tender strokes. This is more complicated than your basic jerk off technique, "
					+ "but you take inspiration from some of the more seductive handjobs you've received.",target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.unique){
			return "She roughly womanhandles your balls, sapping some of your strength.";
		}
		if(modifier==Result.miss){
			return self.name()+" grabs for your dick and misses.";
		}
		if(modifier==Result.powerful){
			return String.format("%s grabs your cock and roughly jerks you off. Her forceful handjob is almost painful, but it's also "
					+ "surprisingly effective.", self.name());
		}
		else if(modifier==Result.clever){
			return String.format("%s plays with your dick, using light teasing touches. Her caress is pretty effective on its own, "
					+ "but the teasing is so frustrating that you involuntary thrust against her hand, arousing yourself further.",self.name());
		}
		else{
			return String.format("%s takes hold of your manhood with a sensual grip and begins to play you like an instrument. Her talented "
					+ "fingers seem to know all the most sensitive spots on your penis and balls. It's so good, you almost forget to fight back.",self.name());
		}
	}
	
	@Override
	public String toString(){
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Rough Handjob";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Teasing Handjob";
		}
		return name;
	}

	@Override
	public String describe() {
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Intense handjob using Power: 20 Mojo";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Careful handjob using Cunning: 20 Mojo";
		}else{
			return "Mojo boosted erotic handjob: 20 Mojo";
		}
	}
}
