package skills;

import pet.FairyFem;
import pet.FairyFemRoyal;
import pet.FairyMale;
import pet.FairyMaleRoyal;
import pet.Ptype;
import characters.Attribute;
import characters.Character;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class SpawnFaerie extends Skill {
	public Ptype gender;
	
	public SpawnFaerie(Character self,Ptype gender) {
		super("Summon Faerie", self);
		this.gender=gender;
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Arcane)>=3;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane)>=3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		int cost = 15;
		if(self.has(Trait.faefriend)){
			cost = 10;
		}
		return self.canActNormally(c)&&self.pet==null&&self.canSpend(cost);
	}

	@Override
	public String describe() {
		int cost = 15;
		if(self.has(Trait.faefriend)){
			cost = 10;
		}
		return "Summon a Faerie familiar to support you: "+cost+" Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int cost = 15;
		if(self.has(Trait.faefriend)){
			cost = 10;
		}
		self.spendMojo(cost);
		if(self.getPure(Attribute.Arcane)>=24){
			int power = 5+self.bonusPetPower()+(self.get(Attribute.Arcane)/10);
			int ac = 8+self.bonusPetEvasion()+(self.get(Attribute.Arcane)/10);
			if(self.human()){
				c.write(self,deal(0,Result.strong,target));
				if(gender==Ptype.fairyfem){	
					self.pet=new FairyFemRoyal(self,power,ac);
					c.offerImage("Fairy_Royal.png", "Art by AimlessArt");
				}
				else{
					self.pet=new FairyMaleRoyal(self,power,ac);
				}
			}
			else{
				if(target.human()){
					c.write(self,receive(0,Result.strong,target));
				}
				self.pet=new FairyFemRoyal(self,power,ac);
				c.offerImage("Fairy_Royal.png", "Art by AimlessArt");
			}
		}else{
			int power = 2+self.bonusPetPower()+(self.get(Attribute.Arcane)/10);
			int ac = 4+self.bonusPetEvasion()+(self.get(Attribute.Arcane)/10);
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
				if(gender==Ptype.fairyfem){	
					self.pet=new FairyFem(self,power,ac);
					c.offerImage("Fairy.png", "Art by AimlessArt");
				}
				else{
					self.pet=new FairyMale(self,power,ac);
				}
			}
			else{
				if(target.human()){
					c.write(self,receive(0,Result.normal,target));
				}
				self.pet=new FairyFem(self,power,ac);
				c.offerImage("Fairy.png", "Art by AimlessArt");
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new SpawnFaerie(user,gender);
	}

	@Override
	public Tactics type() {
		return Tactics.summoning;
	}
	public String toString(){
		if(self.getPure(Attribute.Arcane)>=24){
			if(gender==Ptype.fairyfem){
				return "Faerie Princess";
			}
			else{
				return "Faerie Prince";
			}
		}else{
			if(gender==Ptype.fairyfem){
				return "Faerie (female)";
			}
			else{
				return "Faerie (male)";
			}
		}
		
	}
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.strong){
			if(gender==Ptype.fairyfem){
				return "You start a summoning chant and in your mind, seek out a familiar. A beautiful faerie girl appears in front of you. Despite her nakedness, she wears exquisitely crafted "
						+ "jewelry and hair decorations. She flies around you gracefully before landing delicately on your shoulder.";
			}
			else{
				return "You start a summoning chant and in your mind, seek out a familiar. A six inch tall faerie prince winks into existence in response to your invitation. His tiny crown " +
						"glows with powerful magic as he hovers in the air on dragonfly wings.";
			}
		}else{
			if(gender==Ptype.fairyfem){
				return "You start a summoning chant and in your mind, seek out a familiar. A pretty little faerie girl appears in front of you and gives you a friendly wave before " +
						"landing softly on your shoulder.";
			}
			else{
				return "You start a summoning chant and in your mind, seek out a familiar. A six inch tall faerie boy winks into existence in response to your call. The faerie " +
						"hovers in the air on dragonfly wings.";
			}
		}
		
	}
	@Override
	public boolean equals(Object other){
		return this.getClass()==other.getClass() &&
				this.toString() == other.toString();
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.strong){
			return self.name()+" casts a spell as she extends her hand. In a flash of magic, a small, naked girl with butterfly wings appears in her palm.";
		}else{
			return self.name()+" casts a spell as she extends her hand. In a flash of magic, a small, naked girl with butterfly wings appears in her palm.";
		}
	}

}
