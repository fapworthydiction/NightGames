package skills;

import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;
import status.Stsflag;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Handjob extends Skill {

	public Handjob(Character self) {
		super("Handjob", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachBottom(self)&&(target.pantsless()||(self.has(Trait.dexterous)&&target.bottom.size()<=1))&&target.hasDick()&&self.canAct()&&(!c.stance.penetration(target)||c.stance.en==Stance.anal);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 4 +target.get(Attribute.Perception);
		Result type = Result.normal;
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.get(Attribute.Seduction)>=8){
				m += Global.random(self.get(Attribute.Seduction)/2);	
				self.buildMojo(10);
			}
			else if(target.human()){
				type = Result.weak;
			}
			if(self.getPure(Attribute.Professional)>=3){
				m += self.get(Attribute.Professional);
				type = Result.special;
				self.buildMojo(15);
				if(self.has(Trait.sexuallyflexible)){
					self.add(new ProfessionalMomentum(self,self.get(Attribute.Professional)*5),c);
				}else{
					self.add(new ProfMod("Dexterous Momentum",self,Anatomy.fingers,self.get(Attribute.Professional)*5),c);
				}
			}
			if(self.is(Stsflag.shadowfingers)){
				type = Result.critical;
			}
			if(self.human()){
				c.write(self,deal(m,type,target));
			}
			else if(target.human()){
				c.write(self,receive(m,type,target));
			}
			m = self.bonusProficiency(Anatomy.fingers, m);
			target.pleasure(m,Anatomy.genitals,Result.finisher,c);
			if(self.has(Trait.roughhandling)){
				target.weaken(m/2,c);
				if(self.human()){
					c.write(self,deal(m,Result.unique,target));
				}
				else if(target.human()){
					c.write(self,receive(m,Result.unique,target));
				}
			}
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
		if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Jewel")){
			c.offerImage("Handjob.png", "Art by Fujin Hitokiri");
		}
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Handjob(user);
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.unique){
			return "You opportunistically give her balls some rough treatment.";
		}
		if(modifier==Result.miss){
			return "You reach for "+target.name()+"'s dick but miss.";
		}else if(modifier==Result.critical){
			return String.format("Your dexterous shadow tendrils wrap around %s's shaft. As you stroke, you let your shadows go wild, "
					+ "rubbing and massaging her sensitive member in a way no human appendages could.", target.name());
		}
		else if(modifier==Result.special){
			switch (Global.random(3)) {
			case 0:
				return String
						.format("You take hold of %s's cock and run your fingers over it briskly, hitting all the right spots.",
								target.name());
			case 1:
				return String.format(
						"Your hold on %s's dick tightens, and where once there were gentle touches there are now firm jerks.",
						target.name());
			default:
				return String.format(
						"You have latched on to %s's cock with both hands now, twisting them in a fierce milking movement and eliciting pleasured groans from %s.",
						target.name(),target.pronounTarget(false));
			}
		}
		else{
			return "You grab "+target.name()+"'s girl-cock and stroke it using the techniques you use when masturbating.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.unique){
			return "She roughly womanhandles your balls, sapping some of your strength.";
		}
		if(modifier==Result.miss){
			return self.name()+" grabs for your dick and misses.";
		}else if(modifier==Result.critical){
			return String.format("%s bizarre shadow fingers coil around your dick and wriggle all over your length. A shiver runs through "
					+ "you at the strange, but extremely pleasurable sensation.", self.name());
		}
		else if(modifier==Result.special){
			switch (Global.random(3)) {
			case 0:
				return String
						.format("%s takes hold of your cock and run %s fingers over it briskly, hitting all the right spots.",
								self.name(),self.possessive(false));
			case 1:
				return String.format(
						"%s's hold on your dick tightens, and where once there were gentle touches there are now firm jerks.",
						self.name());
			default:
				return String.format(
						"%s has latched on to your cock with both hands now, twisting them in a fierce milking movement and eliciting pleasured groans from you.",
						self.name());
			}
		}
		int r;
		if(!target.bottom.isEmpty()){
			return self.name()+" slips her hand into your "+target.bottom.peek().getName()+" and strokes your dick.";
		}
		else if(modifier==Result.weak){
			return self.name()+" clumsily fondles your crotch. It's not skillful by any means, but it's also not entirely ineffective.";
		}
		else{
			if(target.getArousal().get()<15){
				return self.name()+" grabs your soft penis and plays with the sensitive organ until it springs into readiness.";
			}
			
			else if	((r = Global.random(3)) == 0){
				return self.name()+" strokes and teases your dick, sending shivers of pleasure up your spine.";
			}
			else if(r==1){
				return self.name()+" rubs the sensitive head of your penis and fondles your balls.";
			}
			else{
				return self.name()+" jerks you off like she's trying to milk every drop of your cum.";
			}
		}
	}
	
	@Override
	public String toString(){
		if(self.getPure(Attribute.Professional)>=3){
			return "Pro Handjob";
		}
		else{
			return "Handjob";
		}
	}

	@Override
	public String describe() {
		if(self.getPure(Attribute.Professional)>=3){
			return "A professional handjob that increases effectiveness with repeated use";
		}
		else{
			return "Rub your opponent's dick";
		}
	}
}
