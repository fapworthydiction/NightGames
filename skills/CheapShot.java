package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Behind;
import status.Primed;

public class CheapShot extends Skill {

	public CheapShot(Character self) {
		super("Cheap Shot", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Temporal)>=2;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal)>=2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&!c.stance.prone(target)&&!c.stance.behind(self)&&!c.stance.penetration(self)&&self.getStatusMagnitude("Primed")>=3;
	}

	@Override
	public String describe() {
		return "Stop time long enough to get in an unsportsmanlike attack from behind: 3 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.add(new Primed(self,-3));
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
			if(Global.random(5)>=3){
				c.write(self,self.bbLiner());
			}
		}
		c.stance=new Behind(self,target);
		target.pain(8+Global.random(16)+self.get(Attribute.Power),Anatomy.genitals,c);
		if(self.has(Trait.wrassler)){
			target.calm(Global.random(6),c);
		}
		else{
			target.calm(Global.random(10),c);
		}
		self.buildMojo(10);

		self.emote(Emotion.confident, 15);
		self.emote(Emotion.dominant, 15);
		target.emote(Emotion.nervous,10);
		target.emote(Emotion.angry,40);

	}

	@Override
	public Skill copy(Character user) {
		return new CheapShot(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(target.nude()){
			if(target.hasBalls()){
				return String.format("You freeze time briefly, giving you a chance to circle around %s. When time resumes, %s looks around in "
						+ "confusion, completely unguarded. You capitalize on your advantage by crouching behind %s and delivering a decisive "
						+ "uppercut to %s dangling balls.",
						target.name(),target.pronounSubject(false),target.pronounTarget(false),target.possessive(false));
			}else{
				return String.format("You freeze time briefly, giving you a chance to circle around %s. When time resumes, %s looks around in "
					+ "confusion, completely unguarded. You capitalize on your advantage by delivering a swift, but "
					+ "painful cunt punt.",target.name(),target.pronounSubject(false));
			}
		}else{
			return String.format("You freeze time briefly, giving you a chance to circle around %s. When time resumes, %s looks around in "
					+ "confusion, completely unguarded. You capitalize on your advantage by crouching behind %s and delivering a decisive "
					+ "uppercut to the groin.",target.name(),target.pronounSubject(false),target.pronounTarget(false));
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(target.nude()){
			if(target.hasBalls()){
				return String.format("%s suddenly vanishes right in front of your eyes. That wasn't just fast, %s completely disappeared. Before "
						+ "you can react, you're hit from behind with a devastating punch to your unprotected balls.",self.name(),self.pronounSubject(false));
			}else{
				return String.format("%s suddenly vanishes right in front of your eyes. That wasn't just fast, %s completely disappeared. Before "
						+ "you can react, you're hit from behind with a devastating punch to your bare vulva.",self.name(),self.pronounSubject(false));

				
			}
		}else{
			return String.format("%s suddenly vanishes right in front of your eyes. That wasn't just fast, %s completely disappeared. You hear something "
					+ "that sounds like 'Za Warudo' before you suffer a painful groin hit from behind.",self.name(),self.pronounSubject(false));
		}
	}

}
