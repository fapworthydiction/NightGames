package skills;

import characters.*;
import characters.Character;
import global.Flag;
import stance.StandingOver;
import global.Global;

import combat.Combat;
import combat.Result;

public class Knee extends Skill {

	public Knee(Character self) {
		super("Knee", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&!c.stance.prone(self)&&self.canAct()&&!c.stance.behind(target)&&!c.stance.penetration(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				if(c.stance.prone(target)){
					c.write(self,receive(0,Result.special,target));
				}
				else{
					c.write(self,receive(0,Result.normal,target));
				}
				if(Global.random(5)>=3){
					c.write(self,self.bbLiner());
				}
				if(!Global.checkFlag(Flag.exactimages)||self.id()== ID.CASSIE){
					c.offerImage("Knee.png", "Art by AimlessArt");
				}
			}
			m = 4+Global.random(11)+self.get(Attribute.Power);
			target.pain(m,Anatomy.genitals,c);
			if(self.has(Trait.wrassler)){
				target.calm(m/2,c);
			}
			else{
				target.calm(m,c);
			}
			self.buildMojo(10);
			target.emote(Emotion.angry,75);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Power)>=10 && !self.has(Trait.cursed);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=10 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new Knee(user);
	}
	public int speed(){
		return 4;
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return target.name()+" blocks your knee strike.";
		}
		if(target.hasBalls()){
			return String.format("You deliver a powerful knee strike to %s's dangling balls. %s lets out a pained whimper and nurses %s injured parts.",
					target.name(),target.pronounSubject(false),target.possessive(false));
		}
		else{
			return "You deliver a powerful knee strike to "+target.name()+"'s delicate lady flower. She lets out a pained whimper and nurses her injured parts.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to knee you in the balls, but you block it.";
		}
		if(modifier==Result.special){
			return self.name()+" raises one leg into the air, then brings her knee down like a hammer onto your balls. You cry out in pain and instinctively try " +
					"to close your legs, but she holds them open.";
		}
		else{
			switch(Global.random(2)){
			case 1:
				return self.name()+" manages to get one of her legs in between yours, and jerks her knee upwards, hitting you squarely in the balls.  "
						+ "You go cross-eyed for a moment as the pain works its way up your stomach and into your throat.";
			}
			return self.name()+" steps in close and brings her knee up between your legs, crushing your fragile balls. You groan and nearly collapse from the " +
					"intense pain in your abdomen.";
		}
	}

	@Override
	public String describe() {
		return "Knee opponent in the groin";
	}
}
