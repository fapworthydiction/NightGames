package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Consumable;
import items.Item;
import status.Stsflag;

public abstract class UseItem extends Skill {
	private Item consumable;

	public UseItem(Item item, Character self) {
		super(item.getFullName(self), self);
		consumable = item;
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&self.has(consumable)&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public String describe() {
		return consumable.getFullDesc(self);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}
}
