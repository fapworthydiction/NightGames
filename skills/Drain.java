package skills;

import status.Abuff;
import global.Global;
import characters.Character;
import characters.Anatomy;
import characters.Trait;
import characters.Attribute;
import combat.Combat;
import combat.Result;

public class Drain extends Skill {

	public Drain(Character self) {
		super("Drain", self);
	}

	@Override
	public boolean requirements() {
		return this.self.getPure(Attribute.Dark)>=21;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>=21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (this.self.canAct()) && (c.stance.dom(this.self))
				&& (c.stance.penetration(this.self))
				&& (this.self.canSpend(20));
	}

	@Override
	public String describe() {
		return "Drain your opponent of their energy";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		int type = Global.random(10) == 0 ? 6 : Global.random(6);
		if (this.self.human()) {
			c.write(self,deal(type, Result.normal, target));
		} else if (target.human()) {
			c.write(self,receive(type, Result.normal, target));
		}

		switch (type) {
		case 0:
			target.weaken(10,c);
			this.self.heal(10,c);
			break;
		case 1:
			target.spendMojo(10);
			this.self.modMojo(10);
			break;
		case 2:
			target.add(new Abuff(target,Attribute.Power, -1,20),c);
			self.add(new Abuff(self,Attribute.Power, 1,20),c);
			target.weaken(10,c);
			break;
		case 3:
			target.add(new Abuff(target,Attribute.Seduction, -1,20),c);
			self.add(new Abuff(self,Attribute.Seduction, 1,20),c);
			target.tempt(10,c);
			break;
		case 4:
			target.add(new Abuff(target,Attribute.Cunning, -1,20),c);
			self.add(new Abuff(self,Attribute.Cunning, 1,20),c);
			target.spendMojo(target.getMojo().get());
			break;
		case 5:
			self.pleasure(self.getArousal().max()
					- self.getArousal().get() - 1,Anatomy.genitals,c);
		case 6:
			target.add(new Abuff(target,Attribute.Power, -1,20),c);
			self.add(new Abuff(self,Attribute.Power, 1,20),c);
			target.add(new Abuff(target,Attribute.Seduction, -1,20),c);
			self.add(new Abuff(self,Attribute.Seduction, 1,20),c);
			target.add(new Abuff(target,Attribute.Cunning, -1,20),c);
			self.add(new Abuff(self,Attribute.Cunning, 1,20),c);
			target.mod(Attribute.Perception, 1);
			target.weaken(target.getStamina().get(),c);
			target.spendMojo(target.getMojo().get());
			target.pleasure(target.getArousal().max()
					- target.getArousal().get() - 1,Anatomy.genitals,c);
			break;
		default:
			break;
		}
		self.getMojo().gain(1);
	}

	@Override
	public Skill copy(Character target) {
		return new Drain(target);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(self.hasPussy()){
			String base = "You put your powerful vaginal muscles to work whilst"
					+ " transfixing " + target.name()
					+ "'s gaze with your own, goading his energy into his cock."
					+ " Soon it erupts from him, ";
			switch (damage) {
			case 2:
				return base + "and you can feel his strength pumping into you.";
			case 3:
				return base + "and you can feel his memories and experiences flow"
						+ " into you, adding to your skill.";
			case 4:
				return base + "taking his fighting spirit with him and"
						+ " adding it to your own";
			case 0:
				return "but unfortunately you made a mistake, and only feel a small"
						+ " bit of his energy traversing the space between you.";
			case 1:
				return "but unfortunately you made a mistake, and only feel a small"
						+ " bit of his energy traversing the space between you.";
			case 6:
				return base
						+ "far more powerfully than you even thought possible."
						+ " You feel a fragment of his soul break away from him and"
						+ " spew into you, taking with it a portion of his strength,"
						+ " skill and wits and merging with your own. You have clearly"
						+ " won this fight, and a lot more than that.";
			default:
				// Should never happen
				return " but nothing happens, you feel strangely impotent.";
			}
		}
		else{
			String base = "With your cock deep inside "+target.name()+", you can feel the heat from her core. You draw the energy from her, mining her depths. ";
			switch (damage) {
			case 2:
				return base + "You feel yourself grow stronger as you steal her physical power.";
			case 3:
				return base + "You manage to steal some of her sexual experience and skill at seduction.";
			case 4:
				return base + "You draw some of her wit and cunning into yourself.";
			case 0:
				return "You attempt to drain "+target.name()+"'s energy through your intimate connection, taking a bit of her energy.";
			case 1:
				return "You attempt to drain "+target.name()+"'s energy through your intimate connection, stealing some of her restraint.";
			case 5:
				return "You attempt to drain "+target.name()+"'s energy through your intimate connection, but it goes wrong. You feel intense pleasure feeding " +
						"back into you and threatening to overwhelm you. You brink the spiritual link as fast as you can, but you're still left on the brink of " +
						"climax.";
			case 6:
				return base
						+ "You succeed in siphoning off a portion of her soul, stealing both her physical and mental strength. This energy will eventually " +
						"return to its owner, but for now, you're very powerful!";
			default:
				// Should never happen
				return " but nothing happens, you feel strangely impotent.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		String base = "You feel the succubus' pussy suddenly tighten around you. "
				+ "She starts kneading your dick bringing you immense pleasure and soon"
				+ " you feel yourself erupt into her, but you realize your are shooting"
				+ " something far more precious than semen into her; as more of the ethereal"
				+ " fluid leaves you, you feel ";
		switch (damage) {
		case 2:
			return base + "your strength leaving you with it, "
					+ "making you more tired than you have ever felt.";
		case 3:
			return base
					+ "memories of previous sexual experiences escape your mind,"
					+ " numbing your skills, rendering you more sensitive and"
					+ " perilously close to the edge of climax.";
		case 4:
			return base + "your mind go numb, causing your confidence and"
					+ "cunning to flow into her.";
		case 0:
			return "Clearly the succubus is trying to do something really special to you, "
					+ "as you can feel the walls of her vagina squirm against you in a way "
					+ "no human could manage, but all you feel is some drowsiness.";
		case 1:
			return "Clearly the succubus is trying to do something really special to you, "
					+ "as you can feel the walls of her vagina squirm against you in a way "
					+ "no human could manage, but all you feel is your focus waning somewhat.";
		case 5:
			return self.name()+" squeezes you with her pussy and starts to milk you, but you suddenly feel her shudder and moan loudly. Looks like her plan backfired.";
		case 6:
			return base
					+ "something snap loose inside of you and it seems to flow right "
					+ "through your dick and into her. When it is over you feel... empty "
					+ "somehow. At the same time, "+self.name()+" seems radiant, looking more powerful,"
					+ " smarter and even more seductive than before. Through all of this,"
					+ " she has kept on thrusting and you are right on the edge of climax."
					+ " Your defeat appears imminent, but you have already lost something"
					+ " far more valuable than a simple sex fight...";
		default:
			// Should never happen
			return " nothing. You should be feeling something, but you're not.";
		}
	}

}
