package skills;

import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Component;
import items.Consumable;
import status.Hypersensitive;
import status.Stsflag;
import status.Tied;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class TortoiseWrap extends Skill {

	public TortoiseWrap(Character self) {
		super("Tortoise Wrap", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Fetish)>=21;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish)>=21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.reachTop(self)&&!c.stance.reachTop(target)&&(self.has(Component.Rope))&&c.stance.dom(self)&&!target.is(Stsflag.tied)&&self.is(Stsflag.bondage)
				&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public String describe() {
		return "User your bondage skills to wrap your opponent to increase her sensitivity";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Component.Rope, 1);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.add(new Tied(target),c);
		target.add(new Hypersensitive(target),c);
	}

	@Override
	public Skill copy(Character user) {
		return new TortoiseWrap(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You skillfully tie a rope around "+target.name()+"'s torso in a traditional bondage wrap. She moans softly as the rope digs into her supple skin.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" ties you up with a complex series of knots. Surprisingly, instead of completely incapacitating you, she wraps you in a way that only " +
				"slightly hinders your movement. However the discomfort of the rope wrapping around you seems to make your sense of touch more pronounced.";
	}

}
