package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Consumable;
import items.Flask;

public class GoodnightKiss extends Skill {

	public GoodnightKiss(Character self) {
		super("Goodnight Kiss", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Ninjutsu)>=18;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.kiss(self)&&self.canAct()&&self.canSpend(20)&&self.has(Flask.Sedative);
	}

	@Override
	public String describe() {
		return "Deliver a powerful knockout drug via a kiss: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		self.consume(Flask.Sedative, 1);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.tempt(Global.random(4),c);
		target.weaken(target.getStamina().get(),c);
		target.getStamina().empty();
	}

	@Override
	public Skill copy(Character user) {
		return new GoodnightKiss(user);
	}
	public int speed(){
		return 7;
	}
	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You surreptitiously coat your lips with a powerful sedative, careful not to accidently ingest any. As soon as you see an opening, "
				+ "you dart in and kiss %s softly. Only a small amount of the drug is actually transferred by the kiss, but it's enough. %s immediately staggers "
				+ "as the strength leaves %s body.",target.name(),target.pronounSubject(true),target.possessive(false));
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s lunges toward you and kisses you energetically. You respond immediately, but before you can deepen the kiss, %s dodges back with a "
				+ "wink. A second later, you realize something is very wrong. A chill spreads through your body as your strength drains away. %s quickly wipes the "
				+ "remaining drug from her lips with a mischievous grin.",self.name(),self.pronounSubject(false),self.name());
	}

}
