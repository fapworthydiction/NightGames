package skills;

import global.Scheduler;
import items.Attachment;
import items.Flask;
import items.Item;
import items.Toy;
import status.Oiled;
import status.Stsflag;
import global.Flag;
import global.Global;
import global.Modifier;
import combat.Combat;
import combat.Result;

import characters.Attribute;
import characters.Character;
import characters.Anatomy;

public class UseDildo extends Skill{

	public UseDildo(Character self) {
		super(Toy.Dildo.getFullName(self), self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (self.has(Toy.Dildo)||self.has(Toy.Dildo2))&&self.canAct()&&target.hasPussy()&&c.stance.reachBottom(self)&&target.pantsless()&&!c.stance.penetration(self)
				&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		int block = 0;
		if(c.lastact(target) != null && c.lastact(target).name == Toy.Onahole.getFullName(target)){
			block = 5;
		}
		if(target.roll(this, c, accuracy()+self.tohit() - block)){
			int level = 1;
			m = 6+(target.get(Attribute.Perception)/2)+(self.get(Attribute.Science)/2);
			if(self.has(Attachment.DildoSlimy)){
				m *= 2;
				level += 1;
			}
			if(self.has(Toy.Dildo2)){
				
				m += 5;
				level += 1;
			}
			if(self.has(Attachment.DildoLube) && self.has(Flask.Lubricant) && !target.is(Stsflag.oiled)){
				if(self.human()){
					c.write(self,deal(1,Result.upgrade,target));
				}
				else if(target.human()){
					c.write(self,receive(1,Result.upgrade,target));
				}
				self.consume(Flask.Lubricant,1);
				target.add(new Oiled(target));
			}
			if(self.human()){
				c.write(self,deal(level,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(level,Result.normal,target));
			}
			if(self.has(Attachment.DildoSlimy)){
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Kat")){
					c.offerImage("Dildo4.png", "Art by AimlessArt");
				}
			}else if(self.has(Attachment.DildoLube)){
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Kat")){
					c.offerImage("Dildo3.png", "Art by AimlessArt");
				}
			}else if(self.has(Toy.Dildo2)){
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Kat")){
					c.offerImage("Dildo2.png", "Art by AimlessArt");
				}
			}else{
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Kat")){
					c.offerImage("Dildo1.png", "Art by AimlessArt");
				}
			}
			m = self.bonusProficiency(Anatomy.toy, m);
			if(self.has(Toy.Dildo2)){
				target.pleasure(m,Anatomy.genitals,c);
			}else{
				target.pleasure(m,Anatomy.genitals,Result.finisher,c);
			}
		}
		else{
			if(self.human()){
				c.write(self,deal(block,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(block,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseDildo(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			if(damage > 0){
				return "You try to slip a dildo into "+target.name()+", but she catches it with her "+Toy.Onahole.getFullName(target)+".";
			}else{
				return "You try to slip a dildo into "+target.name()+", but she blocks it.";
			}
			
		}
		else if(modifier == Result.upgrade){
			return "As you rub the head of the dildo against "+target.name()+"'s pussy, it shoots out a generous load of lubricant to "
					+ "coat her groin.";
		}
		else{
			switch(damage){
			case 3:
				return "As the slimy dildo touches "+target.name()+"'s nethers, it starts to react eagerly. It delves into her vagina and writhes around, exploring "
						+ "her intimate depths. She trembles at the intense sensation and moans loudly.";
			case 2: 
				switch(Global.random(2)){
				case 1:
					return "You push the dildo into " + target.name() + ". While she's reeling, you crank up the sonic vibration, and she nearly falls to her knees from the sensation.";
				default:
					return "You touch the imperceptibly vibrating dildo to "+target.name()+"'s love button and she jumps as if shocked. Before she can defend herself, you " +
					"slip it into her pussy. She starts moaning in pleasure immediately.";
				}
				
			default:
				switch(Global.random(2)){
				case 1:
					return "You quickly slip the tip of your dildo into " + target.name() + "'s pussy. Before she can react, you push it in further, eliciting a pleasured moan from her lips.";
				default:
					return "You rub the dildo against "+target.name()+"'s lower lips to lubricate it before you thrust it inside her. She can't help moaning a little as you " +
					"pump the rubber toy in and out of her pussy.";
				}
			}
			
		}
				
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String describe() {
		return "Pleasure opponent with your dildo";
	}

}
