package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Attachment;
import items.Toy;
import status.Sensitive;
import status.Sore;
import status.Stsflag;

public class UseClamps extends Skill {

	public UseClamps(Character self) {
		super("Nipple Clamps", self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.reachTop(self)&&target.topless()&&
				self.has(Toy.nippleclamp)&&
				(!self.human()|| Scheduler.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m;
		if(target.roll(this, c, accuracy()+self.tohit())){
            m = 4+target.get(Attribute.Perception)+self.get(Attribute.Science)/2;
            m = self.bonusProficiency(Anatomy.toy,m);
            if(self.human()){
                c.write(self,deal(0,Result.normal,target));
            }
            else if(target.human()){
                c.write(self,receive(0,Result.normal,target));
            }
            target.add(new Sore(target, 4,Anatomy.chest,2f),c);
            target.add(new Sensitive(target,4,Anatomy.chest,2f),c);
	}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseClamps(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return"You try to attach your nipple clamps to "+target.name()+" but she blocks them";
		}
		else{
			return "Apply your golden clamps to "+target.name()+"'s nipples, leaving them swollen and extra sensitive.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
        if(modifier==Result.miss){
            return self.name()+ " tries to put some gold clamps on your nipples, but you push her away.";
        }
        else{
            return self.name() + " sticks a pair of ornate nipple clamps onto your chest and yanks them off. You " +
                    "grit your teeth at the sharp pain that leaves your nipples feeling sore and sensitive.";
        }
	}

	@Override
	public String describe() {
		return "Use your clamps to sensitize your opponents' nipples";
	}

}
