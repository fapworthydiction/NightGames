package skills;

import items.Consumable;
import status.Enthralled;
import status.Stsflag;
import characters.Character;

import combat.Combat;
import combat.Result;
import global.Global;
import global.Modifier;

public class DarkTalisman extends UseItem {

	public DarkTalisman(Character self) {
		super(Consumable.Talisman, self);
	}
	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Consumable.Talisman, 1);
		if(self.human()){
			c.write(deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(receive(0,Result.normal,target));
		}
		target.add(new Enthralled(target,self),c);
	}

	@Override
	public Skill copy(Character user) {
		return new DarkTalisman(user);
	}
	
	@Override
	public String deal(int damage, Result modifier, Character target) {
		switch(Global.random(2)){
		case 1:
			return "You hold the talisman in front of "+target.name()+"'s face and watch her eyes lose focus as she falls under your control.";
		default:
			return "You brandish the dark talisman, which seems to glow with power. The trinket crumbles to dust, but you see the image remain in the reflection of "+target.name()+"'s eyes.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		switch(Global.random(3)){
		case 2:
			return "You find your eyes locked on the talisman in "+self.name()+"'s hand, unsure of when exactly it got there.";
		case 1:
			return self.name()+" pulls out an evil looking talisman. Your eyes are drawn to it against your will.";
		default:
			return self.name()+" holds up a strange talisman. You feel compelled to look at the thing, captivated by its unholy nature.";			
		}
	}

}
