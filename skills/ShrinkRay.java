package skills;

import items.Component;
import items.Item;
import status.Shamed;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;

public class ShrinkRay extends Skill {

	public ShrinkRay(Character self) {
		super("Shrink Ray", self);
	}

	@Override
	public boolean requirements() {	
		return self.getPure(Attribute.Science)>=12;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science)>=12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&target.nude()&&self.has(Component.Battery, 2);
	}

	@Override
	public String describe() {
		return "Briefly shrink your opponent's 'assets' to damage her ego: 2 Batteries";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Component.Battery, 2);
		if(self.human()){
			if(target.hasDick()){
				c.write(self,deal(0,Result.special,target));
			}
			else{
				c.write(self,deal(0,Result.normal,target));
			}
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.add(new Shamed(target),c);
		target.spendMojo(15);
		target.emote(Emotion.nervous, 20);
		target.emote(Emotion.desperate, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new ShrinkRay(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return "You aim your shrink ray at "+target.name()+"'s cock, shrinking her male anatomy. She turns red and glares at you in humiliation.";
		}
		else{
			return "You use your shrink ray to turn "+target.name()+"'s breasts into A cups. She whimpers and covers her chest in shame.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" points a device at your groin and giggles as your genitals shrink. You flush in shame and cover yourself. The effect wears off quickly, " +
				"but the damage to your dignity lasts much longer.";
	}

}
