package skills;

import stance.*;
import status.Bound;
import status.Stsflag;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

public class Struggle extends Skill {

	public Struggle(Character self) {
		super("Struggle", self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return ((!c.stance.mobile(self)&&!c.stance.dom(self))||self.bound())&&!self.stunned()&&!self.distracted()&&!self.is(Stsflag.enthralled);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.bound()){
			Bound status=(Bound)target.getStatus(Stsflag.bound);
			if(self.check(Attribute.Power, 10-self.escape())){
				if(self.human()){
					if(status!=null){
						c.write(self,"You manage to break free from the "+status+".");
					}
					else{
						c.write(self,"You manage to snap the restraints that are binding your hands.");
					}
				}
				else if(target.human()){
					if(status!=null){
						c.write(self,self.name()+" slips free from the "+status+".");
					}
					else{
						c.write(self,self.name()+" breaks free.");
					}
				}
				self.free();
			}
			else{
				if(self.human()){
					if(status!=null){
						c.write(self,"You struggle against the "+status+", but can't get free.");
					}
					else{
						c.write(self,"You struggle against your restraints, but can't get free.");
					}
				}
				else if(target.human()){
					if(status!=null){
						c.write(self,self.name()+" struggles against the "+status+", but can't free her hands.");
					}
					else{
						c.write(self,self.name()+" struggles, but can't free her hands.");
					}
				}
			}
		}
		else if(c.stance.penetration(self)){
			if(c.stance.enumerate()==Stance.anal){
				if(self.check(Attribute.Power, c.stance.escapeDC(target, self)+(target.getStamina().get()/2-self.getStamina().get()/2)+(target.get(Attribute.Power)-self.get(Attribute.Power)))){
					if(self.human()){
						c.write(self,"You manage to break away from "+target.name()+".");
					}
					else if(target.human()){
						c.write(self,self.name()+" pulls away from you and your dick slides out of her butt.");
					}
					c.stance.insert(target);
				}
				else{
					if(self.human()){
						c.write(self,"You try to pull free, but "+target.name()+" has a good grip on your waist.");
					}
					else if(target.human()){
						c.write(self,self.name()+" tries to squirm away, but you have better leverage.");
					}	
				}
			}
			else{
				if(self.check(Attribute.Power, c.stance.escapeDC(target, self)+(target.getStamina().get()/2-self.getStamina().get()/2)+(target.get(Attribute.Power)-self.get(Attribute.Power)))){
					if(self.human()){

						if (c.stance.en==Stance.flying) {
							if(self.getPure(Attribute.Dark)>=18){
								c.stance = new Flying(self, target);
								c.write(self,"You struggle with "+target.name()+" until she is unable to keep the two of you in the air. "
										+ "You start to plummet rapidly to the ground. Fortunately you can fly too. You summon your own wings "
										+ "and snatch "+target.name()+" out of the air, thrusting your cock back into her. Now you're in control.");
							}else if(self.getPure(Attribute.Ninjutsu)>=5){
								c.stance = new Neutral(self, target);
								c.write(self,"You pry yourself free from your captor and drop to the ground. "
										+ "The drop would be a problem for someone without ninja training, "
										+ "but you mange to roll safely and hop back to your feet.");
							}
							else{
								c.write(self,"You manage to shake yourself loose from the demoness.\n"
										+ "Immediatly afterwards you realize letting go of the person"
										+ " holding you a good distance up from the ground may not have been"
										+ " the smartest move you've ever made, as the ground is quickly"
										+ " approaching your face.");
								c.stance = c.stance.insert(self);
							}
						}else if(c.stance.en==Stance.standing){
						    c.write(self,"You grab "+target.name()+"'s ass and hold her up, taking control of the pace.");
						    c.stance = new Standing(self,target);
                        }
						else if(c.stance.behind(self)){
							c.write(self,"You manage unbalance "+target.name()+" and push her forward onto her hands and knees. You follow her, still inside her tight wetness, and continue " +
									"to fuck her from behind.");
							if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Kat")){
								c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
							}
							c.stance=new Doggy(self,target);
						}

						else{
							c.write(self,"You surpise "+target.name()+" by hugging her close to your chest, preventing her from using stabilizing her position with her arms. You " +
									"roll on top of her into traditional missionary position, careful not to let your cock slip out of her.");
							if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Angel")){
								c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
							}
							c.stance=new Missionary(self,target);
						}
					}
					else if(target.human()){
					    if(c.stance.en==Stance.standing){
                            c.write(self,self.name()+" wraps her legs around your waist and manages to take control of your fucking.");
                            c.stance = new Standing(self,target);
                        }
                        else if(c.stance.prone(self)){
							c.write(self,self.name()+" wraps her legs around your waist and suddenly pulls you into a deep kiss. You're so surprised by this sneak attack that you " +
									"don't even notice her roll you onto your back until you feel her weight on your hips. She moves her hips experimentally, enjoying the control " +
									"she has in cowgirl position.");
							if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Cassie")){
								c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
							}
							c.stance=new Cowgirl(self,target);
						}else if (c.stance.en==Stance.flying) {
							if(self.getPure(Attribute.Dark)>=18){
								c.stance = new Flying(self, target);
								c.write(self,self.name()+" spreads her own wings and pulls you into a nose-dive with a strong flap. While you're busy "
										+ "frantically trying to recover, she secures her grip on you. She catches you before you hit the ground and pulls "
										+ "you back into the air.");
							}else if(self.getPure(Attribute.Ninjutsu)>=5){
								c.stance = new Neutral(self, target);
								c.write(self,self.name()+" shakes herself out of your arms and off your dick. She drops to the ground, but lands "
										+ "gracefully, thanks to her extensive training.");
							}
							else{
								c.write(self,self.name()+" shakes herself out of your arms and off your dick. She drops to the ground, landing "
										+ "hard. You descend rapidly and confirm she's not seriously hurt. Fortunately she seems fine, and also "
										+ "fortunately, you have the drop on her.");
								c.stance = c.stance.insert(self);
							}
						}
						else{
							c.write(self,self.name()+" manages to reach between her legs and grab hold of your ballsack, stopping you in mid thrust. She smirks at you over her shoulder " +
									"and pushes her butt against you, using the leverage of " +
									"your testicles to keep you from backing away to maintain you balance. She forces you onto your back, while never breaking your connection. after " +
									"some complex maneuvering, you end up on the floor while she straddles your hips in a reverse cowgirl position.");
							c.stance=new ReverseCowgirl(self,target);
						}
					}
					else{
						if(self.hasDick()||self.has(Trait.strapped)){
							if(c.stance.behind(self)){
								c.stance=new Doggy(self,target);
							}
							else if (c.stance.en==Stance.flying) {
								c.stance = c.stance.insert(self);
							}
							else{
								if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Angel")){
									c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
								}
								c.stance=new Missionary(self,target);
							}
						}else{
							if(c.stance.prone(self)){
								if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Cassie")){
									c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
								}
								c.stance=new Cowgirl(self,target);
							}
							else{
								c.stance=new ReverseCowgirl(self,target);
							}
						}
					}
				}
				else{
					if(self.human()){
						c.write(self,"You try to tip "+target.name()+" off balance, but she drops her hips firmly, pushing your cock deep inside her and pinning you to the floor.");
					}
					else if(target.human()){
						if(c.stance.behind(target)){
							c.write(self,self.name()+" struggles to gain a more dominant position, but with you behind her, holding her waist firmly, there is nothing she can do.");
						}
						else{
							c.write(self,self.name()+" tries to roll on top of you, but you use you superior upper body strength to maintain your position.");
						}
					}
					c.stance.decay();
				}
			}
		}
		else{
			if(self.check(Attribute.Power, c.stance.escapeDC(target, self)+(target.getStamina().get()/2-self.getStamina().get()/2)+(target.get(Attribute.Power)-self.get(Attribute.Power)))){
				if(self.human()){
					c.write(self,"You manage to scramble out of "+target.name()+"'s grip.");
				}
				else if(target.human()){
					c.write(self,self.name()+" squirms out from under you.");
				}
				if(c.stance.prone(self)){
					c.stance=new StandingOver(target,self);
				}
				c.stance=new Neutral(self,target);
			}
			else{
				if(self.human()){
					c.write(self,"You try to free yourself from "+target.name()+"'s grasp, but she has you pinned too well.");
				}
				else if(target.human()){
					c.write(self,self.name()+" struggles against you, but you maintain your position.");
				}
				c.stance.decay();
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Struggle(user);
	}
	public int speed(){
		return 0;
	}
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String describe() {
		return "Attempt to escape a submissive position using Power";
	}
}
