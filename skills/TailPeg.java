package skills;

import global.Global;
import stance.Stance;
import status.Shamed;
import combat.Combat;
import combat.Result;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

public class TailPeg extends Skill {

	public TailPeg(Character self) {
		super("Tail Peg", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Dark)>1;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.getArousal().get() >= 30 && self.canAct()
				&& self.canSpend(20) && target.pantsless()
				&& c.stance.en != Stance.standing
				&& c.stance.en != Stance.standingover
				&& !(c.stance.en == Stance.anal && c.stance.dom(self))
				&& self.has(Trait.tailed);
	}

	@Override
	public String describe() {
		return "Shove your tail up your opponent's ass.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (target.roll(this, c,
				accuracy() + this.self.tohit()
						- (c.stance.penetration(self) ? 0 : 5))) {
			self.spendMojo(20);
			boolean shamed = false;
			if (Global.random(4) == 2) {
				target.add(new Shamed(target),c);
				shamed = true;
			}
			if (target.human()) {
				if (c.stance.penetration(self))
					c.write(self,receive(0, Result.special, target));
				else if (c.stance.dom(target))
					c.write(self,receive(0, Result.critical, target));
				else if (c.stance.behind(self))
					c.write(self,receive(0, Result.strong, target));
				else
					c.write(self,receive(0, Result.normal, target));
				if (shamed)
					c.write(self,"The shame of having your ass violated by "
							+ self.name() + " has destroyed your confidence.");
			} else if (self.human()) {
				if (c.stance.penetration(self))
					c.write(self,deal(0, Result.special, target));
				else if (c.stance.dom(target))
					c.write(self,deal(0, Result.critical, target));
				else if (c.stance.behind(self))
					c.write(self,deal(0, Result.strong, target));
				else
					c.write(self,deal(0, Result.normal, target));
				if (shamed)
					c.write(self,"Her face flushes in shame at the foreign object invading her "
							+ "ass.");
			}
			target.pleasure(10 + Global.random(10),Anatomy.ass,c);
			target.pain(3 + Global.random(5),Anatomy.ass,c);
			target.emote(Emotion.nervous, 10);
			target.emote(Emotion.desperate, 10);
			self.emote(Emotion.confident, 15);
			self.emote(Emotion.dominant, 25);
		} else {
			if (target.human())
				c.write(self,receive(0, Result.miss, target));
			else
				c.write(self,deal(0, Result.miss, target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new TailPeg(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		switch (modifier) {
		case critical:
			return String.format("With your superior position, %s is helpless to resist as the tip of your tail teases her anus. "
					+ "You decide to go even further and thrust it several inches into the tight hole.", target.name());
		case miss:
			return String.format("You try to sneak your tail into %s's back door, but she catches it and pushes it away.",target.name());
		case normal:
			return String.format("You close in on %s, and your tail curls around outside her peripheral vision. Before she realizes "
					+ "what's happening, you thrust it right up her bum.", target.name());
		case special:
			return String.format("As your cock fills %s pussy, your prehensile tail snakes between her butt cheeks and thrusts into "
					+ "her ass.", target.name());
		case strong:
			return String.format("Holding %s from behind, you have great access to her bare ass. You thrust your tail inside, making her let out a startled "
					+ "yelp.", target.name());
		default:
			return "<<This should not be displayed, please inform The Silver Bard: TailPeg-deal>>";
		}
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		switch (modifier) {
		case critical:
			return "Smiling down on you, "
					+ self.name()
					+ " spreads your legs and tickles your butt with her tail."
					+ " You notice how the tail itself was slick and wet as it"
					+ " slowly pushes through your anus, spreading your cheeks apart. "
					+ self.name()
					+ " pumps it in and out a for a few times before taking "
					+ "it out again.";
		case miss:
			return self.name()
					+ " tries to peg you with her tail but you manage to push"
					+ " your butt cheeks together in time to keep it out.";
		case normal:
			return self.name()
					+ " suddenly moves very close to you. You expect an attack from the front"
					+ " and try to move back, but end up shoving her tail right up your ass.";
		case special:
			return self.name()
					+ " smirks and wiggles her tail behind her back. You briefly look "
					+ "at it and the see the appendage move behind you. You try to keep it"
					+ " out by clenching your butt together, but a squeeze of "
					+ self.name()
					+ "'s vagina breaks your concentration, so the tail slides up your ass"
					+ " and you almost lose it as your cock and ass are stimulated so thoroughly"
					+ " at the same time.";
		case strong:
			return self.name()
					+ " hugs you from behind and rubs her chest against your back."
					+ " Distracted by that, she managed to push her tail between your"
					+ " ass cheeks and started tickling your prostrate with the tip.";
		default:
			return "<<This should not be displayed, please inform The Silver Bard: TailPeg-receive>>";
		}
	}

}
