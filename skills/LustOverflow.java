package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.ProfMod;

public class LustOverflow extends Skill {

	public LustOverflow(Character self) {
		super("Lust Overflow", self);

	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Dark)>=30;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>=30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.dom(self)&&c.stance.penetration(self)&&self.canSpend(20);
	}

	@Override
	public String describe() {
		return "Dramatically increases power of sex moves, but increases arousal each turn: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.spendMojo(20);
		self.add(new Horny(self,10,10),c);
		self.add(new ProfMod("Sex Mastery",self,Anatomy.genitals,200),c);
		target.emote(Emotion.horny, 50);
		self.emote(Emotion.horny, 50);
		target.emote(Emotion.desperate, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new LustOverflow(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You channel the power of your lust into your cock, letting the infernal energy guide your hips.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(self.hasDick()){
			return String.format("%s spreads %s wings as %s dark aura turns inward. %s cock seems to expand, as its power starts to overwhelm you.", 
					self.name(),self.possessive(false),self.possessive(false),self.possessive(true));
		}else{
			return String.format("%s spreads %s wings as %s dark aura turns inward. "
					+ "You suddenly feel the sensation of %s pussy on your dick change as it actively starts to milk you.",
					self.name(),self.possessive(false),self.possessive(false),self.possessive(false));
		}
		
	}

}
