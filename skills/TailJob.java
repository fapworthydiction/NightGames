package skills;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;



public class TailJob extends Skill {

	public TailJob(Character self) {
		super("Tailjob", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Animism)>=6;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Animism)>=6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&target.pantsless()&&c.stance.mobile(self)&&!c.stance.mobile(target)&&!c.stance.penetration(target)&&self.has(Trait.tailed);
	}

	@Override
	public String describe() {
		return "Use your tail to tease your opponent";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.pleasure(self.get(Attribute.Animism)*self.getArousal().percent()/100+Global.random(target.get(Attribute.Perception)),Anatomy.genitals,c);
	}

	@Override
	public Skill copy(Character user) {
		return new TailJob(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You skillfully use your flexible tail to stroke and tease "+target.name()+"'s sensitive girl parts.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" teases your sensitive dick and balls with her soft tail. It wraps completely around your shaft and strokes firmly.";
	}

}
