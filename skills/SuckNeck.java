package skills;

import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;

import combat.Combat;
import combat.Result;

public class SuckNeck extends Skill {

	public SuckNeck(Character self) {
		super("Suck Neck", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.kiss(self)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			int m = 0;
			if(self.getPure(Attribute.Dark)>=1){
				if(self.human()){
					c.write(self,deal(0,Result.special,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.special,target));
				}
				m = self.get(Attribute.Dark);
				target.weaken(m,c);
				self.heal(m,c);
				self.spendArousal(m);
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.normal,target));
				}
			}
			target.pleasure(3+m+Global.random(self.get(Attribute.Seduction)/2),Anatomy.neck,c);
			self.buildMojo(5);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=12;
	}

	@Override
	public boolean requirements(Character user) {
		return self.getPure(Attribute.Seduction)>=12;
	}

	@Override
	public Skill copy(Character user) {
		return new SuckNeck(user);
	}
	public int speed(){
		return 5;
	}
	public int accuracy(){
		return 7;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}
	public String toString(){
		if(self.getPure(Attribute.Dark)>=1){
			return "Drain energy";
		}
		else{
			return name;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You lean in to kiss "+target.name()+"'s neck, but she slips away.";
		}
		else if(modifier == Result.special){
			return "You draw close to "+target.name()+" as she's momentarily too captivated to resist. You run your tongue along her neck and bite gently. She shivers and you " +
					"can feel the energy of her pleasure flow into you, giving you strength.";
		}
		else{
			switch(Global.random(2)){
			case 1:
				return "You lick, kiss, suck, and nibble all over " + target.name() + "'s neck, causing her to moan under your ministrations.";
			default:
				return "You lick and suck "+target.name()+"'s neck hard enough to leave a hickey.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" goes after your neck, but you push her back.";
		}
		else if(modifier == Result.special){
			return self.name()+" presses her lips against your neck. She gives you a hickey and your knees start to go weak. It's like your strength is being sucked out through " +
					"your skin.";
		}
		else{
			return self.name()+" licks and sucks your neck, biting lightly when you aren't expecting it.";
		}
	}

	@Override
	public String describe() {
		return "Suck on opponent's neck. Highly variable effectiveness";
	}
}
