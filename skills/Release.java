package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Behind;
import stance.Neutral;

public class Release extends Skill {

	public Release(Character self) {
		super("Release", self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct() && c.stance.dom(self);
	}

	@Override
	public String describe() {
		return "Release your opponent and return to neutral standing position.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		c.stance = new Neutral(target,self);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else{
			c.write(self,receive(0,Result.normal,target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Release(user);
	}

	@Override
	public Tactics type() {
		return Tactics.negative;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You back off and let "+target.name()+" recover.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" takes a step back and lets you recover.";
	}

}
