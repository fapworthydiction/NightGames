package skills;

import global.Global;

import items.Component;
import items.Consumable;
import items.Flask;
import items.Item;
import items.Potion;

import java.util.Arrays;
import java.util.List;

import characters.Character;
import combat.Combat;
import combat.Result;

public class CommandGive extends PlayerCommand {

	public static final List<? extends Item> TRANSFERABLES = Arrays.asList(Potion.EnergyDrink, Flask.SPotion,
			Flask.Aphrodisiac, Flask.Sedative, Component.Battery, Potion.Beer, Flask.Lubricant, Component.Rope,
			Consumable.ZipTie, Component.Tripwire, Component.Spring);
	private Item transfer;
	
	public CommandGive(Character self) {
		super("Take Item", self);
		transfer = null;
	}
	
	public boolean usable(Combat c, Character target) {
		if (!super.usable(c, target))
			return false;
		for (Item transferable : TRANSFERABLES)
			if (target.has(transferable))
				return true;
		return false;
	}

	@Override
	public String describe() {
		return "Make your opponent give you an item.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		do {
			transfer = TRANSFERABLES.get(Global.random(TRANSFERABLES.size()));
			if (!(target.has(transfer) && TRANSFERABLES.contains(transfer)))
				transfer = null;
		} while (transfer == null);
		target.consume(transfer, 1);
		self.gain(transfer);
		c.write(self,deal(0, Result.normal, target));
		transfer = null;
	}

	@Override
	public Skill copy(Character user) {
		return new CommandGive(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return target.name() + " takes out " + transfer.pre() + transfer.getName()
				+ " and hands it to you.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The"
				+ " Silver Bard: CommandGive-receive>>";
	}

}
