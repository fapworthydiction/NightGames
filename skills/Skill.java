package skills;
import java.awt.image.BufferedImage;

import combat.Combat;
import combat.Result;

import characters.Character;

// change start -GD
// new
public abstract class Skill implements Comparable<Skill>{
// old
//public abstract class Skill{
	/**
	 * 
	 */
	// new
	public String name;
	// old
	//protected String name;
	
// change end -GD
	protected Character self;
	protected String image;
	protected String artist;
	protected Integer sortOrder = null;
	public Skill(String name, Character self){
		this.name=name;
		this.self=self;
		this.image=null;
		this.artist=null;
	}
	public abstract boolean requirements();
	public abstract boolean requirements(Character user);
	public abstract boolean usable(Combat c, Character target);
	public abstract String describe();
	public abstract void resolve(Combat c, Character target);
	public abstract Skill copy(Character user);
	public abstract Tactics type();
	public abstract String deal(int damage,Result modifier,Character target);
	public abstract String receive(int damage, Result modifier,Character target);
	public int accuracy(){
		return 5;
	}
	public int speed(){
		return 5;
	}
	public String toString(){
		return name;
	}
	public Character user(){
		return self;
	}
	public void setSelf(Character self){
		this.self=self;
	}
	public void setName(String name){
		this.name=name;
	}
	// change add -GD
	// new
	public Integer getSortOrder(){
		if (sortOrder != null)
			return sortOrder;
		return type().ordinal();
	}
	// change end -GD
	@Override
	public boolean equals(Object other){
		return this.getClass()==other.getClass();
	}
	// change add -GD
	// new
	@Override
	public int compareTo(Skill other){
		int ret = getSortOrder() - other.getSortOrder();
		if (ret != 0)
			return ret;
		
		return name.compareTo(other.name);
	}
	// change end -GD
}
