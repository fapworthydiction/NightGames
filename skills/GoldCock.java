package skills;

import status.GoldenCock;
import status.Stsflag;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;

public class GoldCock extends Skill {

	public GoldCock(Character self) {
		super("Golden Cock", self);
	}

	@Override
	public boolean requirements() {
		return self.has(Trait.goldcock);
	}

	@Override
	public boolean requirements(Character user) {
		return self.has(Trait.goldcock);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&self.canSpend(30)&&!self.is(Stsflag.goldencock)&&self.pantsless();
	}

	@Override
	public String describe() {
		return "Power up your cock: 30 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.add(new GoldenCock(self),c);
	}

	@Override
	public Skill copy(Character user) {
		return new GoldCock(user);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You focus for a moment to release the extraordinary power of your cock. Your member emits a bright golden glow.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" ";
	}

}
