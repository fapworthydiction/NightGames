package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Hypersensitive;
import status.Shamed;
import status.Status;
import status.Stsflag;

public class Humiliate extends Skill {

	public Humiliate(Character self) {
		super("Humiliate", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Hypnosis)>=6;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis)>=6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.behind(self)&&
				!c.stance.behind(target)&&!c.stance.sub(self)&&(target.is(Stsflag.charmed)||target.is(Stsflag.enthralled));
	}

	@Override
	public String describe() {
		return "Use hypnotic suggestion to fill your opponent with shame.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else{
			c.write(self,receive(0,Result.normal,target));
		}
		if(target.is(Stsflag.shamed)){
			Status sts = target.getStatus(Stsflag.shamed);
			if(sts!=null){
				target.add(new Shamed(target,target.getStatusMagnitude("Shamed")),c);
			}
			else{
				target.add(new Shamed(target),c);
			}
		}else{
			target.add(new Shamed(target),c);
		}
		target.emote(Emotion.nervous, 30);
		target.emote(Emotion.desperate, 20);


	}

	@Override
	public Skill copy(Character user) {
		return new Humiliate(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You manipulate all of %s's remaining inhibitions, making %s believe %s "
				+ "is in the most shameful situation %s can imagine.", 
				target.name(), target.pronounTarget(false),target.pronounSubject(false),target.pronounSubject(false));
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s points out that you are exposing quite a lot to the audience. Somehow you failed to notice "
				+ "that you are naked and visibly aroused while the entire student body is watching. The Games are usually a "
				+ "more private matter, you didn't even realize you were the center of so much attention before %s said something.", 
				self.name(),self.pronounSubject(false));
	}

}
