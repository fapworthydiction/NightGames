package skills;

import stance.Stance;
import stance.StandingOver;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class Dive extends Skill {

	public Dive(Character self) {
		super("Dive", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Submissive)>=1;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive)>=1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.en==Stance.neutral;
	}

	@Override
	public String describe() {
		return "Hit the deck! Avoids some attacks.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		c.stance = new StandingOver(target,self);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else{
			c.write(self,receive(0,Result.normal,target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Dive(user);
	}

	@Override
	public Tactics type() {
		return Tactics.negative;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You take evasive action and dive to the floor. Ok, you're on the floor now. That's as far as you planned.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" dives dramatically away and lands flat on the floor.";
	}

}
