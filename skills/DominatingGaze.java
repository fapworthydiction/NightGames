package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Masochistic;
import status.Stsflag;

public class DominatingGaze extends Skill {

	public DominatingGaze(Character self) {
		super("Dominating Gaze", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Discipline)>=12;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline)>=12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&self.is(Stsflag.composed)&&
				(target.getMood()== Emotion.nervous||target.getMood()==Emotion.desperate)&&
				self.canSpend(10)&&!target.is(Stsflag.masochism);
	}

	@Override
	public String describe() {
		return "Use your dominant demeanor to put your opponent in a masochistic mood";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.spendMojo(10);
		target.add(new Masochistic(target),c);
	}

	@Override
	public Skill copy(Character user) {
		return new DominatingGaze(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "As "+target.name()+" looks at you with a hint of nervousness in her eyes, you meet her gaze. You are going to hurt her. You know that. She knows that. So wouldn’t it be a lot better if she simply allowed herself to enjoy it, as you make her hurt in just the right way? "+target.name()+" shudders slightly, and you press your advantage. You let her know that all she has to do is let you take control, and just worry about enjoying herself.<p>" +
				""+target.name()+" seems to shrink into herself as your presence overwhelms her. You’ve got her.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "You make the mistake of looking directly at "+self.name()+"’s face, letting your nervousness show through. "+self.name()+" isn’t one to let this go, unfortunately. <i>\"You really needn’t be so concerned,\"</i> she says, though her smile says otherwise. <i>\"Please don’t get me wrong, I do plan to let you feel the delicious taste of my crop many more times, but would that truly be so bad?\"</i> A chill runs up your spine as she says this, and it only gets worse as she leans toward you. <i>\"Why don’t you let me show you just how enjoyable it can be to let your Mistress punish you? Leave everything to me.\"</i><p>" +
				"You feel like you’re shrinking into yourself right now, but you can’t help but admit that the thought of letting "+self.name()+" take control and hurt you as she wishes sounds rather enticing right now.\n";
	}

}
