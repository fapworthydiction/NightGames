package skills;

import status.Charmed;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class KissEX extends Skill {

	public KissEX(Character self) {
		super("Passionate Kiss", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.kiss(self)&&self.canAct()&&self.canSpend(15);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m=0;
		Result style = Result.seductive;
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			style = Result.powerful;
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			style = Result.clever;
		}
		self.spendMojo(15);
		if(style == Result.powerful){
			m=Global.random(4)+self.get(Attribute.Power)/4;
		}else if(style == Result.clever){
			m=Global.random(4)+self.get(Attribute.Cunning)/4;
		}else{
			m=Global.random(4)+self.get(Attribute.Seduction)/4;
			m*=1.3f;
		}
		
		if(self.has(Trait.romantic)){
			m*=1.2f;
		}
		if(self.human()){
			c.write(self,deal(m,style,target));
			if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Jewel")){
				c.offerImage("Kiss.jpg", "Art by AimlessArt");
			}
			if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Cassie")){
				c.offerImage("Cassie Kiss.jpg", "Art by AimlessArt");
			}
		}
		else if(target.human()){
			c.write(self,receive(m,style,target));
			if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Jewel")){
				c.offerImage("Kissed.jpg", "Art by AimlessArt");
			}
		}
		target.tempt(m,Result.foreplay,c);
		target.pleasure(1,Anatomy.mouth,c);
		if(self.has(Trait.greatkiss) && self.canSpend(10) && Global.random(2)==0){
			target.add(new Charmed(target),c);
			self.spendMojo(10);
		}
	}

	@Override
	public boolean requirements() {
		return self.get(Attribute.Seduction)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Seduction)>=9;
	}

	@Override
	public Skill copy(Character user) {
		return new KissEX(user);
	}
	public int speed(){
		return 6;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.powerful){
			return String.format("You aggressively pull %s into an intense kiss. You push your tongue into her mouth before she has a "
					+ "chance to react. By her response, she clearly doesn't mind a little assertiveness.",target.name());
		}
		else if(modifier==Result.clever){
			return String.format("You steal a quick kiss from %s, pulling back before she can respond. As she hesitates in confusion, you kiss her twice more, "+
					"lingering on the last to run your tongue over her lips.",target.name());
		}
		else{
			return String.format("You pull %s close for a passionate kiss. She responds eagerly, parting her lips and bringing out her tongue to tangle with yours. "
					+ "Your own tongue proves superior though, and she soon melts into your arms as you explore the sensitive corners of her mouth.",target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.powerful){
			return String.format("%s pulls you into an aggressive kiss, forcing her tongue into your mouth. "
					+ "It catches you off guard, but her assertiveness is quite arousing.",self.name());
		}
		else if(modifier==Result.clever){
			return String.format("%s leans in close to capture your lips, a lusty look in her eyes. You instinctively close your eyes "
					+ "to meet the kiss, but it doesn't arrive. Confused, you open your eyes to see her mischievous grin only inches away. "
					+ "She quickly darts in and steals a kiss, catching you by surprise.",self.name());
		}
		else{
			return String.format("%s licks her lips seductively, before leaning in to claim yours. Despite the feeling that she has the "
					+ "advantage here, you can't resist accepting the kiss eagerly. Her tongue lightly traces your lips, then slips in to "
					+ "explore your mouth. You have a lot of practice kissing, but she's so seductive and talented that you can barely respond.",self.name());
		}
	}
	@Override
	public String toString(){
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Forceful Kiss";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Steal Kiss";
		}else{
			return name;
		}

	}

	@Override
	public String describe() {
		if(self.get(Attribute.Power)>self.get(Attribute.Cunning) && self.get(Attribute.Power)>self.get(Attribute.Seduction)){
			return "Strong kiss using Power: 15 Mojo";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Seduction)){
			return "Sneaky kiss using Cunning: 15 Mojo";
		}else{
			return "Mojo boosted kiss: 15 Mojo";
		}
	}
}
