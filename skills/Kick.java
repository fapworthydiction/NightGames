package skills;

import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Kick extends Skill {

	public Kick(Character self) {
		super("Kick", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Power)>=17;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=17;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.feet(self)&&self.canAct()&&(!c.stance.prone(self)||self.has(Trait.dirtyfighter)&&!c.stance.penetration(self));
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(!target.bottom.isEmpty()&&self.getPure(Attribute.Ki)>=21&&Global.random(3)==2){
			if(self.human()){
				c.write(self,deal(0,Result.special,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.special,target));
			}
			target.shred(1);
		}
		else if(target.roll(this, c, accuracy()+self.tohit())){
			int m = Global.random(12)+self.get(Attribute.Power);
			if(self.human()){
				if(c.stance.prone(self)){
					c.write(self,deal(m,Result.strong,target));
					m *= 1.5;
				}
				else{
					c.write(self,deal(m,Result.normal,target));
	
				}
			}
			else if(target.human()){
				if(c.stance.prone(self)){
					c.write(self,receive(m,Result.strong,target));
					m *= 1.5;
				}
				else{
					c.write(self,receive(m,Result.normal,target));
				}
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Jewel")){
					c.offerImage("Kick.jpg", "Art by AimlessArt");
				}
				self.buildMojo(10);
			}
			m = self.bonusProficiency(Anatomy.feet, m);
			target.pain(m,Anatomy.genitals,c);
			if(self.has(Trait.wrassler)){
				target.calm(m/4,c);
			}
			else{
				target.calm(m/2,c);
			}			
			target.emote(Emotion.angry,80);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Kick(user);
	}
	public int speed(){
		return 8;
	}
	public Tactics type() {
		return Tactics.damage;
	}
	public String toString(){
		if(self.getPure(Attribute.Ki)>=14){
			return "Shatter Kick";
		}
		else{
			return "Kick";
		}
	}
	
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return "Your kick hits nothing but air.";
		}
		if(modifier==Result.special){
			return String.format("You focus your ki into a single kick, targeting not %s's body, but %s %s. The garment is completely destroyed, but " +
					"%s is safely left completely unharmed. Wait, you are actually fighting right now, aren't you?",
					target.name(),target.possessive(false),target.bottom.peek(),target.pronounSubject(false));
		}
		if(modifier==Result.strong){
			if(target.hasBalls()){
				return String.format("Lying on the floor, you feign exhaustion, hoping %s will lower her guard. As %s approaches unwarily, you suddenly kick up between " +
						"%s legs, delivering a painful hit to %s family jewels.",target.name(),target.pronounSubject(false),target.possessive(false),target.possessive(false));
			}else{
				return "Lying on the floor, you feign exhaustion, hoping "+target.name()+" will lower her guard. As she approaches unwarily, you suddenly kick up between " +
						"her legs, delivering a painful hit to her sensitive vulva.";
			}
		}
		else{
			if(target.hasBalls()){
				return String.format("You deliver a swift kick between %s's legs, hitting %s squarely on the balls.",target.name(),target.pronounTarget(false));
			}
			else{
				return "You deliver a swift kick between "+target.name()+"'s legs, hitting her squarely on the baby maker.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+"'s kick hits nothing but air.";
		}
		if(modifier==Result.special){
			return self.name()+" launches a powerful kick straight at your groin, but pulls it back just before impact. You feel a chill run down your spine and your testicles " +
					"are grateful for the last second reprieve. Your "+target.bottom.peek()+" crumble off your body, practically disintegrating.... Still somewhat grateful.";
		}
		if(modifier==Result.strong){
			return "With "+self.name()+" flat on her back, you quickly move in to press your advantage. Faster than you can react, her foot shoots up between " +
					"your legs, dealing a critical hit on your unprotected balls.";
		}
		else{
			switch(Global.random(2)){
			case 1:
				return self.name()+" delivers a swift and confident kick right into your sensitive nutsack.  "
						+ "You can almost taste your balls in the back of your throat.";
			default:
				return self.name()+"'s foot lashes out into your delicate testicles with devastating force. ";
			}
		}
	}

	@Override
	public String describe() {
		if(self.getPure(Attribute.Ki)>=14){
			return "A precise, Ki-powered kick that can destroy clothing";
		}
		else{
			return "Kick your opponent in the groin";
		}
	}
}
