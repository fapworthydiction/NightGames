package skills;

public enum TacticGroup {
	    All,
	    Pleasure,
	    Positioning,
	    Hurt,
	    Misc,
	    Recovery,
	    Manipulation,
	    Flask,
	    Potion,
	    Demand,
}
