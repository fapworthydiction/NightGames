package skills;

import global.Global;
import stance.StandingOver;
import status.Bound;
import status.Oiled;
import status.Stsflag;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;

import combat.Combat;
import combat.Result;

public class TentaclePorn extends Skill {

	public TentaclePorn(Character self) {
		super("Tentacle Porn", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Fetish)>=12;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish)>=12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !c.stance.sub(self)&&!c.stance.prone(self)&&!c.stance.prone(target)&&self.canAct()&&self.getArousal().get()>=20&&self.canSpend(10);
	}

	@Override
	public String describe() {
		return "Create a bunch of hentai tentacles.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(target.nude()){
				if(target.bound()){
					if(self.human()){
						c.write(self,deal(0,Result.special,target));
					}
					else if(target.human()){
						c.write(self,receive(0,Result.special,target));
					}
					target.pleasure(Global.random(self.get(Attribute.Fetish)+target.get(Attribute.Perception)),Anatomy.genitals,c);
					target.emote(Emotion.horny, 10);
				}
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.normal,target));
				}
				if(!target.is(Stsflag.oiled)){
					target.add(new Oiled(target),c);
				}
				target.pleasure(Global.random(self.get(Attribute.Fetish)/2),Anatomy.genitals,c);
				target.emote(Emotion.horny, 20);
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.weak,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.weak,target));
				}
			}
			target.add(new Bound(target,10+self.get(Attribute.Fetish)/3,"tentacles"));
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new TentaclePorn(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You summon a mass of tentacles that try to snare "+target.name()+", but she nimbly dodges them.";
		}
		else if(modifier == Result.weak){
			return "You summon a mass of phallic tentacles that wrap around "+target.name()+"'s arms, holding her in place.";
		}
		else if(modifier == Result.normal){
			return "You summon a mass of phallic tentacles that wrap around "+target.name()+"'s naked body. They squirm against her and squirt slimy fluids on her body.";
		}
		else{
			return "You summon tentacles to toy with "+target.name()+"'s helpless form. The tentacles toy with her breasts and penetrate her pussy and ass.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" stomps on the ground and a bundle of tentacles erupt from the ground. You're barely able to avoid them.";
		}
		else if(modifier == Result.weak){
			return self.name()+" stomps on the ground and a bundle of tentacles erupt from the ground around you, entangling your arms and legs.";
		}
		else if(modifier == Result.normal){
			return self.name()+" stomps on the ground and a bundle of tentacles erupt from the ground around you, entangling your arms and legs. The slimy appendages " +
					"wriggle over your body and coat you in the slippery liquid.";
		}
		else{
			return self.name()+" summons slimy tentacles that cover your helpless body, tease your dick, and probe your ass.";
		}
	}

}
