package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;

public class PerfectTouch extends Skill {

	public PerfectTouch(Character self) {
		super("Sleight of Hand", self);
	}

	@Override
	public boolean requirements() {
		return self.has(Trait.amateurMagician);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&(!target.nude())&&self.canSpend(40)&&!c.stance.prone(self)&&self.canAct()&&!c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(40);
		if(target.has(Trait.reflexes)){
			if(self.human()){
				c.write(self,deal(0,Result.unique,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.unique,target));
			}
			self.pain(5, Anatomy.head);
			self.emote(Emotion.nervous, 10);
			target.emote(Emotion.dominant,15);
		}
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
				c.write(target,target.nakedLiner());
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			target.undress(c);
			target.emote(Emotion.nervous, 10);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return self.has(Trait.amateurMagician);
	}

	@Override
	public Skill copy(Character user) {
		return new PerfectTouch(user);
	}
	public int speed(){
		return 2;
	}
	public int accuracy(){
		return 1;
	}
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.unique){
			return "You feint to the left while your right hand grabs "+target.name()+"'s clothes. Before you can remove anything, she hits you across the face with a lightning fast backfist. "
					+ "You stumble back, as your head rings from the impact.";
		}
		if(modifier==Result.miss){
			return "You try to steal "+target.name()+"'s clothes off of her, but she catches you.";
		}
		else{
			switch(Global.random(3)){
			case 2:
				return "In a flurry of movement, you strip each piece of " + target.name() + "'s clothing off. By the time she can block your hands, she's already naked.";
			case 1:
				return  "You divert " + target.name() + "'s attention with a small show of footwork. While she's distracted, you effortlessly strip each piece of her clothing off before she notices what's happening.";
			default:
				return "You feint to the left while your right hand makes quick work of "+target.name()+"'s clothes. By the time she realizes what's happening, you've " +
				"already stripped all her clothes off.";
			}
			
		}
				
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" lunges toward you, but you catch her hands before she can get ahold of your clothes.";
		}
		else{
			return self.name()+" lunges towards you, but dodges away without hitting you. She tosses aside a handful of clothes, at which point you realize you're " +
					"naked. How the hell did she manage that?";
		}
				
	}

	@Override
	public String describe() {
		return "Strips opponent completely: 40 Mojo";
	}
}
