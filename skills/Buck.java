package skills;

import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import characters.Anatomy;

import combat.Combat;
import combat.Result;

public class Buck extends Skill {

	public Buck(Character self) {
		super("Buck", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Submissive)>=18;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.penetration(self)&&c.stance.sub(self);	
	}

	@Override
	public void resolve(Combat c, Character target) {
		Anatomy inside;
		if(c.stance.anal()){
			if(self.human()){
				c.write(self,deal(0,Result.anal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.anal,target));
			}
			inside = Anatomy.ass;
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			inside = Anatomy.genitals;
		}
		int m = self.getSexPleasure(2, Attribute.Submissive);
		target.pleasure(self.bonusProficiency(inside, m),inside,Result.finisher,c);
		int r = target.getSexPleasure(1, Attribute.Seduction)/3;
		r += target.bonusRecoilPleasure(r);
		if(self.has(Trait.experienced)){
			r = r/2;
		}

		c.stance.setPace(2);
	}

	@Override
	public Skill copy(Character user) {
		return new Buck(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			return "You do your best to thrust back against "+target.name()+", as "+target.pronounSubject(false)+" pegs you.";
		}else{
			return "You buck your hips, thrusting into "+target.name()+" from below. She moans in pleasure as she tries to retain control.";
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			return self.name()+" presses her ass against your hips, hitting you with unexpected pleasure despite the awkward position.";
		}else{
			return self.name()+" pushes her hips against you, matching you thrust for thrust. The sudden change of pace catches you off-guard "
					+ "and hits you with a jolt of pleasure.";
		}
		
	}

	@Override
	public String describe() {
		return "Buck against your opponent from the bottom, giving some pleasure";
	}

}

