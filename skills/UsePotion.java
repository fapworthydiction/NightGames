package skills;

import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Potion;
import characters.Character;

import combat.Combat;
import combat.Result;

public class UsePotion extends Skill {
	private Potion item;

	public UsePotion(Character self, Potion item) {
		super(item.getName(), self);
		this.item = item;
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&self.canAct()&&self.has(item)&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public String describe() {
		return item.getDesc();
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(item, 1);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,self));
		}
		self.add(item.getEffect(self),c);
	}

	@Override
	public Skill copy(Character user) {
		return new UsePotion(user,item);
	}

	@Override
	public Tactics type() {
		return item.getTactic();
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return item.getMessage(self);
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name() +item.getMessage(self);
	}

}
