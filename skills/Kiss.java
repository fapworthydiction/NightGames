package skills;

import status.Charmed;
import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Kiss extends Skill {

	public Kiss(Character self) {
		super("Kiss", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.kiss(self)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m=Global.random(4)+self.get(Attribute.Seduction)/4;
		if(self.has(Trait.greatkiss)&&self.canSpend(10)&&Global.random(4)==3){
			m += Global.random(3);
			if(self.human()){
				c.write(self,deal(m,Result.special,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Jewel")){
					c.offerImage("Kiss.jpg", "Art by AimlessArt");
				}
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Cassie")){
					c.offerImage("Cassie Kiss.jpg", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(m,Result.special,target));
				if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Jewel")){
					c.offerImage("Kissed.jpg", "Art by AimlessArt");
				}
			}			
			target.add(new Charmed(target),c);
			self.spendMojo(10);
		}
		else if(self.get(Attribute.Seduction)>=9){
			if(self.human()){
				c.write(self,deal(m,Result.normal,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Jewel")){
					c.offerImage("Kiss.jpg", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(m,Result.normal,target));
				if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Jewel")){
					c.offerImage("Kissed.jpg", "Art by AimlessArt");
				}
			}
			self.buildMojo(10);
		}
		else{			
			if(self.human()){
				c.write(self,deal(m,Result.weak,target));
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Jewel")){
					c.offerImage("Kiss.jpg", "Art by AimlessArt");
				}
			}
			else if(target.human()){
				c.write(self,receive(m,Result.weak,target));
				if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Jewel")){
					c.offerImage("Kissed.jpg", "Art by AimlessArt");
				}
			}
			self.buildMojo(5);
		}
		if(self.has(Trait.romantic)){
			m *= 1.2f;
		}
		target.tempt(m,Result.foreplay,c);
		if(self.get(Attribute.Seduction)>=9){
			target.pleasure(1,Anatomy.mouth,c);
		}

	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Kiss(user);
	}
	public int speed(){
		return 6;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return "You pull "+target.name()+" to you and kiss her passionately. You run your tongue over her lips until her opens them and immediately invade her mouth. " +
					"You tangle your tongue around hers and probe the sensitive insides her mouth. As you finally break the kiss, she leans against you, looking kiss-drunk and needy.";
		}
		else if(modifier==Result.weak){
			return "You aggressively kiss "+target.name()+" on the lips. It catches her off guard for a moment, but she soon responds approvingly.";
		}
		else{
			switch(Global.random(3)){
			case 0: return "You pull "+target.name()+" close and capture her lips. She returns the kiss enthusiastically and lets out a soft noise of approval when you " +
					"push your tongue into her mouth.";
			case 1: return "You press your lips to "+target.name()+"'s in a romantic kiss. You tease out her tongue and meet it with your own.";
			default: return "You kiss "+target.name()+" deeply, overwhelming her senses and swapping quite a bit of saliva.";
			}
			
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return self.name()+" seductively pulls you into a deep kiss. As first you try to match her enthusiastic tongue with your own, but you're quickly overwhelmed. She draws " +
					"your tongue into her mouth and sucks on it in a way that seems to fill your mind with a pleasant, but intoxicating fog.";
		}
		else if(modifier==Result.weak){
			return self.name()+" presses her lips against yours in a passionate, if not particularly skillful, kiss.";
		}
		else{
			switch(Global.random(3)){
			case 0: return self.name()+" grabs you and kisses you passionately on the mouth. As you break for air, she gently nibbles on your bottom lip.";
			case 1: return self.name()+" peppers quick little kisses around your mouth before suddenly taking your lips forcefully and invading your mouth with her tongue.";
			default: return self.name()+" kisses you softly and romantically, slowly drawing you into her embrace. As you part, she teasingly brushes her lips against yours.";
			}
		}
	}

	@Override
	public String describe() {

			return "Kiss your opponent";
	}
}
