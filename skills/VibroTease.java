package skills;

import items.Item;
import items.Toy;
import stance.Stance;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;

import combat.Combat;
import combat.Result;

public class VibroTease extends Skill {

	public VibroTease(Character self) {
		super("Vibro-Tease", self);
	}

	@Override
	public boolean requirements() {
		return self.has(Toy.Strapon2);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Toy.Strapon2);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.dom(self)&&c.stance.en==Stance.anal&&self.has(Toy.Strapon2);
	}

	@Override
	public String describe() {
		return "Turn up the strapon vibration";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else{
			if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
		}
		m = 3+self.get(Attribute.Seduction)/2+target.get(Attribute.Perception)+(self.get(Attribute.Science)/2);
		m = self.bonusProficiency(Anatomy.toy, m);
		target.pleasure(m,Anatomy.genitals,c);
		self.pleasure(2+self.get(Attribute.Perception),Anatomy.genitals,c);
		self.buildMojo(20);
		c.stance.setPace(0);
	}

	@Override
	public Skill copy(Character user) {
		return new VibroTease(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" cranks up the vibration of her strapon to maximum level which stirs up your insides. " +
				"She teasingly pokes the tip against your prostate which causes your limbs to get shaky from the pleasure.";
	}

}
