package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Toy;
import status.Beaded;
import status.Stsflag;

public class InsertBead extends Skill {
    public InsertBead(Character self) {
        super("Insert Anal Bead", self);
    }

    @Override
    public boolean requirements() {
        return true;
    }

    @Override
    public boolean requirements(Character user) {
        return true;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.has(Toy.AnalBeads)&&
                self.canAct()&&c.stance.reachBottom(self)&&target.pantsless()&&
                (!self.human()|| Scheduler.getMatch().condition!= Modifier.notoys);
    }

    @Override
    public String describe() {
        return "Insert an anal bead into your opponent's ass";
    }

    @Override
    public void resolve(Combat c, Character target) {
        int m;
        if(target.roll(this, c, accuracy()+self.tohit())){
            if(self.human()){
                c.write(self,deal(0,Result.normal,target));
            }
            else if(target.human()){
                c.write(self,receive(0,Result.normal,target));
            }
            target.add(new Beaded(target,1),c);
        }
        else{
            if(self.human()){
                c.write(self,deal(0,Result.miss,target));
            }
            else if(target.human()){
                c.write(self,receive(0,Result.miss,target));
            }
        }
    }

    @Override
    public Skill copy(Character user) {
        return new InsertBead(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if(modifier==Result.miss){
            return "You try to insert an anal bead into "+target.name()+"'s ass, but she's struggling too much.";
        }
        else{
            return "You manage to push an anal bead into "+target.name()+"'s butt. You could pull it out now, but it would " +
                    "make a bigger impact if you can get some more in there.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if(modifier==Result.miss){
            return self.name()+ " tries to stick a bead up your ass, but you manage to get away.";
        }
        else{
            return self.name() + " shoves an anal bead up your ass before you can resist. As uncomfortable as it is, you're sure getting it " +
                    "pulled out will be worse.";
        }
    }
}
