package skills;

import characters.*;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;

public class LowBlow extends Skill {

	public LowBlow(Character self) {
		super("Low Blow", self);
	}

	@Override
	public boolean requirements() {
		return self.getRank()>=2 &&
			(self.name()=="Cassie" || self.name()=="Angel" || self.name()=="Yui" || self.name()=="Maya" || 
			self.name()=="Samantha" || self.name()=="Eve" || self.name()=="Jewel");
	}

	@Override
	public boolean requirements(Character user) {
		return user.getRank()>=2 &&
				(user.name()=="Cassie" || user.name()=="Angel" || user.name()=="Yui" || user.name()=="Maya" || 
				user.name()=="Samantha" || user.name()=="Eve" || user.name()=="Jewel");
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c) && self.canSpend(30);
	}

	@Override
	public String describe() {
		return "A specialized low attack: 30 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(30);
		int m = self.getLevel()+self.get(Attribute.Power);
		if(target.human()){
			c.write(self,receive(m,Result.normal,target));
		}
		target.pain(m,Anatomy.genitals,c);
		if(self.has(Trait.wrassler)){
			target.calm(m/4,c);
		}
		else{
			target.calm(m/2,c);
		}			
		target.emote(Emotion.angry,50);

	}

	@Override
	public Skill copy(Character user) {
		return new LowBlow(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		switch(self.id()){
			case CASSIE:
				return "Cassie begins casting a spell, bringing her fingers in front of her and pointing them at your groin.<br>"
						+ "<i>\"Trench Run formation! Launch!\"</i>"
						+ "With an arcane flash, two tiny female faeries appear and speed towards you along the ground like torpedoes. "
						+ "When they reach the ground at your feet, the faeries fly straight upward between your legs "
						+ "and deliver powerful uppercuts to each of your testicles.<br>"
						+ "Cassie pumps her fist triumphantly. <i>\"Great shot kids! That was two in a million!\"</i>";
			case EVE:
				return "Eve puckers her lips and moves her face towards yours.  Expecting a kiss, you are caught off guard when she instead "
						+ "spits directly into your eyes.  Momentarily blinded, you step back and wipe your eyes, but Eve uses the "
						+ "opportunity to wind up for a well-placed kick that connects solidly with both your balls.  "
						+ "<i>\"I sure hope you didn't think this was going to be a fair fight,\"</i> she says, while you try to both "
						+ "restore your vision and also clutch helplessly at your testicles.";

			case JEWEL:
				return "Jewel steps in close to you and does an exaggerated wind-up for a big right haymaker.  "
						+ "You raise your hands to block the well-telegraphed punch, but she catches you completely off-guard when she quickly "
						+ "lashes her left arm out and, with a lightning-fast flick of her wrist, firmly slaps your balls with the back of her hand. ";
			case SAMANTHA:
				return "Samantha turns around and seductively backs her perfect ass up into your crotch.  She arches her back and bends forward "
						+ "slightly as she grinds herself firmly against your dick and balls.  You start to get distracted by her marvelous body, "
						+ "when she suddenly kicks her heel up backwards, hitting you squarely between the legs.  She turns back around and "
						+ "whispers in your ear, <i>\"You know what they say; all is fair in love and war.\"</i>";
			case YUI:
				return  "Faster than you can react, Yui drops a tiny bomb, and you lose sight of her in a cloud of smoke.  You glance around looking for her, "
						+ "but you appear to be alone.  You suddenly feel a hand swing upward between your legs with an open-palm strike, "
						+ "and you hear the clear, slapping sound of your ballsack being forcefully bounced against your pelvis.  "
						+ "<i>\"That is a very old technique,\"</i> Yui says, silently crouching behind you, \"<i>It is called 'Ringing the Temple Bell.'\"</i>";
			case MAYA:
				return "Maya gracefully drops to the ground in a perfect split, with one of her legs in front of her and one behind her.  "
						+ "Her new position leaves her at the perfect height to punch you in the crotch, and she plants her knuckles firmly "
						+ "into your nutsack.  You clutch at your tenderized balls as she gracefully gets up from her split and says: "
						+ "<i>\"Not protecting your testicles in an amateur mistake.  I would expect that from a freshman.\"</i>";
			default:
				return String.format("%s brings you close to her, grabs your hands and places them forcefully on her breasts.  "
						+ "She gazes hypnotically into your eyes, and she softly moans as you feel her nipples slowly stiffen against your palms.  "
						+ "You are losing yourself in her gaze, and you lower your guard, as if in a trance.  %s takes advantage of your "
						+ "distracted state to grab you by the shoulders and raise her knee into your groin, crushing your unprotected testicles.",self.name(),self.name());
		}
	}

}
