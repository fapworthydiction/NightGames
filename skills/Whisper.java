package skills;

import status.Enthralled;
import status.Sensitive;
import global.Flag;
import global.Global;
import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Whisper extends Skill {

	public Whisper(Character self) {
		super("Whisper", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.kiss(self)&&self.canAct()&&!self.has(Trait.direct);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.has(Trait.darkpromises)&&Global.random(5)==4&&self.canSpend(15)){
			if(self.human()){
				c.write(self,deal(0,Result.special,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.special,target));
				if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Angel")){
					c.offerImage("Dark Promise.jpg", "Art by AimlessArt");
				}
			}
			self.spendMojo(15);
			target.add(new Enthralled(target,self),c);
			
		}
		else if(self.getPure(Attribute.Professional)>=1){
			if(self.human()){
				c.write(self,deal(0,Result.strong,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.strong,target));
			}
			target.add(new Sensitive(target, 2,Anatomy.genitals,1+self.get(Attribute.Professional)/10),c);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
		}

		target.tempt((self.get(Attribute.Seduction)/2)+(2*self.getSkimpiness()),c);
		if(self.has(Trait.secretkeeper)){
			target.emote(Emotion.nervous,50);
		}
		target.emote(Emotion.horny, 30);
		self.buildMojo(10);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=32 && !self.has(Trait.direct);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=32 && !user.has(Trait.direct);
	}

	@Override
	public Skill copy(Character user) {
		return new Whisper(user);
	}
	public int speed(){
		return 9;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return "You whisper words of domination in "+target.name()+"'s ear, filling her with your darkness. The spirit in her eyes seems to dim as she submits to your will.";
		}
		if(modifier==Result.strong){
			return "You whisper extraordinarily suggestive promises into "+target.name()+"'s ear until "+target.pronounSubject(false)+"'s primed to receive your touch.";
		}
		else {
			return "You whisper sweet nothings in "+target.name()+"'s ear. Judging by her blush, it was fairly effective.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return this.self.name() + " whispers in your ear in some eldritch language."
					+ " Her words echo through your head and you feel a"
					+ " strong compulsion to do what she tells you";
		}
		if(modifier==Result.strong){
			return self.name()+" whispers some deliciously seductive suggestions in your ear. "+self.pronounSubject(false)+" paints such an erotic mental picture "
					+ "for you that you find your dick straining with anticipation.";
		}
		else{
			return self.name()+" whispers some deliciously seductive suggestions in your ear.";
		}
	}

	@Override
	public String describe() {
		return "Arouse opponent by whispering in her ear";
	}
}
