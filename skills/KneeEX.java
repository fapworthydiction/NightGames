package skills;

import characters.*;
import characters.Character;
import global.Flag;
import stance.StandingOver;
import global.Global;

import combat.Combat;
import combat.Result;

public class KneeEX extends Skill {

	public KneeEX(Character self) {
		super("Mighty Knee", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&!c.stance.prone(self)&&self.canAct()&&
				!c.stance.behind(target)&&!c.stance.penetration(target)&&self.canSpend(20);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		Result style = Result.powerful;
		if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning) && self.get(Attribute.Seduction)>self.get(Attribute.Power)){
			style = Result.seductive;
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Power)){
			style = Result.clever;
		}
		self.spendMojo(20);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.human()){
				c.write(self,deal(0,style,target));
			}
			else if(target.human()){
				c.write(self,receive(0,style,target));
				if(Global.random(5)>=3){
					c.write(self,self.bbLiner());
				}
				if(!Global.checkFlag(Flag.exactimages)||self.id()== ID.CASSIE){
					c.offerImage("Knee.png", "Art by AimlessArt");
				}
			}
			if(style == Result.powerful){
				m = 4+Global.random(11)+self.get(Attribute.Power);
				m*=1.3f;
			}else if(style == Result.clever){
				m = 4+Global.random(11)+self.get(Attribute.Cunning);
			}else{
				m = 4+Global.random(11)+self.get(Attribute.Seduction);
			}
			
			target.pain(m,Anatomy.genitals,c);
			if(self.has(Trait.wrassler)){
				target.calm(m/2,c);
			}
			else{
				target.calm(m,c);
			}
			target.emote(Emotion.angry,40);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Power)>=10 && !self.has(Trait.cursed);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=10 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new KneeEX(user);
	}
	public int speed(){
		return 4;
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return target.name()+" blocks your knee strike.";
		}
		if(target.hasBalls()){
			if(modifier==Result.powerful){
				return String.format("You brace yourself for a moment, before unleashing your strongest knee strike to %s's vulnerable ballsack. "
						+ "Her eyes water, and her mouth drops open in silent agony.",target.name());
			}
			else if(modifier==Result.clever){
				return String.format("You distract %s with a cunning feint, before striking with a knee-shaped sneak attack to her dangling balls. "
						+ "You don't have a lot of power behind it, but accuracy and surprise do the job just fine.",target.name());
			}
			else{
				return String.format("You seductively pull %s into a heated kiss. You feel her erection poking you, which helps you locate your real "
						+ "target without looking. You deliver a quick, decisive knee strike to her defenseless balls. You feel her completely freeze in shock and "
						+ "pain, but hold the kiss for another couple seconds before releasing her.", target.name());
			}
		}
		else{
			if(modifier==Result.powerful){
				return String.format("You brace yourself for a moment, before unleashing your strongest knee strike to %s's unguarded groin. "
						+ "With no testicles to protect, she probably wasn't expecting a low blow. Judging by her pained expression, her "
						+ "pussy is a pretty effective target.",target.name());
			}
			else if(modifier==Result.clever){
				return String.format("You distract %s with a cunning feint, before delivering a sharp knee strike to her sensitive vulva. "
						+ "It's a quick attack without much power, but she was completely unprepared for it. A true critical hit.",target.name());
			}
			else{
				return String.format("You tease %s with multiple quick kisses, until you're sure she's completely focused on your lips. While she's "
						+ "distracted and defenseless, you knee her firmly in the pussy. Her eyes go wide in shock for a moment, before they start to "
						+ "water from the pain.",target.name());
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" tries to knee you in the balls, but you block it.";
		}
		if(modifier==Result.powerful){
			return String.format("%s grabs you by the shoulders and slams her knee into your groin with devastating force. The pain is "
					+ "indescribable, and for a second you wonder if your testicles were pushed into your body by the force.",self.name());
		}
		else if(modifier==Result.clever){
			return String.format("%s's eyes momentarily dart past you. She tries to hide her reaction, but not quite good enough. You reflexively "
					+ "look behind you, but there's nothing there. Suddenly, a burst of pain from your groin hits you. You left your guard down just "
					+ "long enough for a sneaky knee to the balls.",self.name());
		}
		else{
			return String.format("%s licks her lips seductively, before leaning in to claim yours. Despite the feeling that she has the "
					+ "advantage here, you can't resist accepting the kiss eagerly. The moment your lips meet, she plants her knee painfully "
					+ "between your legs. The kiss was just a diversion. Her real target was your delicate balls.",self.name());
		}
	}

	@Override
	public String toString(){
		if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning) && self.get(Attribute.Seduction)>self.get(Attribute.Power)){
			return "Kiss and Knee";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Power)){
			return "Tricky Knee";
		}
		else{
			return name;
		}

	}

	@Override
	public String describe() {
		if(self.get(Attribute.Seduction)>self.get(Attribute.Cunning) && self.get(Attribute.Seduction)>self.get(Attribute.Power)){
			return "Knee strike after lowering your target's guard with your charms: 15 Mojo";
		}else if(self.get(Attribute.Cunning)>self.get(Attribute.Power)){
			return "Knee strike while misdirecting your target: 15 Mojo";
		}else{
			return "Mojo boosted Knee strike: 15 Mojo";
		}
	}

}
