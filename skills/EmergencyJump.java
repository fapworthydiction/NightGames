package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import stance.Behind;
import status.Abuff;
import status.Primed;

public class EmergencyJump extends Skill {

	public EmergencyJump(Character self) {
		super("Emergency Jump", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Temporal)>=4;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal)>=4;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return ((c.stance.sub(self)&&!c.stance.mobile(self)&&!c.stance.penetration(self)&&!c.stance.penetration(target))||self.bound())&&!self.stunned()&&!self.distracted()&&self.getStatusMagnitude("Primed")>=2;
	}

	@Override
	public String describe() {
		return "Escape from a disadvantageous position and/or bind: 2 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.add(new Primed(self,-2));
		self.free();
		c.stance = new Behind(self,target);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.emote(Emotion.confident, 15);
		target.emote(Emotion.nervous, 15);

	}

	@Override
	public Skill copy(Character user) {
		return new EmergencyJump(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You're in trouble for a moment, so you trigger your temporal manipulator and stop time just long "
				+ "enough to free yourself.");
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("You thought you had %s right where you want %s, but %s seems to vanish completely and escape.",
				self.name(),self.pronounTarget(false),self.pronounSubject(false));
	}

}
