package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;

public class BunshinAssault extends Skill {

	public BunshinAssault(Character self) {
		super("Bunshin Assault", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Ninjutsu)>=6;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&!c.stance.behind(target)&&!c.stance.penetration(target)&&self.canSpend(6);
	}

	@Override
	public String describe() {
		return "Attack your opponent with shadow clones: 3 Mojo per attack (min 2)";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int clones = Math.min(Math.min(self.getMojo().get()/3, self.get(Attribute.Ninjutsu)/3),6);
		Result r;
		self.spendMojo(clones*3);
		if(self.human()){
			c.write(String.format("You form %d shadow clones and rush forward.",clones));
		}
		else if(target.human()){
			c.write(String.format("%s moves in a blur and suddenly you see %d of %s approaching you.",self.name(),clones,self.pronounTarget(false)));
		}
		for(int i=0;i<clones;i++){
			if(target.roll(this, c, accuracy()+self.tohit())){
				switch(Global.random(4)){
				case 0:
					r=Result.weak;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					target.pain(Global.random(4)+3,Anatomy.ass,c);
					target.calm(3,c);
					break;
				case 1:
					r=Result.normal;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					target.pain(Global.random(4)+self.get(Attribute.Power)/3, Anatomy.chest,c);
					target.calm(3,c);
					break;
				case 2:
					r=Result.strong;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					target.pain(Global.random(8)+self.get(Attribute.Power)/2, Anatomy.genitals,c);
					if(!self.has(Trait.wrassler)){
						target.calm(3,c);
					}
					break;
				default:
					r=Result.critical;
					if(self.human()){
						c.write(self,deal(0,r,target));
					}
					else if(target.human()){
						c.write(self,receive(0,r,target));
					}
					target.pain(Global.random(12)+self.get(Attribute.Power), Anatomy.genitals,c);
					if(!self.has(Trait.wrassler)){
						target.calm(3,c);
					}
					break;
				}
				target.emote(Emotion.angry, 30);
			}else{
				if(self.human()){
					c.write(self,deal(0,Result.miss,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.miss,target));
				}
			}
		}

	}

	@Override
	public Skill copy(Character user) {
		return new BunshinAssault(user);
	}
	public int speed(){
		return 4;
	}
	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("%s dodges one of your shadow clones.",target.name());
		}else if(modifier==Result.weak){
			return String.format("Your shadow clone gets behind %s and slaps %s hard on the ass.",target.name(),target.pronounTarget(false));
		}else if(modifier==Result.strong){
			if(target.hasBalls()){
				return String.format("One of your clones gets grabs and squeezes %s's balls.",target.name());
			}else{
				return String.format("One of your clones hits %s on %s sensitive tit.",target.name(),target.possessive(false));
			}
		}else if(modifier==Result.critical){
			if(target.hasBalls()){
				return String.format("One lucky clone manages to deliver a clean kick to %s's fragile balls.",target.name());
			}else{
				return String.format("One lucky clone manages to deliver a clean kick to %s's sensitive vulva.",target.name());
			}
		}else{
			return String.format("One of your shadow clones lunges forward and strikes %s in the stomach.",target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You quickly dodge a shadow clone's attack.");
		}else if(modifier==Result.weak){
			return String.format("You lose sight of one of the clones until you feel a sharp spank on your ass cheek.");
		}else if(modifier==Result.strong){
			if(target.hasBalls()){
				return String.format("A %s clone gets a hold of your balls and squeezes them painfully.",self.name());
			}else{
				return String.format("A %s clone unleashes a quick roundhouse kick that hits your sensitive boobs.",self.name());
			}
		}else if(modifier==Result.critical){
			if(target.hasBalls()){
				return String.format("One lucky %s clone manages to land a snap-kick squarely on your unguarded jewels.",self.name());
			}else{
				return String.format("One %s clone hits you between the legs with a fierce cunt-punt.",self.name());
			}
		}else{
			return String.format("One of %s clones delivers a swift punch to your solar plexus.",self.possessive(false));
		}
	}

}
