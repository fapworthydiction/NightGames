package skills;

import items.Component;
import items.Item;
import items.Toy;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Nurple extends Skill {

	public Nurple(Character self) {
		super("Twist Nipples", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Power)>=13 && !self.has(Trait.cursed);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=13 && !user.has(Trait.cursed);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.topless()&&c.stance.reachTop(self)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.has(Toy.ShockGlove)&&self.has(Component.Battery,2)){
				if(self.human()){
					c.write(self,deal(0,Result.special,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.special,target));
				}
				target.pain(Global.random(9)+target.get(Attribute.Perception),Anatomy.chest,c);
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.normal,target));
				}
				target.pain(Global.random(9)+target.get(Attribute.Perception)/2,Anatomy.chest,c);
			}
			target.calm(Global.random(5),c);
			self.buildMojo(10);
			target.emote(Emotion.angry,25);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Nurple(user);
	}
	public int speed(){
		return 7;
	}
	public Tactics type() {
		return Tactics.damage;
	}
	public String toString(){
		if(self.has(Toy.ShockGlove)){
			return "Shock breasts";
		}
		else{
			return name;
		}
	}
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You grope at "+target.name()+"'s breasts, but miss.";
		}
		else if(modifier == Result.special){
			return "You grab "+target.name()+"'s boob with your shock-gloved hand, painfully shocking her.";
		}
		else{
			switch(Global.random(2)){
			case 1:
				return "You grab each of " + target.name() + "'s nipples in your hands and wrench them in opposite direction, eliciting a sharp yelp from her.";
			default:
				return "You pinch and twist "+target.name()+"'s nipples, causing her to yelp in surprise.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" tries to grab your nipples, but misses.";
		}
		else if(modifier == Result.special){
			return self.name()+" touches your nipple with her glove and a jolt of electricity hits you.";
		}
		else{
			switch(Global.random(2)){
			case 1:
				return self.name()+" twists your sensitive nipples, giving you a jolt of pain.";
			default:
				return self.name() + " takes your nipples in her fingers and starts twisting, not stopping until she hears a small cry of pain.";
			}
		}
	}

	@Override
	public String describe() {
		return "Twist opponent's nipples painfully";
	}
}
