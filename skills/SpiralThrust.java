package skills;

import characters.*;
import characters.Character;
import stance.Stance;

import combat.Combat;
import combat.Result;

public class SpiralThrust extends Skill {

	public SpiralThrust(Character self) {
		super("Spiral Thrust", self);
	}

	@Override
	public boolean requirements() {
		return self.has(Trait.spiral);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.spiral);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.dom(self)&&c.stance.penetration(self)&&self.canSpend(10);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int x = self.getMojo().get()/2;
		x = Math.min(x,20+self.getLevel());
		int m = self.bonusProficiency(Anatomy.genitals, x);
		if(c.stance.en==Stance.anal){
			if(self.human()){
				c.write(self,deal(0,Result.anal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.anal,target));
			}
			target.pleasure(m,Anatomy.ass,c);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
			target.pleasure(m,Anatomy.genitals,c);
		}

		self.spendMojo(2*x);
		int r = target.getSexPleasure(3, Attribute.Seduction);
		r += target.bonusRecoilPleasure(r);
		if(self.has(Trait.experienced)){
			r = r/2;
		}

		self.pleasure(r,Anatomy.genitals,c);
		self.emote(Emotion.horny, 20);
		c.stance.setPace(2);
	}

	@Override
	public Skill copy(Character user) {
		return new SpiralThrust(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			return "You unleash your strongest technique into "+target.name()+"'s ass, spiraling your hips and stretching her tight sphincter.";
		}
		else{
			return "As you thrust into "+target.name()+"'s hot pussy, you feel a power welling up inside you. You put everything you have into moving your hips circularly " +
					"while you continue to drill into her.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			return self.name()+" drills into your ass with extraordinary power. Your head seems to go blank and you fall face down to the ground as your arms turn to jelly and give out.";
		}
		else{
			return self.name()+" begins to move her hips wildly in circles, rubbing every inch of your cock with her hot, slippery pussy walls, bringing you more pleasure " +
					"than you thought possible.";
		}
	}

	@Override
	public String describe() {
			return "Converts your mojo into fucking: All Mojo";
	}

}
