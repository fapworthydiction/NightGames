package skills;

import global.Global;
import status.Horny;
import characters.Attribute;
import characters.Character;
import characters.Emotion;

import combat.Combat;
import combat.Result;

public class LustAura extends Skill {

	public LustAura(Character self) {
		super("Lust Aura", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Dark)>=3;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>=3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&self.canSpend(5);
	}

	@Override
	public String describe() {
		return "Inflicts arousal over time: 5 Arousal, 5 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(5);
		self.spendArousal(5);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.add(new Horny(target,3+2*self.getSkimpiness(),3+Global.random(3)),c);
		target.emote(Emotion.horny, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new LustAura(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You allow the corruption in your libido to spread out from your body. "+target.name()+" flushes with arousal and presses her thighs together as the aura taints her.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" releases an aura of pure sex. You feel your body becoming hot just being near her.";
	}

}
