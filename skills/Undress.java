package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.Charmed;

public class Undress extends Skill {

	public Undress(Character self) {
		super("Undress", self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		if(self.canAct()&&!c.stance.sub(self)&&!self.nude()&&!c.stance.prone(self)&&!self.has(Trait.strapped)&&!c.stance.behind(self)){
			if(striptease()){
				setName("Strip Tease");
			}
			else{
				setName("Undress");
			}
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public String describe() {
		if(striptease()){
			return "Tempt opponent by removing your clothes";
		}
		else{
			return "Removes your own clothes";
		}
	}

	@Override
	public void resolve(Combat c, Character target) {	
		if(striptease()&&!c.stance.behind(self)){
			if(self.getPure(Attribute.Professional)>=7){
				if(self.human()){
					c.write(self,deal(0,Result.special,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.special,target));
				}
				target.add(new Charmed(target));
			}else{
				if(self.human()){
					c.write(self,deal(0,Result.strong,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.strong,target));
				}
			}
			if(target.human()){
				if(!Global.checkFlag(Flag.exactimages)||self.name().startsWith("Angel")){
					c.offerImage("Strip Tease.jpg", "Art by AimlessArt");
				}
			}
			target.tempt((self.get(Attribute.Seduction)/4)*(self.top.size()+self.bottom.size())+self.get(Attribute.Professional),c);
			self.buildMojo(20);
			target.emote(Emotion.horny, 30);
			self.emote(Emotion.confident, 15);
			self.emote(Emotion.dominant, 15);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,target));
			}
		}
		self.undress(c);
	}

	@Override
	public Skill copy(Character user) {
		return new Undress(user);
	}

	@Override
	public Tactics type() {
		if(striptease()){
			return Tactics.pleasure;
		}
		else{
			return Tactics.negative;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return "You seductively perform a short dance, shedding clothes as you do so. "
					+ target.name()
					+ " seems quite taken with it, as "
					+ target.pronounSubject(false)
					+ " is practically drooling onto the ground.";
		}
		else if(modifier==Result.strong){
			return "During a brief respite in the fight as "+target.name()+" is catching her breath, you make a show of seductively removing your clothes. " +
					"By the time you finish, she's staring with undisguise arousal, pressing a hand unconsciously against her groin.";
		}
		else{
			return "You quickly strip off your clothes and toss them aside.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.special){
			return self.name()
					+ " takes a few steps back and starts "
					+ "moving sinuously. She sensually runs her hands over her body, "
					+ "undoing straps and buttons where she encounters them, and starts"
					+ " peeling her clothes off slowly, never breaking eye contact."
					+ " You can only gawk in amazement as her perfect body is revealed bit"
					+ " by bit, and the thought of doing anything to blemish such"
					+ " perfection seems very unpleasant indeed.";
		}
		else if(modifier==Result.strong){
			return self.name()+" asks for a quick time out and starts sexily slipping her her clothes off. Although there are no time outs in the rules, you can't help staring " +
					"at the seductive display until she finishes with a cute wiggle of her naked ass.";
		}
		else{
			return self.name()+" puts some space between you and strips naked.";
		}
	}
	private boolean striptease(){
		return self.getPure(Attribute.Seduction)>=24 && !self.has(Trait.direct);
	}
}
