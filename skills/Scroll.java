package skills;

import global.Global;
import items.Consumable;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;

import combat.Combat;
import combat.Result;

public class Scroll extends UseItem {

	public Scroll(Character self) {
		super(Consumable.FaeScroll, self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Arcane)>=2;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane)>=2;
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Consumable.FaeScroll, 1);
		if(target.nude()){
			if(self.human()){
				c.write(deal(0,Result.normal,target));
			}else if(target.human()){
				c.write(receive(0,Result.normal,target));
			}
			target.pleasure(25+self.get(Attribute.Arcane),Anatomy.genitals,c);
		}else{
			if(self.human()){
				c.write(deal(0,Result.weak,target));
			}else if(target.human()){
				c.write(receive(0,Result.weak,target));
			}
			target.undress(c);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Scroll(user);
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.weak){
			return "You unroll the summoning scroll and unleash a cloud of cute, naked faeries. They immediately swarm around "+target.name()+", grabbing and pulling at her " +
					"clothes. By the time they disappear, she's left completely naked.";
		}else{
			return "You unroll the summoning scroll and unleash a cloud of cute, naked faeries. They eagerly take advantage of "+target.name()+"s naked body, teasing and tickling " +
					"every exposed erogenous zone. She tries in vain to defend herself, but there are too many of them and they're too quick. She's reduced to writhing and giggling " +
					"in pleasure until the brief summoning spell expires.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.weak){
			return self.name()+" pulls out a scroll and a swarm of butterfly-winged faeries burst forth to attack you. They mischievously grab at your clothes, using magical assistance " +
					"to efficiently strip you naked.";
		}else{
			return self.name()+" pulls out a scroll and a swarm of butterfly-winged faeries burst forth to attack you. A couple of them fly into your face to distract you with naked girl " +
					"parts, while the rest play with your naked body. They focus especially on your dick and balls, dozens of tiny hands playfully immobilizing you with ticklish pleasure. The spell " +
					"doesn't actually last very long, but from your perspective, it feels like minutes of delightful torture.";
		}
	}

}
