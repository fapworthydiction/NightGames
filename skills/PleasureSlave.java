package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Stsflag;

public class PleasureSlave extends Skill {

	public PleasureSlave(Character self) {
		super("Pleasure Slave", self);
	}

	@Override
	public boolean requirements() {
		return self.get(Attribute.Submissive)>=30;
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Submissive)>=30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless()&&target.hasPussy()&&c.stance.oral(self)&&self.canAct()&&!c.stance.penetration(self)&&
				self.is(Stsflag.shamed)&&self.canSpend(30);
	}

	@Override
	public String describe() {
		return "Use the power of your shame to service your mistress";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(30);
		int m = Math.max(self.get(Attribute.Seduction),self.get(Attribute.Submissive))+target.get(Attribute.Perception);
		m *= self.getStatusMagnitude("Shamed");
		m = self.bonusProficiency(Anatomy.mouth, m);
		if(self.has(Trait.silvertongue)){
			m *= 1.2;
		}
		if(self.human()){
			c.write(self,deal(m, Result.normal, target));
		}else if(target.human()){
			c.write(self,receive(m, Result.normal, target));
		}
		target.pleasure(m, Anatomy.genitals, c);


	}

	@Override
	public Skill copy(Character user) {
		return new PleasureSlave(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("Feeling like a worthless slave boy, you devote your entire being to servicing %s. You lavish her lovely flower with your tongue, "
				+ "as though her pleasure is the only thing that can redeem you. Judging by your mistress moaning and trembling, you are doing an acceptable job."
				, target.name());
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s holds your penis worshipfully, before taking the entire length in her mouth. She licks and sucks with tremendous enthusiasm, "
				+ "giving her all to service you. The pleasure is too much to handle, and despite her attitude, you're completely at her mercy.",self.name());
	}

}
