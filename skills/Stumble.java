package skills;

import global.Global;
import stance.Behind;
import stance.Mount;
import stance.ReverseMount;
import stance.Stance;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class Stumble extends Skill {

	public Stumble(Character self) {
		super("Stumble", self);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Submissive)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive)>=9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.en==Stance.neutral;
	}

	@Override
	public String describe() {
		return "An accidental pervert classic";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(Global.random(2)==0){
			c.stance = new Mount(target,self);
		}else{
			c.stance = new ReverseMount(target,self);
		}
		if(self.human()){
			c.write(deal(0,Result.normal,target));
		}else{
			c.write(receive(0,Result.normal,target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Stumble(user);
	}

	@Override
	public Tactics type() {
		return Tactics.negative;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You slip and fall to the ground, pulling "+target.name()+" awkwardly on top of you.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" stumbles and falls, grabbing you to catch herself. Unfortunately, you can't keep your balance and you fall on top of her. Maybe that's not so unfortunate.";
	}

}
