package skills;

import stance.Behind;
import stance.Stance;
import stance.StandingOver;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class Cowardice extends Skill {

	public Cowardice(Character self) {
		super("Cowardice", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Submissive)>=3;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive)>=3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&target.canAct()&&c.stance.en==Stance.neutral;
	}

	@Override
	public String describe() {
		return "Turning your back to an opponent will likely get you attacked from behind.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		c.stance = new Behind(target,self);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else{
			c.write(self,receive(0,Result.normal,target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Cowardice(user);
	}

	@Override
	public Tactics type() {
		return Tactics.negative;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You try to run away, but "+target.name()+" catches you and grabs you from behind.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" tries to sprint away, but you quickly grab her from behind before she can escape.";
	}

}
