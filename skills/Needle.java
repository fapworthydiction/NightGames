package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Consumable;
import status.Drowsy;
import status.Horny;

public class Needle extends Skill {

	public Needle(Character self) {
		super("Needle Throw", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Ninjutsu)>=1;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self)&&self.canAct()&&self.has(Consumable.needle)&&!c.stance.prone(self)&&(!self.human()|| Scheduler.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public String describe() {
		return "Throw a drugged needle at your opponent.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Consumable.needle, 1);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.normal,self));
			}
			target.add(new Horny(target,3,4),c);
			target.add(new Drowsy(target,4),c);
		}else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,self));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Needle(user);
	}
	public int accuracy(){
		return 8;
	}
	public int speed(){
		return 9;
	}
	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You throw a small, drugged needle at %s, but %s dodges it.",target.name(),target.pronounSubject(false));
		}else{
			return String.format("You hit %s with one of your drugged needles. She flushes with arousal and appears to have trouble standing.",target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You spot a glint of metal and dodge out of the way just in time to avoid a small needle. You don't know what %s coated it with, "
					+ "but it's probably a good thing it missed.",self.name());
		}else{
			return String.format("You feel a tiny prick on your neck and discover you've been stuck by a small needle. You pull it out quickly, but already feel "
					+ "the drug going to work. Your body feels heavy and dull, except for your cock, which springs to attention.");
		}
	}

}
