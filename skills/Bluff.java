package skills;

import status.Unreadable;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Bluff extends Skill {

	public Bluff(Character self) {
		super("Bluff", self);
	}

	@Override
	public boolean requirements() {
		return self.has(Trait.pokerface)&&self.getPure(Attribute.Cunning)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.pokerface)&&user.getPure(Attribute.Cunning)>=9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&self.canSpend(20);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = Global.random(25);
		if(self.human()){
			c.write(self,deal(m,Result.normal,target));
		}
		else{
			c.write(self,receive(m,Result.normal,target));
		}
		self.spendMojo(20);
		self.add(new Unreadable(self),c);
		self.heal(m,c);
		self.calm(25-m,c);
		self.emote(Emotion.confident, 30);
		self.emote(Emotion.dominant, 20);
		self.emote(Emotion.nervous,-20);
	}

	@Override
	public Skill copy(Character user) {
		return new Bluff(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You force yourself to look less tired and horny than you actually are. You even start to believe it yourself.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "Despite your best efforts, "+self.name()+" is still looking as calm and composed as ever. Either you aren't getting to her at all, or she's good at hiding it.";
	}

	@Override
	public String describe() {
		return "Regain some stamina and lower arousal. Hides current status from opponent.";
	}

}
