package skills;

import global.Global;
import characters.Character;
import characters.Anatomy;
import characters.Trait;
import combat.Combat;
import combat.Result;

public class CommandOral extends PlayerCommand {

	public CommandOral(Character self) {
		super("Force Oral", self);
	}

	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && self.pantsless()
				&& c.stance.reachBottom(self);
	}
	
	@Override
	public String describe() {
		return "Force your opponent to go down on you.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		boolean silvertounge = target.has(Trait.silvertongue);
		boolean lowStart = self.getArousal().get() < 15;
		self.pleasure((silvertounge ? 8 : 5)
				+ Global.random(10),Anatomy.genitals,c);
		self.buildMojo(30);
		boolean lowEnd = self.getArousal().get() < 15;
		
		if (self.human())
			if (lowStart)
				if (lowEnd)
					c.write(self,deal(0, Result.weak, target));
				else
					c.write(self,deal(0, Result.strong, target));
			else
				c.write(self,deal(0, Result.normal, target));		

	}

	@Override
	public Skill copy(Character user) {
		return new CommandOral(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		switch (modifier) {
		case normal:
			return target.name() + " is ecstatic at being given the privilege of"
					+ " pleasuring you and does a fairly good job at it, too. She"
					+ " sucks your hard dick powerfully while massaging your balls.";
		case strong:
			return target.name() + " seems delighted to 'help' you, and makes short work"
					+ " of taking your flaccid length into her mouth and getting it "
					+ "nice and hard.";
		case weak:
			return target.name() + " tries her very best to get you ready by running"
					+ " her tongue all over your groin, but even"
					+ " her psychically induced enthusiasm can't get you hard.";
		default:
			return "<<This should not be displayed, please inform The"
			+ " Silver Bard: CommandOral-deal>>";
		}
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The"
				+ " Silver Bard: CommandOral-receive>>";
	}

}
