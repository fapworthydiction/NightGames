package skills;

import status.Shield;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class Barrier extends Skill {

	public Barrier(Character self) {
		super("Barrier", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Arcane)>=18;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&self.canSpend(3);
	}

	@Override
	public String describe() {
		return "Creates a magical barrier to protect you from physical damage: 3 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(3);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.add(new Shield(self,self.get(Attribute.Arcane)/2),c);
	}

	@Override
	public Skill copy(Character user) {
		return new Barrier(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You conjure a simple magic barrier around yourself, reducing physical damage. Unfortunately, it will do nothing against a gentle caress.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" holds a hand in front of her and you see a magical barrier appear briefly, before it becomes invisible.";
	}

}
