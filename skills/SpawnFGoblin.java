package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import pet.FGoblin;
import pet.Slime;

public class SpawnFGoblin extends Skill {

	public SpawnFGoblin(Character self) {
		super("Summon Fetish Goblin", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Fetish)>=6;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish)>=6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&self.pet==null&&self.getArousal().get()>=25;
	}

	@Override
	public String describe() {
		return "Summons a hermaphroditic goblin embodying multiple fetishes: Arousal at least 25";
	}

	@Override
	public void resolve(Combat c, Character target) {
		int power = 3+self.bonusPetPower()+(self.get(Attribute.Fetish)/10);
		int ac = 2+self.bonusPetEvasion()+(self.get(Attribute.Fetish)/10);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.pet=new FGoblin(self,power,ac);
	}

	@Override
	public Skill copy(Character user) {
		return new SpawnFGoblin(user);
	}

	@Override
	public Tactics type() {
		return Tactics.summoning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return String.format("You channel all the fetishes in your twisted libido into a single form. The creature is about 4 feet tall and has a shapely female body covered "
				+ "with bondage gear. Her face is completely obscured by a latex mask, but her big tits and her crotch are completely exposed. She has a large cock, "
				+ "which looks ready to burst if it wasn't tightly bound at the base. Past her heavy sack, you can see sex toys sticking out of both her pussy and ass.");
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return String.format("%s shivers and moans as she sinks into her darkest fantasies. Something dangerous is coming. Sure enough a short feminine figure in bondage gear appears "
				+ "before you. Her face is completely obscured by a latex mask, but her big tits and her crotch are completely exposed. She has a large cock, "
				+ "which looks ready to burst if it wasn't tightly bound at the base. Past her heavy sack, you can see sex toys sticking out of both her pussy and ass.", self.name());
	}

}
