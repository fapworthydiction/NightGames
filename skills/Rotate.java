package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import stance.*;

public class Rotate extends Skill {
    public Rotate(Character self) {
        super("Rotate", self);
    }

    @Override
    public boolean requirements() {
        return true;
    }

    @Override
    public boolean requirements(Character user) {
        return true;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct() && c.stance.dom(self) &&
                (c.stance.en == Stance.missionary || c.stance.en == Stance.doggy ||
                        c.stance.en == Stance.cowgirl || c.stance.en == Stance.reversecowgirl ||
                        c.stance.en == Stance.anal || c.stance.en == Stance.analm);
    }

    @Override
    public String describe() {
        return "Move your partner to a different sexual position.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        switch(c.stance.en){
            case missionary:
                if(self.human()){
                    c.write(self,deal(0, Result.normal, target));
                }
                c.stance = new Doggy(self,target);
                break;
            case doggy:
                if(self.human()){
                    c.write(self,deal(0, Result.strong, target));
                }
                c.stance = new Missionary(self,target);
                break;
            case anal:
                if(self.human()){
                    c.write(self,deal(0, Result.strong, target));
                }else if(target.human()){
                    c.write(self,receive(0, Result.anal, this.self));
                }
                c.stance = new AnalM(self,target);
                break;
            case analm:
                if(self.human()){
                    c.write(self,deal(0, Result.normal, target));
                }else if(target.human()){
                    c.write(self,receive(0, Result.critical, this.self));
                }
                c.stance = new Anal(self,target);
                break;
            case cowgirl:
                if(target.human()){
                    c.write(self,receive(0, Result.normal, this.self));
                }
                c.stance = new ReverseCowgirl(self,target);
                break;
            case reversecowgirl:
                if(target.human()){
                    c.write(self,receive(0, Result.strong, this.self));
                }
                c.stance = new Cowgirl(self,target);
                break;
        }
    }

    @Override
    public Skill copy(Character user) {
        return new Rotate(user);
    }

    @Override
    public Tactics type() {
        return Tactics.positioning;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        switch(modifier){
            case strong:
                return "You flip "+target.name()+" onto her back and continue fucking her.";
            default:
                return "You turn "+target.name()+" onto her hands and knees to fuck her from behind.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        switch(modifier){
            case anal:
                return self.name()+" flips you onto your back and continues to peg you.";
            case critical:
                return self.name()+" turns you onto your hands and knees so she can peg you from behind.";
            case strong:
                return self.name()+" turns around to face you.";
            default:
                return self.name()+" turns to face your feet.";
        }
    }
}
