package skills;

import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Piston extends Skill {

	public Piston(Character self) {
		super("Piston", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=18;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.dom(self)&&c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = self.getSexPleasure(3, Attribute.Seduction)+target.get(Attribute.Perception);
		Anatomy inside;
		if(c.stance.en==Stance.anal){
			if(self.human()){
				if(c.stance.behind(self)){
					c.write(self,deal(0,Result.anal,target));
				}
				else{
					c.write(self,deal(0,Result.upgrade,target));
				}			
			}
			else if(target.human()){
				if(c.stance.behind(self)){
					c.write(self,receive(0,Result.anal,target));
				}
				else{
					c.write(self,receive(0,Result.upgrade,target));
				}
			}
			inside = Anatomy.ass;
		}
		else{
			if(self.human()){
				c.write(self,deal(m,Result.normal,target));
			}
			else if(target.human()){
				c.write(self,receive(m,Result.normal,target));
			}
			inside = Anatomy.genitals;
		}
		m += self.getMojo().get()/10;
		if(self.has(Trait.strapped)){
			m += +(self.get(Attribute.Science)/2);
			m = self.bonusProficiency(Anatomy.toy, m);
		}else{
			m = self.bonusProficiency(Anatomy.genitals, m);
		}
		if(self.getPure(Attribute.Professional) >= 11){
			if(self.has(Trait.sexuallyflexible)){
				self.add(new ProfessionalMomentum(self,self.get(Attribute.Professional)*5),c);
			}else{
				self.add(new ProfMod("Sexual Momentum",self,Anatomy.genitals,self.get(Attribute.Professional)*5),c);
			}
		}
		target.pleasure(m,inside,Result.finisher,c);
		target.pleasure(m,inside,c);
		int r = target.getSexPleasure(3, Attribute.Seduction);
		r += target.bonusRecoilPleasure(r);
		if(self.has(Trait.experienced)){
			r = r/2;
		}

		self.pleasure(r,Anatomy.genitals,Result.finisher,c);
		self.buildMojo(20);
		c.stance.setPace(2);
	}

	@Override
	public Skill copy(Character user) {
		return new Piston(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.anal||modifier == Result.upgrade){
			return "You pound "+target.name()+" in the ass. She whimpers in pleasure and can barely summon the strength to hold herself off the floor.";
		}
		else{
			return "You rapidly pound your dick into "+target.name()+"'s pussy. Her pleasure filled cries are proof that you're having an effect, but you're feeling it " +
					"as much as she is.";
		}			
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.anal){
			return self.name()+" relentlessly pegs you in the ass as you groan and try to endure the sensation.";
		}
		else if(modifier == Result.upgrade){
			return self.name()+" pistons into you while pushing your shoulders on the ground. While her Strap-On stimulates your prostate, "
					+self.name()+"'s tits are shaking above your head.";
		}
		else{
			return self.name()+" enthusiastically moves her hips, bouncing on your cock. Her intense movements relentlessly drive you both toward orgasm.";
		}
	}

	@Override
	public String describe() {
		return "Fucks opponent without holding back. Very effective, but dangerous";
	}

}
