package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Masochistic;
import status.OrderedStrip;
import status.Stsflag;

public class MastersOrder extends Skill {

	public MastersOrder(Character self) {
		super("Master's Order", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Discipline)>=6;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline)>=6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&self.is(Stsflag.composed)&&
				target.canAct()&&!target.nude()&&
				self.canSpend(20)&&!target.is(Stsflag.orderedstrip);
	}

	@Override
	public String describe() {
		return "Order your opponent to undress for you or be punished: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.spendMojo(20);
		target.add(new OrderedStrip(target),c);
	}

	@Override
	public Skill copy(Character user) {
		return new MastersOrder(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You catch "+target.name()+"'s gaze with your own and move toward her menacingly. You briefly glance toward her apparel, then meet her gaze once more, mentioning that she seems to be wearing far too much clothing right now. She'll have to deal with soon, or else face the consequences of her disobedience.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return (self.name()+" suddenly manages to catch your gaze. She moves toward you, somehow manage to loom menacingly over you as she does so. <i>\"Now now,\"</i> "+self.name()+" says. <i>\"Don't you think you're wearing far too much for an event like this. I recommend you remedy this immediately.\"</i> She gives you a grin with just a hint of a threat behind it. You wouldn't want to disobey and make me upset with you, now would you?<p>" +
				"You get a strong feeling that if you don't strip yourself very soon, you're in deep trouble");
	}

}
