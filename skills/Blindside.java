package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Mount;
import stance.Stance;

public class Blindside extends Skill {

	public Blindside(Character self) {
		super("Blindside", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.get(Attribute.Professional) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c) && !c.stance.prone(target);
	}

	@Override
	public String describe() {
		return "Distract your opponent and take them down.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())&&self.check(Attribute.Seduction,target.knockdownDC()-self.get(Attribute.Professional))){
			if (self.human()) {
				c.write(self,deal(0,Result.normal,target));
			} else {
				c.write(self,receive(0,Result.normal,target));
			}
			c.stance = new Mount(self, target);
			self.emote(Emotion.confident, 15);
			self.emote(Emotion.dominant, 15);
			target.emote(Emotion.nervous,10);
		}else{
			if (self.human()) {
				c.write(self,deal(0,Result.miss,target));
			} else {
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Blindside(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public boolean requirements() {
		return self.get(Attribute.Professional) >= 9;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return "You try to trip "+target.name()+", but she keeps her balance.";
		}
		else{
			if(Global.random(2)==0){
				return String.format("You run your hands along %s's back and "
						+ "whisper something extremely lewd, even for the games, in her ear. "
						+ "While she is distracted, you wrap your calf behind hers and trip her on to the "
						+ "ground, landing on top of her waist. She looks up into your eyes, surprised by what happened.",
						target.name());
			}
			return String.format("You move up to %s and kiss %s strongly. "
					+ "While %s is distracted, you throw %s down and plant "
					+ "yourself on top of %s.",
			target.name(), target.pronounTarget(false), target.pronounSubject(false),
			target.pronounTarget(false), target.pronounTarget(false));
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" hooks your ankle, but you recover without falling.";
		}
		else{
			return "Seductively swaying her hips, " + self.name()
				+ " sashays over to you. "
				+ "Her eyes fix you in place as she leans in and firmly kisses you, shoving her tongue down"
				+ " your mouth. You are so absorbed in kissing back, that you only notice her ulterior motive"
				+ " once she has already swept your legs out from under you and she has landed on top of you.";
		}
	}

}
