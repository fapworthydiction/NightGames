package skills;

import status.Stsflag;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class Sacrifice extends Skill {

	public Sacrifice(Character self) {
		super("Sacrifice", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Dark)>=15;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>=15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&!c.stance.sub(self)&&self.getArousal().percent()>=70&&self.canSpend(25);
	}

	@Override
	public String describe() {
		return "Damage yourself to reduce arousal: 25 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(25);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.weaken(15+self.get(Attribute.Dark),c);
		self.calm(2*self.get(Attribute.Dark),c);
	}

	@Override
	public Skill copy(Character user) {
		return new Sacrifice(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You feed your own lifeforce and pleasure to the darkness inside you. Your legs threaten to give out, but you've regained some self control.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" pinches her nipples hard while screaming in pain. You see her stagger in exhaustion, but she seems much less aroused.";
	}

}
