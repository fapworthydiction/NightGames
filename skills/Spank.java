package skills;

import items.Toy;
import status.Shamed;
import status.Stsflag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Spank extends Skill {

	public Spank(Character self) {
		super("Spank", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless()&&!c.stance.prone(target)&&c.stance.reachBottom(self)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
        if(target.roll(this, c, accuracy()+self.tohit())) {
            int m = Global.random(self.get(Attribute.Power) / 4) + 4 + target.get(Attribute.Perception) / 2;
            int item = 0;
            if (self.has(Toy.Paddle)) {
                m *= 1.5;
                item++;
                self.buildMojo(20);
                self.emote(Emotion.dominant, 30);
            }
            if (self.has(Trait.disciplinarian) && !c.stance.penetration(self)) {
                if (self.human()) {
                    c.write(self, deal(item, Result.special, target));
                } else if (target.human()) {
                    c.write(self, receive(item, Result.special, target));
                }
                m *= 1.5;
                if (Global.random(10) >= 5 || !target.is(Stsflag.shamed) && self.canSpend(5)) {
                    target.add(new Shamed(target), c);
                    self.spendMojo(5);
                    target.emote(Emotion.angry, 10);
                    target.emote(Emotion.nervous, 15);
                }
                target.pain(m, Anatomy.genitals, c);
            } else {
                if (self.human()) {
                    c.write(self, deal(item, Result.normal, target));
                } else if (target.human()) {
                    c.write(self, receive(item, Result.normal, target));
                }
                target.pain(m, Anatomy.ass, c);
            }
            target.emote(Emotion.angry, 45);
            target.emote(Emotion.nervous, 15);
            self.buildMojo(10);
        }else{
            if (self.human()) {
                c.write(self, deal(0, Result.miss, target));
            } else if (target.human()) {
                c.write(self, receive(0, Result.miss, target));
            }
        }
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=8;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=8;
	}

	@Override
	public Skill copy(Character user) {
		return new Spank(user);
	}
	public int speed(){
		return 8;
	}
	public int accuracy(){
		return 4;
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return "You try to spank "+target.name()+", but she dodges away.";
		}
		if(modifier==Result.special){
			if(damage == 1){
                return "You swing your paddle between "+target.name()+"'s legs, hitting her sensitive genitals with a painful sounding smack.";
			}else {
				if (target.has(Trait.imagination) && Global.random(3) == 0) {
					return "You pull " + target.name() + " over your knees and begin spanking her ass while whispering in her ears about just how bad of a girl she's been, " +
							"and what you're going to have to do to her later. Suddenly, you place a hard slap on her delicate pussy, snapping her back to reality with a jolt.";
				}
				switch (Global.random(2)) {
					case 1:
						return "You bend " + target.name() + " over your knee and spank her, alternating between hitting her soft butt cheek and her sensitive pussy.";
					case 2:
						return "Pulling " + target.name() + " across your knees, you firmly spank her and tell her how bad she's been until her face turns red.";
					default:
						return "You bend " + target.name() + " over your knee and spank her, alternating between hitting her soft butt cheek and her sensitive pussy.";
				}
			}
		}
		else{
			if(damage == 1){
                return "You paddle "+target.name()+"sharply on the ass, leaving a bright red mark.";
			}else {
				switch (Global.random(3)) {
					case 1:
						return "You spank " + target.name() + " on the ass, causing a satisfying slapping sound to ring out.";
					case 2:
						return "You slap " + target.name() + "'s butt, admiring the way it shakes as your hand rebounds off it.";
					case 3:
						return "You spank " + target.name() + " on her naked butt cheek.";
					default:
						return "You spank " + target.name() + " on her naked butt cheek.";
				}
			}
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" aims a slap at your ass, but you dodge it.";
		}
		if(modifier==Result.special){
			if(damage == 1){
                return self.name() + " delivers an eye watering paddle blow to your exposed balls.";
			}else {
				if (target.get(Attribute.Submissive) >= 15 && Global.random(3) == 0) {
					return self.name() + " pulls you across her knees and rests her hand on your ass, and you instantly go limp as you realise what she's about to do - " +
							"tensing up will only make it sting more. No slap comes, though, and instead you hear a whisper in your ear: <i>\"Trying to avoid your punishment? " +
							" That won't do. Now show me your ass properly like a good boy.\"</i> Without thinking, you tense up again and push out your buttcheeks. " + self.name() + " " +
							"gives you a few hard spanks before letting you go, and your cheeks burn with shame when you realise what you just submitted to in the middle of a sex fight.";
				}
				switch (Global.random(2)) {
					case 0:
						return self.name() + " pulls you across her knees and starts spanking you, hard. Stinging pain spreads all across your ass, and every blow is accompanied " +
								"by a matching one to your pride as you are treated like nothing more than a naughty boy. After a firm slap to the testicles, " + self.name() + " releases you.";
					default:
						return self.name() + " bends you over like a misbehaving child and spanks your ass twice. The third spank aims lower and connects solidly with your ballsack, " +
								"injuring your manhood along with your pride.";
				}
			}
		}
		else{
			if (damage == 1) {
                return self.name() + " hits your ass with a loud, painful smack of her paddle.";
			}else {
					switch (Global.random(2)) {
						case 0:
							return self.name() + " slaps you on the ass, leaving a red mark and a stinging feeling.";
						default:
							return self.name() + " lands a stinging slap on your bare ass.";
					}
			}
		}
		
	}

	@Override
	public String describe() {
		return "Slap opponent on the ass";
	}
}
