package skills;

import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Trait;

import combat.Combat;
import combat.Result;

import skills.Skill;
import stance.StandingOver;
import status.Bound;
import status.ShadowFingers;

public class DarkTendrils extends Skill {

	public DarkTendrils(Character self) {
		super("Dark Tendrils", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Dark)>=12 && !self.has(Trait.cursed);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark)>=12 && !user.has(Trait.cursed);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !c.stance.sub(self)&&!c.stance.prone(self)&&!c.stance.prone(target)&&self.canAct();
	}

	@Override
	public String describe() {
		return "Summon shadowy tentacles to grab or trip your opponent: 15 Arousal";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendArousal(15);
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(Global.random(2)==1){
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.normal,target));
				}
				target.add(new Bound(target,10+self.get(Attribute.Dark),"shadows"),c);
			}
			else if(self.check(Attribute.Dark,target.knockdownDC()-self.getMojo().get())){
				if(self.human()){
					c.write(self,deal(0,Result.weak,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.weak,target));
				}
				c.stance=new StandingOver(self,target);
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.miss,target));
				}
				else if(target.human()){
					c.write(self,receive(0,Result.miss,target));
				}
			}
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
		if(self.getPure(Attribute.Dark)>=24){
			self.add(new ShadowFingers(self));
			if(self.human()){
				c.write(self,deal(0,Result.special,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.special,target));
			}
		}

	}

	@Override
	public skills.Skill copy(Character user) {
		return new DarkTendrils(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	public int accuracy(){
		return 8;
	}
	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You summon dark tentacles to hold "+target.name()+", but she twists away.";
		}
		else  if(modifier == Result.weak){
			return "You summon dark tentacles that take "+target.name()+" feet out from under her.";
		}
		else if(modifier == Result.special){
			return "With your advanced mastery of the dark arts, you add smaller tentacles to the fingers on your right hand. These should be useful.";
		}
		else{
			return "You summon a mass of shadow tendrils that entangle "+target.name()+" and pin her arms in place.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" makes a gesture and evil looking tentacles pop up around you. You dive out of the way as they try to grab you.";
		}
		else  if(modifier == Result.weak){
			return "Your shadow seems to come to life as dark tendrils wrap around your legs and bring you to the floor.";
		}
		else if(modifier == Result.special){
			return "A small mass of shadows wrap around "+self.possessive(false)+" hand, covering "+self.possessive(false)+" fingers.";
		}
		else{
			return self.name()+" summons shadowy tentacles that snare your arms and hold you in place.";
		}
	}

}
