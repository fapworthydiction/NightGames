package skills;

import status.Charmed;
import status.Enthralled;
import status.Stsflag;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class Suggestion extends Skill {

	public Suggestion(Character self) {
		super("Suggestion", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Hypnosis)>=1;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis)>=1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.behind(self)&&
				!c.stance.behind(target)&&!c.stance.sub(self)&&!target.is(Stsflag.charmed)&&!target.is(Stsflag.enthralled)&&
				self.canSpend(5);
	}

	@Override
	public String describe() {
		return "Hypnotize your opponent so she can't defend herself";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(5);
		if(!target.is(Stsflag.cynical)){
			if(self.getPure(Attribute.Hypnosis)>= 10){
				if(self.human()){
					c.write(self,deal(0,Result.strong,target));
				}else{
					c.write(self,receive(0,Result.strong,target));
				}
				target.add(new Enthralled(target,self),c);
			}else{
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}else{
					c.write(self,receive(0,Result.normal,target));
				}
				target.add(new Charmed(target),c);
			}
			
		}else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}else{
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Suggestion(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return String.format("You attempt to put %s under hypnotic suggestion, but %s doesn't appear to be affected.",target.name(),target.pronounSubject(false));
		}else{
			return String.format("You speak in a calm, rhythmic tone, lulling %s into a hypnotic trance. Her eyes seem to glaze over slightly, momentarily "
					+ "slipping under your influence.", target.name());
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" attempts to put you under hypnotic suggestion, but you manage to regain control of your consciousness.";
		}else{
			return self.name()+" speaks in a firm, but relaxing tone, attempting to put you into a trance. Obviously you wouldn't let yourself be "
					+ "hypnotized in the middle of a match, right? ...Right? ...Why were you fighting her again?";
		}
	}

}
