package skills;

import items.Component;
import items.Item;
import items.Toy;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Squeeze extends Skill {

	public Squeeze(Character self) {
		super("Squeeze Balls", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.hasBalls()&&c.stance.reachBottom(self)&&self.canAct()&&!self.has(Trait.shy);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			int m = Global.random(self.get(Attribute.Power))+target.get(Attribute.Perception)-(2*target.bottom.size());
			Result type = Result.normal;
			if(target.pantsless()){
				if(self.has(Toy.ShockGlove)&&self.has(Component.Battery,2)){
					self.consume(Component.Battery, 2);
					type = Result.special;
					m+=self.get(Attribute.Science);
				}
				else if(target.has(Trait.armored)){
					type = Result.item;
				}
			}
			else if(self.name()=="Mara" && self.getRank()>=2){
				type = Result.unique;
				m += Global.random(self.getLevel());
			}
			else{
				type = Result.weak;
			}
			if(self.human()){
				c.write(self,deal(m,type,target));
			}
			else if(target.human()){
				c.write(self,receive(m,type,target));
			}
			target.pain(m,Anatomy.genitals,c);
			if(self.has(Trait.wrassler)){
				target.calm(m/4,c);
			}
			else{
				target.calm(m/2,c);
			}
			self.buildMojo(10);
			target.emote(Emotion.angry,15);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Power)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=9;
	}

	@Override
	public Skill copy(Character user) {
		return new Squeeze(user);
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You try to grab "+target.name()+"'s balls, but she avoids it.";
		}
		else if(modifier == Result.special){
			return "You use your shock glove to deliver a painful jolt directly into "+target.name()+"'s testicles.";
		}
		else if(modifier == Result.weak){
			return "You grab the bulge in "+target.name()+"'s "+target.bottom.peek()+" and squeeze.";
		}
		else if(modifier == Result.item){
			return "You grab the bulge in "+target.name()+"'s "+target.bottom.peek()+", but find it solidly protected.";
		}
		else{
			return "You manage to grab "+target.name()+"'s balls and squeeze them hard. You feel a twinge of empathy when she cries out in pain, but you maintain your grip.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" grabs at your balls, but misses.";
		}
		else if(modifier == Result.unique){
			return "Mara deftly slips her hand down the front of your pants and tightly wraps her small fingers around your testicles, "
					+ "holding your balls firmly in her gloved hand.  You are thankful that she isn't squeezing them very hard, "
					+ "but you suddenly feel a debilitating shock of electricity being delivered directly to your ballsack.";
		}
		else if(modifier == Result.special){
			return self.name()+" grabs your naked balls roughly in her gloved hand. A painful jolt of electricity shoots through your groin, sapping your will to fight.";
		}
		else if(modifier == Result.weak){
			return self.name()+" grabs your balls through your "+target.bottom.peek()+" and squeezes hard.";
		}
		else if(modifier == Result.item){
			return self.name()+" grabs your crotch through your "+target.bottom.peek()+", but you can barely feel it.";
		}
		else{
			switch(Global.random(2)){
				case 1:
					return self.name()+" grabs both your balls roughly in her hand, crushing them in her grip.  "
							+ "You feel your voice momentarily raise an octave as your testicles are squeezed together.";
				default:
					return self.name()+" reaches between your legs and grabs your exposed balls. "
					+ "You writhe in pain as she pulls and squeezes them.";
			}
			
		}
	}
	public String toString(){
		if(self.has(Toy.ShockGlove)){
			return "Shock Balls";
		}
		else{
			return name;
		}
	}

	@Override
	public String describe() {
		return "Grab opponent's groin, deals more damage if naked";
	}
}
