package skills;

import status.Beastform;
import status.Nimble;
import status.Stsflag;
import characters.Attribute;
import characters.Character;

import combat.Combat;
import combat.Result;

public class BeastTF extends Skill {

	public BeastTF(Character self) {
		super("Beast Transform", self);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Animism)>=18;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Animism)>=18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)&&self.canSpend(30)&&self.is(Stsflag.feral)&&!self.is(Stsflag.beastform);
	}

	@Override
	public String describe() {
		return "Unleash your animal side: 30 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		self.add(new Beastform(self),c);
	}

	@Override
	public Skill copy(Character user) {
		return new BeastTF(user);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You physically manifest your animal spirit, growing fur, ears, and a tail. "
				+ "You feel the bestial power warp your muscles, making you faster and stronger.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" lets out an animal roar and her bestial features become much more pronounced. Fur rapidly grows on her limbs, "
				+ "her teeth sharpen, and she crouches like a predator.";
	}

}
