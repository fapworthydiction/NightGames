package skills;

import global.Flag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class FondleBreasts extends Skill {

	public FondleBreasts(Character self) {
		super("Fondle Breasts", self);
	}

	@Override
	public boolean requirements() {
		return !self.has(Trait.cursed);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.reachTop(self)&&target.hasBreasts()&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(target.topless()){
				int m = Global.random(3+self.get(Attribute.Seduction)/2)+target.get(Attribute.Perception)/2;
				if(self.human()){
					c.write(self,deal(m,Result.normal,target));
				}
				target.pleasure(m,Anatomy.chest,c);
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Angel")){
					c.offerImage("Breast Fondle_nude.jpg", "Art by AimlessArt");
				}
			}
			else{
				int m = Math.max(Global.random(2+self.get(Attribute.Seduction)/2)-target.top.size()+2,1);
				if(self.human()){
					c.write(self,deal(m,Result.normal,target));
				}
				target.pleasure(m,Anatomy.chest,Result.foreplay,c);
				if(!Global.checkFlag(Flag.exactimages)||target.name().startsWith("Angel")){
					c.offerImage("Breast Fondle_cloth.jpg", "Art by AimlessArt");
				}
			}
			self.buildMojo(10);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new FondleBreasts(user);
	}
	public int speed(){
		return 6;
	}
	public int accuracy(){
		return 7;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			if(Global.random(2)==0){
				return "You reach towards "+target.name()+"'s chest, but come up short.";
			}else{
				return "You grope at "+target.name()+"'s breasts, but miss.";

			}
		}
		else if(target.top.isEmpty()){
			switch(Global.random(3)){
			case 2:
				return "You take your time and gently caress both of "+target.name()+"'s tender breasts with your fingers. You occasionally brush her nipple to make her coo.";
			case 1:
				return "You massage "+target.name()+"'s soft breasts and pinch her nipples, causing her to moan with desire.";
			default:
				return "You slowly tease " + target.name() + "'s exposed chest, running your hands all over her boobs.";
			}
		}
		else{
			switch(Global.random(3)){
			case 2:
				return "You play with " + target.name() + "'s breasts, trying to find her nipples through her " + target.top.peek().getName() + ".";
			case 1:
				return "You tease " + target.name() + "'s boobs through her " + target.top.peek().getName() + ".";
			default:
				return "You massage "+target.name()+"'s breasts over her "+target.top.peek().getName()+".";
			}
			
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String describe() {
		return "Grope your opponent's breasts. More effective if she's topless";
	}
}
