package Comments;
import characters.Character;
import combat.Combat;
import status.Stsflag;

public class StatusRequirement implements CustomRequirement {

	private final Stsflag flag;

	public StatusRequirement(String flag) {
		this.flag = Stsflag.valueOf(flag);
	}

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null || flag == null)
			return false;

		return self.getStatus(flag) != null;
	}

}