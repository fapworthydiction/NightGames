package characters;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;

import javax.swing.Icon;

import Comments.CommentGroup;
import Comments.CommentSituation;
import daytime.Daytime;
import global.Match;
import skills.Skill;

import combat.Combat;
import combat.Result;

import actions.Action;
import actions.Movement;
import areas.Area;


public interface Personality extends Serializable{
	public Skill act(HashSet<Skill> available,Combat c);
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match);
	public NPC getCharacter();
	public void rest(int time, Daytime day);
	public String bbLiner();
	public String nakedLiner();
	public String stunLiner();
	public String winningLiner();
	public String taunt();
	public String victory(Combat c,Result flag);
	public String defeat(Combat c,Result flag);
	public String victory3p(Combat c,Character target, Character assist);
	public String intervene3p(Combat c,Character target, Character assist);
	public String resist3p(Combat c,Character target, Character assist);
	public String watched(Combat c, Character target, Character viewer);
	public String describe();
	public String draw(Combat c,Result flag);
	public boolean fightFlight(Character opponent);
	public boolean attack(Character opponent);
	public void ding();
	public String startBattle(Character opponent);
	public boolean fit();
	public String night();
	public void advance(int Rank);
	public boolean checkMood(Emotion mood, int value);
	public float moodWeight(Emotion mood);
	public String image();
	public void pickFeat();
	public CommentGroup getComments();
	public int getCostumeSet();
	public void declareGrudge(Character opponent, Combat c);
	public void resetOutfit();
}
