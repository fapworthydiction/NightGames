package characters;

public enum Emotion {
	confident	("She looks fairly confident and eager to fight."),
	angry		("She looks pissed."),
	nervous		("She seems a little unsure of herself."),
	desperate	("She knows she's losing and has started to become desperate."),
	horny		("She seems much more interested in fucking than fighting."),
	dominant	("She has you right where she wants you. She's just playing with you now."),
	hunting		(""),
	retreating	(""),
	sneaking	(""),
	bored		(""),
	;
	private String description;
	
	private Emotion(String description){
		this.description=description;
	}
	public Emotion inverse(){
		switch(this){
		case angry:
			return horny;
		case nervous:
			return confident;
		case desperate:
			return dominant;
		case horny:
			return angry;
		case dominant:
			return desperate;
		default:
			return nervous;
		}
	}
	public String describe(){
		return description;
	}
}
