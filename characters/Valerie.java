package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Result;
import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Trophy;
import skills.Skill;
import stance.Stance;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Valerie implements Personality {
    public NPC character;

    public Valerie(){
        character = new NPC("Valerie",ID.VALERIE,10,this);
        character.outfit[Character.OUTFITTOP].add(Clothing.bra);
        character.outfit[Character.OUTFITTOP].add(Clothing.noblevest);
        character.outfit[Character.OUTFITTOP].add(Clothing.noblecloak);
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.panties);
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.tdresspants);
        character.change(Modifier.normal);
        character.add(Trait.female);
        character.add(Trait.composure);
        character.add(Trait.cropexpert);
        character.add(Trait.buttslut);
        character.setUnderwear(Trophy.ValerieTrophy);
        character.plan = Emotion.hunting;
        character.mood = Emotion.confident;
        character.strategy.put(Emotion.sneaking, 1);
        this.character.set(Attribute.Power, 7);
        this.character.set(Attribute.Discipline, 4);
        this.character.set(Attribute.Seduction, 8);
        this.character.getStamina().setMax(80);
        this.character.getArousal().setMax(120);
        this.character.getMojo().setMax(50);
    }

    @Override
    public Skill act(HashSet<Skill> available, Combat c) {
        HashSet<Skill> mandatory = new HashSet<Skill>();
        HashSet<Skill> tactic = new HashSet<Skill>();
        Skill chosen;
        for(Skill a:available){
            if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Ass Fuck")){
                mandatory.add(a);
            }
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
        }
        if(!mandatory.isEmpty()){
            Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
            return actions[Global.random(actions.length)];
        }
        ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
        if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
            chosen = character.prioritizeNew(priority,c);
        }
        else{
            chosen = character.prioritize(priority);
        }
        if(chosen==null){
            tactic=available;
            Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
            return actions[Global.random(actions.length)];
        }
        else{
            return chosen;
        }
    }

    @Override
    public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
        Action proposed = character.parseMoves(available, radar, match);
        return proposed;
    }

    @Override
    public NPC getCharacter() {
        return character;
    }

    @Override
    public void rest(int time, Daytime day) {
        String loc;
        ArrayList<String> available = new ArrayList<String>();
        available.add("Hardware Store");
        available.add("Black Market");
        available.add("XXX Store");
        available.add("Bookstore");
        if(character.rank>0){
            available.add("Workshop");
        }
        available.add("Play Video Games");
        for(int i=0;i<time-4;i++){
            loc = available.get(Global.random(available.size()));
            day.visit(loc, character, Global.random(character.money));
            if(loc!="Exercise"&&loc!="Browse Porn Sites"){
                available.remove(loc);
            }
        }
        character.visit(4);
    }

    private boolean composed(){
        return character.is(Stsflag.composed);
    }
    @Override
    public String bbLiner() {
        if(composed()){
            return "";
        }else{
            return "";
        }
    }

    @Override
    public String nakedLiner() {
        if(composed()){
            return "Valerie’s expression shows just a hint of frustration with herself. <i>\"I suppose I need to work harder on shoring up my defenses, if you could strip me this easily.\"</i>";
        }else{
            return " Valerie’s eyes widen as she tries momentarily to cover herself. <i>\"Damn it, you’re going to pay for this.\"</i>";
        }
    }

    @Override
    public String stunLiner() {
        if(composed()){
            return "Valerie’s eyes fall closed for a moment. <i>\"Touché…\"</i> she breathes out.";
        }else{
            return "Valerie grimaces from pain, but she’s too out of it to fight back. <i>\"Urgh… that was a bullshit tactic and you know it…\"</i>";
        }
    }

    @Override
    public String winningLiner() {
        return null;
    }

    @Override
    public String taunt() {
        if(composed()){
            return "Valerie’s eyes narrow, almost imperceptibly. <i>\"I know you can do better than this.\"</i>";
        }else{
            return "Valerie grins widely, and you can see a touch of madness in her eyes for just a moment. <i>\"Aha! Now it’s your turn, you bastard!\"</i>";
        }
    }

    @Override
    public String victory(Combat c, Result flag) {
        if(composed()){
            if(c.stance.sub(character) && c.stance.en == Stance.anal || c.stance.en == Stance.analm){
                return "<i>\"Pardon me... "+Roster.get(ID.PLAYER).name()+"?\"</i> Valerie says, her words and tone of voice completely incongruous with the fact that her ass is currently stuffed with your cock as you sodomize her ever closer to orgasm. Somehow she's managed to hold herself together, though you suspect that the feeling of your cock inside her ass is slowly driving her crazy.\n" +
                        "You don't let Valerie's words stop you, continuing to slide your dick rhythmically in and out of her ass even as you ask her what she wants to say.\n" +
                        "Valerie turns her head to the side, looking up pleadingly at you with one eye. <i>\"I was wondering if... you might let me concede?\"</i>\n" +
                        "As far as you're aware, there aren't any rules in the Games that allow a competitor to concede, so that isn't an option. Even if she could concede, does Valerie think you'd let her out of this so easily? She's more than earned a thorough assfucking tonight, and that's exactly what you're going to give her, like it or not. Well... it's probably more like <i>\"like it or love it,\"</i> to be honest.\n" +
                        "Valerie lets out a sigh. <i>\"I suppose... mm... that's fair,\"</i> she says, her words interrupted by a moan. Her eyelids fall closed, and she rests her head on the ground. <i>\"Very well then, I'm very nearly ready to climax. Afterwards though, please do... ah! ...remember that I did try to offer an alternative, and don't... mm... judge me too harshly.\"</i>\n" +
                        "You nod at Valerie, and you can't help but smile at her. You have a pretty good idea of what's coming if you make her come from anal sex, and that's half the fun of this. You take a moment to adjust your position, reaching one hand out to grab Valerie's hair while the other reaches around her to play with her pussy. You pull your dick almost all the way out of her ass, and then suddenly pull back on her hair as you slam your dick into her colon.\n" +
                        "Valerie cries out, but you aren't finished with her yet. You slide your fingers within her pussy, searching around until you find her clit. You tease it gently as you pull your cock back out of her ass, and then just as you slam it in again, you pinch in hard on her clit. The twin assault is what does it for Valerie, and she screams out in a mix of agony and ecstasy as an orgasm floods through her body. Her muscles spasm, threatening to shake you off, but you hold on, not letting her ass escape from your cock just yet.\n" +
                        "You keep thrusting your cock in and out of Valerie's ass, working toward your own orgasm now that you've solidly won the match. <i>\"Oh fuck... fuck...\"</i> Valerie says, her composure finally broken from the force of her orgasm. <i>\"Oh fuck yes. Fuck my ass! Come on, fuck my ass, "+Roster.get(ID.PLAYER).name()+"!\"</i> she cries out.\n" +
                        "Far from being exhausted by her orgasm, Valerie seems energized right now. She places both of her hands on the ground and pushes herself back up, giving herself better leverage to pound her ass back against your cock with every thrust. With Valerie assisting in her own assfucking, it barely takes any time at all before the soft depths of her asshole squeeze a powerful orgasm out of your cock, loosing your seed within her.\n" +
                        "You let out a sigh and begin to relax, but Valerie doesn't seem to be done with you yet. <i>\"Wha- why the fuck are you stopping?\"</i> she says, turning her head so she can glare back at you. <i>\"I believe I said, 'fuck my ass, "+Roster.get(ID.PLAYER).name()+".' Does my ass look thoroughly fucked to you?\"</i>\n" +
                        "Valerie's ass might not be thoroughly fucked yet, but your dick certainly is. It's going to be hard going to give her much more of a fucking with it right now.\n" +
                        "Valerie growls at you. <i>\"Does that sound like my problem? I'm giving you thirty seconds to start fucking my ass again, or else I'll make you regret it. I don't care if your limp dick can't handle it; use my strap-on if you have to!\"</i>\n" +
                        "You growl back at Valerie, and you feel your dick twitch inside of her ass. She's the loser here, where does she get off making demands like that? You're going to have to teach her a lesson for that. Luckily enough, this is just enough motivation to get your dick hard again, and you're perfectly set to punish her with a fierce assfucking.\n" +
                        "Valerie smiles as she feels your dick hardening inside you, but you don't allow to treat this as a victory. You're about to punish her asshole right now, and she'd better act like a good, remorseful loser. This command earns you a soft yelp from Valerie. A blush fills her cheeks, and she nods at you. <i>\"Y-yes, sir... sorry sir... Please punish my asshole as you see fit, sir.\"</i>\n" +
                        "That's what you wanted to hear. With Valerie properly obedient, you begin her punishment, sliding your cock as deep into her asshole as possible. That isn't all she's in for, though. You force one of your fingers in beside your cock, squeezing her asshole open just a bit wider.\n" +
                        "Valerie lets out a strangled moan as you pull open her ass. <i>\"Ahhh... fuuuuck!\"</i> she says. <i>\"Ah... ugh... that's it... please punish me just like that... I deserve to have my ass torn apart as the spoils of your victory!\"</i>\n" +
                        "As encouraging as it is to hear that from her, you don't want to actually tear Valerie's ass, so you settle for just the one finger, taking care as you slowly slide your cock in and out of her ass once more. It soon becomes apparent that your finger is just getting in the way though, so you remove it and place your hand on Valerie's hip for better leverage as you resume punishing her asshole. You shift your hips as you thrust into her, making sure to hit her colon at every possible angle, leaving no inch of it untouched by your cock.\n" +
                        "<i>\"Yes... ah fuck yes.. Mmm... that's it... come on, more... harder...\"</i> Valerie says, seemingly forgetting once again that she's supposed to be being punished right now. You swat your hand against her ass in an attempted reminder of this fact, but she just lets out a pleased yelp and wiggles her ass excitedly in response. With a sigh, you give in and allow her to openly enjoy this.\n" +
                        "You're about ready to cum again, and there's no way you'll be able to go a third round. You have to make sure Valerie gets off as well. You place both of your hands on her asscheeks, squeezing down hard on them, then pulling them in toward your cock so she can feel it even more inside her ass. A few more strokes of your cock like this seems to do the trick for her; Valerie's arms give out beneath her and she collapses to the ground, screaming out as an anal orgasm overtakes her body.\n" +
                        "Unsurprisingly, Valerie's spasming colon does you in as well, squeezing out another orgasm from your cock. You manage to hold onto her until you've completely emptied yourself into her ass, but in the end you're so spent that you find yourself collapsed on top of her.\n" +
                        "After a long while in which you and Valerie try to catch your breath, Valerie is the first to speak up. <i>\"I... um... apologize for that, "+Roster.get(ID.PLAYER).name()+". I truly do hope you don't judge me for my shameful behavior just then...\"</i>\n" +
                        "You shake your head. You knew what you were getting into, and you enjoyed yourself.\n" +
                        "Valerie smiles softly. <i>\"Very well. I suppose I enjoyed myself too. Um, if you wouldn't mind removing yourself from my rear, though? It's rather hard to try to compose myself for my next match while still impaled like this.\"</i>\n";
            }
            if(flag == Result.intercourse){
                return "You find yourself enraptured by the sight of Valerie as she slowly rides you. The only signs of arousal from her are her hardened nipples and the wetness of her pussy, making it impossible to tell how close she might be to orgasm. One thing's for sure though: You're rapidly approaching the edge, and watching her beautiful body fluidly ride your cock is only getting you there faster.<br>" +
                        "Valerie leans forward a bit, arching her back as if in ecstacy. A seductive expression spreads across her face, and she gazes down at you. \"You can touch me if you want,\" she says. She lets her hips fall down, and with your cock fully enveloped by her pussy, she rotates her hips around, letting her softness massage your cock from all angles. \"No need to be shy.\"<br>" +
                        "It's only as Valerie says this that you realize that you've been so enchanted by her that you'd forgotten to fight back. Is she just toying with you by bringing this up, taunting you with the fact that her victory is all but inevitable, and she isn't even afraid of you doing anything to stop her? That doesn't seem to be her style, though. Perhaps a more likely explanation is that she's trying to be a good sport by reminding you not to give up until it's all over.<br>" +
                        "You shake your head, trying to clear it of its Valerie-induced haze and focus on the match. You might be close to your limit, but you haven't reached it yet. If you don't get your cock out of Valerie's pussy soon though, it'll all be over. You raise your hands, preparing to shove Valerie off of you, when Valerie's hands shoot down and grasp your wrists.<br>" +
                        "\"I said you could touch,\" Valerie says, a firm tone in her voice now. \"I didn't mean 'push.' I'll punish you if I have to, but I'll give you one more chance to be nice and obedient.\" Valerie stops moving her hips for a moment, giving you just a small reprieve. \"I think we both know what's going to happen next. We can do it the easy way or the hard way, but the choice is yours. I'm going to let go of your wrists. If you want to be a good boy, you'll put them on my breasts and start catching up with bringing me to my own orgasms. Do anything else, and I'll make you cum faster than you can say 'Yes, Mistress,' and then proceed to punish you. Understood?\"<br>" +
                        "You quickly try to assess your options here. Fight back, then lose the instant Valerie touches your cock again, and get punished. Or obey, lose the instant she resumes riding you, get... well, probably not punished. Slowly, you nod at Valerie. The choice is clear when you think about it in those terms.<br>" +
                        "\"Good,\" Valerie says. She slowly lets go of your wrists, then leans forward and places her hands on your shoulders. \"Now get to work. Your Mistress is going to be quite upset with you if you don't manage to make her cum before you go soft.\"<br>" +
                        "You nod at Valerie again, and bring your hands to her breasts as she commanded. Just as you cup the twin mounds, Valerie lets out a hum and resumes riding you. The sight of her pleasured face, the feel of her breasts in your hands, and the feeling of her soft pussy enveloping your cock all combine to push you over the edge. Your cock bursts from the pleasure of it all, spurting its seed inside Valerie.<br>" +
                        "You don't have much of a chance to enjoy your orgasm though. Now that Valerie has already won, she isn't likely to fight back, so you take a bit more initiative. You get up the strength to sit up, pushing Valerie back so she's sitting on your lap. Wrapping one arm around her, you bring your other downward, seeking out her slit so your fingers can work their magic and bring her the rest of the way.<br>" +
                        "Valerie does nothing to fight back as you give her her due reward as the victor. She simply wraps her arms around her back to hold on as you bring her to her own orgasm. It's still hard to judge how close she is, so you do everything you can to push her over the edge quickly. Your fingers find her clit and rapidly rub it, and you lean your face into her neck and suck in hard on her pulse point. Finally, when your hand trails down Valerie's back and to her ass, giving it a solid squeeze, her body begins to gently shudder.<br>" +
                        "\"Ahhh...\" Valerie lets out a sigh as her body tenses up, and then slowly relaxes. \"Hmm...\" she says, shifting her hips around for a moment. \"Well, your cock feels a bit soft, but I think that was a good enough orgasm that I can forgive you.\"<br>" +
                        "You let out a breath you didn't even realize you'd been holding. It seems you've managed to avoid punishment, at least this time.<br>";
            }else{
                return "Almost in the blink of an eye, Valerie vanishes from sight. All you were able to spot was a blur as she moved past you, so you turn in that direction. It seems she was a step ahead of you though, as she'd circled all the way around to what's now your back. One of Valerie's arms crosses your chest, holding you in place while her free hand reaches down to your cock. You grimace as you realize you're too close to the edge to have any chance of coming back from this. Defeated, you allow Valerie to finish you off with a surprisingly delicate handjob.<br>" +
                        "Valerie continues pumping your cock until it’s completely empty, her free hand moving around to stroke your back as she does so. <i>\"That was certainly a good fight,\"</i> she says, leaning in to plant a quick kiss on your shoulder.<br>" +
                        "It certainly could have been better, but there’s nothing you can do about that now. You’ll just have to do better next time. In the meantime, there’s still the matter of Valerie’s arousal to deal with. <br>" +
                        "<i>\"Ah…\"</i> Valerie says. She lets go of your cock and moves around in front of you, shaking her head slightly. <i>\"That’s alright. You’re under no obligation to help me get off.\"</i><br>" +
                        "It’s not a matter of obligation. You want to return the favor. And besides, it wouldn’t be fair to ask her to go into her next fight still so aroused, after all.<br>" +
                        "The corner of Valerie’s lips curls up into just a bit of a smile. <i>\"You’re not going to take ‘no’ for an answer, are you?\"</i><br>" +
                        "Well, you would if she were serious, but you can tell from that question that she doesn’t really want you to give up on your offer.<br>" +
                        "Valerie’s mouth fully smiles now, and a light blush tinges her cheeks. <i>\"Okay, fine, you win,\"</i> she says. She carefully sits down on the ground in front of you and then lays back. <i>\"Have your way with me.\"</i><br>" +
                        "An invitation like that almost makes it feel like you won this match, but you remind yourself that you should be focusing on Valerie’s pleasure here. You gaze into her eyes as you lean down, ready to finish the job and give her the orgasm a winner deserves.<br>";
            }

        }else{
            if(c.stance.sub(character) && c.stance.en == Stance.anal || c.stance.en == Stance.analm){
                return "Valerie's orgasm manages to catch you by surprise. Perhaps it shouldn't have been so surprising, given how much she'd been moaning up to this moment and begging you to fuck her ass harder, but you were still expecting her to need a bit of clitoral stimulation to get over the edge. Nope. She managed to reach an orgasm solely from the sensations of your cock stroking in and out of her ass.\n" +
                        "<i>\"Mm... ah, fuck that was good!\"</i> Valerie says as she comes down from her orgasm. She crosses her arms on the ground and rests the side of her head on them, looking up at you with one eye. She playfully wiggles your ass at you and says, <i>\"Okay, your turn. Wanna cum in my ass, or wanna cum with me in your ass?\"</i> Valerie winks as she says this, though she does seem to be quite serious with the suggestion.\n" +
                        "You chuckle at Valerie and shake your head. She's going to have to beat you if she wants to take your ass; you're not giving it up this easily. Since she lost the match, you're going to celebrate your victory by having some fun with her ass.\n" +
                        "Valerie moans a bit as you resume thrusting your cock. <i>\"Mmm... well, I certainly can't complain about that. Means I get to feel your lovely cock in my ass a bit longer...\"</i>\n" +
                        "You grin at Valerie. You could probably get yourself off soon if you want, but you decide to slow down a bit so that the two of you can enjoy this for just a bit longer. While you slowly slide your dick inside of Valerie's ass, you ask her why, if she enjoys this so much, she always makes such a show of resisting it.\n" +
                        "<i>\"Well, that's what the Games are about, right?\"</i> Valerie says. She closes her eyes and adjusts her position, pushing herself up and arching her back for a moment. <i>\"Ahh... that's it. Just like that, "+Roster.get(ID.PLAYER).name()+". Mm... anyway, now that it's over, there's no point resisting. I can just enjoy myself. And I'm well past the point of caring about keeping up my composed demeanor now, too. Much easier to just let you see how much I'm enjoying myself.\"</i>\n" +
                        "You have to agree with that. It's quite nice to see Valerie in this state. Often she can get angry, demanding, or a bit out of it when she loses her composure, but right now it's just led her to letting her horny and sensual side show through in full. It's all the more arousing watching her enjoy herself as you fuck her ass - or perhaps at this point, <i>\"make love to her ass\"</i> would be the better way to phrase it, with how you two are going at it. The Games might explain why she resists letting you have your way with her, but with how much she enjoys this, it does make you wonder why she verbally protests.\n" +
                        "<i>\"Mmm...\"</i> Valerie replies, and her cheeks fill up with a slight blush. <i>\"Promise not to tell anyone?\"</i> she says.\n" +
                        "You pump your cock in and out of her ass one more time before replying, agreeing not to say a word.\n" +
                        "Valerie hums for a moment, and she begins to move her body in pace with your cock, pushing herself back against it with each thrust into her ass. <i>\"Well, I have a bit of a fetish for being forced into things. Not seriously being forced, mind you, but it's fun to fantasize about. And the Games are absolutely perfect for that. Where else could I experience having someone like you trying to have his way with me, but with the goal of forcing me to be the one to cum first? Even right now... just thinking about how you've forced me to let you have your way with my ass, forcing me into a screaming orgasm... haa... Ah fuck, you'd better be about ready to cum, "+Roster.get(ID.PLAYER).name()+", because I don't know if I can hold myself back any longer.\"</i>\n" +
                        "You grin at Valerie. You're just about ready, you tell her, picking up the pace as you thrust into her ass. Deciding to play into her fantasy a bit, you remind her that she's about to pay the price for losing in the Games. She'd better be ready for another mind-shattering anal orgasm, because it's coming whether she likes it or not.\n" +
                        "<i>\"Oh fuck yeah... That's the way... force me to cum, "+Roster.get(ID.PLAYER).name()+",\"</i> Valerie says. She continues to pound her ass back against your cock, a series of moans leaving her mouth as she gets ever closer to her own climax.\n" +
                        "As you reach the point of no return, you give Valerie a stern command to cum. This command does the trick for her. She lets out a scream and her ass clamps down on your cock as a second orgasm rips through her body, squeezing an orgasm out of you as well. You spray your seed within her ass, letting out a contented sigh as you fill Valerie's ass at long last.\n" +
                        "Valerie collapses onto the ground in the aftermath of her orgasm, finally pulling her ass off of your cock. You take a breath, then give her a moment to rest as you gather your supplies together and get ready for the next match. Once you're ready, you glance back down at Valerie to make sure she's alright, and you notice that a bright red blush has filled her cheeks.\n" +
                        "Valerie glances up at you and gives you a weak smile, then quickly glances away, her cheeks filling with even more of a blush. Now that her senses are coming back to her, she's probably embarrassed by what she'd confessed to you minutes earlier.\n" +
                        "You smile softly at Valerie, then kneel down beside her. You lean down and place a quick kiss in her cheek, then move over to whisper in her ear. You challenge her to do her best next time if she's serious about wanting to take your ass. At these words, a grin crosses Valerie's face, and she nods at you. You might be in trouble the next time you two meet, but right now it feels worth it.\n";
            }
            if(flag == Result.intercourse){
                return "Valerie's pussy squeezes in on your cock like a vise. She lets out a grunt as she pounds down on you, obviously expending a great deal of effort and concentration to squeeze in on you this hard. You really should be taking advantage of her distraction to try to get her off of you, if only this didn't feel so damn good. Even during the Games, it isn't often that a partner does this for you.<br>" +
                        "You need to do something though. Valerie's expression is mostly horny, but the small bit of anger in it makes you fear for what could happen if you lose - well, let's face it, when you lose. Getting her to cum quickly afterward might be the only way to keep her from getting any wild ideas for what to do after she wins.<br>" +
                        "Your loss might be inevitable, but at least now you have a goal in mind. You wrap a hand around Valerie's head, pulling her down to you and giving her a fierce kiss. Your other hand moves to her back and then slides down to her ass, alternately squeezing it, spanking it, and pinching it. This somehow seems to drive Valerie into even more of a frenzy, and you soon find her tongue in your mouth, exploring every inch of it and making it perfectly clear who's in charge here.<br>" +
                        "You're unable to keep up with Valerie in the kiss, and you soon find yourself giving in to her. But as soon as you do, she pulls away. Her mouth finds its way to your ear, and after chewing on your earlobe for a moment, she says, \"Cum for me, "+Roster.get(ID.PLAYER).name()+". And don't you fucking dare take your hand off my ass, got it?\"<br>" +
                        "You weakly agree to this. You don't really have much choice in the first matter, anyway. As for the second, if she likes one hand on her ass, then two should be even better. You move your other hand to her ass as well and begin to squeeze and massage her asscheeks in turn, which earns you a pleased growl from Valerie.<br>" +
                        "\"Now that's how you show initiative...\" Valerie says. As soon as she finishes speaking, she captures your lips in another kiss. Her teeth gently chew on your lower lip for a moment, and then she pulls back just enough to say, \"And three, two, one... cum!\"<br>" +
                        "You're not sure if Valerie was predicting it or causing it, but as soon as her countdown ends, an orgasm overtakes you. At least, it starts to. With how tight her pussy is squeezing in on your cock, your cum is painfully held back.<br>" +
                        "Noticing your distress, Valerie chuckles at you. \"Sorry, but I had to teach you a bit of a lesson for how you treated me during this match. I'll let you cum. All you have to do is say, 'Please let me cum, Mistress Valerie.'\"<br>" +
                        "You wince in pain. As embarrassing as that is, you did lose this match, and that does merit a bit of a hit to your dignity. Reluctantly, you repeat the words, trying your best to make your plea seem earnest.<br>" +
                        "\"Good boy,\" Valerie says. Her pussy suddenly loosens up, letting your cum finally break free. With how long it was held back, you can only imagine how hard it's shooting out inside of her. She certainly seems to be enjoying the feeling though, as she says, \"Fucking finally...\"<br>" +
                        "Valerie begins to ride you fanatically, reminding you more of a wild beast than the restrained girl she normally is. She soon screams out as an orgasm overtakes her as well, her voice getting more and more high-pitched until she finally reaches her peak. Her motions freeze, and then her body begins to quiver, and finally she collapses on top of you.<br>" +
                        "Valerie doesn't move for a while, but that's fine with you. You need some time to recover yourself. Finally, you jokingly ask her if that was good for her.<br>" +
                        "\"You fucking bet it was,\" Valerie says. She leans in to place a quick kiss on your cheek. \"Thanks for fighting until the end, by the way. It's so much better that way.\" Her voice is a bit softer now, and it somehow brings a smile to your face. You might not have won, but at least you didn't leave her unsatisfied.<br>";
            }else{
                return "When Valerie manages to grasp your cock in her hand, you know the battle is lost. You’ve held back as long as you can, but there’s no way you can stop her from getting you off now, and she isn’t close enough to the edge that you can even hope to bring this match to a draw. With a sigh, you let yourself relax and simply enjoy the feeling as Valerie furiously strokes your cock and pushes you over the edge, spurting your seed on the ground in front of you.<br>" +
                        "Valerie lets go of your cock as soon as you begin shooting your seed. <i>\"Yes!\"</i> she cries out, pumping her arm in victory. <i>\"Oh, you have no idea how much I’ve been wanting this.\"</i><br>" +
                        "You grimace a bit, wondering just what Valerie might have in mind for you now. This probably wouldn’t go so poorly for you if you hadn’t pushed her so far during the match, but you had, and now you had to face the price for that.<br>" +
                        "<i>\"Let’s see… what can we do?\"</i> Valerie says, moving in front of you and looking your body and up down. She licks her lips thirstily as she appraises your body. <i>\"Damn… It’s going to be hard getting you hard again before someone else stumbles upon us. I guess your mouth will have to do. On your knees.\"</i> Valerie blinks as she says this, and then quickly says, <i>\"Please.\"</i><br>" +
                        "Valerie seems to be getting a hold of herself again, but perhaps a good orgasm will delay that a bit longer. Obediently, you get onto your knees in front of her, deciding to see just how long you can keep her in this state. A hungry gaze appears on Valerie’s face, and you can see her breathing heavily. She takes a tentative step toward you, getting just close enough that you can reach out your tongue and lick it across her slit.<br>" +
                        "<i>\"Ahh!\"</i> Valerie lets out an uncharacteristically high-pitched yelp as you lick her. You keep up the assault, soon turning her yelps into moans of pleasure. You reach your hands around to grasp her ass, squeezing in on it as you eat her out. <i>\"That’s it… just like that…\"</i> Valerie says, placing her hands on the back of your head and holding you in place, giving you no choice but to finish her off, bringing her to a screaming orgasm.<br>" +
                        "Valerie drops to the ground, her eyes falling closed as she smiles and hums to herself for a moment, basking in the afterglow of her orgasm. Before too long, you see the normal Valerie take hold again, her emotions carefully hidden behind a pleasant facade once more.<br>" +
                        "\"Mmm... well done,\" she says. She leans over to plant a kiss on your cheek. \"I'm looking forward to our next match.\"<br>";
            }
        }
    }

    @Override
    public String defeat(Combat c, Result flag) {
        if(composed()){
            if(flag == Result.intercourse){
                return "If it weren't for the fact that Valerie's completely given up on fighting back, you'd have little idea how close to orgasm she is right now. She seems to be focusing all her efforts on holding back her own climax, likely hoping that in trying to fuck her to orgasm, you'd end up getting yourself off first. It isn't the best plan, considering that in your current doggy-style position, you can easily pull out and finish her off by hand if you get worried about cumming too soon, but now isn't the best time to offer her strategy tips.<br>" +
                        "You resist the urge to make a pun about offering Valerie a different kind of tip, partly because you never said anything about \"strategy tips\" out loud, and partly because a pun that bad would make her dry up faster than a... fast-drying-thing. What dries fast? Something in a desert probably, but what? It would have to be wet to begin with, but also prone to drying quickly...<br>" +
                        "\"Ahh... ahh... ahhhhh!\" A moan breaks free from Valerie's mouth, pulling you out of your thoughts. Her arms collapse beneath her, and she slumps to the ground as an orgasm begins to overtake her.<br>" +
                        "You hold onto her hips, feeling them shudder as Valerie's orgasm washes through her body, and you slow your thrusting just a bit. You don't want to over-stimulate her, but you do want to provide her a little more pleasure to prolong the orgasm just a bit. It seems to work, and it takes a long while before Valerie's quivering ceases completely. When it does, she lets out a soft hum.<br>" +
                        "\"Mm... good job. Go ahead. Your turn now,\" Valerie says. She still seems to be a bit hazy from the orgasm, which makes her voice a bit cuter than usual. Expressing this to her, you spot a soft blush filling her cheek, but she doesn't say anything further. Instead, she simply wiggles her ass for a moment, enticing you to finish yourself off inside of her.<br>" +
                        "You happily resume thrusting into Valerie. Now that the match is over, you can simply let yourself enjoy this moment. For a while, you simply stroke your cock in and out of Valerie's pussy, but a mischievous mood strikes you, perhaps brought on by that adorable blush you'd gotten from Valerie just a minute ago. You begin to talk to her, asking if she tries to be that cute on purpose, or if it just comes naturally to her.<br>" +
                        "You spot another blush filling Valerie's cheeks, but she quickly turns her head away, burying it in her arms and denying you the spoils of your tease. That won't do at all, you tell her. She lost, so the least she can do for you is show you how cute she is when she blushes.<br>" +
                        "Valerie whimpers at this. At first you think she's going to resist, but she eventually turns her head, looking up at you with one big eye, pleading for mercy. That look is what does it for you. It's just too unbelievably adorable. With how long you've been inside Valerie, the warmth that look causes to rush through your chest is all you need to push you over the edge. You push your cock as deep into Valerie's pussy as you can and let yourself go.<br>" +
                        "You're left with a smile on your face in the aftermath of your orgasm. As you pull out of Valerie, you look down at her and apologize if your teasing embarrassed her too much.<br>" +
                        "Valerie's face is still filled with a blush, but once she turns around and pushes herself up to her knees, she shakes her head. \"No... It's... it's fine,\" she says. She has trouble meeting your gaze for a moment, but eventually she forces herself to do so. \"Maybe even a bit nice. I'm glad you find me cute... at times like that.\"<br>" +
                        "Well, the cuteness peaks when she blushes like that, you tell her, but she's always quite attractive to you.<br>" +
                        "A smile splits across Valerie's face as you say this. She briefly tries to hide it, but then shakes her head. Letting herself smile genuinely at you for a moment, she says, \"Thank you. I'll be sure to remember you said that the next time I defeat you.\"<br>" +
                        "That sounds good to you, but you hope she won't forget it next time you win, either.<br>";
            }else{
                return "Valerie is past the point of actively fighting back. At this point, she’s just fighting to prolong her orgasm as long as possible. But with your fingers buried in her slit, stroking her rapidly and circling around to find all her sensitive spots, she’s fighting a losing battle. Finally, she opens her mouth, letting out a soft moan as her body begins to shudder.<br>" +
                        "Not satisfied with just a moan, you pick up the pace with your fingers, seeing if you can coax a bit more of a reaction out of her. Valerie grimaces, but the tactic seems to be working. Her mouth opens to moan again, the pitch of her voice slowly rising as a second wave hits her. Deciding she’s had enough, you slow down the pace of your fingers and hold her with your other arm as her body relaxes.<br>" +
                        "Valerie breathes heavily for a moment. Her eyes flutter open, looking up at you, and she gives you a warm smile. <i>\"You really are quite good at this. It makes me almost want to lose to you again, if this is what I have to look forward to.\"</i><br>" +
                        "While you wouldn’t mind at all if she did that, there’s still the matter of your arousal right now to take care of. You’d hardly want to go into the next match near the breaking point, after all.<br>" +
                        "<i>\"Oh!\"</i> Valerie says, her eyes widening. <i>\"Of course. That’s the price of losing, after all.\"</i> Valerie pushes herself up and moves around to your side. She brings her hand out to grasp your cock, but then she pauses, looking up at you with a slight blush in her cheeks. <i>\"But, um, don’t get me wrong… I’m not doing this just because you won. This is also thanks for the delightful orgasm you just gave me. I don’t know if I’ll be able to do quite as good of a job, but I’ll see what I can do.\"</i><br>" +
                        "You’re sure she’s just being modest, and as she begins to stroke your cock, this conclusion only becomes stronger. Oh yes, she can do quite a good job as well.<br>";
            }

        }else{
            if(flag == Result.intercourse){
                return "It's a miracle you've managed to keep Valerie from throwing you off of her as long as you have. Somehow, you've managed to ride her, pumping your dick in and out of her pussy, through all her thrashing, shoving, kicking, and even hair-pulling, until finally she seems to be at the cusp of orgasm. Her moans get progressively louder, to the point that she's now practically howling in pleasure.<br>" +
                        "All you need now is to finish her off. You were using your hands to hold down Valerie's shoulders, but you feel like she's close enough now that you can spare a hand and use it on her clit instead. As it turns out though, you felt wrong. No sooner do you remove your hand from Valerie's shoulder than does she use the extra freedom to roll you over onto your back.<br>" +
                        "You curse as you try to regain control. Valerie is close enough to orgasm that you aren't worried about losing the match, but this is certainly going to make things harder. You reach up to try to push Valerie off of you, but she leans back and your hands miss her. You're about to try again when your eyes fully process the sight before you, causing you to pause.<br>" +
                        "\"Fuck, that's it...\" Valerie says. She leans back as she continues to ride you, her eyes closed. While a moment ago she was using her hands to hold you down, now they're on her breasts, squeezing them as she seems to be focused entirely on her own pleasure.<br>" +
                        "It would seem that Valerie's far enough gone that's she forgotten entirely about the match, and only wants to get off right now. You can certainly work with that. You place your hands on Valerie's stomach and ask her if it wouldn't feel better if it were your hands on her breasts rather than hers.<br>" +
                        "\"Mm... I like the way you think,\" Valerie says. Her eyes open so she can look down at you, but her hands don't move from her breasts. \"But I think you can find an even more fun use for your hands if you put your mind to it. Surprise me and I might reward you.\"<br>" +
                        "Yep, she's definitely forgotten about the Games. Smiling at Valerie, you decide to take full advantage of her absent-mindedness by doing exactly as she suggests. You slide one of your hands downward, seeking out her slit and slowly digging your thumb into her folds. Meanwhile, you reach your other hand around to grasp her ass.<br>" +
                        "Valerie lets out a deep moan. She pauses in her motions for a moment as the pleasure of your touch seems to distract her, but then picks up again. \"Hmm... That's nice... but not surprising. Come on, you can do better than- AHH!\"<br>" +
                        "There was a reason you placed one of your hands on Valerie's ass, and it wasn't just to give it a good squeeze. Well, that was part of the reason, but not the full reason. The rest of the reason was so that you could exploit one of Valerie's weaknesses if needed. Just as Valerie began to taunt you, you slipped your fingers into the crack of her ass and began to circle them around her asshole, prompting a high-pitched moan from Valerie and causing her cheeks to fill with a blush.<br>" +
                        "With Valerie momentarily stunned, you double down on your assault. You push your hips up, impaling Valerie on your cock. You press down hard on her clit and simultaneously begin to wiggle your fingertip inside Valerie's asshole.<br>" +
                        "\"Oh fuck, I can't believe you...\" Valerie trails off, her mouth hanging open in stun. Somehow, she hasn't achieved orgasm just yet, but one more wiggle of your fingertip inside her ass is all it takes to break her completely. Valerie whimpers, her eyes roll into the back of her head, and her body jerks on top of you as an orgasm overtakes her. Her jerking motions manage to help your finger slip all the way into her asshole, which seems to push her to a second peak.<br>" +
                        "Valerie's body shudders uncontrollably, and her face fills with a deep blush. You wiggle your finger a bit more within Valerie's ass, and she whimpers in defeat from the pleasurable assault. Finally, she slumps forward, breathing heavily.<br>" +
                        "\"Oh wow...\" she says. \"You really are a naughty boy, aren't you? ...Don't worry though, I love you for it...\"<br>" +
                        "You blink as \"I love you\" slips out of Valerie's mouth, but you don't dwell on it right now. She's still in the afterglow of her orgasm, and not in her right mind besides.<br>" +
                        "\"Hmm...\" Valerie says. She seems to have recovered just a little now, and she pushes herself up so she can meet your gaze. \"Well, that was certainly a pleasant surprise, so you've earned yourself a reward. Maybe you're due for a little surprise of your own...\"<br>" +
                        "You smile at Valerie. Maybe later, but right now all you want is for her to finish riding you to orgasm. Getting to watch her as she does that is more than enough of a reward.<br>" +
                        "Valerie's face splits into a grin. \"You flatterer, you. Okay, you win.\" Valerie leans down and places a quick kiss on your lips, then leans back up. She holds your gaze as she slowly begins to ride you once more. A bit surprisingly given her current state of mind, she takes it slowly. You don't complain though; she's doing a great job right now making your cock feel like it's in heaven.<br>" +
                        "In little time, you feel your own orgasm approaching. Closing your eyes, you surrender yourself to the sensations. Pleasure shoots through your cock as your cum breaks free, shooting into Valerie's heavenly pussy at long last.<br>" +
                        "Before you can open your eyes, you feel your lips claimed by Valerie in a soft kiss. After a sweet, heavenly kiss, she pulls back and whispers, \"Time to get ready for the next match, unfortunately. Try to find me again though. I'll be sure to have a fun little surprise in store for you.\"<br>" +
                        "You might have to watch out for that. Or maybe look forward to that.<br>";
            }else{
                return "Valerie thrashes wildly in your grasp, but you somehow manage to hold onto her. The closer you get her to the edge, the wilder she seems to get. With how Valerie normally acts, you would have never expected she’d have such a beast inside of her, but now that it’s been unleashed, you can’t help but want to see how far you can push her.<br>" +
                        "<i>\"Oh fuck!\"</i> Valerie cries out as your fingers graze over her clit. She tries to move away from you to keep you from doing that again, but you manage to roll her over onto her back and get on top of her, facing her pussy. Before she can struggle away, you place your fingers over her slit and begin to rub her furiously, pressing down hard on clit. <i>\"No! Fuck fuck fuck fuck fuuuuuck!\"</i> Valerie cries out, trying in vain to get out from under you.<br>" +
                        "Within moments, Valerie’s struggles to escape turn into thrashing from the orgasm that overtakes her body. You stroke her clit for a few seconds longer, then switch to giving it a good pinch, which prompts a fierce cry to break free from Valerie’s throat. At last, her body seems to relax beneath you, so you gently let go of her clit and turn around to face her.<br>" +
                        "Valerie’s face is flushed and sticky with sweat, with a few strands of hair getting in the way of her eyes. She’s too out of it to brush them away now, though. She can’t even seem to focus her eyes, even. Slowly, she seems to gather herself together, but the red on her cheeks only deepened. <i>\"Wow…\"</i> she says. <i>\"I really… let myself go there, didn’t I?\"</i><br>" +
                        "Yes, she certainly did, and you loved every minute you could see her like that.<br>" +
                        "Valerie’s cheeks get even redder as you tell her this. <i>\"Thanks…\"</i> she says. After a deep breath, Valerie turns her head to look up at you. <i>\"Well… this match is yours. Any special requests for your prize?\"</i><br>" +
                        "There are any number of things you’d like to ask of Valerie, but after the display she just gave you, you don’t feel like you need much more right now. Besides, she looks to be too worn out at the moment to do much of her own accord. So, you tell her to simply relax and let you do the work.<br>" +
                        "Valerie nods, laying back and letting herself rest for a moment. You pull back from her, slowly stroking your cock as you decide what you’d like most to do with her. Her clit is probably still sore from the orgasm she just experienced, so perhaps you can play with her breasts for a bit. They aren’t quite big enough to give you a full boob-job, but you can still have some fun with them.<br>" +
                        "You position yourself over Valerie’s chest and kneel down, placing your shaft between her breasts. Catching on to what you’re doing, Valerie places her hands to either side of her breasts and squeezes them in. You begin to slowly slide your cock forward and back between the two mounds, reaching your hands down to gently play with them a bit as you do so. The position is a bit hard on your knees, but with how aroused you are from the match, it isn’t long before you let loose, spurting your seed over Valerie’s neck and chin.<br>" +
                        "<i>\"Mmm…\"</i> Valerie hums for a moment, smiling in satisfaction. She scoops up a bit of your seed with her finger and then licks it off. <i>\"I was kind of hoping you’d cum in my mouth, but I suppose this will do. I’ll just have to clean myself up the hard way before the next match,\"</i> she says with a wink. It would seem that she hasn’t quite composed herself again yet, but you’re certainly not complaining.<br>";
            }
        }
    }

    @Override
    public String victory3p(Combat c, Character target, Character assist) {
        if(target.human()){
            return "<i>\"Well,\"</i> Valerie says, taking a moment to brush a strand of hair out of her face as she takes a moment to regain her composure. <i>\"This wasn’t exactly how I’d planned to end this match, but I do hope you won’t be mad if I take advantage of it.\"</i> She steps toward you, leaning in to plant a quick kiss on your cheek before she gets down to business. <i>\"Don’t worry,\"</i> she whispers as her hands slowly slide down your body, toward your cock. <i>\"You’re going to love this.\"</i>";
        }else{
            return "Valerie’s eyes widen with glee as she sees "+target.name()+" displayed before her, ready for her to finish off. An uncharacteristic madness is in her eyes, and you almost feel sorry for "+target.name()+". Valerie can get quite wild when she’s been pushed too far, and it seems that "+target.name()+" has made the mistake of doing just that.<br>" +
                    "<i>\"What a delightful turn of events,\"</i> Valerie says, stepping slowly toward "+target.name()+" and you. <i>\"How shall we do this?\"</i> She places a finger beneath "+target.name()+"’s chin and lifts her face up so she can make eye contact. <i>\"So many ways to get you off, but that would hardly be a suitable punishment. Maybe we should just tie you up and then "+Roster.get(ID.PLAYER).name()+" and I can have some fun our own while you can do nothing but watch?\"</i><br>" +
                    "While that idea is certainly tempting, it would rather go against the spirit of the Games.<br>" +
                    "<i>\"The Games?\"</i> Valerie says, blinking in confusion for a moment. <i>\"Oh, right.\"</i> She looks back at "+target.name()+" and purses her lips. <i>\"Alright, I’ve decided. Hard and fast, no mercy, and if you don’t scream loudly enough, we’re doing it again and again until you do.\"</i><br>" +
                    "Okay, you definitely feel sorry for "+target.name()+". But not nearly enough to switch teams and help her out. She knew what she was getting in for, and now she has to pay the price<br>";
        }
    }

    @Override
    public String intervene3p(Combat c, Character target, Character assist) {
        if(target.human()){
            return "Your fight against "+assist.name()+" has barely gotten started when you’re distracted by a tap on the shoulder. You tense up, knowing this can’t be good news, and you take a step back to see who’s arrived, without letting "+assist.name()+" out of your sight. Valerie appears in your field of vision, and she gives you an apologetic smile. <i>\"I hope you don’t take offense to this, "+Roster.get(ID.PLAYER).name()+",\"</i> she says, <i>\"but I’ll be helping "+assist.name()+" out with this one. Don’t worry, I’ll make sure not to let her go too hard on you.\"</i><br>" +
                    "You glance back and forth between "+assist.name()+" and Valerie, gauging your chances at taking them both on, or perhaps even convincing "+target.name()+" to ally with you against Valerie, but neither option has good odds. You make a last-ditch attempt to lunge at "+assist.name()+", in the hope that you might somehow be able to take her out quickly, but Valerie manages to restrain you before you can land a single attack.<br>" +
                    "<i>\"I can’t blame you for not giving up, but I’m afraid this ends now,\"</i> Valerie says as she pulls your arms back and restrains them behind your back. <i>\"Just relax and let yourself enjoy it, okay?\"</i>";
        }else{
            return "Your fight against "+target.name()+" has barely gotten started when you’re distracted by a tap on the shoulder. You tense up, knowing this can’t be good news, and you take a step back to see who’s arrived, without letting "+target.name()+" out of your sight. Valerie appears in your field of vision, and after making eye contact with you for a moment, her eyes widen and she gives you an apologetic smile. <i>\"Oh!\"</i> she says. <i>\"Did I give you the impression I was here to help out "+target.name()+" against you? Sorry, you can relax; it’s the other way around.\"</i><br>" +
                    "You glance back at "+target.name()+" as you hear this, just in time to see her expression change from glee to dread. She makes the mistake of spending too long trying to figure out what to do next, and Valerie uses this time to dash in capture "+target.name()+" in a hold before she can flee.<br>" +
                    "<i>\"You can finish her off,\"</i> Valerie says, turning herself and "+target.name()+" to face you. <i>\"Just don’t be too mean. We did have the advantage after all, so it wouldn’t be fair to punish her too much.\"</i><br>";
        }
    }

    @Override
    public String resist3p(Combat c, Character target, Character assist) {
        return null;
    }

    @Override
    public String watched(Combat c, Character target, Character viewer) {
        return "As you catch sight of Valerie and "+target.name()+" in heat of combat, you glance back and forth between them as you try to decide who you should help out. But as you glance at Valerie, you spot a telltale twitching in her expression which is anything but good news. However this fight goes, getting involved right now could spell trouble for you, so you decide to stay out and leave the two girls to decide it between themselves.<br>" +
                "<i>\"Damn it, why do you have to keep being so fucking cheap?\"</i> Valerie cries out, momentarily catching "+target.name()+" off-guard with her sudden shift in demeanor. This doesn’t last long though, as "+target.name()+" quickly moves in to capitalize on Valerie’s weakened guard. In a scramble of arms and legs, she manages to strip Valerie of her remaining clothes and pin the girl down.<br>" +
                "It looks like Valerie is in for a quick loss, but she’s pissed off enough from being treated like this that she spits in "+target.name()+"’s face, using the momentary shock to roll the two of them over and gain the superior position. She’s still at the disadvantage of being naked while "+target.name()+" is still fully-clothed, but she quickly gets to work at fixing that, tearing at her opponent’s clothes and literally ripping them off her body with a frightening amount of strength.<br>" +
                "And then suddenly, Valerie lets "+target.name()+" go, pulling back and spinning around to pick up something from her supplies in a fluid motion. As "+target.name()+" pushes herself up, Valerie spins back around and a strong Crack! pierces the air, following by a howl of mixed pleasure and pain from "+target.name()+". As Valerie slows down enough for you to figure out what was going on, you spot her riding crop in her hand and "+target.name()+" cradling her genitals in pain. A direct hit.<br>" +
                "Valerie assumes a fencing stance with her riding crop extended, waving it tauntingly in "+target.name()+"’s face. "+target.name()+" tries to snatch it away, but the pain must be dulling her reflexes, as the motion is slow and easily dodged. As she lets "+target.name()+" move past her, Valerie swings the riding crop down, striking "+target.name()+"’s ass with another fierce blow and earning herself another yelp from her opponent.<br>" +
                "<i>\"Come on, you’ve got to try harder than that,\"</i> Valerie says, smirking. <i>\"You’re making me feel like a bullfighter here.\"</i> This taunt enrages "+target.name()+" into spinning around and trying to tackle Valerie, but the result is exactly the same as before: Valerie easily dodges and uses her crop to strike "+target.name()+"’s rear once more for her trouble. <i>\"That’s two. Care to go for three, or have you learned your lesson?\"</i><br>" +
                "Apparently not, as "+target.name()+" charges at Valerie once more. But this time, she anticipates Valerie’s dodge and shifts at the last moment. Unfortunately for her though, Valerie was still a step ahead of her and is able to twist her body back the other way, causing "+target.name()+" to trip over her leg and fall to the ground, exhausted.<br>" +
                "<i>\"Well, I’d thank you for wearing yourself out for me, if I were in a thankful mood,\"</i> Valerie says. She places a foot on "+target.name()+"’s chest, holding her down as she reaches over to her supplies to take out some rope. <i>\"Instead, I’ll show you exactly what happens to people who don’t know when to quit.\"</i><br>" +
                ""+target.name()+" puts up some resistance, but she’s too out-of-sorts by this point to prevent Valerie from tying her up. She soon finds herself face-down, with her arms tied behind her back and her legs tied together. In this position, her ass is exposed to your view, and you can see the pair of welts on it from Valerie’s riding crop.<br>" +
                "<i>\"So, the current count is three,\"</i> Valerie says. <i>\"One to the front, two to the back. Since you’re not putting up much of a fight right now, I think I’ll set myself a challenge. Ten. On exactly my tenth strike, no sooner, no later, I will make you cum. If I fail… well, I don’t plan to cede the match to you, but I suppose maybe I’ll apologize for the pain. If I succeed, then I’ll be expecting an apology from you for.. for…\"</i> Valerie furrows her brow. <i>\"Fuck it, I don’t care. You’re apologizing to me, and I don’t care what for.\"</i><br>" +
                ""+target.name()+" lets out a whimper, but it does no good. Valerie’s riding crop strikes down hard on her ass.<br>" +
                "<i>\"Four.\"</i><br>" +
                ""+target.name()+" screams in pain, but this is cut off by another quick pair of strikes, without nearly enough time between them for her to recover.<br>" +
                "<i>\"Five and six. Are you feeling it yet? My special touch? I can tell you’re getting a bit worked up now.\"</i><br>" +
                "From what you can see of "+target.name()+"’s cheek, it’s gotten rather red now, so it would seem that Valerie’s guess is correct. She’s not purely going for pain right now, but wielding her crop in just the right way to make sure even someone who isn’t normally a masochist gets a perverse degree of pleasure out of it. <br>" +
                "<i>\"Seven!\"</i> Valerie had struck "+target.name()+"’s ass again while you were lost in thought, but the new red welt is evidence of the force of her strike. Valerie then kicks "+target.name()+"’s side, rolling her over awkwardly onto her back. <i>\"Time to finish this.\"</i> Valerie says, pointing the crop down at "+target.name()+"’s breasts now.<br>" +
                ""+target.name()+"’s eyes widen and she begins to try to say something, but she can’t manage to get it out before Valerie’s crop comes down and strikes her right nipple. Valerie continues in her motion, sweeping the crop around in a grand circle, and then swings it down to hit "+target.name()+"’s left nipple as well. <br>" +
                "<i>\"Eight and nine.\"</i> Valerie says, barely audible over "+target.name()+"’s screams of mixed pain and pleasure. She points her riding crop at "+target.name()+"’s crotch now. <i>\"And now, the coup de grâce.\"</i><br>" +
                ""+target.name()+" manages to open her eyes even wider, and she has time to plead for mercy as Valerie makes a show of readying her final swing. It’s all for naught though as Valerie’s arm comes down, her riding crop cutting through the air with incredible speed until it strikes its target. "+target.name()+" lets out a powerful scream as an orgasm is painfully torn from her body, and you can spot a few tears in her eyes as she slumps into the ground.<br>" +
                ""+target.name()+" doesn’t have long to relax though, as Valerie’s riding crop lifts up her chin. <i>\"Now, what do you say?\"</i><br>" +
                ""+target.name()+" grumbles in response to this, but she’s probably too afraid of what Valerie might do if she refuses, so she mutters out a feeble apology.<br>" +
                "Valerie smiles. <i>\"Good. I’d untie you, but… I don’t particularly want to. Good luck getting yourself untied before "+Roster.get(ID.PLAYER).name()+" decides to take advantage of you. I’m sure he’s all too eager to help himself right now after the show you put on for him.\"</i><br>" +
                "Well, you might need to work on your hiding skills, but at least Valerie doesn’t seem to be mad at you for not helping out.<br>";
    }

    @Override
    public String describe() {
        if(character.is(Stsflag.composed)){
            return "Valerie appears calm and collected, not letting a single emotion slip through her facade. Even in the middle of the Games, she’s somehow managed to ensure that not a single one of her hairs is out of place. Her eyes examine you closely as she looks for an opening, and her muscles are tensed and ready to strike at any sign of weakness you show.";
        }else{
            return "Madness has taken over Valerie’s eyes in a way that shouldn’t be humanly possible. With the stress of combat keeping her from keeping her composure, she’s given up on holding herself back. She’s more vulnerable now, but also more dangerous.";
        }
    }

    @Override
    public String draw(Combat c, Result flag) {
        if(composed()){
            if(flag == Result.intercourse){
                return "The change in Valerie's expression is subtle. If your face weren't currently just inches away from hers, your eyes locked on hers, you probably never would have noticed the change. If you had to describe the change, you'd have to say that Valerie's expression just lost a bit of its detachment. Her guard wasn't broken; rather, she voluntarily dropped it, just a bit.<br>" +
                        "All of which makes now the perfect time to capitalize on her vulnerability and use it to win the match. At least, that's what the clinical, detached part of your brain tells you. Unfortunately for it, the rest of your brain isn't listening to it right now. The change in Valerie's expression might have been subtle by most people's standards, but for her it speaks volumes. She probably has just as good a sense of the match as you do, if not better, and she certainly knows that the two of you are on pace to orgasm at the same time. And from that one little change, she's communicating that she's happy with this outcome.<br>" +
                        "And to be honest, you're happy with it as well. You smile softly at Valerie, wordlessly agreeing with her to work toward a draw in the final moments of this match. In response to your smile, Valerie's expression softens and opens up to you just a bit more, and she leans up to plant a soft kiss on your lips.<br>" +
                        "The kiss is slow, tender, and not at all befitting this stage of a match, but it feels perfect right now. You pause the motions of your hips for a moment as you simply enjoy the kiss. When you do start moving again, it's at a much more measured pace than before. You're not so much trying to fuck Valerie to orgasm anymore as you are trying to bring you and your lover to a simultaneous climax.<br>" +
                        "A soft moan escapes from Valerie's throat, causing her lips to vibrate gently against yours. You moan in turn, letting her feel your own lips vibrate for a moment before you pull away. You gaze down into Valerie's eyes, trying to gauge how close to orgasm she is. Before you can get a good sense of her state of mind though, you feel her hands wrap around the back of your head and pull you back in for another kiss, even deeper than the last.<br>" +
                        "She must be close, but you can't easily tell quite how close she is. And with you rapidly approaching the edge, this could easily turn into a loss if you aren't careful. You can't bear to pull away from the kiss or stop thrusting into her, but that won't stop you from other methods of getting her off.<br>" +
                        "Using just your left hand to hold yourself up, you place your right hand on Valerie's stomach, then slowly trail it downwards. Valerie moans into your kiss, encouraging you, so you speed up a bit and bring your hand to her slit. You continue thrusting your dick into her as you dig in with your fingers, seeking out Valerie's clit to help push her over the edge.<br>" +
                        "You can feel your own orgasm fast approaching, but with Valerie's moans getting just a bit more high-pitched, you know she has to be just as close. It's time then. Your fingers find Valerie's clit, and you push in hard on it as you let yourself go. With one last thrust deep into her pussy, you explode into her.<br>" +
                        "Valerie's pussy clenches in on your dick, squeezing you empty as her body explodes into an orgasm of its own. Her moans are strangled silent for a moment as her body tenses up, until finally she lets out a low moan as she falls back to earth. All throughout, her lips never part from yours.<br>" +
                        "And even afterward, you don't want to be the one to break the kiss first. And neither does Valerie, apparently. The two of you share a kiss for what must be minutes, enjoying the closeness this post-climax moment has brought you. Eventually though, you do have to get back to the games. Reluctantly, you pull away from Valerie - silently letting her win the \"who can keep kissing the longest\" competition - and try to get your mind back in the right state for the rest of the Games tonight.<br>";
            }else{
                return "The fate of this match is balancing on the edge of knife. One wrong move by you could spell defeat, but the same could be said for Valerie. All it would take is one good assault on her pussy to push her over edge, but there’s no easy way to get at her without making yourself vulnerable as well, and she could push you over the edge just as easily.<br>" +
                        "The stalemate seems to last minutes, though in reality it’s probably only a few seconds. But it’s a few seconds without either of you making a move aside from cautiously circling the other, which makes it clear that both of you are of the same frame of mind. There’s no way to win without heavy risk of losing first.<br>" +
                        "<i>\"Stalemate,\"</i> Valerie says.<br>" +
                        "You nod, agreeing with her conclusion.<br>" +
                        "<i>\"That’s not my conclusion,\"</i> Valerie says. <i>\"I’m declaring it. Three turns cycling back to the same position. By chess rules, the match is officially a stalemate.\"</i><br>" +
                        "You aren’t exactly playing chess here, but that doesn’t make her wrong about the state of the match. A draw counts as a win for both of you in the tally at the end of the night, but you both lose your clothes. At this point, agreeing to a draw is probably a safer choice than risking a loss if you try to steal a win. But just agreeing to it would leave you both too horny for the next match.<br>" +
                        "Valerie pauses in her step. <i>\"Good point,\"</i> she says. Her eyes meet yours as she says, <i>\"So we finish together. Agreed?\"</i><br>" +
                        "You nod. A voice in the back of your mind proposes that you could use this opportunity to get Valerie off first while she’s unsuspecting, but you quickly silence it. You can’t imagine Valerie going back on her word like that, and you won’t allow yourself to be the villain in this scenario.<br>" +
                        "No sooner do you drop your defensive stance than does Valerie spring forward. You worry briefly that you’ve been had, but she doesn’t reach for your cock. Instead, she wraps her hands around the back of your head and pull you in for a deep kiss. Relieved that your trust in her wasn’t misplaced, you eagerly kiss her back.<br>" +
                        "Your cock throbs painfully throughout the kiss. Even the slightest touch would be enough to set it off, but Valerie is being careful enough that she doesn’t brush against it. At last, she pulls back from the kiss and removes one hand from the back of your head. <i>\"Are you ready?\"</i> she says.<br>" +
                        "You bring your hand to within inches of Valerie’s crotch and then nod.<br>" +
                        "<i>\"Okay,\"</i> she says. There’s a slightly breathy tone to her voice, and you notice that her cheeks are slowly reddening. You hold eye contact with her as she says, <i>\"On 3, we get each other off. 1… 2… 3!\"</i><br>" +
                        "Your hand was tensed and ready to dive in, your fingers plunging deep into Valerie’s pussy as soon on the count of <i>\"3.\"</i> At the same moment, Valerie grips your cock, stroking it with just as much vigor as your fingers stroke her. It doesn’t take either of you more than a couple seconds before you reach your peaks.<br>" +
                        "You almost lose your balance from the force of your orgasm, but somehow you and Valerie manage to support each other through it. Smiling and chuckling slightly, Valerie begins to pull away, but before she can, you reach your hands around her and pull her in for one more kiss. The next match might be coming soon, but the two of you can afford just one more moment to simply enjoy each other’s company first.<br>";
            }

        }else{
            if(flag == Result.intercourse){
                return "By this point, you and Valerie have flipped positions so many times that you're starting to get dizzy. It would almost be easier to just let her stay on top and try to defeat her from the bottom than it would be to roll her over, only to then get rolled over again yourself seconds later. You're on the edge of running out of stamina, and it's a wonder that the same isn't true for Valerie.<br>" +
                        "You're not ready to give up and stay on the bottom just yet though. With the remainder of your energy, you barely manage to force Valerie over onto her back once again. Somehow, your dick remains lodged in her pussy, and you're able to give her a couple good thrusts before you find yourself on your back once more. And then you're right back where you started, with Valerie riding you and bringing you ever closer to orgasm.<br>" +
                        "You just don't have the energy left to get her on her back one more time. All you can do is try to somehow make Valerie cum before she does the same to you. You reach up to Valerie, but instead of pushing her this time, you place your hands on her breasts, hoping that just a bit more pleasure will be what it takes to push her over the edge first.<br>" +
                        "Valerie lets out a grunt as she feels your hands on her breasts. She covers them with her own hands, pulling them in a bit tighter for a moment, then grasping them and pulling them away. \"Come on, don't tell me you're out of energy this easily,\" she says. Your mind can barely process the words at this point, with how close you are to cumming. \"No. I'm not having it.\"<br>" +
                        "Before you can fully process what Valerie's saying, you somehow find yourself on top of her once more. Despite having no idea how this could have happened, your instincts kick in and you use what little energy you have left to begin thrusting into Valerie's pussy once more, hoping against hope that you can make her orgasm first.<br>" +
                        "When Valerie's hands grab your head and pull you to her in a deep, passionate kiss, all hope is lost. Your cock explodes into her, filling her pussy with your seed as you moan into Valerie's mouth. It isn't until you're halfway through emptying your balls into her that you realize that you're not the only one moaning. Somehow you'd made Valerie climax at just the same moment you did, clutching a draw from the maw of defeat.<br>" +
                        "At least... you'd like to take credit for that. If you'd been on the bottom, Valerie would have most likely been able to hold back just long enough to win. But she'd flipped the two of you over again, putting herself in the weaker position. When you finally manage to regain enough energy, you ask Valerie what she was thinking doing that.<br>" +
                        "\"Huh?\" Valerie says. She seems to be winded from the match as well, and she looks at you in confusion. \"What do you mean?\"<br>" +
                        "You repeat the question, clarifying for her that she'd practically thrown the match away when she'd rolled the two of you back over.<br>" +
                        "\"...Match?\" Valerie says, blinking at you. She looks around for a moment, and then a blush fills her cheeks. \"Oh. Right. I uh... kinda maybe forgot about that when the sex got to be too much fun...\"<br>" +
                        "You stare at Valerie in silence for a long while. Finally, you let out a sigh. Maybe you earned this draw after all. It's certainly a fair bit better than losing a match - you get the money and score for a win, even if you have to give up your clothes. And you don't have to suffer the indignity of satisfying the winner's desires afterward either, which is probably quite a good thing knowing how Valerie can get sometimes.<br>" +
                        "From beneath you, Valerie lets out a yawn. \"Alright, lemme know when the match is over and I'll get up...\" she says, her eyes falling closed as she apparently settles down for a nap.<br>" +
                        "At least, that's what she wants you to think. Her eyes don't even fully close; she seems to be trying to watch you to see if her tease lands successfully. Shaking your head and smiling at Valerie, you stand back up as you get ready for your next match.<br>";
            }else{
                return "<i>\"Uwahh!\"</i> Valerie cries out as you manage to push her off of you and roll her over onto her back. She’d almost had you, but you were able to muster up the strength of will at the last moment to get her off of you, and now it’s your chance to turn the tables on her.<br>" +
                        "You sit down on Valerie’s stomach, facing her pussy. Your erect cock blocks your view of it, but at least this way you can better defend yourself from her hands while you work to finish her off. You reach your hands down to Valerie’s crotch, keeping your arms on either side of your cock to block off Valerie's access to it. You use one hand to spread Valerie open while your other dips its fingers inside of her. By this point, she’s utterly soaked, and it shouldn’t be long before you can get her off.<br>" +
                        "<i>\"Fuck, no…\"</i> Valerie cries out. You feel her hands try to reach around and grasp your cock, but you’re successfully able to block her access. Unless she can muster the strength to push you off before it’s too late, this match is yours. <i>\"Fuck!\"</i> Valerie curses, frustrated at her inability to fight back. <i>\"You asshole… You… asshole…\"</i><br>" +
                        "As you slip another finger inside Valerie, you get ready for her to try and push you off of her. You feel her hands on your ass, and you tense, getting ready for her to make her attack, but it seems you’ve misjudged her. Valerie wasn’t intending to push you off at all. Instead, she’d set her sights on the one target you’d left her with access to: your asshole.<br>" +
                        "Your eyes widen as you feel Valerie’s finger slip inside your ass. You never should have left yourself so vulnerable to her. If she manages to find your prostate, it’s all over. At this point, you probably can’t even pull away safely; all she has to do is follow after you and keep up her assault. The only option you have left is to fight back, so fight back you do.<br>" +
                        "You were planning to wait a bit longer before sliding another finger into Valerie’s pussy, but you don’t have time to waste now. Luckily, she’s slick enough that you have no trouble getting a third finger in, but even the combined force of three of your fingers isn’t enough to push her over the edge. Meanwhile, Valerie’s finger is wiggling its way into your anus, feeling far better than it has any right to, even without finding your prostate quite yet.<br>" +
                        "Out of frustration, you pull your fingers out of Valerie’s vagina and move them to her asshole. You might not be able to get her off this way, but you can at least get a small dose of petty revenge by making sure that you’re not the only one to get their ass fingered right now. Your middle finger is easily able to slide inside her ass, and you feel it clench on you in reflex. You tauntingly wiggle it around inside her. You might pay for this after you lose, but at least you can get a bit of revenge right now.<br>" +
                        "<i>\"Oh fuck yeah… just like that…\"</i> Valerie’s finger stops its motions within your ass, and the words catch you off-guard enough that you almost do the same with your finger, but you have just enough presence of mind to keep from stopping. <i>\"That’s right…\"</i> Valerie says, letting out a moan of pleasure. <i>\"Make me cum… make my asshole cum, just like that…\"</i><br>" +
                        "That’s a request you certainly don’t have it in you to deny. But maybe you don’t have to do it <i>\"just like that.\"</i> You pull your finger back until just the tip is inside Valerie, and carefully push in your index finger beside it. Slowly, you manage to slide both fingers inside of her, earning yourself a deep moan from the girl.<br>" +
                        "<i>\"Yes… yes…\"</i> Valerie says. You can feel her finger resume stroking inside your ass, but it feels more like she’s just trying to return the pleasure than to win the match at this point. <i>\"I’m almost there… almost… there…\"</i><br>" +
                        "Grinning, you decide it’s time to push Valerie over the edge. You thrust your fingers in and out for a moment, then spin them back and forth, then curl them up, then finally pull them apart as much as you can, stretching her asshole to its limit even as you continue sliding the fingers in and out. You’re so focused on the task that you don’t notice Valerie’s finger creeping its way toward your prostate until it’s too late.<br>" +
                        "Almost before you realize what’s happening, a fierce jolt of pleasure shoots through your cock, forcing it to shoot out a jet of cum. At the same time, Valerie bucks beneath you and lets out a loud groan as her own orgasm overtakes her.<br>" +
                        "You silently curse to yourself. You were so close to a win, but then you slipped up at the last moment. But then again, you’d never have gotten to that point if Valerie hadn’t slipped up herself and lost her focus as you’d begun fingering her asshole, so perhaps it was fair in the end. <br>" +
                        "From behind you, you hear the sound of Valerie’s laughter. <i>\"Oh my god…\"</i> she says. You pull away from her, her finger sliding out of your ass as you remove yours from hers, and you turn around to see her burying her beet-red face in the ground as best as she can. <i>\"I can’t believe we just finished each other off that way… What the hell got into us?\"</i><br>" +
                        "She was the one to start with the anal play, you remind her, which only results in her face getting even redder.<br>" +
                        "<i>\"Gah, I know!\"</i> she says, her voice breaking into giggles for a moment. <i>\"I guess I just lost control of myself there… both when giving and receiving.\"</i> After a few deep breaths, Valerie looks up at you. <i>\"But, um… it did feel good, right?\"</i><br>" +
                        "Well, you can’t really deny that it did.<br>" +
                        "Valerie smiles at this. <i>\"Good. It felt good for me too. That doesn’t mean you have my permission to finger my ass whenever you want, though.\"</i><br>" +
                        "Of course not. Only during the games. Or once you’ve gotten her horny enough that she begs you to do it again.<br>" +
                        "<i>\"Gah!\"</i> Valerie’s hands quickly cover her face to hide her blush from you. She soon breaks out into giggles again though, barely managing to get out a weak, <i>\"Stop it!\"</i> which is completely betrayed by the smile on her face.<br>";
            }
        }
    }

    @Override
    public boolean fightFlight(Character opponent) {
        return fit();
    }

    @Override
    public boolean attack(Character opponent) {
        return true;
    }

    @Override
    public void ding() {
        character.mod(Attribute.Discipline, 1);
        int rand;
        for(int i=0; i<(Global.random(3)/2)+1;i++){
            rand=Global.random(3);
            if(rand==0){
                character.mod(Attribute.Power, 1);
            }
            else if(rand==1){
                character.mod(Attribute.Seduction, 1);
            }
            else if(rand==2){
                character.mod(Attribute.Cunning, 1);
            }
        }

        character.getStamina().gain(3);
        character.getArousal().gain(6);
        character.getMojo().gain(2);
    }

    @Override
    public String startBattle(Character opponent) {
        if(character.getGrudge()!=null){
            switch(character.getGrudge()){
                case icequeen:
                    return "<i>\"Are you ready to try this again?\"</i> Valerie asks, giving you a slight smile. <i>\"You did quite a good job satisfying me last match, but you made the mistake of leaving me just a bit too sated. Now that I’ve had my fill, it won’t be so easy this time.\"</i>";
                case highcomposure:
                    return "The only emotion you can make out in Valerie’s face is frustration, but it almost seems like she’s making a choice to let you see that. Is she mad about her previous loss to you?<br>" +
                            "<i>\"No, I’m not mad at you for winning,\"</i> Valerie says, shaking her head. <i>\"I’m disappointed in myself for losing control like that. Be warned, I don’t plan to let it happen again.\"</i><br>";
                case potentcomposure:
                    return "Valerie’s face is an utter blank as she faces off against you, without even her normal facade of politeness. All of her efforts seem to be focused on holding herself together right now, and she doesn’t even spare the energy to greet you before the start of combat.";
                case enraged:
                    return "<i>\"You!\"</i> Valerie snaps as she sees you, a fury burning in her eyes. <i>\"It’s time for me to get my revenge for earlier. Don’t expect me to hold back this time.\"</i>";
                default:
                    break;
            }
        }if(character.nude()){
            return "From the confident expression on Valerie’s face, you’d never know that she was naked right now. There might be just a bit of nervousness in her eyes though, from starting off combat with this sort of disadvantage, but she’s doing a good job not to let it show. Just as you begin to let your gaze drop to look over her body, you spot her mouth widening into a grin. <i>\"I hope you enjoy the view, but don’t let it distract you from what we came here to do. That is, unless you plan to give me an easy win.\"</i>";
        }

        return "Valerie holds her riding crop in her right hand, twirling it around casually for just a moment before she grips it firmly and shifts her stance to focus on the battle. She points the riding crop toward you like a sword and gives you a small grin.  <i>\"En garde.\"</i>";
    }

    @Override
    public boolean fit() {
        return !character.nude()&&character.getStamina().percent()>=50;
    }

    @Override
    public String night() {
        if(Global.checkFlag(Flag.ValerieSub)){
            return "You say goodbye to your fellow competitors one by one as they finish getting dressed and head off for the night. There's just one person missing from the group, and it isn't until you give up and head off yourself that you finally lay eyes on her. It turns out that Valerie has been waiting for you on your way home, and she gives you a soft smile as she spots you.<p>" +
                    "\"I've been waiting for you, Master,\" Valerie says. You glance down, noticing that she's put on her slave collar tonight. You smile at Valerie and ask what your slave has in mind for tonight. \"Well...\" Valerie says, looking to the side a bit shyly. \"I must confess that I've been rather disobedient tonight. It's only appropriate that my Master should punish me as he sees fit.\"<p>" +
                    "Your smile widens a bit as she says this. You will indeed need to punish her. But maybe after that you can reward her for so willingly offering herself up for punishment like this. With a nod at Valerie, you lead her to your dorm. There's one close call with a fellow student who seems to notice the collar on Valerie's neck, but it turns out that he's stoned out of his mind and is just as likely to think you're walking an oversized (and very attractive) dog. Other than that, Valerie is spared the embarrassment of being seen collared by you - though whether that's a good thing or a bad thing is up for debate.<p>" +
                    "When you enter your room, you don't even have to say anything before Valerie strips off her clothes. Once she's wearing nothing but her collar, she climbs up on your bed and gets on her hands and knees, wordlessly signalling to you that she's ready for her punishment. You walk over to the bed and place your hand on her bare ass, slowly stroking it as you decide just what you want to do with her right now.<p>" +
                    "While Valerie has probably earned herself a whipping tonight, you're feeling a bit too affectionate toward her right now to do that. Instead, a spanking feels more appropriate. You pull your hand back for a moment, and then you slam it down firmly against Valerie's ass. In addition to the smacking sound as you strike her ass, this earns you a pleasant sound from Valerie herself, somewhere between a yelp and a moan.<p>" +
                    "You continue the spanking, though you've barely hit Valerie half a dozen times before her moans make it sound like she's on the verge of orgasm. You place your hand on her ass and slide it between her cheeks. You stroke your fingers over her asshole for a moment, which prompts Valerie to shudder and say, \"Haa... please... Master...\"<p>" +
                    "Obliging Valerie, you slip your fingers a bit lower, prodding at Valerie's pussy. Just before sliding them inside, you lean over to Valerie's face and capture her lips with your own. As soon as your fingers enter her, Valerie shudders and moans into your kiss, nearly collapsing to the bed as an orgasm overtakes her body.<p>" +
                    "\"Mmm...\" Valerie says. She lets herself fall to your bed, rolling over onto her side so she can look up at you. \"My Master is truly too kind to me... whatever can I do to repay him?\" Valerie's gaze meets yours, and she gives you an inviting look that makes you a bit weak at the knees.<p>" +
                    "Smiling at Valerie, you crawl into bed with her, slowly stripping off your own clothes. You might not have much energy left after the Games, but you do have just enough left for one more round of lovemaking.<p>";
        }
        return "With the match finished for the night, you can’t help but feel left wanting just a bit more. You decide to wait around a bit, chatting with the other competitors just in case anyone else is feeling similarly. As you head towards a group of the girls, Valerie notices you walking over to them and looks up at you. A flash of eagerness spreads across her face as she makes eye contact with you, but she soon catches herself and tries to cover up this expression. After that though, she appears to have second thoughts, and she makes eye contact with you once more, allowing a bit of desire to show through her stoic facade.<p>" +
                "You slip into the group beside Valerie and place your hand on the small of her back, gently stroking it as you talk with the other girls for a few minutes. One by one, the other girls leave, until it’s just you and Valerie remaining. Once no one else is around to hear, Valerie says to you, <i>\"You know, I made a bet to myself tonight. If you were to talk to me before anyone else after the match, I’d be yours tonight. Otherwise, I’d make you mine.\"</i><p>" +
                "You think back to your actions. While you’d placed your hand on her back before talking to anyone else, you hadn’t actually spoken to her first. Would that count for her bet?<p>" +
                "<i>\"That’s what I’m trying to decide,\"</i> Valerie says. She glances to the side, looking down the path toward your dorm, and then nods. <i>\"It’s the thought that counts. You came to me first, so that’s what matters. So, for tonight, I’ll pledge myself to you, with all my love, to do with as you please.\"</i> Valerie meets your gaze once more as she says this, and you can see the evidence of her affection in her eyes. <i>\"...On one condition.\"</i><p>" +
                "A condition? Given how tempting and sweet Valerie had just made her offer, you probably would agree to any possible condition she could name.<br>" +
                "Valerie’s smile widens just a bit as she sees how eager you are to accept. <i>\"All I ask is that before I fall asleep, you tell me honestly how you feel about me. Agreed?\"</i><p>" +
                "If you weren’t able to honestly say something to her that would make her melt, this would be the time to let her down gently. But that couldn’t be further from the truth. You nod at Valerie, agreeing to her condition, and then lead her back to your dorm.<p>" +
                "One hour later, when the two of you are the verge of falling asleep from orgasm after orgasm, you roll over on your side, facing Valerie. You lean forward to plant a soft kiss on her lips, then pull back. You place your hand gently on her cheek as you whisper sweet words of affection to her. In her vulnerable state, she’s unable to hold back the tears of joy that leak out of her eyes.<p>" +
                "<i>\"I love you too, "+Roster.get(ID.PLAYER).name()+"...\"</i> she says, before burying her face in your chest and wrapping her arms tightly around you.<br>" +
                "<br>";
    }

    @Override
    public void advance(int Rank) {

    }

    @Override
    public boolean checkMood(Emotion mood, int value) {
        if(composed()){
            switch(mood){
                case dominant:
                case confident:
                    return value>=50;
                case angry:
                case desperate:
                    return value>=200;
                default:
                    return value>=100;
            }
        }else{
            switch(mood){
                case angry:
                case desperate:
                    return value>=30;
                case nervous:
                case horny:
                    return value>=50;
                case confident:
                    return value>=250;
                default:
                    return value>=100;
            }
        }
    }

    @Override
    public float moodWeight(Emotion mood) {
        if(composed()){
            switch(mood){
                case confident:
                case dominant:
                    return 1.2f;
                case desperate:
                case angry:
                    return .7f;
                default:
                    return 1f;
            }
        }else{
            switch(mood){
                case angry:
                case desperate:
                    return 1.2f;
                case confident:
                case dominant:
                    return .5f;
                default:
                    return 1f;
            }
        }
    }

    @Override
    public String image() {
        return null;
    }

    @Override
    public void pickFeat() {
        ArrayList<Trait> available = new ArrayList<Trait>();
        for(Trait feat: Global.getFeats()){
            if(!character.has(feat)&&feat.req(character)){
                available.add(feat);
            }
        }
        if(available.isEmpty()){
            return;
        }
        character.add((Trait) available.toArray()[Global.random(available.size())]);
    }

    @Override
    public CommentGroup getComments() {
        CommentGroup comments = new CommentGroup();
        if(composed()){
            comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Alright, just let me do all the work now, and let yourself enjoy it.\"</i>");
            comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Don’t you want to cum inside me? Please, just let me help you do so.\"</i>");
            comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"That’s it. Do me as hard as you’d like, and fill me up with your seed.\"</i>");
            comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Ah! I mean… slow down, please. You wouldn’t want to make me wait for you after I climax, right?\"</i>");
            comments.put(CommentSituation.ANAL_PITCH_WIN,"<i>\"Don’t worry, you’ll forget about all the pain soon enough.\"</i>");
            comments.put(CommentSituation.ANAL_PITCH_LOSE,"<i>\"Please forgive me for this, but you left me with few options if I wish to win this match.\"</i>");
            comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"Haa… you don’t need to do this. It’s too late anyway. Just… mm… just let yourself go and then leave my rear alone, please?\"</i>");
            comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Please stop… If you keep doing this, I… I’ll… Please…\"</i>");
            comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Mm, good. There’s no point in fighting back at this point. Just try to enjoy yourself.\"</i>");
            comments.put(CommentSituation.MOUNT_DOM_LOSE, "<i>\"You haven’t beaten me yet. Now let me show you just how good it can feel to be on the receiving end.\"</i>");
            comments.put(CommentSituation.MOUNT_SUB_WIN, "<i>\"I know you want to have sex with me. Go ahead, you have my permission. You can even come inside me if you want.\"</i>");
            comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"So this is how you want to finish the match, is it? I can’t say it’s a bad way to end it, but don’t think I’m giving up just yet.\"</i>");
            comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"I really do want to taste your seed. You wouldn’t mind obliging me, would you?\"</i>");
            comments.put(CommentSituation.SIXTYNINE_LOSE, "<i>\"Oh wow… how did you ever get so good with your tongue?\"</i>");
            comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Just relax now, and let me take care of the rest.\"</i>");
            comments.put(CommentSituation.BEHIND_DOM_LOSE, "<i>\"Alright, you’ve had your fun. Now it’s my turn, if you don’t mind.\"</i>");
            comments.put(CommentSituation.BEHIND_SUB_WIN, "<i>\"I hope you don’t have anything nefarious in mind. Be warned that after I win, I might just have to punish you for anything you do now.\"</i>");
            comments.put(CommentSituation.BEHIND_SUB_LOSE, "<i>\"Wait, what are you planning to do?\"</i>");
            comments.put(CommentSituation.PIN_DOM_WIN, "<i>\"You’re all mine, now.\"</i>");
            comments.put(CommentSituation.PIN_DOM_LOSE, "<i>\"You didn’t think it would be that easy, did you? Let’s see if we can make things a little harder for you… pun intended.\"</i>");
            comments.put(CommentSituation.PIN_SUB_WIN, "<i>\"I knew you still had some fight left in you. If you think you can still win, I’m curious to see what you can do.\"</i>");
            comments.put(CommentSituation.PIN_SUB_LOSE, "<i>\"Don’t expect me to give up, even now.\"</i>");
            comments.put(CommentSituation.SELF_CHARMED, "<i>\"Did anyone ever tell you how lovely your eyes are? I just can’t stop gazing at them…\"</i>");
            comments.put(CommentSituation.SELF_BOUND, "<i>\"Hmm. This is hardly a sporting tactic...\"</i>");
            comments.put(CommentSituation.OTHER_BOUND, "<i>\"My apologies for this, but I’m going to need to keep you from resisting for a bit.\"</i>");
            comments.put(CommentSituation.OTHER_STUNNED, "<i>\"You can take a moment to recover. I’ll just occupy myself with your body while you do so.\"</i>");
            comments.put(CommentSituation.SELF_HORNY, "<i>\"If you don’t mind, could we perhaps hurry this match along? I find myself rather eager to get to the fun part.\"</i>");
            comments.put(CommentSituation.OTHER_HORNY, "<i>\"I see you’re having trouble resisting me. If you want to stop resisting, I have absolutely no complaints about that.\"</i>");
            comments.put(CommentSituation.SELF_OILED, "<i>\"Why did you do this…? Please don’t tell me you’re planning to… to…\"</i>");
            comments.put(CommentSituation.OTHER_OILED, "<i>\"Mm, there we go. And I must say, you look quite lovely with your skin glistening like this.\"</i>");
            comments.put(CommentSituation.SELF_SHAMED, "<i>\"Ah… no… no… please…\"</i>");
        }else{
            comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Now, let me show you what this pussy of mine can do.\"</i>");
            comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Damn it… this just feels too good to stop…\"</i>");
            comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"You like this, don’t you? You like feeling your cock deep in my pussy, pounding me mercilessly…\"</i>");
            comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Ahhh… no… stop! Stop! You’re making me cum, fuck, stop!\"</i>");
            comments.put(CommentSituation.ANAL_PITCH_WIN,"<i>\"Your turn, asshole! Or I should say, my turn in your asshole! Okay, I know it’s a horrible pun, but that shouldn’t be what you’re complaining about right now!\"</i>");
            comments.put(CommentSituation.ANAL_PITCH_LOSE,"<i>\"Fuck it, I don’t care if this makes me cum! I’m going to fuck your ass until I can’t stand up any longer.\"</i>");
            comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"Yes! That’s it! Fuck my ass just like that! I want to feel your seed fill me up, so cum in me!\"</i>");
            comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"No no no… fuck… my ass… no… fuck… fuck my ass… yes… I mean… fuck… fuck… just like that… make me cum from my ass… I’m almost there...\"</i>");
            comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Alright, asshole. Do you think you’ve earned the privilege to cum in my cunt, or should I just make you cum on the ground?\"</i>");
            comments.put(CommentSituation.MOUNT_DOM_LOSE, "<i>\"Okay, we can do this the hard way, or the hard way. I mean… just be hard for me, okay?\"</i>");
            comments.put(CommentSituation.MOUNT_SUB_WIN, "<i>\"Come on, fuck my pussy already. I know you want to.\"</i>");
            comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"Just fucking put it in me already!\"</i>");
            comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"I can’t believe how fucking good your cock tastes… now let me taste your sperm too.\"</i>");
            comments.put(CommentSituation.SIXTYNINE_LOSE, " <i>\"Fuck your fucking tongue… that thing should be illegal…\"</i>");
            comments.put(CommentSituation.BEHIND_DOM_WIN, " <i>\"Alright. No more mister nice girl. …I mean missus nice guy. I mean… fuck!\"</i>");
            comments.put(CommentSituation.BEHIND_DOM_LOSE, "<i>\"Alright, you’ve had your fun. Now it’s my turn, if you don’t mind.\"</i>");
            comments.put(CommentSituation.BEHIND_SUB_WIN, " <i>\"Pfft! Like you can really turn things around at this point, even if you were to… to… The point is you’ve lost!\"</i>");
            comments.put(CommentSituation.BEHIND_SUB_LOSE, " <i>\"Ahh… Are you really going to take me from this position?\"</i>");
            comments.put(CommentSituation.PIN_DOM_WIN, "<i>\"Oh, you have no idea how much fun I’m going to have now that I have you at my mercy like this.\"</i>");
            comments.put(CommentSituation.PIN_DOM_LOSE, "<i>\"Fucking finally… now it’s my turn.\"</i>");
            comments.put(CommentSituation.PIN_SUB_WIN, " <i>\"Don’t fucking think you can still manage to win this! Your ass is mine and we both know it!\"</i>");
            comments.put(CommentSituation.PIN_SUB_LOSE, "<i>\"…damn it, why am I so fucking turned on by this…?\"</i>");
            comments.put(CommentSituation.SELF_CHARMED, "<i>\"How the hell are you so damn sexy?\"</i>");
            comments.put(CommentSituation.SELF_BOUND, " <i>\"Oh come on! This is a dirty trick and you know it!\"</i>");
            comments.put(CommentSituation.OTHER_BOUND, "<i>\"There we go. Now it’s time to have some fun with you.\"</i>");
            comments.put(CommentSituation.OTHER_STUNNED, "<i>\"You didn’t think I’d let you go unpunished, did you?\"</i>");
            comments.put(CommentSituation.SELF_HORNY, "<i>\"What the fuck are you waiting for? Fuck me already before I fuck you!\"</i>");
            comments.put(CommentSituation.OTHER_HORNY, " <i>\"Don’t give me that face. I was already planning to make you cum.\"</i>");
            comments.put(CommentSituation.SELF_OILED, "<i>\"Wait… what are you planning? You’d better fucking leave my ass alone!\"</i>");
            comments.put(CommentSituation.OTHER_OILED, "<i>\"Oh damn, you’re one hot boy like this…\"</i>");
            comments.put(CommentSituation.SELF_SHAMED, "<i>\"Fine, I’m a fucking slut who wants you to fuck her brains out, are you happy?\"</i>");
        }
        return comments;
    }

    @Override
    public int getCostumeSet() {
        return 1;
    }

    @Override
    public void declareGrudge(Character opponent, Combat c) {
        if(character.getGrudge()==Trait.icequeen || character.getGrudge()==Trait.highcomposure || character.getGrudge()==Trait.potentcomposure){
            character.addGrudge(opponent,Trait.enraged);
        }else {
            switch (Global.random(3)) {
                case 0:
                    character.addGrudge(opponent, Trait.icequeen);
                    break;
                case 1:
                    character.addGrudge(opponent, Trait.highcomposure);
                    break;
                default:
                    character.addGrudge(opponent, Trait.potentcomposure);
                    break;
            }
        }
    }
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        character.outfit[Character.OUTFITTOP].add(Clothing.bra);
        character.outfit[Character.OUTFITTOP].add(Clothing.noblevest);
        character.outfit[Character.OUTFITTOP].add(Clothing.noblecloak);
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.panties);
        character.outfit[Character.OUTFITBOTTOM].add(Clothing.tdresspants);
    }
}
