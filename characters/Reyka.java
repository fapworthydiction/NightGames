package characters;

import daytime.Daytime;
import global.*;

import items.Clothing;
import items.Component;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentGroup;
import Comments.CommentSituation;
import skills.Skill;
import actions.Action;
import actions.Movement;
import characters.Attribute;
import combat.Combat;
import combat.Result;
import status.Stsflag;

public class Reyka implements Personality {
	private static final long serialVersionUID = 8553663088141308399L;
	public NPC character;

	public Reyka() {
		this.character = new NPC("Reyka",ID.REYKA, 10, this);
		this.character.outfit[0].add(Clothing.lbustier);
		this.character.outfit[1].add(Clothing.lminiskirt);
		character.closet.add(Clothing.lbustier);
		character.closet.add(Clothing.lminiskirt);
		this.character.change(Modifier.normal);
		this.character.set(Attribute.Dark, 12);
		this.character.set(Attribute.Seduction, 14);
		this.character.set(Attribute.Cunning, 7);
		this.character.set(Attribute.Speed, 7);
		this.character.setUnderwear(Trophy.ReykaTrophy);
		this.character.getStamina().setMax(40);
		this.character.getArousal().setMax(110);
		this.character.getMojo().setMax(70);
		character.add(Trait.female);
		character.add(Trait.succubus);
		character.add(Trait.darkpromises);
		character.add(Trait.greatkiss);
		character.add(Trait.tailed);
		character.add(Trait.Confident);
		Global.gainSkills(this.character);
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 5);
		character.strategy.put(Emotion.sneaking, 2);
	}

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Fuck")||a.toString().equalsIgnoreCase("Piston")||a.toString().equalsIgnoreCase("Tighten")||a.toString().equalsIgnoreCase("Ass Fuck")){
				mandatory.add(a);
			}
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}	
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day) {
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Strapon)||character.has(Toy.Strapon2))&&character.money>=600&&character.getPure(Attribute.Seduction)>=20){
			character.gain(Toy.Strapon);
			character.money-=600;
		}
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Workshop");
		available.add("Play Video Games");
		for(int i=0;i<time;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		character.visit(3);
		if(Global.random(3)>1){
			character.gain(Component.Semen);
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.ReykaDWV, 1);
		}
	}

	@Override
	public String bbLiner() {
		switch(Global.random(2)){
		case 1:
			return "<i>\"That wasn't too hard, was it?  We better make sure everything still works properly!\"</i>";
		default:
			return "Reyka looks at you with a pang of regret: <i>\"In hindsight, damaging"
				+ " the source of my meal might not have been the best idea...\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		return "<i>\"You could have just asked, you know.\"</i> As you gaze upon her naked form,"
				+ " noticing the radiant ruby adorning her bellybutton, you feel"
				+ " sorely tempted to just give in to your desires. The hungry look"
				+ " on her face as she licks her lips, though, quickly dissuades you"
				+ " from doing so";
	}

	@Override
	public String stunLiner() {
		return "Reyka is laying on the floor, her wings spread out behind her,"
				+ " panting for breath";
	}

	@Override
	public String winningLiner() {
		return "<i>\"Hurry up and come already, I'm getting hungry!\"</i>";
	}

	@Override
	public String taunt() {
		return "<i>\"You look like you will taste nice. Maybe if let me have "
				+ "a taste, I will be nice to you too\"</i>";
	}

	@Override
	public String victory(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if (opponent.hasDick())
			character.gain(Component.Semen);
		character.arousal.empty();
		if(flag==Result.anal){
			Global.modCounter(Flag.PlayerAssLosses, 1);
			return "Reyka alternates between long hard thrusts and sensual grinding to keep you from getting used to the stimulation, and the pleasure it is " +
			"inflicting on you stops you from mustering the resolve to fight back. <i>\"I do love a good bit of pegging.\"</i> Reyka comments as she begins " +
			"to gently rock the head of the strapon over your prostate, leaving you breathing hard as your mouth hangs open. <p>"
			+ "<i>\"There's a special " +
			"pleasure in making a boy a little butt slave.\"</i> Her words shock you and cause your resistance to slip a little. <i>\"Hmmm?\"</i> She purrs <i>\"Would " +
			"you like that?\"</i> she asks, picking up the pace of her thrusting. <i>\"To be my little pet boy slut?\"</i> Your only response is to cum. Hard. Ropes " +
			"of cum fall to the ground below you.<p>"
			+ "Reyka pouts as she pulls out <i>\"Such a good waste of semen though.\"</i> she tuts. <i>\"Looks like you " +
			"still owe me a meal.\"</i> She smirks in a way that makes your eyes flash quickly left to right, looking for an escape route. Reyka is too quick " +
			"however and soon you find yourself pinned with your still hard cock buried deep in her pussy.<p>"
			+ "She rides you until you cum again and she " +
			"has cum twice herself. She stands up and begins collecting her clothes and her spoils as the victor.<p>"
			+ "She turns to you. <i>\"The offer still " +
			"stands; you'd make a great sub if you're ever interesting in broadening your sexual horizons. Open minded men are hard to find.\"</i> She admits, " +
			"smiling.<p>"
			+ "You shake your head; you don't think that sort of thing would really suit you. Her smile deflates some but she nods her head and " +
			"turns to go.<p>"
			+ "<i>\"Let me know if that ever changes, I'd definitely enjoy opening your mind,\"</i> she calls over her shoulder as she leaves.";
		}
		if(opponent.is(Stsflag.enthralled)){
			return "With your mind completely ensnared by Reyka's command, you don't even attempt to hold on as she stimulates you to the very limit of your arousal, just before you finish however, the succubus speaks<p>" +
					"<i>\"Don't cum yet\"</i> Reyka orders firmly, and under her spell you suddenly find yourself unable not to comply. Though your member yearns for release Reyka's command keeps you from cumming, knowing she has you exactly where she wants you she gives you a (literally) devilish smile before diving on top of you, mounting your chest and facing your groin. <p>" +
					"You can feel Reyka begin to absentmindedly play with your boner before she repeats herself.<p>" +
					"<i>\"Do not cum until I say so\"</i> she speaks commandingly before her tone softens <i>\"You have no idea how long I've waited to have you like this "+opponent.name()+"\"</i> Her voice, still laced with dark magic somehow floods you with arousal even further, leaving your cock to tremble at her every motion. Then she leans forwards and takes you in her mouth. Immediately you're flooded with the sensation of her mouth expertly teasing your full length, her tongue salaciously lapping at the head. Within seconds your body begins practically aching for release, your member spasms desperately at every touch of Reyka's warm, wet tongue.<p>" +
					"Reyka however shows no sign of letting you finish however, even though she can clearly tell how desperate your becoming. She redoubles her efforts as you buck your hips futilely, the need to finish becoming almost too much to bare, as you writhe underneath her submissively, she suddenly pulls away and sits back up. <p>" +
					"<i>\"Alright "+opponent.name()+" this is starting to feel maybe a little too cruel, so, when I kiss the tip, you can cum okay?\"</i> She says with a surprisingly affectionate tone, turning her head to flash you a sweet sideways smile as she begins playing with you absentmindedly with one hand. She watches you writhe and moan with an amused expression until she decides she's toyed with you enough. She turns back around, and still tenderly stroking your shaft, leans over your crotch once more. After a couple of agonisingly long seconds, you feel Reyka peck the head of your dick with her soft gentle lips and the mind control breaks and you're thrown into what's possibly the most intense orgasm of your life! As you cum Reyka wraps her lips around your dick and hungrily swallows your multiple loads until finally you finish, lying dazed, completely spent.  As your mind swims back into clarity, you hear Reyka speak softly<p>" +
					"<i>\"Now that was a meal, I'll be sure to come for you next time I need a mid game snack...\"</i>";
		}
		if (flag == Result.intercourse){
			return "When it becomes obvious that she's going to win, Reyka takes control. Her tail lashes around your neck, just hard enough to "
					+ "hint that things could get worse for you if you don't behave, and she makes sure she's on top.<p>"
					+ "Soon, you have no say in this anymore. She's got you pinned with your hands against the ground, dizzy and thoughtless "
					+ "from the sight and smell and feel of her. Your cock feels like she's got it in a velvet fist, too hot by half, and "
					+ "she expertly shifts her hips against you to milk a load out of your balls.<p>"
					+ "All that time, she keeps eye contact with you. You can't seem to so much as blink. Your orgasm doesn't exactly seem to "
					+ "end, either, but just... elongate, into something else. Some greater, extended experience, something tantric you can't "
					+ "identify.<p>"
					+ "Reyka leans down to run her tongue along your lips, laughing, and even these fractured, scattered thoughts you're "
					+ "having become impossible--<p>"
					+ "<i>Your life begins and ends with this bed.<p>"
					+ "You don't have to leave. You don't know why you'd want to. Sometimes, in memories that seem veiled by smoke, you remember "
					+ "things like having to eat, or mortal ambitions you once had. Those are gone almost as soon as they occur, washed away on "
					+ "slow waves of insatiable lust. Your universe is six feet across and twelve feet long, covered in silk sheets.<p>"
					+ "Your mistress needs you, and you obey. She sinks down onto you, her hands flat against your naked chest, and rides you "
					+ "at her own pace.<p>"
					+ "You know what she wants without having to ask. It's what you want. You'd jump into a fire for the mistress, kill a man, "
					+ "change the course of history. You move as she prefers, as little or as much as she demands, the two of you together in a "
					+ "silent symphony.<p>"
					+ "When you come, you give her much more than simple semen. Heat. Strength. Life. Every time you convulse and fill her with "
					+ "another gush of come, you send another few trailing days of your existence into her and are glad for the contribution.<p>"
					+ "One day, she may take the last one, and you'll die with a smile on your face--</i><p>"
					+ "You wake up.<p>"
					+ "You're naked and alone, left in a corner of the room where you won't be noticed immediately by anyone who walks in. "
					+ "Your clothes are, of course, nowhere to be found. A nearby wall clock tells you that you can't have been unconscious "
					+ "for more than a couple of minutes.<p>"
					+ "It feels like you just donated blood. It takes you three tries to stand, but once you do, it's easier than you "
					+ "thought it'd be. After a couple of minutes, you manage to take one step, then another, and you feel your strength "
					+ "slowly coming back. You're not out of the Games just yet.<p>"
					+ "That wasn't a dream, you think. That was proof of intention. You've been given a sneak preview, somehow, of life as "
					+ "Reyka's pet, in her larder, should she make good on those threats of hers.<p>"
					+ "Now that you're awake, it isn't all that enticing... except in a distant, what-if sort of way. You contemplate what "
					+ "it'd be like to live like that, free from all concerns. The ultimate submissive. Nothing for you but what she wants.<p>"
					+ "You shake your head and go back out into the fray.";
		}
		return "With a final cry of defeat (and pleasure) you erupt under Reyka's"
				+ " attentions. She immediately pounces on you and draws your lips to hers."
				+ " As does so, she inhales deeply, drawing more than just air out of you."
				+ " You feel your strength flowing into her and even though your vision"
				+ " is already darkening, you see her starting to literally glow, surrounded"
				+ " by a soft, deep red aura.<p>"
				+ "As she continues to drink in your energy,"
				+ " you feel something shift in you, as if something that was there all"
				+ " along but has always gone unnoticed by you suddenly got yanked on."
				+ " Just before you pass out, you see her wings enveloping you both"
				+ " in a dark, warm cocoon.<p> "
				+ "After what seems like an eternity,"
				+ " but what actually lasted for only a few minutes, you wake up and"
				+ " drowsily look around. You can see Reyka sitting cross-legged a few feet"
				+ " away, her wings folded neatly behind her back and her eyes fixed on"
				+ " yours."
				+ (opponent.hasDick() ? " You notice a bottle of pearly"
						+ " white liquid behind her and can only guess where it came from."
						: "")
				+ " <p>"
				+ "<i>\"You tasted heavenly\"</i>, she says, while you are still too"
				+ " far gone to catch on to the irony of the statement, <i>\"So here I was,"
				+ " drinking in your delicious energy, and it struck me that if I claimed"
				+ " your soul now, I wouldn't be able to drink from you again. So I simply"
				+ " took a little nibble into it and let you recover. I will expect you to"
				+ " repay this kindness soon, and there is only one thing I will accept"
				+ " as payment. I'll leave you to figure out what it is.\"</i><p>"
				+ "With that,"
				+ " she walks off, her hips, barely covered by her short miniskirt,"
				+ " seductively waving good-bye. For now.";
	}

	@Override
	public String defeat(Combat c,Result flag) {
		character.arousal.empty();
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(flag == Result.intercourse){
			return "These days, you often wonder if your epitaph is going to be <i>\"It seemed like a good idea at the time.\"</i><p>"
					+ "Like now, for instance.<p>"
					+ "Reyka left an opening, or maybe let you think she had, and you took it, or maybe she made you want to take it. "
					+ "You can't exactly remember what happened here. It's been at least thirty seconds and there's a lot going on "
					+ "in your life right now.<p>"
					+ "Whatever it was, you ended up fucking her. This was absolutely the soundest strategy available to you at the "
					+ "time. You can't think of why you'd have done anything else, even though you knew from the moment you slid "
					+ "inside her that it was a terrible idea.<p>"
					+ "Now she isn't letting you pull out.<p>"
					+ "You aren't fucking her anymore so much as trying to escape, but her legs are locked around your waist, she's "
					+ "using her wings for wind resistance, and the two of you stumble around the room together like you're both "
					+ "angry drunks.<p>"
					+ "You can't make sounds that aren't animal groans and she won't. Stop. Smiling. She knows she's won this one "
					+ "and you're just playing out the string. Her next meal is coming in fast, and maybe a little more than that. "
					+ "Her tail's wound around your leg, and every so often, the tip pokes your sphincter in an attempt to break "
					+ "your concentration. Something's got to give and you're sure it'll be you.<p>"
					+ "You manage to spin as the two of you lurch back across the room and into the nearest hallway. Reyka loses "
					+ "some air as her back hits the wall, and just for a second, you feel things change around the two of you. "
					+ "Her concentration just lapsed.<p>"
					+ "You capitalize on that and put your hands on her thighs for leverage, so you can fuck her as hard as you "
					+ "can with what little you've got left in you. It's one last desperate attempt, not even to make her come, "
					+ "but to at least get her to stop fucking smiling like that.<p>"
					+ "To your relief, she does. Reyka's eyes widen, her fingers sink into your shoulder blades, and you get a "
					+ "welcome burst of adrenaline. You give Reyka all of it, slamming your hips against hers hard enough that "
					+ "you half expect to put her through the wall.<p>"
					+ "It surprises both of you when she comes first. You feel a dull flash of pain as her fingers and thighs "
					+ "tighten on you, but it's all part of the same envelope of sensation and it doesn't slow you down. "
					+ "Reyka's mouth opens in a soundless shout, she shudders out a climax against you, and the feel of triumph "
					+ "at that is more of an orgasm than the one you have a moment later.<p>"
					+ "Your legs decide they've had enough a moment later, and you clumsily stagger into a sitting position. "
					+ "Your cock begins to soften and pulls out of Reyka, who disengages from you and gently floats away.<p>"
					+ "<i>\"You're sure you're human?\"</i> she asks. <i>\"Absolutely sure?\"</i><p>"
					+ "You look at her strangely.<p>"
					+ "<i>\"No weird birthmarks you had removed? Was there anything weird about your parents, or your "
					+ "grandparents? You don't have any incubus blood at all?\"</i><p>"
					+ "Reyka runs two fingers through the cum leaking down her thighs and licks them clean. She looks sweaty "
					+ "and bedraggled, but no more exhausted for it. You, conversely, feel and probably look like a wrung-out "
					+ "dishtowel.<p>"
					+ "She settles to the ground on bare feet, fishes what's left of her clothing out of the wreckage in "
					+ "your wake, and kicks it over to you.<p>"
					+ "<i>\"You should look into it,\"</i> Reyka says. <i>\"No mere human could do what you do.\"</i><p>"
					+ "That might have been a compliment. You're not sure.<p>"
					+ "<i>\"And next time,\"</i> she says, <i>\"I might have to take you a little more seriously.\"</i><p>"
					+ "With that, she floats out the nearest window, out into the night.";
		}
		return "As you bring Reyka ever closer to her climax, her prehensile"
				+ " tail suddenly pulls you to the ground and coils around your neck."
				+ " She squats down over your face and uses her tail to push your face"
				+ " up against her pussy.<p>"
				+ "The scent is so enticing that you simply must"
				+ " have a taste, and you start lapping at her feverishly. Somewhere along"
				+ " the way her tail let go of your neck, the compulsion it provided having"
				+ " been replaced by that of her aphrodisiac juices.<p>"
				+ "As she finally screams out"
				+ " in orgasm, the sound reverberating through your soul, the amount of"
				+ " fluids gushing into your willing mouth nearly drown you. After a few"
				+ " seconds she rolls off of you, although you don't notice it, having passed"
				+ " out from the overdose of aphrodisiacs.<p>"
				+ "You come to your senses just"
				+ " in time to see Reyka drinking down the load of cum you are shooting"
				+ " into her mouth. Somehow, you keep from passing out as she drinks"
				+ " some of your energy and soon, you see her face hovering over yours.<p>"
				+ " <i>\"Sorry about that, but you wore me out so thoroughly I just needed"
				+ " a little snack to get back on my feet. You'll be perfectly fine in"
				+ " a few minutes, after that we can go back to hunting each other. I"
				+ " certainly hope we meet again soon, that treatment you gave me is well"
				+ " worth the trouble of fighting through the others to get at you.\"</i>"
				+ " As she walks away into the distance you can't help but feel like"
				+ " 'winning' is not quite the word you would use to describe what you"
				+ " just went through.";
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if (target.human()) {
			return "<i>\"How kind of you to hold him for me, dear.\"</i> Reyka bows her head ever so slightly towards "+assist.name()+" and then turns her gaze upon you prone form. " +
					"She pulls a blindfold out of a small pocket in her miniskirt and secures it tightly over your eyes. <i>\"Wouldn't want to spoil the surprise, would we?\"</i> For " +
					"just a moment, you feel a slight pull on you mind, but the sensation passes quickly, replaced by that of one of her slender fingers invading your mouth. " +
					"It is covered with a fragrant liquid and given what you already know about her, there is little doubt in your mind of its origins. Your suspicions " +
					"are proven correct when the aphrodisiac reaches your loins, which respond as expected.<p>"
					+ "Apparently not one to stand on ceremony, Reyka immediately " +
					"settles over your now rock hard dick and lowers herself down onto it. The sensation is beyond comparison, her pussy wiggles and twists around you almost " +
					"as if it has a mind of its own, a mind connected to your own, knowing what will bring you the most pleasure. Your experiences in sex-fighting have " +
					"left you with impressive sexual stamina, but in the face of a succubus' unimpeded attentions, no man can hope to last. All too quickly you succumb " +
					"to the feelings, pouring your seed into the succubus. Just your seed. You were expecting her to take so much more, but you just feel a little tired, " +
					"not more so than after a regular orgasm.<p>"
					+ "The mystery is unveiled when Reyka removes the blindfold with her left hand. In her right hand, she is " +
					"holding a bottle. That bottle is firmly planted against the head of your still twitching dick and filled with your cum. <i>\"You looked scrumptious, " +
					"sitting there all helpless, but I was really in need of some supplies. Still, I didn't want to deny you the pleasure, so I crafted a teeny tiny " +
					"illusion just for you.\"</i> As she says this, she pours a small drop of your semen onto her finger and licks it up. <i>\"Yum, I might just have to find " +
					"you again later.\"</i> Both she and "+assist.name()+" walk off, in opposite directions, the former holding your clothes and the latter quietly giggling at " +
					"your embarrassment. Ah, well.";
		}
		else if(target.hasDick()){
			return "At the sight of "+target.name()+"'s erect cock, Reyka wraps her soft hands around it, slowly jerking up and down."
					+ "Not seeing the reaction she wants, the succubus starts to fondle her breasts, arousing her prey."
					+ "<i>\"You like that?\"</i> she asks, exposing her breasts and teasing the tip of her dick with her fingertip."
					+ "Hungering for semen, she licks "+target.name()+"'s glans with her long tongue, enticing her hole. <p>"
					+ "Enjoying the expression on her face, Reyka starts to suck down on her manliness, welcoming her into her"
					+ "mouth. Feeling her hips start to move, the succubus begins to deep throat, allowing "+target.name()+"'s dick to"
					+ "reach the deepest parts of her throat. Feeling her dick starting to twitch a bit, she stops for a"
					+ "brief moment to catch air and prolong her orgasm. With her dick slipping out of her saliva-"
					+ "dripping mouth, the succubus manages to mutter out a sentence while slowly fondling her balls,"
					+ "<i>\"I've tasted better, but you're not so bad either, let's do something that'll feel even better...\"</i><p>"
					+ "Seeing that her prey's starting to emit pre-cum, the succubus decides it's time to heat things up"
					+ "a bit. She stands up and removes all her clothing, spreading her pussy dominantly.. <i>\"You"
					+ "think you can handle this?\"</i> she says standing over her lustful prey's wet cock.<p>"
					+ "Reyka grips "+target.name()+"'s dick in a swift motion and holds it at the edge of her hole, "
					+ "<i>\"Let's see how long you'll last.\"</i> The succubus gives a faint smile while drilling her opponent's penis into her deepest cavities, pleasuring them both."
					+ "<p>"
					+ "The succubus rides "+target.name()+"'s dick roughly against her insides, making sure to finish her off quickly. "
					+ "At the critical moment, she quickly stops to exit and swallow her load. Cum fills her"
					+ "mouth, dripping down her throat and chin. She swallows everything in a single gulp.<p>"
					+ "Smiling at you, the succubus says in a devilish manner, <i>\"Thanks for helping, but I'm not quite"
					+ "feeling satisfied yet, mind helping me out again?\"</i>";
		}
		return "<i>\"My my, what a cute little offering you have caught for me tonight\"</i>, Reyka says, looking you at you with a satisfied grin on her face. <i>\"Not very nutritious, " +
				"but certainly a good deal of fun.\"</i> With that, she starts gently undressing "+target.name()+". When she is finished she squats down in front of her, bringing " +
				"her tail up between them. <i>\"Where would you prefer it dear?\"</i>, she asks "+target.name()+", whose eyes grow wide in shock. She manages to stammer out a " +
				"few syllables, but nothing quite coherent. \"No preference? Then I guess I will simply choose for you\" She brings her spade-tipped tail between "+target.name()+"'s " +
				"legs and starts running the very tip rapidly across her labia. When it is sufficiently wet, she moves it slightly upwards and moves it briskly back and forth over " +
				target.name()+"'s clit.<p>"+target.name()+", at first scared, now has her eyes closed and begins moaning feverishly. Just when she has almost reached her climax, " +
				"Reyka digs her tail deep into "+target.name()+"'s drooling pussy. This sends "+target.name()+" loudly over the edge. Her screams of pleasure are almost deafening, " +
				"and you have to work really hard to restrain her convulsing body. After a minute or so, the orgasm subsides and "+target.name()+" falls asleep and you gently lay her " +
				"down. When you turn to look at "+target.name()+", you are startled by the predatory look in her eyes. <i>\"I'm afraid all the excitement has left me a tad peckish. Be a " +
				"dear and help me out with that, will you?\"</i> You ponder whether or not you made a mistake in helping her.";
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if (target.human())
			return "Your fight with "
					+ assist.name()
					+ " starts out poorly; she already"
					+ " has you naked and aroused, whereas she seems as cool and calm as when"
					+ " you started. You haven't lost yet though, you just need to find an opening " +
					"and turn things around. A noise behind you causes you to turn and your vision is " +
					"filled with two piercing red eyes. <i>\"Kneel.\"</i> You drop to your knees involuntarily. " +
					"The rational part of your brain is telling you that Reyka is trying to dominate your " +
					"mind and you should resist, but what's the point? Reyka's tail binds your wrists and " +
					"she forces you to turn back to a bewildered "+assist.name()+". <i>\"I'm not poaching " +
					"your prey,\"</i> you hear her say. <i>\"He's all yours.\"</i>";

		return "Your fight with "
				+ target.name
				+ " starts out poorly; she already"
				+ " has you naked and aroused, whereas she seems as cool and calm as when"
				+ " you started. Fortune, though, seems to have a strange sense of humor as"
				+ " your salvation comes in the form of a winged demon swooping down on "
				+ target.name
				+ ". The two are briefly entangled in a ball of limbs and wings,"
				+ " but soon Reyka comes out on top. She is pinning "
				+ target.name
				+ " helplessly to the ground, holding her arms behind her back and"
				+ " locking her shoulders in place with her wings. The struggle has "
				+ "left " + target.name
				+ " completely naked and ready for you to"
				+ " take advantage of.";
	}
	
	public String watched(Combat c, Character target, Character viewer){
		return "Just being in the same room with Reyka is dizzying. If she's actively trying to get to you, a bout with her is like fighting through a fever dream.<p>"
					+ "It's easier to tell what she's doing when she's not the one you're focusing on. From this perspective, you can tell she's just toying with "+target.name()+". This is just about over.<p>"
					+ "Reyka feigns a moment of vulnerability, backpedals, and disappears around a blind corner. "+target.name()+" thinks she's on the verge of winning, shakes her head to clear it, and runs after her.<p>"
					+ "They've found a dark part of the building, where the lights don't quite reach, and the shadows erupt as "+target.name()+" passes them. Tendrils of wet, living darkness wrap around her wrists and ankles. She puts up a fight, wrenching her hands and legs free one at a time, but every tendril that breaks or loses its grip is replaced by two more.<p>"
					+ "You can barely see Reyka, half-hidden by shadows that are a little too deep to be entirely natural. She's got both hands up, directing the eruptions of darkness like a conductor with a baton.<p>"
					+ "A few minutes later, it's over. The shadows drag "+target.name()+" towards the wall, then against it and a couple of inches into the air, with her hands and feet obscured from view. Reyka's shoulders sag with what might be relief, and she walks towards "+target.name()+" with a little extra swagger in her hips.<p>"
					+ "She tears away the remnants of "+target.name()+"'s clothing, and leans in to rest her forehead against "+target.name()+"'s. Whatever she says, it makes "+target.name()+" wriggle furiously against her bonds, but it does no good. It's like her limbs are sunk to the joints in concrete.<p>"
					+ "Reyka laughs, delighted, and kisses her way down [Opponent's] body, slow and unhurried, just as if they aren't in a competition or at risk of being interrupted. She pauses to find and torment all of "+target.name()+"'s most sensitive areas, her collarbone, her nipples, the smooth plane of skin just below her navel. The tendrils move "+target.name()+"'s legs a bit further apart, and Reyka's mouth fastens onto "+target.name()+"'s slit.<p>"
					+ "She makes a sound like she wasn't expecting it to taste this good and dives in, making "+target.name()+" writhe and arch her back against the wall. It's not long before "+target.name()+" cries out, then sags against her bonds, coming down from the orgasm she couldn't keep herself from having.<p>"
					+ "Reyka looks up at her, licking "+target.name()+"'s taste off her lips and cheeks with a tongue that's just a little too long. "+target.name()+" says something, dejected, and Reyka answers it with a deep, messy kiss.<p>"
					+ "It looks like it might just be some kind of polite thank-you for the meal until you notice Reyka's tail rise up from behind her. It snakes between Reyka's legs, and "+target.name()+"'s eyes widen as its tip pokes at her pussy. She looks down, surprised, but not as surprised as someone else might be. It's the Games, after all, and it's not like Reyka hides her tail.<p>"
					+ "Reyka murmurs something. "+target.name()+" closes her eyes, then nods, just once. Reyka grins, showing more teeth than a human has, and sinks a few slow inches of her tail into "+target.name()+"'s pussy.<p>"
					+ "She rides its surface as she slides it in and out of "+target.name()+". Reyka pulls "+target.name()+" close against her with her arms around her waist, kissing her, as her tail pushes in a bit further, then stops. She asks a question, "+target.name()+" nods again, and Reyka begins to move the tail faster, out, then back in, half a foot or more of it disappearing into "+target.name()+" in each quick, sinuous thrust.<p>"
					+ "Reyka clings to "+target.name()+" for support as she brings herself off with her tail, grinding her clit against its surface as she fucks "+target.name()+" with it. Reyka pushes "+target.name()+"'s sweaty hair out of her face and says something; whatever it is makes "+target.name()+"'s eyes widen and face go blank for a second, despite the sensations, and Reyka laughs again. It trails off into a throaty, room-filling moan as she comes.<p>"
					+ "She waits a few minutes longer, until "+target.name()+"'s body shakes with one more orgasm, before Reyka pulls herself away. Reyka puts a final kiss against the tip of "+target.name()+"'s nose, then gathers her clothing from the floor and saunters away, directly into the shadows at the end of the hall.<p>"
					+ "You're almost positive there's no door there, but Reyka's gone anyway.<p>"
					+ "A few seconds later, the tendrils holding "+target.name()+" to the wall dissolve away into nothing. She falls to the floor, catching herself on her hands, breathing hard, before collecting herself and moving on.";
	}

	@Override
	public String describe() {
		return "Reyka the succubus stands before you, six feet tall with"
				+ " the most stunningly beautiful body you have ever seen."
				+ " Her long black hair enshrines her perfect face like a priceless"
				+ " painting, her breasts are large - yet not overly so -"
				+ " and impossibly firm. Her arms are slim and end in long-fingered,"
				+ " soft hands, nails polished shining red. Underneath, her long and"
				+ " perfectly formed legs and delicate feet stand in an imposing posture."
				+ " Behind her, you see a long, arrow tipped tail slowly waving around, as"
				+ " well as a pair of relatively small but powerful-looking bat wings.<br>"
				+ " Her gaze speaks of indescribable pleasure, but your mind reminds you"
				+ " of the cost of indulging in a succubus' body: Give her half a chance"
				+ " and she will suck out your very soul.";
	}

	@Override
	public String draw(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		return "As you and Reyka are both thrusting against each other for all you're worth, you feel your inevitable climax approaching very rapidly.<p>"
				+ "Just " +
				"before you erupt into her, you notice a strange change come over her. Her tail and wings seem to evaporate before you and her white skin gains " +
				"a slight tan. The challenging look in her eyes and confident little smile are replaced by an expression of surprise and shock. But you're barely aware " +
				"of any of this. All you can see are her crimson irises giving way to a deep, radiant blue.<p>"
				+ "Just as you spot what you think might be a single tear " +
				"forming in her right eye, both of you reach your orgasms. You were expecting to feel her power wash over you, to feel it pour into every nook " +
				"and cranny of your soul and then draw that soul out and into her, but none of that happens. The only sensations are the pure, tantric bliss of " +
				"simultaneous climax and a strange affection for the two deep blue eyes staring into yours.<p>"
				+ "When eventually the paradise you found yourself in " +
				"once again gives way to the cold bitterness of reality and you see the first red spots already reappearing in Reyka's eyes, your " +
				"rapidly souring mood is mollified by her passionate, warm embrace. She just holds you like that, not speaking, not moving, even as the demonic parts " +
				"of her anatomy return to her shapely form.<p>"
				+ "After a few minutes, which might as well have been days for you, she lets go of you, leans " +
				"back some, and softly speaks the two words you least expected ever to hear from her: <i>\"Thank you.\"</i><p>"
				+ "With that, she leaves what few clothes she " +
				"usually wears, turns around and walks away. Not the domineering, seductively swaying stride you have grown used to, but rather a slower, more " +
				"composed walk with her head slightly bowed. You are absolutely perplexed by this rare display of emotion, but after a while you gather your wits " +
				"- and her clothes - and take off into the night. In the back of your mind, you know she will be back to hunt you down later, but this experience " +
				"will remain entrenched in you memory for quite some time.";
	}
	
	@Override
	public boolean fightFlight(Character paramCharacter) {
		return !this.character.nude() || Global.random(3) == 1;
	}

	@Override
	public boolean attack(Character paramCharacter) {
		return !this.character.nude() || Global.random(3) == 1;
	}

	@Override
	public void ding() {
		if(character.getLevel() >= 30){
			int rand = Global.random(3);
			if (rand == 0) {
				this.character.mod(Attribute.Power, 1);
			} else if (rand == 1) {
				this.character.mod(Attribute.Seduction, 1);
			} else if (rand == 2) {
				this.character.mod(Attribute.Cunning, 1);
			}
			character.getStamina().gain(2);
			character.getArousal().gain(3);
		}
		else{
			this.character.mod(Attribute.Dark, 1);
			for (int i = 0; i < (Global.random(3)/2)+1; i++) {
				int rand = Global.random(3);
				if (rand == 0) {
					this.character.mod(Attribute.Power, 1);
				} else if (rand == 1) {
					this.character.mod(Attribute.Seduction, 1);
				} else if (rand == 2) {
					this.character.mod(Attribute.Cunning, 1);
				}
			}
			character.getStamina().gain(4);
			character.getArousal().gain(6);
		}
	}

	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case enthralling:
				return "As you approach Reyka, her eyes suddenly flash bright red and you find yourself unable to look away. She slowly, deliberately "
						+ "approaches you and firmly grabs your crotch.<br>"
						+ "<i>\"Sometimes I think you underestimate me, just because you make me feel good. You're just a human, don't forget your place.\"</i> "
						+ "Against your will, you find yourself nodding to her. Her dark power and personality are too much for you to overcome "
						+ "with willpower alone.<p>"
						+ "She smiles, satisfied, and releases her grip on you. <i>\"If you understand, then we can begin.\"</i>";
			case succubusvagina:
				return "As you and Reyka face off, she grins wickedly at you and flashes her pussy. <i>\"Did you enjoy fucking me last time? I never thought a "
						+ "sexfighter would willingly stick his dick in a succubus, so I wasn't ready. Now that I know how much you want to cum inside me, "
						+ "I'll show you my favorite way to drain a man dry.\"</i>";
			case darkness:
				return "As you prepare to fight Reyka, you suddenly freeze in place. Something is different about her. She's radiating an intense, malicious "
						+ "aura. What the hell happened to her?<p>"
						+ "<i>\"To me? Nothing.\"</i> Reyka extends her wings in an intimidating manner. <i>\"Did the human forget I was a demon? Just because the "
						+ "cheeky human made me cum first, he forgot I'm the daughter of one of the most prestigious infernal families? Maybe that's what happened "
						+ "to me. What do you think is about to happen to that cheeky little human?\"</i><p>"
						+ "Oh shit, she's really holding a grudge over her last loss. She'll be going all out this time.";
			default:
				break;		
			}
		}
		if(character.nude()){
			return "Reyka coyly covers her naked body with her wings. <i>\"Don't you have any decency? Shouldn't you look away from an undressed lady?\"</i>"
					+ "She laughs and folds her wings behind her, exposing herself. <i>\"I guess you can't take your eyes off me. Fortunately, I'm not easily embarrassed.\"</i>";
		}
		if(opponent.pantsless()){
			return "Reyka's eyes focus on your exposed groin. <i>\"What a delicious looking cock you have. I'd like to taste that a couple "
					+ "of different ways. What do you say?\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Reyka gives you a smile that's more pleasant than predatory. <i>\"You're quickly becoming my favorite human. My prey isn't "
					+ "usually such good company.\"</i> She licks her lips, and you see a dangerous glint in her eyes. <i>\"Of course, that makes "
					+ "your seed even tastier. Don't blame me if I get a little carried away.\"</i>";
		}

		return "<i>\"Yum, I was just looking for a tasty little morsel.\"</i><p>"
				+ "Reyka strikes a seductive pose and the devilish smile"
				+ " on her face reveals just what, or more specifically,"
				+ " who she intends that morsel to be.";
	}

	@Override
	public boolean fit() {
		return (!this.character.nude() || Global.random(3) == 1)
				&& (this.character.getStamina().percent() >= 50)
				&& (this.character.getArousal().percent() <= 50);
	}

	@Override
	public String night() {
		return "You feel exhausted after yet another night of sexfighting. You're not complaining, of course; "+
				"what guy would when having this much sex with several different girls? Still, a weekend would "+
				"be nice sometime...<p>"
				+ "About half way to your room, Reyka steps in front of you. Where did she come from? "+
				"<i>\"Listen, "+Global.getPlayer().name()+", I've been doing some thinking lately. You know very well I've had sex with a lot "+
				"of guys and a fair amount of girls, too, right?\"</i><p>"
				+ "You just nod, wondering where this is going. <i>\"Well, "+
				"in all that time no one has ever made me feel the way you can. I don't know why, really, but I can't help "+
				"feeling there's something special about you.\"</i><p>"
				+ "You stand there, paralyzed, with a look of amazement "+
				"on your face. Reyka intimidates you. Hell, she is downright terrifying at times. To see and hear "+
				"her like this is like nothing you had ever expected from her. For a moment, you think this is all some "+
				"elaborate trick of some sort, but that thought vanishes the instant you see tears welling in her eyes.<p>"+
				"<i>\"I just... We demons aren't supposed to feel like this, you know? We don't form relationships. It's all "+
				"just a constant power struggle, constant scheming and looking over your shoulder and sleeping with a "+
				"knife under your pillow. It has never bothered me before; it's simply what I am. That's what I used to "+
				"think, anyway. Now, I'm not so sure... about anything...\"</i> She quitely sobs while saying this, and you "+
				"embrace her.<p>"
				+ "You hold her there for some time, before inviting her to spend the night at your place. "+
				"You don't even have sex when you get there, you just both lay down in your single bed, close to "+
				"each other, and enjoy a peaceful sleep together with your arms around her and her head on your shoulder.";
	}

	@Override
	public void advance(int rank) {
		if(rank >= 3 && !character.has(Trait.infernalexertion)){
			character.add(Trait.infernalexertion);
		}
		if(rank >= 2 && !character.has(Trait.royalguard)){
			character.add(Trait.royalguard);
		}
		
	}
	@Override
	public NPC getCharacter() {
		return character;
	}
	
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case dominant:
			return value>=25;
		case nervous:
			return value>=80;
		default:
			return value>=50;
		}
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case dominant:
			return 1.2f;
		case nervous:
			return .7f;
		default:
			return 1f;
		}
	}

	@Override
	public String image() {
		return "assets/reyka_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Ah, yes! Give me more!\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"I can't lose! Not like this!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Are you regretting fucking me now? It's time for you to cum now.\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Damn it! How did you get this good?!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"Thought my ass would be harmless, did you?\"</i>");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Mmmm. I wonder if you'll cum right away if I put it in. Shall we find out?\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_WIN, "<i>\"Hah! Even on top it's hopeless! Now fuck me and finish it!\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"Please... At least feed me...\"</i>");
		comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"You're a good little slave, aren't you\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"You really just can't resist me, can you?\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Reyka covers her pussy and moans deeply, though it is impossible to tell if it is from pain or pleasure.");
		return comments;
	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(character.getGrudge()==Trait.darkness || character.getGrudge() == Trait.succubusvagina){
			character.addGrudge(opponent, Trait.enthralling);
		}else if(c.eval(character)==Result.intercourse){
			character.addGrudge(opponent, Trait.succubusvagina);
		}else{
			character.addGrudge(opponent, Trait.darkness);
		}
		
	}
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        this.character.outfit[0].add(Clothing.lbustier);
        this.character.outfit[1].add(Clothing.lminiskirt);
    }
}
