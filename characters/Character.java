package characters;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import global.*;

import items.Clothing;
import items.ClothingType;
import items.Component;
import items.Consumable;
import items.Envelope;
import items.Flask;
import items.Item;
import items.Potion;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;
import java.util.Scanner;
import java.util.Stack;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.print.attribute.standard.Copies;
import javax.swing.Icon;

import stance.Position;

import pet.Pet;

import skills.Distracted;
import skills.PullOut;
import skills.Skill;
import skills.Finger;
import skills.FondleBreasts;
import skills.Fuck;
import skills.Handjob;
import skills.Kiss;
import skills.Masochism;
import skills.Nothing;
import skills.Nurple;
import skills.Recover;
import skills.Restrain;
import skills.ReverseStraddle;
import skills.Shove;
import skills.Slap;
import skills.Spank;
import skills.Squeeze;
import skills.Straddle;
import skills.StripBottom;
import skills.StripTop;
import skills.Struggle;
import skills.Stunned;
import skills.Tactics;
import skills.Tickle;
import skills.UseFlask;
import skills.UsePotion;
import status.*;
import trap.Trap;

import combat.Combat;
import combat.Encounter;
import combat.Result;

import actions.Move;
import actions.Movement;
import areas.Area;
import areas.NinjaStash;


public abstract class Character extends Observable implements Cloneable{
	/**
	 * 
	 */
	protected String name;
	protected ID identity;
	protected int level;
	protected int xp;
	protected int rank;
	public int money;
	public HashMap<Attribute,Integer> att;
	protected Meter stamina;
	protected Meter arousal;
	protected Meter mojo;
	public Stack<Clothing> top;
	public Stack<Clothing> bottom;
	protected Area location;
	public Stack<Clothing>[] outfit;
	protected HashSet<Skill> skills;
	protected HashSet<Skill> flaskskills;
	protected HashSet<Skill> potionskills;
	protected HashSet<Status> status;
	public HashSet<Trait> traits;
	//public HashSet<Status> removelist;
	public HashSet<Status> addlist;
	private HashSet<Character> mercy;
	protected ArrayList<Item> inventory;
	protected Item underwear;
	public State state;
	protected int busy;
	public HashMap<Emotion,Integer> emotes;
	public Emotion mood;
	public HashSet<Clothing> closet;
	public Pet pet;
	public ArrayList<Challenge> challenges;
	protected HashMap<Character,Trait> grudges;
	protected Trait activeGrudge;
	protected Trait matchmod;
	
	BufferedImage spriteBody = null;
	BufferedImage spriteAccessory = null;
	BufferedImage spriteAccessory2 = null;
	BufferedImage spriteUnderwear = null;
	BufferedImage spriteStrapon = null;
	BufferedImage spriteBra = null;
	BufferedImage spriteBottom = null;
	BufferedImage spriteTop = null;
	BufferedImage spriteOuter = null;
	BufferedImage spriteCoattail = null;
	BufferedImage spritePenisSoft = null;
	BufferedImage spritePenisHard = null;
	BufferedImage spriteKemono = null;
	BufferedImage blushLow = null;
	BufferedImage blushMed = null;
	BufferedImage blushHigh = null;
	
	
	public static final int OUTFITTOP = 0;
	public static final int OUTFITBOTTOM = 1;
	
	public Character(String name, int level){
		this.name=name;
		this.level=level;
		att = new HashMap<Attribute,Integer>();
		att.put(Attribute.Power, Constants.STARTINGPOWER);
		att.put(Attribute.Cunning, Constants.STARTINGCUNNING);
		att.put(Attribute.Seduction, Constants.STARTINGSEDUCTION);
		att.put(Attribute.Perception, Constants.STARTINGPERCEPTION);
		att.put(Attribute.Speed, Constants.STARTINGSPEED);
		this.money=0;
		stamina=new Meter(Constants.STARTINGSTAMINA,this);
		stamina.fill();
		arousal = new Meter(Constants.STARTINGAROUSAL,this);
		mojo = new Meter(Constants.STARTINGMOJO,this);
		top = new Stack<Clothing>();
		bottom = new Stack<Clothing>();
		outfit = new Stack[2];
		outfit[0]=new Stack<Clothing>();
		outfit[1]=new Stack<Clothing>();
		closet = new HashSet<Clothing>();
		skills = new HashSet<Skill>();
		flaskskills = new HashSet<Skill>();
		potionskills = new HashSet<Skill>();
		status = new HashSet<Status>();
		traits = new HashSet<Trait>();
		//removelist = new HashSet<Status>();
		addlist = new HashSet<Status>();
		mercy=new HashSet<Character>();
		inventory=new ArrayList<Item>();
		challenges=new ArrayList<Challenge>();
		grudges = new HashMap<Character, Trait>();
		location=new Area("","",null);
		state=State.ready;
		busy=0;
		setRank(0);
		this.pet=null;
		this.activeGrudge = null;
		emotes = new HashMap<Emotion,Integer>();
		mood = Emotion.confident;
		for(Emotion e: Emotion.values()){
			emotes.put(e, 0);
		}
		skills.add(new Struggle(this));
		skills.add(new Nothing(this));
		skills.add(new Recover(this));
		skills.add(new Straddle(this));
		skills.add(new ReverseStraddle(this));
		skills.add(new Stunned(this));
		skills.add(new Distracted(this));
		skills.add(new PullOut(this));
		for(Flask f: Flask.values()){
			flaskskills.add(new UseFlask(this,f));
		}
		for(Potion p: Potion.values()){
			potionskills.add(new UsePotion(this,p));
		}
	}
	public Character clone() throws CloneNotSupportedException {
	    Character c = (Character) super.clone();
	    c.att = (HashMap) att.clone();
		c.stamina = (Meter) stamina.clone();
		c.arousal = (Meter) arousal.clone();
		c.mojo = (Meter) mojo.clone();
		c.top = (Stack) top.clone();
		c.bottom = (Stack) bottom.clone();
		c.outfit = (Stack[]) outfit.clone();
		c.skills = (HashSet) skills.clone();
		c.status = new HashSet<Status>();
		for (Status s: status){
			c.status.add(s.copy(c));
		}
		c.traits = (HashSet) traits.clone();
		//c.removelist = (HashSet) removelist.clone();
		c.addlist = (HashSet) addlist.clone();
		c.mercy = (HashSet) mercy.clone();
		c.inventory = (ArrayList) inventory.clone();
		c.skills = (HashSet) skills.clone();
		c.emotes = (HashMap) emotes.clone();
		c.activeGrudge = activeGrudge;
		c.mood = mood;
		return c;
	}
	public String name(){
		return name;
	}
	public ID id(){ return identity; }
	public int get(Attribute a){
		int total =0;
		if(att.containsKey(a)){
			total = att.get(a);
		}
		for(Status s:status){
			total+=s.mod(a);
		}
		switch(a){
		case Power:
			if(has(Trait.powerup)){
				total += 10;
			}
			if(has(Trait.mighty)){
				total += 2;
			}
			if(has(Trait.revvedup)){
				if(getArousal().percent()>25){
					total+=5;
				}
				if(getArousal().percent()>50){
					total+=5;
				}
				if(getArousal().percent()>75){
					total+=5;
				}
			}
			break;
		case Cunning:
			if(has(Trait.inspired)){
				total += 10;
			}
			break;
		case Seduction:
			if(has(Trait.confidentdom)&&(getMood()==Emotion.dominant || getMood()==Emotion.confident)){
				total *= 2;
			}
			if(has(Trait.revvedup)){
				if(getArousal().percent()>25){
					total+=5;
				}
				if(getArousal().percent()>50){
					total+=5;
				}
				if(getArousal().percent()>75){
					total+=5;
				}
			}
			break;
		case Arcane:
			if(has(Trait.mystic)){
				total+=2;
			}
			break;
		case Dark:
			if(has(Trait.broody)){
				total+=2;
			}
			if(has(Trait.darkness)){
				total += 10;
			}
			break;
		case Ki:
			if(has(Trait.martial)){
				total+=2;
			}
			break;
		case Fetish:
			if(has(Trait.kinky)){
				total+=2;
			}
			if(has(Trait.revvedup)){
				if(getArousal().percent()>25){
					total+=5;
				}
				if(getArousal().percent()>50){
					total+=5;
				}
				if(getArousal().percent()>75){
					total+=5;
				}
			}
			break;
		case Science:
			if(has(Trait.geeky)){
				total+=2;
			}
			break;
		case Ninjutsu:
			if(has(Trait.stealthy)){
				total+=2;
			}
			break;
		case Animism:
			if(has(Trait.furry)){
				total+=2;
			}
			break;
		case Speed:
			for(Clothing article:top){
				if(article.attribute()==Trait.bulky){
					total--;
				}
			}
			for(Clothing article:bottom){
				if(article.attribute()==Trait.bulky){
					total--;
				}
			}
			if(has(Trait.streaker)&& nude()){
				total += 4;
			}
			if(has(Trait.flash)){
				total += 10;
			}
			break;
		}
		if(total<0){
			return 0;
		}
		return total;
	}
	public int getPure(Attribute a){
		int total =0;
		if(att.containsKey(a)){
			total = att.get(a);
		}
		return total;
	}
	public int getAdvanced(){
		int total = 0;
		for(Attribute a : att.keySet()){
			if(a == Attribute.Science || a == Attribute.Arcane || 
			   a == Attribute.Animism || a == Attribute.Dark || 
			   a == Attribute.Fetish || a == Attribute.Ki || 
			   a == Attribute.Ninjutsu || a == Attribute.Professional){
				total += att.get(a);
			}
		}
		return total;
	}
	public int getAdvancedTrainingCost(){
		return 400 + Constants.TRAININGSCALING*getAdvanced();
	}
	public boolean hasSpecialization(){
	    for(Attribute a: att.keySet()){
	        if(a.isSpecialization()){
	            return true;
            }
        }
        return false;
    }
	public void mod(Attribute a, int i){
		if(att.containsKey(a)){
			att.put(a, att.get(a)+i);
		}
		else{
			set(a,i);
		}
	}
	public void set(Attribute a, int i){
		att.put(a, i);
	}
	public boolean check(Attribute a, int dc){
		if(get(a)==0){
			return false;
		}
		else{
			return get(a)+Global.random(20)>=dc;
		}
	}
	public int getLevel(){
		return level;
	}
	public void gainXP(int i){
		float scale = 1f;
		if(has(Trait.fastLearner)){
			scale += 0.1f;
		}
		if(has(Trait.veryfastLearner)){
			scale += 0.1f;
		}
		if(Global.checkFlag(Flag.doublexp)){
			scale += 1f;
		}
		if(has(Trait.hiddenpotential)){
			scale += 0.3f;
		}
		xp += Math.round(i*scale);
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public void rankup(){
		this.rank++;
	}
	public abstract void ding();
	public int getXP(){
		return xp;
	}
//Damage
	public void scaleLevel(int targetLevel){
		while(level<targetLevel){
			ding();
		}
		xp = 0;
	}
	public int pain(int i, Anatomy area, Combat c){
		int pain = Math.max(1,i);
		if(area == Anatomy.genitals){
			c.lowBlow(this);
		}
		pain = pain(i,area);
		c.reportPain(this, pain);
		return pain;
	}
	public int pain(int i, Anatomy area){
		int pain = Math.max(1,i);
		for(Status s: status){
			pain+=s.damage(i,area);
			pain*=s.sore(area);
		}
		if(area == Anatomy.genitals){
			if(has(Trait.achilles)){
				pain *= 1.5;
			}
			if(has(Trait.brassballs)){
				pain *= 0.8;
			}
			if(has(Trait.armored)){
				pain *= 0.3;
			}
		}
		emote(Emotion.angry,pain);
		stamina.reduce(pain);
		return pain;
	}
	public int weaken(int i, Combat c){
		int weak = weaken(i);
		c.reportWeaken(this, weak);
		return weak;
	}
	public int weaken(int i){		
		int weak = Math.max(1,i);
		for(Status s: status){
			weak+=s.weakened(i);
		}
		if(weak>=stamina.get()){
			weak = stamina.get()-1;
		}
		emote(Emotion.nervous,weak);
		stamina.reduce(weak);
		return weak;
	}
	public int heal(int i, Combat c){
		int h = heal(i);
		c.reportHeal(this, h);
		emote(Emotion.confident,h);
		return h;
	}
	public int heal(int i){
		stamina.restore(i);
		return i;
	}
	public int pleasure(int i, Anatomy area, Combat c){
		return pleasure(i,area, Result.normal, c);
	}
	public int pleasure(int i, Anatomy area, Result mod, Combat c){
		int pleasure = Math.max(1,i);
		float value = Math.max(1,i);
		if(mod==Result.foreplay){
			pleasure=Math.round((value*.2f)+(value*.8f*(Math.min(1, 1.25f-(getArousal().get()/(.8f*getArousal().max()))))));
		}
		if(mod==Result.finisher){
			pleasure=Math.round((value*.2f)+(value*.8f*(Math.min(1, getArousal().get()/(.8f*getArousal().max())))));
		}
		pleasure = pleasure(pleasure,area);
		c.reportPleasure(this, pleasure, mod);
		return pleasure;
	}
	public int pleasure(int i, Anatomy area){
		int pleasure = Math.max(1,i);
		for(Status s: status){
			pleasure+=s.pleasure(i,area);
			pleasure*=s.sensitive(area);
		}
		if(has(Trait.desensitized)){
			pleasure-=1;
		}
		if(has(Trait.hairtrigger)&&(area == Anatomy.genitals)){
			pleasure*=2;
		}
		if(has(Trait.veteranprostitute)&& area == Anatomy.genitals){
			pleasure*=0.75;
		}
		if(has(Trait.silvercock)&& area == Anatomy.genitals){
			pleasure*=0.9;
		}
		if(has(Trait.hardon)&&(area == Anatomy.genitals)){
			pleasure*=1.3;
		}
		if(has(Trait.buttslut)&&(area == Anatomy.ass)){
			pleasure*=2;
		}
		if(area == Anatomy.chest){
			if(hasBreasts()){
				pleasure *= 1.2;
			}
		}
		pleasure = Math.max(1,i);
		arousal.restore(pleasure);
		return pleasure;
	}
	public int tempt(int i,Combat c){
		return tempt(i,Result.normal,c);
	}
	public int tempt(int i, Result mod, Combat c){
		int temptation = i;
		temptation = tempt(temptation,mod);
		c.reportTemptation(this, temptation, Result.normal);
		return temptation;

	}
	public int tempt(int i){
		if(!arousal.isFull()){
			int temptation = Math.max(1,i);
			for(Status s: status){
				temptation+=s.tempted(i);
			}
			if(has(Trait.imagination)){
				temptation *= 1.5f;
			}
			if(has(Trait.icequeen)){
				temptation *= 0.5f;
			}
			arousal.restore(temptation);
			if(arousal.isFull()){
				arousal.reduce(1);
			}
			emote(Emotion.horny,Math.round(temptation));
			return temptation;
		}else{
			return 0;
		}
	}
	public int tempt(int i, Result mod){
		float value = Math.max(1,i);
		if(mod==Result.foreplay){
			value=(value*.2f)+(value*.8f*(Math.min(1, 1.25f-(getArousal().get()/(.8f*getArousal().max())))));
		}
		if(mod==Result.finisher){
			value=(value*.2f)+(value*.8f*(Math.min(1, getArousal().get()/(.8f*getArousal().max()))));
		}
		return tempt(Math.round(value));
	}
	public int calm(int i,Combat c){
		int mag = calm(i);
		c.reportCalm(this, mag);
		return mag;
	}
	public int calm(int i){
		arousal.reduce(i);
		return i;
	}
	public Meter getStamina(){
		return stamina;
	}
	public Meter getArousal(){
		return arousal;
	}
	public Meter getMojo(){
		return mojo;
	}
	public void modMojo(int i){
		mojo.restore(i);
		if(mojo.get()<0){
			mojo.set(0);
		}
	}
	public void buildMojo(int percent){
		int x = (percent*mojo.max())/100;
		for(Status s: status){
			x+=s.gainmojo(x);
		}
		if(has(Trait.lameglasses) || has(Trait.legend)){
			x = 0;
		}
		mojo.restore(x);
	}
	public void spendMojo(int i){
		int cost = i;
		for(Status s: status){
			cost+=s.spendmojo(i);
		}
		if(has(Trait.overflowingmana)){
			cost = cost/20;
		}
		mojo.reduce(cost);
		if(mojo.get()<0){
			mojo.set(0);
		}
	}
	public void spendArousal(int i){
		if(has(Trait.infernalexertion)){
			weaken(i);
		}else{
			tempt(i);
		}
		
	}
	public void spendStamina(int i){
		weaken(i);
	}
	public Area location(){
		return location;
	}
	public boolean isBusy(){
		return state != State.ready && state != State.quit;
	}
	public int init(Combat c){
		int s = att.get(Attribute.Speed)+Global.random(10);
		if(c.stance.sub(this)){
			s -= 2;
		}
		if(c.stance.prone(this)){
			s -= 2;
		}
		return s;
	}
//Clothing
	public boolean nude(){
		return topless()&&pantsless();
	}
	public boolean topless(){
		for(Clothing article: top){
			if(article.attribute()!=Trait.ineffective){
				return false;
			}
		}
		return true;
	}
	public boolean pantsless(){
		for(Clothing article: bottom){
			if(article.attribute()!=Trait.ineffective){
				return false;
			}
		}
		return true;
	}
	public boolean canFuck(){
		for(Clothing article: bottom){
			if(article.attribute()!=Trait.ineffective && article.attribute()!=Trait.accessible){
				return false;
			}
		}
		return true;
	}
	public void dress(Combat c){
		for(Clothing article: outfit[0]){
			if(c.clothespile.contains(article)&&!top.contains(article))
			  top.push(article);
		}
		for(Clothing article: outfit[1]){
			if(c.clothespile.contains(article)&&!bottom.contains(article))
			  bottom.push(article);
		}
	}
	public void change(){
		change(Modifier.normal);
	}
	public void change(Modifier rule){
		top=(Stack<Clothing>) outfit[0].clone();
		bottom=(Stack<Clothing>) outfit[1].clone();
	}
	public void undress(Combat c){
		c.clothespile.addAll(top);
		c.clothespile.addAll(bottom);
		ArrayList<Clothing> clist = (ArrayList<Clothing>) c.clothespile.clone();
		for(Clothing item: clist){
			if(item.isTemp()){
				c.clothespile.remove(item);
			}
		}
		top.clear();
		bottom.clear();
	}
	public boolean nudify(){
		ArrayList<Clothing> temp = new ArrayList<Clothing>();
		temp.addAll(top);
		for(Clothing article: temp){
			if(article.attribute()!=Trait.indestructible){
				top.remove(article);
			}
		}
		temp.clear();
		temp.addAll(bottom);
		for(Clothing article: temp){
			if(article.attribute()!=Trait.indestructible){
				bottom.remove(article);
			}
		}
		return nude();
	}
	public boolean stripAttempt(int primaryAtt, Character attacker, Combat c, Clothing target){
		if(!canAct()){
			return true;
		}
		float magnitude = primaryAtt*attacker.getStamina().percent()/100;
		float dc = target.dc()+(getLevel()*getStamina().percent()/100);
		if(has(Trait.modestlydressed)){
			dc += 10;
		}
		dc *= 1 - (getArousal().percent()/200);
		
		magnitude *= target.stripOffenseBonus(attacker);
		if(c.stance.dom(attacker)){
			magnitude *= 1.2;
		}
		return (Global.random(20)+magnitude > dc+Global.random(10));
	}
	public Clothing strip(int half, Combat c){
		if(half==0){
			if(topless()){
				return null;
			}
			else{
				if(!top.peek().isTemp()){
					c.clothespile.add(top.peek());
				}
				return top.pop();
			}
		}
		else{
			if(pantsless()){
				return null;
			}
			else{
				if(!bottom.peek().isTemp()){
					c.clothespile.add(bottom.peek());
				}
				return bottom.pop();
			}
		}
	}
	public Clothing stripRandom(Combat c){
		if(Global.random(2)==0){
			if(!top.isEmpty()){
				return strip(0,c);
			}else{
				return strip(1,c);
			}
		}else{
			if(!bottom.isEmpty()){
				return strip(1,c);
			}else{
				return strip(0,c);
			}
		}
	}

	public Clothing shred(int half){
		if(half==0){
			if(!topless()&&top.peek().attribute()!=Trait.indestructible){
				return top.pop();
			}
			else{
				return null;
			}
		}
		else{
			if(!pantsless()&&bottom.peek().attribute()!=Trait.indestructible){
				return bottom.pop();
			}
			else{
				return null;
			}
		}
	}
	public Clothing shredRandom(){
		if(Global.random(2)==0){
			if(!top.isEmpty()){
				return shred(OUTFITTOP);
			}else{
				return shred(OUTFITBOTTOM);
			}
		}else{
			if(!bottom.isEmpty()){
				return shred(OUTFITBOTTOM);
			}else{
				return shred(OUTFITTOP);
			}
		}
	}
	public boolean canWear(Clothing article){
		return !isWearing(article.getType());
	}
	public boolean isWearing(ClothingType type){
		for(Clothing worn: top){
			if(worn.getType()==type){
				return true;
			}
		}
		for(Clothing worn: bottom){
			if(worn.getType()==type){
				return true;
			}
		}
		return false;
	}
	public void removeLayer(ClothingType type){
		ArrayList<Clothing> copy = new ArrayList<Clothing>();
		copy.addAll(top);
		for(Clothing worn: copy){
			if(worn.getType()==type){
				top.remove(worn);
			}
		}
		copy.clear();
		copy.addAll(bottom);
		for(Clothing worn: bottom){
			if(worn.getType()==type){
				bottom.remove(worn);
			}
		}
	}
	public void wear(Clothing article){
		switch(article.getType()){
		case TOPUNDER:
			top.insertElementAt(article, 0);
			break;
		case UNDERWEAR:
			bottom.insertElementAt(article, 0);
			break;
		case TOP:
			if(isWearing(ClothingType.TOPUNDER)){
				top.insertElementAt(article, 1);
			}
			else{
				top.insertElementAt(article, 0);
			}
			break;
		case TOPOUTER:
			top.push(article);
			break;
		default:
			bottom.push(article);
		}
	}
	public void replaceArticle(Clothing article){
		if(canWear(article)){
			wear(article);
		}else{
			removeLayer(article.getType());
			wear(article);
		}
	}
	public int getSkimpiness(){
		int result = 2;
		for(Clothing article: top){
			if(article.attribute()!=Trait.skimpy&&article.attribute()!=Trait.ineffective){
				result--;
				break;
			}
		}
		for(Clothing article: bottom){
			if(article.attribute()!=Trait.skimpy&&article.attribute()!=Trait.ineffective){
				result--;
				break;
			}
		}
		if(has(Trait.scandalous)){
			result += 1;
		}
		return result;
	}
	public Clothing getOutfitItem(ClothingType slot){
		for(Clothing c: outfit[0]){
			if(c.getType()==slot){
				return c;
			}
		}
		for(Clothing c: outfit[1]){
			if(c.getType()==slot){
				return c;
			}
		}
		return null;
	}
	public String getTop(){
		if(topless()){
			return "shirt";
		}
		return top.peek().getName();
	}
	public String getBottom(){
		if(!pantsless()){
			if(bottom.peek().getType()==ClothingType.BOTOUTER){
				return bottom.peek().getName();
			}
		}
		return "pants";
	}

	
	public void add(Trait t){
		traits.add(t);
	}
	public void remove(Trait t){
		traits.remove(t);
	}
	public boolean has(Trait t){
		for(Clothing shirt:top){
			if(shirt.adds(t)){
				return true;
			}
		}
		for(Clothing pants:bottom){
			if(pants.adds(t)){
				return true;
			}
		}
		if(activeGrudge == t){
			return true;
		}
		if(matchmod == t){
			return true;
		}
		for(Status s:status){
			if (s.tempTraits(t)){
				return true;
			}
		}
		return traits.contains(t);
	}
	public void setModifier(Trait t){
		matchmod = t;
	}
//Anatomy Checks
	public boolean hasDick(){
		return traits.contains(Trait.male)||traits.contains(Trait.herm);
	}
	public boolean hasBalls(){
		return traits.contains(Trait.male)||traits.contains(Trait.herm);
	}
	public boolean hasPussy(){
		return traits.contains(Trait.female)||traits.contains(Trait.herm);
	}
	public boolean hasBreasts(){
		return traits.contains(Trait.female)||traits.contains(Trait.herm);
	}
	public int countFeats(){
		int count = 0;
		for(Trait t: traits){
			if(t.isFeat()){
				count++;
			}
		}
		return count;
	}
	public void regen(boolean combat){
		int regen = Math.max(1, getStamina().max()/50);
		for(Status s: status){
			regen+=s.regen();
		}
		if(has(Trait.BoundlessEnergy)){
			regen+=Math.max(1, getStamina().max()/20);;
		}
		if(has(Trait.streaker)&&nude()){
			regen+=Math.max(1, getStamina().max()/50);;
		}
		if(has(Trait.healing)){
			regen+=Math.max(1, getStamina().max()/10);;
		}
		heal(regen);
		int calming = 0;
		if(has(Trait.confidentdom)&&(getMood()==Emotion.dominant || getMood()==Emotion.confident)){
			calming += getArousal().max()/10;
		}
		if(has(Trait.tantra)){
			calming += getArousal().max()/20;
		}
		if(has(Trait.legend)){
			calming += getArousal().max()/10;
		}
		if(combat){
			if(has(Trait.exhibitionist)&&nude()){
				buildMojo(5);
			}
			if(has(Trait.streaker)&&nude()){
				buildMojo(10);
			}
			if(has(Trait.stylish)){
				buildMojo(3);
			}
			if(has(Trait.SexualGroove)){
				buildMojo(2);
			}
			if(has(Trait.lame)){
				buildMojo(-3);
			}
			if(has(Trait.spirited)){
				buildMojo(50);
			}
		}
	}
	public void add(Status status){
		boolean cynical = false;
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(this.status);
		for(Status s:copy){
			if(s.flags().contains(Stsflag.cynical)){
				cynical = true;
			}
			if(s.toString().equals(status.toString())){
				if(s.stacking()){
					s.stack(status);
				}
				return;
			}
		}
		if(cynical && status.mindgames()){
			return;
		}
		if(has(Trait.shameless)&&status.flags().contains(Stsflag.shamed)){
			return;
		}
		else{
			this.status.add(status);
		}		
	}
	public void add(Status status,Combat c){
		boolean cynical = false;
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(this.status);
		for(Status s:copy){
			if(s.flags().contains(Stsflag.cynical)){
				cynical = true;
			}
			if(s.toString().equals(status.toString())){
				if(s.stacking()){
					s.stack(status);
				}
				return;
			}
		}
		if(cynical && status.mindgames()){
			return;
		}
		if(has(Trait.shameless)&&status.flags().contains(Stsflag.shamed)){
			return;
		}
		else{
			c.reportStatus(this, status);
			this.status.add(status);
		}		
	}
    public void removeStatus(Status sts){
	    this.status.remove(sts);
    }
    public void removeStatus(Status sts, Combat c){
        removeStatus(sts);
        c.reportStatusLoss(this,sts);
    }
    public void removeStatus(Stsflag flag){
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(status);
        for(Status s: copy){
            if(s.flags().contains(flag)){
                status.remove(s);
            }
        }
    }
	public void removeStatus(Stsflag flag,Combat c){
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(status);
        for(Status s: copy){
            if(s.flags().contains(flag)){
                status.remove(s);
                c.reportStatusLoss(this,s);
            }
        }

    }
/*	public void dropStatus(){
		status.removeAll(removelist);
		for(Status s: addlist){
			add(s);
		}
		removelist.clear();
		addlist.clear();
	}*/
	public boolean is(Stsflag sts){
		for(Status s: status){
			if(s.flags().contains(sts)){
				return true;
			}
		}
		return false;
	}
	public boolean stunned(){
		for(Status s: status){
			if(s.flags().contains(Stsflag.stunned)){
				return true;
			}
		}
		return false;
	}
	public boolean distracted(){
		for(Status s: status){
			if(s.flags().contains(Stsflag.distracted)||s.flags().contains(Stsflag.charmed)){
				return true;
			}
		}
		return false;
	}
	public boolean bound(){
		for(Status s: status){
			if(s.flags().contains(Stsflag.bound)){
				return true;
			}
		}
		return false;
	}
	public void free(){
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(status);
		removeStatus(Stsflag.bound);
	}
	public int escape(){
		int total=0;
		for(Status s: status){
			total+=s.escape();
		}
		if(has(Trait.houdini)){
			total+=get(Attribute.Cunning)/5;
		}
		if(has(Trait.freeSpirit)){
			total+=2;
		}
		return total;
	}
	public boolean canAct(){
		return !(stunned()||distracted()||bound()||is(Stsflag.enthralled));
	}
	public boolean canActNormally(Combat c){
		return canAct() && c.stance.mobile(this) && !c.stance.prone(this) && !c.stance.sub(this);
	}
	public boolean genitalsAvailable(Combat c){
		boolean available = pantsless()||(c.getOther(this).has(Trait.dexterous)&&bottom.size()<=1);
		if(!c.stance.reachBottom(c.getOther(this))){
			return false;
		}
		if(c.stance.penetration(this) && !(hasDick()&&hasPussy())){
			return false;
		}
		return available;
	}
	public abstract void detect();
	public abstract void faceOff(Character opponent,Encounter enc);
	public abstract void spy(Character opponent,Encounter enc);
	public abstract String describe(int per);
	public abstract void victory(Combat c, Result flag);
	public abstract void defeat(Combat c, Result flag);
	public abstract void intervene3p(Combat c,Character target, Character assist);
	public abstract void victory3p(Combat c,Character target, Character assist);
	public abstract void watcher(Combat c, Character victory, Character defeated);
	public abstract String watched(Combat c, Character voyeur, Character defeated);
	public abstract void act(Combat c);
	public abstract void move(Match match);
	public abstract void draw(Combat c, Result flag);
	public abstract boolean human();
	public abstract String challenge(Character opponent);
	public abstract String bbLiner();
	public abstract String nakedLiner();
	public abstract String stunLiner();
	public abstract String winningLiner();
	public abstract String taunt();
	public abstract void intervene(Encounter enc, Character p1,Character p2);
	public abstract boolean resist3p(Combat combat, Character intruder, Character assist);
	public abstract void showerScene(Character target, Encounter encounter);
	public abstract void load(Scanner loader);
	public abstract void afterParty();
	public abstract void eot(Combat c, Character opponent, Skill last);
	public abstract Emotion moodSwing();
	public abstract void resetOutfit();
	public JsonObject save() {
		JsonObject saver = new JsonObject();
		saver.addProperty("Name", name);
		saver.addProperty("Level",level);
		saver.addProperty("Rank",getRank());
		saver.addProperty("XP", xp);
		saver.addProperty("Money", money);
		saver.addProperty( "Stamina", stamina.max());
		saver.addProperty("Arousal",arousal.max());
		saver.addProperty("Mojo",mojo.max());
		saver.addProperty("Score",Scheduler.getScore(id()));

		JsonObject attributes = new JsonObject();
		for(Attribute a: att.keySet()){
			attributes.addProperty(a.toString(),att.get(a));
		}
		saver.add("Attributes",attributes);

		JsonArray traits = new JsonArray();
		for(Trait t: this.traits){
			traits.add(t.name());
		}
		saver.add("Traits",traits);

		JsonArray upper = new JsonArray();
		for(Clothing c: outfit[Character.OUTFITTOP]){
			upper.add(c.name());
		}
		saver.add("Upper Clothing",upper);

		JsonArray lower = new JsonArray();
		for(Clothing c: outfit[Character.OUTFITBOTTOM]){
			lower.add(c.name());
		}
		saver.add("Lower Clothing",lower);

		JsonArray allclothes = new JsonArray();
		for(Clothing c: closet){
			allclothes.add(c.name());
		}
		saver.add("Owned Clothes",allclothes);

		JsonArray items = new JsonArray();
		for(Item i: inventory){
			items.add(i.toString());
		}
		saver.add("Inventory",items);
;
		return saver;
	}
	public void load(JsonObject loader){
		this.name = loader.get("Name").getAsString();
		this.level = loader.get("Level").getAsInt();
		this.rank = loader.get("Rank").getAsInt();
		this.xp = loader.get("XP").getAsInt();
		this.money = loader.get("Money").getAsInt();
		this.stamina.setMax(loader.get("Stamina").getAsInt());
		this.arousal.setMax(loader.get("Arousal").getAsInt());
		this.mojo.setMax(loader.get("Mojo").getAsInt());

		JsonObject attributes = loader.getAsJsonObject("Attributes");
		for(Attribute a: Attribute.values()){
			if(attributes.has(a.name())){
				att.put(a,attributes.get(a.name()).getAsInt());
			}
		}
		traits.clear();
		for(JsonElement t:loader.getAsJsonArray("Traits")){
			traits.add(Trait.valueOf(t.getAsString()));
		}
		if(human()) {
            outfit[Character.OUTFITTOP].clear();
            for (JsonElement t : loader.getAsJsonArray("Upper Clothing")) {
                outfit[Character.OUTFITTOP].push(Clothing.valueOf(t.getAsString()));
            }

            outfit[Character.OUTFITBOTTOM].clear();
            for (JsonElement t : loader.getAsJsonArray("Lower Clothing")) {
                outfit[Character.OUTFITBOTTOM].push(Clothing.valueOf(t.getAsString()));
            }
        }else{
            resetOutfit();
        }


		closet.clear();
		for(JsonElement t:loader.getAsJsonArray("Owned Clothes")){
			closet.add(Clothing.valueOf(t.getAsString()));
		}
		inventory.clear();
		for(JsonElement t:loader.getAsJsonArray("Inventory")){
			inventory.add(Global.getItem(t.getAsString()));
		}
		Scheduler.setScore(id(),loader.get("Score").getAsInt());
		resetSkills();
        Global.gainSkills(this,false);
        stamina.fill();
        arousal.empty();
        mojo.empty();

	}

	public void resetSkills(){
	    skills.clear();
        skills.add(new Struggle(this));
        skills.add(new Nothing(this));
        skills.add(new Recover(this));
        skills.add(new Straddle(this));
        skills.add(new ReverseStraddle(this));
        skills.add(new Stunned(this));
        skills.add(new Distracted(this));
        skills.add(new PullOut(this));
        flaskskills.clear();
        for(Flask f: Flask.values()){
            flaskskills.add(new UseFlask(this,f));
        }
        potionskills.clear();
        for(Potion p: Potion.values()){
            potionskills.add(new UsePotion(this,p));
        }
    }

	public void learn(Skill copy) {
		skills.add(copy);
	}
	public boolean stealthCheck(int perception) {
		return check(Attribute.Cunning,Global.random(20)+perception+(level/2))||(state==State.hidden);
	}
	public boolean spotCheck(int perception){
		if(has(Trait.assassin) && Global.random(2)==0){
			return false;
		}
		if(state==State.hidden){
			int dc = perception+10;
			if(has(Trait.Sneaky)){
				dc-=10;
			}
			return check(Attribute.Cunning,dc);
		}		
		else{
			return true;
		}
	}
	public int bonusDisarm(){
		if(has(Trait.cautious)){
			return 5;
		}
		return 0;
	}
	public int getSexPleasure(int pace, Attribute used){
		int m = Math.round((3*pace+get(used)/2)*(.8f+(.1f*Global.random(4))));
		if(has(Trait.strapped)){
			m += (get(Attribute.Science)/2);
			m = bonusProficiency(Anatomy.toy, m);
		}else{
			m = bonusProficiency(Anatomy.genitals, m);
		}
		return m;
	}
	public int bonusRecoilPleasure(int mag){
		float modifier = 0f;
		float cap = .1f;
		if(is(Stsflag.shamed)){
			cap += getStatusMagnitude("Shamed")*.05f;
		}
		modifier = Math.min(get(Attribute.Seduction)*.01f,cap);
		if(has(Trait.responsive)){
			modifier += .2f;
		}
		if(has(Trait.ishida3rdart)){
			modifier += get(Attribute.Ninjutsu)/20;
		}
		return Math.round(mag * modifier);
	}
	public int bonusPetPower(){
		int total = 0;
		if(has(Trait.leadership)){
			total+=get(Attribute.Perception)/2;
		}
		if(has(Trait.coordinatedStrikes)){
			total+=get(Attribute.Cunning)/4;
		}
		return total;
	}
	public int bonusPetEvasion(){
		int total = 0;
		if(has(Trait.tactician)){
			total+=get(Attribute.Perception)/2;
		}
		if(has(Trait.evasiveManuevers)){
			total+=get(Attribute.Cunning)/4;
		}
		return total;
	}
	public int bonusProficiency(Anatomy using, int baseDamage){
		float prof = 1.0f;
		for(Status s: status){
			prof *= s.proficiency(using);
		}
		if(has(Trait.legend)){
			prof += 1f;
		}
		switch(using){
			case fingers:
				if(has(Trait.handProficiency)){
					prof += .1f;
				}
				if(has(Trait.handExpertise)){
					prof += .1f;
				}
				if(has(Trait.handMastery)){
					prof += .1f;
				}
				if(has(Trait.mittens)){
					prof -= .75f;
				}
				if(has(Trait.smallhands)){
					prof += .3f;
				}
				break;
			case mouth:
				if(has(Trait.oralProficiency)){
					prof += .1f;
				}
				if(has(Trait.oralExpertise)){
					prof += .1f;
				}
				if(has(Trait.oralMastery)){
					prof += .1f;
				}
				break;
			case genitals:
				if(has(Trait.intercourseProficiency)){
					prof += .1f;
				}
				if(has(Trait.intercourseExpertise)){
					prof += .1f;
				}
				if(has(Trait.intercourseMastery)){
					prof += .1f;
				}
				if(has(Trait.succubusvagina)){
					prof += 1.5f;
				}
				if(has(Trait.hardon)){
					prof += 1f;
				}
				break;
			case feet:
				if(has(Trait.footloose)){
					prof += .5f;
				}
				break;
			case toy:
				if(has(Trait.toymaster)){
					prof += .2f;
				}
				if(has(Trait.experimentalweaponry)){
					prof += 1f;
				}
				break;
		}
		return Math.round(prof*baseDamage);
	}
	public int bonusPin(int baseDC){
		float scale = 1f;
		if(has(Trait.Clingy)){
			scale += .1f;
		}
		if(has(Trait.predatorinstincts)){
			scale += .5f;
		}
		return Math.round(baseDC*scale);
	}
	public void travel(Area dest){
		state=State.ready;
		location.exit(this);
		location=dest;
		dest.enter(this);
	}
	public void flee(Area location2) {
		Area[] adjacent = location2.adjacent.toArray(new Area[location2.adjacent.size()]);
		travel(adjacent[Global.random(adjacent.length)]);
		location2.endEncounter();
	}
	public void upkeep() {
		regen(false);
		if(has(Trait.Confident)){
			mojo.reduce(5);
		}else{
			mojo.reduce(10);
		}
		if(bound()){
			free();
		}
		//dropStatus();
		if(has(Trait.QuickRecovery)){
			heal(4);
		}
		if(has(Trait.exhibitionist)&&nude()){
			buildMojo(3);
		}
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(status);
        for(Status s: copy){
            s.decay();
        }
		decayMood();
		moodSwing();
		setChanged();
		notifyObservers();
	}
	public void gain(Item item) {
		inventory.add(item);
	}
	public void gain(Clothing item) {
		closet.add(item);
	}
	public void gain(Item item, int q){
		for(int i=0;i<q;i++){
			gain(item);
		}
	}
	public boolean has(Item item){
		return inventory.contains(item);
	}
	public boolean hasAll(ArrayList<Item> recipe){
		HashMap<Item,Integer> counts = new HashMap<Item,Integer>();
		for(Item i: recipe){
			if(counts.containsKey(i)){
				counts.put(i, counts.get(i)+1);
			}else{
				counts.put(i, 1);
			}
		}
		for(Item i: counts.keySet()){
			if(!has(i,counts.get(i))){
				return false;
			}
		}
		return true;
	}
	public boolean has(Item battery, int quantity){
		for(Item i:inventory){
			if(i==battery){
				quantity--;
			}
			if(quantity<=0){
				return true;
			}
		}
		return false;
	}
	public boolean has(Clothing item){
		return closet.contains(item);
	}
	public void consume(Item item, int quantity){
		if(has(Trait.resourceful)&&Global.random(5)==0){
			quantity--;
		}
		for(int i=0;i<quantity;i++){
			inventory.remove(item);
		}
	}
	public void consumeAll(ArrayList<Item> items){
		for(Item i: items){
			consume(i,1);
		}
	}
	public void remove(Item item, int quantity){
		for(int i=0;i<quantity;i++){
			inventory.remove(item);
		}
	}
	public int count(Item item){
		int total=0;
		for(Item i:inventory){
			if(i==item){
				total++;
			}
		}
		return total;
	}
	public void chargeBattery(){
		int power = count(Component.Battery);
		if(power<20){
			gain(Component.Battery,20-power);
		}
	}
	public Trait getGrudge(){
		return activeGrudge;
	}
	public void addGrudge(Character target, Trait grudge){
		grudges.put(target, grudge);
	}
	public void clearGrudge(Character target){
		grudges.remove(target);
	}
	public void defeated(Character victor){
		mercy.add(victor);
	}
	public void resupply(){
		for(Character victor: mercy){
			if(has(Trait.event)){
				victor.bounty(5);
			}else{
				victor.bounty(1);
			}
		}
		mercy.clear();
		change(Scheduler.getMatch().condition);
		state=State.ready;
		if(location().present.size()>1){
			if(location().id()==Movement.dorm){
				if(Scheduler.getMatch().gps(Movement.quad).present.isEmpty()){
					if(human()){
						Global.gui().message("You hear your opponents searching around the dorm, so once you finish changing, you hop out the window and head to the quad.");
					}
					travel(Scheduler.getMatch().gps(Movement.quad));
				}
				else{
					if(human()){
						Global.gui().message("You hear your opponents searching around the dorm, so once you finish changing, you quietly move downstairs to the laundry room.");
					}
					travel(Scheduler.getMatch().gps(Movement.laundry));
				}
			}
			if(location().id()==Movement.union){
				if(Scheduler.getMatch().gps(Movement.union).present.isEmpty()){
					if(human()){
						Global.gui().message("You don't want to be ambushed leaving the student union, so once you finish changing, you hop out the window and head to the quad.");
					}
					travel(Scheduler.getMatch().gps(Movement.quad));
				}
				else{
					if(human()){
						Global.gui().message("You don't want to be ambushed leaving the student union, so once you finish changing, you sneak out the back door and head to the pool.");
					}
					travel(Scheduler.getMatch().gps(Movement.pool));
				}
			}
		}
	}
	public void finishMatch(){
		for(Character victor: mercy){
			if(has(Trait.event)){
				victor.bounty(5);
			}else{
				victor.bounty(1);
			}
		}
		ArrayList<Item> copy = new ArrayList<Item>();
		copy.addAll(inventory);
		for(Item i: copy){
			if(i.getClass()==Envelope.class){
				inventory.remove(i);
			}
		}
		grudges.clear();
		mercy.clear();
		matchmod = null;
		rest();
	}
	public void place(Area loc){
		location=loc;
		loc.present.add(this);
	}
	public void rest(){
		change(Modifier.normal);
		clearStatus();
		getStamina().fill();
		getArousal().empty();
		getMojo().empty();
	}
	public void bounty(int points){
		Scheduler.getMatch().score(this,points);
	}
	public boolean eligible(Character p2) {
		return (!mercy.contains(p2))&&state!=State.resupplying;
	}
	public void setUnderwear(Item panties){
		underwear = panties;
	}
	public Item getUnderwear(){
		return underwear;
	}
	public void bathe(){
		status.clear();
		stamina.fill();
		state=State.ready;
	}
	public void masturbate(){
		arousal.empty();
		state=State.ready;
	}
	public void craft(){
		int roll = Global.random(10);
		if(check(Attribute.Cunning,40)){
			if(roll>=7 && Global.checkFlag(Flag.Kat)){
				gain(Potion.Furlixir);
			}else{
				gain(Flask.PAphrodisiac);
			}
		}
		if(check(Attribute.Ninjutsu,5)){
			if(roll==9){
				gain(Consumable.needle);
				gain(Consumable.smoke);
				gain(Flask.Sedative);
			}
			else if(roll>=7){
				gain(Flask.Sedative);
			}
			else if(roll>=5){
				gain(Consumable.smoke);
			}
			else if(roll>=3){
				gain(Consumable.needle);
			}
		}
		if(check(Attribute.Science,20)){
			if(roll==9){
				gain(Potion.Fox);
				gain(Potion.Nymph);
				gain(Potion.Bull);
			}else if(roll>=7){
				gain(Potion.Cat);
			}else if(roll>=5){
				gain(Potion.Fox);
			}else if(roll>=3){
				gain(Potion.Nymph);	
			}else if(roll>=1){
				gain(Potion.Bull);
			}else{
				gain(Potion.SuperEnergyDrink);
			}
		}
		else if(check(Attribute.Science,5)){
			if(roll==9){
				gain(Flask.Aphrodisiac,2);
				gain(Flask.DisSol);
				gain(Potion.SuperEnergyDrink);
			}
			else if(roll>=7){
				gain(Flask.Aphrodisiac);
				gain(Flask.DisSol);
				gain(Potion.SuperEnergyDrink);
			}
			else if(roll>=5){
				gain(Flask.Lubricant,3);
				gain(Potion.SuperEnergyDrink);
			}
			else if(roll>=3){
				gain(Flask.Lubricant);
				gain(Flask.Sedative);
				gain(Potion.SuperEnergyDrink);
			}
			else{
				gain(Potion.SuperEnergyDrink);
			}
		}
		else if(check(Attribute.Cunning, 25)){
			if(roll==9){
				gain(Flask.Aphrodisiac);
				gain(Flask.DisSol);
			}
			else if(roll>=5){
				gain(Flask.Aphrodisiac);
			}
			else{
				gain(Flask.Lubricant);
				gain(Flask.Sedative);
			}
		}
		else if(check(Attribute.Cunning, 20)){
			if(roll==9){
				gain(Flask.Aphrodisiac);
			}
			else if(roll>=7){
				gain(Flask.DisSol);
			}
			else if(roll>=5){
				gain(Flask.Lubricant);
			}
			else if(roll>=3){
				gain(Flask.Sedative);
			}
			else{
				gain(Potion.EnergyDrink);
			}
		}
		else if(check(Attribute.Cunning, 15)){
			if(roll==9){
				gain(Flask.Aphrodisiac);
			}
			else if(roll>=8){
				gain(Flask.DisSol);
			}
			else if(roll>=7){
				gain(Flask.Lubricant);
			}
			else if(roll>=6){
				gain(Potion.EnergyDrink);
			}else if(human()){
				Global.gui().message("Your concoction turns a sickly color and releases a foul smelling smoke. You trash it before you do any more damage.");
			}
		}
		else{
			if(roll>=7){
				gain(Flask.Lubricant);
			}
			else if(roll>=5){
				gain(Flask.Sedative);
			}else if(human()){
				Global.gui().message("Your concoction turns a sickly color and releases a foul smelling smoke. You trash it before you do any more damage.");
			}
		}
		state=State.ready;
	}
	public void search(){
		int roll = Global.random(10);
		switch(roll){
		case 9:
			gain(Component.Tripwire);
			gain(Component.Tripwire);
			break;
		case 8:
			gain(Consumable.ZipTie);
			gain(Consumable.ZipTie);
			gain(Consumable.ZipTie);
			break;
		case 7:
			gain(Component.Phone);
			break;
		case 6:
			gain(Component.Rope);
			break;
		case 5:
			gain(Component.Spring);
			break;
		default:
			if(human()){
				Global.gui().message("You don't find anything useful");
			}
		}
		state=State.ready;
	}
	public void delay(int i){
		busy+=i;
	}
	public abstract void promptTrap(Encounter enc,Character target,Trap trap);
	public int lvlBonus(Character opponent){
		if(opponent.getLevel()>this.getLevel()){
			if(opponent.has(Trait.event)){
				return 50;
			}
			return 5*(opponent.getLevel()-this.getLevel());
		}
		else{
			return 0;
		}
	}
	public int getAttraction(Character other){
		return Roster.getAttraction(id(),other.id());
	}
	public void gainAttraction(Character other, int x){
		Roster.gainAttraction(id(),other.id(),x);
	}
	public int getAffection(Character other){
		return Roster.getAffection(id(),other.id());
	}
	public void gainAffection(Character other, int x){
		Roster.gainAffection(id(),other.id(),x);
	}
	public Character getRandomFriend(){
		ArrayList<Character> known = new ArrayList<Character>();
		for( Character person : Roster.getExisting()){
			if(Roster.getAttraction(person.id(), id()) > 0){
				known.add(person);
			}
		}
		if(known.isEmpty()){
			return null;
		}else{
			return known.get(Global.random(known.size()));
		}

	}
	public int ac(){
		int ac = (get(Attribute.Cunning)/3)+get(Attribute.Perception)+get(Attribute.Speed);
		for(Status s: status){
			ac += s.evade();
		}
		if(has(Trait.clairvoyance)){
			ac += 5;
		}
		if(has(Trait.untouchable)){
			ac += 25;
		}
		if(!canAct()){
			ac -= 999;
		}
		return ac;
	}
	public int bonusCounter(){
		int counter = 0;
		for(Status s: status){
			counter += s.counter();
		}
		if(has(Trait.clairvoyance)){
			counter += 3;
		}
		if(has(Trait.aikidoNovice)){
			counter +=3;
		}
		return counter;
	}
	public int tohit(){
		int hit = get(Attribute.Speed)+getLevel();
		return hit;
	}
	public boolean roll(Skill attack, Combat c, int accuracy){
		int attackroll = accuracy + 2+Global.random(8)+Global.random(8);
		if(attackroll+10<ac()+bonusCounter()){
			counterattack(attack.user(),attack.type(), c);
		}
		return attackroll > ac();
	}
	public int knockdownDC(){
		int dc = getStamina().get()/4;
		if(is(Stsflag.braced)){
			dc+=getStatus(Stsflag.braced).value();
		}
		if(getPure(Attribute.Discipline)>=9 && is(Stsflag.composed)){
		    dc+=getStatus(Stsflag.composed).mag();
        }
		if(has(Trait.landsonfeet)){
			dc+=50;
		}
		return dc;
	}
	public abstract void counterattack(Character target, Tactics type, Combat c);
		
	public void clearStatus(){
		status.clear();
	}
	public Status getStatus(Stsflag flag){
		for(Status s: status){
			if(s.flags().contains(flag)){
				return s;
			}
		}
		return null;
	}
	
	public int getStatusMagnitude(String name){
		for(Status s: status){
			if(s.toString()==name){
				return s.mag();
			}
		}
		return 0;
	}
	public boolean canReadBasic(Character target){
		if(get(Attribute.Perception)>3){
			if(target==this || !target.is(Stsflag.unreadable) || get(Attribute.Perception)>6){
				return true;
			}
		}
		return false;
	}
	public String getBasicArousalDamage(String type, int magnitude){
		double p = magnitude*1.0/getArousal().max();
		if(p>.25){
			return "extreme";
		}else if(p>.15){
			return "intense";
		}else if(p>.1){
			return "strong";
		}else if(p>.05){
			return "moderate";
		}else if(p>.02){
			return "a little";
		}else{
			return "minimal";
		}
	}
	public String getBasicPain(int magnitude){
		double p = magnitude*1.0/getStamina().max();
		if(p>.25){
			return "severe";
		}else if(p>.15){
			return "intense";
		}else if(p>.1){
			return "major";
		}else if(p>.05){
			return "manageable";
		}else if(p>.02){
			return "small";
		}else{
			return "negligible";
		}
	}
	public String getBasicWeaken(int magnitude){
		double p = magnitude*1.0/getStamina().max();
		if(p>.25){
			return "severely";
		}else if(p>.15){
			return "Heavily";
		}else if(p>.1){
			return "significantly";
		}else if(p>.05){
			return "moderately";
		}else if(p>.02){
			return "slightly";
		}else{
			return "minimally";
		}
	}
	public String getBasicCalm(int magnitude){
		double p = magnitude*1.0/getArousal().max();
		if(p>.7){
			return "completely";
		}
		else if(p>.4){
			return "a lot";
		}else if(p>.2){
			return "significantly";
		}else if(p>.1){
			return "moderately";
		}else if(p>.03){
			return "slightly";
		}else{
			return "minimally";
		}
	}
	public String getBasicHeal(int magnitude){
		double p = magnitude*1.0/getStamina().max();
		if(p>.7){
			return "completely";
		}
		else if(p>.4){
			return "a lot";
		}else if(p>.2){
			return "significantly";
		}else if(p>.1){
			return "moderately";
		}else if(p>.03){
			return "slightly";
		}else{
			return "minimally";
		}
	}
	public boolean canReadAdvanced(Character target){
		if((target==this && get(Attribute.Perception)>3)|| (!target.is(Stsflag.unreadable) && get(Attribute.Perception)>6)){
			return true;
		}
		return false;
	}
	public Integer prize() {
		return 50+(100*((int)Math.pow(getRank(),2)));
	}
	public void emote(Emotion emo,int amt){
		emotes.put(emo, Math.min(emotes.get(emo)+amt,100));
		Emotion inverse = emo.inverse();		
		emotes.put(inverse, Math.max(emotes.get(inverse)-amt/2,0));
		if(emotes.get(emo)<0){
			emotes.put(emo, 0);
		}
		if(emotes.get(inverse)<0){
			emotes.put(inverse, 0);
		}
	}
	
	public Emotion getMood(){
		return mood;
	}
	public void decayMood(){
		for(Emotion e: emotes.keySet()){
			if(nude()&&!has(Trait.exhibitionist)&&!has(Trait.shameless)&& e==Emotion.nervous){
				emotes.put(e, emotes.get(e)-((emotes.get(e)-50)/2));
			}else if(nude()&&has(Trait.exhibitionist)&& e==Emotion.horny){
				emotes.put(e, emotes.get(e)-((emotes.get(e)-50)/2));
			}
			else if(!nude()&&e==Emotion.confident){
				emotes.put(e, emotes.get(e)-((emotes.get(e)-50)/2));
			}
			else{
				if(emotes.get(e)>=5){
					emotes.put(e, emotes.get(e)-(emotes.get(e)/2));
				}
			}
		}
	}
	public Move findPath(Area target) {
		if (this.location.name.equals(target.name))
			return null;
		ArrayDeque<Area> queue = new ArrayDeque<Area>();
		Vector<Area> vector = new Vector<Area>();
		HashMap<Area, Area> parents = new HashMap<Area,Area>();
		queue.push(this.location);
		vector.add(this.location);
		Area last = null;
		while (!queue.isEmpty()) {
			Area t = queue.pop();
			parents.put(t, last);
			if (t.name.equals(target.name)) {
				while (!this.location.adjacent.contains(t)) {
					t = parents.get(t);
				}
				return new Move(t);
			}
			for (Area area : t.adjacent) {
				if (!vector.contains(area)) {
					vector.add(area);
					queue.push(area);
				}
			}
			last = t;
		}
		return null;
	}
	public boolean knows(Skill skill){
		for(Skill s:skills){
			if(s.equals(skill)){
				return true;
			}
		}
		return false;
	}
	public void preCombat(Character opponent, Combat c){
		if(has(Trait.perfectplan)){
			opponent.nudify();
		}
		if(has(Trait.ninjapreparation)){
			opponent.add(new Bound(opponent,30,"net"));
			opponent.add(new Horny(opponent,3,4),c);
			opponent.add(new Drowsy(opponent,4),c);
		}
		if(has(Trait.sadisticmood)){
			opponent.add(new Masochistic(opponent));
		}
		if(has(Trait.enthralling)){
			opponent.add(new Enthralled(opponent,this));
		}
		if(has(Trait.sparehandcuffs)){
			gain(Consumable.Handcuffs);
			gain(Consumable.Handcuffs);
		}
		if(has(Trait.defensivemeasures)){	
			replaceArticle(Clothing.cup);
		}
		if(has(Trait.confidentdom)){
			emote(Emotion.dominant,50);
		}
		if(getPure(Attribute.Discipline)>=1){
		    add(new Composed(this,5+(get(Attribute.Discipline)/2),get(Attribute.Discipline)));
        }
	}
	public void endofbattle(Character Opponent, Combat c){
        ArrayList<Status> copy = new ArrayList<Status>();
        copy.addAll(status);
		for(Status s: copy){
			if(!s.lingering())
			  removeStatus(s);
		}
		if(pet!=null){
			pet.remove();
		}
		activeGrudge = null;
		//dropStatus();
	}
	public boolean canSpend(int mojo){
		int cost=mojo;
		for(Status s:status){
			cost+=s.spendmojo(mojo);
		}
		if(has(Trait.overflowingmana)){
			cost = cost/20;
		}
		return getMojo().get()>=cost;
	}
	public ArrayList<Item> getInventory() {
		return inventory;
	}
	public ArrayList<Status> getStatus() {
		ArrayList<Status> result = new ArrayList<Status>();
		result.addAll(status);
		return result;
	}
	public ArrayList<String> listStatus() {
		ArrayList<String> result = new ArrayList<String>();
		for(Status s: status){
			result.add(s.toString());
		}
		return result;
	}
	public String dumpstats() {
		String s = name()+": Level "+getLevel()+"; ";
		for(Attribute a: att.keySet()){
			s = s+a.name()+" "+att.get(a)+", ";
		}
		s = s+"<br>Max Stamina "+stamina.max()+", Max Arousal "+arousal.max()+", Max Mojo "+mojo.max()+", Affection "+getAffection(Global.getPlayer())+".<p>";
		return s;
	}
	public void accept(Challenge c){
		challenges.add(c);
	}
	public void evalChallenges(Combat c, Character victor){
		for(Challenge chal: challenges){
			chal.check(c, victor);
		}
	}
	public float getUrgency() {

         // How urgent do we want the fight to end? We are
         // essentially trying to approximate how many
         // moves we will still have until the end of the
         // fight.

        // Once arousal is too high, the fight ends.
         float remArousal = getArousal().max() - getArousal().get();

         // Arousal is worse the more clothes we lose, because
         // we will probably lose it quicker.
         float arousalPerRound = 5;
         for(int i = 0; i < top.size(); i++) arousalPerRound /= 1.2;
         for(int i = 0; i < bottom.size(); i++) arousalPerRound /= 1.2;

         // Depending on our stamina, we should count on
         // spending a few of these rounds knocked down,
         // so effectively we have even less rounds to
         // work with - let's make them count!
         float fitness = Math.max(0.5f, Math.min(1.0f,
                         (float) getStamina().get() / 40));

         return remArousal / arousalPerRound * fitness;
	}
	public float rateUrgency(float urgency, float urgency2) {
		// How important is it to reach a goal with given
		// urgency, given that we are gunning roughly for
		// the second urgency? We use a normal distribution
		// (hah, fancy!) that gives roughly 50% weight at
		// a difference of 15.
		float x = (urgency - urgency2) / 15;
		return (float) Math.exp(-x*x);
	}

	public float getFitness(float urgency, Position p) {
		float fit = 0;
        // Urgency marks
		float ushort = rateUrgency(urgency, 5);
		float umid = rateUrgency(urgency, 15);
        float ulong = rateUrgency(urgency, 25);
        float usum = ushort + umid + ulong;
		// Always important: Position
		if (p.dom(this)) { fit += 2; }
		if (p.sub(this)) { fit -= 2; }
		if (p.reachTop(this)) { fit += 1; }
		if (p.reachBottom(this)) { fit += 1; }
		if (p.mobile(this)) { fit += 2; }
		if (p.penetration(this) && p.dom(this)) { fit += 5; }
		if (p.behind(this)) { fit += 1; }
		// Also important: Clothing.
		// Especially when aroused (protects vs KO)
		fit += ushort * 3 * (top.size() + bottom.size());
		// Also somewhat of a factor: Inventory (so we don't
		// just use it without thinking)
		for(int i = 0; i < inventory.size(); i++)
		        fit += (float) inventory.get(i).getPrice() / 10;
		// Extreme situations
		if (arousal.isFull()) fit -= 99;
		if (stamina.isEmpty()) fit -= umid * 3;
		// Short-term: Arousal
		fit += ushort / usum * (getArousal().max() - getArousal().get());
		// Mid-term: Stamina
		fit += umid / usum * getStamina().get();
		// Long term: Mojo
		fit += ulong / usum * getMojo().get();
		// TODO: Pets, status effects...
		return fit;
	}
	public abstract String getPortrait();
	public String pronounSubject(boolean capital) {
		if(capital){
			if(has(Trait.male)){
				return "He";
			}
			return "She";
		}
		if(has(Trait.male)){
			return "he";
		}
		return "she";
	}
	public String pronounTarget(boolean capital) {
		if(capital){
			if(has(Trait.male)){
				return "Him";
			}
			return "Her";
		}
		if(has(Trait.male)){
			return "him";
		}
		return "her";
	}	public String possessive(boolean capital) {
		if(capital){
			if(has(Trait.male)){
				return "His";
			}
			return "Her";
		}
		if(has(Trait.male)){
			return "his";
		}
		return "her";
	}
	public void matchPrep(Match m) {
		change(m.condition);
		state=State.ready;
		Global.gainSkills(this);
		if(getPure(Attribute.Ninjutsu)>=9){			
			placeStash(m);
		}
		if(has(Trait.challengeSeeker)){
			new Challenge(m).resolve(this);
		}
		if(getPure(Attribute.Science)>=1){
			while(!has(Component.Battery,20)){
				inventory.add(Component.Battery);
			}
		}
	}
	private void placeStash(Match m) {
		Movement location;
		switch(Global.random(6)){
		case 0:
			location = Movement.library;
			break;
		case 1:
			location = Movement.dining;
			break;
		case 2:
			location = Movement.lab;
			break;
		case 3:
			location = Movement.workshop;
			break;
		case 4:
			location = Movement.storage;
			break;
		default:
			location = Movement.la;
			break;
		}
		Area loc = m.gps(location);
		loc.place(new NinjaStash(this));
		if(human()){
			Global.gui().message("<b>You've arranged for a hidden stash to be placed in the "+loc.toString()+".</b>");
		}
		
	}
	protected int getCostumeSet(){
		return 1;
	}
	public BufferedImage getSpriteImage(){
		int costume = getCostumeSet();
		String res = "";
		if(Global.gui().lowRes()){
			res = "_low";
		}
		if(spriteBody == null){
			try {
                URL path = Global.gui().getClass().getResource(
                        "assets/" + name + getCostumeSet() + "_Sprite" + res + ".png");
                if(path == null){
                    path = Global.gui().getClass().getResource(
                            "assets/"+name+"_Sprite"+res+".png");
                }
				spriteBody = ImageIO.read(path);


			} catch (IOException e) {

			} catch (IllegalArgumentException e){
				
			}
		}
		if(spriteBody == null){
			return null;
		}
		BufferedImage sprite = new BufferedImage(spriteBody.getWidth(), spriteBody.getHeight(), spriteBody.getType());
		Graphics2D g = sprite.createGraphics();
		if(isWearing(ClothingType.TOPOUTER)){
			if(spriteCoattail == null){
				try {
					spriteCoattail = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_outerback"+res+".png"));
					g.drawImage(spriteCoattail, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteCoattail, 0, 0, null);
			}
		}
		g.drawImage(spriteBody,0,0,null);			
		try {
			BufferedImage face = ImageIO.read(Global.gui().getClass().getResource(
					"assets/"+name+"_"+mood.name()+res+".png"));
			g.drawImage(face, 0, 0, null);
		} catch (IOException e) {
			
		} catch (IllegalArgumentException e){
			
		}
		if(getArousal().percent()>= 75){
			if(blushHigh == null){
				try {
					blushHigh = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+"_blush3"+res+".png"));
					g.drawImage(blushHigh, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(blushHigh, 0, 0, null);
			}
		}else if(getArousal().percent()>= 50){
			if(blushMed == null){
				try {
					blushMed = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+"_blush2"+res+".png"));
					g.drawImage(blushLow, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(blushMed, 0, 0, null);
			}
		}if(getArousal().percent()>= 25){
			if(blushLow == null){
				try {
					blushLow = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+"_blush1"+res+".png"));
					g.drawImage(blushLow, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(blushLow, 0, 0, null);
			}
		}
		if(spriteAccessory == null){
			try {
				spriteAccessory = ImageIO.read(Global.gui().getClass().getResource(
						"assets/"+name+costume+"_accessory"+res+".png"));
				g.drawImage(spriteAccessory, 0, 0, null);
			} catch (IOException e) {
				
			} catch (IllegalArgumentException e){
				
			}
		}else{
			g.drawImage(spriteAccessory, 0, 0, null);
		}
		if(is(Stsflag.beastform)){
			if(spriteKemono == null){
				try {
					spriteKemono = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+getCostumeSet()+"_kemono"+res+".png"));
					if(spriteKemono == null) {
						spriteKemono = ImageIO.read(Global.gui().getClass().getResource(
								"assets/" + name + "_kemono" + res + ".png"));
					}
					g.drawImage(spriteKemono, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteKemono, 0, 0, null);
			}
		}else{
			if(spriteAccessory2 == null){
				try {
					spriteAccessory2 = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_accessory2"+res+".png"));
					g.drawImage(spriteAccessory2, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteAccessory2, 0, 0, null);
			}
		}
		if(isWearing(ClothingType.UNDERWEAR)){
			if(has(Trait.strapped)){
				if(spriteStrapon == null){
					try {
						spriteStrapon = ImageIO.read(Global.gui().getClass().getResource(
								"assets/"+name+"_strapon"+res+".png"));
						g.drawImage(spriteStrapon, 0, 0, null);
					} catch (IOException e) {
						
					} catch (IllegalArgumentException e){
						
					}
				}else{
					g.drawImage(spriteStrapon, 0, 0, null);
				}
			}
			else if(spriteUnderwear == null){
				try {
					spriteUnderwear = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_underwear"+res+".png"));
					g.drawImage(spriteUnderwear, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteUnderwear, 0, 0, null);
			}
		}
		if(isWearing(ClothingType.TOPUNDER)){
			if(spriteBra == null){
				try {
					spriteBra = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_bra"+res+".png"));
					g.drawImage(spriteBra, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteBra, 0, 0, null);
			}
		}
		if(isWearing(ClothingType.BOTOUTER)){
			if(spriteBottom == null){
				try {
					spriteBottom = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_bottom"+res+".png"));
					g.drawImage(spriteBottom, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteBottom, 0, 0, null);
			}
		}
		if(isWearing(ClothingType.TOP)){
			if(spriteTop == null){
				try {
					spriteTop = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_top"+res+".png"));
					g.drawImage(spriteTop, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteTop, 0, 0, null);
			}
		}
		if(isWearing(ClothingType.TOPOUTER)){
			if(spriteOuter == null){
				try {
					spriteOuter = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_outer"+res+".png"));
					g.drawImage(spriteOuter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteOuter, 0, 0, null);
			}
		}if(hasDick() && !isWearing(ClothingType.BOTOUTER)){
			if(arousal.get()<15){
				if(spritePenisSoft == null){
					try {
						spritePenisSoft = ImageIO.read(Global.gui().getClass().getResource(
								"assets/"+name+"_penis_soft"+res+".png"));
						g.drawImage(spritePenisSoft, 0, 0, null);
					} catch (IOException e) {
						
					} catch (IllegalArgumentException e){
						
					}
				}else{
					g.drawImage(spritePenisSoft, 0, 0, null);
				}
			}else{
				if(spritePenisHard == null){
					try {
						spritePenisHard = ImageIO.read(Global.gui().getClass().getResource(
								"assets/"+name+"_penis_hard"+res+".png"));
						g.drawImage(spritePenisHard, 0, 0, null);
					} catch (IOException e) {
						
					} catch (IllegalArgumentException e){
						
					}
				}else{
					g.drawImage(spritePenisHard, 0, 0, null);
				}
			}
		}
		if(Global.checkFlag(Flag.statussprites)){
			BufferedImage sts = Global.gui().loadStatusFilters(this);
			if(sts != null){
				g.drawImage(sts, 0, 0, null);
			}
		}
		g.dispose();
		return sprite;
	}
	public void clearSpriteImages() {
		spriteBody = null;
		spriteAccessory = null;
		spriteUnderwear = null;
		spriteStrapon = null;
		spriteBra = null;
		spriteBottom = null;
		spriteTop = null;
		spriteOuter = null;
		spriteCoattail = null;
		spritePenisHard = null;
		spritePenisSoft = null;
		blushLow = null;
		blushMed = null;
		blushHigh = null;

		
	}
}
