package characters;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.imageio.ImageIO;

import com.google.gson.JsonObject;
import combat.Combat;
import combat.Encounter;
import combat.Result;
import global.Global;
import global.Match;
import global.Modifier;
import items.ClothingType;
import skills.Skill;
import skills.Tactics;
import trap.Trap;

public class Dummy extends Character {
	int costume;
	boolean topouter;
	boolean top;
	boolean topinner;
	boolean botouter;
	boolean botinner;
	int blush;
	Emotion mood;
	boolean strapped;

	public Dummy(String name) {
		super(name, 1);
		costume = 1;
		topouter = false;
		top = false;
		topinner = false;
		botouter = false;
		botinner = false;
		blush = 0;
		strapped = false;
		mood = Emotion.confident;
	}
	
	public Dummy(String name, int costume, boolean dressed) {
		this(name);
		this.costume = costume;
		if(dressed){
			dress();
		}
	}

	@Override
	public void ding() {
		// TODO Auto-generated method stub

	}

	@Override
	public void detect() {
		// TODO Auto-generated method stub

	}

	@Override
	public void faceOff(Character opponent, Encounter enc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void spy(Character opponent, Encounter enc) {
		// TODO Auto-generated method stub

	}

	@Override
	public String describe(int per) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void victory(Combat c, Result flag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void defeat(Combat c, Result flag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void intervene3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub

	}

	@Override
	public void victory3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub

	}

	@Override
	public void act(Combat c) {
		// TODO Auto-generated method stub

	}

	@Override
	public void move(Match match) {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(Combat c, Result flag) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean human() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String bbLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nakedLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String stunLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String winningLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String taunt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void intervene(Encounter enc, Character p1, Character p2) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean resist3p(Combat combat, Character intruder, Character assist) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void showerScene(Character target, Encounter encounter) {
		// TODO Auto-generated method stub

	}

	@Override
	public JsonObject save() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void load(Scanner loader) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterParty() {
		// TODO Auto-generated method stub

	}

	@Override
	public void eot(Combat c, Character opponent, Skill last) {
		// TODO Auto-generated method stub

	}

	@Override
	public Emotion moodSwing() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resetOutfit() {

	}

	@Override
	public String challenge(Character opponent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void promptTrap(Encounter enc, Character target, Trap trap) {
		// TODO Auto-generated method stub

	}

	@Override
	public void counterattack(Character target, Tactics type, Combat c) {
		// TODO Auto-generated method stub

	}
	public void setCostumeLevel(int set){
		costume = set;
		clearSpriteImages();
	}
	
	public void setBlush(int level){
		blush = level;
	}
	
	public void dress(){
		topouter = true;
		top = true;
		topinner = true;
		botouter = true;
		botinner = true;
	}
	
	public void undress(){
		topouter = false;
		top = false;
		topinner = false;
		botouter = false;
		botinner = false;
	}
	
	public void setTopOuter(boolean wearing){
		topouter = wearing;
	}

	public void setTop(boolean wearing){
		top = wearing;
	}
	
	public void setTopInner(boolean wearing){
		topinner = wearing;
	}
	
	public void setBotOuter(boolean wearing){
		botouter = wearing;
	}
	
	public void setBotInner(boolean wearing){
		botinner = wearing;
	}
	
	public void setStrapped(boolean wearing){
		strapped = wearing;
	}
	
	public void setMood(Emotion mood){
		this.mood = mood;
	}
	
	@Override
	public String getPortrait() {
		return "assets/"+name+"_"+ mood.name()+".jpg";
	}
	
	@Override
	public BufferedImage getSpriteImage(){
		String res = "";
		if(Global.gui().lowRes()){
			res = "_low";
		}
		if(spriteBody == null){
			try {
				spriteBody = ImageIO.read(Global.gui().getClass().getResource(
						"assets/"+name+"_Sprite"+res+".png"));
			} catch (IOException e) {
				
			} catch (IllegalArgumentException e){
				
			}
		}
		if(spriteBody == null){
			return null;
		}
		BufferedImage sprite = new BufferedImage(spriteBody.getWidth(), spriteBody.getHeight(), spriteBody.getType());
		Graphics2D g = sprite.createGraphics();
		g.drawImage(spriteBody,0,0,null);
		try {
			BufferedImage face = ImageIO.read(Global.gui().getClass().getResource(
					"assets/"+name+"_"+mood.name()+res+".png"));
			g.drawImage(face, 0, 0, null);
		} catch (IOException e) {
			
		} catch (IllegalArgumentException e){
			
		}
		if(blush >= 3){
			if(blushHigh == null){
				try {
					blushHigh = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+"_blush3"+res+".png"));
					g.drawImage(blushHigh, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(blushHigh, 0, 0, null);
			}
		}else if(blush == 2){
			if(blushMed == null){
				try {
					blushMed = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+"_blush2"+res+".png"));
					g.drawImage(blushLow, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(blushMed, 0, 0, null);
			}
		}if(blush == 1){
			if(blushLow == null){
				try {
					blushLow = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+"_blush1"+res+".png"));
					g.drawImage(blushLow, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(blushLow, 0, 0, null);
			}
		}
		if(spriteAccessory == null){
			try {
				spriteAccessory = ImageIO.read(Global.gui().getClass().getResource(
						"assets/"+name+costume+"_accessory"+res+".png"));
				g.drawImage(spriteAccessory, 0, 0, null);
			} catch (IOException e) {
				
			} catch (IllegalArgumentException e){
				
			}
		}else{
			g.drawImage(spriteAccessory, 0, 0, null);
		}
		if(topouter){
			if(spriteCoattail == null){
				try {
					spriteCoattail = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_outerback"+res+".png"));
					g.drawImage(spriteCoattail, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteCoattail, 0, 0, null);
			}
		}
		if(botinner){
			if(strapped){
				if(spriteStrapon == null){
					try {
						spriteStrapon = ImageIO.read(Global.gui().getClass().getResource(
								"assets/"+name+"_strapon"+res+".png"));
						g.drawImage(spriteStrapon, 0, 0, null);
					} catch (IOException e) {
						
					} catch (IllegalArgumentException e){
						
					}
				}else{
					g.drawImage(spriteStrapon, 0, 0, null);
				}
			}
			else if(spriteUnderwear == null){
				try {
					spriteUnderwear = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_underwear"+res+".png"));
					g.drawImage(spriteUnderwear, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteUnderwear, 0, 0, null);
			}
		}
		if(topinner){
			if(spriteBra == null){
				try {
					spriteBra = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_bra"+res+".png"));
					g.drawImage(spriteBra, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteBra, 0, 0, null);
			}
		}
		if(botouter){
			if(spriteBottom == null){
				try {
					spriteBottom = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_bottom"+res+".png"));
					g.drawImage(spriteBottom, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteBottom, 0, 0, null);
			}
		}
		if(top){
			if(spriteTop == null){
				try {
					spriteTop = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_top"+res+".png"));
					g.drawImage(spriteTop, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteTop, 0, 0, null);
			}
		}
		if(topouter){
			if(spriteOuter == null){
				try {
					spriteOuter = ImageIO.read(Global.gui().getClass().getResource(
							"assets/"+name+costume+"_outer"+res+".png"));
					g.drawImage(spriteOuter, 0, 0, null);
				} catch (IOException e) {
					
				} catch (IllegalArgumentException e){
					
				}
			}else{
				g.drawImage(spriteOuter, 0, 0, null);
			}
		}
		g.dispose();
		return sprite;
	}

	@Override
	public void watcher(Combat c, Character victory, Character defeated) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String watched(Combat c, Character voyeur, Character defeated) {
		// TODO Auto-generated method stub
		return null;
	}


}
