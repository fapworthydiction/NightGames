package characters;

import daytime.Daytime;
import global.*;

import items.Clothing;
import items.Item;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentGroup;
import Comments.CommentSituation;
import combat.Combat;
import combat.Result;

import skills.Skill;
import skills.Tactics;
import stance.Stance;
import status.Energized;
import status.Stsflag;
import actions.Action;
import actions.Movement;
import actions.Resupply;
import actions.Move;
import areas.Area;

public class Cassie implements Personality {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8601852023164119671L;
	public NPC character;
	
	public Cassie(){
		character = new NPC("Cassie",ID.CASSIE,1,this);
		character.outfit[0].add(Clothing.bra);
		character.outfit[0].add(Clothing.blouse);
		character.outfit[1].add(Clothing.panties);
		character.outfit[1].add(Clothing.skirt);
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.blouse);
		character.closet.add(Clothing.panties);
		character.closet.add(Clothing.skirt);
		character.change(Modifier.normal);
		character.mod(Attribute.Power, 1);
		character.mod(Attribute.Seduction, 1);
		character.mod(Attribute.Cunning, 1);
		character.mod(Attribute.Perception, 1);
		Global.gainSkills(character);
		character.add(Trait.female);
		character.add(Trait.softheart);
		character.add(Trait.romantic);
		character.add(Trait.imagination);
		character.setUnderwear(Trophy.CassieTrophy);
		character.plan = Emotion.bored;
		character.mood = Emotion.confident;
	}
	@Override
	public Skill act(HashSet<Skill> available,Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a: available){
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
        }
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}
	@Override
	public void rest(int time, Daytime day) {
		if(character.rank>=1){
			if(character.money>0){
				day.visit("Magic Training", character, Global.random(character.money));
			}
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		if(character.rank>0){
			available.add("Reference Room");
			available.add("Workshop");
		}
		available.add("Play Video Games");
		for(int i=0;i<time-4;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.CassieDWV, 1);
		}
		character.visit(4);
	}

	@Override
	public String describe() {
		if(character.has(Trait.witch)){
			return "Cassie has changed a lot since you started the Game. Maybe she isn't that different physically. She has the same bright blue eyes and the same sweet smile. " +
					"The magic spellbook and cloak are both new. She's been dabbling in the arcane, and it may be your imagination, but you feel like you can perceive the power " +
					"radiating from her. Her magic seems to have given her more confidence and she seems even more eager than usual.";
		}
		else{
			return character.name+" is a cute girl with shoulder-length auburn hair, clear blue eyes, and glasses. She doesn't look at all like the typical sex-fighter. " +
				"She's short with modest breasts. She's not chubby, but you would describe her body as soft rather than athletic. Her gentle tone and occasional " +
				"flickers of shyness give the impression of sexual innocence, but she seems determined to win.";
		}
	}
	@Override
	public String victory(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag==Result.anal){
			character.arousal.empty();
			Global.gui().displayImage("Cassie_pegging.jpg", "Art by AimlessArt");
			Global.modCounter(Flag.PlayerAssLosses, 1);
			return "Cassie bucks her hips against your ass wildly causing the strapon to rub hard against your prostate. Your arms and legs feel like jelly as she thrusts in again and again. " +
					"You're almost shocked as you feel yourself on the edge of orgasm and you're certain you wouldn't be able to stop yourself if Cassie keeps this pace up. Above you Cassie moans " +
					"loudly clearly in a world of her own. You don't think she even notices as the pleasure from your prostate overcomes you and you shoot your white flag of surrender on the " +
					"ground. As you orgasm, Cassie's thrusting kicks up another notch and shortly afterwards she comes from the stimulation of the strapon rubbing against her clit. You both collapse " +
					"to the ground and lie there for a minute or so catching your breaths. <i>\"I guess I got a bit too carried away and lost.\"</i> murmurs Cassie. You sigh internally and point out that " +
					"you actually came while she was pegging you. <i>\"You came?\"</i> she gasps. <i>\"I mean the shopkeeper said it would work but....\"</i> she trails off.  She smiles, and stands. <i>\"I never knew " +
					"I'd enjoy that so much.\"</i> Her grin widens in a way that makes you nervous. <i>\"I might need to try that again in the future.\"</i> You decide to bid a hasty retreat leaving your " +
					"clothes behind to the victor.";
		}
		else if(flag==Result.intercourse){
			return "You feel yourself rapidly nearing the point of no return as Cassie rides your dick. You fondle and tease her sensitive nipples to increase her pleasure, but it's a losing battle. You're " +
					"going to cum first. She smiles gently and kisses you as you ejaculate inside her hot pussy. She shivers slightly, but you know she hasn't climaxed yet.<p>"
					+ "When she breaks the kiss, her flushed " +
					"face lights up in a broad smile. <i>\"It feels like you released a lot. Did you feel good?\"</i> You groan and slump flat on the ground in defeat. She gives you a light kiss on the tip of your nose " +
					"and starts to grind her clit against your pelvis.<p>"
					+ "<i>\"Come on, don't be mean. Tell me I made you feel good,\"</i> she whispers in a needy voice. <i>\"It'll help me finish faster.\"</i> Is she really " +
					"getting off on praise, or on the knowledge that her technique gave you pleasure? Either way, there's no reason to lie, she definitely made you feel amazing. She shudders and starts to breathe " +
					"harder as you whisper to her how good her pussy felt.<p>"
					+ "She leans forward to present her modest breasts to you. <i>\"Can you touch my nipples more? I really like that.\"</i> You reach up and play with " +
					"her breasts as she continues to grind against you. She stops your pillow talk by kissing you desperately just before you feel her body tense up in orgasm. She collapses on top of you and kisses " +
					"you cheek contently.<p>"
					+ "<i>\"I'll keep practicing and make you feel even better next time, \"</I> she tells you happily. <i>\"I promise.\"</i> ";
		}
		else if(character.has(Trait.witch)&&character.has(Trait.silvertongue)&&Global.random(2)==0){
			character.arousal.empty();
			return "Cassie's efforts to pleasure you finally break your resistance and you find yourself completely unable to stop her. She slips between your legs and takes your straining " +
					"dick into her mouth. She eagerly sucks on your cock, while glancing up to meet your eyes. Her talented oral technique blows away your endurance and you spill your seed " +
					"into her mouth. She swallows your cum and smiles at you excitedly.<p>"
					+ "<i>\"Gotcha. Did that feel good?\"</i> You nod and slump to the floor to catch your breath.<p>"
					+ "Cassie goes " +
					"quiet for a bit and you realize you still need to return the favor. <i>\"It's not that,\"</i> Cassie replies when you broach the subject. <i>\"I've just been learning a spell that " +
					"I kinda want to try. Can we try it?\"</i> You nod your consent. You trust Cassie not to do anything really bad to you. She softly chants a spell with a fairly long incantation " +
					"and then kisses you on the lips. <p>" +
					"<i>\"Let's see if it worked.\"</i> She seductively slides down your body to bring her face next to your now flaccid dick again. She licks and " +
					"sucks your member until it returns to full mast. She lets your dick go and grins, looking more flushed and aroused than usual. <i>\"So far, so good.\"</i> She reaches between her " +
					"legs and masturbates in front of you. A gasp escapes you as you feel an unfamiliar pleasure between your legs. <i>\"Our senses are temporarily linked,\"</i> Cassie explains. " +
					"<i>\"I've always wondered what a blowjob felt like. I can see why you like it.\"</i><p>"
					+ "She leans into your chest and you embrace her, feeling a comfortable warmth. <i>\"Now that we're " +
					"sharing everything, shall we really feel good together?\"</i> She guides your penis into her waiting entrance and you both let out a moan as you thrust into her. In addition to " +
					"the usual pleasure from being surrounded by her hot, wet pussy, you're hit by a second surge of pleasure and a pleasant fullness you're not used to. Cassie seems pretty " +
					"overwhelmed by that simple movement as well. It's unlikely either of you will be able to last very long at this rate. <i>\"Probably not,\"</i> Cassie gasps out. <i>\"But imagine how it'll " +
					"feel if we cum together.\"</i> That's an intimidating prospect given how much insertion affected you both, but it's worth undertaking.<p>"
					+ "Cassie rocks her hips on top of you while you " +
					"thrust slowly and steadily. Despite the dual channel pleasure making concentrating on your movements almost impossible, the two of you are able to synchronize perfectly. You " +
					"can feel the exact angle and speed to thrust to maximize Cassie's pleasure and she's probably doing the same for you. Without saying anything, you both accelerate your thrusts " +
					"as you both approach climax. You've never felt a girl's orgasm before, the pleasure seems to come from deep inside your core (her core technically), but you're pretty sure she's " +
					"as close as you are. Cassie kisses you passionately right as you both go over the edge and you shoot your seed into her shuddering pussy. It feels like you've been struck by lightning " +
					"and your vision goes white.<p>"
					+ "You gradually come to your senses and see Cassie collapsed next to you. Your faces are only inches apart and you can't resist kissing her gently as " +
					"she regains consciousness. The feeling is noticeably singular and you feel somehow lonely as you realize her spell must have worn off. <i>\"Wow,\"</i> she lets out breathlessly. " +
					"<i>\"That felt like I was 12 again and masturbating for the first time.\"</i> She suddenly turns bright red and hides her face in your chest. <i>\"You didn't hear that! Just pretend I " +
					"didn't say anything.\"</i>";
		}
		else if(character.arousal.percent()>50){
			character.arousal.empty();
			return "Despite your best efforts, you realize you've lost to Cassie's diligent manipulation of your penis. It takes so much focus to hold back your ejaculation " +
					"that you can't even attempt to retaliate. She pumps your twitching dick eagerly as the last of your endurance gives way. The pleasure building up in the base " +
					"of your shaft finally overwhelms you and you almost pass out from the intensity of your climax. White jets of semen coat Cassie's hands in the proof of your defeat. <p>" +
					"As you recover, you notice Cassie restlessly rubbing her legs together with unfulfilled arousal and offer to help get her off however she prefers. She looks down at " +
					"your spent, shrivelled dick and gently fondles it while pouting cutely.<p>"
					+ "<i>\"I have a cute boy all to myself, but he's already worn out.\"</i> She leans in close and whispers in " +
					"your ear, <i>\"If you get hard again, we can have sex.\"</i><p>" +
					"Your cock responds almost immediately to her words and her soft caress. In no time, you're back to full mast. Cassie " +
					"straddles your hips and guides the head of your member to her entrance. She leans down to kiss you passionately as she lowers herself onto you. As you pierce her tight, wet pussy, " +
					"she moans into your mouth. She rides you enthusiastically and you can feel your pleasure building again despite having just cum.<p>"
					+ "Cassie is breathing heavily and clearly on the " +
					"verge of her own orgasm. You fondle and pinch her nipples, which pushes her over the edge. Her pussy clamps down on you, squeezing out your second load. As her stamina gives out, " +
					"she collapses next to you.<p>"
					+ "<i>\"Best prize ever. I should beat you more often,\"</i> you hear her mutter.";
		}
		else{
			return "Despite your best efforts, you realize you've lost to Cassie's diligent manipulation of your penis. It takes so much focus to hold back your ejaculation " +
				"that you can't even attempt to retaliate. She pumps your twitching dick eagerly as the last of your endurance gives way. The pleasure building up in the base " +
				"of your shaft finally overwhelms you and you almost pass out from the intensity of your climax. White jets of semen coat Cassie's hands in the proof of your defeat. " +
				"You recover your senses enough to offer to return the favor.<P>"
				+ "<i>\"No need,\"</i> she teases good-naturedly. <i>\"I have a bit more self-control than a horny boy.\"</i><p>"
				+ "Her victorious smile is " +
				"bright enough to light up a small city as she gives you a chaste kiss on the cheek and walks away, taking your clothes as a trophy.";
		}
	}
	@Override
	public String defeat(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(flag==Result.intercourse){
			return "As you thrust repeatedly into Cassie's slick folds, she trembles and moans uncontrollably. You lean down to kiss her soft lips and she responds by wrapping her arms around " +
					"you. You feel her nails sink into your back as she clings to you desperately. Her insides tighten and shudder around your cock as she orgasms. You keep kissing her and stroking " +
					"her hair until she goes limp.<p>"
					+ "When you break the kiss she covers her beet red face with both hands. <i>\"I can't believe I came alone. You made me feel so good, I couldn't help it.\"</i> " +
					"You can't see her expression, but her voice sounds sheepish rather than defeated. <p>"
					+ "You spot her glasses on the floor nearby, knocked off in the throes of her orgasm. You pick them " +
					"up and gently push her hands away from her face. She's flushed and her bangs are matted to her face with sweat, but she's as beautiful as ever.<p>"
					+ "You place her glasses on her " +
					"head and she smiles shyly. <i>\"I want you to keep moving. We won't be done until you orgasm too.\"</i> Your cock twitches inside her as if trying to remind you of its need. Cassie must " +
					"feel it, because she giggles and kisses you lightly.<p>"
					+ "You start thrusting again and she gasps with delight. She's still sensitive from her climax and if possible you want to give her " +
					"another. You suck gently on her earlobe and feel her twitch in surprise at the sensation. You know you won't last much longer in her warm, tight pussy, but Cassie is completely entrusting " +
					"her body to you, giving you the freedom to pleasure her. You work your way down her neck, kissing, licking and listening to her breathing grow heavier. <p>"
					+ "Her reactions are having a " +
					"more of an effect on you than you expected. Soon you need to slow down to maintain control. <i>\"Keep going,\"</i> Cassie coos. <i>\"I want you to feel good. I want you to feel good because " +
					"of me.\"</i> You don't think she's quite there yet, but you speed up like she asks.<p>"
					+ "In moments, you hit your peak and shoot your load inside her. Cassie lets out a moan and you feel her " +
					"shudder. Did she just cum again?<p>"
					+ "She giggles contently. <i>\"I guess having a cute boy climax inside me is a big turn-on. We should do this more often.\"</i> If she wants to lose to you more " +
					"often, you aren't going to complain.<p>"
					+ "She sits up and kisses you softly on the cheek. <i>\"Maybe I'll win next time.\"</i>";
		}
		if(character.is(Stsflag.bound)){
			return "<i>\"What's the matter?\"</i> Cassie says, trying her best to give you a taunting look despite the fact that her flushed cheeks completely ruin the effect. <i>\"Are you too weak to overpower a helpless girl, needing to resort to binding her instead?\"</i><p>"
					+ "Cassie is anything but helpless, you well know, and she's easily capable of managing to get out of her restraints given enough time. So of course you don't plan to give her this time. She's close to the edge now, and just needs a final push to finish her off, but that's no reason to get careless. You push Cassie to the ground as you loom menacingly over her, deciding to return her taunts with some of her own. She should know well what the heroine is in for if she lets herself get captured by the villain, after all.<p>"
					+ "<i>\"You wouldn't dare!\"</i> Cassie says, immediately catching on to roleplay idea and enthusiastically playing along. Perhaps she knows she's already lost by this point and has just decided to have some fun with it, or perhaps she's just turned on enough that she isn't thinking straight right now. There's even a slim chance she's doing this to try to catch you off-guard soon, so you'll have to be on the watch for that. <i>\"Try your best, blackguard! I'll never succumb to your dark desires!\"</i><p>"
					+ "Blackguard? Was that seriously the insult she was going for?<p>"
					+ "Cassie pouts and whispers back to you, <i>\"It's the best I could think of in the moment, so sue me! But fine, if you're going to- Mmf!\"</i><p>"
					+ "Just as you sense Cassie is about to try and turn the tables on you, you cut her off by capturing her lips within a dominating kiss. This is, after all, how the villain traditionally starts his assault on the heroine. You don't leave it at just that, though. You hold down her arms with one hand, trying to keep her from wriggling out of her constraints, while your other hand grasps her breast. As you feel up her breast and gently play with her nipple, you make sure to use your legs to keep hers pinned down, leaving Cassie with no avenues of escape.<p>"
					+ "Cassie moans into the kiss in protest, though you can tell by how weak her movements are that this is probably more token resistance than anything else. She's too far gone to be able to come back from this, so you feel safe giving her just a bit of an opening as you move in for the kill. You pull back from the kiss and give Cassie your best villainous smile. Time for the main course.<p>"
					+ "<i>\"What?\"</i> Cassie says, her eyes opening. She rolls back and forth beneath you, trying - or at least, pretending to try - to get away. <i>\"You can't mean - no! No one's ever touched me there before! Please have mercy!\"</i><p>"
					+ "There's no mercy for Cassie tonight. You hold her down as you push yourself back. You move your hand from her breast to her leg, pulling it to the side as you lean in and bring your mouth to her pussy. Even in the dim light of night, you can see the juices of it glistening. It will probably only take the slightest touch to push Cassie over the edge, and now it's time to provide just that touch. You move in, licking your tongue across the length of Cassie's slit, and then pushing inside her lips.<p>"
					+ "<i>\"Noooo!\"</i> Cassie cries out, though the flood of juices from her pussy betray her protest. <i>\"This can't... be happening... I can't... be enjoying this...\"</i> Confident that Cassie isn't going to truly try to fight back at this point, you move your other hand down to her free leg and pull it to the side as well, spreading her open and freeing your tongue to fully explore the depths of her pussy. <i>\"No... no... no no no no nononononooooooo!\"</i><p>"
					+ "Cassie's hips buck wildly from the force of her orgasm, nearly tossing you up into the air. You manage to hold onto her though, and you continue your assault on her cunt through it all. There's no monetary benefit from prolonging her orgasm like this, but you just can't help yourself. You want to see how far you can push her. And as it turns out, you're able to push her just far enough to hit a second peak before her body finally gives out on her and she collapses back to the ground.<p>"
					+ "<i>\"No more...\"</i> Cassie says. You can hear her panting heavily now, and from her tone of voice you gather that she's serious this time. You plant one final kiss on her lower lips, and then move back up to her face, giving her a gentle, lingering kiss to cap off this experience. <i>\"Mmm...\"</i> she says. <i>\"We should roleplay more often. This was fun.\"</i><p>"
					+ "It was. But it's not quite over yet, you make clear to her, motioning down to your erect penis. She wouldn't exactly expect the villain to let her leave him unsatisfied, would she?<p>"
					+ "Cassie chuckles at this. <i>\"Of course. I suppose you do deserve a reward for that,\"</i> she says. She begins to try to free her hands from their restraints, but a firm glare from you quickly disabuses her of this notion. She's still your captive until she's earned her freedom. Chuckling again, but with a bit more of a smile this time, Cassie awkwardly rolls you over onto your back, and then moves down to take your cock between her lips.<p>"
					+ "With as long as you've held off, and as good as Cassie is at this, it only takes moments before you explode into her mouth. Cassie offers a moan in token protest, but she obediently takes your load in her mouth. After swallowing the first batch, she even makes sure to suck out the remaining cum from your dick. She dutifully cleans your cock off as it softens, until finally she pulls back and looks up at you with pleading eyes.<p>"
					+ "<i>\"Have I earned my freedom now?\"</i> she says. Her expression is simply irresistible, so you smile and nod at her. <i>\"Good,\"</i> she says, quickly slipping her hands out of their bonds. She can't seem to completely wipe off the smile on her face as the two of you stand up. <i>\"I should remind you though, before we meet again: The hero always wins at the end of Act 3. Just you wait.\"</i><p>"
					+ "The hero might win at the end of Act 3, but there's no telling just how long Acts 1 and 2 will last. Cassie might find herself in for quite the ordeal in the meantime.";
		}
		else if(!character.pantsless() && !character.topless()){
			return "You're very surprised as Cassie leans back against the wall, panting with need. Her face is flushed red. Her knees are shaking. She's rubbing her thighs together. And she's still wearing clothes. You're not certain how you managed to overwhelm her like this, but she clearly has little resistance left in her.<p>" +
					"A mean thought passes through your head, and you realize you could have some fun with the situation. You step close to her, and stare into her eyes with a small grin. She stares back, and doesn't notice as your arms snake forward, and your hands slip under her shirt. She gasps, but before she can respond, you begin tickling her, your fingers dancing along her sides. She can't stop herself from laughing, even as she tries and fails to push you away. <i>\"Wh-what are you doing- haha! This isn't fahah-fair!\"</i> You laugh with her and ask what's wrong. She's not enjoying this? <i>\"I need to cum! I'm so close, and this won't hel- ahaha!\"</i><p>" +
					"Finally deciding to show mercy and end the match, you pull your hands out from under her shirt. She slumps back against the wall, even more tired than she was a moment ago, and you pin her against it. She puts up a token resistance, but there's clearly no hope of her escaping you at this point, especially with how needy she is. One of your hands starts groping at her breasts. The other moves between her legs, her panties and clothes soaked through with her arousal.<p>" +
					"As you tease her closer and closer to orgasm, she stops her struggling. She gives herself up to your ministrations and moans in pleasure. She squeezes your hand between her thighs as your fingers push through her clothes to tease her vulva. Her breasts are sensitive from friction as you gently grope them through her top. She shudders against you, and you catch her mouth in a passionate kiss as she finally orgasms.<p>" +
					"As she comes down from her sexual high, her strength gives out and you help gently sit on the floor. You give a moment to recover as she slowly begins catching her breath. She puts her head in her arms and groans. <i>\"I can't believe you got me like this. It's so…so…\"</i> You give her a pat on the head, assuring her that it happens to the best. <i>\"Maybe, but it's still really embarrassing.\"</i><p>" +
					"You let her wallow in her self-pity for a moment before politely reminding her that you need her clothes now... and that you could use some help with your erection. She groans even louder than before. <i>\"So now after you made me cum with my clothes on, I still have to get undressed.\"</i> She looks up at you, trying to act annoyed. She can't keep the façade up and ends up giving you a small smile despite her embarrassment. <i>\"Well…you did make me feel good. So, I suppose I owe you an orgasm. Alright, then. Let me just…\"</i><p>" +
					"Cassie rises to her feet as she decides how to pleasure you. With a blush, she begins putting on an inexperienced striptease. She starts with her shirt, crossing her arms as she reaches for the hem. She slowly lifts it upwards, slowing down even more as she approaches her breasts. She keeps her shirt tight against her as her bra comes into view, pulling her breasts up with her shirt. As it clears her breasts, they bounce pleasantly as they fall into position. She moves onto her skirt next. She slowly stretches the elastic band around her waist as she begins lowering it. Her panties are quickly revealed, at which point she simply drops the skirt to the floor and steps over it.<p>" +
					"Now wearing only her bra and panties, she approaches you. She leans in close, looking a little unsure of herself. <i>\"Am I…doing this right?\"</i> You assure her that her show is very arousing. She smiles, grateful for your words. <i>\"Thank you.\"</i> With that, she gives you a quick kiss before backing up again. She reaches back and undoes her bra. She holds in place with her hands for a moment before letting it drop to the floor, allowing her breasts to lightly bounce free. Finally, she reaches for her panties and lowers them teasingly, showing a more and more of her vulva, bit by bit until her most intimate of areas is completely revealed.<p>" +
					"She surprises you by leaning back against the wall once more. <i>\"Well…here you go. Come get your reward.\"</i> You eagerly take her up on that offer. Your dick is hard, and she's still wet from her orgasm. You waste no time in approaching her once again and pushing into her depths. You moan in unison as your mouths join in a kiss. You catch her arms with your hands and pin her against the wall. Cassie shivers, and she begins squeezing tighter around you. She bounces as best she can while pressed against the wall in time with your thrusts. As aroused as you have been for a while, it isn't long before you find yourself close to finishing, and it seems like Cassie is approaching a second high as well. You cum together and hug each other while orgasm rocks each of you. While Cassie recovers from her two orgasms, you redress and pick up her clothes as your prize. <i>\"You know…\"</i> Cassie speaks before you leave. <i>\"I'm not going to let you pull that one off again.\"</i> With a little bit of smile, you reply that she's welcome to try and stop you.<p>";
		}
		else if(opponent.getArousal().percent()<=20){
			return "This is weird, even for you. Cassie sags against a wall, holding on by her fingertips, flushed, sweaty, and all but defeated. You can smell her arousal from here, like subtle perfume.<p>"
					+ "And you're barely hard. She's been going after you with everything she's got for the last five minutes and it bounced right off. You're not sure whether to credit your training or wonder if she's coming off a bad bout.<p>"
					+ "Either way, this is how the Game is played. There's no reason not to have some fun with this. She'd be doing the same to you. Hell, she has.<p>"
					+ "You grab her by the shoulder and flip her around, putting her back to the wall, as you get rid of what little is left of her clothing. Her eyes are barely in focus; she's not really here; she's caught up in an erotic daze, breathing in soft little gasps that are more arousing than anything she tried to do to you before.<p>"
					+ "There's no resistance as you slide two fingers inside her. She's too wet for that, and too eager for any kind of sensation. You've driven her into a quiet sort of frenzy, and Cassie begins to gasp out loud as you finger-fuck her, bucking her hips back at you. When your thumb finds her clit, that's endgame, and Cassie starts to inarticulately scream, loud enough that there's no way the other competitors won't hear her. You're on a time limit now.<p>"
					+ "You clap your other hand over her mouth, and when she finally cries out in orgasm, you feel it as a wet vibration against your palm. When you let Cassie go, she collapses against the wall, braced with both hands, flushed bright red and breathing in big ragged gulps.<p>"
					+ "That's where you leave her, barely holding herself vertical, as you collect what's left of her clothes.<p>"
					+ "<i>\"That,\"</i> Cassie says, in a voice like a croak, <i>\"was rude.\"</i><p>"
					+ "You have to grin at her, and make an effort to keep it light. No wolf, no teeth, just as if you're sharing a private joke.<p>"
					+ "<i>\"I am going to get you back so hard for this.\"</i><p>"
					+ "You're looking forward to it.";
		}
		else if(character.has(Trait.witch)&&Global.random(2)==0){
			opponent.add(new Energized(opponent,10));
			return "You capture Cassie's lips and slip your hand between her legs to facilitate her imminent orgasm. You rub her soaked pussy lips and she moans against your lips. Her body " +
					"tenses as she clings to you, letting you know she's reached her climax. You keep rubbing her petals as she starts to relax. She shows no sign of breaking the kiss or " +
					"letting you go, so you decide to see if you can give her consecutive orgasms.<p>"
					+ "You dig your fingers into Cassie's sensitive pussy and rub her insides. She eyes open " +
					"wide and she lets out a noise of surprise. You tease her tongue with your own and she melts against you again. It only takes a few minutes before her pussy squeezes your " +
					"fingers and she hits her second orgasm.<p>"
					+ "Your fingers don't even slow down this time. You move away from her lips to focus on licking and sucking her neck. Her pussy twitches " +
					"erratically as you finger her.<p>"
					+ "<i>\"It's so intense! I can't stop twitching!\"</i> She moans plaintively, but she doesn't seem to dislike it. <i>\"I love it! But I think I'm going to die!\"</i> " +
					"You've been going easy on her clit up until now, but now you rub it firmly with your thumb and gently bite down on her collarbone. She screams in pleasure through her third orgasm, " +
					"which lasts much longer than her first two.<p>"
					+ "Cassie goes limp as you hold her tenderly. You haven't had any release, but she seems in no condition to help now. She makes " +
					"a content noise and looks ready to fall asleep in your arms. You tickle her lightly and remind her that the match isn't over.<p>"
					+ "<i>\"Can't go on,\"</i> She murmurs sleepily. <i>\"Already " +
					"dead. You've slain me.\"</i> She looks at you with half closed eyes. <i>\"You must be an angel, you're practically glowing.\"</i><p>"
					+ "Suddenly her eyes go wide and she sits bolt upright. " +
					"<i>\"You are glowing! You've got all my mana.\"</i> You look at your hands, which do seem to be faintly glowing and you feel unusually energized. Cassie groans quietly. <i>\"When a " +
					"mage orgasms, she releases some of her mana. You made me cum so much I don't have enough mana left to use my magic. Give it back!\"</i><p>"
					+ "She looks on the verge of tears, but you " +
					"don't actually know how to return her magic energy. She pushes you onto your back and straddles your unsatisfied erection. <i>\"We can fix this,\"</i> she mutters as she guides your " +
					"dick into her. <i>\"If you cum inside me, I'll get nearly all of it back. I just need to make sure I don't orgasm again.\"</i> She swings her hips to ride you.<p>"
					+ "It doesn't take long " +
					"since you've yet to cum even once, but Cassie's already had three orgasms and she looks like she's closing in on her fourth. When you hit your peak and shoot your load into her, " +
					"she bites her lips and braces herself against the pleasure.<p>"
					+ "<i>\"Thanks,\"</i> she whispers in a strained voice. <i>\"I'm powered up again.\"</i> She lifts her hips to get off of you, but the " +
					"sensation of your cock sliding out of her catches her by surprise and she shudders uncontrollably again.<p>"
					+ "<i>\"Goddammit,\"</i> she whines pitifully. <i>\"It's just not fair.\"</i>";
		}
		else{
			return "As Cassie moans and shivers, it's clear she's past the point of no return. <i>\"Please,\"</i> she begs. <i>\"Give me a kiss before I cum.\"</i> You kiss her firmly on the lips and " +
				"rub her clit relentlessly. She shudders and holds you tight as she rides out an intense orgasm. You wait until she comes down before gently disentangling yourself " +
				"from her embrace. <p>"
				+ "<i>\"Thanks. Not that I'm happy about losing, but that felt amazing.\"</i> Cassie smiles " +
				"sheepishly and takes hold of your still-hard cock. <i>\"I'm the one who got you this turned on, right? Then I'm going to take responsibility and finish you off.\"</i> " +
				"You're slightly skeptical of her reasoning, not that you're going to turn down her offer. <p>"
				+ "<i>\"As a girl, it would be a disgrace to get a boy all hot and bothered, " +
				"only to have another girl make him cum.\"</i> She explains. She sets to licking and stroking your dick, showing no less enthusiasm than she did during the fight. " +
				"The delightful sensations from her fingers and tongue soon bring you to a messy climax on her face. You thank her as you collect your clothes and hers, " +
				"leaving her naked, but still in good spirits.";
		}
	}
	public String draw(Combat c,Result flag){
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag==Result.intercourse){
			if(character.has(Trait.witch)&&character.getAffection(opponent)>=12&&opponent.getPure(Attribute.Arcane)>=4&&Global.random(2)==0){
				return "You thrust your hips in time with Cassie's, pushing you both closer to orgasm. At this rate, it seems a draw is pretty much certain. If you pulled out, "
						+ "there's a chance you could change tactics and take the advantage, but right at this moment, it feels like there are more important things than winning.<p> "
						+ "Cassie interlocks her fingers with yours, her eyes filled with desire and pleasure."
						+ "<i>\"Together! Please!\"</i> She manages to gasp between moans. You're quite happy to oblige. You grind against her hips while kissing her deeply. She presses "
						+ "against your body as her tongue eagerly engages yours.<p>"
						+ "As you feel your climax near, you're aware of your mana stirring inside you. A soft glow emanates from Cassie as her own magical energy responds. You can "
						+ "sense it gathering everywhere your skin touches hers, attracted like opposite charges. Your bodies shudder in simultaneous orgasm as the flood gates "
						+ "burst. A cascade of mana surges through you both, threatening to drown you in pleasure. You hold Cassie close as you both weather the storm of sensation.<p>"
						+ "You finally remember to breath again as the reaction dies down."
						+ "<i>\"That felt so good, I thought I was on my way to heaven.\"</i> Cassie rests limply against you, too exhausted to move. You're in no hurry to move. You "
						+ "simply bask in her warmth, filled with exhaustion, satisfaction, and a deep sense of love.<p>"
						+ "It takes you a moment to realize the emotions aren't your own. "
						+ "It's hard to articulate, but you can sense that feelings are flowing into you from Cassie. She looks at you in surprise, clearly feeling the same thing. In "
						+ "fact, you know for sure she's feeling the same.<p>"
						+ "<i>\"You too, right? Ah!\"</i> Realization suddenly dawns on her. <i>\"I think I read about this. We must have been emotionally in synch when the mana loop "
						+ "occurred. Our emotions will keep leaking out as long as we're... connected.\"</i> She casts a meaningful glance downward to where you're still penetrating "
						+ "her.<p>"
						+ "You've apparently stumbled onto the ultimate form of afterglow. Only possible with two magic users under very specific circumstances. You kiss her "
						+ "softly, indulging in the warm feelings she's letting out.<p>"
						+ "You feel a strange giddy nervousness seize your heart as Cassie turns bright red. She smiles sheepishly as she pulls her hips away, breaking the bond.<p>"
						+ "<i>\"Sorry.\"</i> She whispers. <i>\"I'm really happy. Really really happy, but I don't think I can handle you knowing everything I feel for very long.\"</i><p>"
						+ "She snuggles up to you again, her clear blue eyes staring into yours. <i>\"A girl's heart is supposed to be mysterious. I can't reveal all its secrets "
						+ "to a boy. You'll just need to figure out what I'm feeling the old fashion way.\"</i>";
			}else{
			return "You and Cassie move your hips against each other, both rapidly approaching orgasm. As you thrust again and again into her tight folds, you feel yourself pass " +
					"the point of no return. You need to make her cum, now! You kiss her passionately, forcing your tongue into her mouth. The deep kiss combined with your continuous " +
					"thrusting have the desired effect.<p>"
					+ "She embraces you tightly as her climax washes over her. At the same time, you fill her womb with your seed. You didn't quite " +
					"win, but a draw means you both get points, though you both also forfeit your clothes.<p>"
					+ "Right now, you're both too tired to really care about that. You lie on the " +
					"floor trying to regain your strength, still holding each other, still joined below the waist. You probably look more like lovers than opponents now and part of you " +
					"feels the same.<p>"
					+ "However, the match isn't over and you both need to get moving before your other opponents find you. You give Cassie a light kiss on the lips and part " +
					"ways.";
			}
		}
        if(opponent.human()) {
            Global.gui().displayImage("premium/Cassie Petting.jpg", "Art by AimlessArt");
        }
		return "You finger Cassie's wet pussy as she frantically strokes your dick. You're both so close to the end that one of you could cum at any moment. There's no room for " +
				"positioning or strategy, just a simple endurance contest as you hold each other tightly. Cassie leans forward and kisses you passionately. She must be almost done, " +
				"you can feel her body trembling. Unfortunately, you've reached the limit of your endurance.<p>"
				+ "<i>\"Are you going to cum?\"</i> Cassie manages to ask between moans. <i>\"Me too. " +
				"I think we're going to finish together.\"</i> You thrust your fingers as deep into her flower as you can and rub her love bud with your thumb. Her free hand pulls you into " +
				"a tight embrace as the last of your restraint gives out and you cover her stomach with semen.<p>"
				+ "You and Cassie lean against each other, exhausted and sticky. You " +
				"can't help noticing her pleased smile. You thought you felt her climax, but did she actually win?<p>"
				+ "Cassie shakes her head, still smiling. <i>\"We came at the same time. " +
				"I'm just happy we're so closely matched. Every good protagonist needs a good rival to keep pushing her forward. I may be the least suited for this type of competition, " +
				"but I'm going to keep practicing and improving.\"</i> She pulls you close and kisses you again.<p>"
				+ "<i>\"You're going to need to improve too, so you can keep up with me.\"</i> " +
				"She's pretty affectionate for a rival. She's probably better suited to be the protagonist's love interest.<p>"
				+ "<i>\"The protagonist and the rival usually share feelings of " +
				"mutual respect and friendship. There's not reason they couldn't be lovers too. Besides, you're the rival character here. I'm totally the protagonist.\"</i>";
	}
	@Override
	public String bbLiner() {
		if(character.getAffection(Global.getPlayer())>=25){
			return "Cassie looks apologetic and a bit flustered. <i>\"Sorry! Sorry! I love your boy parts, but they're also a really good target.\"</i>";
		}
		switch(Global.random(3)){
		case 1:
			return "Cassie seems a little embarrassed for having to resort to such attacks.  "
					+ "<i>\"T-They said low blows were fair game!\"</i> she stammers.";
		case 2:
			return "Cassie looks away, sheepishly. <i>\"I-I swear I wasn't aiming for your balls!\"</i>  Somehow you doubt that.";
		default: 
			return "Cassie winces apologetically. <i>\"That looks really painful. Sorry, but I can't afford to go easy on you.\"</i>";
		}
	}
	@Override
	public String nakedLiner() {
		return "Cassie blushes noticeably and covers herself. <i>\"No matter how much time I spend naked, it doesn't get any less embarrassing.\"</i>";
	}
	@Override
	public String stunLiner() {
		return "Cassie groans softly as she tends her bruises, <i>\"Come on, you don't have to be so rough.\"</i> she complains.";
	}
	@Override
	public String winningLiner() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String taunt() {
		return "Cassie giggles and taps the head of your dick. <i>\"Your penis is so eager and cooperative,\"</i> she jokes. <i>\"Are you sure you're not just letting me win?\"</i>";
	}
	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude();
	}
	@Override
	public boolean attack(Character opponent) {
		return !character.nude();
	}
	@Override
	public void ding() {
		if(character.getPure(Attribute.Arcane)>=1){
			character.mod(Attribute.Arcane, 1);
			int rand;
			for(int i=0; i<(Global.random(3)/2)+1;i++){
				rand=Global.random(4);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
				else if(rand==3){
					character.mod(Attribute.Arcane, 1);
				}
			}
		}
		else{
			int rand;
			for(int i=0; i<(Global.random(3)/2)+2;i++){
				rand=Global.random(3);
				if(rand==0){
					character.mod(Attribute.Power, 1);
				}
				else if(rand==1){
					character.mod(Attribute.Seduction, 1);
				}
				else if(rand==2){
					character.mod(Attribute.Cunning, 1);
				}
			}
		}
		character.getStamina().gain(4);
		character.getArousal().gain(4);
		character.getMojo().gain(3);
	}
	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Cassie positions herself between your legs, enjoying her unrestricted access to your naked body. She lightly runs her fingers along the length of your " +
					"erection and places a kiss on the tip. <i>\"Don't worry,\"</i> she whispers happily. <i>\"I'm going to make sure you enjoy this.\"</i> She slowly begins licking and " +
					"sucking you penis like a popsicle. You tremble helplessly as she gradually brings you closer and closer to your defeat. A low grunt is the only warning " +
					"you can give of your approaching climax, but Cassie picks up on it. She backs off your dick just far enough to circle her tongue around the sensitive head, " +
					"pushing you over the edge. You shoot your load over her face and glasses as she pumps your shaft with her hand.";
		}
		else{
			if(target.hasDick()){
				return String.format("Cassie kneels between %s's legs and takes hold of %s cock. She gives you a hesitant look. <i>\"This is a bit awkward.\"</i> Is she suddenly "
						+ "reluctant to pleasure a penis? You can attest that she's quite good at it.<p>"
						+ "Cassie's cheeks turn noticeably red. <i>\"Just don't get jealous.\"</i> She starts to stroke the cock, while slowly licking the glans. %s moans in "
						+ "pleasure and bucks %s hips. Cassie's technique has obviously gotten quite good. It only takes a few minutes for her to milk out a mouthful of semen. "
						+ "You can't help feeling a bit envious, maybe you should go a round with her before the match ends.",
						target.name(),target.possessive(false),target.name(),target.possessive(false) );
			}else{
				return "Cassie settles herself in front of "+target.name()+" and tenderly kisses her on the lips. <i>\"I don't really swing this way, but setting the mood is " +
						"important.\"</i> She leans in to lick and suck "+target.name()+" neck, before moving down to her breasts. She gives each nipple attention until "+target.name()+
						" is panting with desire. She continues downward to "+target.name()+"'s pussy and starts eating her out. "+target.name()+" moans loudly and arches her back against " +
						"you. You gently lower her to the floor as she recovers from her climax, while Cassie wipes the juices from her mouth and looks satisfied at her work.";
			}
		}
	}
	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "You grapple with "+assist.name()+", but neither of you can find an opening. She loses her balance while trying to grab you and you manage to trip her. " +
					"Before you can follow up, a warm body presses against your back and a soft hand gently grasps your erection. Cassie whispers playfully in your ear. <i>\"Hello "
					+target.name()+". How about a threesome?\"</i> You start to break away from Cassie, but "+assist.name()+" is already back on her feet. You struggle valiantly, " +
					"but you're quickly overwhelmed by the two groping and grappling girls. Cassie manages to force both your arms under her, leaving you helpless.<br>";
		}
		else{
			return "You wrestle "+target.name()+" to the floor, but she slips away and gets to her feet before you. You roll away to a safe distance before you notice that " +
					"she's not coming after you. She seems more occupied by the hands that have suddenly grabbed her breasts from behind. You cautiously approach and realize " +
					"it's Cassie who is holding onto the flailing "+target.name()+". Releasing her boobs, Cassie starts tickling "+target.name()+" into submission and pins her " +
					"arms while she catches her breath.<br>";
		}
	}
	
	public String watched(Combat c, Character target, Character viewer){
		if (viewer.human()){
			if(character.has(Trait.witch)) {
				if (target.hasDick()) {
					return "As you peek around a corner, you catch sight of " + target.name() + " engaged in combat with Cassie.  Cassie is already naked, but " + target.name() + " has managed to keep her clothes on so far. The fight looks like it's rather heated, with both of them scowling at the other in anger. Given how angry they both are right now, it's probably not the best idea to get involved either way; you're far too likely to end up being the target of one or both of them if you do so. Instead, you decide to stay where you are, watching the fight from  out of sight. This is a good chance to learn about Cassie and " + target.name() + "'s tactics without worrying about being the target of them, after all.<p>"
							+ "Cassie jumps in, grabbing at " + target.name() + "'s clothes as if to strip her, but this turns out to be a feint. As " + target.name() + " reaches for Cassie's hand to try and fend off the stripping attempt, Cassie grabs her wrist. Using her opponent's momentum against her, Cassie pivots and lifts " + target.name() + " up and over her back, throwing her to the ground and causing her to land hard on back and winding her. You wince in sympathy; Cassie can be surprisingly dangerous when she puts her mind to it.<p>"
							+ "Now that her opponent is stunned, you expect Cassie to follow up by stripping her while she can't react, but instead she steps back. She seems to be focusing intently and muttering something, and after a moment she extends her hand toward " + target.name() + "'s prone body. In a flash, " + target.name() + "'s clothes burst into an explosion of flower petals, leaving her naked on the ground.<p>"
							+ "" + target.name() + " is still too winded to stand up, but this doesn't stop her from reaching over to her pile of supplies and grabbing a bottle. Cassie moves in to try to stop her, but she's just a moment too slow. " + target.name() + " pops open the bottle and tosses it Cassie, covering her in what looks like a lubricant. Given that they're both naked now, that lubricant will make it nearly impossible for Cassie to get a firm enough grip to pull off any of her Judo moves again.<p>"
							+ "As the lubricant spreads across her body, Cassie struggles to hold down " + target.name() + ". At last, the tangle is broken when " + target.name() + " gives Cassie a fierce shove and pushes her off and onto her back. " + target.name() + " quickly jumps to her feet, and with an enraged scowl on her face, stomps on Cassie's pussy. Cassie lets out a cry of agony, and even you can feel some sympathy pains for her.<p>"
							+ "As Cassie cradles her crotch, " + target.name() + " rolls her over onto her stomach. While Cassie is too out of it to take advantage of the distraction, " + target.name() + " grabs a nearby set of handcuffs. She pulls Cassie's hands behind her back and cuffs them together, then holds Cassie down with one hand while uses the other to stroke her cock, bringing it up to a full erection.<p>"
							+ "After the stomping she just suffered, you know that a fucking is not going to be pleasant for Cassie. Part of you wants to intervene to save her from this, but that would be denying " + target.name() + " her hard-earned victory and would cause her to bear quite a grudge against you. Sighing, you whisper an apology to Cassie as you leave her to her fate. She did do her fair share to anger " + target.name() + " after all, so it's only fair that she suffer her vengeance.<p>"
							+ "It seems that " + target.name() + " is just a bit more angry than you expected, though. From the angle of her hips, you can tell that she isn't aiming her cock for Cassie's pussy after all, but at her asshole. As she leans in, Cassie seems to realize this as well. She raises her head and lets out a squeal of protest, but it doesn't stop " + target.name() + ". With the lubricant applied earlier easing the way, " + target.name() + " manages to shove her dick into Cassie's asshole, impaling the poor girl on the thick shaft.<p>"
							+ "Cassie's eyes widen and she lets out a squeak as her ass is violated. Deciding that maybe it's worth intervening after all, you quickly look around the area for a good way to approach. You spot another good hiding spot just a few feet from them; if you can get to that, you can wait for the best moment to strike. " + target.name() + " is currently focused on fucking Cassie's ass, so you just have to wait for Cassie to close her eyes or look away before you make your move.<p>"
							+ "You watch Cassie closely, waiting for a chance to get in close without alerting her, lest " + target.name() + " notice the change in Cassie's expression and put up her guard. As you watch though, Cassie's face shifts from a grimace of pain to intense focus and she starts to softly whisper something. Perhaps she's planning something to get her out of this. Even so, you can't take the chance that it won't work, and you use this moment to quickly but quietly sneak towards the pair, hiding just beside them.<p>"
							+ "As you peer out, getting ready to grab " + target.name() + " and pull her away from Cassie, a blinding flash of light bursts from the two of them. You pull your arm up to cover your eyes from the light, though you aren't quite quick enough to block it all out. As you blink your eyes to help them adjust back to the darkness, you try to figure out what just happened.<p>"
							+ "A scream and curse from " + target.name() + " gives you your first clue. She lets out a stream of insults and questions at Cassie in a mix of confusion and anger, the words barely making any sense but their intentions quite clear. You just have to know what happened at this point, so you lean out to get a good view of the two girls.<p>"
							+ "Astoundingly, the two of them have completely swapped positions. Now " + target.name() + " is the one on the ground, handcuffed and with a strap-on stuffed in her ass. <i>\"A short-range teleportation spell,\"</i> Cassie says. There's a tinge of delight in her voice as she speaks. <i>\"Honestly, I can't believe I managed to pull it off under those circumstances, especially with enough finesse to only swap our bodies and not the cuffs. And since you decided that anal is on the menu tonight, I put in a bit of extra effort for your sake to teleport in my strap-on as well so you can experience it as well.\"</i><p>"
							+ "Cassie pulls back, sliding the strap-on out of " + target.name() + "'s asshole. " + target.name() + " lets out a howl of pain from this, and she presses her face to the ground, squeezing her eyes shut. You can sympathize with some of the pain, but it shouldn't be that bad with the lubricant applied earlier. The lubricant. Applied to Cassie. Cassie had teleported in her strap-on, but there was next to no chance she would have been able to teleport in lube for it as well. And if you could guess by the sound of " + target.name() + "'s scream, she was currently enduring an ass fucking dry.<p>"
							+ "<i>\"What's the matter?\"</i> Cassie says. <i>\"You can dish it out, but you can't take it?\"</i><p>"
							+ "She doesn't realize. This is bad. She's paused now, but if she keeps this up, she could end up seriously injuring " + target.name() + "'s ass. You have to do something. Looking around, you spot a nearby bottle of lubricant lying on the ground amid Cassie's supplies. There's no telling how Cassie would respond if you appeared on her from this short distance, so you decide to play this stealthily. You stretch out your leg, using your foot to guide the bottle toward Cassie. You get it rolling in the right direction, and then pull your leg back and out of sight.<p>"
							+ "Cassie jerks as the bottle comes into contact with her foot, looking down to see what had happened. It takes a moment for the realization to dawn on her as she eyes the bottle of lubricant. Her face fills with a blush, and she leans down to pick it up. She clears her throat and says, <i>\"Erm, fine then. Let's make this a bit easier on you.\"</i> She pops open the bottle and spreads the lube out over the strap-on. She then slides it the rest of the way out of " + target.name() + "'s ass and spreads some lube inside it as well.<p>"
							+ "You let out a sigh of relief. " + target.name() + "'s ass was safe - from injury at least, if not from the thorough fucking it was about to receive - and Cassie wouldn't get in trouble for seriously injuring her opponent. Smiling, you let yourself simply enjoy the show as Cassie pounds away at " + target.name() + "'s asshole. After the rough fighting that had been going on, neither of them had started out very aroused, but " + target.name() + " was so out of it now that Cassie has plenty of time to bring her to orgasm.<p>"
							+ "" + target.name() + " seems to have completely given up at this point. She might have been grateful enough for the lube that she'd decided not to fight back, or maybe she was just enjoying herself too much right now to think about resisting. In any case, Cassie was able to bring her to orgasm without any further trouble, needing only to reach around and give " + target.name() + "'s cock a few quick strokes to finish her off.<p>"
							+ "As " + target.name() + " collapses to the ground, Cassie smiles at her and says, <i>\"I think you've been through enough. Don't worry about getting me off in return.\"</i><p>"
							+ "" + target.name() + " mutters her thanks as Cassie pulls out of her ass. Cassie pulls off the strap-on and turns toward her belongings, but you realize a moment too late that she's actually turning toward you instead. Too late to hide, you give Cassie an apologetic grin for spying on her like this.<p>"
							+ "Cassie makes eye contact with you for a moment, then turns her head down and lets an embarrassed smile split across her face. <i>\"Oh my god...\"</i> she whispers. <i>\"I can't believe I forgot about the lube. I'm so glad you were here to remind me.\"</i> Cassie looks back up at you, then leans in and plants a kiss on your cheek. <i>\"Thanks, " + viewer.name() + ". I owe you one.\"</i>";
				} else {
					return "As you peek around a corner, you catch sight of " + target.name() + " engaged in combat with Cassie.  Cassie is already naked, but " + target.name() + " has managed to keep her clothes on so far. The fight looks like it's rather heated, with both of them scowling at the other in anger. Given how angry they both are right now, it's probably not the best idea to get involved either way; you're far too likely to end up being the target of one or both of them if you do so. Instead, you decide to stay where you are, watching the fight from  out of sight. This is a good chance to learn about Cassie and " + target.name() + "'s tactics without worrying about being the target of them, after all.<p>"
							+ "Cassie jumps in, grabbing at " + target.name() + "'s clothes as if to strip her, but this turns out to be a feint. As " + target.name() + " reaches for Cassie's hand to try and fend off the stripping attempt, Cassie grabs her wrist. Using her opponent's momentum against her, Cassie pivots and lifts " + target.name() + " up and over her back, throwing her to the ground and causing her to land hard on back and winding her. You wince in sympathy; Cassie can be surprisingly dangerous when she puts her mind to it.<p>"
							+ "Now that her opponent is stunned, you expect Cassie to follow up by stripping her while she can't react, but instead she steps back. She seems to be focusing intently and muttering something, and after a moment she extends her hand toward " + target.name() + "'s prone body. In a flash, " + target.name() + "'s clothes burst into an explosion of flower petals, leaving her naked on the ground.<p>"
							+ "" + target.name() + " is still too winded to stand up, but this doesn't stop her from reaching over to her pile of supplies and grabbing a bottle. Cassie moves in to try to stop her, but she's just a moment too slow. " + target.name() + " pops open the bottle and tosses it Cassie, covering her in what looks like a lubricant. Given that they're both naked now, that lubricant will make it nearly impossible for Cassie to get a firm enough grip to pull off any of her Judo moves again.<p>"
							+ "As the lubricant spreads across her body, Cassie struggles to hold down " + target.name() + ". At last, the tangle is broken when " + target.name() + " gives Cassie a fierce shove and pushes her off and onto her back. " + target.name() + " quickly jumps to her feet, and with an enraged scowl on her face, stomps on Cassie's pussy. Cassie lets out a cry of agony, and even you can feel some sympathy pains for her.<p>"
							+ "As Cassie cradles her crotch, " + target.name() + " rolls her over onto her stomach. While Cassie is too out of it to take advantage of the distraction, " + target.name() + " grabs a nearby set of handcuffs. She pulls Cassie's hands behind her back and cuffs them together, then holds Cassie down with one hand while she searches through her items with the other. She soon finds what she's looking for, pulling out a strap-on and quickly putting it on.<p>"
							+ "After the stomping she just suffered, you know that a strap-on fucking is not going to be pleasant for Cassie. Part of you wants to intervene to save her from this, but that would be denying " + target.name() + " her hard-earned victory and would cause her to bear quite a grudge against you. Sighing, you whisper an apology to Cassie as you leave her to her fate. She did do her fair share to anger " + target.name() + " after all, so it's only fair that she suffer her vengeance.<p>"
							+ "It seems that " + target.name() + " is just a bit more angry than you expected, though. From the angle of her hips, you can tell that she isn't aiming the strap-on for Cassie's pussy after all, but at her asshole. As she leans in, Cassie seems to realize this as well. She raises her head and lets out a squeal of protest, but it doesn't stop " + target.name() + ". With the lubricant applied earlier easing the way, " + target.name() + " manages to shove her strap-on into Cassie's asshole, impaling the poor girl on the toy.<p>"
							+ "Cassie's eyes widen and she lets out a squeak as her ass is violated. Deciding that maybe it's worth intervening after all, you quickly look around the area for a good way to approach. You spot another good hiding spot just a few feet from them; if you can get to that, you can wait for the best moment to strike. " + target.name() + " is currently focused on fucking Cassie's ass, so you just have to wait for Cassie to close her eyes or look away before you make your move.<p>"
							+ "You watch Cassie closely, waiting for a chance to get in close without alerting her, lest " + target.name() + " notice the change in Cassie's expression and put up her guard. As you watch though, Cassie's face shifts from a grimace of pain to intense focus and she starts to softly whisper something. Perhaps she's planning something to get her out of this. Even so, you can't take the chance that it won't work, and you use this moment to quickly but quietly sneak towards the pair, hiding just beside them.<p>"
							+ "As you peer out, getting ready to grab " + target.name() + " and pull her away from Cassie, a blinding flash of light bursts from the two of them. You pull your arm up to cover your eyes from the light, though you aren't quite quick enough to block it all out. As you blink your eyes to help them adjust back to the darkness, you try to figure out what just happened.<p>"
							+ "A scream and curse from " + target.name() + " gives you your first clue. She lets out a stream of insults and questions at Cassie in a mix of confusion and anger, the words barely making any sense but their intentions quite clear. You just have to know what happened at this point, so you lean out to get a good view of the two girls.<p>"
							+ "Astoundingly, the two of them have completely swapped positions. Now " + target.name() + " is the one on the ground, handcuffed and with a strap-on stuffed in her ass. <i>\"A short-range teleportation spell,\"</i> Cassie says. There's a tinge of delight in her voice as she speaks. <i>\"Honestly, I can't believe I managed to pull it off under those circumstances, especially with enough finesse to only swap our bodies and not the strap-on or cuffs. And since you decided that anal is on the menu tonight, it's only right that I finish you off with a taste of your own medicine.\"</i><p>"
							+ "Cassie pulls back, sliding the strap-on out of " + target.name() + "'s asshole. " + target.name() + " lets out a howl of pain from this, and she presses her face to the ground, squeezing her eyes shut. You can sympathize with some of the pain, but it shouldn't be that bad with the lubricant applied earlier. The lubricant. Applied to Cassie. Cassie had made sure the strap-on wasn't teleported, but had she thought about the lube as well? Keeping that in place would have been nearly impossible, and if you could guess by the sound of " + target.name() + "'s scream, she was currently enduring an ass fucking dry.<p>"
							+ "<i>\"What's the matter?\"</i> Cassie says. <i>\"You can dish it out, but you can't take it?\"</i><p>"
							+ "She doesn't realize. This is bad. She's paused now, but if she keeps this up, she could end up seriously injuring " + target.name() + "'s ass. You have to do something. Looking around, you spot a nearby bottle of lubricant lying on the ground amid Cassie's supplies. There's no telling how Cassie would respond if you appeared on her from this short distance, so you decide to play this stealthily. You stretch out your leg, using your foot to guide the bottle toward Cassie. You get it rolling in the right direction, and then pull your leg back and out of sight.<p>"
							+ "Cassie jerks as the bottle comes into contact with her foot, looking down to see what had happened. It takes a moment for the realization to dawn on her as she eyes the bottle of lubricant. Her face fills with a blush, and she leans down to pick it up. She clears her throat and says, <i>\"Erm, fine then. Let's make this a bit easier on you.\"</i> She pops open the bottle and spreads the lube out over the strap-on. She then slides it the rest of the way out of " + target.name() + "'s ass and spreads some lube inside it as well.<p>"
							+ "You let out a sigh of relief. " + target.name() + "'s ass was safe - from injury at least, if not from the thorough fucking it was about to receive - and Cassie wouldn't get in trouble for seriously injuring her opponent. Smiling, you let yourself simply enjoy the show as Cassie pounds away at " + target.name() + "'s asshole. After the rough fighting that had been going on, neither of them had started out very aroused, but " + target.name() + " was so out of it now that Cassie has plenty of time to bring her to orgasm.<p>"
							+ "" + target.name() + " seems to have completely given up at this point. She might have been grateful enough for the lube that she'd decided not to fight back, or maybe she was just enjoying herself too much right now to think about resisting. In any case, Cassie was able to bring her to orgasm without any further trouble, needing only to reach around and apply a bit of extra stimulation to " + target.name() + "'s clit to get her off.<p>"
							+ "As " + target.name() + " collapses to the ground, Cassie smiles at her and says, <i>\"I think you've been through enough. Don't worry about getting me off in return.\"</i><p>"
							+ "" + target.name() + " mutters her thanks as Cassie pulls out of her ass. Cassie pulls off the strap-on and turns toward her belongings, but you realize a moment too late that she's actually turning toward you instead. Too late to hide, you give Cassie an apologetic grin for spying on her like this.<p>"
							+ "Cassie makes eye contact with you for a moment, then turns her head down and lets an embarrassed smile split across her face. <i>\"Oh my god...\"</i> she whispers. <i>\"I can't believe I forgot about the lube. I'm so glad you were here to remind me.\"</i> Cassie looks back up at you, then leans in and plants a kiss on your cheek. <i>\"Thanks, " + viewer.name() + ". I owe you one.\"</i>";
				}
			}else{
                return "You see Cassie up ahead, but you are far enough behind her that she doesn't notice you.  You quietly follow her a moment while you decide on your plan of attack, but before you can make up your mind, you hear footsteps rapidly approaching, and you see "+target+" jump out of the darkness at Cassie.  Face-to-face, the two of them share a look of mutual acknowledgement – the match has begun.  You aren't one to pass up a free show, so you decide to let them have it out, while you watch safely from the shadows.<p>" +
                        "Cassie begins the bout on a gentle note, using soft, seductive techniques.  She gracefully slips her hands up inside of "+target+"'s top and lightly massages her breasts while tenderly kissing and sucking "+target+"'s lips. "+target+" responds with long caresses up and down Cassie's figure, and her curious fingers soon find their way down the front of Cassie's panties.  The girls' slow, measured movements are almost synchronous as they take their time with one another, both becoming increasingly aroused in the process. <p>" +
                        "The match is heating up; gradually their tops come off, then their bottoms, and eventually they slide each other out of their panties.  Soon the two are stark naked and tangled together, their slippery, sweaty bodies gliding and grinding against each other.  At this point, it appears as though Cassie is getting the better of "+target+", who looks as if she is about to succumb to Cassie's allure; you aren't surprised, Cassie's slow, loving advances are almost impossible to resist.  Standing toe-to-toe, Cassie is gently fingering "+target+" while licking her nipples, and you watch as "+target+"'s eyes lazily close and her knees begin to buckle - it seems like the match is about to be decided…<p>" +
                        ""+target+"'s eyes suddenly burst open, and she appears to be getting a second wind.  With a quick shake of her head, "+target+" snaps herself out of it – she grabs Cassie's hands, steps in close to her, and brings her knee straight upwards, smashing her kneecap cleanly into Cassie's pussy lips.  The sudden aggressiveness catches Cassie completely off-guard; she drops on her knees with a pitiful squeak, looking both hurt and betrayed by the intimate attack.  Crumpling to the ground, Cassie's face is flushed and her eyes are shut tightly to hold back tears, and "+target+" uses the opportunity to compose herself – she takes a deep breath and fixes her hair, evaluating the situation. <p>" +
                        "Unable to stand up, Cassie rolls onto her back with a quiet moan as she brings her knees to her chest and covers her injured privates.  With Cassie incapacitated, "+target+" asserts her control of the match by getting on her knees and straddling Cassie's face, facing her crotch.  "+target+" takes both of Cassie's tender nipples in her fingers and makes slow, gentle circles around them, before pinching them tightly and pulling them in opposite directions.  Cassie squirms helplessly beneath "+target+", unable to protect her vulnerable breasts while still cupping her aching vulva.<p>" +
                        "Cassie is having the fight pummeled out of her, and for a moment you feel like intervening on her behalf.  "+target+" triumphantly raises her fists in the air, looking to bring them down onto Cassie's unprotected breasts, when Cassie suddenly wraps her arms around "+target+"'s waist and begins lapping eagerly at "+target+"'s pussy from below.  Thrown off-balance, "+target+" lurches her hips forward and throws her head back, abandoning her attack to steady herself with her hands.  Cassie's tongue is too skillful for "+target+" to ignore, so she tries unsuccessfully to squirm free of Cassie's grip.<p>" +
                        "Holding fast, Cassie continues her oral attack, and "+target+"'s resistance is gradually wearing down as she unconsciously shifts the weight on her hips, moving her clit closer to Cassie's eager tongue. "+target+" makes one last attempt to break the hold, but it is too late; you see her past the point of no return as she surrenders herself to the approaching orgasm.  "+target+" bends forward and lowers her hips, practically forcing her clit into Cassie' mouth, as she begins to tremble; you finally see "+target+"'s legs tighten around Cassie's head and her back arches involuntarily.  Unable to control herself any longer, "+target+"'s body spasms and squirms as she lets out a loud, protracted moan; "+target+" has lost.  After a few last licks, Cassie lets go of "+target+", but she remains on top of Cassie for a moment, exhausted and unable to move.<p>" +
                        ""+target+" flops off of Cassie, who looks positively worn out herself, and for a quiet moment they lay side-by-side on the ground.  With a look of both hesitant defeat and playful lust, "+target+" gets up and repositions herself between Cassie's legs.  Cassie gives a sheepish smile and parts her legs for "+target+", who begins orally pleasing her.  Even from several yards away, you can hear the wet sounds of licking and kissing as "+target+" enthusiastically works Cassie's little clit.  Cassie reaches down and strokes "+target+"'s hair as she gets closer and closer to orgasm – you see her face contort in a grimace of intense pleasure and her whimpers get progressively louder.  You see Cassie tense up one last time, and she lets out a squeal of pure delight as "+target+" buries her face further into Cassie's wet womanhood, bringing her to an explosive and well-deserved climax.<p>" +
                        "As "+target+" pulls her lips away from Cassie's clit, you see a long, thick trail of sticky saliva and love juice glimmer between the two in the dim light.  They take a minute to catch their breath, and without another word, they collect their clothes and walk back into the night to continue the games.<p>";
            }
		}
		return "";
	}
	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case overflowingmana:
				return "As Cassie approaches you, her magical energy is visible to the naked eye. She seems to glow and crackle with "
						+ "power. She twists the aura around her hands as she grins at you.<p>"
						+ "<i>\"Do you see this? When you made me orgasm last fight, it was like you opened the flood gates. I'm full "
						+ "of Arcane power right now, and I'm going to use all of it to thank you.\"</i>";
			case spirited:
				return "Cassie gives you a bright smile, practically bouncing with eagerness. Did she just finish an energy drink?<p>"
						+ "<i>\"I don't know why, but I've been feeling really good since our last fight. I don't just mean the normal "
						+ "way you make me feel good. I feel like I could keep going all night.\"</i>";
			case determined:
				return "Cassie gives you a pleasant smile, but her eyes show firm resolve. <i>\"I may have lost last time, but don't think "
						+ "I'm going to just give up. I'll keep going until I give you an orgasm.\"</i><p>"
						+ "Her determination is strong. You probably won't be able to keep her down easily.";
			case modestlydressed:
				if(character.nude()){
					return "Cassie pouts as she tries to cover her nakedness. <i>\"Shit. After last fight, I had a plan to try to keep "
							+ "my clothes on. Instead, I'm naked before the fight even starts. It's not fair!\"</i>";
				}else{
					return "Cassie grins and does a little spin to show off. <i>\"What do you think of my clothes?\"</i> Her outfit is cute, "
							+ "but not much different than usual. If anything, it appears to fit more snugly.<br>"
							+ "<i>\"Exactly. I paid special attention to making sure all my clothes fit perfectly, so they'll be harder to "
							+ "take off. If I can stay dressed, I should have a much better chance of winning.\"</i>";
				}
			default:
				break;
			}
		}
		if(character.nude()){
			return "Cassie does her best to cover her naked body as her cheeks turn pink. <i>\"We haven't even started yet and I'm already naked. Go easy "
					+ "on me, OK?\"</i>";
		}
		if(opponent.pantsless()){
			return "Cassie giggles cutely as her eyes drift over your naked body. <i>\"Is it my birthday? I don't usually run into hot naked boys when I'm "
					+ "walking around the campus.\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Cassie gives you a warm smile as she prepares to do sexy battle. <i>\"Fighting you is always the highlight of my night, win or lose. "
					+ "I'm still going to do my best to win though.\"</i>";
		}
		if(character.has(Trait.witch)){
			return "Cassie extends a hand and a soft light radiates from her palm. <i>\"Do you like my magic? I'm still learning, but I bet it can do "
					+ "some cool things.\"</i>";
		}
		return "Cassie looks hesitant for just a moment, but can't contain a curious little smile as she prepares to face you.";
	}
	@Override
	public boolean fit() {
		return !character.nude()&&character.getStamina().percent()>=50&&character.getArousal().percent()<=50;
	}
	@Override
	public String night() {
		Global.gui().loadPortrait(Global.getPlayer(), this.character);
		return "After the match, you stick around for a few minutes chatting with your fellow competitors. You haven't seen Cassie yet, but you at least want to say goodnight to her.<p>" +
				"You feel a warm hand grasp yours and find Cassie standing next to you, smiling shyly. She doesn't say anything, but that smile communicates her intentions quite well. " +
				"You bid the other girls goodnight and lead Cassie back to your room.<p>"
				+ "The two of you quickly undress each other while sharing brief kisses. You lay down on the bed and " +
				"she straddles you, guiding your dick into her pussy. She lets out a soft, contented noise as you fill her. Without moving her hips, she lays against your chest and kisses " +
				"you romantically. You embrace her and start thrusting your hips against her. She matches her movements to your slow, but pleasurable strokes.<p>"
				+ "You both take your time, " +
				"more interested in feeling each other's closeness than in reaching orgasm, but gradually you both feel your pleasure building.<p>"
				+ "Cassie buries her face in your chest, letting " +
				"out hot, breathy moans. You run your hands through her hair and softly stroke her back and the nape of her neck. It's hard to tell whether her orgasm set off your ejaculation " +
				"or the other way around, but you release your load into her shuddering pussy.<p>"
				+ "Neither of you make any movement to separate from each other. Remaining inside her until morning " +
				"sounds quite nice.<p>"
				+ "<i>\"I love you.\"</i> The whisper was so soft you're not sure you heard it. When you look at Cassie's face, she's fast asleep.";
	}
	public void advance(int rank){
		if(rank >= 2 && !character.has(Trait.hiddenpotential)){
			character.add(Trait.hiddenpotential);
		}
		if(rank >= 1 && !character.has(Trait.witch)){
			character.add(Trait.witch);
			character.outfit[0].removeAllElements();
			character.outfit[1].removeAllElements();
			character.outfit[0].add(Clothing.bra);
			character.outfit[0].add(Clothing.Tshirt);
			character.outfit[0].add(Clothing.cloak);
			character.outfit[1].add(Clothing.panties);
			character.outfit[1].add(Clothing.skirt);
			character.closet.add(Clothing.cloak);
			character.clearSpriteImages();
			character.mod(Attribute.Arcane,1);
		}
	}
	@Override
	public NPC getCharacter() {
		return character;
	}
	@Override
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case nervous:
			return value>=30;
		case angry:
			return value>=80;
		default:
			return value>=50;
		}
	}
	@Override
	public String image() {
		return "assets/cassie_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case nervous:
			return 1.2f;
		case angry:
			return .7f;
		default:
			return 1f;
		}
	}
	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Oh, yes! Give it all to me! Please!\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Ah! I... Can't... Lose... Yet!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Ah! Do you feel good? You don't have to hold back, just let it out!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Ah, ah ah! Please...\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"You are a dirty boy, aren't you? I might be a bit dirty as well for doing this...\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Not my butt! Don't make me cum from my butt!\"</i>");
		comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"I think I really like this position. How about you?\"</i>");
		comments.put(CommentSituation.OTHER_ENTHRALLED, "<i>\"Oh, I really shouldn't do this... but it's really exciting!\"</i>");
		comments.put(CommentSituation.OTHER_BOUND, "<i>\"If you just sit still like that, I'll make sure you enjoy it...\"</i>");
		comments.put(CommentSituation.SELF_BOUND, "<i>\"This isn't fair! I want to make you feel good too!\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"Am I turning you on? Hehe. I'm glad.\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"I'm turning into such a horny girl... I want you so much!\"</i>");
		comments.put(CommentSituation.SELF_CHARMED, "<i>\"You want to do something nice to me? Ok, just for a bit.\"</i>");
		comments.put(CommentSituation.OTHER_OILED, "<i>\"With your dick so slippery, I bet I can make you feel great!\"</i>");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Do you like a girl on top? I know I'm having fun.\"</i>");
		comments.put(CommentSituation.SELF_SHAMED, "<i>\"Don't tease me so much! I'm so embarrassed I could die!\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Cassie covers her groin and turns her face away from you, flushing with equal parts pain and embarrassment.");
		return comments;
	}
	
	@Override
	public int getCostumeSet() {
		if(character.has(Trait.witch)){
			return 2;
		}else{
			return 1;
		}
	}
	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(opponent.getGrudge()==Trait.spirited && character.has(Trait.witch)){
			character.addGrudge(opponent,Trait.overflowingmana);
		}else if(c.eval(character)==Result.intercourse) {
			character.addGrudge(opponent,Trait.spirited);
		}else{
			switch(Global.random(2)){
			case 0:
				character.addGrudge(opponent,Trait.determined);
				break;
			case 1:
				character.addGrudge(opponent,Trait.modestlydressed);
				break;
			default:
				break;
			}
		}
		
	}
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        if(character.has(Trait.witch)){
            character.outfit[0].add(Clothing.bra);
            character.outfit[0].add(Clothing.Tshirt);
            character.outfit[0].add(Clothing.cloak);
            character.outfit[1].add(Clothing.panties);
            character.outfit[1].add(Clothing.skirt);
        }else{
            character.outfit[0].add(Clothing.bra);
            character.outfit[0].add(Clothing.blouse);
            character.outfit[1].add(Clothing.panties);
            character.outfit[1].add(Clothing.skirt);
        }

    }
}
