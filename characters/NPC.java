package characters;
import global.*;

import items.Clothing;
import items.Component;
import items.Consumable;
import items.Flask;
import items.Item;

import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import Comments.CommentGroup;
import Comments.CommentSituation;
import skills.Skill;
import skills.Tactics;
import stance.Behind;
import status.Enthralled;
import status.Feral;
import status.Horny;
import status.Status;
import status.Stsflag;
import trap.Trap;

import actions.Action;
import actions.Leap;
import actions.Move;
import actions.Movement;
import actions.Shortcut;
import actions.Wait;
import areas.Area;

import combat.Combat;
import combat.Encounter;
import combat.Result;
import daytime.Daytime;


public class NPC extends Character {
	private Personality ai;
	public Emotion plan;
	public Area goal;
	public HashMap<Emotion, Integer> strategy;
	private CommentGroup comments;

	public NPC(String name, ID identity, int level,Personality ai){
		super(name,level);
		this.identity = identity;
		this.ai=ai;	
		strategy = new HashMap<Emotion, Integer>();
		strategy.put(Emotion.hunting, 3);
		strategy.put(Emotion.bored, 3);
		strategy.put(Emotion.sneaking, 3);
	}
	@Override
	public String describe(int per) {
		String description=ai.describe();
		for(Status s:status){
			if(s.describe()!=null&&s.describe()!=""){
				description = description+"<br>"+s.describe();
			}
		}
		description = description+"<p>";
		if(top.empty()&&bottom.empty()){
			description = description+"She is completely naked.<br>";
		}
		else{
			if(top.empty()){
				description = description+"She is topless and ";
				if(!bottom.empty()){
					description=description+"wearing ";
				}
			}
			else{
				description = description+"She is wearing "+top.peek().pre()+top.peek().getName()+" and ";
			}
			if(bottom.empty()){
				description = description+"is naked from the waist down.<br>";
			}
			else{
				description = description+bottom.peek().pre()+bottom.peek().getName()+".<br>";
			}
		}
		description = description+observe(per);
		return description;
	}
	private String observe(int per){
		String visible = "";
		for(Status s: status){
			if(s.flags().contains(Stsflag.unreadable)){
				return visible;
			}
		}
		if(per>=9){
			visible = visible+"Her arousal is at "+arousal.percent()+"%<br>";
		}
		if(per>=8){
			visible = visible+"Her stamina is at "+stamina.percent()+"%<br>";
		}
		if(per>=7 && per<9){
			if(arousal.percent()>=75){
				visible = visible+"She's dripping with arousal and breathing heavily. She's at least 3/4 of the way to orgasm<br>";
			}
			else if(arousal.percent()>=50){
				visible = visible+"She's showing signs of arousal. She's at least halfway to orgasm<br>";
			}
			else if(arousal.percent()>=25){
				visible = visible+"She's starting to look noticeably aroused, maybe a quarter of her limit<br>";
			}
		}
		if(per>=6 && per<8){
			if(stamina.percent()<=33){
				visible = visible+"She looks a bit unsteady on her feet<br>";
			}
			else if(stamina.percent()<=66){
				visible = visible+"She's starting to look tired<br>";
			}
		}
		if(per>=3 && per<7){
			if(arousal.percent()>=50){
				visible = visible+"She's showing clear sign of arousal. You're definitely getting to her.<br>";
			}
		}
		if(per>=4 && per<6){
			if(stamina.percent()<=50){
				visible = visible+"She looks pretty tired<br>";
			}
		}
		if(per>=5){
			visible = visible+mood.describe()+"<br>";
		}
		return visible;
	}
	
	@Override
	public void victory(Combat c, Result flag) {
		c.write(ai.victory(c, flag));
		Character target;
		target = c.getOther(this);
		this.gainXP(15+lvlBonus(target));
		target.gainXP(15+target.lvlBonus(this));
		if(c.stance.penetration(this)){
			getMojo().gain(2);
			if(has(Trait.mojoMaster)){
				getMojo().gain(2);
			}
		}
		target.arousal.empty();
		undress(c);
		target.undress(c);
		if(c.underwearIntact(target)){
			this.gain(target.getUnderwear());
		}
		dress(c);
		target.defeated(this);
		Roster.gainAttraction(this.id(),target.id(),2);
		if(target.has(Trait.gracefulloser)){
            Roster.gainAttraction(this.id(),target.id(),2);
        }
		plan = rethink();
		if(has(Trait.insatiable)){
			arousal.set(Math.max(Math.round(arousal.max()*.5f),arousal.get()));
		}
	}

	@Override
	public void defeat(Combat c, Result flag) {
		c.write(ai.defeat(c,flag));
		arousal.empty();
		plan = rethink();
	}
	public boolean resist3p(Combat combat, Character intruder, Character assist){
		if(has(Trait.cursed)){
			if(intruder.human()||assist.human()){
				Global.gui().message(ai.resist3p(combat, intruder, assist));
			}
			return true;
		}
		else{
			return false;
		}
	}
	public void intervene3p(Combat c,Character target, Character assist){
		this.gainXP(10+lvlBonus(target));
		target.defeated(this);
		c.write(ai.intervene3p(c, target,assist));
		assist.gainAttraction(this, 1);
	}
	public void victory3p(Combat c,Character target, Character assist){
		this.gainXP(15+lvlBonus(target));
		target.gainXP(15+target.lvlBonus(this)+target.lvlBonus(assist));
		target.arousal.empty();
		dress(c);
		target.undress(c);
		
		if(c.underwearIntact(target)){
			this.gain(target.getUnderwear());
		}
		target.defeated(this);
		c.write(ai.victory3p(c,target,assist));
		gainAttraction(target,1);
	}
	public void watcher(Combat c,Character victor, Character defeated){
		if(has(Trait.voyeurism)){
			buildMojo(50);
		}
		gainAttraction(victor,3);
	}
	public String watched(Combat c,Character voyeur, Character defeated){
		return ai.watched(c, defeated, voyeur);
	}
	@Override
	public void act(Combat c) {
		HashSet<Skill> available = new HashSet<Skill>();
		Character target;
		if(c.p1==this){
			target=c.p2;
		}
		else{
			target=c.p1;
		}
		for(Skill act:skills){
			if(act.usable(c, target)){
				available.add(act);
			}
		}
		for(Skill act:flaskskills){
			if(act.usable(c, target)){
				available.add(act);
			}
		}
		c.act(this, ai.act(available,c));
	}
	public Skill actFast(Combat c) {
		HashSet<Skill> available = new HashSet<Skill>();
		Character target;
		if(c.p1==this){
			target=c.p2;
		}
		else{
			target=c.p1;
		}
		for(Skill act:skills){
			if(act.usable(c, target)){
				available.add(act);
			}
		}
		return ai.act(available,c);
	}
	@Override
	public boolean human() {
		return false;
	}
	@Override
	public void draw(Combat c, Result flag) {
		Character target;
		if(c.p1==this){
			target=c.p2;
		}
		else{
			target=c.p1;
		}
		this.gainXP(15+lvlBonus(target));
		target.gainXP(15+target.lvlBonus(this));
		this.arousal.empty();
		target.arousal.empty();
		if(this.has(Trait.insatiable)){
			this.arousal.restore((int) (arousal.max()*.2));
		}
		if(target.has(Trait.insatiable)){
			target.arousal.restore((int) (arousal.max()*.2));
		}
		target.undress(c);
		this.undress(c);
		if(c.underwearIntact(this)){
			target.gain(this.getUnderwear());
		}
		if(c.underwearIntact(target)){
			this.gain(target.getUnderwear());
		}
		target.defeated(this);
		this.defeated(target);
		c.write(ai.draw(c,flag));
        Roster.gainAttraction(this.id(),target.id(),4);
		if(getAffection(target)>0){
			Roster.gainAffection(this.id(),target.id(),1);
			if(this.has(Trait.affectionate)||target.has(Trait.affectionate)){
                Roster.gainAffection(this.id(),target.id(),1);
			}
		}
		plan = rethink();
	}
	@Override
	public String bbLiner() {
		return ai.bbLiner();
	}
	@Override
	public String nakedLiner() {
		return ai.nakedLiner();
	}
	@Override
	public String stunLiner() {
		return ai.stunLiner();
	}
	@Override
	public String winningLiner() {
		return ai.winningLiner();
	}
	@Override
	public String taunt() {
		return ai.taunt();
	}
	@Override
	public void detect() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void move(Match match) {
		if(state==State.combat){
			if(location.fight == null){
				state = State.ready;
				move(match);
			}else{
				location.fight.battle();
			}
		}
		else if(busy>0){
			busy--;
		}
		else if (this.is(Stsflag.enthralled)) {
			Character master;
			master = ((Enthralled)getStatus(Stsflag.enthralled)).master;
			Move compelled = findPath(master.location);
			if (compelled != null){
				compelled.execute(this);
				return;
			}
		}
		else if(state==State.shower||state==State.lostclothes){
			bathe();
		}
		else if(state==State.crafting){
			craft();
		}
		else if(state==State.searching){
			search();
		}
		else if(state==State.resupplying){
			resupply();
		}
		else if(state==State.webbed){
			state=State.ready;
		}
		else if(state==State.masturbating){
			masturbate();
		}
		else{
			if(!location.encounter(this)){
				HashSet<Action> available = new HashSet<Action>();
				HashSet<Movement> radar = new HashSet<Movement>();
				for(Area path:location.adjacent){
					available.add(new Move(path));
					if(path.ping(get(Attribute.Perception))){
						radar.add(path.id());
					}
				}
				if(getPure(Attribute.Cunning)>=28){
					for(Area path:location.shortcut){
						available.add(new Shortcut(path));
					}
				}
				if(getPure(Attribute.Ninjutsu)>=5){
					for(Area path:location.jump){
						available.add(new Leap(path));
					}
				}
				for(Action act:Global.getActions()){
					if(act.usable(this)){
						available.add(act);
					}
				}
				if(location.humanPresent()){
					Global.gui().message("You notice "+name()+parseMoves(available,radar,match).execute(this).describe());
				}
				else{
					parseMoves(available,radar,match).execute(this);
				}
			}
		}
	}
	@Override
	public void faceOff(Character opponent, Encounter enc) {
		if(has(Consumable.smoke)&&!ai.fightFlight(opponent)){
			consume(Consumable.smoke,1);
			enc.fightOrFlight(this, ai.fightFlight(opponent),true);
		}else{
			enc.fightOrFlight(this, ai.fightFlight(opponent),false);
		}
	}
	@Override
	public void spy(Character opponent, Encounter enc) {
		if(ai.attack(opponent)){
			enc.ambush(this, opponent);
		}
		else{
			location.endEncounter();
		}
	}
	@Override
	public void ding(){
		xp-=95+(level*5);
		if(xp<0){
			xp=0;
		}
		level++;
		ai.ding();
		if(countFeats()<level/4){
			ai.pickFeat();
		}
		if(has(Trait.expertGoogler)){
			getArousal().gain(1);
		}
		if(has(Trait.fitnessNut)){
			getStamina().gain(1);
		}
		Global.gainSkills(this);
	}
	@Override
	public void rankup(){
		this.rank++;
		ai.advance(rank);
	}
	@Override
	public void showerScene(Character target, Encounter encounter) {
		if(this.has(Flask.Aphrodisiac)){
			encounter.aphrodisiactrick(this, target);
		}
		else if(!target.nude()&&Global.random(3)>=2){
			encounter.steal(this, target);
		}
		else{
			encounter.showerambush(this, target);
		}
	}
	@Override
	public void intervene(Encounter enc, Character p1, Character p2) {
		if(Global.random(5)==0){
			enc.watch(this);
		}
		if(Global.random(20)+getAffection(p1) + (p1.has(Trait.sympathetic)?10:0)>=Global.random(20)+getAffection(p2)+ (p2.has(Trait.sympathetic)?10:0)){
			enc.intrude(this, p1);
		}
		else{
			enc.intrude(this, p2);
		}
	}

	@Override
	public void load(Scanner loader) {
		level=Integer.parseInt(loader.next());
		setRank(Integer.parseInt(loader.next()));
		xp=Integer.parseInt(loader.next());
		money=Integer.parseInt(loader.next());
		String e = loader.next();
		while(!e.equals("@")){
			set(Attribute.valueOf(e),Integer.parseInt(loader.next()));
			e = loader.next();
		}
		stamina.setMax(Integer.parseInt(loader.next()));
		arousal.setMax(Integer.parseInt(loader.next()));
		mojo.setMax(Integer.parseInt(loader.next()));
		e = loader.next();
		while(!e.equals("*")){
			Roster.gainAffection(id(),ID.fromString(e),Integer.parseInt(loader.next()));
			e = loader.next();
		}
		e = loader.next();
		while(!e.equals("*")){
			Roster.gainAttraction(id(),ID.fromString(e),Integer.parseInt(loader.next()));
			e = loader.next();
		}
		e = loader.next();
		outfit[0].clear();
		while(!e.equals("!")){
			outfit[0].add(Clothing.valueOf(e));
			e = loader.next();
		}
		e=loader.next();
		outfit[1].clear();
		while(!e.equals("!")&&!e.equals("!!")){
			outfit[1].add(Clothing.valueOf(e));
			e = loader.next();
		}
		if(e.equals("!!")){
			e=loader.next();
			closet.clear();
			while(!e.equals("!!!")){
				closet.add(Clothing.valueOf(e));
				e = loader.next();
			}
		}
		e=loader.next();
		while(!e.equals("@")){
			traits.add(Trait.valueOf(e));
			e = loader.next();
		}
		e=loader.next();
		while(!e.equals("$")){
			inventory.add(Global.getItem(e));
			e = loader.next();
		}
		change(Modifier.normal);
		resetSkills();
		Global.gainSkills(this);
	}
	@Override
	public String challenge(Character opponent) {
		if(grudges.containsKey(opponent)){
			activeGrudge = grudges.get(opponent);
		}
		return ai.startBattle(opponent);
	}
	@Override
	public void promptTrap(Encounter enc,Character target,Trap trap) {
		if(ai.attack(target)){
			enc.trap(this, target,trap);
		}
		else{
			location.endEncounter();
		}
	}
	@Override
	public void afterParty() {
		Global.gui().message(ai.night());
	}
	public void daytime(int time, Daytime day){
		ai.rest(time,day);
	}
	@Override
	public void counterattack(Character target, Tactics type, Combat c) {
		switch(type){
		case damage:
			c.write(name()+" avoids your clumsy attack and swings her fist into your nuts.");
			target.pain(4+Global.random(get(Attribute.Cunning)),Anatomy.genitals,c);
			break;
		case pleasure:
			if(target.pantsless()){
				c.write(name()+" catches you by the penis and rubs your sensitive glans.");
			}
			else{
				c.write(name()+" catches you as you approach and grinds her knee into the tent in your "+target.bottom.peek());
			}
			target.pleasure(4+Global.random(get(Attribute.Cunning)),Anatomy.genitals,c);
			break;
		case positioning:
			c.write(name()+" outmaneuvers you and catches you from behind when you stumble.");
			c.stance=new Behind(this,target);
			break;
		default:
		}
	}
	public Skill prioritize(ArrayList<HashSet<Skill>> plist){
		if(plist.isEmpty()){
			return null;
		}
		ArrayList<HashSet<Skill>> wlist = new ArrayList<HashSet<Skill>>();
		for(int i=0;i<plist.size();i++){
			if(!plist.get(i).isEmpty()){
				for(int j=0;j<plist.size()-i;j++){
					wlist.add(plist.get(i));
				}
			}	
		}
		if(wlist.isEmpty()){
			return null;
		}
		HashSet<Skill> cat = wlist.get(Global.random(wlist.size()));
		if(cat.isEmpty()){
			return null;
		}
		else{
			return cat.toArray(new Skill[cat.size()])[Global.random(cat.size())];
		}
	}
	public ArrayList<HashSet<Skill>> parseSkills(HashSet<Skill> available, Combat c){
		Character target = c.getOther(this);
		HashSet<Skill> damage = new HashSet<Skill>();
		HashSet<Skill> pleasure = new HashSet<Skill>();
		HashSet<Skill> fucking = new HashSet<Skill>();
		HashSet<Skill> position = new HashSet<Skill>();
		HashSet<Skill> debuff = new HashSet<Skill>();
		HashSet<Skill> recovery = new HashSet<Skill>();
		HashSet<Skill> calming = new HashSet<Skill>();
		HashSet<Skill> summoning = new HashSet<Skill>();
		HashSet<Skill> stripping = new HashSet<Skill>();
		ArrayList<HashSet<Skill>> priority = new ArrayList<HashSet<Skill>>();
		for(Skill a:available){
			if(a.type()==Tactics.damage){
				damage.add(a);
			}
			else if(a.type()==Tactics.pleasure){
				pleasure.add(a);
			}
			else if(a.type()==Tactics.fucking){
				fucking.add(a);
			}
			else if(a.type()==Tactics.positioning){
				position.add(a);
			}
			else if(a.type()==Tactics.status){
				debuff.add(a);
			}
			else if(a.type()==Tactics.recovery){
				recovery.add(a);
			}
			else if(a.type()==Tactics.calming){
				calming.add(a);
			}
			else if(a.type()==Tactics.summoning||a.type()==Tactics.preparation){
				summoning.add(a);
			}
			else if(a.type()==Tactics.stripping){
				stripping.add(a);
			}
		}
		switch(this.mood){
		case confident:
			priority.add(pleasure);
			priority.add(fucking);
			priority.add(position);
			priority.add(stripping);
			priority.add(summoning);
			if(!target.stunned()){
				priority.add(damage);
			}
			priority.add(debuff);
			break;
		case angry:
			if(!target.stunned()){
				priority.add(damage);
			}
			priority.add(debuff);
			priority.add(position);
			priority.add(stripping);
			priority.add(fucking);
			break;
		case nervous:
			priority.add(position);
			priority.add(calming);
			priority.add(summoning);
			priority.add(recovery);
			priority.add(pleasure);
			if(!target.stunned()){
				priority.add(damage);
			}
			break;
		case desperate:
			priority.add(calming);
			priority.add(recovery);
			priority.add(position);
			if(!target.stunned()){
				priority.add(damage);
			}
			priority.add(pleasure);
			break;
		case horny:
			priority.add(fucking);
			priority.add(stripping);
			priority.add(pleasure);
			priority.add(position);
			break;
		case dominant:
			priority.add(fucking);
			priority.add(stripping);
			priority.add(pleasure);
			priority.add(debuff);
			priority.add(summoning);
			priority.add(position);
			if(!target.stunned()){
				priority.add(damage);
			}
			break;
		}
		return priority;
	}
	public Action parseMoves(HashSet<Action> available, HashSet<Movement> radar, Match match){
		HashSet<Action> enemy = new HashSet<Action>();
		HashSet<Action> safe = new HashSet<Action>();
		HashSet<Action> highpri = new HashSet<Action>();
		HashSet<Action> lowpri = new HashSet<Action>();
		if(location==goal){
			plan = rethink();
		}
		if(nude()){
			for(Action act: available){
				if(act.consider()==Movement.resupply){
					return act;
				}
				if(goal==null){
					if(act.getClass()==Move.class){
						Move movement = (Move)act;
						if(movement.consider()==Movement.union&&!radar.contains(Movement.union)||movement.consider()==Movement.dorm&&!radar.contains(Movement.dorm)){
							goal = movement.getDestination();
						}
					}
				}
			}
			if(goal==null){
				if(Global.random(2)==1){
					goal = match.gps(Movement.dorm);
				}else{
					goal = match.gps(Movement.union);
				}
			}
		}
		if(getArousal().percent()>=40&&!location().humanPresent()&&radar.isEmpty()){
			for(Action act: available){
				if(act.consider()==Movement.masturbate){
					return act;
				}
			}
		}
		if(getStamina().percent()<=60||getArousal().percent()>=30){
			for(Action act: available){
				if(act.consider()==Movement.bathe){
					return act;
				}
				if(goal==null){
					if(act.getClass()==Move.class){
						Move movement = (Move)act;
						if(movement.consider()==Movement.pool||movement.consider()==Movement.shower){
							goal = movement.getDestination();
						}
					}
				}
			}
			if(goal==null){
				if(Global.random(2)==1){
					goal = match.gps(Movement.shower);
				}else{
					goal = match.gps(Movement.pool);
				}
			}
		}
		if(get(Attribute.Science)>=1&&!has(Component.Battery,10)){
			for(Action act: available){
				if(act.consider()==Movement.recharge){
					return act;
				}
			}
			if(goal==null){
				goal = match.gps(Movement.workshop);
			}
		}
		if(goal==null){
			if(plan==Emotion.hunting){
				switch(Global.random(5)){
				case 4:
					goal = match.gps(Movement.shower);
					break;
				case 3:
					goal = match.gps(Movement.union);
					break;
				case 2:
					goal = match.gps(Movement.la);
					break;
				case 1:
					goal = match.gps(Movement.dining);
					break;
				default:
					goal = match.gps(Movement.workshop);
					break;
				}
			}
			else if(plan==Emotion.sneaking){
				switch(Global.random(5)){
				case 4:
					goal = match.gps(Movement.storage);
					break;
				case 3:
					goal = match.gps(Movement.bridge);
					break;
				case 2:
					goal = match.gps(Movement.tunnel);
					break;
				case 1:
					goal = match.gps(Movement.workshop);
					break;
				default:
					goal = match.gps(Movement.lab);
					break;
				}
			}
			else if(plan==Emotion.bored){
				switch(Global.random(5)){
				case 4:
					goal = match.gps(Movement.storage);
					break;
				case 3:
					goal = match.gps(Movement.kitchen);
					break;
				case 2:
					goal = match.gps(Movement.shower);
					break;
				case 1:
					goal = match.gps(Movement.workshop);
					break;
				default:
					goal = match.gps(Movement.lab);
					break;
				}
			}
			else if(plan==Emotion.retreating){
				if(Global.random(2)==1){
					goal = match.gps(Movement.dorm);
				}else{
					goal = match.gps(Movement.union);
				}
			}
		}
		highpri.add(findPath(goal));
		for(Action act: available){
			if(radar.contains(act.consider())){
				enemy.add(act);
			}else if(act.consider()==Movement.quad||act.consider()==Movement.kitchen||act.consider()==Movement.dorm||act.consider()==Movement.shower||act.consider()==Movement.storage||
					act.consider()==Movement.dining||act.consider()==Movement.laundry||act.consider()==Movement.tunnel||act.consider()==Movement.bridge||act.consider()==Movement.engineering||
					act.consider()==Movement.workshop||act.consider()==Movement.lab||act.consider()==Movement.la||act.consider()==Movement.library||act.consider()==Movement.pool||
					act.consider()==Movement.union){
				safe.add(act);
			}
			
			else if(plan==Emotion.hunting){
				if(act.consider()==Movement.mana||act.consider()==Movement.recharge||act.consider()==Movement.locating||act.consider()==Movement.potion||act.consider()==Movement.trap||act.consider()==Movement.energydrink){
					lowpri.add(act);
				}
			}
			else if(plan==Emotion.sneaking){
				if(location.hasTrap(this)){
					highpri.clear();
					if(act.consider()==Movement.hide||act.consider()==Movement.wait){
						highpri.add(act);
					}
				}else{
					if(act.consider()==Movement.hide||act.consider()==Movement.scavenge||act.consider()==Movement.trap||act.consider()==Movement.recharge){
						highpri.add(act);
					}
					else if(act.consider()==Movement.mana||act.consider()==Movement.potion||act.consider()==Movement.bathe||act.consider()==Movement.energydrink){
						lowpri.add(act);
					}
				}
			}
			else if(plan==Emotion.bored){
				if(act.consider()==Movement.mana||act.consider()==Movement.recharge||act.consider()==Movement.scavenge||act.consider()==Movement.craft){
					highpri.add(act);
				}
				else if(act.consider()==Movement.trap||act.consider()==Movement.potion||act.consider()==Movement.bathe||act.consider()==Movement.energydrink||act.consider()==Movement.beer||
						act.consider()==Movement.oil){
					lowpri.add(act);
				}
			}
			else if(plan==Emotion.retreating){
				if(act.consider()==Movement.energydrink){
					highpri.add(act);
				}
			}
		}
		if((plan == Emotion.hunting||plan == Emotion.bored)&&!enemy.isEmpty()){
			highpri.addAll(enemy);
		}
		else if(plan == Emotion.retreating){
			highpri.removeAll(enemy);
			lowpri.addAll(safe);
		}
		else if(plan == Emotion.sneaking&&!enemy.isEmpty()){
			lowpri.addAll(enemy);
		}

		if(highpri.isEmpty()||Global.random(4)==0){
			highpri.addAll(lowpri);
		}
		if(highpri.isEmpty()){
			highpri.addAll(available);
		}
		Action[] actions = highpri.toArray(new Action[highpri.size()]);
		Action chosen=actions[Global.random(actions.length)];
		if(chosen==null){
			chosen = new Wait();
		}
		return chosen;
	}
	
	public Emotion moodSwing(){
		/*Emotion current = mood;
		for(Emotion e: emotes.keySet()){
			if(ai.checkMood(e, emotes.get(e))){
				emotes.put(e, 0);
				mood=e;
				return e;
			}
		}
		return current;*/
		Emotion current = mood;
		float max=emotes.get(current);
		for(Emotion e: emotes.keySet()){
			if(emotes.get(e)*ai.moodWeight(e)>max){
				mood=e;
				max=emotes.get(e);
			}
		}
		return mood;
	}

    @Override
    public void resetOutfit() {
        ai.resetOutfit();
    }

    public Emotion rethink(){
		goal = null;
		Emotion current = plan;
		if(!ai.fit()){
			return Emotion.retreating;
		}
		int total = 5;
		total+=strategy.get(Emotion.hunting);
		total+=strategy.get(Emotion.sneaking);
		total+=strategy.get(Emotion.bored);
		int result = Global.random(total);
		if(result>=5+strategy.get(Emotion.hunting)+strategy.get(Emotion.bored)){
			current = Emotion.bored;
		}
		else if(result>=5+strategy.get(Emotion.hunting)){
			current = Emotion.sneaking;
		}
		else if(result>=5){
			current = Emotion.hunting;
		}
		return current;
	}
	@Override
	public void eot(Combat c, Character opponent, Skill last) {
		ArrayList<Status> copy = new ArrayList<Status>();
		copy.addAll(status);
		for(Status s:copy){
			s.turn(c);
		}
        //dropStatus();
		if(opponent.pet!=null&&canAct()&&c.stance.mobile(this)&&!c.stance.prone(this)){
			if(get(Attribute.Speed)>opponent.pet.ac()*Global.random(20)){
				opponent.pet.caught(c,this);
			}
		}
		if(opponent.has(Trait.pheromones)&&opponent.getArousal().percent()>=50&&!is(Stsflag.horny)&&Global.random(5)==0){
			c.write("You see "+name()+" swoon slightly as she gets close to you. Seems like she's starting to feel the effects of your musk.");
			add(new Horny(this,2+2*opponent.getSkimpiness(),3),c);
		}
		if(opponent.has(Trait.tailmastery)&&opponent.has(Trait.tailed)&&!opponent.distracted()&&!opponent.stunned()&&pantsless()){
			c.write("You tease "+name()+"'s sensitive ares with your fluffy tail.");
			pleasure(Global.random(5),Anatomy.genitals,c);
		}
		if(has(Trait.RawSexuality)){
			tempt(1);
			opponent.tempt(1);
		}
		if(c.stance.dom(this)){
			emote(Emotion.dominant,15);
			emote(Emotion.confident,5);
		}
		else if(c.stance.sub(this)){
			emote(Emotion.nervous,15);
			emote(Emotion.desperate,10);
		}
		if(opponent.nude()){
			emote(Emotion.horny,5);
			emote(Emotion.confident,5);
			emote(Emotion.dominant,10);
		}
		if(nude()){
			emote(Emotion.nervous,10);
			emote(Emotion.desperate,10);
			if(has(Trait.exhibitionist)){
				emote(Emotion.horny,20);
			}
		}
		if(opponent.getArousal().percent()>=75){
			emote(Emotion.dominant,15);
			emote(Emotion.confident,5);
		}
		if(getArousal().percent()>=75){
			emote(Emotion.desperate,15);
			emote(Emotion.nervous,10);
		}
		if(getArousal().percent()>=90){
			emote(Emotion.desperate,25);
		}
		if(!canAct()){
			emote(Emotion.desperate,10);
		}
		if(!opponent.canAct()){
			emote(Emotion.dominant,20);
		}
		if((getPure(Attribute.Animism)>=4&&getArousal().percent()>=50)||has(Trait.feral)||has(Trait.furaffinity) && getArousal().percent()>=25){
			if(!is(Stsflag.feral)){
				add(new Feral(this));
			}
		}	
		moodSwing();
	}
	public void visit(int time){
		int max = 0;
		int visits = time;
		Character bff = null;
		for(Character friend: Scheduler.getAvailable(LocalTime.of(3,0))){
			if(getAttraction(friend)>max&&!friend.human()){
				max = getAttraction(friend);
				bff = friend;
			}
		}
		if(bff!=null){
			bff.gainAffection(this,1);
			switch(Global.random(3)){
			case 0:
				Daytime.train(this, bff, Attribute.Power);
			case 1:
				Daytime.train(this, bff, Attribute.Cunning);
			default:
				Daytime.train(this, bff, Attribute.Seduction);
			}
			visits -= 1;
		}
		while(visits>0){
			bff = getRandomFriend();
			if(bff!=null){
				bff.gainAffection(this,1);
				switch(Global.random(3)){
				case 0:
					Daytime.train(this, bff, Attribute.Power);
				case 1:
					Daytime.train(this, bff, Attribute.Cunning);
				default:
					Daytime.train(this, bff, Attribute.Seduction);
				}

			}
            visits -= 1;
		}
	}
	public NPC clone() throws CloneNotSupportedException {
	    return (NPC) super.clone();
	}
	private float rateMove(Skill skill, Combat c, float urg, float fit1, float fit2) {
	      // Clone ourselves a new combat... This should clone our characters, too
		Combat c2;
		try {
			c2 = c.clone();
		} catch(CloneNotSupportedException e) {
			return 0;
		}
		// Now do it!
		if (c.p1 == this) {
			skill.setSelf(c2.p1);
			skill.resolve(c2, c2.p2);
			skill.setSelf(c.p1);
       } else {
    	   skill.setSelf(c2.p2);
    	   skill.resolve(c2, c2.p1);
    	   skill.setSelf(c.p2);
      }
		// How close is the fight to finishing?
       float urgency = Math.min(c2.p1.getUrgency(), c2.p2.getUrgency());
       float dfit1 = c2.p1.getFitness(urgency, c2.stance) - fit1;
       float dfit2 = c2.p2.getFitness(urgency, c2.stance) - fit2;
       return (c.p1 == this ? dfit1 - dfit2 : dfit2 - dfit1);
   }

	public Skill prioritizeNew(ArrayList<HashSet<Skill>> plist, Combat c)
	{
		if (plist.isEmpty()) {
			return null;
	    }
	    // The higher, the better the AI will plan for "rare" events better
	    final int RUN_COUNT = 5;
	    // Decrease to get an "easier" AI. Make negative to get a suicidal AI.
	    final float RATING_FACTOR = 0.1f;
	    // Increase to take input weights more into consideration
	    final float INPUT_WEIGHT = 1.0f;

	    // Starting fitness
	    float urgency0 = Math.min(c.p1.getUrgency(), c.p2.getUrgency());
	    float fit1 = c.p1.getFitness(urgency0, c.stance);
	    float fit2 = c.p2.getFitness(urgency0, c.stance);
	    // Now simulate the result of all actions
	    NavigableMap<Float, Skill> ratingMap = new TreeMap<Float, Skill>();
	    float sum = 0; float weight = 0;
	    for (HashSet<Skill> skills : plist) {
	       for (Skill skill : skills) {
	    	   // Run it a couple of times
               float rating = 0;
               for (int j = 0; j < RUN_COUNT; j++) {
            	   rating += rateMove(skill, c, urgency0, fit1, fit2);
               }
               // Sum up rating, add to map
               rating = (float) Math.exp(RATING_FACTOR * rating - weight);
               sum += rating;
               ratingMap.put(sum, skill);
	       }
	       if (!skills.isEmpty()) weight += INPUT_WEIGHT;
	    }
	    c.clearImages();
	    if (sum == 0) return null;
	    // Debug
	    if (Global.debug) {
	       String s = "AI choices: ";
	       float last = 0;
	       for (Map.Entry<Float,Skill> entry : ratingMap.entrySet()) {
	               float d = entry.getKey() - last;
	               last = entry.getKey();
	               s += entry.getValue().toString() +
	                               "(" + String.format("%.1f", d / sum * 100) + "%) ";
	       }
	       System.out.println(s);
	    }
	    // Select
	    float s = ((float)Global.random((int) (sum * 1024))) / 1024;
	    return ratingMap.ceilingEntry(s).getValue();
	}
	
	@Override
	public String getPortrait() {
		return ai.image();
	}
	
	protected int getCostumeSet(){
		return ai.getCostumeSet();
	}
	public String getComment(Combat c) {
		return CommentSituation.getBestComment(ai.getComments(), c, this, c.getOther(this));
	}
	
}
