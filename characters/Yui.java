package characters;

import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Item;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentGroup;
import Comments.CommentSituation;
import skills.Skill;
import skills.Tactics;
import actions.Action;
import actions.Movement;

import combat.Combat;
import combat.Result;
import status.Stsflag;

public class Yui implements Personality {
	public NPC character;
	public Yui(){
		character = new NPC("Yui",ID.YUI,1,this);
		character.outfit[0].add(Clothing.kunoichitop);
		character.outfit[1].add(Clothing.panties);
		character.outfit[1].add(Clothing.ninjapants);
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.kunoichitop);
		character.closet.add(Clothing.halfcloak);
		character.closet.add(Clothing.panties);
		character.closet.add(Clothing.ninjapants);
		character.change(Modifier.normal);
		character.setUnderwear(Trophy.YuiTrophy);
		character.add(Trait.female);
		character.add(Trait.shy);
		character.plan = Emotion.sneaking;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.sneaking, 5);
		character.strategy.put(Emotion.bored, 2);
		this.character.set(Attribute.Cunning, 7);
		this.character.set(Attribute.Ninjutsu, 5);
		this.character.set(Attribute.Speed, 7);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
		for(Skill a:available){
			if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Fertility Rite")){
				mandatory.add(a);
			}
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }

		}
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public NPC getCharacter() {
		return character;
	}

	@Override
	public void rest(int time, Daytime day) {
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Dojo");
		available.add("Play Video Games");
		available.add("Workshop");
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		if(!(character.has(Toy.Dildo)||character.has(Toy.Dildo2))&&character.money>=250){
			character.gain(Toy.Dildo);
			character.money-=250;
		}
		if(!(character.has(Toy.Crop)||character.has(Toy.Crop2))&&character.money>=200){
			character.gain(Toy.Crop);
			character.money-=200;
		}
		for(int i=0;i<time-1;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
		}
		character.visit(1);
	}

	@Override
	public String bbLiner() {
		switch(Global.random(2)){
		case 1:
			return "<i>\"Master, the groin makes a better target than the eyes, throat, and solar plexus combined.  You should improve your ability to protect your considerably vulnerable testicles.  I will do everything I can to help you practice,\"</i> she says with a quick bow.";
		default:
			return "Yui gives a quick bow of apology. <i>\"Sorry Master. I was trained to always target my opponent's weakest point.\"</i>";
		}

	}

	@Override
	public String nakedLiner() {
		return "A deep blush colors Yui's cheeks, but she doesn't cover herself. <i>\"It's fine,\"</i> she whispers to herself unconvincingly. "
				+ "<i>\"A kunoichi doesn't get embarrassed, especially not in front of her master.\"</i>";
	}

	@Override
	public String stunLiner() {
		return "Yui lets out a quiet grunt of exertion as she tries to get back to her feet. <i>\"That was a good hit. I wasn't quite fast enough to dodge it.\"</i>";
	}

	@Override
	public String winningLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String taunt() {
		return "<i>\"Master, I thought you were better than this. Are you letting me win? Ah! You must be pent up. I'll help you cum immediately.\"</i>";
	}

	@Override
	public String victory(Combat c, Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag == Result.intercourse){
			return "You have stopped Yui's furious offensive by getting her on the ground underneath you.  "
					+ "You are leaning on her hands, pinning them to the ground by her head as you penetrate her.  "
					+ "You know that she loves it when you have sex with her, and are hoping that will bring a "
					+ "quick end to the bout.<p>"
					+ "<i>\"Oh Master,\"</i> she moans. <i>\"I love the feeling of you deep inside me.  "
					+ "I will make sure your loss is just as enjoyable.\"</i>  You look at her confused, don't you "
					+ "have the superior position.  She watches you for a minute like she is expecting you to realize something, "
					+ "but it doesn't come to you.  Finally a look of deep concentration crosses her face and you feel her "
					+ "vaginal muscles begin to pulse and shift around your shaft.  The rhythm picks up and the feeling is "
					+ "far more intense then when you have penetrated the other girls, it almost feels like she is sucking "
					+ "you off instead of you screwing her.<p>"
					+ "<i>\"Have I mentioned . . . Kunoichi's third . . . hidden art?\"</i> she says slowly, clearly concentrating.  "
					+ "You try to think back to previous encounters with her and the moves she has mentioned. You start to "
					+ "throb inside her as she continues to concentrate on moving her muscles around your invading member.  "
					+ "She smiles as you begin to squirm, realizing the mistake you made.  She wraps her legs around you as "
					+ "you try to pull back and out, trapping you inside her hot, wet pussy as she milks your cock.  "
					+ "You are getting close as you scramble to find a way to try and fight back.  You let go of her hands "
					+ "and grab her breasts, trying to either stimulate her faster than she is doing to you, or break her concentration.  "
					+ "But now that her hands are free she is able to bat yours away without much thought at all.<p>"
					+ "She starts breathing harder and is clearly concentrating all of her energy, you assume most of it is going "
					+ "to pleasuring you, but you are hoping that at least some of it is being used to prevent her own orgasm.  "
					+ "After a couple more attempts at grabbing her breasts which she successfully blocks, you change your "
					+ "target and try to reach down between her legs, hoping to get to her clit.  As you get close though her "
					+ "hands make three rapid movements and you find that you are frozen.  You try to move but are unable "
					+ "to make any part of your body obey you.<p>"
					+ "<i>\"Master . . . \"</i> she says between breaths, <i>\"Just enjoy . . . it will be . . . over soon.\"</i>  "
					+ "You feel her feet dig into your lower back as she pulls your forward, fully inside her.   "
					+ "The end is inevitable now as you feel her muscles contract and release around you, seeming to "
					+ "focus mostly on your sensitive head.  You let out a sudden strangled gasp as your balls contract "
					+ "and you release your seed inside of her.  You shoot load after load inside her as her muscles "
					+ "begin to release your cock.<p>  "
					+ "Finally you are spent, but you are still unable to move.  Yui unwraps herself from around you and pulls back, letting your cock pop out.  For some reason you are still completely hard despite having cum.<p>"
					+ "<i>\"Hmm . . . I'm sorry Master,\"</i> she says, <i>\"it seems I hastily cast my paralysis jutsu and it was too powerful.\"</i> She walks around you as she explains.  <i>\"It should have worn off by now.  Even this is stuck solid.\"</i> She taps your erection as she says this.  <i>\"Master, while I have you like this I would like to try something.\"</i>  Without waiting for an answer that you can't give, she gets down on her knees in front of you, facing away.  She leans down and presents her ass and pussy to you, then slowly moves backwards while carefully lining your cock up with her slit.<p>"
					+ "<i>\"It's much harder to master Kunoichi's third hidden art from this angle,\"</i> she says.  <i>\"But I want to become a better ninja for you.\"</i>  She impales herself on your entire length and you feel her muscles begin to move around your cock again.  You try to protest as you realize you are super sensitive in your artificially hard state, but all you can get out is a low grunt.   Quickly she is panting away, and her muscle movements are far more erratic then before.  <p>"
					+ "<i>\"The problem is . . . the feelings are more intense . . . so it is harder to concentrate,\"</i> she pants.  You can see she is working hard not to move any muscles other than the ones inside her pussy, and as she continues she begins to slide back and forth along your length.  Finally you see her give up and she starts just sliding back and forth along your length.  As she does, you begin to notice little shudders occurring across your own body, radiating out from your crotch.  In almost no time she lets out some little groans and you can feel her cumming around you, and you suddenly begin to shake as you feel a sudden shot of pleasure, almost like an orgasm but without the same feeling of satisfaction.  You suddenly fall backwards as your muscles unlock and find yourself laying on the ground, your cold, wet penis quickly deflating.<p>  "
					+ "<i>\"Another failure,\"</i> Yui says dejectedly as she stands up and looks at you.  <i>\"I will get better for you Master, I promise.\"</i>  She picks up her clothes and moves into the shadows as you try to pick yourself up.";
		}else if(c.lastact(character)!=null && c.lastact(character).toString().startsWith("Bunshin")){
			return "Yui has simultaneously worn you out and worked you up; it has been incredibly tiring to manage her speed and focus, and her efforts have made you rock hard and on the brink of an explosive orgasm. She is exceptionally fast, and she works with a swift efficiency as she tickles and licks your most sensitive parts. <p>" +
                    "Yui abruptly pauses her offensive and takes three quick steps backward. Concentrating intensely, she performs a series of gestures in the air in front of her, and she splits into five copies of herself.  All five figures advance on you simultaneously, and you quickly lose track of which one was the 'real' Yui as they come at you from all sides. <p>" +
                    "You try your best to maintain your composure in the swarm, but each copy moves with Yui's deftness, and it is extremely difficult to keep up.  You do not know which clone to focus your efforts on, and there are too many of them for you to handle at once.  Within the swarm, one of the clones grabs your right arm, another clone grabs your left arm, and two of them take each of your legs.  You are soon pinned to the ground in a spread-eagle position, with the fifth clone standing over you. <p>" +
                    "The clones holding your arms begin to lick and kiss your mouth and neck on either side, and the two holding your legs take turns gently massaging your balls.  The fifth clone kneels between your legs and takes your dick in her hand, carefully positioning your eager phallus upright as she bends over to reach it with her mouth. <p>" +
                    "You can feel the warm breath of the clone between your legs as she opens her mouth wide to accommodate your throbbing member.  She strokes up and down your shaft with both hands while making wet circles around the head of your penis with her tongue.  She moves the circles further and further down until the head of your penis is completely engulfed within her mouth.  She slowly works her way to the base of your shaft, and your dick is beginning to twitch with anticipation – you are now past the point of no return, and you feel your balls begin to lurch under the gentle fondling of the clones holding your legs. <p>" +
                    "The clone between your legs only needs a couple of these long, slow slurps before you explode.  Your muscles contract and your instincts tell you to push her head down and force yourself as deeply into her throat as possible, but thankfully there is no need; the clone grabs your butt and pulls it towards her, tightly inhaling your entire length as you shoot a copious load down her throat.  You ejaculate over and over, and your entire body shudders and flexes with each squirt against the team of clones that struggle to contain your spasms.<p>" +
                    "You take a long, deep breath, and the clones holding your arms and legs vanish in a wisp of smoke.  The only figure remaining is Yui herself, who is still kneeling between your legs, wiping her mouth and bowing graciously.  <i>\"Master, even in victory, I learn from you.  Thank you for the lesson.\"</i>  She stands up, gives one more short bow, and disappears out of sight.<p>";
		}
		return "You're getting dangerously aroused in your fight against Yui. You need to take a more aggressive tactic before she finishes you off. "
				+ "You grab the smaller girl and force her onto her back, your dominant hand firmly on her groin. She gasps softly as your fingers "
				+ "explore her sensitive parts.<p>"
				+ "<i>\"Ah! Master, your fingers are heavenly, but I'm afraid you've fallen into my trap.\"</i> She locks her legs over your shoulder, "
				+ "pinning your arm in place. Before you can free yourself, another Yui jumps on you from behind and pins your free arm. She prepared "
				+ "a shadow clone in advance to ambush you!?<p>"
				+ "<i>\"I'd love to let you keep touching me, but I need your arms immobilized so you can't protect your groin.\"</i> You don't quite understand "
				+ "her plan until you feel another shadow clone's fingers on your scrotum. You can just barely hear her muttering. <i>\"...Jin, Retsu, Zai, Zen...\"</i><p>"
				+ "The shadow clones vanish as soon as the incantation is finished, and you immediately start to feel an urgent heat building in your testicles. "
				+ "With a hand free, you have little trouble breaking Yui's hold and putting her in a more secure pin. She doesn't even resist. Unfortunately, "
				+ "as the heat continues to build in your groin, it creates an almost overwhelming need to ejaculate. The warmth flows from your balls to the base "
				+ "of your cock and you struggle to hold it back. You slide two fingers into Yui pussy and try to make her cum as fast as possible.<p>"
				+ "<i>\"Master, I know firsthand how much pleasure your fingers can bring, but even you can't make me orgasm fast enough to win. Even if I don't "
				+ "touch you, you will ejaculate very soon.\"</i> You know she's telling the truth. You're resisting with all your willpower, but precum is leaking "
				+ "liberally from your penis. <i>\"You should just relax and let me service you. It will feel much more satisfying.\"</i><p>"
				+ "You don't answer her. It takes all of your concentration to just hold back your climax and keep moving your fingers aimlessly. Yui can't reach "
				+ "your groin with her hands, but she starts rubbing your dick with the soft soles of her feet. You could try grabbing her legs or changing position to "
				+ "stop her, but you can't muster the energy. You <b>need</b> to cum right now, and her footjob feels amazing.<p>"
				+ "Yui holds your face and pulls you into a passionate kiss as your resistance breaks down. Her dexterous feet milk your straining cock until you feel "
				+ "a powerful surge of pleasure shoot through your body. Your dick spasms repeatedly as it paints the floor with your semen. The flood of pleasure and "
				+ "relief leaves you completely spent, and you collapse onto her bosom, exhausted.<p>"
				+ "<i>\"...Master? Master...?\"</i> Yui quietly calls out to you, but you're too tired to respond. <i>\"You're asleep...? I'll wake you up in a little "
				+ "while.\"</i> You aren't actually unconscious, but you don't feel like correcting her at the moment. You feel her hand wrap around the base of your "
				+ "penis, inexplicably still erect despite your orgasm. <i>\"Still hard? It must be because you resisted for so long. You built up too much for a single "
				+ "ejaculation.\"</i><p>"
				+ "She moves a little beneath you and starts to rub your sensitive member against her wet slit. <i>\"It's ok if I'm a little naughty, "
				+ "right? I'll be done before you wake up.\"</i> Her voice is thick with arousal. <i>\"It doesn't vibrate, but the real thing feels better than any of "
				+ "my toys.\"</i><p>"
				+ "Yui vigorously frots against the length of your dick, pleasuring herself without penetration. The stimulation is intense for you too. Despite having "
				+ "just cum, you're filled with the need to ejaculate again. Combined with your exhaustion, you feel powerless to do anything but moan quietly as Yui humps "
				+ "you. Soon, she shudders in orgasm and your overstimulated dick shoots another load of jizz on her abdomen.<p>"
				+ "Yui diligently cleans you both up as you finally muster the effort to sit up. <i>\"Master, you're awake! Your orgasm was so intense you passed out. "
				+ "I've been watching over you to make sure none of the other competitors take advantage.\"</i> You gently stroke her head. It's probably best not to "
				+ "bring up the fact that you were awake for what just happened.<p>"
				+ "<i>\"Since you lost the fight, you'll have to proceed naked for now. As you are both exhausted and nude, you seem very vulnerable. Shall I escort you "
				+ "for now?\"</i> You decline as politely as you can. Her words are supportive enough, but there's a hunger in her eyes that makes you uncomfortable. "
				+ "Yui means well, but you're starting to realize she may be the most dangerous opponent to lose to.";
	}

	@Override
	public String defeat(Combat c, Result flag) {	
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(flag == Result.intercourse){
			return "You pull Yui in to your body, feeling her breasts push up against your chest. You lean in and kiss her passionately, "
				+ "separating your lips to let your tongue play over hers. Her body trembles underneath your fingertips as you "
				+ "trace the curves of her torso, slowly working your way down to her ass. <p>"
				+ "You give it a slap which pushes her now wet thighs around the length of your cock. She moans as your length rubs along "
				+ "the lips of her slick pussy. You manage two or three thrusts before you feel her knees give out from under her, and you "
				+ "realise it is only your strength that is holding her upright. <p>"
				+ "<i>\"Master...\"</i> she moans as you lean forwards, lowering her to the ground. Her intense physical training obviously "
				+ "didn't cover this eventuality, and she offers no resistance as you spread her legs - she even reaches down and starts to "
				+ "rub her clit. <p>"
				+ "You push your head teasingly against her lips and she verily writhes with pleasure, squirming against the floor in an "
				+ "attempt to shuffle towards you and on to your cock. After a short moment of teasing you give her what you want and slip "
				+ "inside her easily, despite your size. <p>"
				+ "<i>\"Ah, master!...\"</i> she cries, her back arching as she bucks against you.  You've barely touched her and yet the "
				+ "girl seems on the brink of orgasm. <i>\"My training never.... Never prepared me for....\"</i> she trails off into a melodic "
				+ "series of moans and you can feel her pussy clench around your length as her entire body convulses in pleasure. <p>"
				+ "She may be lost in a world of delight but you're nowhere near done, and you remind her of that by playfully slapping one "
				+ "of her breasts. She yelps in surprise before grabbing at your arm and pulling you deep inside her. <p>"
				+ "<i>\"I'm... I'm cumming again!\"</i> she whimpers, grinding and bucking her hips against your pelvis. She lets out a little "
				+ "yelp each time you thrust into her, and she throws her arms to the ground, pushing herself up against you as her pussy "
				+ "tightens around you yet again. <p>"
				+ "As her pleasure subsides she scrambles off you, turning over and onto her knees. One hand dives furiously between her legs, "
				+ "pumping her clit, whilst the other reaches out for you in a frantic attempt to pull you back in. <p>"
				+ "You willingly oblige, and grab her hips firmly, digging your fingertips into her sweat covered skin, ensuring your grip. "
				+ "She moans wildly, her free hand flying around wildly as she tries to grasp hold of something to solidify her trembling body. <p>"
				+ "You hold her steady with your hands as you thrust deeply into her pussy. With each pump she whimpers with pleasure and there "
				+ "is a little 'schlick' noise emitted from between her legs. Her fingers dance over her clit and her entire body weight pushes "
				+ "her face into the floor with each movement of your hips, but it does nothing to muffle her cries of pleasure which can be "
				+ "heard in every corner of the building. <p>"
				+ "<i>\"Ah, master.. Please, please cum deep inside me!\"</i> she cries, and despite your best efforts to hold out for longer "
				+ "you are swiftly pushed over the edge. <p>"
				+ "Thrusting as deeply as you can you unload stream after stream of thick cum into her pussy. She bucks back against you once, "
				+ "twice, and on the third time she cums again, spraying your thighs with a mix of your own cum and her squirt. <p>"
				+ "You bask in a few moments of post-fuck bliss before pulling out of Yui's still shivering body. As soon as you let go she "
				+ "collapses to the floor in a sweaty, sticky heap of pleasure.";
		}else if(!character.topless() && !character.pantsless()){
		    return "This is why Yui calls you <i>\"master.\"</i><p>" +
                    "Yui is ridiculously fast, clever as a fox, and tenacious as hell – but she’s still a teenage girl, after all, and you have her right where you want her.  Her cheeks are flushed, her nipples erect, and you can feel her strength slowly wearing away at this point in the match.  The two of you are facing each other, several feet apart, searching for an opening in the other’s defense.<p>" +
                    "Yui claps her hands together in front of her, and appears to be gathering her energy.  You move towards her to break her concentration, but when you get within striking distance, she unleashes a series of lightning-fast attacks in a frantic effort to turn the match in her favor.  Yui darts at you and thrusts her open palm towards your solar plexus.  You easily anticipate the attack and step to the side, and your unexpected movement makes Yui stumble forward.  She hastily regains her stance, and this time kicks her foot up between your legs; again, you are too quick, and you take a short step back while Yui ineffectually kicks the air in front of you.  Looking desperate, Yui makes a third attack, crouching down and sweeping her leg in a wide arc, trying to take your legs out from under you, and you see your opportunity:  You jump over Yui’s sweep, and come down right on top of her as you land. <p>" +
                    "You use your falling momentum to land with your hands on her shoulders, pushing her back onto the ground, facing up.  You try to land with your legs between hers, but Yui partially stops you, so you land with one of your knees between Yui’s legs, while Yui has a knee between yours.  The two of you both jerk your knees at the other’s crotch, but neither of you have enough leverage to land an effective strike.  Instead, you change your tactic, and you begin firmly grinding the top of your knee into Yui’s pussy.  Though she still has her pants on, you can feel the fabric effortlessly gliding across Yui’s anatomy, it is clear that underneath her pants, Yui is soaking wet.<p>" +
                    "Yui lets out a short gasp as you pin her with your hands, and she tries to close her legs as you maintain your position.  You grind your knee deeper into her crotch, and, even through her pants, you can feel her engorged clit on your thigh.  You lean forward to kiss her, and she eagerly kisses you back; you can feel her slowly abating her struggle, and she is soon offering no resistance to your hands pinning hers.  <i>\"Oh, master…\"</i> Yui moans softly into your ear.<p>" +
                    "You hasten your grinding and Yui responds accordingly, breathing faster and faster.  Since she has stopped struggling, you release her hands so that you can focus on massaging her breasts; her moans intensify as you tease and fondle her perky little tits.  As you expected, instead of launching a counterattack, she uses her newly-freed hands to brace herself against her quickly approaching orgasm.  Her body contracts forwards, her hips begin to buck back and forth, and she shuts her eyes tightly as she breaks her mouth free from yours.  <i>\"Master…master…I…I can’t...I…master…aaaaahhhhh!\"</i>  Her body quakes beneath yours, and you struggle to maintain your position against her orgasmic tremors.  With one final exhalation, Yui falls backwards onto the floor, limp and spent.<p>" +
                    "You get up to leave, but Yui grabs your hand.  She lazily opens her eyes, and says <i>\"Your turn, master.\"</i>  You admit, grinding Yui into a quivering puddle was more than a little arousing, and your raging erection could use some release, so you put up no resistance as Yui guides you onto your back and kneels between your legs.  Her tongue is dripping with saliva as she takes your member into her mouth and gobbles your entire length.  She gently plays with your balls with one hand, and maintains a firm stroke at the base of your shaft with the other.  Yui’s blowjobs are as you would expect; efficient, masterful, and incredibly effective.  It only takes her a few strokes before you feel yourself about to blow; Yui can sense it too, and sucks you all the way to the back of her throat as you finally explode a warm, copious load down her esophagus - and she swallows every last drop.<p>" +
                    "Yui takes you out of her mouth, and licks your slowly softening shaft from the base to the tip.  She stands up and gives a dutiful bow.  <i>\"Thank you for the lesson, master.  I eagerly anticipate the next one.\"</i>  She picks her clothes up, and swiftly exits back into the night.  You take a few more deep breaths, yourself, before getting up and returning to the games.<p>";
        }
		return "Yui trembles with pleasure as your experienced fingers work on her sensitive pussy. You kiss her on the lips and she embraces you desperately, "
		+ "too overwhelmed by passion to fight back. She's on the brink of orgasm, but you want to play with her a bit more, so you slow your fingering "
		+ "to light, teasing touches.<p>"
		+ "<i>\"Master! That feels so good, but please let me cum!\"</i> You tickle her clit lightly, making her hips jump. She's supposed to be "
		+ "a trained kunoichi, but she's acting as needy as a horny virgin. Shouldn't she be better as resisting pleasure than this?<p>"
		+ "Yui lets out a plaintive moan at your teasing. <i>\"That's not fair, Master! My traini-AH! My training relies on detaching pleasure "
		+ "from emotion. It doesn-AH! It won't help me against you, Master!\"</i> She's so cute that you just have to give her another kiss. As "
		+ "your tongue slips into her mouth, you feel her body start to convulse in orgasm. You're barely touching her, but apparently the kiss was "
		+ "enough to set her off. You finger her in earnest to maximize her climax and she screams into your mouth.<p>"
		+ "You break the kiss as her orgasm subsides, leaving her in a blissful daze. </i>\"Master... That was wonderful... I...\"</i> She regains her "
		+ "focus as her eyes land on your erection. She firmly pushes you onto your back and settles between your legs. <i>\"Allow me to "
		+ "pleasure you, too.\"</i><p>"
		+ "Without waiting for your response, Yui starts sucking your dick passionately. You groan quietly and gently stroke her head as you enjoy her "
		+ "earnest effort. Her tongue and lips are extremely skilled, and you're soon brought to the brink of orgasm without any of her weird ninja techniques. "
		+ "She takes as much of your shaft as she can manage and sucks strongly. Your hips buck as you shoot your load into her mouth. She continues sucking "
		+ "until your ejaculation finishes, swallowing every drop of your seed.<p>"
		+ "She wipes her mouth after she finishes and looks at you hopefully. <i>\"Master, did my feelings reach you? Your kiss touched me so deeply, "
		+ "I overflowed with joy. I wanted to share it with you as much as I could.\"</i> You pull the naked girl into a tight embrace and kiss her "
		+ "softly on the forehead. Her devotion and affection definitely reached you.";
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Yui stands over you and looks over your naked, restrained body excitedly. <i>\"I'm sorry Master, but this is how the game works, right?\"</i> "
					+ "She tries to sound calm, but you can tell she's practically drooling with anticipation. She straddles your hips and positions her slick "
					+ "entrance above your cock. <i>\"I'll finish you with the utmost care and pleasure.\"</i><p>"
					+ "Yui slowly lowers her hips to take your length inside her. You both let out a moan in unison as she starts to move her hips rhythmically. This "
					+ "seems like a risky strategy when she's as turned on as you are, but you know she has some tricks up her sleeve. Her vaginal walls suddenly contract "
					+ "around your shaft and your hips buck involuntarily. <i>\"How do you like the Ishida Kunoichi's third hidden art?\"</i> Her voice is slightly strained, "
					+ "but hot with desire. <i>\"It takes a lot of concentration, but if you can't resist, I can make you cum quickly.\"</i><p>"
					+ "Her insides continue to contract and relax in time with her hip movements, rapidly milking you to orgasm. You shoot your load inside her and feel "
					+ "her orgasm about 20 seconds later.";
		}
		else{
			if(target.hasDick()){
				return String.format("Yui stares at %s's exposed penis with intense curiosity. <i>\"It's very different than master's. I guess dicks really "
						+ "do come in all shapes and sizes...\"</i> She grabs the shaft with both hands and begins a practiced handjob. <i>\"But they all cum, just the same.\"</i><br>"
						+ "She flashes you an expectant grin. Her pun only earns a shrug, but you give her a smile for the effort. She seems sufficiently "
						+ "encouraged anyway. <i>\"Of course, your penis is still my favorite, Master. If you see any techniques you want me to use on you after "
						+ "the match, just let me know. Oh! Like this one.\"</i> She pulls a black cloth from her outfit and wraps it around %s's cock. <i>\"It's pure silk, "
						+ "very pleasant on sensitive skin.\"</i> She rapidly moves the cloth back and forth, like she's shining a shoe. %s lets out a passionate "
						+ "moan at the sensation. In no time at all, %s covers the cloth in hot cum.<br>"
						+ "Yui gives you another exceptant look. This time she gets a thumbs up for execution.",
						target.name(),target.name(),target.pronounSubject(false));
			}
			else{
				return String.format("Yui looks down at %s with a dominance you aren't used to seeing from her. It's slightly comical since her fight left her nude. "
						+ "<i>\"You were a worthy opponent for me alone, but you were no match for the unbreakable trust between me and my Master.\"</i> "
						+ "She kneels confidently between the helpless girl's legs. <i>\"Don't worry, I'll give you a nice pleasant orgasm.\"</i><br> "
						+ "Yui begins to finger %s, who lets out a gasp of surprise. Yui notices her victim's confusion and holds up a compact, fingertip-size "
						+ "vibrator. <i>\"Are you enjoying the Ishida Kunoichi Hidden Weapon technique? You make some really cute sounds. "
						+ "Let Master and I hear some more of them.\"</i><br>"
						+ "%s lets out quite a few moans before, during, and even slightly after she climaxes at Yui's relentless hands. The cute ninja seems "
						+ "to have a bit of a dominant streak when it comes to other girls.",
						target.name(),target.name(),target.name());
			}
		}
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "Your fight with "+assist.name()+" has barely started when you hear a familiar voice call out to you. <i>\"Master! I was hoping you would be here.\"</i> " +
					"Before you can react, Yui grabs you and eagerly kisses you on the lips. Your surprise quickly gives way to extreme lightheadedness and drowsiness. " +
					"Your legs give out and you collapse into her arms. Did Yui drug you? <i>\"Please forgive this betrayal, Master. You work so hard fighting and training " +
					"every night. For the sake of your health, I thought it was necessary to make you take a break.\"</i> She sounds genuinely apologetic, but also a little " +
					"excited. <br>"
					+ "<i>\"Don't worry. We'll take good care of you until you can move again.\"</i> She carefully lowers your limp upper body onto her lap as "+
					assist.name()+" fondles your dick to full hardness. <i>\"I'm sure we can relieve some of your built up stress too.\"</i><br>";
		}
		else{
			return "This fight could certainly have gone better than this. You're completely naked and have your hands bound behind your back. "+target.name()+" is just taking " +
					"her time to finish you off. A familiar voice calls out to her. <i>\"I see you've caught my master. I've always wanted to get him in this position.\"</i> You " +
					"both surprised to see Yui standing nearby. She hadn't made a sound when she approached. <i>\"Do you mind if I play with him for a moment? I promise I " +
					"won't make him cum.\"</i><p>"
					+ "She kneels in front of you, fondling your balls playfully, but you suddenly feel her freeing your hands. She leans close to your ear and whispers quietly. "
					+ "<i>\"I'll create an opening so you can finish her off, Master.\"</i><br>"
					+ "She stands up casually as if to walk away, but suddenly grabs "+target.name()+"'s arms. "
					+ "You scramble to your feet and join the fight. In no time at all, you and your ninja companion have her completely immobilized.";
		}
	}
	
	public String watched(Combat c, Character target, Character viewer){
		if (viewer.human()){
			return "You hear a familiar sounding yelp as you are wandering around campus looking for your next opponent.  You head toward the sound and quickly find Yui facing off against "+target.name()+".  She has clearly been caught by surprise as "+target.name()+" is behind her holding her arms behind her back.  Yui is struggling against her captor, but not getting anywhere very fast, which you find odd as you know she is much more skilled than that.  Curious, you decide to find a place to observe the fight from. <p>"
					+ "Once you are settled in an out of the way spot where the two fighters can't see you, you turn back to the fight.  While you weren't paying attention, Yui has freed herself and put a little bit of distance between herself and her opponent.  This gives her space to work, which you know is a dangerous thing for Yui to have.  The other girl lunges at Yui, trying to get close enough to mount an attack, but Yui quickly gets out of the way and ends up behind her.  In the blink of an eye Yui removes "+target.name()+"'s top and spins away before a counterattack can come.  Yui uses her size and speed to dance literal circles around the other girl, keeping her on the defensive.  <p>"
					+ "Suddenly "+target.name()+" drops something on the ground and Yui steps on it, coming to a sudden stop.    Yui looks down as she tried to pick up her feet but she has become anchored to the spot.  Without her speed, she has become vulnerable to her opponent's attacks, and the other girl doesn't waste a minute in taking advantage of this.  She dashes up to Yui and rips off her top.  Yui let's out a yelp and her arm comes up to cover her newly exposed breasts.  She tries to mount a defense with her free arm, but between being stuck to the ground and only using one arm she doesn't stand a chance.  Yui begins to squirm and look uncomfortable as her body is fondled by the other girl through her remaining clothing.<p>"
					+ "Suddenly, you see two shadow clones sneak up behind "+target.name()+" and realize just moments before she does, that they are coming to Yui's rescue.  One grabs Yui's torturer under her arms and pulls her back while the other sweeps her legs out from under her.  She falls to the ground and they pin her there while Yui pulls her feet out of the sticky substance that had held her to the ground.  Once she is free, she leaps up and comes down on the other girl's stomach, stunning her.  Before she can recover, Yui strips her bottom half completely, leaving her completely naked.  She looks down at her immobilized opponent for only a moment before her hands begin to attack her pussy and clit.  The other girl begins to squirm and struggle under both the shadow clones hold as well as Yui's weight on her stomach.  It looks like the match is close to over.  Yui is in the power position and her opponent has been immobilized.<p>  "
					+ "You decide to leave and find your own opponent, but as you shift to get up you knock against something and there is a loud clattering sound.  You duck back down out of sight, but when you look up Yui has sprung up and is facing in your direction in a defensive posture.  She is no longer paying attention to the girl she was fighting, but instead concerned that another opponent is looking for an opening.  While she isn't looking, the other girl manages to free herself from one of Yui's shadow clones.  Once free of one, it takes no time before she gets free of the second.  Yui spins around, but before she fully comprehends that her opponent is now free, she is taken to the ground.  "+target.name()+" dives between her legs and holds Yui down as she tears off her bottoms.  Without waiting she lunges forward and attacks Yui's pussy with her tongue.<p>"
					+ "<i>\"AIIEEE!\"</i> Yui yells out from the unexpected attack to her private area.  She tries to kick and push the other girl's head away, but is unsuccessful.  You can see her breathing increase as the attacks start to get to her, and the other girl shows no signs of quitting now that she has gotten the upper hand.  Knowing that Yui tends to find a way out of sticky situations, the other girl reaches up and goes to grab Yui's breasts to try and add more stimulation to bring an end to the match.  Unfortunately this turns out to be her undoing as Yui grabs her arm and pulls up, hyperextending it.  It is now "+target.name()+"'s turn to scream, which causes her to lift her head.  The shifting of her weight is enough to allow Yui the leverage to get free.  In the blink of an eye, she has reversed the hold and is now sitting across her opponent's chest as she lays face up on the ground.  The two shadow clones come over to help, and are joined by two more.  One grabs the prone girl's hands and pulls them over her head while two more each take a leg and hold them down.  The last gets between her legs and begins to attack her pussy.  Yui just sits on her chest, watching her struggle as the realization that she has lost dawns on her.  Her breathing increases and she moans more and more as the shadow clone does its job.<p>"
					+ "<i>\"You were a worthy opponent,\"</i> Yui said as "+target.name()+"'s orgasm builds, <i>\"but this was inevitable.\"</i>  She watches as the other girl lets out a final, loud groan and falls over the edge.  As the other girl cums, Yui slides upwards and put's her own pussy over her vanquished foes mouth.  She gets the message and begins to lick at Yui's clit.  She throws her head back as her hand comes up to play with her nipples.  The previous attacks by her opponent, combined with dominating her opponent, must have aroused her more than she let on because within a minute she was gasping and you can see her begin to cum on her opponent.  Finally she gets up off of "+target.name()+" and releases her shadow clones.  She bows to the other girl, who is still lying on the ground trying to catch her breath, and walks off.  You quickly slide out of your hiding spot and head off as well, not waiting around to see what happens to the other girl.";
		}
		return "";
	}

	@Override
	public String describe() {
		return "Yui is a much more convincing kunoichi in her combat outfit. Her stylish ninja top gives her freedom of movement and is skimpy enough "
				+ "to show off her toned midriff and a fair bit of side-boob. It's a little distracting, which is probably the intention. "
				+ "In contrast to her exposed body, her face is hidden behind her long blonde bangs. A real shame, since you know she's quite beautiful.";
	}

	@Override
	public String draw(Combat c, Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(c.state==Result.intercourse){
		return "You feel your arousal building dangerously high, but you haven't reached the point of no return yet. Yui does her best to pin you down and "
				+ "ride your dick, but she's too aroused to maintain control. You manage to free your hands and grab her "
				+ "slim hips. She's light enough that you're able to set the pace, despite her superior position.<p>"
				+ "<i>\"Ah! Master!\"</i> The inexperienced kunoichi loses herself in the pleasure and can't seem to muster an effective counterattack. Instead "
				+ "she moans uncontrollably and can barely hold herself up. What you can see of her face behind her obscuring hair is a mask of wanton desire.<p>"
				+ "For a short while, you are confident you can finish her this way without cumming, but suddenly her vaginal walls squeeze your cock tightly. "
				+ "At first you think she's orgasming, but as her insides tighten and relax repeatedly, you realize this is actually a sexual technique. Whether "
				+ "she's doing it intentionally, or her training is just so thoroughly ingrained in her, her pussy is doing a damn good job milking you. Your "
				+ "best chance now is to pull out and finish her off by hand.<p>"
				+ "<i>\"Master, I love your dick! Let's cum together!\"</i> Yui pulls you into a passionate kiss as your hips grind together. You might be able "
				+ "to claim victory if you pull out now, but it would probably break the poor girl's heart. Some things are more important than winning.<p>"
				+ "You thrust deep into Yui and shoot your pent-up load into her womb. Her body shudders and presses tight against you as she hits her own climax. "
				+ "You embrace her closely as you both indulge in your shared moment of blissful pleasure.<p>"
				+ "In the afterglow, you hear Yui giggle quietly to herself as her head rests on your shoulder. It's cute, but you want in on the joke. You tickle "
				+ "her neck softly and ask what she finds so funny.<p>"
				+ "<i>\"It's not funny, I'm just happy. I was just thinking about our first time together. We came together that time too.\"</i> She's cute enough "
				+ "to warm your heart, but as her master should be a little strict. You gently reprimand her for her lack of discipline. If you had simply pulled "
				+ "out at the end, she would have lost the fight completely.<p>"
				+ "She just smiles and cuddles against you contently. <i>\"But you didn't pull out. Thank you, Master.\"</i> There's really nothing you can say in "
				+ "the face of such pure trust. Instead you just hold her for a bit longer.";
		}else{
			return "The fight has been fast and furious to this point, with Yui pulling off a lot of fast moves that have "
					+ "left you reeling time and again. She ends up pulling off a quick series of moves that end with her flipping "
					+ "in front of you, so that her pussy passes close to your nose as her tongue quickly caresses your dick "
					+ "multiple times. Between the smell of her and the sensations, you won't last much longer against her "
					+ "assault. You step back and take a deep breath, trying to regain your focus before her next move. "
					+ "As Yui is landing from her latest attack, she notices your hesitation. She decides to capitalize on "
					+ "it and the instant her foot touches the ground she launches toward you in a low tackle move. You open "
					+ "your eyes as she starts the move and realize your focusing technique has worked. She appears to be "
					+ "moving much more slowly now and you can see where she is and anticipate where she is going to end "
					+ "up. You bring your arm up, catching her below her soft breasts, pushing upward so she rotates. She "
					+ "ends up parallel to you. You manage to stay standing as she slams into you, instinctively bringing her "
					+ "legs around your waist. You can feel her heat and wetness against your stomach as she connects.<p>"
					+ "Before she can latch on too tightly you get your hands under her ass and lift her up. Her small "
					+ "size and light weight make it easy to move her upwards. In no time you have her pussy level with your "
					+ "mouth. You immediately go to work with your tongue, licking her from clit to almost her asshole, and "
					+ "back again.<p>"
					+ "<i>\"Oh! Master!\"</i> you hear her moan as she latches her legs around your neck and pulls herself "
					+ "close. This limits your reach to just her entrance and clit. However, that is enough to keep her "
					+ "squirming and you can tell that she is pretty aroused. You start to think you just might pull off a win "
					+ "here.<p>"
					+ "Suddenly you feel a familiar sensation running along the length of your cock. It takes a few "
					+ "seconds, but you realize that it is a tongue. At the same moment a pair of hands take hold of your balls. "
					+ "You manage to loosen her hold on your neck enough to see down and notice a familiar blonde head that "
					+ "can only belong to Yui. Somehow she managed to summon a clone despite your distraction and now "
					+ "you feel your arousal rising once more. You try to push her clone away with your leg while retaining "
					+ "your grip on the real Yui, but the clone fights back and eventually you lose your balance. You fall over "
					+ "backwards and partially lose your grip on Yui's ass.<p>"
					+ "You hit the ground and grapple for a couple minutes with her as she tries to exploit the sudden "
					+ "weakness in your offensive. She manages to swing herself around, but you manage to grab ahold of her "
					+ "waist and keep her from getting away. However, it turns out her plan is something other than escape. "
					+ "When you finally regain control it is too late, she has turned fully around and is facing your cock. She "
					+ "begins to stroke your length with her hand before you are able to react. You quickly scramble and "
					+ "manage to get your arm around her. You find her clit and go to work, racing her frantic stroking as you "
					+ "each try to make the other cum first. You feel yourself getting dangerously close but you can tell that "
					+ "she is getting closer as well.<p>"
					+ "Yui suddenly becomes desperate and gambles by leaning over and taking you into her mouth. "
					+ "You let out a groan as you try your hardest to hold back. You look up and realize that her shifting has "
					+ "caused her ass to rise into the air, exposing her pussy to you. You reach up and sink two fingers deep "
					+ "into her. You begin to pump in and out and she begins to squirm more and more. She is obviously "
					+ "getting close, but you aren't sure if you can hold out. You are at the point where you begin to hope that "
					+ "you can just make the match a draw.<p>"
					+ "Suddenly you feel yourself go past the point of no return. At the same time though Yui lifts up "
					+ "and lets out a groan and she begins to cum. At the same time you explode, coating your stomach as "
					+ "well as Yui's stomach and chest with your cum. You groan and slump against the ground as your energy "
					+ "also drains out of you. A draw feels just like a loss at this point, especially when you fought so hard. "
					+ "Once your cock stops erupting, Yui slumps down along your legs, appearing just as drained. "
					+ "The two of you just lay there like that for a bit. Finally Yui gets up. Her legs appear wobbly and "
					+ "for a minute you worry about whether she will be able to stay upright. You stand up and grab her "
					+ "around the waist in support.<p>"
					+ "<i>\"Thank you Master,\"</i> she says, <i>\"but you need to leave your kunoichi to fend for herself. I'm sorry "
					+ "I couldn't give you a win, go catch up with the others and try to make up for this draw\"</i>";
		}
	}

	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude()||opponent.nude();
	}

	@Override
	public boolean attack(Character opponent) {
		return true;
	}

	@Override
	public void ding() {
		character.mod(Attribute.Ninjutsu, 1);
		int rand;
		for(int i=0; i<(Global.random(3)/2)+1;i++){
			rand=Global.random(4);
			if(rand==0){
				character.mod(Attribute.Power, 1);
			}
			else if(rand==1){
				character.mod(Attribute.Seduction, 1);
			}
			else if(rand==2){
				character.mod(Attribute.Cunning, 1);
			}
			else {
				character.mod(Attribute.Ki, 1);
			}
		}
		character.getStamina().gain(5);
		character.getArousal().gain(5);
		character.getMojo().gain(3);
	}

	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case ninjapreparation:		
				return "As you approach Yui, she suddenly sprints away. You quickly give chase, but are blindsided by a spring-propelled net "
						+ "after a few steps. As you struggle to untangle yourself from the net, your head starts to swim, and you realize Yui "
						+ "got you with a couple of her drugged needles while you were distracted.<p>"
						+ "The kunoichi girl approaches to finish you off. <i>\"Sorry Master, but you should know it's dangerous to persue a ninja.\"</i>";
			case flash:
				return "After your last fight, you confidently stride toward Yui, but she dashes backward to keep a safe distance. She seems pretty cautious. "
						+ "More importantly, she's a lot quicker than you realized.<p>"
						+ "<i>\"Of course, Master. I am a ninja, after all. I can be extremely nimble when I need to be.\"</i>";
			case ishida3rdart:
				return "Yui looks a little more nervous than usual, but smiles at you excitedly. <i>\"Master, I know I sometimes have "
						+ "trouble concentrating when we're doing... intense things,\"</i> her hands moves unconsciously to her groin. "
						+ "<i>\"But I've been doing image training to prepare. If you put it inside me again, I'm sure I'll do my techniques "
						+ "properly this time.\"</i>";
			default:
				break;
	
			}
		}
		if(character.nude()){
			return "Yui gives you a sheepish grin as she tries to cover her naked body. <i>\"I guess I got a little careless, Master. Normally I wouldn't "
					+ "let anyone but you undress me.\"</i>";
		}
		if(opponent.pantsless()){
			return "<i>\"Ah! Master!\"</i> Yui starts to bow to you, but freezes partway through. She seems to be staring at your naked crotch with intensity. "
					+ "You cough to get her attention.<p>"
					+ "<i>\"Oh, right! The match! I was just planning my strategy. I definitely wasn't daydreaming about how I lost my virginity.\"</i>";
		}
		if(character.getAffection(opponent)>=30){
			return "Yui smiles brightly as she sees you. ";
		}
		return "Yui gives you a polite, eager bow. <i>\"Master, let's have a good sparring match. Just like our training, but "
				+ "with more pleasure!\"</i>";
	}

	@Override
	public boolean fit() {
		return character.getStamina().percent()>=60&&character.getArousal().percent()<=20&&!character.nude();
	}

	@Override
	public String night() {
		return "After the match, you hang around to chat with the girls for a few minutes. Yui in particular seems restless when you "
				+ "talk to her. It's clear she's hoping for something, but she's restraining herself from asking. Maybe she doesn't want "
				+ "to appear needy. When you invite her to your room, she looks as eager as an excited puppy.<p>"
				+ "You reach the dorm, and she gets a little self-conscious about how much she was sweating during the match and excuses "
				+ "herself to use the shower. Realizing you also smell of sweat (and other fluids), you decide to join her.<p>"
				+ "After washing each other clean, and then messy, and then clean again, the two of you eventually collapse naked into "
				+ "your bed. You're exhausted enough that you could fall asleep immediately, but Yui is still feeling a bit frisky, so "
				+ "you decide to go one last round.<p>"
				+ "You make love to her slowly and tenderly, the way you never have time for during a match. When it's finished, she "
				+ "snuggles comfortably in your arms as you start to drift off to sleep.<p>"
				+ "<i>\"Master, when I moved here, I never imagined I'd meet someone like you.\"</i> You hear Yui whisper softly. It's not "
				+ "clear whether she knows you're conscious, so you just listen quietly. <i>\"I'm glad you were my first. I know you may "
				+ "fall in love with one of the other girls, but I'll be your kunoichi for as long as you'll let me.\"</i><p>"
				+ "She goes quiet after that. For a long moment, you consider whether to answer her, but soon her soft, regular breathing "
				+ "tells you she's fallen asleep.";
	}

	@Override
	public void advance(int rank) {
		if(rank >= 2 && !character.has(Trait.assassin)){
			character.add(Trait.assassin);
		}
	}

	@Override
	public boolean checkMood(Emotion mood, int value) {
		switch(mood){
		case confident:
			return value>=30;
		default:
			return value>=70;
		}
	}
	@Override
	public float moodWeight(Emotion mood) {
		switch(mood){
		case confident:
			return 1.2f;
		default:
			return .8f;
		}
	}

	@Override
	public String image() {
		return "assets/yui_"+ character.mood.name()+".jpg";
	}

	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Master, don't hold back. I'll happily accept your seed.\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Master, don't hold back. I'll happily accept your seed.\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"It feels so good! I can't hold back!\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Master! Ah! Even on top, your dick is irresistible!\"</i>");
		comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Master, you shouldn't leave your back open when a ninja is nearby.\"</i>");
		comments.put(CommentSituation.BEHIND_SUB_LOSE, "<i>\"From behind!? How!?\"</i>");
		comments.put(CommentSituation.SIXTYNINE_WIN, "<i>\"Your dick is really throbbing. Are you enjoying my technique?\"</i>");
		comments.put(CommentSituation.SIXTYNINE_LOSE, "<i>\"Master, you're too good at this! I can't keep up!\"</i>");
		comments.put(CommentSituation.OTHER_BOUND, "<i>\"Master, you look a little tied up. Don't worry, I'll handle your needy penis.\"</i>");
		comments.put(CommentSituation.SELF_BOUND, "<i>\"Well done, Master. You caught your very own ninja girl. I'll be out of this in a snap!\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"Please, Master... I don't care if I lose, just give me your love!\"</i>");
		comments.put(CommentSituation.SELF_CHARMED, "<i>\"Yes, Master. Whatever you need.\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"In my butt!? That's a really advanced technique!\"</i>");
		comments.put(CommentSituation.OTHER_STUNNED, "<i>\"I didn't expect you to go down so quickly. I'll give you lots of pleasure if you don't resist.\"</i>");
		comments.put(CommentSituation.OTHER_OILED, "<i>\"Preparations are complete. Time for a well-lubricated handjob!\"</i>");
		comments.put(CommentSituation.SELF_SHAMED, "<i>\"M-Master, don't look! It's embarrassing...\"</i>");
		comments.put(CommentSituation.PIN_DOM_WIN, "<i>\"I've practiced this hold, you won't get away so easily.\"</i>");
		comments.put(CommentSituation.PIN_SUB_LOSE, "<i>\"I know how to break this hold...ah...um... This might be trickier than I thought.\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "In a high, weak voice, Yui says, <i>\"Thank you for the lesson, master, I still have much to learn.\"</i>");
		return comments;
	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if(c.eval(character)==Result.intercourse){
			character.addGrudge(opponent, Trait.ishida3rdart);
		}else{
			switch(Global.random(2)){
			case 0:
				character.addGrudge(opponent,Trait.flash);
				break;
			case 1:
				character.addGrudge(opponent,Trait.ninjapreparation);
				break;
			default:
				break;
			}
		}
		
	}
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        character.outfit[0].add(Clothing.kunoichitop);
        character.outfit[1].add(Clothing.panties);
        character.outfit[1].add(Clothing.ninjapants);
    }

}
