package characters;


import daytime.Daytime;
import global.*;

import items.Clothing;
import items.Item;
import items.Toy;
import items.Trophy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import Comments.CommentGroup;
import Comments.CommentSituation;
import skills.Skill;
import skills.Tactics;
import stance.Stance;
import status.Feral;
import status.Stsflag;

import combat.Combat;
import combat.Result;

import actions.Action;
import actions.Move;
import actions.Movement;
import actions.Resupply;
import areas.Area;

public class Kat implements Personality {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8169646189131720872L;
	public NPC character;
	public Kat(){
		character = new NPC("Kat",ID.KAT,10,this);
		character.outfit[0].add(Clothing.bra);
		character.outfit[0].add(Clothing.Tshirt);
		character.outfit[1].add(Clothing.panties);
		character.outfit[1].add(Clothing.skirt);
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.Tshirt);
		character.closet.add(Clothing.panties);
		character.closet.add(Clothing.skirt);
		character.change(Modifier.normal);
		character.setUnderwear(Trophy.KatTrophy);
		character.add(Trait.female);
		this.character.set(Attribute.Power, 10);
		this.character.set(Attribute.Animism, 14);
		this.character.set(Attribute.Cunning, 8);
		this.character.set(Attribute.Speed, 8);
		this.character.set(Attribute.Seduction, 7);
		this.character.getStamina().setMax(70);
		this.character.getArousal().setMax(90);
		this.character.getMojo().setMax(60);
		character.add(Trait.dexterous);
		character.add(Trait.pheromones);
		character.add(Trait.tailed);
		character.add(Trait.shy);
		character.add(Trait.sympathetic);
		character.plan = Emotion.sneaking;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 1);
	}
	@Override
	public Skill act(HashSet<Skill> available,Combat c) {
		HashSet<Skill> mandatory = new HashSet<Skill>();
		HashSet<Skill> tactic = new HashSet<Skill>();	
		Skill chosen;
        for(Skill a:available){
            if(a.toString()=="Command"||a.toString().equalsIgnoreCase("Ass Fuck")){
                mandatory.add(a);
            }
            if(character.is(Stsflag.orderedstrip)){
                if(a.toString()=="Undress"||a.toString()=="Strip Tease"){
                    mandatory.add(a);
                }
            }
        }
		if(!mandatory.isEmpty()){
			Skill[] actions = mandatory.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		ArrayList<HashSet<Skill>> priority = character.parseSkills(available, c);
		if(Global.checkFlag(Flag.hardmode)&&Scheduler.getMatch().condition!=Modifier.quiet){
			chosen = character.prioritizeNew(priority,c);
		}
		else{
			chosen = character.prioritize(priority);
		}
		if(chosen==null){
			tactic=available;
			Skill[] actions = tactic.toArray(new Skill[tactic.size()]);
			return actions[Global.random(actions.length)];
		}
		else{
			return chosen;
		}
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		Action proposed = character.parseMoves(available, radar, match);
		return proposed;
	}

	@Override
	public void rest(int time, Daytime day) {
		if(!(character.has(Toy.Onahole)||character.has(Toy.Onahole2))&&character.money>=300){
			character.gain(Toy.Onahole);
			character.money-=300;
		}
		if(!(character.has(Toy.Tickler)||character.has(Toy.Tickler2))&&character.money>=300){
			character.gain(Toy.Tickler);
			character.money-=300;
		}
		String loc;
		ArrayList<String> available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Reference Room");
		available.add("Play Video Games");
		for(int i=0;i<time-3;i++){
			loc = available.get(Global.random(available.size()));
			day.visit(loc, character, Global.random(character.money));
			if(loc!="Exercise"&&loc!="Browse Porn Sites"){
				available.remove(loc);
			}
		}
		if(character.getAffection(Global.getPlayer())>0){
			Global.modCounter(Flag.KatDWV, 1);
		}
		character.visit(3);
	}

	@Override
	public String bbLiner() {
		switch(Global.random(2)){
		case 1:
			return "Kat's already large eyes widen even further as she looks at the pained expression on your face.  "
					+ "<i>\"Nya!  A swift shot to the nyutsack.  Works every time!\"</i>";
		default:
			return "Kat gives you a look of concern and sympathy. <i>\"Nya... Are you ok? I didn't mean to hit you that hard.\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		if(character.getArousal().percent()>=50){
			return "Kat makes no effort to hide the moisture streaming down her thighs. <i>\"You want my pussy? I'm nyot going to myake it easy for you.\"</i>";
		}
		else{
			return "Kat blushes deep red and bashfully tries to cover her girl parts with her tail. <i>\"Don't stare too much, ok?\"</i>";
		}
	}

	@Override
	public String stunLiner() {
		return "Kat mews pitifully on the floor. <i>\"Don't be so meaNya.\"</i>";
	}

	@Override
	public String winningLiner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String taunt() {
		return "Kat smiles excitedly and bats at your cock. <i>\"Are you already close to cumming? Nya! I want to play with you more!\"</i>";
	}

	@Override
	public String victory(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		character.arousal.empty();
		if(flag==Result.intercourse){
			return "She pounces at you, pushing you onto your back and holds you down with the weight of her body. A cute mew of a smile crosses her face, and her tongue sticks " +
					"out slightly from between her lips. She is riding your cock in a regular rhythm now, not worried as she knows you are much closer to your climax than her.<p>" +
					"As you gasp and wriggle, trying to escape from a loss she reaches out and gently scratching and tickling your nipples.<p>" +
					"<i>\"Nyahaha!\"</i> she giggles, toying with your body as a cat would toy with a mouse it caught.<p>" +
					"The added sensation of having your nipples molested is enough to drive you over the edge and you feel your cock begin pulsating inside of her tight cunt. " +
					"You grunt a couple of times as her pheromones prolong the contractions of your climax. When the cum has finally been drunk from your cock by her tight love " +
					"hole she gasps and lets you slip out.<p>" +
					"Collapsing onto your back your chest heaves, your mind a blurry mess from the massive orgasm you just experienced. You can feel your orgasmic high subside " +
					"slowly and peek over at the catgirl to see how she's doing. To your surprise you can see that she's curled up beside you with a small trail of your semen " +
					"leaking out of her pussy and forming a little puddle around her ass.<p>The look on her face is one of mixed emotions. Something in between the satisfaction " +
					"of having a primal need sated and frustration because something else is lacking. The catgirl seems confused, not sure how to react to all the emotions " +
					"bubbling up inside of her. A low <i>\"meeeow~\"</i> escapes her lips as she stares at you, her big round eyes blinking slowly. She begins mumble something " +
					"else, stopping just before the second syllable passes her lips. Her tail begins swaying back and forth suggesting a persistent unquenched agitation.<p>" +
					"To you the catgirl looks more like a cat who caught the smell of her master cooking chicken and came running to beg for a piece rather than the sly little " +
					"vixen that just pounced at you, milking your excited cock for all the semen she could. Then you realised the problem she was facing, receiving your cum in " +
					"her pussy only served to satisfy her primal instinct; but that primal instinct was waning now and it was her human side that was beginning to surface. That " +
					"human side that desired to do more than simply reproduce, that human desire for pleasure.  To satisfy that deep, human need to be allowed an orgasm, she's " +
					"not about to let you go until she's completely satisfied. Crawling up over you she meets your gaze, her eyes begging for you to give her that relished release. " +
					"She doesn't need to utter a single sound; you're certain what she needs, even if she doesn't quite know herself.<p>" +
					"You debate how to repay her, eventually settling on your skilled fingers. You roll over on top of Kat, whispering hot breathy nothings into her furry ear. " +
					"She squirms and wriggles at the attention, her pussy moistening under your skilled manipulation of her clitoris. She is putty in your hands now, you plunge " +
					"two fingers past her labia, feeling them easily slide into her body. Her aching g-spot longs to be rubbed and massaged. Her arms clench tightly around your " +
					"back, as you are pulled in closer to her body you feel her warmth radiating off her hot skin.<p>" +
					"<i>\"NyaaaaAAAAAAA!\"</i> she howls in an animalistic outburst, her hips bucking uncontrollably against the perpetual rubbing of your fingertips. In mere " +
					"minutes you have lifted her to an orgasmic high, soaking your hand in her sweet essence before crashing back to Earth in exhaustion.<p>" +
					"<i>\"Wow...\"</i> she remarks, easing her grip around your back as you slowly withdraw your fingers. Her tail tickles your side as it curls back around. She " +
					"stands up and puts her clothes back on before rushing away down the corridor, <i>\"See you around!\"</i>";
		}
		else if(!character.is(Stsflag.feral)){
			return "Kat's deft ministrations push you over the edge, pleasure racing through your body as your climax bursts" +
					"from the tip of your shaft. She lets out a surprised gasp as some of your seed falls against her lithe body," +
					"taking a short hop away from you and letting you settle into a sitting position, too caught up in the" +
					"lingering surge of your release to move.<p>" +
					"You're expecting Kat to pounce on you at any moment, the Cat never one to let you off so easily, but to" +
					"your surprise Kat remains where she is, tail twitching behind her as she slowly trails a finger through the" +
					"ropes of warm cum clinging to her chest. She looks off in her own little world, cheeks flushing a lovely" +
					"shade of pink when she finally notices you watching her, free hand darting down to hide her sex even as" +
					"she keeps wandering her fingers through the lewd mess you've left over her body.<p>" +
					"<i>\"I...Um.\"</i> She pauses, eyes sparkling even through her embarrassment and a proud smile tugging shyly at" +
					"the corners of her mouth, <i>\"I made you feel really good, didn't I?\"</i> You nod, a rueful grin stealing over" +
					"your lips as you realize you're still talking to the Girl. Kat's nervous excitement is infectious, however," +
					"your smile warming as she blushes all the harder, hiding her face behind her hands and peeking out at you" +
					"between her fingers, hips twisting back and forth slightly as her tail continues to dance behind her.<p>" +
					"<i>\"I'm not really...The Girl isn't...\"</i> She glances past you, as if expecting one of the other participants to" +
					"stumble upon you both at any moment, but after a heartbeat more takes a slow, shuddering breath and" +
					"drops her hands to her sides, slowly stepping forward to stand little more than a foot in front of you, <i>\"I'm" +
					"not used to finishing things like this but...Since I won-\"</i> She rests one hand on the top of your head, her" +
					"touch wandering fondly over you as she brings her other hand down, shifting her thighs apart to expose" +
					"her delicate folds to you as she brushes two fingers over her petals, spreading them to reveal the" +
					"glistening pink of her sex, <i>\"Make me feel good too, please.\"</i><p>" +
					"You're quite happy to acquiesce to Kat's surprisingly bold request, and bring your hands up to stroke over" +
					"the back of her thighs, the girl trembling gently at your touch before letting out a soft pant of breath as" +
					"you cup your palms over her pert rear. You gently pull her closer, Kat letting out a shy whimper that's" +
					"soon eclipsed by a soft, sweet moan as you press a tender kiss over her clit. The soft pressure against her" +
					"sensitive button makes the girl all but melt against you, a shudder of pleasure rolling up her spine as you" +
					"massage that tender bud between your lips.<p>" +
					"The pleasant scent of her steadily building desire soon makes you head spin and you press a little firmer" +
					"against her, parting your lips to lavish a slow lick over her sensitive pearl. The fresh sensation pulls a" +
					"sweet cry of pleasure from Kat's lips and she starts to shyly roll her hips forward, her motions somewhat" +
					"hesitant and uneven as she seeks out more of your embrace.<p>" +
					"You give her rear a tight squeeze and pull her a little closer, nudging her on as you match the pace of your" +
					"steady licks to her slow rocking motions, tilting your head back slightly to glance up and see her watching" +
					"you with a look of rapture. Her cheeks are still dark with embarrassment and pleasure, lips parted as sweet" +
					"moans mingle with her trembling breathing, but her eyes are still missing the primal need the Cat brings" +
					"out in her. The moment she catches you looking she lets out a startled gasp and presses both hands down" +
					"on the top of your head, pushing your head back down as she grinds her sex forward against your lips and" +
					"tongue, <i>\"Nyo! Don't look!\"</i><p>" +
					"Despite her embarrassment, Kat starts to rock her hips even faster than before, lusty mewls of pleasure" +
					"mixing in with her gasps as you run your tongue up and down over her folds, squeezing and massaging" +
					"her soft rear in rhythm with your motions. Feeling that she must be getting close, you press your tongue" +
					"against the entrance of her sex, only for Kat to hook one leg over your shoulder and shove herself against" +
					"your lips, forcing your tongue into her greedy passage.<p>" +
					"The jolt of pleasure tears a fully catlike cry from her lips, Kat forcing you to lean back a little as she starts" +
					"to ride your tongue with eager little jerks of her hips, the sweet taste of her lust coating your tongue as" +
					"you respond by pushing it as deep as you can into her greedy sex, sliding passionate licks over her inner" +
					"walls.<p>" +
					"You glance up once more and find the Cat staring down at you, watching you with a look of pure delight" +
					"as she slides one hand behind your head, keeping you pressed against her hot and eager folds as she" +
					"reaches down with her other hand to toy with her clit. Not wanting to let Kat get carried away all on her" +
					"own, you give her rear a tight squeeze, making her squirm and grind against your lips as you thrust you" +
					"tongue into her silken passage again and again.<p>" +
					"It only takes a moment more before Kat lets out one final, wild cry, back arching beautifully as her entire" +
					"body tenses, sex squeezing down around your tongue as she climaxes against your lips. You slow down" +
					"your pace but don't relent in your steady licks, drawing shuddering moans and slowly fading mewls from" +
					"Kat as she works down from her release.<p>" +
					"After a little while longer she pulls away from you, a little unsteady on her feet, and spares a quick glance" +
					"at you face, her shy gaze once more clear of the Cat's primal desires, <i>\"That was really good. Thank you.\"</i>" +
					"A hint of her proud smile from earlier flickers across her lips, the afterglow of her climax and the fact that" +
					"she had done almost all of it as the Girl seeming to bolster Kat's confidence as she leans in to give you a" +
					"quick kiss before darting away.";
		}
		else{
			opponent.arousal.set(opponent.arousal.max()/3);
			return "As Kat pleasures you, you're quickly reaching the limit of your control. You try to put some distance between you and her to catch your breath. The familiar " +
					"tightness in your groin warns you that any stimulation will probably set you off right now. Eager to finish you off, Kat pounces on you and grabs your " +
					"dick with both hands. <br><i>\"Nyaha! I got you!\"</i> The last of your endurance is blown away as she jerks you off excitedly. You shoot your load into the air, " +
					"narrowly missing her face.<p>" +
					"You lay on the floor, trying to catch your breath and notice that Kat is staring at you intently, still holding onto your " +
					"member. If she plans to keep milking you, you're going to need a few minutes to recover. <i>\"I caught you. You're nyat getting away until we're done.\"</i> " +
					"There's no way she failed to notice your ejaculation, the fight's clearly over. Kat bites her lip and you notice her hips squirming with need. Ah, that's " +
					"what she means. You were never planning to leave without returning the favor. You ask her to let you up so you can service her properly. She shakes her head emphatically. " +
					"<i>\"I'm nyat letting go of you until I'm satisfied.\"</i> This must be an instinctive thing. She's caught her prey, so she's reluctant to give it up. <p>" +
					"Fortunately, " +
					"she's pretty light, so you're able to simply drag her hips closer to your face. She still refuses to loosen her grip on your penis, which is starting to " +
					"harden again from the continuous stimulation, but at least you have access to her girls parts now. You slide a finger into her wet pussy and she gives an " +
					"appreciative moan. You spot her little love bud peeking out between her lips, but you leave it alone for now, you're aiming for a different target. You rub " +
					"your probing finger along her inner walls until you notice a spot with a rougher texture. As you stimulate this area, Kat's waist trembles and her voice catches " +
					"in her throat. G-Spot successfully located.<p>" +
					"You add a second finger and focus on rubbing her G-Spot until she's writhing in pleasure and mewing uncontrollably. " +
					"Only then do you close your lips around her clitoris and attack it with your tongue. This triggers Kat's climax instantly. Her hands involuntarily squeeze your " +
					"dick as her pussy squeezes your fingers just as firmly. As her orgasm ends and she lays on the floor panting, she finally lets go of your dick.<p>"
					+ "<i>\"I'm sorry about " +
					"being stubborn,\"</i> you hear her mumble. <i>\"Sometimes my cat instincts make it hard to think and I get carried away. I hope I didn't hurt your- Nya!?\"</i> She lets out " +
					"a surprised yelp as she opens her eyes and sees your fully engorged cock inches away from her face.<p>"
					+ "This is apparently too much for her to handle right now, because " +
					"she turns beet red and runs away in a panic, leaving you frustrated and unsatisfied.";
		}
	}

	@Override
	public String defeat(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		declareGrudge(opponent,c);
		if(c.stance.en == Stance.standing){
		    return "You feel Kat starting to tense up as your constant thrusts push her closer and closer to the edge, her panting gasps intermixed with mewling cries of pleasure. She squirms in your arms, pushing against your chest as she tries to wiggle out of your grip, but you tighten your hold on her slender waist and have little trouble keeping the lithe feline secure in your grasp.<p>" +
                    "With just a few thrusts more Kat arches her back, tail lifting up into the air behind her as she cries out, her sex squeezing down around your shaft as you thrust up, burying your rod fully inside her tight passage. You give yourself a heartbeat or two to catch your breath and enjoy the look of pleasure decorating Kat’s expression before wrapping one arm around her back, pulling her close against your chest as you support the girl’s light frame with your other arm. Kat lets out a soft murr of pleasure as she nuzzles against your shoulder, her panting breaths playing in warm little puffs over your skin as she basks in the afterglow.<p>" +
                    "You give her rear a teasing squeeze before whispering that you’re not finished yet, hardly giving Kat a chance to process your words before you pull your hips back and give a firm thrust upwards, burying your shaft deep inside the feline’s warm passage. Kat’s eyes go wide at the sudden jolt of pleasure, her soft inner walls quickly squeezing down around your rod, clinging to your member as you tighten your grip, holding Kat in place and leaving her at your tender mercy.<p>" +
                    "<i>\"Wait! I’m too sensit-Nyah!\"</i> Her words are lost in a rather feline sounding cry of pleasure as you slam into her again, steadily settling right back into your firm pace from before. Each time your shaft shoves deep into her sex Kat lets out a cry of pleasure, her inner walls trembling and clenching around your rod as you send jolt after jolt of pleasure through Kat’s slender body.<p>" +
                    "She squirms in your arms, lifting her hips as best she can as she tries to slow the pace down, but with her small frame it’s oh so easy for you to tighten your grip on her pert rear and pull her back against your hips, making Kat moan with pleasure as each thrust once more buries the full length of your rod inside her.<p>" +
                    "It doesn’t take much more before Kat stops trying to pull away from your thrusts and instead buck back into them, her wiggling turning to steady rolls of her hips as she presses herself against your chest. Her more catlike side must be kicking in hard, moans of pleasure turning to lusty mewls as she wraps her arms around your back, nails digging lightly into your skin each time you slam inside her.<p>" +
                    "Not content to let Kat so easily settle into things, you cup both your hands beneath her ass, squeezing and kneading her as you ratchet up the pace even more, burying a rough kiss against the side of her neck as you slam your hips up again and again. Kat’s inner walls squeeze wildly around your shaft as her second climax comes on hard, the feline leaving little scratches all over your back as she clings tightly to your body.<p>" +
                    "She flexes her back, soft breasts pressing into your chest as she locks her legs around your waist and squeezes as hard as she can, crying out in wild ecstasy as you thrust forward in the same moment, your hips grinding together as you bury your shaft as deep as you can inside her drooling sex. Her inner walls clench down around your cock just as hard, your shaft throbbing in their silken grip as your final thrust pushes you over the edge, pleasure enveloping your entire body as Kat clings to you.<p>" +
                    "A shiver of delight rolls up her spine as the heat of your release pours deep into her sex, the Girl slowly resurfacing as her more feline desires melt away into warm contentment, Kat going limp in your arms as you both taper off from your shared release. She rests her head against your shoulder for a moment, gasping softly as you slowly pull your hips back, a slick mix of her lust and your seed dripping from her soft folds as your shaft slides free.<p>" +
                    "After a few moments more you carefully set her down, Kat seeming just a touch unsteady on her feet as she leans against you for a few heartbeats more before giving you a shy little smile and making her retreat.<p>";
		            }
		else if(flag==Result.intercourse){
			return "Kat squeaks as you pump your cock inside her over and over, penetrating her deeper with each thrust. She seems to be particularly vulnerable to being fucked" +
					" doggy style; perhaps it makes her g-spot easier to hit each time you thrust into her.<p>" +
					"<i>\"Ah ahhhh eeeepppp!\"</i><p>" +
					"Her back suddenly arches after a particularly strong thrust. You feel her pussy walls clamp and quiver around your cock, undulating as she rides out a climax.<p>" +
					"<i>\"Nyaaa!\"</i><p>" +
					"Her final pleasurable moan escapes her lips before she slides off your cock and onto the floor, catching her breath.<p>" +
					"<i>\"A-alright, this is the part when I...\"</i> she reaches up to your stiff cock with trembling hands and grabs it with a nervous excitement. For a second " +
					"you think she is going to jerk you off, then you hear her again, <i>\"L-lie down...\"</i><p>" +
					"You comply, lying on your back while she positions herself above you, straddling your waist. She is clearly in heat, her cheeks blushing a crimson red, and " +
					"her pussy as hot and wet as any girl you've ever felt. As she squats down, taking your member inside her she half-gasps, half-purrs. It doesn't take long " +
					"for her to pick up the pace and establish a steady rhythm of gliding up and down your shaft. Her ears twerk back and forth and she leans forward, resting " +
					"her palms on your shoulders.<p>" +
					"You can barely take it anymore, her pussy feels is a perfect fit, massaging your cock to a blissful orgasm. You grunt twice and shoot a load deep inside her. " +
					"This seems to satisfy her as much, if not more than her first orgasm. Perhaps some primal instinct to breed? Sliding off your cock she plants a quick kiss " +
					"on your forehead and scurries out of the room, leaving you gasping and pulling yourself back to your feet.";
		}
		else if(character.is(Stsflag.bound)){
            return "You almost let out a yelp when you feel something wrap around your cock. You'd just managed to bind Kat's hands behind her back, so they definitely aren't to blame for that. After a brief moment of worry that someone else has joined the match and is attempting to finish you off, you finally clue into the fact that what's wrapped around your cock right now is far too soft to be anything other than Kat's tail.<br>" +
                    "You'll have to do something about that; she's supposed to be tied up right now, and it's not fair that she gets to keep trying to arouse you like this. You reach down and grab the base of Kat's tail, warning her that she'd better keep it to herself.<br>" +
                    "<i>\"Nya-ahh... sorry...\"</i> Kat says, her voice a bit breathy right now. <i>\"That... ah... thing has a mind of its ownya...\"</i> Kat's tail slowly relaxes as she says this, but what's more interesting is what's going on with the rest of her body. She's stopped trying to break free of the zip tie you'd wrapped around her wrists, and instead she simply seems to be shuddering in pleasure. You can even see her toes curling up right now.<br>" +
                    "You look down at your hand, wrapped around the base of Kat's tail. It would seem you've found one of her weaknesses. A grin crosses your face as you slowly twist your hand one way, and then back the other.<br>" +
                    "<i>\"No... no... nyaoo...\"</i> Kat says, her voice fading into a moan. You can see her squeezing her eyes closed, her face scrunched up rather adorably. She doesn't have the strength to resist right now, and she must know that the match is lost. She's probably seconds away from cumming right now, and you aren't even touching her pussy at the moment.<br>" +
                    "As easy as it would be to win the match right now, you can't help but find Kat just cute enough that you want to have a bit more fun with her first. You loosen your grasp on your tail and move your hand up, grabbing onto it a bit further up, where hopefully it's a bit less sensitive. This seems to be the case, as Kat appears to relax a bit. You give her a few seconds to recover, as you don't want her cumming too soon.<br>" +
                    "After a bit of time, Kat slowly opens her eyes, looking up at you with the one eye that you can see. <i>\"W-why'd you stop?\"</i> she says.<br>" +
                    "Well, she did say <i>\"no,\"</i> you remind her. That's not the real reason, but you're curious to see how she'll respond to that.<br>" +
                    "Kat furrows her brow, her face scrunching up cutely once more. <i>\"I didn't mean 'stop'... I'm just annyaoyed that it feels that good, and nyow you knyow how to exploit that weakness of mine...\"</i><br>" +
                    "Well, that doesn't have to be bad, you say. Kat might be about to lose, but you can use this knowledge to make it feel amazing for her.<br>" +
                    "<i>\"Mmeow? I'm listening...\"</i> Kat says, and you can feel her tail twitch in your hand.<br>" +
                    "You tell Kat to just let you do all the work and enjoy herself. She hasn't officially lost the match yet, and you won't be in much of a mood to give her a treat if she takes advantage of your mercy.<br>" +
                    "Kat mews in response, her arms and legs pulling in to her body in a show of submission. Praising her for this, you let go of her tail, then bring your hand down to the base of it. You place your index finger just at the edge of it, where its pink fur blends into her flesh, and you slowly trace your finger along this boundary. Kat sucks in her breath and shudders a bit again. This is just the foreplay, though; you've got much more fun things in mind.<br>" +
                    "While your finger is distracting Kat, you shift your position to line yourself up directly behind her. Seeing Kat submit to you like this has gotten your cock quite hard, and you're ready to give it the release it's begging for - just a moment after Kat gets her release, of course. You bring your cock toward Kat's slit and trace the tip of it along her nether lips. You shift your hand as you do this, gripping once more onto the base of Kat's tail.<br>" +
                    "Using the base of Kat's tail like a handle, you pull her back. Kat lets out a high-pitched moan, her brain overwhelmed with the twin sensations of your cock sliding into her pussy and your hand gripping her tail. You're pretty sure you know the answer, but you ask Kat how that feels. You don't get a response though; Kat's mouth simply hangs open and lets out a whimper. It would seem she's not in much of a place for coherent thought right now.<br>" +
                    "That suits you just fine. The sooner she gets off, the sooner you can let yourself go. Using your grip on Kat's tail to pull her forward and back, you slowly begin to fuck her. You trace your free hand over her back, then circle around to cup her breast. You play with the soft flesh as you continue to pull her on and off of your cock. You're close to cumming yourself, but you just have to wait a bit longer, until-<br>" +
                    "<i>\"Nyaaaahhhhhh!\"</i> Kat cries out, her body suddenly tensing up. Her tail twitches within your grip as if trying to escape, but you hold on firmly. Instead of easing up on her, you pick up the pace, moving your hips as well now to fuck Kat even harder through her orgasm. Her scream lasts so long that she finally runs out of breath, gasping for air even as her orgasm continues to course through her body.<br>" +
                    "At last, Kat's shudders seem to cease, and she weakly collapses to the ground beneath you. Letting out a sigh, you finally let yourself go. Your cock explodes into her depths, spraying your seed deep within Kat's pussy. You close your eyes and let out a moan as you squeeze out the last few drops into Kat, then finally allow yourself to relax. You lean down and place a kiss on Kat's back, thanking her for the entertaining match.<br>" +
                    "Kat shakes her head. She pulls away from you and rolls over so she can look up at you, her face plastered with a cute smile. <i>\"I should be the one thanking you for that. You almost make me want to lose my next match against you as well.\"</i><br>" +
                    "Almost? Well, you'll just have to do a bit better next time to turn that into a sure thing<br>";
        }
		else{
			return "Kat whimpers in pleasure and drops limp to the floor, either too aroused to resist or simply realizing it's futile. Your hand busily works between her " +
					"open thighs as she moans and squirms erotically. Her pert, round breasts jiggle with her movements and you can't resist covering them in wet kisses. You " +
					"part her lower lips with your fingers and gently tickle her sensitive love button. Her back arches and she lets out a loud meow as she climaxes." +
					"<p>Kat lays on the floor in a daze for a " +
					"short while before self-consciously covering her private parts. <i>\"You got me. Your fingers are very...\"</i> She reddens when she notices your erection and quickly " +
					"averts her eyes. <i>\"W-would you like me to help deal with... that?\"</i> Her innocent reactions are adorable. You feel compelled to see her even more embarrassed, " +
					"and of course to see her orgasm again. You forcefully pin her arms by her sides, exposing her naked body, and you stick your rigid member on front of her " +
					"face.<p>"
					+ "\"Nya!? Should I.. should I use my mouth?\" She hesitantly sticks out her tongue and starts licking the head of your penis. You release her hands " +
					"and she softly grasps and strokes your dick.<p>"
					+ "Her timid efforts are surprisingly effective, so you reward her by pulling her legs open and sticking your " +
					"face into her crotch. She stutters out a protest, but it devolves into whimpering when you run your tongue over her pussy lips. She's so enthralled " +
					"by your oral skills that she momentarily forgets to service your dick and you have to stop licking her until she resumes.<p>"
					+ "She gradually starts licking and " +
					"sucking your penis more passionately and soon there's no trace of her previous shyness. Her tail is twitching erratically and almost hits you in the face " +
					"a few times as she lets out little mewling noises despite her mouth being full. You feel pleasure building up in your dick as you enter the home stretch. You " +
					"quickly locate her swollen clit and focus your tongue-work on it while you push two fingers into her soaked pussy. You feel her shudder and tense under you while " +
					"you give in to the pleasure and fill her mouth with cum.<p>"
					+ "In the afterglow, Kat seems so exhausted and content that you have to make sure she's ok. She gives " +
					"you a shy smile as she sits up. <i>\"That was a little overwhelming, but I don't mind that sort of thing.\"</i> You give her a kiss on the cheek and stroke her head " +
					"for a minute before you head out on your way.";
		}
	}

	@Override
	public String describe() {
		return "It's easy to forget that Kat's older than you when she looks like she's about to start high school. She's a very short and slim, though you know she's " +
				"stronger than she looks. Adorable cat ears poke through her short, strawberry blonde hair and a soft tail dangles from the top of her cute butt. She " +
				"looks a bit timid, but there's a gleam of desire in her eyes.";
	}

	@Override
	public String draw(Combat c,Result flag) {
		Character opponent=c.getOther(character);
		character.clearGrudge(opponent);
		if(flag==Result.intercourse){
		return "Kat lets out a near-constant mewing of pleasure as you thrust repeatedly into her tight pussy. You feel yourself rapidly approaching orgasm and judging by " +
					"how she's desperately clinging to you and thrashing her tail, she's probably in a similar state. As you feel the pressure building up in your abdomen, you " +
					"suck on her neck hard enough to give her a hickey. Your cock spurts inside her and she yowls as she hits her climax. <p>You enjoy your afterglow, holding her " +
					"small warm body while she pants with exertion. You softly stroke her cat ears and she looks up at you with content eyes. <i>\"Feels nice... That and down there.\"</i> " +
					"You grin and grind your cock -starting to soften, but still inside her- against her love bud. She lets out a soft whimper and shivers with pleasure. <i>\"Nya! " +
					"Really nice....\"</i><p>The fight is over and you're not physically ready for a second round, but Kat doesn't seem to be in any hurry to leave. You don't mind spoiling " +
					"her a bit. You kiss her softly on the lips and gently run your fingers over her smooth skin. You're not focusing on any erogenous zones, but she still shivers " +
					"and coos appreciatively under your touch. <i>\"Nya....\"</i> She lets out a happy noise as you release her lips. <i>\"Spoil me as much as you like.\"</i><p>You sit up and pull " +
					"Kat onto your lap, holding her with one hand, while the other plays with her messy girl parts. Even if she wasn't wet with her own juices, your previous load of " +
					"spunk would be plenty of lubrication. She wraps her arms around your neck and hugs you as you finger her to ecstasy. When she starts to shudder in your arms, " +
					"signalling her second climax, you press your lips firmly against hers. <i>\"Thanks,\"</i> She whispers shyly after she recovers. <i>\"You're really sweet. I still can't " +
					"let you win without a fight, but if we met during the day....\"</i> She blushes deeply and quickly excuses herself.";
		}
		else{
			return "Kat has you backed up against a wall, your lips locked in a passionate kiss. Her nimble fingers are wrapped around your stiff cock, pumping on your member as " +
					"quickly as she can. The feeling of her tongue dancing in your mouth, is driving you to the brink of an orgasm. You're almost certain you're not going to make it.<p>" +
					"You reach down, fumbling desperately between her thighs, trying to find her clitoris and make a comeback. It's difficult to concentrate though, as she starts " +
					"gently rolling your balls in between her skilled fingers.<p>" +
					"Suddenly you locate her clitoris with your thumb, rubbing it quickly as you plunge two fingers up into her dripping pussy.<p>" +
					"\"Nyyaaaa!\" she meows in ecstasy, breaking the kiss. Her fingers return to your shaft and resume pumping it furiously.You seize the opportunity to regain " +
					"lost ground and teasingly nibble on her ultra-sensitive ear, letting your tongue dance over the edge of it. Suddenly your gut clenches as you feel the first " +
					"waves of an orgasm begin to build at the base of your cock. You're going to cum, you're going to-<p>" +
					"<i>\"NYYAAAAAAAAAAAAAA!\"</i><br>" +
					"Kat's squeals echo down the corridors as a climax crashes through her body, making her spasm and writhe in ecstasy. You explode at the same time, shooting " +
					"thick streams of cum over her belly. The two of you lean against each other in the wake of the climax.<p>" +
					"Kat recovers a little bit quicker than you and scampers away down the corridor.<br>" +
					"<i>\"See ya around!\"</i>";
		}
	}
	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude()||opponent.nude();
	}
	@Override
	public boolean attack(Character opponent) {
		return true;
	}
	@Override
	public void ding() {
		int rand;
		for(int i=0; i<(Global.random(3)/2)+2;i++){
			rand=Global.random(4);
			if(rand==0){
				character.mod(Attribute.Power, 1);
			}
			else if(rand==1){
				character.mod(Attribute.Seduction, 1);
			}
			else if(rand==2){
				character.mod(Attribute.Cunning, 1);
			}
			else{
				character.mod(Attribute.Animism, 1);
			}
		}
		character.getStamina().gain(4);
		character.getArousal().gain(4);
	}
	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if(target.human()){
			return "Kat crouches between your legs, watching your erect penis twitch and gently bob. She playfully bats it back and forth, clearly enjoying herself. That is " +
					"most definitely not a toy, but she seems to disagree. As your boner wags from side to side, she catches it with her mouth, fortunately displaying the " +
					"presence of mind to keep her teeth clear. She covers the head of your dick with her mouth and starts to lick it intently, while she teases your shaft " +
					"with both hands. Oh God. You're not sure whether or not this is still a game to her, but she's being very effective. With the combined pleasure from her " +
					"mouth and hands, it's not long before your hips start bucking in anticipation of release. She jerks you off with both hands and sucks firmly on your glans. " +
					"You groan as you ejaculate into her mouth and she eagerly swallows every drop.";
		}else if(target.hasDick()){
			return "Kat gives "+target.name()+" a distinctively feline smirk as she licks her lips, her hands wrapping around her opponent's throbbing "
					+ "member and pumping up and down as she leans in, swirling her tongue around the tip. "+target.name()+" shudders, struggling in "
					+ "your grip, but you hold her hands firmly. Kat's tail curls behind her as she sucks the girl's cock. You can see the veiny shaft "
					+ "glistening with saliva. By the way "+target.name()+"'s toes are clenching, you know she won't last much longer. She's already put "
					+ "up quite a fight. <i>\"S-Stop!\"</i> she cries, but her plea makes Kat's ears perk up. She pulls back, peering up at "+target.name()
					+" with light eyes, purring softly as her tongue massages the underside of the glans.<p>"
					+ "<i>\"You want me to stop, nyaa?\"</i> Kat murmurs, lapping at the shiny precum that's appearing as she continues to furiously "
					+ "masturbate her foe. "+target.name()+" is at a loss for words, too overcome by the sensations to think coherently anymore. "
					+ "Kat plunges "+target.name()+"'s cock back into her mouth, taking it in as far as she can as she feels the girl begin to orgasm. "
					+ "You hold her steady as she bucks wildly, moaning loudly. Kat slurps down her load, cum dripping from the corners of her mouth. "
					+ "When she's finished, she sits up, wiping her mouth with a satisfied expression. It looks like victory is yours.";
		}
		else{
			return "Kat stalks over to "+target.name()+" like a cat. Very much like a cat. She leans toward the other girl's breasts and starts to softly lick her nipples. " +
					target.name()+" stifles a moan of pleasure as Kat gradually covers her breasts in saliva, not missing an inch. Kat gradually works her way down, giving " +
					"equal attention to "+target.name()+"'s bellybutton. By the helpless girl's shudders and stifled giggles, it clearly tickles, but also seems to heighten " +
					"her arousal. You feel her body tense up in anticipation when Kat licks her way down towards her waiting pussy. Instead Kat veers off and starts kissing " +
					"and licking her inner thighs. "+target.name()+" trembles in frustration and she must realize she has no chance to win, because she yells out: <i>\"Please! " +
					"Stop teasing me and let me cum!\"</i> Kat blinks for a moment and looks surprised, like she had forgotten her original purpose, but she soon smiles and dives " +
					"enthusiastically between "+target.name()+"'s legs. It doesn't take long before you feel her shudder in orgasm from Kat's intense licking, but even then, " +
					"Kat shows no sign of stopping. "+target.name()+" stammers out a protest and you sympathetically release her hands, but she's too overwhelmed by pleasure " +
					"to defend herself as Kat licks her to a second orgasm.";
		}
	}
	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if(target.human()){
			return "Your fight with "+assist.name()+" goes back and forth for several minutes, leaving you both naked and aroused, but without a clear winner. You back off " +
					"a couple steps to catch your breath, when suddenly you're blindsided and knocked to the floor. You look up to see Kat straddling your chest in full cat-mode. " +
					"She's clearly happy to see you, kissing and nuzzling your neck while mewing happily. She's very cute, but unfortunately you're unable to dislodge her from " +
					"her perch on your upper body. More unfortunately, your completely defenseless lower body is exposed to "+assist.name()+", who doesn't let the opportunity go " +
					"to waste.<p>";
		}
		else{
			return "Your fight with "+target.name()+" goes back and forth for several minutes, leaving you both naked and aroused, but without a clear winner. You back off " +
					"a couple steps to catch your breath, when you notice a blur of motion out of the corner of your eye. Distracted as you are, you aren't paying attention to " +
					"your footing and stumble backwards. "+target.name()+" advances to take advantage of your mistake, but she soon realizes you aren't looking at her, but " +
					"behind her. She turns around just in time to get pounced on by an exuberant catgirl. Kat starts to playfully tickle her prey, incapacitating the poor, " +
					"naked girl with uncontrollable fits of laughter. You stand up, brush off your butt, and leisurely walk towards your helpless opponent to finish her off.<p>";
		}
	}
	
	public String watched(Combat c, Character target, Character viewer){
			if(target.hasDick()){
				return "You decide to remain hidden and watch how the remainder of the fight plays out. Kat seems to be quite hyped up already, the lithe girl bounding and bouncing around "+target.name()+" like a runaway pinball, grinning all the while. "+target.name()+" is left spinning in place, her exhaustion and desperation growing by the moment as she tries gambit after gambit, resulting in a few close calls for Kat, but nothing solid enough to shift the momentum of the fight.<p>"
						+ "One misstep later and the fight is over, "+target.name()+" missing her footing and Kat immediately capitalizing on her mistake, the feline launching herself in a wild pounce against "+target.name()+"'s stomach, sending the hapless girl down to the ground. You can't help but smile, having been on the receiving end of Kat's signature pounces a few times yourself, the feline straddling "+target.name()+"'s waist to keep her pinned while Kat slips a hand between her opponent's thighs.<p>"
						+ ""+target.name()+" squeezes her legs together, trying to stave off the seemingly inevitable end to the match, but with Kat fully given over to her feral self "+target.name()+"'s resistance just draws a wicked grin to the catgirl's lips, her focus shifting as she trails her free hand up "+target.name()+"'s side, making the girl squirm beneath her before letting out a gasp of pleasure as Kat's wandering touch closes over "+target.name()+"'s left breast.<p>"
						+ "Kat's tail lashes back and forth through the air behind her as she kneads at "+target.name()+"'s breast, the helpless girl's shuddering breaths punctuated by increasingly sweet moans as Kat grinds her soft palm down against "+target.name()+"'s stiff nipple. Kat seems to be completely caught up in teasing "+target.name()+", the feline panting a little herself as she slowly rolls her hips, grinding and humping atop "+target.name()+"'s waist as she kneads ever more eagerly over "+target.name()+"'s bosom. The constant flow of pleasure gradually erodes "+target.name()+"'s remaining stamina, her legs trembling weakly before finally giving way, Kat seizing the opportunity to slide her hand down, fingers pressing down over "+target.name()+"'s glistening petals as the feline lets out a triumphant mrowl.<p>"
						+ ""+target.name()+" tries to squeeze her thighs together once more as a last defense but it's already too late, her hips jerking helplessly in time with Kat's touch as the feline glides her fingers up and down over "+target.name()+"'s folds. Kat grinds her middle finger in tight circles over "+target.name()+"'s clit, making the helpless girl let out a cry of pleasure as her entire body locks up, back arcing up off the ground as she hits her climax.<p>"
						+ "Even with her victory complete Kat doesn't relent, fingers working back and forth against "+target.name()+"'s clit as the catgirl humps and grinds impatiently over "+target.name()+"'s waist, a frustrated mrowl welling up from deep inside the feline's chest. "+target.name()+" can only pant and moan as Kat toys with her sensitive body until the feline finally turns around, leg swinging up to give you a brief glimpse of Kat's sex, nectar overflowing from her petals, before she straddles "+target.name()+"'s head and pushes herself down against "+target.name()+"'s lips.<p>"
						+ "Kat's tail goes straight up in the air and she lets out a pleasure-soaked meow as "+target.name()+" seizes the opportunity to pay the feline back for her relentless teasing, reaching up to tightly grip Kat's thighs as she pushes her tongue into the catgirl's greedy sex. Despite the pleasure writ large over Kat's face, her meows don't quite lose that needy edge as she humps wildly back against "+target.name()+"'s lips, looking for all the world like she's trying to ride her opponent's tongue. Skilled though she is, "+target.name()+"'s mouth isn't quite enough to assuage that feral ache deep within the feline's core, even as each pressing lick makes Kat's entire body shiver with pleasure.<p>"
						+ "It only takes a moment before the catgirl's focus lands on "+target.name()+"'s achingly stiff shaft, a supremely"
						+ "pleased purr bubbling up from Kat's throat as she leans forward, wrapping one hand around the base of"
						+ ""+target.name()+"'s shaft as she ducks down to plant a soft kiss against the tip. "+target.name()+" squirms beneath the"
						+ "delighted feline and makes a sudden attempt to wrest control back into her favor, sucking hard on the"
						+ "feline's clit as she tries to roll on top. "+target.name()+" seems more interested in enjoying the lewd embrace"
						+ "than escaping it, her rather enthusiastic response to the catgirl's actions making Kat let out a surprised"
						+ "gasp of pleasure.<p>"
						+ "Kat glances over her shoulder, a soft growl of irritation humming in her chest, and gives her hips a few"
						+ "short jerks, grinding down against "+target.name()+"'s lips as if she's batting an unruly mouse back into"
						+ "submission. "+target.name()+" seems to get the message, Kat's delighted grin returning as she turns back to her"
						+ "teasing play, once more kissing the tip of "+target.name()+"'s shaft before turning her head to the side and"
						+ "running a slow, wet lick down the side of her opponent's shaft. She lavishes warm licks around the root of"
						+ ""+target.name()+"'s rod for a moment, gliding her hand up to circle her palm over "+target.name()+"'s glans before"
						+ "bringing her head back up and planting another eager kiss against the back of "+target.name()+"'s cockhead.<p>"
						+ ""+target.name()+" seems quite happy to let Kat does as she pleases, rocking her hips just a little to nudge the"
						+ "head of her shaft against the catgirl's lips, Kat meeting the touches with eager, swirling licks over"
						+ ""+target.name()+"'s crown. "+target.name()+" thrusts up again, moaning against Kat's petals as she returns the feline's"
						+ "licks, but Kat has other plans as she pulls away from "+target.name()+"'s grip and scoots forward, tail thrashing"
						+ "eagerly behind her as she poises herself over "+target.name()+"'s rod.<p>"
						+ "You catch a glimpse of "+target.name()+"'s face as Kat shifts position, the girl seeming delighted with what the"
						+ "feline has in mind, but as she tries to sit up Kat leans back to plant a hand over her bosom and push her"
						+ "back down, the catgirl wrapping her other hand around "+target.name()+"'s shaft and grinding the head against"
						+ "her eager folds.<p>"
						+ ""+target.name()+" bites her bottom lip but seems to will herself to be patient, Kat not making her wait long at all"
						+ "as the feline, her position on top secured, drops her hips and in one smooth motion takes the full length of"
						+ ""+target.name()+"'s shaft inside her greedy sex. The pair let out shared cries of pleasure, a look of primal delight"
						+ "washing over Kat's face as she savors the feeling of having "+target.name()+"'s rod buried inside her.<p>"
						+ "You're at the perfect angle to watch as Kat lifts her hips, "+target.name()+"'s shaft glistening with the feline's"
						+ "lust as Kat barely makes it half way up "+target.name()+"'s rod before slamming eagerly back down onto that"
						+ "hard length, forcing a cry of pleasure from "+target.name()+" as the feline's sex drools nectar over the base of"
						+ "her rod. With the Cat in full control, Kat immediately settles into a wild pace, leaning back to brace both"
						+ "hands on "+target.name()+"'s chest as Kat's fills the air with her lustful meows.<p>"
						+ ""+target.name()+" tries to thrust up in an effort to gain some sort of control, but Kat's erratic pace never lets"
						+ ""+target.name()+" settle into any sort of rhythm, the girl's panting moans growing increasingly more ragged as"
						+ "she draws closer and closer to release. Seemingly heedless of "+target.name()+"'s pleasure, Kat doesn't break her"
						+ "pace in the slightest, her own climax seeming to come out of nowhere as she throws her head back with a"
						+ "wild, ecstatic cry, her sex squeezing down tightly around "+target.name()+"'s rod even as the catgirl keeps right"
						+ "on riding her.<p>"
						+ "Unable, and most likely unwilling, to hold back against the sudden blast of pleasure, "+target.name()+" lets out a"
						+ "cry of her own, her shaft throbbing visibly as she pumps her release into Kat's greedy sex. Only when she"
						+ "feels the heat of "+target.name()+"'s release filling her does Kat finally start to slow down, panting and meowing"
						+ "sweetly as she grinds on "+target.name()+"'s shaft with slow, short strokes, coaxing out every last drop of the"
						+ "girl's release.<p>"
						+ "The pair spend a few moments panting together, "+target.name()+" finally managing to struggle into a sitting"
						+ "position so she can plant a kiss against the side of Kat's neck. That seems to shock the Girl back to the"
						+ "surface, Kat almost jumping with a swift gasp before hastily climbing out of "+target.name()+"'s lap, the feline's"
						+ "cheeks flaming with embarrassment as she makes a hasty retreat, leaving "+target.name()+" to get back to her"
						+ "feet and walk away in turn, laughing softly as she goes.";
			}else{
				return "You decide to remain hidden and watch how the remainder of the fight plays out. Kat seems to be quite hyped up already, the lithe girl bounding and bouncing around "+target.name()+" like a runaway pinball, grinning all the while. "+target.name()+" is left spinning in place, her exhaustion and desperation growing by the moment as she tries gambit after gambit, resulting in a few close calls for Kat, but nothing solid enough to shift the momentum of the fight.<p>"
						+ "One misstep later and the fight is over, "+target.name()+" missing her footing and Kat immediately capitalizing on her mistake, the feline launching herself in a wild pounce against "+target.name()+"'s stomach, sending the hapless girl down to the ground. You can't help but smile, having been on the receiving end of Kat's signature pounces a few times yourself, the feline straddling "+target.name()+"'s waist to keep her pinned while Kat slips a hand between her opponent's thighs.<p>"
						+ ""+target.name()+" squeezes her legs together, trying to stave off the seemingly inevitable end to the match, but with Kat fully given over to her feral self "+target.name()+"'s resistance just draws a wicked grin to the catgirl's lips, her focus shifting as she trails her free hand up "+target.name()+"'s side, making the girl squirm beneath her before letting out a gasp of pleasure as Kat's wandering touch closes over "+target.name()+"'s left breast.<p>"
						+ "Kat's tail lashes back and forth through the air behind her as she kneads at "+target.name()+"'s breast, the helpless girl's shuddering breaths punctuated by increasingly sweet moans as Kat grinds her soft palm down against "+target.name()+"'s stiff nipple. Kat seems to be completely caught up in teasing "+target.name()+", the feline panting a little herself as she slowly rolls her hips, grinding and humping atop "+target.name()+"'s waist as she kneads ever more eagerly over "+target.name()+"'s bosom. The constant flow of pleasure gradually erodes "+target.name()+"'s remaining stamina, her legs trembling weakly before finally giving way, Kat seizing the opportunity to slide her hand down, fingers pressing down over "+target.name()+"'s glistening petals as the feline lets out a triumphant mrowl.<p>"
						+ ""+target.name()+" tries to squeeze her thighs together once more as a last defense but it's already too late, her hips jerking helplessly in time with Kat's touch as the feline glides her fingers up and down over "+target.name()+"'s folds. Kat grinds her middle finger in tight circles over "+target.name()+"'s clit, making the helpless girl let out a cry of pleasure as her entire body locks up, back arcing up off the ground as she hits her climax.<p>"
						+ "Even with her victory complete Kat doesn't relent, fingers working back and forth against "+target.name()+"'s clit as the catgirl humps and grinds impatiently over "+target.name()+"'s waist, a frustrated mrowl welling up from deep inside the feline's chest. "+target.name()+" can only pant and moan as Kat toys with her sensitive body until the feline finally turns around, leg swinging up to give you a brief glimpse of Kat's sex, nectar overflowing from her petals, before she straddles "+target.name()+"'s head and pushes herself down against "+target.name()+"'s lips.<p>"
						+ "Kat's tail goes straight up in the air and she lets out a pleasure-soaked meow as "+target.name()+" seizes the opportunity to pay the feline back for her relentless teasing, reaching up to tightly grip Kat's thighs as she pushes her tongue into the catgirl's greedy sex. Despite the pleasure writ large over Kat's face, her meows don't quite lose that needy edge as she humps wildly back against "+target.name()+"'s lips, looking for all the world like she's trying to ride her opponent's tongue. Skilled though she is, "+target.name()+"'s mouth isn't quite enough to assuage that feral ache deep within the feline's core, even as each pressing lick makes Kat's entire body shiver with pleasure.<p>"
						+ "Slowly, "+target.name()+"'s skilled ministrations cause Kat's desire for pleasure to override her more animal instincts, the feline's moans growing rich and lusty as she settles into a more controlled rhythm, pressing herself down against "+target.name()+"'s lips each time the girl slides her tongue over Kat's tender petals. With her eyes clearing just a little, Kat leans forward to return the gesture, back arching and tail swishing through the air as she ducks down between "+target.name()+"'s thighs, a slow lick over her opponent's clit drawing a muffled moan from "+target.name()+".<p>"
						+ "Kat wraps one arm under "+target.name()+"'s right thigh, brushing slow strokes up and down "+target.name()+"'s leg with her other hand as she trails her fingers over her opponent's glistening folds. You have an almost perfect view of the action as Kat presses two fingers between "+target.name()+"'s petals, making the girl tremble beneath her as "+target.name()+"'s sex greedily swallows up the catgirl's fingers.<p>"
						+ "With the pair fully entwined, Kat starts to slowly pump her fingers in and out of "+target.name()+"'s sex, glistening nectar soon coating the catgirl's hand as "+target.name()+"'s legs tremble, her cries of pleasure muffled against Kat's netherlips. However, "+target.name()+" doesn't seem willing to let Kat so easily toy with her a second time, her constant blend of firm licks and suckling kisses over the feline's sex causing Kat to soon add her own sweet meows of pleasure to the night air.<p>"
						+ "With each pleasure feeding into the next it's not long before the pair reach a climactic finish, Kat pressing her fingers deep into "+target.name()+"'s slick passage as the feline pushes herself down against "+target.name()+"'s lips, both of them letting out trembling cries of pleasure as they hit a shared release.<p>"
						+ "The pair lay there for a moment, both struggling to recover from the pleasant afterglow of their embrace. Unsurprisingly, it's Kat who recovers first, shakily sitting up and taking a moment to lick her fingers clean before climbing off "+target.name()+" and scampering off, leaving the other girl to pick herself up off the ground and make her own unsteady exit.";
			}
	}
	@Override
	public String startBattle(Character opponent) {
		if(character.getGrudge()!=null){
			switch(character.getGrudge()){
			case feral:
				return "Kat approaches you with a confident swagger and her tail swaying slowly. She's clearly in cat mode already. "
						+ "It looks like someone already warmed her up for you.<p>"
						+ "<i>\"Nyot quite. I'm just myaking an effort to embrace my feline side. I'm gonnya go all out from the start.\"</i>";
			case predatorinstincts:
				return "Kat stalks you with a dangerous expression. Despite her cute features and small build, you start to feel like a prey animal faced "
						+ "by a predator.<p>"
						+ "<i>\"I'll catch you this time, and I won't let you go until you cum!\"</i>";
			case landsonfeet:
				return "Kat leaps toward you,  planting a light kiss on your lips. Before you can respond, she backflips "
						+ "out of reach, landing gracefully on one foot. She's even more nimble than you realized.<p>"
						+ "<i>\"Cats always land on their feet. I'll be the one on top this time.\"</i>";
			default:
				break;
			
			}
		}
		if(character.nude()){
			if(character.getArousal().percent()>=50){
				return "Kat approaches you, naked and unashamed. She parts her slick nether lips to show you her arousal.<p>"
						+ "<i>\"You showed up at just the right time, nya. Nyow you're gonnya satisfy me.\"</i>";
			}else{
				return "Kat blushes deep red as she tries to preserve her modesty. <i>\"I was hoping to get dressed before you caught me. "
						+ "Well, let's hurry up and start before this gets any more embarrassing.\"</i>";
			}
		}
		if(opponent.pantsless()){
			return "Kat stares at your dangling penis until you cough to get her attention. <i>\"Nya!? Oh, sorry.\"</i> She blushes a bit and "
					+ "regains her focus. <i>\"Cats are easily distracted by swinging things. Girls are also easily distracted by... certain things...\"</i>";
		}
		if(character.getArousal().percent()>=50){
			
		}
		if(character.getAffection(opponent)>=30){
			return "Kat smiles and lets out a quiet purr when she sees you. She fidgets shyly with her tail, looking more like a girl waiting for a date "
					+ "than someone preparing to fight.<p>"
					+ "<i>\"Sorry, I was just looking forward to this. I almost don't care who wins.\"</i>";
		}
		return "Kat looks a bit nervous, but her tail wags slowly in anticipation. <i>\"Let's have some fun-nya.\"</i>";
	}
	@Override
	public boolean fit() {
		return (!character.nude()&&character.getStamina().percent()>=50)||character.getArousal().percent()>50;
	}
	@Override
	public String night() {
		return "You walk back to your dorm after the match, but something is bothering you the whole way. You feel like you're being watched, but there's no menace to the sensation.<p>" +
				"You hear a rustling behind you and look back just in time to see someone duck behind a bush. 'Someone'.... The cat ears on that hat make your stalker's identity " +
				"pretty obvious. You pretend you didn't see anything and continue toward your dorm.<p>"
				+ "So, a stray Kat is following you home tonight. It would make more sense for her " +
				"to just come out so you can walk together, but you know how shy she can be about things like this. On the other hand, she is following you back to your room at night " +
				"with obvious implications, so... does that count as being forward? <p>"
				+ "You reach the door to your dorm building and look back to see Kat clumsily attempting to hide behind a " +
				"tree.<p>"
				+ "Oh good grief. How long is planning to stay hidden? The door to the building locks automatically, so if you go inside she'll be stuck out here. Besides, the closer " +
				"you get to your room, the harder it's going to be for her to work up to courage to approach you.<p>"
				+ "You walk over to where she's hiding and she freezes in panic. Before she " +
				"can run off, you catch her and gently pat her on the head.<p>"
				+ "It's starting to get chilly out here. She should just come inside with you. Kat blushes furiously, but looks " +
				"delighted as you lead her to your room.";
	}
	public void advance(int rank){
		if(rank >= 3 && !character.has(Trait.tailmastery)){
			character.add(Trait.tailmastery);
		}
		if(rank >= 2 && !character.has(Trait.furaffinity)){
			character.add(Trait.furaffinity);
		}
	}
	@Override
	public NPC getCharacter() {
		return character;
	}
	
	public boolean checkMood(Emotion mood, int value) {
		if(character.is(Stsflag.feral)){
			if(!character.has(Trait.shameless)){
				character.add(Trait.shameless);
				character.remove(Trait.shy);
				character.strategy.put(Emotion.hunting, 5);
			}
			switch(mood){
			case horny:
				return value>=30;
			case nervous:
				return value>=80;
			default:
				return value>=50;
			}			
		}
		else{
			if(!character.has(Trait.shy)){
				character.add(Trait.shy);
				character.remove(Trait.shameless);
				character.strategy.put(Emotion.hunting, 1);
			}
			switch(mood){
			case nervous: case horny:
				return value>=30;
			case angry:
				return value>=80;
			default:
				return value>=50;
			}
		}
	}
	@Override
	public float moodWeight(Emotion mood) {
		if(character.is(Stsflag.feral)){
			switch(mood){
			case horny:
				return 1.2f;
			case nervous:
				return .7f;
			default:
				return 1f;
			}			
		}
		else{
			switch(mood){
			case nervous: case horny:
				return 1.2f;
			case angry:
				return .7f;
			default:
				return 1.2f;
			}
		}
	}
	@Override
	public String image() {
		return "assets/kat_"+ character.mood.name()+".jpg";
	}
	@Override
	public void pickFeat() {
		ArrayList<Trait> available = new ArrayList<Trait>();
		for(Trait feat: Global.getFeats()){
			if(!character.has(feat)&&feat.req(character)){
				available.add(feat);
			}
		}
		if(available.isEmpty()){
			return;
		}
		character.add((Trait) available.toArray()[Global.random(available.size())]);
	}
	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public CommentGroup getComments() {
		CommentGroup comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Nya! Cum in me now!\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Ooohnya! But I'm on top! I should win!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Yes! Fill me! Breed me~!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Nnnya! Oh! Fuck, breed, cum, NYAAA!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"Ooh! Are you gonnya fill up my ass?\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Nya! And they ca-AAH-ll me an animalnya!\"</i>");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Nyanyanya! I'm going to win now!\"</i>");
		comments.put(CommentSituation.SELF_BOUND, "<i>\"Please let me go! I'll do anyathing!\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"Ah! Put it in! Put it in!\"</i>");
		comments.put(CommentSituation.PIN_SUB_LOSE, "<i>\"Ah! Put it in! Put it in!\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"Are you in heat nyow too? I can help with that!\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"Nyaa~... Please! I need it!\"</i>");
		comments.put(CommentSituation.SELF_SHAMED, "<i>\"Don't look! Don't look! Don't look!\"</i>");
		comments.put(CommentSituation.OTHER_CHARMED, "<i>\"Nya? Can I play with you a little? purretty please?\"</i>");
		comments.put(CommentSituation.SELF_BUSTED, "Kat goes cross-eyed and squeezes her knees together protectively.  She lets out a long, low growl from the back of her throat that slowly rises in pitch, until it becomes a pitiful whimper.");
		return comments;
	}
	@Override
	public int getCostumeSet() {
		return 1;
	}
	@Override
	public void declareGrudge(Character opponent, Combat c) {
		switch(Global.random(3)){
		case 2:
			character.addGrudge(opponent,Trait.feral);
			break;
		case 1:
			character.addGrudge(opponent,Trait.predatorinstincts);
			break;
		case 0:
			character.addGrudge(opponent,Trait.landsonfeet);
			break;
		default:
			break;
		}
	}
    @Override
    public void resetOutfit() {
        character.outfit[0].clear();
        character.outfit[1].clear();
        character.outfit[0].add(Clothing.bra);
        character.outfit[0].add(Clothing.Tshirt);
        character.outfit[1].add(Clothing.panties);
        character.outfit[1].add(Clothing.skirt);
    }
}
