package items;

import characters.Dummy;
import characters.NPC;
import combat.Combat;

import status.Dissolving;
import status.Drowsy;
import status.Horny;
import status.Hypersensitive;
import status.Oiled;
import status.Status;
import status.Stsflag;

import java.util.ArrayList;

import characters.Character;

public enum Flask implements Item {
	Lubricant	( "Lubricant",20,"Helps you pleasure your opponent, but makes her hard to hang on to","some ","oily",new Oiled(null),
			"The liquid clings to you and makes your whole body slippery.","She's covered in slick lubricant."	),
	Aphrodisiac	( "Aphrodisiac",40,"Can be thrown like a 'horny bomb'","an ","sweet-smelling",new Horny(null,6,3),
			"An unnatural warmth spreads through your body and gathers in your dick like a fire.","For a second, she's just surprised, but gradually a growing desire starts to make her weak in the knees"	),
	Sedative	( "Sedative",25,"Tires out your opponent, but can also make her numb","a ","milky",new Drowsy(null,3),
			"The fog seems to fill your head and your body feels heavy.","She stumbles for a moment, trying to clear the drowsiness from her head."),
	SPotion		( "Sensitivity Flask",25,"Who knows what's in this stuff, but it makes any skin it touches tingle","a ","fizzy",new Hypersensitive(null),
			"Your skin becomes hot, but goosebumps appear anyway. Even the air touching your skin makes you shiver.","She shivers as it takes hold and heightens her sense of touch."),
	DisSol		( "Dissolving Solution",30,"Destroys clothes, but completely non-toxic","a ","clear",new Dissolving(null),
			"Your clothes start to disintegrate where ever the liquid lands.", "Her clothes start to vanish, exposing more of her soft skin."),
	PAphrodisiac	( "Potent Aphrodisiac",400,"More like a 'horny atomic bomb'","a ","intoxicating",new Horny(null,12,3),
			"You feel momentarily light-headed as all the blood in your body rushes to your penis.","She flushes deeply and instinctively reaches down to touch herself. She manages to restrain herself, but just barely."	),

	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	private String color;
	private Status effect;
	private String dealtext;
	private String receivetext;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public String getFullDesc(Character owner) {
		return getDesc();
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String getFullName(Character owner){
		return getName();
	}
	public String pre(){
		return prefix;
	}

	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	public String getColor(){
		return color;
	}
	public Status effect(){
		return effect;
	}
	public String getText(Character affected){
		if(affected.human()){
			return receivetext;
		}else{
			return dealtext;
		}
	}
	public boolean canUse(Combat c, Character user, Character target){
		switch(this){
		case Lubricant: 
			return target.nude()&&!target.is(Stsflag.oiled);
		case SPotion:
			return target.nude()&&!target.is(Stsflag.hypersensitive);
		case DisSol:
			return !target.nude();
		default:
			return true;
		}
	}
	@Override
	public Boolean listed() {
		return true;
	}
	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<Item>();
	}
	
	private Flask( String name, int price, String desc,String prefix,String color,Status effect, String receivetext, String dealtext )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
		this.color = color;
		this.effect = effect;
		this.dealtext = dealtext;
		this.receivetext = receivetext;
	}
}
