package items;

import characters.Trait;
import characters.Character;

public enum Clothing implements Loot{
	none		("",ClothingType.SPECIAL,0,"",Trait.none,Trait.none,0),
	//Outerwear
	jacket		("jacket","Jacket",ClothingType.TOPOUTER,2,"a ",Trait.none,Trait.none,400),
	cloak		("cloak","Mystic Cloak",ClothingType.TOPOUTER,2,"a ",Trait.mystic,Trait.none,750), 
	labcoat		("lab coat","Lab Coat",ClothingType.TOPOUTER,3,"a ",Trait.geeky,Trait.none,750),
	trenchcoat	("trenchcoat","Trenchcoat",ClothingType.TOPOUTER,7,"a ",Trait.none,Trait.bulky,1000),
	blazer		("blazer","Blazer",ClothingType.TOPOUTER,3,"a ",Trait.none,Trait.none,750),
	windbreaker	("windbreaker","Windbreaker",ClothingType.TOPOUTER,1,"a ",Trait.none,Trait.none,500),
	halfcloak	("half-cloak","Ninja's Half-Cloak",ClothingType.TOPOUTER,2,"a ",Trait.stealthy,Trait.none,800),
	furcoat		("furcoat","Fur Coat",ClothingType.TOPOUTER,3,"a ",Trait.furry,Trait.none,750),
	techarmor	("tech armor","High-Tech Armor",ClothingType.TOPOUTER,10,"",Trait.geeky,Trait.stylish,3000),
	noblecloak	("noble cloak","Noble Cloak",ClothingType.TOPOUTER,5,"a ",Trait.none,Trait.stylish,2000),
	
	//Top
	shirt		("shirt","Simple Shirt",ClothingType.TOP,5,"a ",Trait.none,Trait.none,75),
	tanktop		("tank top","Tank Top",ClothingType.TOP,1,"a ",Trait.none,Trait.none,75),
	Tshirt		("T-shirt","T-Shirt",ClothingType.TOP,5,"a ",Trait.none,Trait.none,100),
	blouse		("blouse","Blouse",ClothingType.TOP,5,"a ",Trait.none,Trait.none,60),
	sweatshirt	("sweatshirt","Sweatshirt",ClothingType.TOP,5,"",Trait.none,Trait.none,50),
	sweater		("sweater","Ugly Sweater",ClothingType.TOP,15,"a ",Trait.lame,Trait.none,200),
	gi			("gi","Kung-Fu Gi",ClothingType.TOP,5,"a ",Trait.martial,Trait.none,500),
	gothshirt	("goth shirt","Goth-Style Shirt",ClothingType.TOP,5,"a ",Trait.broody,Trait.none,700),
	silkShirt	("silk shirt","Stylish Silk Shirt",ClothingType.TOP,4,"a ",Trait.stylish,Trait.none,1200),
	latextop	("latex top","Kinky Latex Top",ClothingType.TOPUNDER,4,"",Trait.kinky,Trait.skimpy,1000),
	blackdress	("dress","Short Violet Dress",ClothingType.TOP,8,"a ",Trait.none,Trait.none,400),
	kunoichitop ("kunoichi top","Kunoichi Top",ClothingType.TOP,7,"a ",Trait.stealthy,Trait.skimpy,1200),
	shinobitop ("shinobi top","Shinobi Top",ClothingType.TOP,10,"a ",Trait.stealthy,Trait.none,1200),
	lbustier	("leather bustier","Leather Bustier",ClothingType.TOP,12,"a ",Trait.none,Trait.skimpy,900),
	chestpiece	("chest piece", "Chest Piece",ClothingType.TOP,10,"a ",Trait.geeky,Trait.none,2000),
	noblevest	("noble vest","Noble Vest",ClothingType.TOP,10,"a ",Trait.none,Trait.stylish,1500),
	legendshirt	("legendary T-shirt","Legendary T-Shirt",ClothingType.TOP,15,"the ",Trait.legend,Trait.none,5000),
	
	
	//Pants
	pants		("pants","Plain Pants",ClothingType.BOTOUTER,15,"",Trait.none,Trait.none,100),
	skirt		("skirt","Simple Skirt",ClothingType.BOTOUTER,12,"a ",Trait.none,Trait.none,60),
	shorts		("shorts","Shorts",ClothingType.BOTOUTER,15,"",Trait.none,Trait.none,100),
	jeans		("jeans","Blue Jeans",ClothingType.BOTOUTER,17,"",Trait.none,Trait.none,200),
	sweatpants	("sweatpants","Baggy Sweatpants",ClothingType.BOTOUTER,15,"",Trait.none,Trait.lame,50),
	miniskirt	("miniskirt","Skimpy Miniskirt",ClothingType.BOTOUTER,12,"a ",Trait.none,Trait.none,400),
	lminiskirt	("leather miniskirt","Leather Miniskirt",ClothingType.BOTOUTER,17,"a ",Trait.none,Trait.none,800),
	latexpants	("latex pants","Kinky Latex Pants",ClothingType.BOTOUTER,15,"",Trait.kinky,Trait.skimpy,1100),
	dresspants	("dress pants","Stylish Dress Pants",ClothingType.BOTOUTER,15,"",Trait.stylish,Trait.none,900),
	kungfupants	("kung-fu pants","Kung-Fu Pants",ClothingType.BOTOUTER,12,"",Trait.martial,Trait.none,600),
	gothpants	("goth pants","Black Goth Pants",ClothingType.BOTOUTER,15,"",Trait.broody,Trait.none,800),
	cutoffs		("cut-off jeans","Cut-Off Jean Shorts",ClothingType.BOTOUTER,16,"",Trait.none,Trait.none,150),
	ninjapants  ("ninja leggings","Ninja Leggings",ClothingType.BOTOUTER,12,"",Trait.stealthy,Trait.none,800),
	kilt		("kilt","Kilt",ClothingType.BOTOUTER,10,"a ",Trait.mighty,Trait.none,600),
	techpants	("tech leggings","High-Tech Leggings",ClothingType.BOTOUTER,15,"",Trait.geeky,Trait.stylish,1500),
	tdresspants	("tailored dress pants","Tailored Dress Pants",ClothingType.BOTOUTER,15,"",Trait.stylish,Trait.none,1500),
	
	//Underwear
	bra			("bra","Bra",ClothingType.TOPUNDER,5,"a ",Trait.none,Trait.none,200),
	panties		("panties","Panties",ClothingType.UNDERWEAR,15,"",Trait.none,Trait.none,250),
	boxers		("boxers","Boxer Shorts",ClothingType.UNDERWEAR,15,"",Trait.none,Trait.none,200),
	underwear	("underwear","Plain Underwear",ClothingType.UNDERWEAR,15,"",Trait.none,Trait.none,200),
	briefs		("briefs","Tight Briefs",ClothingType.UNDERWEAR,15,"",Trait.none,Trait.none,200),
	thong		("thong","Thong",ClothingType.UNDERWEAR,15,"a ",Trait.none,Trait.skimpy,400),
	bikinitop	("bikini top","Skimpy Bikini Top",ClothingType.TOPUNDER,1,"a ",Trait.none,Trait.skimpy,500),
	bikinibottoms	("bikini bottom","Skimpy Bikini Bottom",ClothingType.UNDERWEAR,5,"",Trait.none,Trait.skimpy,500),
	undershirt	("undershirt","White Undershirt",ClothingType.TOPUNDER,5,"a ",Trait.lame,Trait.none,60),
	camisole	("camisole","Thin Camisole",ClothingType.TOPUNDER,10,"a ",Trait.none, Trait.none, 700),
	lacebra		("lacy bra","Lacy Bra",ClothingType.TOPUNDER,5, "a ",Trait.none, Trait.none, 275),
	lacepanties	("lace panties","Lace Panties",ClothingType.UNDERWEAR,17,"",Trait.stylish,Trait.skimpy,1000),
	lacythong	("lacy thong","Lacy Thong",ClothingType.UNDERWEAR,7,"a ",Trait.stylish,Trait.skimpy,275),
	garters		("garters", "Lace Garters",ClothingType.BOTOUTER,5,"",Trait.none, Trait.skimpy,400),
	speedo		("banana hammock","Banana Hammock",ClothingType.UNDERWEAR,10,"a ",Trait.none,Trait.skimpy,500),
	loincloth	("loincloth","Loincloth",ClothingType.UNDERWEAR,5,"a ",Trait.none,Trait.accessible,50),
	nipplecover	("nipple covers","Nipple Covers",ClothingType.TOPUNDER,10,"",Trait.geeky,Trait.skimpy,1500),
	groincover	("groin cover","Groin Cover",ClothingType.UNDERWEAR,10,"a ",Trait.geeky,Trait.skimpy,1500),
	
	//Misc
	strapon		("strap-on dildo",ClothingType.UNDERWEAR,10,"a ",Trait.strapped,Trait.none,0),
	chastitybelt("chastity belt",ClothingType.UNDERWEAR,24,"a ",Trait.armored,Trait.indestructible,0),
	cup			("groin protector",ClothingType.UNDERWEAR,3,"a ",Trait.armored,Trait.bulky,1000),
	warpaint	("war paint",ClothingType.TOPUNDER,0,"",Trait.none,Trait.ineffective,120),
	crotchlesspanties("crotchless panties",ClothingType.UNDERWEAR,15,"",Trait.kinky,Trait.ineffective,1200),
	pouchlessbriefs("pouchless briefs",ClothingType.UNDERWEAR,15,"",Trait.kinky,Trait.ineffective,1200),
	
	//Temporary
	fabloincloth	("fabricated loincloth",ClothingType.UNDERWEAR,10,"",Trait.none,Trait.skimpy,0,true),
	fabwrap		("fabricated wrap",ClothingType.TOP,5,"a ",Trait.none,Trait.none,0,true),
	magethong	("magic thong",ClothingType.UNDERWEAR,10,"",Trait.none,Trait.skimpy,0,true),
	bharness	("leather harness",ClothingType.UNDERWEAR,15,"",Trait.kinky,Trait.skimpy,0,true),
	bstraps		("leather straps",ClothingType.TOPUNDER,5,"",Trait.kinky,Trait.skimpy,0,true),
	;
	private String name;
	private String proper;
	private int dc;
	private Trait buff;
	private String prefix;
	private Trait attribute;
	private int price;
	private ClothingType type;
	private boolean temp;
	
	private Clothing(String name, int dc, String prefix){
		this.name=name;
		this.dc=dc;
		this.prefix=prefix;
		this.temp=false;
	}
	private Clothing(String name, ClothingType type,int dc, String prefix, Trait buff, Trait attribute, int price){
		this(name, name, type, dc, prefix, buff, attribute, price);
	}
	private Clothing(String name, String proper,ClothingType type,int dc, String prefix, Trait buff, Trait attribute, int price){
		this(name,dc,prefix);
		this.proper = proper;
		this.buff=buff;
		this.attribute=attribute;
		this.price=price;
		this.type = type;
	}

	private Clothing(String name, ClothingType type,int dc, String prefix, Trait buff, Trait attribute, int price, boolean temp){
		this(name,type,dc,prefix,buff,attribute,price);
		this.temp=temp;
	}
	public String getName(){
		return name;
	}
	public String getProperName(){
		return proper;
	}
	public String getFullDesc(){
		String desc = "";
		if(this==none){
			return "Nothing";
		}
		desc = proper+" ("+dc+")";
		if(this.buff!=Trait.none || this.attribute!=Trait.none){
			desc = desc +" "+getSpecialDescription();
		}
		return desc;
	}
	public int dc(){
		return dc;
	}
	public String pre(){
		return prefix;
	}
	public String getSpecialDescription(){
		String desc = "";
		if(this.buff != Trait.none){
			desc = desc+this.buff.getDesc();
			if(this.attribute != Trait.none){
				desc = desc+", "+this.attribute.getDesc();
			}
		}else{
			if(this.attribute != Trait.none){
				desc = desc+", "+this.attribute.getDesc();
			}else{
				desc = "No special properties";
			}
		}
		return desc;
	}
	public boolean adds(Trait test){
		return test==buff;
	}
	public Trait attribute(){
		return attribute;
	}
	public ClothingType getType(){
		return this.type;
	}
	public int getPrice(){
		return this.price;
	}
	public boolean isTemp(){
		return this.temp;
	}
	public float stripOffenseBonus(Character attacker){
		float result = 1f;
		if(attacker.has(Trait.bramaster)&&(type==ClothingType.TOP||type==ClothingType.TOPOUTER||type==ClothingType.TOPUNDER)){
			result *= 2f;
		}
		if(attacker.has(Trait.pantymaster)&&(type==ClothingType.BOTOUTER||type==ClothingType.UNDERWEAR)){
			result *= 2f;
		}
		return result;
	}
	@Override
	public void pickup(Character owner) {
		if(!owner.has(this)){
			owner.gain(this);
		}
	}
}
