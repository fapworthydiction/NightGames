package items;

import java.util.ArrayList;

import characters.Character;

public enum Toy implements Item {
	Dildo		( "Dildo",250,"Big rubber cock: not a weapon","a "	),
	Crop		( "Riding Crop",200,"Delivers a painful sting to instill discipline","a "	),
	Onahole		( "Onahole",300,"An artificial vagina, but you can probably find some real ones pretty easily","an "	),
	Tickler		( "Tickler",300,"Tickles and pleasures your opponent's sensitive areas","a "	),
	Strapon		( "Strap-on Dildo",600,"Penis envy much?","a "),
	ShockGlove	( "Shock Glove",800,"Delivers a safe, but painful electric shock", "a " ),
	Aersolizer	( "Aerosolizer",500, "Turns a liquid into an unavoidable cloud of mist", "an "),
	Dildo2		( "Sonic Dildo",2000,"Apparently vibrates at the ideal frequency to produce pleasure","a "	),
	Crop2		( "Hunting Crop",1500,"Equipped with the fearsome Treasure Hunter attachment","a "	),
    Crop3       ( "Mistress Crop",10000,"An exquisitely crafted riding crop that looks as expensive as it is painful","a "),
	Onahole2	( "Wet Onahole",3000,"As hot and wet as the real thing","an "	),
	Tickler2	( "Enhanced Tickler",3000,"Coated with a substance that can increase sensitivity","an "	),
	Strapon2	( "Flex-O-Peg",2500,"A more flexible and versatile strapon with a built in vibrator","the patented "),
	Excalibur	( "Sexcalibur",1000,"A highly customizable toy ",""),
	Paddle		( "Paddle",1000,"","a "),
	AnalBeads	( "Anal Beads",1000,"","a "),
	nippleclamp	( "Nipple Clamps", 1000, "",""),
	bloodhound  ("Bloodhound", 2000, "An app that gives you your opponents' positions at set intervals","")

	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	
	private int price;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public String getFullDesc(Character owner){
		String result = getDesc();
		for(Attachment upgrade: getRenames()){
			if(owner.has(upgrade)){
				result = upgrade.getDesc();
				break;
			}
		}		
		for (Attachment addon: getAddons()){
			if(owner.has(addon) && addon.getDesc()!=""){
				result += ". "+addon.getDesc();
			}
		}
		return result;
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String getFullName(Character owner){
		for(Attachment upgrade: getRenames()){
			if(owner.has(upgrade)){
				return upgrade.getFullName(owner);
			}
		}
		return getName();
	}
	private ArrayList<Attachment> getRenames(){
		ArrayList<Attachment> renames = new ArrayList<Attachment>();
		switch(this){
		case Excalibur:
			renames.add(Attachment.Excalibur4);
			renames.add(Attachment.Excalibur3);
			renames.add(Attachment.Excalibur2);
			break;
		case Dildo:
		case Dildo2:
			renames.add(Attachment.DildoSlimy);
			break;
		case Onahole:
		case Onahole2:
			renames.add(Attachment.OnaholeSlimy);
			break;
		case Crop:
		case Crop2:
			renames.add(Attachment.CropShocker);
			break;
		case Tickler:
		case Tickler2:
			renames.add(Attachment.TicklerFluffy);
			break;
		}
		return renames;
	}
	private ArrayList<Attachment> getAddons(){
		ArrayList<Attachment> addons = new ArrayList<Attachment>();
		switch(this){
		case Excalibur:
			addons.add(Attachment.ExcaliburArcane);
			addons.add(Attachment.ExcaliburScience);
			addons.add(Attachment.ExcaliburKi);
			addons.add(Attachment.ExcaliburDark);
			addons.add(Attachment.ExcaliburFetish);
			addons.add(Attachment.ExcaliburAnimism);
			addons.add(Attachment.ExcaliburNinjutsu);
			break;
		case Dildo:
		case Dildo2:
			addons.add(Attachment.DildoLube);
			break;
		case Onahole:
		case Onahole2:
			addons.add(Attachment.OnaholeVibe);
			break;
		case Crop:
		case Crop2:
			addons.add(Attachment.CropKeen);
			break;
		case Tickler:
		case Tickler2:
			addons.add(Attachment.TicklerPheromones);
			break;
		}
		return addons;
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	@Override
	public Boolean listed() {
		return true;
	}
	private void linkAttachments(){
		
	}
	@Override
	public ArrayList<Item> getRecipe() {
		ArrayList<Item> recipe = new ArrayList<Item>();
		return recipe;
	}
	private Toy( String name, int price, String desc,String prefix )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
		
	}
}
