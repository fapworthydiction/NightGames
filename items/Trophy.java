package items;

import java.util.ArrayList;

import characters.Character;

public enum Trophy implements Item {
	CassieTrophy	("Cassie's Panties","Cute and simple panties"),
	MaraTrophy	("Mara's Underwear","She wears boys underwear?"),
	AngelTrophy	("Angel's Thong","There's barely anything here"),
	JewelTrophy	("Jewel's Panties","Surprisingly lacy"),
	ReykaTrophy	("Reyka's Clit Ring","What else can you take from someone who goes commando?"),
	PlayerTrophy("Your Boxers","How did you end up with these?"),
	EveTrophy	("Eve's 'Panties'","Crotchless and of no practical use"),
	KatTrophy	("Kat's Panties","Cute pink panties. Unfortunately there's no cat motif"),
	YuiTrophy	("Yui's Panties","White and innocent, not unlike their owner"),
	MayaTrophy	("Maya's Panties","Black lace. Very sexy"),
	SamanthaTrophy	("Samantha's Thong","A lacy red thong, translucent in all but the most delicate areas."),
    ValerieTrophy("Valerie's Panties","Silky and high class." );
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public String getFullDesc(Character owner) {
		return getDesc();
	}
	public int getPrice(){
		return 0;
	}
	public String getName(){
		return name;
	}
	@Override
	public String getFullName(Character owner) {
		return getName();
	}
	public String pre(){
		return "";
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	@Override
	public Boolean listed() {
		return true;
	}
	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<Item>();
	}
	private Trophy( String name, String desc )
	{
		this.name = name;
		this.desc = desc;
	}
}
