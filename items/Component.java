package items;

import java.util.ArrayList;

import characters.Character;

public enum Component implements Item {
	Tripwire	( "Trip Wire",10, "A strong wire used to trigger traps","a "	),
	Spring		( "Spring",20,	"A component for traps","a "	),
	Rope		( "Rope",15,"A component for traps","a "	),
	Phone		( "Phone",30,"A cheap disposable phone with a programable alarm","a "		),
	Sprayer		( "Sprayer",30,	"Necessary for making traps that use liquids","a "	),
	MSprayer	( "Mirco Sprayer",300,"A tiny, but reliable sprayer that can be hidden in a device","a "	),
	Totem		( "Fetish Totem",150,"A small penis shaped totem that can summon tentacles","a "),
	Capacitor	( "Prime Capacitor",700,"This high qulity capacitor can reliably store and discharge a significant electrical charge","a "),
	SlimeCore	( "Slime Core",500,"This semi-solid ball pulses as if it's alive","a "),
	Semen		( "Semen",0 ,"A small bottle filled with cum. Kinda gross", "a bottle of "),
	Battery		( "Battery",0,"Available energy to power electronic equipment","a "),
	Foxtail		( "Fox Tail", 500, "One of the tails of the mythical nine-tailed fox", "a "),
	Skin		( "Serpant Skin", 600, "Thin and flexible, but extremely tough","some "),
	HGMotor		( "Micro Motor",500,"This small motor would be a dramatic improvement for compatible store-bought toys","a "),
	Titanium	( "Titanium Alloy",1000,"A valuable crafting material. What could you use it for?","a block of "),
	DragonBone	( "Dragon Bone",3000,"Supposedly a long bone from a dragon. Not much use unless you're planning to forge a legendary weapon","a "),
	PBlueprint	( "Prototype Blueprint",4000,"Theoretical blueprint for the ultimate sex toy","a "),
	PVibrator	( "Prototype Vibrator",4000,"A critical component of the ultimate sex toy","a "),
	PHandle		( "Prototype Handle",4000,"A critical component of the ultimate sex toy","a "),
	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public String getFullDesc(Character owner) {
		return getDesc();
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String getFullName(Character owner){
		return getName();
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	private Component( String name, int price, String desc,String prefix )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
	@Override
	public Boolean listed() {
		return true;
	}
	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<Item>();
	}
}
