package actions;

import characters.Attribute;
import characters.Character;
import areas.Area;

public class Shortcut extends Move {
	public Shortcut(Area destination) {
		super(destination);
		name = "Shortcut("+destination.name+")";
	}
	public boolean usable(Character user) {
		return user.getPure(Attribute.Cunning)>=28;
	}
}
