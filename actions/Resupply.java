package actions;

import global.Global;
import global.Modifier;
import areas.NinjaStash;
import characters.Character;
import characters.State;
import global.Scheduler;

public class Resupply extends Action {

	public Resupply() {
		super("Resupply","Retrieve replacement clothes and check in with the officials, making you eligible to fight opponents who recently defeated you");
	}

	@Override
	public boolean usable(Character user) {
		return user.location().resupply()||user.location().hasStash(user);
	}

	@Override
	public Movement execute(Character user) {
		if(user.human()){
			if(Scheduler.getMatch().condition==Modifier.nudist){
				Global.gui().message("You check in so you're eligible to fight again, but you still don't get any clothes.");
			}
			else{
				Global.gui().message("You pick up a change of clothes and prepare to get back in the fray.");
			}
		}
		if(user.location().hasStash(user)){
			((NinjaStash)user.location().get(new NinjaStash(user))).collect(user);
		}
		user.state=State.resupplying;
		return Movement.resupply;
	}

	@Override
	public Movement consider() {
		return Movement.resupply;
	}

}
