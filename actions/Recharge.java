package actions;

import items.Component;
import items.Item;
import global.Global;

import characters.Attribute;
import characters.Character;

public class Recharge extends Action {

	public Recharge() {
		super("Recharge","Jett has a power supply in here that can rapidly recharge the battery in your multitool");
	}

	@Override
	public boolean usable(Character user) {
		return user.location().recharge()&&user.getPure(Attribute.Science)>0&&user.count(Component.Battery)<20;
	}

	@Override
	public Movement execute(Character user) {
		if(user.human()){
			Global.gui().message("You find a power supply and restore your batteries to full.");
		}
		user.chargeBattery();
		return Movement.recharge;
	}

	@Override
	public Movement consider() {
		return Movement.recharge;
	}

}
