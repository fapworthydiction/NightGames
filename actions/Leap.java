package actions;

import areas.Area;
import characters.Attribute;
import characters.Character;

public class Leap extends Move {
	public Leap(Area destination) {
		super(destination);
		name = "Ninja Leap("+destination.name+")";
	}
	public boolean usable(Character user) {
		return user.getPure(Attribute.Ninjutsu)>=5;
	}
}
