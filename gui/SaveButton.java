package gui;

import global.Global;
import global.SaveManager;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class SaveButton extends KeyableButton {

	public SaveButton() {
		super("Save");
		setFont(new Font("Georgia", 0, 18));
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SaveManager.save(false);
			}
		});
	}
}