package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class RunnableButton extends KeyableButton {
    private static final long serialVersionUID = 5435929681634872672L;
    private String text;
    private Runnable runnable;
    
    public RunnableButton(String text, Runnable runnable) {
        super(formatHTMLMultiline(text, ""));
        this.runnable = runnable;
        this.text = text;
        resetFontSize();

        addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				RunnableButton.this.runnable.run();
			}
		});
    }

    private void resetFontSize() {
        if (getText().contains("<br/>")) {
            setFont(new Font("Georgia", 0, 14));
        } else {
            setFont(new Font("Georgia", 0, 18));            
        }
    }

    private static String formatHTMLMultiline(String original, String hotkeyExtra) {
        // do not word wrap the hotkey extras, since it looks pretty bad.
        return String.format("<html><center>%s%s</center></html>", original, hotkeyExtra);
    }

    @Override
    public String getText() {
        return text;
    }

    public void setHotkeyTextTo(String string) {
        setText(formatHTMLMultiline(text, String.format(" [%s]", string)));
        resetFontSize();
    }
}