package gui;
import global.Global;
import global.Modifier;
import items.Clothing;

import java.util.ArrayList;
import java.util.HashMap;

import characters.Character;

import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import daytime.Activity;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class ClothesChangeGUI extends JPanel {
	private Character player;
	private Activity resume;
	private JPanel menuPane;
	private JPanel imgPane;
	private JLabel img;
	private ArrayList<Clothing> TopOut;
	private ArrayList<Clothing> TopMid;
	private ArrayList<Clothing> TopIn;
	private ArrayList<Clothing> BotOut;
	private ArrayList<Clothing> BotIn;
	private String noneString = "";
	private JComboBox<String> TOBox;
	private JComboBox<String> TMBox;
	private JComboBox<String> TIBox;
	private JComboBox<String> BOBox;
	private JComboBox<String> BIBox;
	private HashMap<String, Clothing> translator;
	private boolean freeze;
	
	public ClothesChangeGUI(Character player){
		freeze = true;
		this.player = player;
		translator = new HashMap<String,Clothing>();
		for (Clothing article: Clothing.values()){
			translator.put(article.getFullDesc(), article);
		}
		setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		
		menuPane = new JPanel();
		imgPane = new JPanel();
		img = new JLabel();
		imgPane.add(img);
		add(imgPane);
		add(menuPane);
		
		menuPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		TopOut = new ArrayList<Clothing>();
		TopMid = new ArrayList<Clothing>();
		TopIn = new ArrayList<Clothing>();
		BotOut = new ArrayList<Clothing>();
		BotIn = new ArrayList<Clothing>();
		
		JSeparator separator_1 = new JSeparator();
		//menuPane.add(separator_1);
		
		JLabel lblTopOuter = new JLabel("Outerwear");
		lblTopOuter.setHorizontalAlignment(SwingConstants.CENTER);
		lblTopOuter.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblTopOuter);
		
		TOBox = new JComboBox<String>();
		TOBox.setFont(new Font("Georgia", Font.PLAIN, 22));		
		menuPane.add(TOBox);
		TOBox.addItem(Clothing.none.getFullDesc());
		TOBox.setSelectedItem(Clothing.none.getFullDesc());
		TOBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!ClothesChangeGUI.this.freeze){
					loadImage();
				}
			}
		});
		
		JLabel lblTop = new JLabel("Tops");
		lblTop.setHorizontalAlignment(SwingConstants.CENTER);
		lblTop.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblTop);
		
		TMBox = new JComboBox<String>();
		TMBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(TMBox);
		TMBox.addItem(Clothing.none.getFullDesc());
		TMBox.setSelectedItem(Clothing.none.getFullDesc());
		TMBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!ClothesChangeGUI.this.freeze){
					loadImage();
				}
			}
		});
		
		JLabel lblTopInner = new JLabel("Undershirts");
		lblTopInner.setHorizontalAlignment(SwingConstants.CENTER);
		lblTopInner.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblTopInner);
		
		TIBox = new JComboBox<String>();
		TIBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(TIBox);
		TIBox.addItem(Clothing.none.getFullDesc());
		TIBox.setSelectedItem(Clothing.none.getFullDesc());
		TIBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!ClothesChangeGUI.this.freeze){
					loadImage();
				}
			}
		});
		
		JSeparator separator = new JSeparator();
		menuPane.add(separator);
		
		JLabel lblBottom = new JLabel("Pants");
		lblBottom.setHorizontalAlignment(SwingConstants.CENTER);
		lblBottom.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblBottom);
		
		BOBox = new JComboBox<String>();
		BOBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(BOBox);
		BOBox.addItem(Clothing.none.getFullDesc());
		BOBox.setSelectedItem(Clothing.none.getFullDesc());
		BOBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!ClothesChangeGUI.this.freeze){
					loadImage();
				}
			}
		});
		
		JLabel lblBottomInner = new JLabel("Underwear");
		lblBottomInner.setHorizontalAlignment(SwingConstants.CENTER);
		lblBottomInner.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblBottomInner);
		
		BIBox = new JComboBox<String>();
		BIBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(BIBox);
		BIBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!ClothesChangeGUI.this.freeze){
					loadImage();
				}
			}
		});
		
		JSeparator separator_2 = new JSeparator();
		//menuPane.add(separator_2);
		
		Box horizontalBox_2 = Box.createHorizontalBox();
		menuPane.add(horizontalBox_2);
		
		Component horizontalStrut = Box.createHorizontalStrut(200);
		horizontalBox_2.add(horizontalStrut);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClothesChangeGUI.this.player.outfit[0].clear();
				ClothesChangeGUI.this.player.outfit[1].clear();
				if(ClothesChangeGUI.this.TIBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[0].push(translator.get((String) ClothesChangeGUI.this.TIBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.TMBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[0].push(translator.get((String) ClothesChangeGUI.this.TMBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.TOBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[0].push(translator.get((String) ClothesChangeGUI.this.TOBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.BIBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[1].push(translator.get((String) ClothesChangeGUI.this.BIBox.getSelectedItem()));
				}
				if(ClothesChangeGUI.this.BOBox.getSelectedItem()!=Clothing.none.getFullDesc()){
					ClothesChangeGUI.this.player.outfit[1].push(translator.get((String) ClothesChangeGUI.this.BOBox.getSelectedItem()));
				}
				ClothesChangeGUI.this.player.change(Modifier.normal);
				Global.gui().removeClosetGUI();
				ClothesChangeGUI.this.resume.visit("Start");
			}
		});
		btnOk.setFont(new Font("Georgia", Font.PLAIN, 24));
		horizontalBox_2.add(btnOk);
		freeze = false;

	}
	public void loadImage(){
		img.setIcon(null);
		String imagepath = "/gui/assets/room/Room.jpg";
		BufferedImage room = null;
		
		try {
			URL imgurl = getClass().getResource(imagepath);
			if(imgurl != null){
				room = ImageIO.read(imgurl);
				
			}
		}catch (IOException localIOException9) {
		}
		Graphics2D g = room.createGraphics();
		
		Clothing TO = translator.get(TOBox.getSelectedItem());
		Clothing TM = translator.get(TMBox.getSelectedItem());
		Clothing TI = translator.get(TIBox.getSelectedItem());
		Clothing BO = translator.get(BOBox.getSelectedItem());
		Clothing BI = translator.get(BIBox.getSelectedItem());
		
		BufferedImage TOIMG = null;
		BufferedImage TMIMG = null;
		BufferedImage TIIMG = null;
		BufferedImage BIIMG = null;
		BufferedImage BOIMG = null;
		
		if(TO != Clothing.none){
			switch(TO){
			case cloak:
			case halfcloak:
				imagepath = "/gui/assets/room/TO_Cloak.png";
				break;
			case windbreaker:
				imagepath = "/gui/assets/room/TO_Windbreaker.png";
				break;
			case blazer:
				imagepath = "/gui/assets/room/TO_Blazer.png";
				break;
			case trenchcoat:
			
				imagepath = "/gui/assets/room/TO_Coat.png";
				break;
			case labcoat:
				imagepath = "/gui/assets/room/TO_LabCoat.png";
				break;
            case furcoat:
                imagepath = "/gui/assets/room/TO_FurCoat.png";
                break;
			default:
				imagepath = "/gui/assets/room/TO_Jacket.png";
			
			}
			try {
				URL imgurl = getClass().getResource(imagepath);
				if(imgurl != null){
					TOIMG = ImageIO.read(imgurl);
				}
			}catch (IOException localIOException9) {
			}
			if(TOIMG != null){
				g.drawImage(TOIMG,0,0,null);
			}
		}
		if(TI != Clothing.none){
			switch(TI){
                case lacebra:
                case bra:
                    imagepath = "/gui/assets/room/TI_Bra.png";
                    break;
                case undershirt:
				    imagepath = "/gui/assets/room/TI_Undershirt.png";
			}
			try {
				URL imgurl = getClass().getResource(imagepath);
				if(imgurl != null){
					TIIMG = ImageIO.read(imgurl);
				}
			}catch (IOException localIOException9) {
			}
			if(TIIMG != null){
				g.drawImage(TIIMG,0,0,null);
			}
		}
		if(TM != Clothing.none){
			switch(TM){
			case silkShirt:
				imagepath = "/gui/assets/room/TM_SilkShirt.png";
				break;
			case sweater:
				imagepath = "/gui/assets/room/TM_Sweater.png";
				break;
			case shirt:
				imagepath = "/gui/assets/room/TM_Shirt.png";
				break;
			case sweatshirt:
				imagepath = "/gui/assets/room/TM_SweatShirt.png";
				break;
			case gi:
				imagepath = "/gui/assets/room/TM_KungfuShirt.png";
				break;
			case legendshirt:
				imagepath = "/gui/assets/room/TM_LegendTShirt.png";
				break;
            case shinobitop:
                imagepath = "/gui/assets/room/TM_Ninja.png";
                break;
            case blouse:
                imagepath = "/gui/assets/room/TM_Blouse.png";
                break;
			default:
				imagepath = "/gui/assets/room/TM_TShirt.png";
			}
			try {
				URL imgurl = getClass().getResource(imagepath);
				if(imgurl != null){
					TMIMG = ImageIO.read(imgurl);
				}
			}catch (IOException localIOException9) {
			}
			if(TMIMG != null){
				g.drawImage(TMIMG,0,0,null);
			}
		}
		if(BI != Clothing.none){
			switch(BI){
			case boxers:
				imagepath = "/gui/assets/room/BI_Boxers.png";
				break;
			case cup:
				imagepath = "/gui/assets/room/BI_Cup.png";
				break;
            case loincloth:
                imagepath = "/gui/assets/room/BI_Loincloth.png";
                break;
            case pouchlessbriefs:
                imagepath = "/gui/assets/room/BI_PouchlessBriefs.png";
                break;
            case speedo:
                imagepath = "/gui/assets/room/BI_Speedo.png";
                break;
            case lacepanties:
            case panties:
                imagepath = "/gui/assets/room/BI_Panties.png";
                break;
			default:
				imagepath = "/gui/assets/room/BI_Briefs.png";
			}
			try {
				URL imgurl = getClass().getResource(imagepath);
				if(imgurl != null){
					BIIMG = ImageIO.read(imgurl);
				}
			}catch (IOException localIOException9) {
			}
			if(BIIMG != null){
				g.drawImage(BIIMG,0,0,null);
			}
		}
		if(BO != Clothing.none){
			switch(BO){
			case shorts:
				imagepath = "/gui/assets/room/BO_Shorts.png";
				break;
			case cutoffs:
				imagepath = "/gui/assets/room/BO_JeanShorts.png";
				break;
			case jeans:
				imagepath = "/gui/assets/room/BO_Jeans.png";
				break;
			case gothpants:
				imagepath = "/gui/assets/room/BO_Goth.png";
				break;
			case kungfupants:
				imagepath = "/gui/assets/room/BO_KungfuPants.png";
				break;
            case kilt:
                imagepath = "/gui/assets/room/BO_Kilt.png";
                break;
            case skirt:
                imagepath = "/gui/assets/room/BO_Skirt.png";
                break;
			default:
				imagepath = "/gui/assets/room/BO_Pants.png";
			}
			try {
				URL imgurl = getClass().getResource(imagepath);
				if(imgurl != null){
					BOIMG = ImageIO.read(imgurl);
				}
			}catch (IOException localIOException9) {
			}
			if(BOIMG != null){
				g.drawImage(BOIMG,0,0,null);
			}
		}
		g.dispose();
		img.setIcon(new ImageIcon(room));
	}
	public void update(Activity event){
		freeze = true;
		this.resume = event;
		TopOut.clear();
		TopMid.clear();
		TopIn.clear();
		BotOut.clear();
		BotIn.clear();
		TOBox.removeAllItems();
		TOBox.addItem(Clothing.none.getFullDesc());
		TOBox.setSelectedItem(Clothing.none.getFullDesc());
		TMBox.removeAllItems();
		TMBox.addItem(Clothing.none.getFullDesc());
		TMBox.setSelectedItem(Clothing.none.getFullDesc());
		TIBox.removeAllItems();
		TIBox.addItem(Clothing.none.getFullDesc());
		TIBox.setSelectedItem(Clothing.none.getFullDesc());
		BOBox.removeAllItems();
		BOBox.addItem(Clothing.none.getFullDesc());
		BOBox.setSelectedItem(Clothing.none.getFullDesc());
		BIBox.removeAllItems();

		for(Clothing article: player.closet){
			translator.put(article.getFullDesc(), article);
			switch(article.getType()){
			case TOPOUTER:
				TopOut.add(article);
				TOBox.addItem(article.getFullDesc());
				break;
			case TOP:
				TopMid.add(article);
				TMBox.addItem(article.getFullDesc());
				break;
			case TOPUNDER:
				TopIn.add(article);
				TIBox.addItem(article.getFullDesc());
				break;
			case BOTOUTER:
				BotOut.add(article);
				BOBox.addItem(article.getFullDesc());
				break;
			case UNDERWEAR:
				BotIn.add(article);
				BIBox.addItem(article.getFullDesc());
				break;
			}
		}
		
		for(Clothing article: player.outfit[0]){
			if(TopOut.contains(article)){
				TOBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[0]){
			if(TopMid.contains(article)){
				TMBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[0]){
			if(TopIn.contains(article)){
				TIBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[1]){
			if(BotOut.contains(article)){
				BOBox.setSelectedItem(article.getFullDesc());
			}
		}
		for(Clothing article: player.outfit[1]){
			if(BotIn.contains(article)){
				BIBox.setSelectedItem(article.getFullDesc());
			}
		}
		loadImage();
		freeze = false;
	}
}
