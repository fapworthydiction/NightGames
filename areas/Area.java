package areas;
import global.Global;
import global.Scheduler;
import status.Stsflag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Observable;

import actions.Movement;

import trap.Trap;


import combat.Encounter;

import characters.Character;


public class Area implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1372128249588089014L;
	public String name;
	public HashSet<Area> adjacent;
	public HashSet<Area> shortcut;
	public HashSet<Area> jump;
	public ArrayList<Character> present;
	public String description;
	public Encounter fight;
	public boolean alarm;
	public ArrayList<Deployable> env;
	public MapDrawHint drawHint;
	private Movement enumerator;
	private boolean pinged;
	
	public Area(String name,String description,Movement enumerator){
		this(name,description,enumerator,new MapDrawHint());
	}
	public Area(String name,String description,Movement enumerator,MapDrawHint drawHint){
		this.name=name;
		this.description=description;
		this.enumerator=enumerator;
		adjacent=new HashSet<Area>();
		shortcut=new HashSet<Area>();
		jump=new HashSet<Area>();
		present=new ArrayList<Character>();
		env=new ArrayList<Deployable>();
		alarm=false;
		fight=null;
		this.drawHint = drawHint;
	}
	public void link(Area adj){
		adjacent.add(adj);
	}
	public void shortcut(Area adj){
		shortcut.add(adj);
	}
	public void jump(Area adj){
		jump.add(adj);
	}
	public boolean open(){
		return enumerator==Movement.quad;
	}
	public boolean corridor(){
		return enumerator==Movement.bridge||enumerator==Movement.tunnel;
	}
	public boolean materials(){
		return enumerator==Movement.workshop||enumerator==Movement.storage;
	}
	public boolean potions(){
		return enumerator==Movement.lab||enumerator==Movement.kitchen;
	}
	public boolean bath(){
		return enumerator==Movement.shower||enumerator==Movement.pool;
	}
	public boolean resupply(){
		return enumerator==Movement.dorm||enumerator==Movement.union;
	}
	public boolean recharge(){
		return enumerator==Movement.workshop;
	}
	public boolean mana(){
		return enumerator==Movement.la;
	}
	public boolean ping(int perception){
		if(fight!=null){
			return true;
		}
		for(Character c:present){
			if(!c.stealthCheck(perception)||open()){
				return true;
			}
		}
		return alarm;
	}
	public boolean alarmTriggered(){
		return alarm;
	}
	public void enter(Character p){
		present.add(p);
		triggerDeployables(p);
	}
	public void triggerDeployables(Character p){
		Deployable found = getEnv();
		if(found!=null){
			found.resolve(p);
		}
	}
	public boolean encounter(Character p){
		if(fight!=null&&fight.getPlayer(1)!=p&&fight.getPlayer(2)!=p){
			p.intervene(fight, fight.getPlayer(1), fight.getPlayer(2));
		}
		else if(present.size()>1){
			for(Character opponent: Scheduler.getMatch().combatants){
				if(present.contains(opponent)&&opponent!=p){
					fight=new Encounter(p,opponent,this);
					return fight.spotCheck();
				}
			}			
		}
		return false;
	}
	public boolean opportunity(Character target,Trap trap){
		if(present.size()>1){
			for(Character opponent:present){
				if(opponent!=target){
					if(target.eligible(opponent)&&opponent.eligible(target)&&fight==null){
						fight=new Encounter(opponent,target,this);
						opponent.promptTrap(fight,target,trap);
						return true;
					}
				}
			}
		}
		remove(trap);
		return false;
	}
	public boolean humanPresent(){
		for(Character player: present){
			if(player.human()){
				return true;
			}
		}
		return false;
	}
	public void exit(Character p){
		present.remove(p);
	}
	public void endEncounter(){
		fight=null;
	}
	public Movement id(){
		return enumerator;
	}
	public Deployable getEnv(){
		if(env.isEmpty()){
			return null;
		}
		int priority = -1;
		Deployable highest = null;
		for(int i=0;i<env.size();i++){
			if(env.get(i).priority()>priority){
				priority = env.get(i).priority();
				highest = env.get(i);
			}
		}
		if(highest!=null){
			return highest;
		}
		return env.get(0);
	}
	public void place(Deployable thing){
		env.add(thing);
	}
	public void remove(Deployable triggered){
		env.remove(triggered);
	}
	public Deployable get(Deployable type){
		for(Deployable thing: env){
			if(thing.getClass()==type.getClass()){
				return thing;
			}
		}
		return null;
	}
	public Movement getEnum(){
		return this.enumerator;
	}
	public boolean hasStash(Character owner) {
		if(env.isEmpty()){
			return false;
		}
		for(int i=0;i<env.size();i++){
			if(env.get(i).getClass()==NinjaStash.class){
				if(env.get(i).owner()==owner){
					return true;
				}
			}
		}
		return false;
	}
	public boolean hasTrap(Character trapper) {
		if(env.isEmpty()){
			return false;
		}
		for(int i=0;i<env.size();i++){
			if(env.get(i).getClass()==Trap.class){
				if(env.get(i).owner()==trapper){
					return true;
				}
			}
		}
		return false;
	}
    public void setPinged(boolean b) {
        this.pinged = b;
    }

    public boolean isPinged() {
        return pinged;
    }
    public String toString(){
    	return name;
    }
}
