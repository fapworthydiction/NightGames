package areas;

import global.Flag;
import global.Global;

import items.Clothing;
import items.Component;
import items.Consumable;
import items.Flask;
import items.Item;
import items.Loot;
import items.Potion;

import java.util.ArrayList;
import java.util.HashMap;


import characters.Attribute;
import characters.Character;
import characters.State;
import characters.Trait;

public class Cache implements Deployable {
	private int dc;
	private Attribute test;
	private Attribute secondary;
	private ArrayList<Loot> reward;
	private HashMap<Loot,Integer> prizelist;
	public Cache(int rank){
		prizelist = new HashMap<Loot,Integer>();
		reward = new ArrayList<Loot>();
		dc = 10 * (rank+1);
		switch(Global.random(4)){
		case 3:
			test = Attribute.Seduction;
			secondary = Attribute.Dark;
			break;
		case 2:
			test = Attribute.Cunning;
			secondary = Attribute.Science;
			break;
		case 1:
			test = Attribute.Perception;
			secondary = Attribute.Arcane;
			dc=13 + rank;
			break;
		default:
			test = Attribute.Power;
			secondary = Attribute.Ki;
			break;
		}

	}
	@Override
	public void resolve(Character active) {
		if(active.state==State.ready){
			calcReward(active);
			if(active.has(Trait.treasureSeeker)){
				dc *= 0.75;
			}
			if(active.check(test, dc)){
				if(active.human()){
					switch(test){
					case Cunning:
						Global.gui().message("<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like " +
								"the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. " +
								"It would probably be a problem to someone less clever. You quickly solve the puzzle and the box opens.<p>");
						break;
					case Perception:
						Global.gui().message("<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. You spot a carefully hidden, but " +
								"nonetheless out-of-place package. It's not sealed and the contents seem like they could be useful, so you help yourself.<p>");
						break;
					case Power:
						Global.gui().message("<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare " +
								"hands if you're strong enough. With a considerable amount of exertion, you manage to force the lid open. Hopefully the contents are worth it.<p>");
						break;
					case Seduction:
						Global.gui().message("<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the " +
								"box is a hole that's too dark to see into and barely big enough to stick a finger into. Fortunately, you're very good with your fingers. With a bit of poking " +
								"around, you feel some intricate mechanisms and maneuver them into place, allowing you to slide the top of the box off.<p>");
						break;
					}
				}

				if(reward.isEmpty()){
					active.money += 200*active.getRank();
				}
				for(Loot i:reward){
					i.pickup(active);
				}
			}
			else if(active.check(secondary, dc-5)){
				if(active.human()){
					switch(test){
					case Cunning:
						Global.gui().message("<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like " +
								"the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. " +
								"Looks unnecessarily complicated. You pull off the touchscreen instead and short the connectors, causing the box to open so you can collect the contents.<p>");
						break;
					case Perception:
						Global.gui().message("<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. You summon a minor spirit to search the " +
								"area. It's not much good in a fight, but pretty decent at finding hidden objects. It leads you to a small hidden box of goodies.<p>");
						break;
					case Power:
						Global.gui().message("<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare " +
								"hands if you're strong enough. You're about to attempt to lift the cover, but then you notice the box is not quite as sturdy as it initially looked. You focus " +
								"your ki and strike the weakest point on the crate, which collapses the side. Hopefully no one's going to miss the box. You're more interested in what's inside.<p>");
						break;
					case Seduction:
						Global.gui().message("<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the " +
								"box is a hole that's too dark to see into and barely big enough to stick a finger into. However, the dark works to your advantage. You take control of the " +
								"shadows inside the box, giving them physical form and using them to force the box open. Time to see what's inside.<p>");
						break;
					}
				}
				if(reward.isEmpty()){
					active.money += 200*active.getRank();
				}
				for(Loot i:reward){
					i.pickup(active);
				}
			}
			else{
				if(active.human()) {
					switch (test) {
						case Cunning:
							Global.gui().message("<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like " +
									"the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. " +
									"You do your best to decode it, but after a couple failed attempts, the screen turns off and stops responding.<p>");
							break;
						case Perception:
							Global.gui().message("<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. Probably nothing.<p>");
							break;
						case Power:
							Global.gui().message("<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare " +
									"hands if you're strong enough. You try to pry the box open, but it's even heavier than it looks. You lose your grip and almost lose your fingertips as the lid " +
									"slams firmly into place. No way you're getting it open without a crowbar.<p>");
							break;
						case Seduction:
							Global.gui().message("<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the " +
									"box is a hole that's too dark to see into and barely big enough to stick a finger into. You feel around inside, but make no progress in opening it. Maybe " +
									"you'd have better luck with some precision tools.<p>");
							break;
					}
				}
			}
			active.location().remove(this);
		}
	}
	public void calcReward(Character receiver){
		int rank = receiver.getRank();
		int value = (rank+1)*10;
		if(rank >= 4){
			if(Global.checkFlag(Flag.excalibur)){
				prizelist.put(Component.PBlueprint, 50);
				prizelist.put(Component.PVibrator, 50);
				prizelist.put(Component.PHandle, 50);
			}
		}
		if(rank >= 3){
			prizelist.put(Component.Capacitor, 45);
			prizelist.put(Component.SlimeCore, 45);
			prizelist.put(Component.Foxtail, 45);
			prizelist.put(Component.DragonBone, 50);
			if(receiver.human()) {
				if(!receiver.has(Clothing.shinobitop)){
					prizelist.put(Clothing.shinobitop, 25);
				}
				if(!receiver.has(Clothing.ninjapants)){
					prizelist.put(Clothing.ninjapants, 25);
				}
				if(!receiver.has(Clothing.halfcloak)){
					prizelist.put(Clothing.halfcloak, 25);
				}
				if(!receiver.has(Clothing.pouchlessbriefs)){
					prizelist.put(Clothing.pouchlessbriefs, 30);
				}
			}
		}
		if(rank >= 2){
			prizelist.put(Consumable.Talisman, 30);
			prizelist.put(Consumable.FaeScroll, 30);
			prizelist.put(Consumable.powerband, 30);
			prizelist.put(Component.MSprayer, 35);
			prizelist.put(Component.Totem, 25);
			prizelist.put(Consumable.Handcuffs, 20);
			prizelist.put(Component.Semen, 15);
			prizelist.put(Potion.SuperEnergyDrink, 10);
			prizelist.put(Component.Titanium, 35);
			prizelist.put(Flask.PAphrodisiac, 15);
			prizelist.put(Component.Skin, 35);
			if(receiver.human()) {
				if(!receiver.has(Clothing.warpaint)){
					prizelist.put(Clothing.warpaint, 35);
				}
				if(!receiver.has(Clothing.gi)){
					prizelist.put(Clothing.gi, 25);
				}
				if(!receiver.has(Clothing.furcoat)){
					prizelist.put(Clothing.furcoat, 20);
				}
			}

		}
		if(rank >= 1){
			if(receiver.human()) {
				if(!receiver.has(Clothing.cup)){
					prizelist.put(Clothing.cup, 20);
				}
				if(!receiver.has(Clothing.trenchcoat)){
					prizelist.put(Clothing.trenchcoat, 25);
				}
				if(!receiver.has(Clothing.kilt)){
					prizelist.put(Clothing.kilt, 25);
				}
			}
			prizelist.put(Component.HGMotor, 20);
			prizelist.put(Potion.Fox, 10);
			prizelist.put(Potion.Bull, 10);
			prizelist.put(Potion.Nymph, 10);
			prizelist.put(Potion.Cat, 10);		
			if(Global.checkFlag(Flag.Kat)){
				prizelist.put(Potion.Furlixir, 15);
			}
		}
		if (receiver.human()) {
			if (!receiver.has(Clothing.speedo)) {
				prizelist.put(Clothing.speedo, 15);
			}
			if (!receiver.has(Clothing.loincloth)) {
				prizelist.put(Clothing.loincloth, 15);
			}
		}
		if(!Global.checkFlag(Flag.CacheNoBasics)) {
			prizelist.put(Component.Sprayer, 7);
			prizelist.put(Flask.Aphrodisiac, 5);
			prizelist.put(Flask.DisSol, 5);
			prizelist.put(Flask.Sedative, 14);
			prizelist.put(Flask.SPotion, 10);
			if (receiver.human()) {
				if (!receiver.has(Clothing.latextop)) {
					prizelist.put(Clothing.latextop, 15);
				}
				if (!receiver.has(Clothing.latexpants)) {
					prizelist.put(Clothing.latexpants, 15);
				}
				if (!receiver.has(Clothing.windbreaker)) {
					prizelist.put(Clothing.windbreaker, 10);
				}
			}
		}
		if(!prizelist.isEmpty()) {
			int total = 0;
			Loot[] bag = new Loot[prizelist.keySet().size()];
			prizelist.keySet().toArray(bag);
			Loot pick;
			while (total < value && reward.size() < 4) {
				pick = bag[Global.random(bag.length)];
				reward.add(pick);
				total += prizelist.get(pick);
			}
		}
	}
	@Override
	public Character owner() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int priority() {
		return 1;
	}
}
