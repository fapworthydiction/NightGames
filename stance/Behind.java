package stance;


import characters.Character;

public class Behind extends Position {

	public Behind(Character top, Character bottom) {
		super(top, bottom,Stance.behind);
		strength = 30;
	}

	@Override
	public String describe() {
		if(top.human()){
			return "You are holding "+bottom.name()+" from behind.";
		}
		else{
			return top.name()+" is holding you from behind.";
		}
	}

	@Override
	public boolean mobile(Character c) {
		return c==top;
	}

	@Override
	public boolean kiss(Character c) {
		return c==top;
	}

	@Override
	public boolean dom(Character c) {
		return c==top;
	}

	@Override
	public boolean sub(Character c) {
		return c==bottom;
	}

	@Override
	public boolean reachTop(Character c) {
		return c==top;
	}

	@Override
	public boolean reachBottom(Character c) {
		return true;
	}

	@Override
	public boolean prone(Character c) {
		return false;
	}

	@Override
	public boolean feet(Character c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean oral(Character c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean behind(Character c) {
		return c==top;
	}

	@Override
	public boolean penetration(Character c) {
		return false;
	}

	@Override
	public Position insert(Character c) {
		return new Doggy(top,bottom);
	}


}
