package stance;

import characters.Character;

public class ReversePin extends Position {

	public ReversePin(Character top, Character bottom) {
		super(top, bottom,Stance.reversepin);
		strength = 60;
	}

	@Override
	public String describe() {
		if(top.human()){
			return "You're sitting on "+bottom.name()+"'s upper body, holding "+bottom.possessive(false)+" in the air.";
		}
		else{
			return top.name()+" is sitting on your chest, holding up your lower body in an undignified pose.";
		}		
	}

	@Override
	public boolean mobile(Character c) {
		return c==top;
	}

	@Override
	public boolean kiss(Character c) {
		return false;
	}

	@Override
	public boolean dom(Character c) {
		return c==top;
	}

	@Override
	public boolean sub(Character c) {
		return c==bottom;
	}

	@Override
	public boolean reachTop(Character c) {
		return false;
	}

	@Override
	public boolean reachBottom(Character c) {
		return c==top;
	}

	@Override
	public boolean prone(Character c) {
		return c==bottom;
	}

	@Override
	public boolean feet(Character c) {
		return false;
	}

	@Override
	public boolean oral(Character c) {
		return c==top;
	}

	@Override
	public boolean behind(Character c) {
		return false;
	}

	@Override
	public boolean penetration(Character c) {
		return false;
	}

	@Override
	public Position insert(Character c) {
		if(top.hasDick()&&bottom.hasPussy()){
			return new Missionary(top,bottom);
		}
		else if(top.hasPussy()&&bottom.hasDick()){
			return new ReverseCowgirl(top,bottom);
		}
		else{
			return this;
		}
	}

}
