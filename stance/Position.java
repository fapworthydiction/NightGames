package stance;
import characters.Character;
import java.io.Serializable;

import combat.Combat;



public abstract class Position implements Serializable, Cloneable{
	public Character top;
	public Character bottom;
	public int time;
	public Stance en;
	protected int pace;
	public int strength;
	
	public Position(Character top, Character bottom, Stance stance){
		this.top=top;
		this.bottom=bottom;
		this.en=stance;
		time=0;
		pace=0;
		strength=0;
	}
	public void decay(){
		time++;
	}
	public void checkOngoing(Combat c){
		return;
	}
	public boolean anal(){
		return this.en == Stance.anal || this.en == Stance.analm;
	}
	public abstract String describe();
	public abstract boolean mobile(Character c);
	public abstract boolean kiss(Character c);
	public abstract boolean dom(Character c);
	public abstract boolean sub(Character c);
	public abstract boolean reachTop(Character c);
	public abstract boolean reachBottom(Character c);
	public abstract boolean prone(Character c);
	public abstract boolean feet(Character c);
	public abstract boolean oral(Character c);
	public abstract boolean behind(Character c);
	public abstract boolean penetration(Character c);
	public abstract Position insert(Character c);
	public Stance enumerate(){
		return this.en;
	}
	public void setPace(int speed){
		this.pace = speed;
	}
	public Position clone() throws CloneNotSupportedException {
	    return (Position) super.clone();
	}
	public int escapeDC(Character dom, Character sub){
		return dom.bonusPin(strength)-((5*time)+sub.escape());
	}
}
