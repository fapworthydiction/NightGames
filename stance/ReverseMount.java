package stance;


import characters.Character;

public class ReverseMount extends Position {

	public ReverseMount(Character top, Character bottom) {
		super(top, bottom,Stance.reversemount);
		strength = 30;
	}

	@Override
	public String describe() {
		if(top.human()){
			return "You are straddling "+bottom.name()+", with your back to her.";
		}
		else{
			return top.name()+" is sitting on your chest, facing your groin.";
		}
	}

	@Override
	public boolean mobile(Character c) {
		return c==top;
	}

	@Override
	public boolean kiss(Character c) {
		return false;
	}

	@Override
	public boolean dom(Character c) {
		return c==top;
	}

	@Override
	public boolean sub(Character c) {
		return c==bottom;
	}

	@Override
	public boolean reachTop(Character c) {
		return c==bottom;
	}

	@Override
	public boolean reachBottom(Character c) {
		return c==top;
	}

	@Override
	public boolean prone(Character c) {
		return c==bottom;
	}

	@Override
	public boolean feet(Character c) {
		return c==top;
	}

	@Override
	public boolean oral(Character c) {
		return c==top;
	}

	@Override
	public boolean behind(Character c) {
		return c==bottom;
	}

	@Override
	public boolean penetration(Character c) {
		return false;
	}

	@Override
	public Position insert(Character c) {
		if(top.hasDick()&&bottom.hasPussy()){
			return new Missionary(top,bottom);
		}
		else if(top.hasPussy()&&bottom.hasDick()){
			return new ReverseCowgirl(top,bottom);
		}
		else{
			return this;
		}
	}

}
