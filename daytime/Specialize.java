package daytime;

import characters.Attribute;
import characters.Character;
import global.Global;

import java.util.ArrayList;

public class Specialize extends Activity{

    public Specialize(Character player) {
        super("Maya", player);
    }

    @Override
    public boolean known() {
        return player.getRank()>=4;
    }

    @Override
    public void visit(String choice) {
        Global.gui().clearText();
        Global.gui().clearCommand();
        if (choice.equals("Start") || choice.startsWith("No")) {
            if(player.hasSpecialization()){
                Global.gui().message("<i>\"Hello, "+player.name()+". You already have a specialization, and cannot learn more than one. I can remove " +
                        "your specialization to give you another chance to pick, but be aware you'll lose all the points you've gained in it.\"</i>");
                Global.gui().choose(this, "Unlearn Specialize");
            }else{
                Global.gui().message("<i>\"Hello, "+player.name() + ". You've reached the point where you should consider taking a specialization.\"</i><p>" +
                        "<i>\"Specializations are very powerful skillsets that give great utility, but you can only have one. They cannot be trained normally, " +
                        "instead you automatically get a point each time you level up. Therefore it's best to gain one as early as possible.\"</i><p>" +
                        "<i>\"Normally we aren't so blunt and 4th wall breaking, but this is a temporary event. You see, several specializations are implemented " +
                        "and ready to use, but the story events that would normally unlock them are not. When those events are in the game, this will be removed.\"</i><p>" +
                        "<i>\"Since this is a preview of sorts, I'll allow you to remove a specialization if you find it's not to your liking. You'll lose all points in " +
                        "that specialization and need to learn a new one from scratch. Be aware that you may not be allowed to unspecialize in future versions. Save often.\"</i>");
                Global.gui().choose(this, "Hypnosis");
                Global.gui().choose(this, "Temporal");
                Global.gui().choose(this, "Professional");
            }
            Global.gui().choose(this, "Leave");
        } else if (choice.equals("Leave")){
            done(true);
        } else if(choice.startsWith("Unlearn")){
            ArrayList<Attribute> att = new ArrayList<Attribute>();
            att.addAll(player.att.keySet());
            for(Attribute a:att){
                if(a.isSpecialization()){
                    player.att.remove(a);
                }
            }
            Global.gui().message("<i>\"Very well. I will remove your current specialization. Next time you come here, you will be able to select a new one.\"</i>");
            Global.gui().choose(this, "Leave");
        } else if(choice.startsWith("Hypnosis")){
            Global.gui().message("<i>\"Hypnosis skills are focused on applying status effects to your opponents while they are Charmed or Enthralled. You can stack Shamed and Horny statuses to cripple your opponent and build huge amounts of arousal over time.\"</i>");
            Global.gui().choose(this, "Confirm Hypnosis");
            Global.gui().choose(this, "No Thanks");
        } else if(choice.startsWith("Temporal")){
            Global.gui().message("<i>\"Temporal skills are about time manipulation and time management. They give you great utility and can get you out of most problems. However, you need to spend turns priming your temporal manipulator in advance.\"</i>");
            Global.gui().choose(this, "Confirm Temporal");
            Global.gui().choose(this, "No Thanks");
        } else if(choice.startsWith("Professional")){
            Global.gui().message("<i>\"The Professional specialization is focused on improving your most commonly used techniques. Repeated fingering, licking, or thrusting causes you to deal increased pleasure each time.\"</i>");
            Global.gui().choose(this, "Confirm Professional");
            Global.gui().choose(this, "No Thanks");
        } else if(choice.startsWith("Confirm Hypnosis")){
            Global.gui().message("<i>\"Congratulations. You are now a Hypnosis specialist like me. Remember that when your suggestion wears off, your opponent will be briefly immune to being charmed or enthralled again. Wait for that to wear off before attempting to hypnotize them again.\"</i>");
            player.mod(Attribute.Hypnosis,1);
            Global.gui().choose(this, "Leave");
        } else if(choice.startsWith("Confirm Temporal")){
            Global.gui().message("<i>\"Congratulations. You are now a Temporal specialist. Remember to keep your time thingy primed in case you need to use it in an emergency.\"</i>");
            player.mod(Attribute.Temporal,1);
            Global.gui().choose(this, "Leave");
        } else if(choice.startsWith("Confirm Professional")){
            Global.gui().message("<i>\"Congratulations. You are now a Professional specialist. By the way, this skillset wasn't originally intended as a specialization, so it might be a bit underpowered, but I'll let you form your own opinion.\"</i>");
            player.mod(Attribute.Professional,1);
            Global.gui().choose(this, "Leave");
        }
    }

    @Override
    public void shop(Character npc, int budget) {

    }
}