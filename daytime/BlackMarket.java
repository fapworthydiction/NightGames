package daytime;

import characters.ID;
import global.Roster;
import items.Consumable;
import items.Flask;
import items.Item;
import items.Potion;
import global.Flag;
import global.Global;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class BlackMarket extends Store {
	private boolean trained;
	public BlackMarket(Character player) {
		super("Black Market", player);
		add(Flask.Aphrodisiac);
		add(Flask.SPotion);
		add(Flask.DisSol);
		add(Potion.Beer);
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.blackMarket);
	}

	@Override
	public void visit(String choice) {
		if(choice=="Start"){
			acted=false;
			trained=false;
		}
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(choice=="Leave"){
			done(acted);
			return;
		}
		checkSale(choice);
		if(player.human()){
			if(Global.checkFlag(Flag.blackMarketPlus)){
				if(!Global.checkFlag(Flag.metRin)){
					Global.gui().message("You knock on the door to the black market. When Ridley answers, you tell him that you're here to see his premium goods on behalf of " +
							"Callisto. Ridley glances back into the room for a moment and then walks past you without saying anything. You stand there confused, until you see the " +
							"girl on the couch stand up and approach you with a smile.<p>"
							+ "<i>\"Hello "+player.name()+",\"</i> she says while extending her hand. <i>\"I'm Rin Callisto. You shouldn't " +
							"be surprised I know who you are, you've been putting on a good show lately.\"</i> You've seen her here before, but you've never taken a good look at her. She " +
							"has elegant features, shoulder length black hair, and looks a couple years older than you. She's very pretty, but you overlooked her because you assumed she " +
							"was Ridley's girlfriend.<p>"
							+ "<i>\"Mike isn't the most pleasant company, but he's a good middleman. He keeps his mouth shut and he doesn't ask questions.\"</i> He must " +
							"not be reliable enough to earn her trust, otherwise she wouldn't feel the need to keep an eye on him all the time.<p>"
							+ "<i>\"Aesop sold you my name, right? I'll have " +
							"to collect my share from him later. I have many items you won't find anywhere else; items that will give you an edge in the Games. Not all of them are completely " +
							"safe, but I think you knew that when you came looking for the black market.\"</i>");
					Global.flag(Flag.metRin);
					Roster.gainAffection(ID.PLAYER,ID.RIN,1);
					Global.gui().message("<b>You've gained Affection with Rin</b>");
				}
				else if(choice.startsWith("Cursed Artifacts")){
					Global.gui().message("You ask Rin about the assorted tomes and unpleasant looking idols she's laid out.<p>"
							+ "<i>\"You should be careful with those, they're all " +
							"cursed. None of them will kill you unless you're unusually susceptible to such things, but the effects would not be pleasant.\"</i> Ok, it sounds " +
							"like these aren't what you're looking for. You aren't looking to get cursed, no matter how useful the artifacts are.<p>"
							+ "<i>\"The items aren't very " +
							"valuable themselves, otherwise I'd have sold them to collectors. A skilled spiritualist could refine the curse to bestow an unholy boon. Fortunately, " +
							"I have the training to do so.\"</i> An unholy boon? That sounds more than a bit worrying.<p>"
							+ "<i>\"Are you religious? In my experience, demons are just another " +
							"flavor of spirit. It's still your choice, do what you like.\"</i>");
					Global.flag(Flag.darkness);
					acted=true;
				}
				else if(choice.startsWith("Dark Power")){
					if(player.money>=player.getAdvancedTrainingCost()){
						player.money-=player.getAdvancedTrainingCost();
						String scene = "";
						clearRandomScenes();
						addRandomScene("Generic",1);
						if(player.getPure(Attribute.Dark)>=6){
							addRandomScene("Imp",1);
							if(player.getPure(Attribute.Dark)==6){
								scene = "Imp";
							}
						}


						/*if(player.getPure(Attribute.Dark)>=8){
						 * Learning Domination
						 * }
						 */
						if(scene == ""){
							scene = getRandomScene();
						}
						if(scene == "Imp"){
							Global.gui().message("Rin takes you to a dimly-lit back room which is entirely empty, except for a strange, ornate circle drawn on the ground.  The circle is about 10 feet in diameter, and takes up most of the room.  Rin closes the door behind you, and faces the circle.  <i>\"Imps are easy to manipulate,\"</i> she says, <i>\"because they are so predictable.  They value sex and domination above all else, and form intensely hierarchical relationships.  Every imp, upon meeting another imp, has to determine who the dominant one is, and who the submissive one is.  Once an imp is submissive, that imp becomes the dominant one’s thrall, and will forever do the bidding of their master without question.  Observe.\"</i><br>" +
									"Rin begins chanting slowly, the circle begins to glow, and two shadowy shapes begin to form within.  Rin finishes her chant, and you see a male and a female imp inside the circle, facing each other.  The imps are both particularly attractive specimens: the female with her perky, shapely breasts and round, firm ass; the male with hefty, engorged balls and thick, pendulous cock.  They posture aggressively at one another for a moment, before lunging at each other in a wild battle for dominance.  Biting, punching, kicking, kissing, and sucking each other senseless, the two seem evenly-matched as they hotly vie for control of each other’s orifices.<br>" +
									"<i>\"These battles can sometimes last for hours, or even days,\"</i> Rin says, sounding a little bored, <i>\"so we are going to speed up the process.\"</i>  Rin casts a quick spell, and you see shadowy tendrils appear all around the male imp.  They grab at his hands and legs, pulling them in all directions and immobilizing him spread-eagle, standing up.  The female imp smiles sadistically and takes a few steps backwards, only to get a running start and kick the helpless male squarely in his massive, dangling testicles.  You hear a loud smacking sound as the female imp’s foot collides with the male imp’s balls, and the male lets out a low, pained groan.  More out of efficiency than empathy, Rin promptly ends her spell and frees the male imp, who crumples to the ground, clutching his testicles. <br>" +
									"The female imp doesn’t hesitate to press her advantage, and she quickly moves to smear her slippery pussy over the stunned male’s nose and mouth.  Her fragrant aphrodisiac takes its effect almost immediately, and you see the male imp’s dick engorge in a matter of seconds.  While the male is still paralyzed with pain, the female easily takes the male’s throbbing dick into her mouth, and the battle is settled within moments as he cannot resist shooting a massive load down her coaxing throat.<br>" +
									"<i>\"This is where it gets interesting,\"</i> Rin says, as the female imp wipes her mouth.  <i>\"If someone were to now dominate the female imp, they would gain the loyalty of both the female and the male, since he has become her thrall.  He will be incapacitated for a while, and she should still be weakened from her previous battle.\"</i>  It suddenly dawns on you what Rin expects you to do.  <i>\"It’s a real two-for-one deal, good luck!\"</i>  Rin says, and gestures towards the circle. <br>" +
									"You know what needs to be done, so you step into the circle, ready to challenge the small female imp.  The imp licks her lips as you approach, and you each size each other up, before she pounces at you.  You are considerably bigger than she is, so she doesn’t provide much of a challenge - you easily swat her away when she tries to fondle you, and your superior strength lets you hold her down with one hand while pleasuring her with the other.  You soon find yourself behind her, with one hand gently squeezing her tiny, plump breasts and the other lightly teasing her clit. <br>" +
									"The imp is shuddering on the brink of orgasm, and you momentarily let your guard down, assured of your victory.  You open your legs into a wider stance to gain leverage and finish her off, but the imp seizes her opportunity by swiftly kicking her foot backwards, striking you cleanly between the balls with the back of her small, knobby heel.  You let go of the imp as your body lurches forward, and you cup your acing scrotum with your hands and bring your knees together.  Bent over in pain, you are easy to knock off-balance, so the imp further presses her advantage by hooking your knee and tripping you flat on your back, though you keep your legs crossed and your knees to your chest.<br>" +
									" <i>\"You have to win for the ritual to work.\"</i>  Rin reminds you, matter-of-factly. <br>" +
									"Your hands are too busy clutching at your bruised balls to stop the imp from straddling your head and wiping a thick bead of seductive lubricant onto your face; the aphrodisiac begins working immediately and you feel your penis begin to stiffen involuntarily – you realize you had better finish this match quickly if you want to win.  The imp stands up with her legs on either side of your head, and you see your chance; you punch your fist up between her legs, repaying her low blow with one of your own.  Your knuckles collide solidly with her clit, and you hear the imp let out a low groan, crossing her eyes as she collapses forwards, bending over and holding her pussy.  You capitalize upon her position by quickly getting behind her and dominantly inserting yourself into her tight slit, doggy-style.<br>" +
									"You completely knocked the fight out of her with that crotch shot, and she doesn’t seem able to resist as you grab her small body by the hips from behind and fuck her senseless.  You feel her spasm and tremble as she climaxes, and you quickly follow with an orgasm of your own, finalizing your victory.  You pull yourself out of the exhausted imp, and both the male and female imps disappear in black wisps of smoke. <br>" +
									"<i>\"Both imps are now under your control, and from now on they will come to your aid when you call them.\"</i> Rin says.  <i>\"Use them wisely, an imp’s loyalty is not to be abused.  This concludes today’s lesson.\"</i>  You make your way back outside, eager to try your new power.<br>");
						}
						if(scene == "Generic") {
							Global.gui().message("Rin lights some incense and has you lie down on the couch with one of the cursed artifacts on your chest. As she performs a lengthy " +
									"ritual, you feel your body heat up and an overwhelming sense of danger flood through you. You're certain something powerful is trying to take control " +
									"of your soul.<p>"
									+ "Rin chants softly and wraps a talisman around a wooden rod. The presence inside you looms and seems ready to devour you, when suddenly she " +
									"strikes the artifact with the rod. A shock runs through your consciousness, and the sense of danger disappears. The dark power is still present, but it " +
									"seems tame now, willing to obey your command should you call for it.<p>"
									+ "<i>\"The ritual is complete. You can keep the artifact as a souvenir, but all its " +
									"power is in you now.\"</i><p>");
						}
						player.mod(Attribute.Dark, 1);
						Roster.gainAffection(ID.PLAYER,ID.RIN,1);
						Global.gui().message("<b>You've gained Affection with Rin</b>");
						acted=true;
						trained=true;
					}
					else{
						Global.gui().message("You can't afford the artifacts.");
					}
				}
				else if(choice=="S&M Gear"){
					Global.gui().message("Rin opens a case containing a variety of S&M toys and bondage gear.<p>"
							+ "<i>\"You probably shouldn't touch those,\"</i> she says as you reach for them. " +
							"<i>\"I didn't pick these up at a sex shop, they have potent enchantments on them.\"</i><p>"
							+ "Enchanted sex toys? At this point, you'll believe just about anything. How " +
							"useful are they? <i>\"They won't do you much good to use them during the match, but if you use them now, their power will transfer to you. Mostly they'll give " +
							"you fetishes that you'll be able to share with your opponents. It's a high risk, high reward style of sex-fighting.\"</i>"
							+ "<p>Rin smiles and reclines on the couch. " +
							"<i>\"If you buy something, I don't mind helping you try it out. No extra charge of course, I'm not a prostitute.\"</i>");
					Global.flag(Flag.fetishism);
					acted=true;
				}
				else if(choice.startsWith("Fetishism")){
					if(player.money>=player.getAdvancedTrainingCost()){
						player.money-=player.getAdvancedTrainingCost();
						player.mod(Attribute.Fetish, 1);
						acted=true;
						trained=true;
						acted=true;
						clearRandomScenes();
						addRandomScene("Exhibitionsim",1);
						String scene = "";

						if(player.getPure(Attribute.Fetish)==1){
							scene = "Exhibitionsim";
						}
						if(player.getPure(Attribute.Fetish)>6){
							addRandomScene("Bondage",2);
						}
						if(player.getPure(Attribute.Fetish)==6){
							scene = "Bondage";
						}
						if(player.getPure(Attribute.Fetish)>9){
							addRandomScene("Masochism",2);
						}
						if(player.getPure(Attribute.Fetish)==9){
							scene = "Masochism";
						}
						if(scene == ""){
							scene = getRandomScene();
						}
						if(scene.startsWith("Exhibitionism")){
							Global.gui().message("You look through the box and your eye is caught by a collar lying toward the bottom of the pile.  You pick it up and Rin’s eyes light up.  <br>" +
									"<i>\"Excellent,\"</i> she says, <i>\"I’m . . . I mean we are going to have a lot of fun with that.  It will grant you the exhibitionism fetish.\"</i><br>" +
									"You look at her in shock, which she easily recognizes on your face.  <i>\"What?  You don’t like people seeing you naked?\"</i> she asks.  <i>\"Seems like you should get used to it since you participate in the games every night.\"</i>  You hadn’t thought about it that way and start to relax.  <i>\"There you go, time to strip.\"</i><br>" +
									"You slowly strip out of your clothes, unsure about doing this in front of Rin.  She watches in mild amusement though as you do, showing no sign of turning away to give you privacy.  Finally you remove your underwear and stand there in front of her completely naked, not sure what to do next.  Once you have your arms at your sides she walks over and slips the collar around your neck.  She gets very close to you, bumping against your flaccid penis with her hip as she fastens it against your skin.  You instinctively pull away and a slight smile crosses her face as you do.  Her fingers linger against your neck for a minute after she finishes working on the buckle and then she finally steps back. <br>" +
									"<i>\"It will take a minute to begin to work,\"</i> she says and stands there watching you.  She just stares at you and you stare back.  You initially feel like you should cover yourself with your hands but you force yourself to continue to stand there with your arms at your side.  As the minutes tick by, your skin starts to tingle, starting slowly at your neck and then spreading down your body.  It travels down your chest, through your stomach and into your crotch.  As it spreads, you begin to notice you are more comfortable with what is happening and don’t feel the desire to move your hands in front of yourself.  You begin to actually enjoy the fact that Rin is watching you and you relax.<br>" +
									"<i>\"Good,\"</i> Rin finally says, <i>\"It looks like the enchantment is starting to take effect.\"</i>  She takes out her phone and holds it up in front of herself.  <i>\"Now for the real thing.\"</i>  Your eyes widen as you hear her phone make the familiar sound that comes with it taking a picture.  She takes a couple more as your hands move to cover your crotch.<br>" +
									"<i>\"Nope, move those hands,\"</i> she says.  <i>\"You are my little bitch now and this is part of the process. I’m taking these pictures to prove that you now like being exposed to other people.\"</i> She adds as if reading your mind.  You drop your hands and she takes a couple more pictures.  Each subsequent picture causes the tingling that has continued across your skin to begin to subside.  It starts receding from the furthest parts of your body and centers on your groin. <br>" +
									"<i>\"Hands behind your back,\"</i> Rin suddenly demands.  <i>\"Let me see your little dick.\"</i>  You hesitate for a minute but then do as she says.  As she takes a few more pictures the sensation fully focuses around your cock and balls then begins to strengthen.  <i>\"Huh, not enjoying this?  I thought you were more of a show off, since you are in the games.  But you haven’t even started to harden yet.\"</i><br>" +
									"<i>\"Now put your hands behind your head,\"</i> she demands.  This time you don’t hesitate and stand there without a hint of modesty as she clicks away.  You feel your cock finally begin to harden as you realize you are actually enjoying this session.  The question of what she is planning to do with the pictures doesn’t even cross your mind at this point, you just want her to keep going.  <br>" +
									"<i>\"Grab your dick and start stroking,\"</i> she says after seeing that you had become comfortable with just posing naked.  This escalation does cause you to hesitate slightly, but after a moment’s thought you begin throbbing at the thought and can’t resist following her direction.  You begin to slowly stoke your length, enjoying the feeling.  You hear her phone make a different sound from before and you realize that she is now taking a video of you instead of just pictures.<br>" +
									"<i>\"Stroke faster, I know you want to you little slut,\"</i> she says.  You pick up the pace. <i>\"Good, my friends are going to love this little video.\"</i>  Your jaw drops when she says that, you didn’t realize that she was planning to share these with anyone.  However, despite your surprise at this revelation you keep stroking.  <br>" +
									"<i>\"You’re enjoying this,\"</i> she states, <i>\"you can’t resist being turned on by the idea of my friends watching my little toy play with himself.\"</i>  You realize that she is right as your excitement is quickly rising inside you.  The idea of people you don’t know, and not knowing how many there might be, watching you masturbate is definitely turning you on.<br>" +
									"<i>\"Come on boy, let them see you cum,\"</i> Rin encourages you, <i>\"Let them know how little control you have over yourself.\"</i>  Your breathing has increased and you feel your orgasm rising inside you as you reach the point of no return.  A small part of your brain wants you to stop, but the larger part wants to show everyone that might watch this video your release.  The majority wins out and you explode only a few minutes later, spraying your sperm out and onto the floor.<br>" +
									"<i>\"There you go,\"</i> she says as you empty your balls onto her floor.  You hear her phone chime as she pushes something on the screen and she lowers it.  She tosses you a box of Kleenex.  <i>\"The transfer has been successful, you now have the exhibitionism fetish.  Now clean up and get dressed.\"</i><br>");
						}
						if(scene.startsWith("Masochism")){
							Global.gui().message("You select one of the S&M toys and pay Rin for it. She picks up the toy - a leather riding crop - and "
									+ "gives you a small wicked smile.<p>"
									+ "<i>\"Good choice. This one will grant you the masochism fetish if used properly. I'm quite good with these. "
									+ "Get undressed and we'll get started.\"</i><p>"
									+ "Your trepidation must show on your face, because her smiles becomes "
									+ "slightly more reassuring. <i>\"Sometimes the path to power can be painful. The gifts in these items may bring you "
									+ "victory, but there's no easy way to unlock them. Besides, you'll enjoy it before we're through. That's the whole "
									+ "point after all.\"</i><p>"
									+ "You strip and stand awkwardly in front of Rin who grins unabashedly as she stares at your flaccid member, twirling "
									+ "the leather riding crop between her fingers.<p>"
									+ "<i>\"I must say, "+player.name()+", you're quickly becoming one of my favorite customers.\"</i><p>"
									+ "Rin expertly twirls the riding crop one last time before sending a hard swat to your nipple!<p>"
									+ "You gasp at the stinging sensation and are surprised by the animalistic urge that flares in the back of your mind.<p>"
									+ "She reels back and smacks your other nipple even harder than before! Another primal, beastly feeling flared in the "
									+ "back of your mind! Your chest is heaving with breaths that are coming faster and faster. Your mind rushes with images "
									+ "of slapping the riding crop from Rin's hands and pushing her up against the wall!<p>"
									+ "Rin traces the tip of the riding crop down your abs and teases your stiffening cock for a moment before sliding it "
									+ "toward your butt. She reels back and delivers full strength blow with a loud SMACK!<p>"
									+ "Much to your own surprise, you moan in pleasure!<p>"
									+ "As the pain from the welting blow on your ass grows your mind races with images of ripping Rin's clothes off and "
									+ "making her yours. You're starting to understand that the pain gets you in touch with your inner beast; the part of "
									+ "you that was designed to hunt sabretooth tigers and to mount girls like Rin.<p>"
									+ "And Rin? The fact that such a petite, slender girl was causing you this pain was incredibly arousing! You, a strong "
									+ "man who could stop this at any moment, wait in excitement for where she'll hit you next. Wherever it is, you know you "
									+ "can take it!<p>"
									+ "<i>\"Wow,\"</i> Rin is staring in wide-eyed disbelief at your throbbing erection. <i>\"I've never seen the enchanted "
									+ "riding crop work so quickly! You must've been a fairly kinky man before this!\"</i><p>"
									+ "She delivers another intoxicating smack to your butt!<p>"
									+ "<i>\"Let's make a fun little wager! If you stay standing for three more hits than I'll let you fuck me however "
									+ "you please.\"</i> Rin doesn't even wait for you to acknowledge her bet before she flicks her wrist between your legs!<p>"
									+ "The tip of the riding crop hits your right ball, causing an intense flash of pain that almost drops you to your knees.<p>"
									+ "<i>\"Hmm, not bad, but I've had men stay standing after the first blow before.\"</i> Rin gently rubs your balls with the "
									+ "riding crop as she mocks you. She slowly reels the riding crop back as she prepares her next strike.<p>"
									+ "You fight the instinctive urge to protect yourself and grit your teeth.<p>"
									+ "The riding crop smacks hard against your left testicle! Your knees wobble as the intense pain takes over all your senses. "
									+ "You almost to drop to the floor but stay standing so your struggle thus far wasn't in vain.<p>"
									+ "<i>\"Wow! I am impressed!\"</i> Rin reaches over and cups your burning testicles. <i>\"Almost everyone is done by the second swat! I wonder if you'll be the first to stay standing after all three! I doubt it though, seeing as you were just as dumb as the others.\"</i><p>Before you can fully comprehend the implication of that statement, Rin grabs your shoulder and launches a devastating knee to your testicles!<p>You drop to the floor and moan, clutching at your bruised balls.<p><i>\"I said three hits. Not three hits with the riding crop. Anyhow, your erection is proof that the fetish has been transferred. You may take as long as you need to recover before you show yourself out.\"</i>");
						}else if(scene.startsWith("Bondage")){
							Global.gui().message("You pull a long, golden rope out of the box and pay Rin for it. She gently picks up the rope and smiles seductively.<p>"
									+ "<i>\"A man after my own tastes. This rope will bestow unto you a bondage fetish. Please hold out your wrists and we will "
									+ "begin the process.\"</i><p>"
									+ "You anxiously hold out your wrists and present them to Rin.<p> "
									+ "Your self-doubt at the choice you made grows as you watch her expertly tie a comfortable, yet inescapable, knot around "
									+ "your wrists. Just as you're about to voice your concerns she gives the rope one final tug before taking a step back "
									+ "and admiring her work.<p>"
									+ "For a moment it seems as if nothing happened. You're about to call the entire thing off before you feel a familiar "
									+ "twitch of arousal in your cock. Your cock continues to grow as your heart and breath begin to race. You look up at "
									+ "Rin in bewilderment who, you just now notice, has been hungrily staring at you this entire time."
									+ "<i>\"You know,\"</i> she gives your bulge a soft squeeze before she begins unbuttoning your pants, <i>\"this fetish "
									+ "isn't about immobility. Rather, it's a fetish for being completely vulnerable to your captor.\"</i>"
									+ "Your pants fall to the floor and, to your bewilderment, your cock is harder than you can ever recall seeing it. It "
									+ "throbs between you two before she places a delicate hand on your cock and begins to slowly stroke your shaft.<p>"
									+ "<i>\"Tell me "+player.name()+",\"</i> her hand slides down your shaft and cups your balls, <i>\"how does it feel to be "
									+ "at my complete mercy?\"</i> Her hand gently, yet firmly, squeezes your delicate testicles.<p>"
									+ "It dawns on you that you hardly know this woman. You have no clue whether she'll continue to treat you gently or if "
									+ "she has a sadistic streak. In a quick moment of panic you try to escape, but find the ropes just as unforgiving as when "
									+ "she tied them.<p>"
									+ "Realizing you're unable to escape sends an unexpected feeling of arousal through your body. Your mind races with lust, "
									+ "fear, arousal, and just as she predicted, a vulnerability that feels more delicious the longer it lasts.<p>"
									+ "Rin goes back to stroking your cock and smiles as she watches the wide range of emotions playing out on your face.<p>"
									+ "<i>\"Enjoying it more, yes? These ropes have had even the strongest of men on their knees. And don't worry. You're "
									+ "in absolutely no danger here.\"</i><p>"
									+ "You breathe a sigh of relief.<p>"
									+ "<i>\"I wouldn't relax too quickly. I'll remind you that I said I would help you learn the fetish of the item you purchased. "
									+ "Never once did I say I would help you cum.\"</i><p>"
									+ "Rin teases your cock for almost three hours, constantly stroking or sucking you to the edge of release but never letting "
									+ "you reach climax. By the end of your session you're on your knees, begging her to let you cum while at the same time "
									+ "secretly hoping she doesn't just so this pleasurable torture can continue.<p>"
									+ "Eventually, she rewards you for holding out as long as you did, and she swallows every drop of cum from your screaming "
									+ "erection down her throat.<p>"
									+ "You leave the shop in a haze, unsure of what happened, but with the rope clutched tightly in your hand.");
						}
						Roster.gainAffection(ID.PLAYER,ID.RIN,1);
						Global.gui().message("<b>You've gained Affection with Rin</b>");
					}
					else{
						Global.gui().message("You can't afford that.");
					}
				}
				else{
					Global.gui().message("Ridley leaves the black market as soon as he sees you. It seems you're a preferred client now, and you can deal with Rin directly. Rin gives " +
							"you a polite smile and stands up. <i>\"Welcome back. What can I do for you?\"</i><p>"
							+ "Upcoming Skills:<br>"+
							Global.getUpcomingSkills(Attribute.Dark, player.getPure(Attribute.Dark))+"<br>"+
							Global.getUpcomingSkills(Attribute.Fetish, player.getPure(Attribute.Fetish)));
				}
				if(!trained){
					if(!Global.checkFlag(Flag.darkness)){
						Global.gui().choose(this,"Cursed Artifacts");
					}else{
						Global.gui().choose(this,"Dark Power: $"+(player.getAdvancedTrainingCost()));
					}
					if(!Global.checkFlag(Flag.fetishism)){
						Global.gui().choose(this,"S&M Gear");
					}else{
						Global.gui().choose(this,"Fetishism: $"+(player.getAdvancedTrainingCost()));
					}
				}
			}
			else{
				Global.gui().message("You knock on the door to the room Aesop pointed you to. A somewhat overweight man, a few years older than you, looks you over for a moment " +
					"before letting you in. The dorm room is tidier than its occupant, whose clothes are noticeably stained and smell faintly of old beer and weed. There's a bong on " +
					"the nearby table and an attractive girl on the couch who makes no indication that she noticed you enter. <i>\"Don't mind the bitch,\"</i> says Ridley, noticing your " +
					"attention. <i>\"She doesn't care who you are and neither do I. What are you looking for?\"</i>");
			}
			for(Item i: stock.keySet()){
				Global.gui().message(i.getName()+": $"+i.getPrice());
			}
			Global.gui().message("You have :$"+player.money+" to spend.");
			displayGoods();
			Global.gui().choose(this,"Leave");
		}
	}

	@Override
	public void shop(Character npc, int budget) {
		int remaining = budget;
		if(npc.getPure(Attribute.Dark)>0&&remaining>=player.getAdvancedTrainingCost()){
			if(remaining>=npc.getAdvancedTrainingCost()*2.5){
				npc.money-=npc.getAdvancedTrainingCost();
				remaining-=npc.getAdvancedTrainingCost();
				npc.mod(Attribute.Dark, 1);
			}
			npc.money-=player.getAdvancedTrainingCost();
			remaining-=player.getAdvancedTrainingCost();
			npc.mod(Attribute.Dark, 1);
		}
		if(npc.getPure(Attribute.Fetish)>0&&remaining>=player.getAdvancedTrainingCost()){
			if(remaining>=npc.getAdvancedTrainingCost()*2.5){
				npc.money-=npc.getAdvancedTrainingCost();
				remaining-=npc.getAdvancedTrainingCost();
				npc.mod(Attribute.Fetish, 1);
			}
			npc.money-=npc.getAdvancedTrainingCost();
			remaining-=npc.getAdvancedTrainingCost();
			npc.mod(Attribute.Fetish, 1);
		}
		int bored = 0;
		remaining = Math.min(remaining, 120);
		while(remaining>25&&bored<5){			
			for(Item i:stock.keySet()){
				if(remaining>i.getPrice()&&!npc.has(i,10)){
					npc.gain(i);
					npc.money-=i.getPrice();
					remaining-=i.getPrice();
				}
				else{
					bored++;
				}
			}
		}
	}
}
