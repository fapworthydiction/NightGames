package daytime;

import characters.Character;
import characters.Dummy;
import characters.Emotion;
import characters.ID;
import global.Flag;
import global.Global;
import global.Roster;

public class Evetime extends Activity {
    private boolean acted;
    private Dummy sprite;

    public Evetime(Character player) {
        super("Eve", player);
        sprite = new Dummy("Samantha");
    }

    @Override
    public boolean known() {
        return Global.checkFlag(Flag.Eve);
    }

    @Override
    public void visit(String choice) {
        Global.gui().clearText();
        Global.gui().clearCommand();
        sprite.dress();
        sprite.setBlush(1);
        sprite.setMood(Emotion.confident);
        if(choice.startsWith("Start")){
            acted = false;
            if(Global.getValue(Flag.GangRank)<1){
                Global.gui().message("It’s a bit of a walk off campus to find the bar where Eve apparently hangs out. It’s a much shittier part of town than you were expecting. The bar/pool hall is only accessible via a seedy looking alleyway, with a sign you wouldn’t have spotted if you didn’t know what to look for. This is not a place you’d want to come to after dark.<br>" +
                        "You enter cautiously. You don’t have a fake ID, but hopefully you’ll have a chance to talk to her before you get kicked out. You should try to act as inconspicuous as possible to avoid getting carded.<br>" +
                        "<br>" +
                        "It turns out the bar’s only occupants are three girls, including Eve. You don’t recognize the other two. One is a short girl with dyed hair, torn clothing, and multiple piercings. She’d look right at home in this seedy bar except for being pretty clearly your age. The other girls seems entirely out of place here; Tall and beautiful with refined features and clothing. They immediately turn their attention toward you.<br>" +
                        "<br>" +
                        "<i>\"Well, look who it is.\"</i> Eve smirks at you. <i>\"Did Aesop send you?\"</i> You shake your head. Aesop told you where to find her, but you’re here on your own business. You choose your words carefully in front of the other two girls, but make it clear that you’d like to speak to Eve in private.<br>" +
                        "<br>" +
                        "Eve picks up a pool cue and returns to a game that was apparently in progress. <i>\"Say what you want. My girls here know what’s what. This is Faye,\"</i> she points her cue toward the taller girl. \"</i>And Zoe.\"</i> She points to the short girl. <br>" +
                        "<br>" +
                        "That makes sense. If this eclectic pair is hanging out with Eve, you aren’t surprised they’re former participants in the Games.<br>" +
                        "<i>\"No, I’m afraid not.\"</i> The tall girl, Faye, speaks in a soothing tone. <i>\"We heard about these Games from Eve here. However, we were not invited to join, despite expressing interest.\"</i><br>" +
                        "<br>" +
                        "Other other girl, Zoe approaches you with barely disguised hostility. <i>\"I take it this guy’s in the Games?\"</i> She sizes you up in a way that makes you uncomfortable. <i>\"He doesn’t look like much. Why’d he make the cut instead of me?\"</i><br>" +
                        "<br>" +
                        "It sounds like Eve has been pretty loose lipped. <i>\"Maya is trying to keep this shit quiet, but why the fuck should I care? I guess they can kick me out, but it’s not like they can steal my powers.\"</i><br>" +
                        "Her contempt for secrecy actually suits your interests. There’s a lot about the Games you don’t know yet, and Eve’s probably been around long enough to have heard some things.<br>" +
                        "<br>" +
                        "Eve considers this while sinking her next shot. <i>\"Yeah, I probably know some stuff. I’m not going to just tell you for free though. You want this info, which makes it valuable. It’s at least worth betting on.\"</i> She takes another shot and sinks another ball. <i>\"I don’t really need your money either. You’ll need to wager something I want.\"</i><br>" +
                        "<br>" +
                        "Eve’s desires are pretty predictable. If she wants to wager sexual favors, that’s something you’re prepared for.<br>" +
                        "<i>\"Well… I don’t just make bets with anyone.\"</i> She aims carefully and sinks the 8 ball. Zoe curses under her breath and reluctantly gets down on her knees. Eve unzips her jeans and pulls out her impressive cock. Neither of the other girls shows any surprise at her unusual genitalia.<br>" +
                        "<br>" +
                        "Zoe gives you a resentful glare before she takes Eve’s member into her mouth and starts to service it. When she wagered a blowjob on their pool game, she probably didn’t expect to do it in front of a stranger.<br>" +
                        "<br>" +
                        "<i>\"You see, we have our own version of the Games.\"</i> Eve affectionately runs her hand through Zoe’s short hair. <i>\"My girls here have earned the right to challenge me. If you want to join in, you’ll have to become a member of my little gang and work your way up.\"</i> She closes her eyes and groans lustily. Apparently Zoe is pretty good with her mouth.<br>" +
                        "<br>" +
                        "You do your best to ignore the sex act going on in front of you. OK, let’s say you wanted to join her games. What would you have to do?<br>" +
                        "<br>" +
                        "<i>\"We have a pretty strict hierarchy here. Zoe here is currently at the bottom of the ladder.\"</i> She pats the head currently blowing her. <i>\"If you join up, you’ll be the new bottom, under her.\"</i> Eve thrusts her hips suddenly as she ejaculates into the girl’s mouth. You wait patiently for her to finish and resume talking.<br>" +
                        "<br>" +
                        "<i>\"Good girl. Since Zoe will be your direct superior, I figure she should decide your initiation.\"</i><br>" +
                        "The short girl takes a few seconds to swallow the load Eve gave her. When she recovers, she stands up and gives you a wicked smile.<br>" +
                        "<br>" +
                        "<i>\"OK pretty boy, you want to join us? You took my spot in the Games, so you’ll have to help me vent my resentment. Specifically, I want your balls.\"</i> She raises her open hand threateningly. <i>\"Five slaps. If you can handle five slaps on your balls without tapping out, I’ll accept you.\"</i><br>");
                Global.gui().choose(this,"Initiation","It won't be fun, but you can take it. Information is worth some pain.");
                Global.gui().choose(this,"Refuse","No way! You're not letting this crazy bitch near your balls!");
            }else{
                Global.gui().message("You hang out with Eve’s gang. Their presence in this shitty bar seems pretty constant, but they don’t seem to care how often you come and go.<br>" +
                        "<br>" +
                        "As the lowest seniority member of the gang, you’re expected to make the drinks and handle minor errands. No one demands anything unreasonable. Zoe, despite her initial hostility, seems pretty pleased to have a subordinate. She still maintains her prickly demeanor, but you can tell she’s starting to warm up to you.<br>" +
                        "<br>" +
                        "Strangely, the girls seem to have free reign of the bar. At one point, you asked Eve why you never see anyone in the bar. She just shrugged. <i>\"We’re here.\"</i> That seems to be all the explanation you’re likely to receive from her.<br>" +
                        "<br>" +
                        "You’re probably not going to get any significant answers out of Eve unless you bet her for it. Unfortunately, in your current position, you can’t challenge Eve or Faye. You’ll have to beat Zoe a few times to work your way up.<br>");
                Roster.gainAffection(ID.PLAYER,ID.EVE,1);
                Global.gui().choose(this,"Leave");
            }
        }else if(choice.startsWith("Leave")){
            Global.gui().showNone();
            done(acted);
        }else if(choice.startsWith("Refuse")){
            Global.gui().message("No way. You aren’t going to degrade yourself just for the privilege of gambling with Eve’s crew.<br>" +
                    "<br>" +
                    "You walk to the door, half-hoping Eve will relent and offer a better deal, but the girls seem to have lost interest in you. They don’t say a word as you leave.<br>");
            Global.gui().choose(this,"Leave");

        }else if(choice.startsWith("Initiation")){
            acted = true;
            Global.gui().message("Fine. If this is your first step to getting some answers from Eve, you’ll just have to do whatever it takes. It won’t be pleasant, but you’ve endured a lot of punishment during the Games.<br>" +
            " <br>" +
            "You take off your pants and pull out your genitals.  Zoe grins and rubs her hands together in anticipation, while Eve and Faye sit down at the bar, sipping their drinks and watching with interest.  Zoe approaches you and gently cups your balls in her hand, jostling them slightly; she leans in close to you and whispers with a smirk, <i>\"Good luck.\"</i> <br>" +
            " <br>" +
            "With her left hand, Zoe wraps her thumb and forefinger around your scrotum, loosely constricting your sack around your testicles, and giving her a clear target.  <i>\"OK, here are the rules,\"</i> she says, now that she literally has you by the balls, <i>\"I get five slaps.  Until all five slaps are finished, no part of you can touch the ground except your feet.  If you make it all the way to five, you’re in.\"</i> To punctuate her statement, she gives your nuts a slight squeeze.  <i>\"Ready, "+player.name()+"?\"</i>  With some hesitation, you nod, and prepare yourself for the worst.<br>" +
            " <br>" +
            "Still holding your balls in her left hand, Zoe slowly draws her right hand into the air.  Her smirk turns into a large sadistic smile as she stares intently at your balls, positioning them on her palm.  All of the muscles surrounding your groin begin to tense in preparation, and you mentally steel yourself against the coming blow.  She pauses for a moment with her arm drawn back, ready to smack you across the grapes; you wish she would just hurry up and get on with it.  <i>\"The anticipation is really the best part.\"</i>  She says, obviously savoring the expression on your face. <br>" +
            " <br>" +
            "With artistic precision, she whips her hand down, slapping both your bare nuts with the tips of her fingers.  Your whole body flinches as the pain shoots through you like lightning; Zoe lets go of your balls, and you instinctively cover them with your hands - you can’t help but let out a small groan as your abdomen tenses and your throat chokes up slightly.  Your recent participation in the Games, however, has toughened you up – you’ve been hit in the nuts worse than this plenty of times before.  Clearing your throat, you regain your composure and straighten your posture.  Zoe even seems a little impressed on how quickly you recovered.  Narrowing her eyes and smiling at you, she coldly says <i>\"One.\"</i><br>" +
            " <br>" +
            "You wince as she takes your balls back into her left hand, holding them firmly in position for another slap.  Again, she seems to be taking her time, lazily pulling her right hand back as she lines up her next shot.  Her body twitches as she begins to bring her right hand down once more, and you shut your eyes, grit your teeth and tense your whole body to prepare for the coming hit.<br>" +
            "<br>" +
            "<i>\"Psych!\"</i> Zoe says, pulling her hand back at the last moment, halting her strike.  You hear the girls at the bar erupt in laughter as you open your eyes, and even Zoe lets out a giggle at her trick, still holding your balls in her hand.  Believing yourself momentarily safe, you relax your body, but in the blink of an eye Zoe whips her hand back out and wracks your ballsack with her fingers.  She didn’t hit you much harder this time, but you were totally unprepared for it, and the surprise made it hurt so much more.  Again, you bend over and clutch at your balls as Zoe releases them from her grip.  She looks over to the girls at the bar, and they all share another laugh as you groan in pain.  Your knees are starting to get a little wobbly, and the pain is moving up into your stomach and making you nauseated, but you manage to stay on your feet, readying yourself for the next hit.  <i>\"Two.\"</i> Zoe says, stifling her laugh.<br>" +
            " <br>" +
            "Your balls have begun to swell slightly, and your scrotum is wrapping snugly around them as they throb along with your heart beat.  Pointing down at your legs, Zoe says, <i>\"All right, big man, spread ‘em.\"</i>  Squaring up your shoulders, you reluctantly widen your stance.  With your feet shoulder-width apart, Zoe doesn’t need to hold your balls to get a clean shot.  She leans forward, puts her left hand on your shoulder, and brings her right hand down and back behind her as if winding up to throw an underhand softball. Everything in your body is telling you to close your legs and protect your nuts, but you somehow muster the resolve to stand up straight, and you hold your arms bravely behind your back as you brace yourself for another blow.<br>" +
            " <br>" +
            "Twisting her whole body into the strike, her right hand swings swiftly forward and upward, cleanly striking your balls from below; the slap sends your balls bouncing in all directions as it connects.  There is a distinct clapping sound as the strike finds its delicate targets, and it feels like this time she got you with the palm of her hand, instead of just her fingers.    <br>" +
            " <br>" +
            "Your world explodes in pain.  This strike was a magnitude harder than the previous two.  You cannot suppress your groan as your legs start to give out beneath you.  You bend forwards, snapping your knees back together, with your testicles tucked between your thighs for protection.  You barely have time to register the pain when Zoe steps around you, launching a vicious and unexpected slap from behind.  You feel her hand smash into your balls, and your mind goes completely blank in a desperate attempt to lessen the pain.  Your mouth is hanging open as you shake unsteadily, rocking forwards and backwards on your feet as you muster all of your willpower to stay standing.  <i>\"Holy shit, Zoe, that was brutal!  Even I felt that one!\"</i>  You hear Eve yell from the bar, as Faye chuckles playfully next to her.<br>" +
            " <br>" +
            "<i>\"Three…and four.\"</i> Zoe says, still behind you, cracking her knuckles as she watches you struggle to maintain your footing.<br>" +
            " <br>" +
            "Your head is spinning, and it feels like your balls are spinning too.  All you want is to curl up on the ground right now, but with every bit of determination you stay standing.  Your knees are shaking and your nuts are burning with excruciating pain; you are barely managing to remain on your feet, the agony in your balls is the only thing you can think about.  This is it, there’s only one more to go…<br>" +
            " <br>" +
            "Zoe stands in front of you, again putting her hand around your nutsack, but instead of lining them up for her final shot, she begins softly massaging them with and unexpected tenderness.  There is a touch of sympathy in her voice as she says, <i>\"You know, "+player.name()+", since you are still on your feet at this point, you have basically passed.  Even if the next hit incapacitates you, you still made it to five.\"</i>  Her massaging intensifies slightly, taking some of the dull ache out of your scrotum. <br>" +
            " <br>" +
            "Zoe continues <i>\"I mean, slapping you one more time wouldn’t prove a thing, anyway.  I think we have all seen here today that you can take a good sacking like a champ, even if you might walk home with a limp.\"</i>  Her ball-fondling skills are far better than you would expect, and you find yourself stiffening while she plays with you.  With one hand on your balls, she moves her other hand up to your shaft and starts stroking it – despite the agonizing pain in your jewels, you are getting gradually drawn in to the excellent handjob Zoe is giving you. <br>" +
            " <br>" +
            "<i>\"Not bad, huh? While I have your junk in my hands, I’m gonna show you why I should be allowed to into the Games.\"</i> Zoe says, seeming determined to get you off; you don’t resist as her hands bring you to the brink of orgasm.  Your balls have been through a lot of abuse, but they still begin to lurch and contract as you prepare to blow your load.  You take one last deep breath, closing your eyes as you blissfully drift into climax.<br>" +
            " <br>" +
            "<i>\"Five!\"</i>  She yells, as you feel her palm violently slamming into your bare nuts.  You squirt a thick, hot stream of cum into the air as your body jerks forward; Zoe releases her grip and you collapse onto the ground in a heap.  Crumpled and limp on the floor, you are in too much pain to make a sound, but you continue to ejaculate, your balls convulsing from equal parts shock and orgasm. <br>" +
            " <br>" +
            "<i>\"Oh, that felt pretty good.\"</i>  Zoe stands over you, looking quite pleased with herself, despite being spattered with your semen.  <i>\"While you’re having fun each night, at least we’ll both remember that I slapped the spunk out of you.\"</i><br>" +
            " <br>" +
            "You are glad this is the last one you had to take, because there was no way you would have been able to stay on your feet at this point.  Eve gets up and walks over to you, announcing proudly, <i>\"Girls, today we welcome our newest member…"+player.name()+"!\"</i>  The girls give a cheer, <i>\"Take all the time you need there, boy.\"</i> Eve says, reaching down and giving you a sympathetic pat on the shoulder.  <i>\"You’re one of my girls now, so if someone messes with you, just let me know.  I’ll make sure they regret it.\"</i>  Your hands are still clutching protectively at your bruised balls, so you just dumbly nod your head in acknowledgement. <br>" +
            " <br>" +
            "Faye graciously offers you a strong drink and a bag of ice.  Both help dull the pain.  Her sweet smile seems like an oasis of kindness against her cruel company.  You have to remind yourself that she looked like she was enjoying the show just as much as Eve.  It’ll take some time to figure her out.<br>" +
            " <br>" +
            "You successfully joined Eve’s gang.  Rough as this was, you’re one step closer to getting the answers you’re looking for.  Hopefully the next few steps will be easier.  You’re now on the bottom of the totem pole, and you’ll have to prove yourself before you can challenge Eve.<br>");
            Roster.gainAffection(ID.PLAYER,ID.EVE,4);
            Global.gui().choose(this,"Leave");
        }

    }

    @Override
    public void shop(Character npc, int budget) {

    }
}
