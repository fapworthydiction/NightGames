package daytime;

import characters.*;
import characters.Character;
import global.*;

import java.time.LocalTime;
import java.util.ArrayList;

public class Daytime {
	private ArrayList<Activity> activities;
	private Player player;
	private int time;
	private Events auto;
	private int NPCDAYTIME = 8;
	
	
	
	public Daytime(Player player){
		Global.gui().clearText();
		this.player=player;
		manageFlags();
		buildActivities();
		this.auto = new Events(player, this);
		time = 9;
		if(auto.checkMorning()){
			return;
		}		
		plan();
	}
	public void manageFlags(){
		if(Global.checkFlag(Flag.metAlice)){
			if(Global.checkFlag(Flag.victory)){
				Global.unflag(Flag.AliceAvailable);
			}else{
				Global.flag(Flag.AliceAvailable);
			}
		}
		if(Global.checkFlag(Flag.metYui)){
			Global.flag(Flag.YuiAvailable);
		}
		if(Global.checkFlag(Flag.threesome)){
			Global.unflag(Flag.threesome);
		}
	}
	
	public void plan(){
		String scene;
		if(Scheduler.getTime().getHour()<22){
			if(Scheduler.getDayString(Scheduler.getDate()).startsWith("Sunday")){
				Global.gui().message("It is currently "+Scheduler.getTimeString()+". There is no match tonight.");

			}else if(Scheduler.hasMatch(player)){
				Global.gui().message("It is currently "+Scheduler.getTimeString()+". Your next match starts at 10:00pm.");
			}else{
				Global.gui().message("It is currently "+Scheduler.getTimeString()+". You have tonight off.");
			}
			Global.gui().refresh();
			Global.gui().clearCommand();
			Global.gui().showNone();
			if(auto.checkScenes()){
				return;
			}
			for(Activity act: activities){
				if(act.known()&&Scheduler.getTime().getHour()<=22){
					Global.gui().addActivity(act);
				}
			}
		}
		else{
			for(Character npc: Roster.getExisting()){
				if(!npc.human()){
					if(npc.getLevel()>=10*(npc.getRank()+1)){
						npc.rankup();
					}
					((NPC)npc).daytime(NPCDAYTIME, this);
				}
			}
			//Global.gui().nextMatch();
			if(Global.checkFlag(Flag.autosave)){
				SaveManager.save(true);
			}
			Scheduler.dusk();
		}
	}
	public void buildActivities(){
		activities = new ArrayList<Activity>();
		activities.add(new Exercise(player));
		activities.add(new Porn(player));
		activities.add(new VideoGames(player));
		activities.add(new Informant(player));
		activities.add(new BlackMarket(player));
		activities.add(new XxxStore(player));
		activities.add(new HWStore(player));
		activities.add(new Bookstore(player));
		activities.add(new Dojo(player));
		activities.add(new AngelTime(player));
		activities.add(new CassieTime(player));
		activities.add(new JewelTime(player));
		activities.add(new MaraTime(player));
		if(Global.checkFlag(Flag.Kat)){
			activities.add(new KatTime(player));
		}
		activities.add(new PlayerRoom(player));
		activities.add(new ClothingStore(player));
		if(Global.checkFlag(Flag.Reyka)){
			activities.add(new ReykaTime(player));
		}
		if(Global.checkFlag(Flag.Samantha)){
			activities.add(new SamanthaTime(player));
		}
		activities.add(new MagicTraining(player));
		activities.add(new Workshop(player));
		activities.add(new YuiTime(player));
		activities.add(new GinetteTime(player));
		activities.add(new ValerieTime(player));
		activities.add(new Evetime(player));
        activities.add(new Specialize(player));
    }
	public int getTime(){
		return Scheduler.getTime().getHour();
	}
	public void advance(int t){
		Scheduler.advanceTime(LocalTime.of(1,0));
	}
	public static void train(Character one, Character two, Attribute att){
		int a;
		int b;
		if(one.getPure(att)>two.getPure(att)){
			a=100-(2*one.get(Attribute.Perception));
			b=90-(2*two.get(Attribute.Perception));
		}
		else if(one.getPure(att)<two.getPure(att)){
			a=90-(2*one.get(Attribute.Perception));
			b=100-(2*two.get(Attribute.Perception));
		}
		else{
			a=100-(2*one.get(Attribute.Perception));
			b=100-(2*two.get(Attribute.Perception));
		}
		if(Global.random(100)>=a){
			one.mod(att, 1);
			if(one.human()){
				Global.gui().message("<b>Your "+att+" has improved.</b>");
			}
		}
	}
	public void visit(String name, Character npc, int budget){
		for(Activity a:activities){
			if(a.toString().equalsIgnoreCase(name)){
				a.shop( npc, budget );
				break;
			}
		}
	}
}
