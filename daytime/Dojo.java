package daytime;


import characters.ID;
import global.Roster;
import items.Clothing;
import characters.Attribute;
import characters.Character;
import global.Flag;
import global.Global;

public class Dojo extends Activity {
	private boolean acted;
	public Dojo(Character player) {
		super("Meditate", player);
		acted = false;
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.meditation);
	}

	@Override
	public void visit(String choice) {
		if(choice=="Start"){
			acted = false;
		}
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(Global.checkFlag(Flag.dojo)&&choice=="Start"){
			if(!Global.checkFlag(Flag.metSuzume)){
				Global.gui().message("You go to the Suzuki dojo to meet Suzume. She looks like she's trying to looks like project the air of a dignified martial arts master, but " +
						"you can tell she's excited at the prospect of getting a student. <i>\"Aesop tells me you're interested in becoming my apprentice. Sorry I didn't offer to " +
						"train you earlier, but Aesop was very insistent that I wait.\"</i> She walks past you and locks the dojo door. <i>\"We have some privacy, so take off your clothes " +
						"and let me take a look at you.\"</i><p>You strip off your clothes, slightly embarrassed about the way she's staring at you. She runs her hands lightly over your " +
						"arms, chest, and back. <i>\"Good musculature. I think you have some of potential.\"</i> She's trying to sound business-like, but there's a noticeable blush on her " +
						"cheeks. You're also affected by her touch and your dick gradually becomes erect. Suzume looks down at your manhood and smiles. <i>\"Maybe a lot of potential... " +
						"Ok, you can get dressed now, unless you need a hand.\"</i> You quickly put your pants back on and cover up your erection. Normal social standards may be more " +
						"flexible here, but it's still probably not a good idea to request a handjob when you're already asking for training.<p><i>\"I'd be happy to have you as a student, " +
						"but I can't train you for free. I don't charge for guided meditation because it's not a traditional dojo service, but you'll need to pay for each training " +
						"session.\"</i> Her expression darkens as she bites her lip. <i>\"My father is not in good health and he can't take students. I need money to keep the dojo open, and " +
						"I know you're making plenty in the Games.\"</i>");
				Global.flag(Flag.metSuzume);
				Global.gui().choose(this,"Train: $"+(player.getAdvancedTrainingCost()));
				Global.gui().choose(this,"Sharpen Senses");
				Global.gui().choose(this,"Shut Out Sensation");
				Global.gui().choose(this,"Leave");
			}
			else{
				Global.gui().message("You go to the Suzuki dojo and remove your shoes out of respect. Suzume (or Suzuki-shisho as she's instructed you to call her) give you a friendly " +
						"smile as you bow. <i>\"Welcome apprentice. Are you ready to continue your training or are you here to meditate?\"</i><p>"
						+ "Upcoming Skills:<br>"
						+ Global.getUpcomingSkills(Attribute.Ki, player.getPure(Attribute.Ki)));
				Global.gui().choose(this,"Train: $"+(player.getAdvancedTrainingCost()));
				Global.gui().choose(this,"Sharpen Senses");
				Global.gui().choose(this,"Shut Out Sensation");
				Global.gui().choose(this,"Leave");
			}
		}
		else if(choice=="Start"){
			Global.gui().message("You contact Suzume and schedule a private meditation session. Dropping Aesop's name saved a lot of time; she didn't ask many questions. Apparently " +
				"he refers people to her pretty regularly. The two of you meet at a martial arts dojo that her family runs, but there's no one else there currently. Suzume herself is " +
				"a thin Asian girl with short brown hair and a subtle Japanese accent. She looks you over with a pleased expression. <i>\"If Tyler sent you to me, I'm guessing you want " +
				"me to train you to either be more alert, or to improve your endurance in... private activities.\"</i> You caught a glint of mischief in her eye, but she quickly covers it up. " +
				"<i>\"Now I need to be clear that hypnotic suggestion is not an exact science and there's no guarantee there will be any noticeable change in your sensitivity. If we are "+
				"successful, there will still be a drawback. Making yourself less sensitive can also cause you to miss fine details, and making yourself more perceptive can turn you into "+
				"a quick shot in bed. So knowing all that, do you still want to go through with this?\"</i>");
			Global.gui().choose(this,"Sharpen Senses");
			Global.gui().choose(this,"Shut Out Sensation");
			Global.gui().choose(this,"Leave");
		}
		else if(choice=="Leave"){
			done(acted);
		}
		else if(choice.startsWith("Train")){
			if(player.money>=player.getAdvancedTrainingCost()){
				clearRandomScenes();
				addRandomScene("Basic",1);
				String scene = "Basic";
				if(player.getPure(Attribute.Ki)>=2){
					addRandomScene("Water",2);
				}
				
				/*if(player.getPure(Attribute.Ki)>=14){
				 * Super saiyan fire form
				 * 
				 * }
				 */

				if(player.getPure(Attribute.Ki)>=5){
					addRandomScene("FlashStep",2);
				}
				
				if(player.getPure(Attribute.Ki)>=11){
					addRandomScene("Stone",2);
				}

				if(player.getPure(Attribute.Ki)==2){
					scene = "Water";
				}else if(player.getPure(Attribute.Ki)==5){
					scene = "FlashStep";
				}else if(player.getPure(Attribute.Ki)==11){
					scene = "Stone";					
				}else{
					scene = getRandomScene();
				}
				if(scene.startsWith("Water")){
					Global.gui().message("You contact Suzume and agree upon a class to master your dodging technique. "
					   		+ "Unlike other times when you trained with her alone naked, the dojo is not as quiet and private when you approach it. "
					   		+ "You hear the sound of flying fists, sharp exhales and sudden energetic screams. Two girls are sparring in front of Shisho "
					   		+ "as you bow and enter the place.<p>"
					   		+ "\"Yame!\" Suzume orders the girls and invites them to greet the guest. \"This is Ashley and Tess. They are here to be your "
					   		+ "sparring partners today.\" Suzume introduces you to her students and adds, \"They have been waiting for you with anticipation, "+player.name()+"\".<p>"
					   		+ "\"Now, "+player.name()+", try undressing my girls. You are one of my... advanced students. Ashley and Tess are both novices, "
					   		+ "so I doubt either of them would be a challenge for you one-on-one.\" You try to suppress your confident grin. "
					   		+ "\"However,\" Suzume continues, \"in today's lesson you are to focus exclusively on dodging. You cannot attack your opponents or "
					   		+ "block any of their attacks. Last time, I showed you the basics of the Water Form. Move like water and evade each attack as it begins.\" "
					   		+ "That's quite a challenge, but Suzuki-shishou's lessons have always relied on a 'learn by doing' style of instruction. "
					   		+ "This may be the best way to master basic evasion.<p>"
					   		+ "Ashley is the first to face you. You adore her slender figure and her long hair cascading down her back almost to her cute butt. "
					   		+ "Her otherwise loose light-brown hair is accented by a single braid on the left side, which gives her an exotic look. Her soft and "
					   		+ "innocent face makes her far from intimidating, though she settles into a basic fighting stance with surprising comfort.<p>"
					   		+ "It is awkward and unfamiliar not being able to attack your opponent, but you have already played by these rules during one of Lilly's "
					   		+ "challenges, so it is doable. A few minutes into the fight and you can already guess that it's probably one of Ashley's first "
					   		+ "lessons in martial arts, as her moves are as clumsy and unrefined as yours were before you started training. She has the enthusiasm, "
					   		+ "but her technique shows a lack of formal training.<p>"
					   		+ "Soon you strip the girl's gi and pants, leaving her in white underwear. You take a few hits while doing so, but nothing serious. "
					   		+ "Soon you strip her bra, exposing her perky breasts and then panties as well, leaving her completely naked. Ashley covers her sweet "
					   		+ "girl parts in shame, her cheeks flush bright red.<p>"
					   		+ "\"Well done, "+player.name()+", that was impressive,\" Suzume exclaims, clapping. \"Ashley, you did quite well considering your "
					   		+ "relative inexperience. There's no reason to be embarrassed.\"<p>"
					   		+ "\"Now it's Tess's turn,\" she says. \"And she made an interesting suggestion while you two were sparring.\" Suzume smiles enchantingly, "
					   		+ "\"Since you are trying to undress her anyway, she proposed that the winner be allowed to request a sexual favor from the loser. "
					   		+ "I assume you don't have a problem with that sort of arrangement?\"<p>"
					   		+ "That is a surprising suggestion from someone outside the Games. Nevertheless, it is a wager you're extremely comfortable with. "
					   		+ "Your new opponent is quite attractive, with straight, shoulder-length, red hair. She's barely taller than the first girl, still "
					   		+ "quite a bit shorter than you, but her sharp eyes shine with confidence. You wouldn't mind some casual fun with her, win or lose.<p>"
					   		+ "Suzume asks you to strip as usual before the next sparring match. You hesitate, given your current company, but to be fair, "
					   		+ "Ashley is already naked. \"Those ladies over here have been distracted by the big bulge in your shorts since the beginning, dear,\" "
					   		+ "Suzume explains, \"I suppose they are wondering just how big your penis is. Why don't you give Tess a preview of what she can look "
					   		+ "forward to after the match? I'm sure they'd both be very pleased. More importantly, you took several hits you should have been able "
					   		+ "to avoid last match. It looks like your outfit may be interfering with your movement.\" Hesitantly you take off your clothes; "
					   		+ "your boner springs out, no longer restrained by your pants.<p>"
					   		+ "Tess is obviously more experienced in fighting. You manage to strip her off gi and pants, leaving her in white bra and panties, "
					   		+ "but she gives you a few bruises in return. Every hit tires you out and saps your endurance, but you gradually start to get the "
					   		+ "hang of moving more fluidly. Your tiredness is forcing you to stick to the minimum required movement to dodge, which actually "
					   		+ "encourages the flow Suzume showed you. Eventually, you are deep into the Water Form and Tess completely stops being able to touch you.<p>"
					   		+ "Choosing the right moment your close the distance and reach around Tess to open her bra. With a skilled motion, you unclasp it... "
					   		+ "Suddenly her arm darts out and grabs you between the legs. You only stopped moving for a moment, but it was enough. Her petite, "
					   		+ "feminine hand gets a firm grip on your tender balls. Instinctively you move your hips and close your legs, but it is too late, "
					   		+ "her fingers are clawing into your testicles from the sides, making a tight grip.<p>"
					   		+ "Panic fills your mind followed by demoralizing pain in your abdomen and testicles. You want to hit back your opponent in order to "
					   		+ "make her let you go, but you refuse to break the rules of the match. In mere seconds, your knees turn into jello as you slump on "
					   		+ "the floor and to your side, helpless and stunned, while Tess continues holding your genitals.<p>"
					   		+ "\"There is nothing you can do. I have you by the balls, big boy,\" the girl exclaims with glee in her voice, digging her fingers "
					   		+ "even deeper into your vulnerable flesh. You weakly clutch her punishing arm your eyes begging for mercy, but the girl is determined "
					   		+ "to finish you off here and now.<p>"
					   		+ "\"If you give up, I'll let go. Hurry and tap out before you faint or I won't be able to claim my payment.\" Tess's voice is pure honey, "
					   		+ "as she grinds your battered testicles against each other and her thumb.<p>"
					   		+ "You really try to resist but this kind of torture is too much, you just have to tap out. In addition, you can no longer hold back your "
					   		+ "groans coming deep from your core, your voice crackle as you scream out your surrender.<p>"
					   		+ "When Tess lets go of your vital body parts, you are finally able to hold your sore sack. With your forehead on the floor, you rock back "
					   		+ "and forth, hands buried deep between your legs nursing your poor testicles. Surprisingly, your cock is still hard despite the ball abuse "
					   		+ "you endured.<p>"
					   		+ "\"Well, it seems like you won't get my pussy then,\" says Tess and straddles your face. \"But beating you got me so hot. And now I take "
					   		+ "my prize.\" Moving her panties to the side, she queens you, rubbing her juicy vagina all over your face. Her sweet aroma takes some attention "
					   		+ "away from the dull pain between your legs. Your cock jerks treacherously as she slides her womanhood over your lips and nose, quickly "
					   		+ "bringing herself to a victorious orgasm. Soon her body is trembling, face contorted with a beautiful agony as she cums, screaming.<p>"
					   		+ "Finished, she moves down to straddle your chest, her face, still flushed, is seductively close to yours as she whispers.  \"Sorry for "
					   		+ "all that pain, I feel bad right now about busting you so hard especially after such a pleasant release\" she smiles sweetly. \"But it "
					   		+ "was my only chance to win and I guess next time you'll pay more attention to protecting your precious jewels and moving around faster "
					   		+ "before I get a hand on them.\" Your erection brushes against her ass, as she lowers down your body before standing up, her legs quivering "
					   		+ "a bit with aftershocks of pleasure. \"Oh, and I hope next time you show me how that monster cock of yours works. Those balls of yours "
					   		+ "barely fit in my hand, but I bet you're quite a shooter.\" She brushes your hardon with her bare feet a few times (which feels fucking "
					   		+ "fantastic), before leaving you overwhelmed and a little on edge.<p>"
					   		+ "Suzume congratulates her student and then dismisses both girls to the changing room. \"Don't get mad.\" Suzume kneels near you to calm "
					   		+ "you down, \"I know it sure was a tough lesson for you, but Water Form is a difficult technique, you can't get it instantly.\" She reaches "
					   		+ "down and gently strokes your pent-up cock. \"But I can tell you, eventually you will succeed.\"<p>"
					   		+ "Your balls still ache slightly, but it's overshadowed by a wave of pleasure as Suzume's soft hand makes you ejaculate with a groan. "
					   		+ "\"Good. It looks like everything still works down there. I'm glad Tess knows how to hold back.\" She gives you an affectionate kiss on "
					   		+ "the cheek, which she often does when you perform exceptionally well. \"You improved a lot over the course of two matches and you really "
					   		+ "helped motivate those girls. I think I should have you train together again.\"");

				}else if(scene.startsWith("Stone")){
					Global.gui().message("Today, you and Suzuki-shisho are joined by three girls. <i>\"You remember Ashley and Tess. This is Dawn.\"</i> " + 
						  "Suzume gestures to an unfamiliar girl with short, black hair. The girl gives you a friendly wave. <i>\"She recently joined the basic class and "
						  + "will be participating in this joint exercise along with the rest of you. They have been studying offensive techniques and you have been focusing "
						  + "on defensive techniques recently, so this is a nice opportunity for both classes to apply their training.\"</i><p>"
						  + "She gestures in your direction. <i>\"You, strip naked.\"</i> Then to the girls. <i>\"You three, bra and panties.\"</i> "
						  + "The girls start undressing without showing any surprise at the commands. Suzuki-shisho must have discussed this with them beforehand. "
						  + "They are obviously more interested in watching you undress than their own half-nakedness. It's still embarrassing getting naked in front "
						  + "of three giggling girls, but you just pretend it's the start of a match.<p>"
						  + "<i>\"Ok, this will be a simple three on one match. "+player.name()+" wins if he can successfully undress all three girls. Each of the girls "
						  + "will attempt to land at least one groin attack before that happens. Since the girls are acting as a team, all three of you must succeed for "
						  + "any of you to receive credit.\"</i> Tess makes a grasping gesture and gives you a wink, obviously "
						  + "anticipating a repeat of her previous win. Suzuki-shisho catches the gesture and nods. <i>\"Yes, grabbing the testicles does count as a "
						  + "successful attack. Grabbing the penis does not count, but it can provide a significant advantage. I'll elaborate on that in future "
						  + "lessons.\"</i><p>"
						  + "You pull her aside to talk as the girls start stretching. This three on one idea seems to be impossible for you. You lost against "
						  + "Tess one on one. <i>\"I think you underestimate how much you've improved. If you make use of everything I've taught you, I'd give "
						  + "you almost even odds against those three.\"</i> She gives your shoulder a light squeeze before moving between you and the girls.<p>"
						  + "Once she confirms everyone is ready, she raises her arm and brings it down to signal the start of the match. <i>\"Hajime!\"</i><p>"
						  + "Tess and Dawn dash forward, hoping for a quick win, but you're faster. Channeling your ki into the floor, you flash step past the "
						  + "girls and land behind Ashley. You steal her bra before she has time to react. <i>\"Kya!\"</i> Ashley squeals in surprise and embarrassment, "
						  + "stumbling and falling to the floor as she covers her bare breasts. You could go for her panties, but Tess and Dawn have heard their "
						  + "friend's distress and are rushing to assist. You're forced to retreat as Tess jumps between you and your target. <i>\"Ash, stay back! "
						  + "We'll soften him up.\"</i><p>"
						  + "It would have been nice to completely strip someone with your surprise attack, but you've at least taken Ashley out of the fight temporarily. "
						  + "Two opponents are a lot easier to handle. You use your water form to evade Tess's while keeping her between Dawn and yourself. As she "
						  + "lashes out with a particularly aggressive kick, you circle to her left and undo her bra clasp. She holds her bra on with both hands, preventing "
						  + "you from completely removing it, but leaving her defensele-<p>"
						  + "Whoa! You barely react in time when she thrusts her butt into your groin. You almost take the hip check directly to the balls, but manage to "
						  + "sway out of the way. She has good instincts, that attack was barely telegraphed at all. Unfortunately, you lose your chance to strip her bra. "
						  + "Fortunately, it'll probably take her a good ten seconds to fix it. That might just be enough time to strip Dawn.<p> "
						  + "You rush past Tess, catching Dawn off-guard. Channeling ki into both hands, you grab her bra-covered breasts. She lets out a soft gasp as her "
						  + "bra falls to pieces in your hands. She recoils back in alarm, confused about your technique. You're about to take advantage when a kick from "
						  + "behind connects with your sensitive balls.<p>"
						  + "You groan in intense pain and wheel around to see a topless Tess pulling her leg back. She discarded her bra rather than take the time to "
						  + "fix it? You hadn't considered that. <i>\"I tagged him! Go for it, Dawn!\"</i> Dawn recovers from her surprise at the sound of her teammate's "
						  + "voice and lunges at you. You try to get away, but the pain in your groin is preventing your legs from responding. The girl catches you around "
						  + "the waist and grabs your balls, squeezing hard. It's not as bad as when Tess got you last fight, but it's more than enough to stop you in your "
						  + "tracks.<p>"
						  + "<i>\"Got him! He's done!\"</i><br>"
						  + "<i>\"Ash, get over here and give him a kick so we can win!\"</i> You don't like the sound of that. You try to break free, but after another squeeze, "
						  + "you're struggling not to just collapse into the fetal position. If you're going to keep fighting, you need to find a way to handle this pain.<p>"
						  + "You think back to the Suzume's meditation. With enough suggestion, you could become so numb that you wouldn't feel a thing. She once described "
						  + "a technique almost the opposite of the Water Form, where you make yourself as tough as stone. Maybe that's the trick.<p>"
						  + "Ashley makes her way to you, and Dawn lets you go so she can line up a kick. You don't have time for pain to slow you down, so you push it away, "
						  + "as if your body was made of solid rock instead of vulnerable flesh. Without hesitating, you lunge out of Dawn's grip and drop into a crouch, grabbing "
						  + "her panties with one hand and Ashley's with the other. You pull both pairs of panties down and the naked girls trip and fall in surprise.<p> "
						  + "Only Tess has any clothing remaining now. You're about to rush her, but surprisingly Ashley jumps in front of you. <i>\"Stay back, Tess! As long "
						  + "as he can't get to your underwear, we'll get him eventually. I just need to bust him once.\"</i> She attacks you aggressively, giving you no chance "
						  + "to get to Tess. It's still not too difficult to block her attacks, but she has nothing to lose and eventually she's bound to get a lucky hit. "
						  + "Dawn is also getting back to her feet behind you. You need to act fast.<p>"
						  + "Ashley seems the most sexually inexperienced of the group. Even if she's being aggressive right now, you can probably use that against her. You "
						  + "close the distance with her suddenly, pressing your naked body against hers so she doesn't have room to attack. She freezes in place and blushes "
						  + "bright red as she feels your boner pressing against her nethers. You're able to spin her around without her noticing, giving you a clear path to "
						  + "Tess.<p>"
						  + "Tess has put some room between herself and the melee, but you're able to reach her with a single flash step. Ashley's reaction a moment ago made "
						  + "it clear that Suzuki-shisho hasn't been giving them any sexual training. That's an area where you have a huge advantage. You kiss Tess firmly on "
						  + "the lips while slipping your hand into her panties to play with her pussy. She moans against your mouth and forgets to struggle against you. She "
						  + "doesn't even seem to notice as you slide her panties down her legs.<p>"
						  + "Suzuki-shisho steps forward and raises her arm. <i>\"That's it! "+player.name()+" is the winner! Good work everyone. If I can offer one piece "
						  + "of advice: Tess and Dawn, when you had the boy captured, one of you should have tried stroking his penis. He would have been much too distracted "
						  + "to escape. You can head home once you've collected your "
						  + "surviving clothes.\"</i><p>"
						  + "Ashley and Dawn retrieve their underwear with some embarrassment and resignation. They both seem preoccupied trying "
						  + "to figure out how they lost despite their numerical advantage, but you can see a familiar determination to improve in their eyes. You're "
						  + "pretty sure there's no risk of either of them quitting.<p>"
						  + "Tess, on the other hand, follows you as you move to retrieve your clothes. <i>\"Aren't you going to finish what you started? You can't just "
						  + "finger me until I'm hot and bothered and expect me to leave unsatisfied.\"</i> She stands in front of you, naked and aroused, but with her "
						  + "hands on her hips and eyes full of fire. You can't help chuckling a bit at the sight. She's suddenly reminding you a lot of another red-head "
						  + "you know. <i>\"Oh yeah? Is that a compliment or an insult?\"</i><p>"
						  + "Suzume approaches with a giggle. <i>\"I know exactly who he means. Trust me, it's a good thing.\"</i><p>"
						  + "Tess nods, satisfied by the answer. <i>\"Good. I won't need to kick you twice in one day.\"</i> She pulls your head down to hers, kissing "
						  + "you passionately, while she starts stroking your cock with her other hand. You're caught off-guard. Suzume is only a couple feet away, "
						  + "watching with an amused smile. Ashley and Dawn have both stopped getting dressed so they can watch from a distance. Still, some of Tess's "
						  + "challenging attitude gets to you, and you'll be damned if she's going to make you cum first in front of all these people.<p>"
						  + "You slip two fingers into her nether lips and pleasure her with your best fingering techniques. She gives a pretty decent handjob, but she "
						  + "doesn't have your experience. You have her shuddering and moaning in orgasm a solid 30 seconds before she's able to make you cum. Tess "
						  + "excuses herself to clean up afterwards, walking away with shaky legs. The other two girls quickly retreat too, trying to pretend they "
						  + "weren't watching.<p>"
						  + "Suzume waits quietly until the two of you are the only ones in the dojo before she grabs your arm with giddy excitement. <i>\"Those girls "
						  + "have so much potential! Especially Tess, but even Ashley impressed me today. They are sure to be picked next time the Games are recruiting. "
						  + "Having five participants in the games as members of my dojo would be huge!\"</i> She's practically hopping up and down. It's adorable. "
						  + "She was maintaining a respectable facade in front of her novice students, but she doesn't seem to have any such concerns about you.<p>"
						  + "She blushes slightly at your teasing and gives you a cute pout. <i>\"Don't think I didn't see you improvise a new technique in the middle "
						  + "of the match. You learned the Stone Form without any guidance. I guess you don't need your shisho anymore, do you?\"</i> You give her a "
						  + "peck on the lips and she smiles. It's nice that she's treating you as an equal, but you're sure she still has plenty to teach you.");
				}
				else if(scene.startsWith("FlashStep")){
					Global.gui().message("You have another private lesson in the Dojo, though it's far from the intimate experience you sometimes have. Today, Suzume's trying "
							+ "to teach you a new movement technique, which has involved you sprinting back and forth across the length of the dojo until you feel ready to "
							+ "collapse.<p>"
							+ "<i>\"Yame.\"</i> Suzuki-shisho gives you a brief break. You hold a ready stance, as trained, while using this opportunity to recover your stamina.<br>"
							+ "<i>\"Your physical movement is pretty good, but you need to synchronize your muscle control with your Ki. Your Ki should flow into the floor just as "
							+ "your calf and ankle apply their maximum force. Relax for just a moment and let me check your leg muscles.\"</i><p>"
							+ "She kneels in front of you and runs her hands over your legs. You're both nude, as usual, and you can't help being distracted by her closeness. "
							+ "You do your best to remain disciplined, even as her hands slide up your thigh.<br>"
							+ "<i>\"Your muscles feel toned enough to manage a flash step, but you Ki is still lacking.\"</i><br>"
							+ "Despite your best efforts, your eyes stray to her slim, nude body. Her body is firm and toned, but her hands feel soft and feminine. As she "
							+ "leans in, her short hair gently tickles your genitals.<p>"
							+ "<i>\"Your Ki seems to be flowing better all of a sudden. Oh, hello there.\"<i> Suzume glances up at your bobbing erection, mere inches from "
							+ "her head. Her surprised expression gives way to a mischievous grin as she stands up and pulls you into a soft embrace. <i>\"I wasn't trying to "
							+ "turn you on, do you lust after your shisho so much?\"<i> You share a brief kiss, and she barely moves her face away when it ends. You can feel "
							+ "her breath on your lips.<p>"
							+ "<i>\"My body isn't very feminine, so it's nice to feel wanted.\"</i> Suzume lets her nether lips brush against your dick so your can feel her wetness. "
							+ "<i>\"Your passionate gaze has given me an idea for your training. I think you just need the proper motivation. You just need to catch me and stick "
							+ "your hard 'chinko' in my wet 'omanko'. Sound good?\"</i><p>"
							+ "Your Japanese isn't great, but that requires no translation. Fortunately, the head of your dick is already lined up with her entrance, so it takes only "
							+ "a single thrust to fill her up...<p>"
							+ "Or so you thought. The instant you thrust your hips, Suzume smoothly pulls away, denying you penetration. Before you can catch her, she leaps backward, "
							+ "putting a few steps between you. <i>\"It's not going to be that easy. You're going to need to be quicker if you want to fuck me.\"</i><p>"
							+ "As you dash forward to catch her, she continues dodging you with the agility of a dancer. You quickly recognize her using the Water Form to avoid you. "
							+ "It's going to be tough to slow her down.<p>"
							+ "One of your lunges gets you particularly close, but Suzume slips past you and grabs you from behind. She gives your sensitive dick a few quick strokes "
							+ "before releasing you. <i>\"It goes without saying, but if you cum before putting it in, I'll be very disappointed.\"</i><p>"
							+ "You need to focus your movements. Suzume's dodges appear effortless. If you continue rushing forward clumsily, it'll just wear you out. She's currently "
							+ "four steps away, but you need to close the distance in one movement. You lunge toward Suzume in a sudden burst of effort, and she immediately disappears "
							+ "from your vision. Is she that much faster than you? No, she's behind you, but she hasn't moved. You just overshot her.<p>"
							+ "It takes you a moment to realize you crossed more than half the dojo in a single step. Suzume looks surprised, but pleased. <i>\"That was it! You performed "
							+ "an almost perfect flash step! You just need to focus more on your destination, so you don't over-\"</i><p>"
							+ "You don't give her a chance to finish the sentence. You flash step directly behind her and thrust your dick into her waiting pussy. Suzume screams in pleasure "
							+ "as you fill her instantly.<p>"
							+ "<i>\"Yes! Right there!\"</i> You pound Suzume from behind like a wild beast, reaching one hand around to rub her clit. You pour all of the frustration from her "
							+ "teasing into your hip movement. She moans with wanton abandon as your fuck her hard. You feel her clench around you as you shoot your load into her womb.<p>"
							+ "You both collapse to the floor together, breathing hard. After about thirty seconds, Suzume is the first to speak.<br>"
							+ "<i>\"That was a good training session. I should remember that for next time.\"</i> She rolls over to face you and gives you a deep kiss. <i>\"I don't "
							+ "play favorites with my students, but you're the only one who can fuck me like that.\"</i>");
				}
				else{
					Global.gui().message("Suzuki-shisho insists that you train in the nude. She claims it's a tribute to an old Japanese grappling tradition. You're about 95% certain she's " +
						"lying. Fortunately the dojo doesn't appear to have any other students, so the two of you have plenty of privacy. It's also closer to the circumstances you're " +
						"normally fighting in.<p><i>\"Your Ki skills can be very useful, but be careful to pace yourself or you may run out of stamina.\"</i>");
				}
				player.money-=player.getAdvancedTrainingCost();
				player.mod(Attribute.Ki, 1);
				Roster.gainAffection(ID.PLAYER,ID.SUZUME,1);
				Global.gui().message("<b>You've gained Affection with Suzume</b>");
				acted=true;
				if(!player.has(Clothing.kungfupants)){
					player.gain(Clothing.kungfupants);
				}
				Global.gui().choose(this,"Leave");
			}
			else{
				Global.gui().message("You don't have enough money for training.");
				Global.gui().choose(this,"Sharpen Senses");
				Global.gui().choose(this,"Shut Out Sensation");
				Global.gui().choose(this,"Leave");
			}			
		}
		else if(choice=="Sharpen Senses"){
			if(Global.random(100)>=50){
				Global.gui().message("Suzume instructs you to sit in the middle of the dojo and close your eyes. <i>\"I'm going to count down from ten. With each number, you will feel your " +
					"mind opening and you will be more receptive to my words. When I reach zero, imagine your mind as an empty vessel, ready to take in everything around you.\"</i> As she " +
					"counts down, you can feel yourself slipping into a trance. You know you could pull back at any time, but that would defeat the entire purpose of this, so instead you " +
					"let yourself go. <i>\"I want you to imagine a thick fog has surrounded you your entire life, shrouding the world and dulling your senses. When I snap my fingers now, the fog " +
					"will clear and your senses will be sharper than ever before. You'll be able to see details you never would have been able to make out. You'll be able to hear a pin drop " +
					"in a storm. The entire universe will rush in through your skin and you'll perceive it all.\"</i><p>Suzume snaps her fingers and instantly everything is different. Your eyes " +
					"are still closed, but you feel like you can perceive the shape of the room around you. Suzume has been pacing behind you, but her footfalls were too light for you to notice. " +
					"now you can pinpoint her exact location and movement. You hear a rustle of cloth from her direction and realize she's taking off her socks. <i>\"We're going to give your heightened " +
					"senses a little practice so you can adapt to them. Keep your eyes closed, but you can speak now. What am I doing?\"</i> When you reply that she's removing socks, you can somehow " +
					"sense her smile. <i>\"Very good, what about now?\"</i> Another rustle; shirt and pants this time. Then bra. Finally you hear the whisper of her panties sliding down her legs. Even " +
					"though you can't see her, you can perceive every move she makes as if she were performing a strip show. Your erection strains against your pants as you hear her nude form approach " +
					"you. When she gets close, you can feel the warmth of her body heat and catch the faint scent of feminine arousal.<p><i>\"I want you to wake up completely, but keep your eyes forward. " +
					"I'm a modest girl after all. You probably feel a bit like a superhero right now, but I'm going to demonstrate your new vulnerability.\"</i> She takes hold of your hand and caresses " +
					"your palm, causing you to jump at the unexpected sensation. <i>\"Your sense of touch is turned up so high right now that your entire body is an erogenous zone. Any place I touch will " +
					"feel the same as the head of your penis.\"</i> She brings your hand to her mouth and kisses the tip of your index finger. You can't help groaning as the pleasure is transmitted throughout " +
					"your body. When she puts your finger in her mouth and begins to lick it, it does feel like she's giving you a blowjob. You manage to endure her 'fingerjob' for about thirty seconds " +
					"before you hit your peak and cum in your pants. Suzume giggles when she realizes what happened. <i>\"I probably should have warned you to bring a change of underwear with you. Don't worry, " +
					"your hypersensitivity will level out over the next hour or so. You'll be a bit more perceptive, but not like you are now.\"</i>");
				if(player.getPure(Attribute.Perception)<9){
					player.set(Attribute.Perception,player.getPure(Attribute.Perception)+1);
				}
			}
			else{
				Global.gui().message("Suzume instructs you to sit in the middle of the dojo and close your eyes. <i>\"I'm going to count down from ten. With each number, you will feel your " +
					"mind opening and you will be more receptive to my words. When I reach zero, imagine your mind as an empty vessel, ready to take in everything around you.\"</i> As she " +
					"counts down, you can feel yourself slipping into a trance. You know you could pull back at any time, but that would defeat the entire purpose of this, so instead you " +
					"let yourself go. <i>\"I want you to imagine a thick fog has surrounded you your entire life, shrouding the world and dulling your senses. When I snap my fingers now, the fog " +
					"will clear and your senses will be sharper than ever before. You'll be able to see details you never would have been able to make out. You'll be able to hear a pin drop " +
					"in a storm. The entire universe will rush in through your skin and you'll perceive it all.\"</i><p>When Suzume snaps her fingers, you immediately realize something is wrong. " +
					"You feel a sensation like your foot falling asleep but on every inch of your skin. You fidget in discomfort and as soon as you move, the sensation become unbearably " +
					"painful. You grit your teeth and struggle to remain completely still, while Suzume hurriedly brings you out of the trance. She squeezes your hand painfully tight and " +
					"eases you into a prone position. <i>\"I know this hurts, but focus on the pain from your hand and the rest of it will go away faster.\"</i> You take her advice and soon the " +
					"sensitivity in the rest of your body dies down. She releases your hand when you finally feel normal again. <i>\"Sorry, sometimes these techniques don't go the way we plan. " +
					"If you want, we can try again another time, but for now you need some time to recover.\"</i>");
			}
			Roster.gainAffection(ID.PLAYER,ID.SUZUME,1);
			Global.gui().message("<b>You've gained Affection with Suzume</b>");
			acted = true;
			Global.gui().choose(this,"Leave");
		}
		else if(choice=="Shut Out Sensation"){
			if(Global.random(100)>=50){
				Global.gui().message("Suzume has you lie down on the cold, somewhat uncomfortable floor. <i>\"Now please close your eyes while I count down from ten. I want you to imagine a set of stairs and "+
						"with at each count I want you to take one step down. As you descend, everything in the world will fade away except my voice.\"</i> She begins to count down and you imagine yourself "+
						"walking down the stairs. By the time she finishes counting, you no longer feel the hard floor beneath you. There's no sensation and no ambient noise. The only thing that matters "+
						"right now is Suzume's voice. <i>\"Your body is your fortress. Nothing can reach you until you allow it. Just keep out everything that doesn't matter.\"</i><p>Suzume stops talking and "+
						"you find yourself alone in your mind. With no sounds to track the time, you aren't sure how long you're waiting before she speaks again. <i>\"How are you feeling right now? You can "+
						"speak.\"</i> You tell her that you are feeling pretty numb, but it's hard to tell how effective the meditation is. You hear Suzume giggle quietly. <i>\"Stay calm, open your eyes, and look"+
						"down.\"</i> You open your eyes and raise your head. Your pants and boxers are gone, and Suzume is gripping your balls tightly. This should be cause for alarm, but her command to 'stay calm' "+
						"is working quite well. <i>\"Don't worry, I would never damage someone in my care. You would, however, be in a lot of discomfort if the suggestion wasn't working.\"</i> She releases your "+
						"genitals, helps you to your feet, and hands you your missing clothes. <i>\"Your sensitivity should already be starting to return. You'll keep some of your endurance, but you'll probably "+
						"feel it when someone undresses you.\"</i>");
				if(player.getPure(Attribute.Perception)>1){
					player.set(Attribute.Perception,player.getPure(Attribute.Perception)-1);
				}
			}
			else{
				Global.gui().message("Suzume has you lie down on the cold, somewhat uncomfortable floor. <i>\"Now please close your eyes while I count down from ten. I want you to imagine a set of stairs and "+
						"with at each count I want you to take one step down. As you descend, everything in the world will fade away except my voice.\"</i> She begins to count down and you imagine yourself "+
						"walking down the stairs. By the time she finishes counting, you no longer feel the hard floor beneath you. There's no sensation and no sound.<p>There's nothing. You feel completely "+
						"separated from your body and lost in the void. Forget about the meditation, you need to wake up now! Nothing happens. You can't find your way back to the world and your body. You begin "+
						"to panic, but suddenly you can see Suzume's face, very close to yours and looking worried. <i>\"Can you hear me? You went too deep and lost my voice. It's not a good idea to be so deep inside "+
						"yourself without something to guide you back. Do you feel ok now?\"</i> You affirm that you're fully awake and feel normal. She gives a quiet sigh of relief. <i>\"We should stop for now. I " +
						"don't think we can salvage this session. Give me a call if you want to try again.\"</i>");
			}
			Roster.gainAffection(ID.PLAYER,ID.SUZUME,1);
			Global.gui().message("<b>You've gained Affection with Suzume</b>");
			acted = true;
			Global.gui().choose(this,"Leave");
		}
	}
	public String toString(){
		if(Global.checkFlag(Flag.dojo)){
			return "Dojo";
		}
		else{
			return name;
		}
	}

	@Override
	public void shop(Character npc, int budget) {
		if(npc.getPure(Attribute.Ki)>0&&budget>=npc.getAdvancedTrainingCost()){
			if(budget>=npc.getAdvancedTrainingCost()*2.5){
				npc.money-=npc.getAdvancedTrainingCost();
				budget-=npc.getAdvancedTrainingCost();
				npc.mod(Attribute.Ki, 1);
			}
			npc.money-=npc.getAdvancedTrainingCost();
			budget-=npc.getAdvancedTrainingCost();
			npc.mod(Attribute.Ki, 1);
		}
		int r = Global.random(4);
		if(r==3&&npc.getPure(Attribute.Perception)<9){
			npc.mod(Attribute.Perception, 1);
		}
		else if(r==2&&npc.getPure(Attribute.Perception)>1){
			npc.mod(Attribute.Perception, -1);
		}
	}

}
