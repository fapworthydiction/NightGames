package daytime;

import java.util.HashMap;

import characters.Character;
import global.Flag;
import global.Global;
import global.Scheduler;

public class MaraEvent implements Scene {
	private int scene;
	private Character player;
	
	public MaraEvent(Character player){
		scene = 1;
	}
	
	@Override
	public void respond(String response) {
		if(response=="Next"){
			scene++;
			play("");
		}else if(response=="Leave"){
			Scheduler.getDay().plan();
		}
	}

	@Override
	public boolean play(String response) {
		switch(scene){
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		}
		return true;
	}

	@Override
	public String morning() {
		if(Global.checkFlag(Flag.MaraDayOff)){
			return "DayOff";
		}else{
			return "";
		}
	}

	@Override
	public String mandatory() {
		return "";
	}

	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		return;
	}


}
