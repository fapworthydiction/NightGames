package daytime;
import global.Global;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

import characters.Character;
import global.Scheduler;

public abstract class Activity {
	protected String name;
	protected LocalTime time;
	protected Character player;
	protected int page;
	protected String tooltip;
	private HashMap<String,Integer> randomScenes;
	
	public Activity(String name,Character player){
		this.name=name;
		this.time=LocalTime.of(1,0);
		this.player=player;
		this.page=0;
		this.randomScenes = new HashMap<String,Integer>();
		this.tooltip = "";
	}
	public Activity(String name,Character player, String tooltip){
		this(name, player);
		this.tooltip = tooltip;
	}
	public abstract boolean known();
	public abstract void visit(String choice);
	public LocalTime time(){
		return time;
	}
	public void next(){
		page++;
	}
	public void done(boolean acted){
		if(acted){
			Scheduler.advanceTime(time);
		}
		page=0;
		Scheduler.getDay().plan();
	}
	public String toString(){
		return name;
	}
	public abstract void shop(Character npc, int budget);
	public void clearRandomScenes(){
		randomScenes.clear();
	}
	public void addRandomScene(String scene, int weight){
		randomScenes.put(scene, weight);
	}
	public String getRandomScene(){
		if(randomScenes.isEmpty()){
			return "";
		}else{
			int totalWeight = 0;
			int count = randomScenes.size();
			for(Integer i: randomScenes.values()){
				totalWeight += i;
			}
			int sum = 0;
			int r = Global.random(totalWeight);
			for(String scene: randomScenes.keySet()){
				sum += randomScenes.get(scene);
				if(r < sum){
					return scene;
				}
			}
			return randomScenes.keySet().iterator().next();
		}
	}
	public String tooltip(){
		return tooltip;
	}
}
