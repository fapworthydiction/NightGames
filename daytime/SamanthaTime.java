package daytime;

import characters.Character;
import characters.Dummy;
import characters.Emotion;
import characters.ID;
import global.Flag;
import global.Global;
import global.Roster;

public class SamanthaTime extends Activity {
	private boolean acted;
	private Dummy sprite;
	
	public SamanthaTime(Character player) {
		super("Samantha", player);
		sprite = new Dummy("Samantha");
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.Samantha);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		sprite.dress();
		sprite.setBlush(0);
		sprite.setMood(Emotion.confident);
		if(choice=="Start"){
			acted = true;
			if(Roster.getAffection(ID.PLAYER,ID.SAMANTHA)>5){
				Global.gui().loadPortrait(player,sprite);
				Global.gui().message("You pause to admire your reflection in the shop window, and adjust your tie just so.<p>" +
						"Samantha is at her usual booth, poised in front of her laptop with one finger on the return key. She's got the hood of her sweatshirt up, and has tuned out the world around herself, to the point where she doesn't notice you when you sit down across from her.<p>" +
						"A couple of minutes crawl by. You ask what she's doing, but she doesn't react until...<p>" +
						"She punches her keyboard with a victorious smile, then sits back and takes a sip from her mug. That's when she and you make eye contact.<p>" +
						"<i>\"Hello, "+player.name()+",\"</i> Samantha says. <i>\"Job interview today?\"</i><p>" +
						"That's a little disappointing. You bought this suit in an attempt to class your look up a little bit, and you say as much.<p>" +
						"<i>\"Where'd you buy it? That's clearly off the rack.\"</i><p>" +
						"The local men's clothing store, you have to admit, and yeah, you just bought something in your size.<p>" +
						"<i>\"You look like you're wearing your dad's clothes,\"</i> Samantha says. <i>\"Here, stand up a second.\"</i><p>" +
						"You do, and she eyes you over the top of her reading glasses.<p>" +
						"<i>\"The sleeves are too long,\"</i> she says. <i>\"So are the pant legs, and the shoulders on the jacket are just a little too broad for you. You should've put down the extra money for custom tailoring. Rookie move.\"</i> It's a little bit of a tease, but no more. <i>\"The color does work for you, though. Noble first effort.\"</i><p>" +
						"You felt really good about this five minutes ago. The last time Samantha kicked you in the balls was somehow less emasculating than this conversation.<p>" +
						"In an attempt to change the subject, you sit back down and ask what she was doing when you walked in.<p>" +
						"<i>\"Oh, I'm day-trading,\"</i> she says. <i>\"It's basically a slot machine for stockbrokers. I shouldn't bother, but it's a rush when you win, so sometimes I dabble.\"</i><p>" +
						"You vaguely recall hearing boring older relatives talking about it.<p>" +
						"In an attempt to recoup some of your pride, you mention you just invested in a mutual fund yourself. Samantha got you thinking in that direction, so you did a little reading and put some money down.<p>" +
						"She pushes her glasses back up her nose with the tip of her thumb, then pulls her laptop a little closer. <i>\"Which one?\"</i><p>" +
						"You mention it, hoping for an impressed reaction. You don't get one.<p>" +
						"<i>\"Oh,\"</i> Samantha says.<p>" +
						"That can't be good.<p>" +
						"<i>\"This... isn't great,\"</i> she says, and turns the laptop to face you.<p>" +
						"It takes you a second, and a quick bit of research on your own phone, before you catch up with her. The fund you invested in is considered high-risk and low-yield; there are surprisingly few companies in its portfolio, and the fund manager has a nasty reputation. You didn't think to look him up before you put money down.<p>" +
						"<i>\"Don't feel too bad,\"</i> Samantha says. <i>\"It's easy for a new investor to get led astray. I can set you up with some better reading.\"</i><p>" +
						"That sounds good, and you say as much. You hope you don't sound too much like a kicked puppy when you do.<p>" +
						"<i>\"Is this something you're interested in?\"</i> she asks. <i>\"Or is this just for my sake?\"</i><p>" +
						"Lying to her seems like it'd backfire in the long run. It's a little from column A, a little from column B. Your income from the Games is high enough that what Samantha said made some sense.<p>" +
						"She takes off her glasses and taps the end of one arm against her teeth, looking at her laptop screen. <i>\"That's... kind of cute, actually. What's your email address?\"</i><p>" +
						"You give it to her.<p>" +
						"<i>\"I need to jump back into this,\"</i> Samantha says. <i>\"Keep an eye on your inbox, okay? We'll talk later.\"</i><p>" +
						"She doesn't wait for you to respond before becoming absorbed in her laptop again. You've just been dismissed. You try not to sulk on your way back out of the shop.<p>" +
						"A couple of hours later, you get half a dozen new emails from what you suspect might be a throwaway account. Two are lists of links to men's fashion sites, detailing do's and don'ts; the other four are a series of articles on finance and investment, with terse annotations.<p>" +
						"You just got several hours of additional homework from a high-class hooker. Life is weird.");
			}else {
				Global.gui().loadPortrait(player,sprite);
				Global.gui().message("This coffee shop is always crowded, due to the food. You only made it inside at all because it's mid-afternoon. You'd need a whip and a chair to get through the breakfast line. Apparently the pastries here can change your life, but you've got better things to do than show up at 4:30 in the morning to camp out for a donut.<p>"
						+ "Samantha has a seat at a booth against the shop's back wall, and you barely manage to spot her. Not only is the crowd making it difficult, but she's almost unrecognizable.<p>"
						+ "She always shows up to the Games in an expensive dress, silk stockings, and perfect makeup, with everything just so. It's a testament to her skill, now that you think about it, that she stays that way more often than not. You can't keep a pair of pants intact for more than ten minutes, but she can come out of the competition with the same silk dress she was wearing when she walked in.<p>"
						+ "This is Samantha on her own time, however, wearing tight workout pants, sneakers, and a baggy hooded sweatshirt. Her hair is pulled into a messy knot on the back of her head with a couple of pencils through it to hold it in place. Her face is clean and shiny, without makeup, and she peers at you for a moment over the top of a pair of reading glasses. If she didn't clearly recognize you, you'd think she was another person entirely.<p>"
						+ "She closes her laptop and waves you over, picking up her mug in both hands. You sit down across from her.<p>"
						+ "<i>\"And how are you today, " + player.name() + "?\"</i> Samantha asks.<p>"
						+ "You shrug and ask what she's up to.<p>"
						+ "<i>\"Day-trading, mostly.\"</i><p>"
						+ "You weren't sure what she was going to say, but it certainly wasn't that. Samantha catches your momentary confusion on the topic and chuckles, but not like she actually thinks you're all that funny.<p>"
						+ "<i>\"The services I provide are very youth-driven, love. A smart professional starts thinking about her retirement plan very early, and I never wanted to end up as some old millionaire's trophy wife.\"</i> She taps the lid of her laptop with one finger. <i>\"So I make investments with the money from the Games, and babysit them carefully.\"</i> Samantha lowers her glasses and peers at you over their rims. <i>\"You ought to be thinking along similar lines.\"</i><p>"
						+ "It's not something that had occurred to you.<p>"
						+ "<i>\"Of course not. You're a college freshman. This is the most life experience you've ever had.\"</i> She isn't insulting you. It's a plain statement of fact, sledgehammer-blunt. <i>\"But no situation lasts forever. No one stays young. You may be a sexual freak of nature, but one day you won't be.\"</i><p>"
						+ "You don't actually know how much older Samantha is than you are. You've been assuming that it can't be much more than a few years. There's something about how she's speaking, though, that suggests she's seen and done a lot in those few years.<p>"
						+ "You want to ask her about that, to get some idea of what makes someone like her, if only for the same grab-bag of reasons that you've tried to get to know the other participants in the Games.<p>"
						+ "While you're scrambling for a way to put that into words, though, Samantha opens her laptop and goes back to work, just as if you aren't there. It isn't a hint; it's a dismissal. You wave away a waitress and leave the coffee shop, still thinking about how to put together that sentence.");
			}
			Roster.gainAffection(ID.PLAYER,ID.SAMANTHA,1);
			Global.gui().message("<b>You've gained Affection with Samantha</b>");
			Global.gui().choose(this,"Leave");
		}
		else if(choice=="Leave"){
			Global.gui().showNone();
			done(acted);
		}

	}

	@Override
	public void shop(Character npc, int budget) {
		// TODO Auto-generated method stub

	}

}
