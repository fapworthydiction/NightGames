package daytime;

import items.Component;
import items.Consumable;
import items.Item;
import items.Potion;
import global.Flag;
import global.Global;


import characters.Character;

public class Bookstore extends Store {
	public Bookstore(Character player) {
		super("Bookstore", player);
		add(Potion.EnergyDrink);
		add(Consumable.ZipTie);
		add(Component.Phone);
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.basicStores);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(choice=="Start"){
			acted = false;
		}
		if(choice=="Leave"){
			done(acted);
			return;
		}
		checkSale(choice);
		if(player.human()){
			if(player.getRank()>=1 && !Global.checkFlag(Flag.metGinette)){
				Global.gui().message("There’s a small commotion in the campus bookstore when you walk inside. A petite girl in a hoodie is arguing with the man at the service desk. <br>" +
						"<br>" +
						"<i>\"Look, we’re short exactly one box. I can see from the order tracking that it arrived here yesterday, but it never made it to the Student Union. Either it got delivered to the wrong building, or you still have it in the back somewhere.\"</i> You can’t see much of the girl’s face between her hood and glasses, but her frustration is clear in her voice.<br>" +
						"<br>" +
						"The man at the service desk has the patient look that says this encounter has lasted much longer than he hoped.<br>" +
						"<i>\"I’m sure we don’t currently have any packages for the Student Council. We already sent everything we received to the Student Union, exactly the same as we do every week. However, if it makes you feel better, I’ll check one more time.\"</i><br>" +
						"<br>" +
						"He moves to the back room, out of sight. To your eyes, it looked like he was more interested in getting away from the desk than actually looking for the missing delivery. The girl curses quietly, apparently thinking the same thing.<br>" +
						"<br>" +
						"She glances your direction, so you can finally get a good look at her face. She’s kinda cute, in a nerdy sort of way. Red hair, green eyes, freckles. She’s wearing glasses and has headphones around her neck.<br>" +
						"<br>" +
						"When your eyes meet, she blushes bright red and looks away. She’s probably embarrassed to be causing a scene in public. Either that or the Games have improved your charms more than you realized.<br>" +
						"<br>" +
						"You originally came here to buy some supplies, but now this girl has you curious. You decide to hang around and see how this resolves.<br>" +
						"<br>" +
						"A few minutes later, the man returns carrying a large box. His expression says that he was really hoping this situation wasn’t going to end up being his fault.<br>" +
						"<i>\"Somehow this package ended up mixed in with the chemistry supplies. I’ll have it sent over to the Student Union today. Sorry for the inconvenience.\"</i><br>" +
						"<br>" +
						"The frustrated girl grabs the box from his hands. <i>\"I’ll just take it over myself. I’m not risking it getting misplaced again.\"</i> She walks away, clearly struggling with the weight of the package. This seems like an appropriate time to intervene. You don’t have any place you need to be right now, so you offer to carry the box for her. She hesitates for a moment, but shyly accepts your help.<br>" +
						"<br>" +
						"…<br>" +
						"<br>" +
						"The girl becomes much more reserved when you’re walking together. She’s apparently much more of an introvert than the situation earlier led you to believe. You’re thinking of a good way to start the conversation when she abruptly breaks the silence.<br>" +
						"<br>" +
						"<i>\"I didn’t think you even noticed me. Most nights I just see you talking to my sister.\"</i><br>" +
						"That stops you cold. Should you know who this girl is? She apparently knows you.<br>" +
						"<br>" +
						"The girl notices your hesitation and realizes she has you at a disadvantage. <i>\"You don’t actually recognize me, do you?\"</i> She gives a coy little smirk. <i>\"And I’ve seen you naked so often.\"</i><br>" +
						"<br>" +
						"OK, context clues time. She’s seen you naked, and at night. She must be watching the Games. You lean close to get a good look at her face, causing her to blush again. Green eyes, freckles. On closer examination, her ginger hair is tied in a ponytail. If you imagined it was in pigtails instead and she was a couple years older…<br>" +
						"<br>" +
						"She must be related to Lilly Quinn. <i>\"Lilly is my big sister. My name’s Ginette.\"</i> She offers a handshake, but realizes you’re still carrying the large box and pulls her hand back awkwardly. <i>\"I’m a freshman like you. I help out with the matches at night. Specifically, I’m in charge of those little care packages you find around the campus.\"</i><br>" +
						"<br>" +
						"She places those? Some of those crates look pretty heavy for such a petite girl. She shakes her head.<br>" +
						"<br>" +
						"<i>\"No, I have a couple football players who actually carry the heavy stuff. I’m mostly in charge of organizing them and buying stuff to put inside.\"</i> Ginette points to the box in your hands. <i>\"I kinda thought you were helping me because you knew the contents were eventually for you guys. Are you actually just a nice guy?\"</i><br>" +
						"<br>" +
						"Maybe a bit. You do have to admit that part of the reason you offered to help is because she looked cute, and you wanted an excuse to talk to her. She looks at the ground shyly, her face completely hidden under her hood.<br>" +
						"<br>" +
						"You should change the subject to make this less awkward. If Ginette’s a freshman, she probably hasn’t participated in the Games yet. You had assumed the support staff was made up of former participants. Has she thought about trying it?<br>" +
						"<br>" +
						"<i>\"Hell no! I don’t know how you Fuck Buddies can strip naked and have sex in front of everyone. I haven’t even… nevermind.\"</i> She blushes and looks away again.<br>" +
						"<br>" +
						"Fuck Buddies?<br>" +
						"<br>" +
						"<i>\"Oh. That’s what some of the support people call you participants. Just… usually not to your faces...\"</i> She looks a bit sheepish. <i>\"Besides, when I got invited, they weren’t even sure whether there would be any male participants this year. I’m not into other girls that way.\"</i><br>" +
						"<br>" +
						"She must be glad that you joined up then. Does she like watching your fights at night?<br>" +
						"<br>" +
						"You continue walking in silence while Ginette simmers in embarrassment. <i>\"You’re the one who gets naked in public every night. How are you so composed while making me blush?\"</i><br>" +
						"<br>" +
						"You can’t help laughing at her a bit. Her reactions are too cute to resist having some fun with her. Fortunately, she takes the teasing in good humor.<br>" +
						"<br>" +
						"<i>\"Lilly didn’t warn me that you were such a flirt. Do you hit on all the girls, or just the ones that buy you aphrodisiacs and sex toys?\"</i><br>" +
						"<br>" +
						"....<br>" +
						"<br>" +
						"The two of you continue making suggestive jokes and small talk. It’s almost a shame when you reach the Student Union and drop off the box.<br>" +
						"<br>" +
						"<i>\"Thanks for your help.\"</i> Ginette smiles at you shyly, her face still red. <i>\"You know… We should probably exchange numbers. I could use some feedback on the stuff I put in your care packages.\"</i> She touches the end of her ponytail nervously, a habit that she apparently shares with her sister.<br>" +
						"<br>" +
						"It would be useful to have some input on the items that show up during matches. Regardless, you don’t mind having an excuse to talk to Ginette again.<br>");
				Global.flag(Flag.metGinette);
			}else{
				Global.gui().message("In addition to textbooks, the campus bookstore sells assorted items for everyday use.");
				for(Item i: stock.keySet()){
					Global.gui().message(i.getName()+": $"+i.getPrice());
				}
				Global.gui().message("You have: $"+player.money+" available to spend.");
				displayGoods();
			}
			Global.gui().choose(this,"Leave");
		}
	}
	@Override
	public void shop(Character npc, int budget) {
		int remaining = Math.min(budget, 50);
		int bored = 0;
		while(remaining>25&&bored<5){
			for(Item i:stock.keySet()){
				if(remaining>i.getPrice()&&!npc.has(i,10)){
					npc.gain(i);
					npc.money-=i.getPrice();
					remaining-=i.getPrice();
				}
				else{
					bored++;
				}
			}
		}
	}
}
