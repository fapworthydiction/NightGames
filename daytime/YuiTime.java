package daytime;

import characters.*;
import characters.Character;
import global.Flag;
import global.Global;
import global.Modifier;
import global.Roster;
import items.Component;
import items.Consumable;
import items.Flask;

public class YuiTime extends Activity {
	private boolean acted;
	private Dummy sprite;
	
	public YuiTime(Character player) {
		super("Yui", player);
		sprite = new Dummy("Yui");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.YuiLoyalty);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		sprite.dress();
		sprite.setBlush(1);
		sprite.setMood(Emotion.confident);
		if(choice=="Start"){
			acted = false;
			if(Global.checkFlag(Flag.YuiUnlocking) && !Global.checkFlag(Flag.Yui)){
				Global.gui().message("You head to Yui's hideout, but she is nowhere to be found. Since she doesn't have a phone, you have no way to contact "
						+ "her. The girl is frustratingly elusive when you actually need to talk to her. No wonder Maya and Aesop were having so much trouble trying "
						+ "to invite her to the Games.");
			}
			if(Global.checkFlag(Flag.YuiAvailable)){
				Global.gui().loadPortrait(player, sprite);
				Global.gui().message("Yui greets you happily when you arrive at her hideout/shed. <i>\"Master! It's good to see you. I've collected some items "
						+ "you may find useful.\"</i> She grins and hands you the items proudly. She's acting so much like an excited puppy that you end up patting "
						+ "her on the head. She blushes, but seems to like it. <i>\"Would you like to spend some time training? Or is there something else I can do "
						+ "to help you?\"</i><br>");
				if(Global.checkFlag(Flag.Yui)){
					Global.gui().message("She gives you a shy, but slightly suggestive look. You're getting as much sex as you could possibly hope for each night, "
							+ "but there's no harm in giving your loyal ninja girl some personal attention.");
				}else{
					Global.gui().message("Hearing the offer from such an attractive blushing girl stirs up a natural temptation. She'd almost certainly agree to "
							+ "any sexual request you could think of. However, knowing Yui's sincerity and "
							+ "innocence, your conscience won't let you take advantage of her loyalty.");
				}
				Global.gui().choose(this,"Train with Yui");
				if(Global.checkFlag(Flag.Yui)){
					Global.gui().choose(this,"Sex Practice","Yui would probably enjoy some more hands-on practice");
					if(Roster.getAffection(ID.PLAYER,ID.YUI)>=16){
						Global.gui().choose(this,"Exposure Play","The campus is still populated this time of day, why not have a little fun?");
					}
				}
				Global.unflag(Flag.YuiAvailable);
				acted = true;
				player.gain(Component.Tripwire);
				player.gain(Component.Rope);
				if(player.getPure(Attribute.Ninjutsu)>=1){
					player.gain(Consumable.needle,3);
				}
				if(player.getPure(Attribute.Ninjutsu)>=3){
					player.gain(Consumable.smoke,2);
				}
				if(player.getPure(Attribute.Ninjutsu)>=18){
					player.gain(Flask.Sedative,1);
				}
			}else{
				Global.gui().message("You head to Yui's hideout, but find it empty. She must be out doing something. It would be a lot easier to track her down if she "
						+ "had a phone.");
			}
			Global.gui().choose(this,"Leave");
		}
		else if(choice=="Leave"){
			Global.gui().showNone();
			done(acted);
		}
		else if(choice.startsWith("Train")){
			sprite.setBlush(0);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("Yui's skills at subterfuge turn out to be as strong as she claimed. She's also quite a good teacher. Apparently she helped train her "
					+ "younger sister, so she's used to it. Nothing she teaches you is overtly sexual, but you can see some useful applications for the Games.");
			player.mod(Attribute.Ninjutsu, 1);

			Roster.gainAffection(ID.PLAYER, ID.YUI, 1);
			Global.gui().message("<b>You gained affection with Yui.</b>");
			Global.gui().choose(this,"Leave");
		}
		else if(choice.startsWith("Sex Practice")){
			sprite.setBlush(3);
			sprite.undress();
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("You reach out and stroke Yui's long bangs out of her face and she looks up at you with those wide, green eyes. "
					+ "She's so cute, you can't help but kiss her. She makes a small noise into your mouth, her hands on your chest, then begins "
					+ "to kiss you back. Her tongue moves so fluidly against yours that for a moment you're completely absorbed in the sensation.<p>"
					+ "She pulls away, breathing a little heavy. <i>\"Master...\"</i> Her breath is hot against your lips. <i>\"D-Do you think I "
					+ "could practice my technique on you?\"</i> You ask her what she means and she looks slightly embarrassed. <i>\"I mean...! It's "
					+ "one thing practicing on toys, but I'm finding real human parts to be a whole new experience! I'm so curious to learn more...\"</i> "
					+ "she trails off, looking down, her bangs hiding her eyes again.<p>"
					+ "You present yourself for her use and she smiles excitedly at you and begins to undress you. In a moment's time, you find "
					+ "yourself naked before her and she quickly does the same. You let your hand trail over her pale breasts and down over her "
					+ "milky, taut stomach. She reaches down and begins to stroke your cock, which is still half flaccid. She frowns a little "
					+ "seeing that she hasn't completely won you over, and begins to quicken her pace, her deft fingers sliding up and down and "
					+ "massaging you as you stiffen to her touch. You wrap your hand around her ass and pull her close, kissing her passionately.<p>"
					+ "As she kisses you back, she leads you to sit down, crawling on top of you and pressing her body against yours as your hands "
					+ "stroke her back and you play with her tongue. She breaks the kiss and moves between your legs, her hands still working up "
					+ "and down your dick.<p>"
					+ "She leans in and examines it closely, peering up at you, a blush creeping across her cheeks. <i>\"I'm very excited to try "
					+ "this, Master!\"</i> She leans in, gently kissing the base of your dick and working her way up the shaft. Her lips are soft "
					+ "and warm, and the sensation is slightly ticklish. When she reaches the tip, she parts her lips and gently licks the head. "
					+ "You moan at the sensation. Hearing your response, she enthusiastically takes the head in her mouth and begins to suck on it.<p>"
					+ "You can feel her tongue exploring the sensitive ridges with an urgent curiosity, but after a few moments, her training "
					+ "kicks in and she begins to slide you into her mouth. With one hand, she tries to tuck her bangs behind her ear, but they "
					+ "quickly fall back into her face as her head moves up and down, letting you push her tongue down and slide deeper into "
					+ "her mouth. She's still stroking the base of your cock.");
			Global.gui().displayImage("premium/Yui BJ Clean.jpg", "Art by AimlessArt");
			Global.gui().message("She pulls back to catch her breath and smiles up at you hungrily. <i>\"Mmm, you taste so much better than my toys, Master!\"</i> "
					+ "You settle back a little and encourage her to continue. She positions herself between your legs and suddenly you feel her "
					+ "mouth on your balls, her tongue probing them and feeling their shape as she sucks on them. She moves up; you can feel your "
					+ "balls resting on her cleavage as she begins to suck you again, her head moving up and down as she slides you deeper and "
					+ "deeper into her mouth.<p>"
					+ "Despite being unfamiliar with an actual penis, you can tell she is experienced at giving pleasure as the sensations of "
					+ "her tongue are quickly bringing you close to the edge. You reach out and grab her head, holding it gently but firmly as "
					+ "your hips move beneath her, forcing her to suck you deeper. She makes a gulping noise deep in her throat, whimpering in "
					+ "pleasure. With one hand, she massages your balls and cups them close to your body. You can see her hips grinding in "
					+ "arousal as you use her mouth. You groan with desire.<p>"
					+ "When she feels your cock tensing up and twitching as it approaches orgasm, her fingers grip the base and stroking "
					+ "fast as she pulls back to avoid choking. You're already cumming before she even gets her mouth off you, and creamy "
					+ "hot semen spills across her tongue and over her lips. She makes a small noise of delight as you continue to spurt your "
					+ "load onto her pale, pretty face.<p>"
					+ "Yui licks her lips and wipes your cum off her cheek, sucking on her finger and beaming at you. <i>\"Wow, you came so "
					+ "quickly, Master! I must have done a good job!\"</i> You pet her head as she dutifully cleans up your cock with her mouth, "
					+ "looking a little shy but very content as well.<p>"
					+ "You tell her that she did a good job, but that practice never hurts. She grabs your hand and looks at you with a "
					+ "serious expression.<p>"
					+ "<i>\"Then promise you'll come back, Master! To help me again!\"</i>");
			Roster.gainAffection(ID.PLAYER, ID.YUI, 2);
			Global.gui().message("<b>You gained affection with Yui.</b>");
			Global.gui().choose(this,"Leave");
		}
		else if(choice.startsWith("Exposure Play")){
			sprite.setBlush(3);
			sprite.undress();
			sprite.setMood(Emotion.nervous);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("You decide to have some fun with Yui. She seems eager to follow your orders, so perhaps you should order her to do "
					+ "something exciting.<p>"
					+ "You instruct her to undress so you can help her train for the Games. Her cheeks redden, but she grins eagerly as she strips naked. "
					+ "She's still embarrassed to show you her slim body and perky breasts, but she's obviously hoping for some intimate attention. "
					+ "However, she's going to need to be patient.<p>"
					+ "While she stands there, naked and blushing, you give her a brief lecture about the importance of stealth during the Games. Her "
					+ "ninja training has surely given her a strong understanding of concealment, but it focused heavily on camouflage and clothing. "
					+ "In the Games, she's most vulnerable when she's naked. Her fair skin will stand out more at night, and an attractive naked girl "
					+ "tends to draw the eye.<p>"
					+ "Yui squirms with embarrassment as you draw more attention to her bare body, but you're just getting started. In order to help her "
					+ "traverse the campus while naked, you're going to have her practice it during the day.<p>"
					+ "<i>\"Master!?\"</i> Her eyes widen with panic at the idea of streaking across a populated campus. Being seen naked by her fellow "
					+ "competitors doesn't really compare to being seen naked by random students. However, you have faith that her ninjutsu training "
					+ "should let her remain unseen as long as she's careful. You'll wait for her behind the football field bleachers, and give her a "
					+ "nice reward if she can make it to you.<p>"
					+ "Yui's face grows even redder as she considers the task. The football field is a considerable distance away, and she'll have to "
					+ "pass several academic buildings on the way. While the field itself should be empty right now, the areas in between will probably "
					+ "be well populated. However, the promised reward definitely has her intrigued.<p>"
					+ "After a long hesitation, she quietly nods. You pack her clothes into your backpack, give her a quick kiss for luck, and head for "
					+ "the football field.<p>"
					+ "...<p>"
					+ "It takes you about ten minutes to reach the football field, but of course you don't have to worry about being noticed. It'll "
					+ "probably take Yui much longer. There's a decent amount of foot traffic along the way as expected.<p>"
					+ "However, as you approach the field, you realize there's a problem. You expected the field to be unoccupied, but a group of "
					+ "students are there, playing ultimate frisbee. Even if Yui can get across campus without getting spotted, you don't see a way "
					+ "for her to approach the bleachers without being clearly visible to the frisbee players.<p>"
					+ "Yui is probably loyal enough to try to complete her task despite the risk. You need to intercept her along the way and pick "
					+ "a different meeting place. Unfortunately, you don't know what route she's taking. She's also being as stealthy as she can, so "
					+ "even if you guess right, you probably won't be able to spot her. Is there a safe place you can wait for her, where she's "
					+ "guaranteed to notice you?<p>"
					+ "<i>\"Master, over here!\"</i> While you're considering your options, you hear Yui's hushed voice from behind the nearby "
					+ "bleachers. You approach and find her still naked, trembling with nerves. Apparently, your concerns were unfounded. She even "
					+ "got here much faster than you imagined.<p>"
					+ "Yui practically leaps into your arms as you approach. The trip here was clearly a little overwhelming, but as you slip a hand "
					+ "between her legs, you find her completely drenched. She lets out a moan at your touch and you have to warn her to stay quiet. "
					+ "The ultimate frisbee players probably won't see the two of you here, but they're probably close enough to hear her if she's too "
					+ "loud. You had hoped for a more private rendezvous, but Yui went above and beyond the challenge you gave her. She clearly deserves "
					+ "her reward.<p>"
					+ "You press her against the nearby wall and kiss her passionately. She practically melts in your arms, and softly whimpers "
					+ "approvingly. You kneel on the ground, leaving a trail of kisses down the front of her nude body. She covers her mouth to stifle "
					+ "her moans as you reach her wet flower and lavish it with attention.<p>"
					+ "You lap up her juices, exploring her lower lips with your tongue. She's extremely sensitive and responsive. It's a lot of fun "
					+ "making her squirm with pleasure. You decide to turn up the intensity a bit by focusing on her little love bud. A high pitched "
					+ "moan escapes her hands, and you wait for a few tense seconds to see if anyone heard it.<p>"
					+ "When you're confident your activities have gone unnoticed, Yui gives you a pleading look. <i>\"Please, Master. I love your tongue, "
					+ "but I need you inside!\"</i><p>"
					+ "This situation is hot enough that you're happy to comply. You free your stiff dick from your pants while Yui turns to face the "
					+ "bleachers, sticking her ass out towards you. You take a moment to line up your cock with her slick pussy, then thrust deep into "
					+ "her hot core. This time you have to cover her mouth to stifle her moan. She appeared to orgasm immediately, and her own hands are "
					+ "occupied trying to hold herself up.");
			Global.gui().displayImage("premium/Yui Public.jpg", "Art by AimlessArt");
			Global.gui().message("Without waiting for her to recover, you fuck her passionately from behind. You both need to be careful not to make too much noise, "
					+ "but the risk of discovery just makes it more exciting. You keep your left hand over Yui's mouth, while your right hand slips down "
					+ "to tease her clit. Her body trembles uncontrollably, not calming at all since her first orgasm.<p>"
					+ "It's not long before you feel your own climax approach. Right as you shoot your load inside Yui, you lean in and suck on her neck "
					+ "hard enough to leave a hickey. Judging by her twitching body and barely stifled scream, she probably came again.<p>"
					+ "You weren't as quiet as you should have been, but fortunately no one seems to have noticed you. You return Yui's clothes, and she "
					+ "gratefully gets dressed. The load of cum you left inside her will probably stain her panties, but if she minds, she sure doesn't "
					+ "show it. She gives you a smile full of adoration and embraces you tightly.<p>"
					+ "<i>\"So, did I do a good job, Master?\"</i> In lieu of a response, you give her a tender kiss.");
			Roster.gainAffection(ID.PLAYER, ID.YUI, 2);
			Global.gui().message("<b>You gained affection with Yui.</b>");
			Global.gui().choose(this,"Leave");
		}
	}

	@Override
	public void shop(Character npc, int budget) {
		npc.gain(Consumable.needle,3);
		npc.gain(Consumable.smoke,2);
		npc.gain(Flask.Sedative,1);

	}

}
