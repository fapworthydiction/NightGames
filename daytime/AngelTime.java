package daytime;


import characters.*;
import characters.Character;
import global.Flag;
import global.Global;
import global.Modifier;
import global.Roster;
import items.Clothing;

public class AngelTime extends Activity {
	private Character angel;
	private Dummy sprite;
	private boolean gauntlet;
	
	public AngelTime(Character player) {
		super("Angel", player);
		angel = Roster.get(ID.ANGEL);
		sprite = new Dummy("Angel",1,true);
	}
	
	@Override
	public boolean known() {
		return Global.checkFlag(Flag.AngelKnown);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		sprite.dress();
		sprite.setCostumeLevel(1);
		sprite.setBlush(0);
		sprite.setMood(Emotion.confident);
		Global.gui().loadPortrait(player, sprite);
		if(choice == "Start"){
			if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>25&&angel.has(Trait.succubus)){
				sprite.setMood(Emotion.nervous);
				Global.gui().message("You meet Angel at her room, but for once, she doesn't seem eager to get to sex. You can tell she has something on her mind, so you let her lean " +
						"against you on the futon while she thinks. It's quiet. You aren't used to your time with Angel being quiet, mostly because you so rarely meet her alone. You " +
						"lose track of time sitting there before she breaks the silence.<p>"
						+ "<i>\""+player.name()+", what do you think about my friends?\"</i> That's not the question you expected. " +
						"You've gotten along with her friends quite well so far.<p>"
						+ "Angel shifts her position so you can't see her face. <i>\"I've had several lovers who couldn't get along with " +
						"my friends. Some of them pretended they did for awhile, some of them wanted me to spend less time with them.\"</i> Her friends are very socially and sexually aggressive. " +
						"You can see how that might make some guys uncomfortable. The fact that Angel and Mei occasionally have sex could probably also be a point of contention. It's probably " +
						"fortunate that the Games got you used to casual and group sex before you met them.<p>"
						+ "<i>\"A lot of people are superficially interested in me, but " +
						"most lose interest when they find out what I'm really like. Sarah, Mei and Caroline know me better than anyone, but they don't think any less of me. Mei and Sarah " +
						"need me as much as I need them. Caroline is good at making friends, but she chooses to stick with us anyway. If I have to choose between them or a boyfriend, " +
						"I choose them without a second thought.\"</i><p>"
						+ "Angel looks you in the eye. You've never seen her this worried and vulnerable. <i>\"Do you really like them?\"</i> You can " +
						"reply with confidence that you've grown quite fond of Sarah, Mei and Caroline, personality quirks and all.<p>"
						+ "<i>\"Good, because I don't kno-... No one's going to make " +
						"me choose between you and them, got it? That's just not going to happen.\"</i> Angel stands up, back to her normal self. <i>\"So should we meet up with them or spend a " +
						"little more time with just the two of us?\"</i>");
					promptScenes();
			}
			else if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>0){
				Global.gui().message("You text Angel, suggesting to meet up. She responds with a location where to meet her. When you arrive however, you find her friends waiting "+
						"for you instead. One of the girls, Caroline, waves you over to where they're sitting. <i>\"Angel stepped away for a minute. Sit down and talk with us until " +
						"she gets back.\"</i> You spend some time chatting with the girls about their hobbies (Caroline plays a lot of video games and Sarah is fond of romance novels) " +
						"and about how you met Angel (you make up something plausible).<p>"
						+ "Mei is sitting right next to you and makes a habit of resting her hand on your leg while " +
						"you're talking. Her whole body language suggests she'd be interested in getting to know you better. After a few minutes however, she has to excuse herself " +
						"to make a quick phone call. On her way out, she slips a small piece of paper into your hand with a flirtatious smile. It turns out to contain her phone number.<p>"
						+ "<i>\"Be careful with that girl. Attempting the Guantlet is one thing, but don't call her behind Angel's back.\"</i> Caroline warns as soon as Mei is out of earshot. "
						+ "<i>\"She has a bad habit of stealing Angel's men.\"</i> That's quite a specific bad habit. " +
						"It must put a considerable strain on their friendship.<p>"
						+ "<i>\"No, Angel never really holds a grudge against her. She has an effective way of punishing Mei and then considers " +
						"them even.\"</i> Caroline doesn't elaborate on this 'punishment,' but you notice Sarah has turned bright red. <i>\"She'd probably get mad if Mei stole you though. " +
						"Angel doesn't usually run off to fix her make-up just because she's meeting a guy. I think she really likes you.\"</i><p>"
						+ "With perfect timing, Angel arrives. Caroline " +
						"stops talking, but gives you an encouraging wink. Angel takes the seat next to you that Mei just vacated and kisses you on the cheek in greeting. <i>\"Hello lover. " +
						"What sexy and scandalous plans do you have for us today?\"</i>");
				promptScenes();
			}
			else if(Roster.getAffection(ID.PLAYER,ID.ANGEL)<15){
				Global.gui().message("While walking through the quad, you spot Angel talking with three other girls. They're too far away to hear what they are talking about, but "+
					"you can tell they're close friends. If Angel was alone, you wouldn't hesitate to talk to her, but their group is radiating an almost impenetrable atmosphere. " +
					"What strikes you more than anything else is how... normal they seem. At night, Angel is always an insatiable sex queen, but here she's just like any other college " +
					"girl. You're still musing on this as the girls pass by. It doesn't seem like Angel noticed you. You decide you'll probably have better luck approaching " +
					"her when she's alone.");
				Roster.gainAttraction(ID.PLAYER,ID.ANGEL,2);
			}
			else{
				Global.gui().message("Using the information you got from Aesop, you go looking for Angel. Your hope is to invite her to spend time training together and possibly sex. " +
					"You find her pretty quickly, but she's at a table with several of her friends. You'd need to talk to her in private if you're going to bring up the Games, " +
					"but you don't have an excuse ready that'll work on her friends. You also don't know how she's going to react if you talk to her out of nowhere.<p>"
					+ "Right when you " +
					"decide to give up and try to catch her after a match, Angel looks up and makes eye contact with you. She tells her friends something that you're too far away to " +
					"catch and walks over to you.<p>"
					+ "<i>\"Did you want to talk to me or were you just planning to stare?\"</i> Her words are harsh, but her tone is light enough that you can " +
					"tell she's just teasing you. You mention Aesop's suggestions on how to train between matches.<p>"
					+ "She thinks for a moment and then holds out her hand. <i>\"Phone,\"</i> she " +
					"demands.<p>"
					+ "You hand over your phone, not understanding her intentions. She enters her number into it and returns it to you. <i>\"Give me a call whenever you want to train, " +
					"or if you're just horny,\"</i> she says with a relaxed smile.<p>"
					+ "You glance behind Angel to her friends, just outside of earshot. Two of them are trying (unsuccessfully) " +
					"to look like they aren't watching you. The third is simply staring openly. Do they think you're Angel's boyfriend? <i>\"Oh, I told them we were fuck-buddies,\"</i> Angel " +
					"explains. <i>\"Come on, I'll introduce you.\"</i> She leads you back to the table and introduces each of her friends.<p>"
					+ "The first girl, Caroline, seems very friendly and laid back. " +
					"Even though you've just met, she talks like you're old friends.<p>"
					+ "The one who was blatantly watching you earlier is a slim, asian girl named Mei. Though she doesn't look " +
					"anything like Angel, something in her eyes tells you they're birds of a feather.<p>"
					+ "Lastly, there's a quiet girl named Sarah who seems nice, but can't seem to say two sentences " +
					"to you without blushing and lowering her eyes.<p>"
					+ "The five of you chat idly for a few minutes before Angel returns to your previous topic in the least subtle way possible. " +
					"<i>\"So, sex? or...?\"</i> She doesn't seem to mind discussing this in front of her friends. None of them seem surprised at her frankness, though Sarah's blush deepens a bit. As " +
					"long as you don't mention your nightly activities, it's probably fine.");
				Roster.gainAttraction(ID.PLAYER,ID.ANGEL,1);
				Global.gui().message("<b>You gained affection with Angel.</b>");
				promptScenes();
			}
			Global.gui().choose(this,"Leave");
		}
		else if(choice.startsWith("Sex")){
				sprite.undress();
				sprite.setBlush(3);
				sprite.setMood(Emotion.horny);
				Global.gui().loadPortrait(player, sprite);
				Global.gui().message("You suggest to Angel that maybe the two of you should head somewhere a bit more private. You barely finish the sentence before she grabs you by the arm and " +
						"practically drags you away. You give the other girls a quick parting wave and you hear Caroline call after you. <i>\"Have good sex.\"</i><p>Angel eventually brings you to her " +
						"dorm room and stops in front of her room, gestures for you to wait, and disappears inside. Isn't she going to invite you in? <i>\"Not today,\"</i> she replies. <i>\"I have a better " +
						"idea.\"</i> You feel a bit awkward loitering in the hall, so you try to make some small talk.<p>Her friends seem nice. <i>\"My friends are very important to me, so I want you to get along " +
						"with them. Just be careful with Mei. If she tries to seduce you, it's just to get attention. She'll dump you in a day or two.\"</i> You thank her for the warning, though Angel " +
						"also has reputation for dumping guys when she gets bored with them. She reappears in the doorway and pushes you against the wall, kissing you forcefully. <i>\"Not you.\"</i> she says " +
						"after breaking the kiss. <i>\"Not today at least.\"</i> She keeps you pinned against the wall, looking as if she's trying to decide whether to fuck you right here. There are a few people passing " +
						"through the hall and you're already getting some weird looks.<p>After a few moments, she pulls you into the shower room, and for the first time you notice she's holding a towel and a " +
						"couple bottles of lotion. She tosses the lotion into an available shower stall and disrobes in record time. She then sets to undressing you with such haste that you're worried " +
						"she's going to literally tear off your clothes. She can take her time, it's not like you're going anywhere. She pulls your hand to her womanhood, which you find hot and very wet. " +
						"<i>\"I'm not really in a patient mood.\"</i> You help her remove your clothes and retreat into the stall before someone else comes in. You turn on the water at a comfortable temperature " +
						"and then you return your attention to Angel. She's got the lotion in hand and starts rubbing it on your body. She pours enough into your hands for you to rub on her.<p><i>\"It's oil " +
						"based,\"</i> she explains. <i>\"So it won't get washed off by the water. It's also safe to use in sensitive holes.\"</i> You're impressed by how thoroughly Angel planned this, despite her " +
						"impatience to get started.");
				Global.gui().displayImage("premium/Angel Shower Fingering.jpg", "Art by AimlessArt");
				Global.gui().message("You decide to test the lotion out by sliding two well-lubricated fingers " +
						"into her pussy. She groans softly and starts oiling up your cock in return. Your fingers explore inside her looking to map out her most sensitive areas. Eventually you find " +
						"a spot that makes her weak in the knees, so obviously you focus on it. When she's trembling on the verge of orgasm, you deliver the coup de grace by pulling back her clitoral hood " +
						"and rubbing her love buzzer with a lubed-up fingertip. She orgasms intensely and you have to catch her to keep you from falling to the floor.<p>Angel's first words after she " +
						"catches her breath are: <i>\"I want you in me right now.\"</i> You couldn't agree more. You push her against the wall and hold her leg up to get clear access to her entrance, while you " +
						"thrust your lotion-slick shaft into her. She moans passionately, still sensitive from her recent orgasm. You thrust into her again and again while her nails dig into your back. ");
				Global.gui().displayImage("premium/Angel Shower Sex.jpg", "Art by AimlessArt");
				Global.gui().message("<i>\"Fuck! I'm going to cum again!\"</i> Angel is practically screaming now. If someone else is anywhere near the showers, they must be able to hear her. You kiss her passionately to quiet her " +
						"as you feel your own orgasm approaching. she grabs your ass with both hands and pulls you as deep into her as possible. Your cock spasms as it shoots its load into her womb and " +
						"fuels her climax.<p>You pull out of her and let the hot water from the shower wash away your sweat and fluids. The oily lotion proves much more water resistant and you're both " +
						"still covered in the slippery substance. Presumably the other bottle Angel brought will clean it off. <i>\"Nope that's just shampoo. The lotion will come off eventually,\"</i> she says " +
						"while grasping your lubricated dick again. <i>\"We just need to rub it all off.\"</i> In the end it takes more than thirty minutes - and a few more orgasms - before you're both clean, but you " +
						"don't regret it.");
			Global.gui().choose(this,"Leave");
			Daytime.train(player,angel,Attribute.Seduction);
			Roster.gainAffection(ID.PLAYER,ID.ANGEL,1);
			Global.gui().message("<b>You gained affection with Angel.</b>");
		}else if(choice.startsWith("Kissing")){
			sprite.undress();
			sprite.setBlush(2);
			sprite.setMood(Emotion.horny);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("You're in Angel's room, naked and feeling a little overwhelmed. She embraces your from behind and you can feel her soft, heavy breasts pressed against " +
					"your back. She nibbles lightly on your ear while motioning toward the naked girl on her bed.<p>"
					+ "<i>\"She's all yours. Show me what you can do.\"</i><p>"
					+ "As for how you got here... " +
					"we should probably back up a bit.<p>"
					+ "...<p>"
					+ "You spent some time chatting with Angel and her friends. At this point it might be fair to call them your friends too. The conversation " +
					"inevitably turned to sex, though you can't quite remember how.<p>"
					+ "<i>\"Boys are simple. Five minutes with one hand and they're happy as a clam.\"</i> Caroline said this while casually miming a " +
					"handjob. <i>\"They expect girls to be simple too, but no amount of fingerwork is a substitute for a kiss and a romantic atmosphere.\"</i><p>"
					+ "Ok, so mood is important, but pleasing a " +
					"girl isn't really that different from pleasing a guy. It's still the fingers, or tongue, or whatever direct stimulation that finishes the job. There's nothing particularly mysterious " +
					"about that.<p>"
					+ "Angel stroked your thigh and gave you a confident smile. <i>\"You only need the fingers because boys are clumsy kissers. If you were a little better at seduction, " +
					"the kiss would be enough.\"</i><p>"
					+ "If Angel could actually make girls cum by kissing them, you probably would have seen her do it by now. You couldn't really bring up the number of times " +
					"you've seen her finger her opponents in front of the whole group, so you settled for general skepticism.<p>"
					+ "At this point Angel frowned and ruminated for a while.<p>"
					+ "<i>\"We're going to " +
					"need a volunteer,\"</i> she eventually declared.<p>"
					+ "...<p>"
					+ "So that's how the three of you ended up here. Mei modestly keeps her hands in front of her groin, but doesn't show any indication " +
					"that she's embarrassed about being nude in front of you. Angel circles in front of you, swaying her hips seductively.<p>"
					+ "<i>\"If you're nervous, I'll show you what a real kiss is like.\"</i><p>" +
					"She gently pulls your head towards her and presses her lips softly against yours. It seems like a surprisingly innocent kiss until her tongue darts out to brush your lips. As she " +
					"withdraws her tongue, she presses the kiss deeper. She nibbles your lower lip gently then attacks your tongue with her own. This cycle of invading your mouth with her tongue and deepening " +
					"the kiss drives all conscious thought from your head. When she pulls away and you regain your senses, you're so aroused that precum is dripping from your erection.<p>"
					+ "Angel pushes you " +
					"gently towards the bed. <i>\"Your turn. Try to make Mei cum with a kiss like that.\"</i><p>"
					+ "Mei glances down at your crotch and smirks. <i>\"Try not to cum on your own first.\"</i><p>"
					+ "You feel weird about kissing Mei in front of Angel, but it was her idea. "
					+ "You push her gently down onto the bed and kiss her softly. You match Angel's style of gradually increasing pressure and " +
					"cycles of aggressive tongue-play. Within seconds, Mei whimpers softly into your mouth as she realizes she underestimated you. <p>"
					+ "Not much later, you feel her body tensing up. Then " +
					"she pulls away from you abruptly, flushed deep red and panting for breath.<p>"
					+ "<i>\"Ok, that's enough,\"</i> she gasps out. <i>\"You win.\"</i><p>"
					+ "So she came already? Mei nods, avoiding eye contact.<p>" +
					"<i>\"Not bad,\"</i> Angel says. <i>\"Of course Mei orgasms pretty easily. You'll still need to practice more before that'll work on someone like me.\"</i> You cut her off by kissing her with " +
					"the same passion you used to get Mei off. Her soft noises of protest melt away as you press the kiss deeper. She makes no attempts to pull away or retaliate and soon you feel a " +
					"shudder run through her and she has to cling to your shoulders to stay on her feet.<p>"
					+ "She smiles as you pull away and whispers, <i>\"You're a quick learner, lover.\"</i> She grasps your " +
					"pent-up cock and strokes it until you cum in her hands.<p>"
					+ "She offers her semen covered hand to Mei, who hesitantly begins to lick it clean. Angel looks back to address you. <i>\"I hope that's " +
					"enough for a little while, I need to punish Mei now.\"</i><p>"
					+ "Mei jumps back in shock. <i>\"What did I do!?\"</i><p>"
					+ "Angel gives her a sadistically sweet smile. <i>\"Are you going to pretend you " +
					"weren't kissing my boy on my bed a minute ago? He made you orgasm and you even drank his cum.\"</i> Angel walks over to the refrigerator in the corner, gets a couple ice cubes from the " +
					"freezer, and puts them in a glass.<p>"
					+ "Mei stammers out protest while she walks back to the bed, but Angel silences her with a kiss. She takes one of the ice cubes and teases each of " +
					"Mei's nipples with it. She runs the cube down Mei's front and touches it to her clit. Mei yelps and tries to jerk away from the sensation, but Angel manages to slip the ice cube into " +
					"her pussy. While Mei shivers and tries to endure the cold object in her most sensitive area, Angel moves between her legs and starts to eat her out. She teases her for awhile before " +
					"she retrieves the ice cube with her tongue and presses it against Mei's clit.<p>");
			Global.gui().displayImage("premium/Angel Mei Kiss.jpg", "Art by AimlessArt");
			Global.gui().message("After a few minutes of alternating between pleasure and torture, Mei screams in passion and goes limp.<p>"
					+ "As " +
					"she's recovering, Angel slips the other ice cube into Mei's mouth and straddles her face. <i>\"If this is punishment then you shouldn't have all the fun. Give me some attention this time.\"</i><p>" +
					"...<p>"
					+ "After some of the most spectacular girl-on-girl action you've ever scene, Mei starts collecting her clothes and prepares to leave. You and Angel suggest she stick around and just relax " +
					"for a while, but she shakes her head. <i>\"Three's a crowd. You two deserve some alone time. Besides, if I stay longer, who knows what Angel will do to me.\"</i><p>"
					+ "After she leaves, you ask Angel " +
					"to explain what exactly the relationship between her and Mei is. You've heard Mei often tries to steal her boyfriends, but they're clearly quite close.<p>"
					+ "Angel settles comfortably into the " +
					"bed, exhausted from multiple orgasms. <i>\"Mei is straight, so even when she's seeking my affection, I have to act like it's punishment. If I neglect her for too long, she steals a boy from " +
					"me so I have an excuse to punish her. She doesn't mean any harm, she just gets lonely.\"</i><p>"
					+ "That seems unnecessarily complicated. Still, it's very considerate of Angel to indulge Mei even when " +
					"they're fighting over guys.<p>"
					+ "<i>\"Of course,\"</i> she says. <i>\"Mei is my friend.\"</i>");
			if(!player.has(Trait.greatkiss)){
				Global.gui().message("<p><b>You've improved your kissing technique to the point where it may render opponents temporarily helpless.</b>");
				player.add(Trait.greatkiss);
				angel.add(Trait.greatkiss);
			}
			Global.gui().choose(this,"Leave");
			Daytime.train(player,angel,Attribute.Seduction);
			Roster.gainAffection(ID.PLAYER,ID.ANGEL,1);
			Global.gui().message("<b>You gained affection with Angel.</b>");
		}
		else if(choice.startsWith("Games")){
				sprite.undress();
				Global.gui().loadPortrait(player, sprite);
				Global.gui().message("You know this can't end well for you. When you suggested playing a game, all of the girls shared a conspiratorial smile and Angel declared she had a game in mind. " +
						"Now you're sitting in a surprisingly roomy girls' dorm room, waiting to see what craziness Angel has in store for you.<p>"
						+ "<i>\"Found it,\"</i> you hear Sarah call from the next room. Mei " +
						"speaks up while she's clearing a spot on the floor. <i>\"It's been awhile since we got to play this. We need to find more interesting boys.\"</i><p>"
						+ "Sarah and Angel return holding a " +
						"cardboard spinner and laying out a mat with multicolored circles on the floor. Huh, you weren't expecting something so normal. You haven't played this since you were a kid.<p>" +
						"<i>\"We added a couple things to the spinner,\"</i> Sarah explains. <i>\"Also, you have to remove an article of clothing every time you fall.\"</i> Of course, it couldn't be a completely " +
						"normal game.<p>"
						+ "Sarah keeps the spinner and the other four of you remove your shoes and wait on the mat.<p>"
						+ "<i>\"Left foot red.\"</i> Simple enough.<p>"
						+ "<i>\"Left hand yellow.\"</i> The mat seemed " +
						"bigger when you were a kid. It's pretty cramped with four of you.<p>"
						+ "<i>\"Right foot blue.\"</i> Ok, now you're pressed between Angel and Mei, which is not a bad place to be. Angel's " +
						"breasts specifically are touching the side of your face. You're pitching a tent, and you're not the only one who noticed. A hand grabs at your erection through your pants and it " +
						"surprises you enough that you fall down, taking Angel and Mei with you.<p>"
						+ "<i>\"Don't be in such a hurry,\"</i> Angel teases, while taking off her top. <i>\"Everyone will be naked soon enough.\"</i>" +
						"<p>About ten minutes and some lost clothing later and Sarah starts calling some weird commands.<p>"
						+ "<i>\"Right hand free.\"</i> Free? What exactly does that mean?<p>"
						+ "<i>\"It means you can move " +
						"your hand and do whatever you want with it,\"</i> Angel explains. <i>\"Like so.\"</i> She reaches over and tickles Mei, causing her to fall and lose her last piece of clothing.<p>"
						+ "<i>\"Left hand " +
						"Angel.\"</i> Angel? Not wanting to waste an opportunity, you grab her ample breast. Caroline has a slightly different definition of 'touch,' and spanks Angel hard on the ass. She yelps " +
						"and falls down onto you. There go her pants, and yours.<p>"
						+ "A few more minutes and you end up reaching over Caroline to get a green circle, which is slightly awkward since she's " +
						"topless and you're in your boxers.<p>"
						+ "<i>\"Right hand boob\"</i> You palm her breast without even thinking, and she lets out a soft noise.<p>"
						+ "You apologize, but she just smiles back at you. " +
						"<i>\"Don't worry about it. You can probably repay me if you hold that position for a little while longer.\"</i><p>"
						+ "<i>\"Right hand male!\"</i> Wait... what? Caroline grins victoriously and her hand darts " +
						"into your boxers to grab your dick.<p>"
						+ "Sarah cheers excitedly and leans in for a closer look. <i>\"Caroline got the penis! That means everyone else loses an article of clothing and that " +
						"means Angel and "+player.name()+" are out. Caroline wins!\"</i> <p>"
						+ "There was definitely no mention of a bonus for grabbing your cock during the rules explanation. Caroline's response to " +
						"your protests is to rub the sensitive head of your dick with her thumb, making you squirm a bit despite your indignation.<p>"
						+ "Angel speaks up from behind you while removing her panties. <i>\"If we warned you, you would have tried to protect your man " +
						"parts. That would have been no fun.\"</i><p>"
						+ "Caroline smiles while still holding your shaft. <i>\"I think of this as a team victory. If you want, I don't mind rewarding you for your cooperation.\"</i><p>" +
						"You decline her offer to tug you off for everyone's entertainment, but since she's the winner, you let her remove your boxers. You spend about 20 minutes just hanging out and chatting. " +
						"No one is allowed to get dressed and you get the lion's share of the interest, but you feel strangely comfortable with most of the girls in similar states of undress. ");					
			Global.gui().choose(this,"Leave");
			Daytime.train(player,angel,Attribute.Cunning);
			Roster.gainAffection(ID.PLAYER,ID.ANGEL,1);
			Global.gui().message("<b>You gained affection with Angel.</b>");
		} else if(choice.contains("Bluffing")){
			sprite.undress();
			sprite.setMood(Emotion.dominant);
			sprite.setBlush(2);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("Today all the girls left the responsibility of choosing a game to you, with Angel's stipulation that it had to including stripping. You picked a simple, but intense " +
					"bluffing game with stripping rules slotted in so naturally it's like the game was designed for them. Not to be immodest, but clearly your genius knows no bounds.<p> "
					+ "The first five " +
					"minutes of the game clearly separate the people who can bluff from those who can't. Mei bet far too aggressively and is already completely naked. You lost one coaster and your belt. " +
					"Angel and Caroline have both been playing cautiously and are still fully clothed.<p>"
					+ "Poor Sarah is far too easy to read " +
					"when she's nervous and has already been called on three bad bluffs. She's down to one coaster and her panties. This is a rather unusual situation since she is always the moderator in " +
					"whatever game the group is playing and doesn't tend to lose her clothes. Unlike the other girls, who treat nudity very casually, she's extremely embarrassed about how much of her body " +
					"you can see.<p>"
					+ "Next round, she's forced to make a wager early since she only has one coaster to put down. She bets three and no one raises her. She flips her own coaster first - a rose, " +
					"fortunately for her, though she no longer has any way to bluff - and flips Caroline's coaster, a skull.<p>"
					+ "Sarah looks crestfallen, but she does stand up and remove her panties. You catch " +
					"a glimpse of her very wet pussy before she covers herself with her hands and sits back down. Mei pats her on the head to comfort her.<p>"
					+ "Caroline starts the next round, and - though she loses " +
					"her top in the next wager - she makes the first successful bet of the game. You and Angel have to play more aggressively now. With the strip rules, she doesn't immediately win the game if she wins another " +
					"bet, but she does get to select and eliminate another player.<p>"
					+ "Over the next few rounds, you make some riskier bluffs and Angel calls you on them every God damn time! You've won one bet, but " +
					"you're down to your skull and your boxers. Fortunately Angel is in the same boat, because she apparently can't read Caroline as well as you. Caroline is in the best shape, but she's down to " +
					"her bra and panties.<p>"
					+ "The next round is almost certainly going to be your or Angel's last and you're determined not to go down without a fight. All the coasters are played this round and " +
					"you have to make a wager, but if anyone calls you, you'll be eliminated. You quietly wager one and think 'rose' in your head.<p>"
					+ "Angel studies your face carefully and slowly smiles. <i>\"I've seen " +
					"every expression you make. You can't hide anything from me. Two.\"</i> No one is going to risk three. You keep your face passive as she flips her rose and reaches for your face-down coaster. " +
					"Her stunned expression when she turns over a skull is priceless. She slips off her panties and gives you a glare that promises angry, aggressive sex the next chance she gets.<p> "
					+ "Caroline speaks " +
					"up. <i>\"Well played, but there's literally no way you can win now that I know you're holding a skull. Do you want to just hand over the boxers now?\"</i><p>"
					+ "You concede defeat and forfeit your boxers. " +
					"Caroline looks you over slowly while deep in thought. <i>\"As the victor, I can have my way with the losers, but... I think I'll leave Angel's boy to her.\"</i> Angel grins and pats the couch next to " +
					"her. As you somewhat hesitantly sit down, she begins to slowly stroke and play with your erection.<p> "
					+ "She leans in close to whisper in your ear. <i>\"Everyone wants to see you cum, so I can tease " +
					"them by teasing you.\"</i> You groan quietly. You can't deny the pleasure she's giving you, but she's clearly not going to let you finish anytime soon.<p>"
					+ "Caroline watches the two of you with a flushed smile. Mei and Sarah are both watching rapt, each with one hand under the table. <p>"
					+ "<i>\""+player.name()+" isn't the only loser in the room,\"</i> Caroline says while glancing at the naked girls. <i>\"How about " +
					"the two of you give him some eye candy by showing him what you're doing under there.\"</i><p>"
					+ "Sarah lets out a surprised whimper at the command, but she and Mei both obey Caroline as she has them sit " +
					"facing you while they masturbate openly. Sarah looks so embarrassed she may cry, but you can tell she's getting off on this more than anyone else. Mei looks more composed, but from time to time you see her " +
					"bite her lip and shudder. You realize she's having multiple little orgasms and trying to hide it.<p>"
					+ "After Sarah climaxes (loudly), Angel speeds up her handjob and whispers in your ear, <i>\"Cum for me. " +
					"You have my permission.\"</i> You couldn't hold back even if you wanted to. You shoot your load so far that some of it even hits Caroline, who is quite a ways away.");
			Global.gui().displayImage("premium/Angel Handjob.jpg", "Art by AimlessArt");
			if(!player.has(Trait.pokerface)){
				Global.gui().message("<p><b>You've mastered the art of bluffing.</b>");
				player.add(Trait.pokerface);
				angel.add(Trait.pokerface);
			}
			Global.gui().choose(this,"Leave");
			Daytime.train(player,angel,Attribute.Cunning);
			Roster.gainAffection(ID.PLAYER,ID.ANGEL,1);
			Global.gui().message("<b>You gained affection with Angel.</b>");
		}

		else if(choice.startsWith("Sparring")){
			sprite.undress();
			sprite.setBlush(2);
			sprite.setMood(Emotion.dominant);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("You and Angel borrow a small fitness room and some wrestling mats for some sparring practice. She suggests clothing removal as the victory condition instead of " +
						"pins or submission, which doesn't surprise you at all.<p>"
						+ "What you weren't expecting was for her to invite an audience. Strip wrestling has become a normal nightly activity for " +
						"you now, but the thought of doing it in front of three uninvolved girls is still a little unsettling.<p>"
						+ "You do some simple stretches to warm up and Angel does the same. "
						+ "When you " +
						"feel sufficiently limber, you ask Angel if she's ready to start.<i>\"I'm ready. I borrowed a pair of panties so we'd have about the same amount of clothing.\"</i><p>"
						+ "Wait... what? " +
						"You know from sexfighting her that she often forgoes a bra, but was she not even wearing panties? Whose panties are she wearing now? No. It's probably not a good idea to let yourself " +
						"get distracted before a match. You try your best to put it out of your mind.<p>"
						+ "Angel moves first, lunging toward you and trying to take out your legs, but you move fast and step " +
						"to the side. She catches one of your legs, but now her momentum is carrying her the wrong way and she falls to the mat. You manage to regain your balance and grab the waistband " +
						"of her shorts, but she rolls out of reach and scrambles to her feet.<p>"
						+ "Caroline gives a low whistle. <i>\"They're both pretty good at this,\"</i> you hear her say. <i>\"When did Angel learn " +
						"to wrestle?\"</i><p>"
						+ "<i>\"They must do this a lot. Do you think it's his fetish or hers?\"</i> Mei this time.<p>"
						+ "While you're distracted by the side conversation, Angel closes the distance again " +
						"and manages to get a hold of your shirt. Pulling you off balance, she manages to trip you and strip off your shirt at the same time. Thinking fast, you manage to catch hold of her " +
						"shorts as you fall, pulling them down to her ankles. As she stumbles back, you escape with shorts in hand.<p>"
						+ "The net result is you are shirtless and Angel is wearing her T-shirt " +
						"and a pair of bright red, side-tie panties. She looks good in them, but she's going to have a hell of a time trying to keep those panties on.<p>"
						+ "You take the initiative this time, " +
						"feinting toward her panties, but actually taking her legs out from under her and landing on top. You take this opportunity to try to pull her top off, but she defends it frantically. " +
						"In response, you change your target and successfully untie both sides of her panties and pull them off.<p>"
						+ "While your hands are occupied though, Angel suddenly slips her hand down the front of your pants and grabs " +
						"your balls. After that, pain happens.<p>"
						+ "When the spectators see your plight, they each react differently. Mei cheers enthusiastically, <i>\"Come on Angel, twist his balls off!\"</i><p>"
						+ "Caroline is a " +
						"bit more sympathetic, though her cheeks are tinted with a light blush. <i>\"Ouch, poor boy. That's probably checkmate.\"</i><p>"
						+ "Sarah says nothing, but is flushed with arousal and has a hand between " +
						"her legs.<p>"
						+ "Angel herself has a smile that's equal parts sadistic and affectionate. She kisses you while trying to work off your pants with her free hand, but eventually decides she needs both hands " +
						"and releases your genitals. Most of the fight has already been squeezed out of you and you collapse to the floor.<p>");
			Global.gui().displayImage("premium/Angel Wrestling.jpg", "Art by AimlessArt");	
			Global.gui().message("Angel straddles your head while she strips off your pants and underwear. " +
						"Her naked, wet pussy hovers right in front of your face, and despite the pain, you can't help feeling turned on by the view and her scent. It's not until she grabs your dick and starts " +
						"jerking you off that you realize you're completely hard. You blush in embarrassment when you realize Angel is showing you off to her friends, but you can't resist the pleasure she's giving " +
						"you.<p>"
						+ "In no time, you disgracefully cum in front of four horny girls, leaving your balls even more sore than before.");					
			Global.gui().choose(this,"Leave");
			Daytime.train(player,angel,Attribute.Power);
			Roster.gainAffection(ID.PLAYER,ID.ANGEL,1);
			Global.gui().message("<b>You gained affection with Angel.</b>");
		} else if(choice.contains("Fighting Dirty")){
			sprite.undress();
			sprite.setBlush(3);
			sprite.setMood(Emotion.desperate);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("Your strip wrestling with Angel has become a routine secondary version of your nightly competitions. Unfortunately, in this venue, Angel has a much better " +
					"win rate than you. You've also gotten used to your three girl audience.<p>"
					+ "Mei's sadistic side comes to the fore as she cheers on Angel whenever she's got the upper hand. Caroline, " +
					"on the other hand, consistently encourages the underdog, usually you. Sarah seems to support both of you evenly, but spends most of the matches covertly masturbating to the " +
					"show.<p>"
					+ "This match seems pretty par for the course. You've managed to get Angel to her bra and panties (that's good), but you're down to just your boxers (that's bad).<p>"
					+ "It's " +
					"going to be a lot harder to remove Angel's underwear than for her to just grab and pull your boxers down. Your best bet is to try to incapacitate her using unconventional techniques " +
					"so you can take your time undressing her. On the subject of unconventional strategy though, your boxers won't provide much protection if Angel starts aiming for your groin.<p>"
					+ "You " +
					"decide it's best to go on the offensive for now. You lunge at Angel and pull her into a bearhug. Being careful not to knock teeth with your aggressive movements, you kiss Angel " +
					"firmly on the lips to distract her as you unhook her bra.<p>"
					+ "She doesn't lose her composure though, and you are forced to give up on the bra when she gets a hold of the waistband " +
					"of your boxers. You struggle to hold your boxers up and you barely manage to pull them out of her grasp and jump away. Since you didn't completely remove the bra, Angel fixes it " +
					"and is no worse of. On the other hand, the tug of war stretched out the elastic waistband of your boxers, making them much looser. You also got a bit of a wedgie from the encounter.<p>" +
					"You're suddenly struck by inspiration, though it's a particularly questionable inspiration. You wait for Angel to charge you to try to finish the match. As soon as she gets within " +
					"reach, you grab the waistband of her panties and pull upwards as hard as you can. Angel yelps as the fabric digs into her delicate pussy. She stops in mid lunge to nurse her girl parts.<p>" +
					"You have to work fast to capitalize on this momentary advantage. You yank her panties down and spank her bare ass cheek. Angel lets out a whimper and you see her shiver in a mixture of " +
					"pain and pleasure. You hit her on the other ass cheek and get the same reaction. Your third slap aims between her legs and hits her squarely on the pussy. She shrieks and falls to " +
					"the mat. You manage to secure her panties and you try to get her bra, but she kicks at you and forces you back.<p>"
					+ "The match is still on, but you clearly came out on top in that " +
					"engagement. You got her panties, but more importantly, you broken Angel's legendary composure. She's angry and humiliated now, and she's not thinking straight.<p>"
					+ "She lunges at you " +
					"recklessly, aiming for your delicates, but you easily counter her. You're able to take control of the match and avoid her clumsy attacks until you find an opening and catch her from " +
					"behind. You manage to undo her bra and pull it off despite her thrashing. Angel finally manages to calm down to a quiet seethe with the realization that she's lost.<p>"
					+ "It occurs to you " +
					"that you haven't heard anything from the spectators in a while. The three of them are all stunned into silence at the turnaround.<p>"
					+ "A sadistic thought crosses your mind and you decide to give " +
					"them an even better show. You turn Angel toward her friends and use your feet to force her legs apart and spread her pussy lips open with your fingers. She flushes deeply at being exposed " +
					"to her friends and you can feel her rapidly getting wetter.<p>"
					+ "Mei and Sarah are staring at Angel and you can see desire in their eyes, while Caroline mostly looks embarrassed about the " +
					"situation. You begin to finger Angel while licking and sucking her neck. As she moans in pleasure, you whisper sadistic questions in her ear.<p>"
					+ "Does her pussy still hurt from being slapped?<br>" +
					"Is she getting off on being watched?<br>"
					+ "Is she ashamed to cum in front of her friends?<p>"
					+ "At the last question, she shakes her head vigorously. <i>\"They-AH! T-they're my best friends. I won't " +
					"hide anything from themMM!... or you. I-I want the people I love to see everything about me. Even-AH! this!\"</i><p>"
					+ "You feel Angel tense up in you arms and a flood of love juice hits your " +
					"hand as she orgasms. As she goes limp in your arms, the other girls snap out of whatever what keeping them silent and they gather around you. Mei stands nearby, but can't seem to make eye " +
					"contact with Angel. Caroline pats Angel on the head while congratulating you on your victory. Sarah crouches next to Angel and kisses her on the cheek affectionately. It occurs to you, not " +
					"for the first time, what a close group of friends they are. You feel honored that they're starting to count you as one of them.");
			if(!player.has(Trait.disciplinarian)){
				Global.gui().message("<p><b>You've learn how to spank your opponent in a way that can ruin their morale.</b>");
				player.add(Trait.disciplinarian);
				angel.add(Trait.disciplinarian);
			}
			Global.gui().choose(this,"Leave");
			Daytime.train(player,angel,Attribute.Power);
			Roster.gainAffection(ID.PLAYER,ID.ANGEL,1);
			Global.gui().message("<b>You gained affection with Angel.</b>");
		}
		else if(choice == "The Gauntlet"){
			Global.gui().message("When you get back to the girls' suite, you bring up something a topic that was sitting in the back of your mind. "
					+ "Caroline mentioned the Gauntlet earlier in passing. It sounded like you were suppose to know what that meant.<p>"
					+ "Angel raises an eyebrow. <i>\"Have you really not heard of the Gauntlet? Not even a rumor?\"</i> <p>"
					+ "<i>\"There's no way!\"</i> Mei gives you an accusatory glare. <i>\"Guys always gossip about this stuff. If you're dating "
					+ "Angel, someone must have said something. Half her boyfriends ask for the Gauntlet less than a week after they start going "
					+ "out. If you want to try it, I'll make you cum in no time.\"</i><p>"
					+ "<i>\"Are we doing this? I'll go get my special socks!\"</i> Sarah rushes out of the room excitedly before anyone can stop "
					+ "her. <p>"
					+ "Caroline is the voice of reason here. <i>\"Let's explain what we're talking about first. Then "+player.name()+" can "
					+ "decide if he's interested. Sound good, Mei?\"</i><p>"
					+ "<i>\"Sure. I'll explain in case you haven't heard the details. Each of us, one at a time, will use our favorite "
					+ "techniques to make you cum. You will be naked, we get 3 minutes each, and you aren't allowed to resist in any way.\"</i> "
					+ "As she's talking, Sarah returns and shows off some soft looking knee socks. <i>\"If you can outlast all four of us, "
					+ "we'll be your naked slaves for an hour and do whatever you want. If you can't-\"</i><p>"
					+ "<i>\"With some restrictions.\"</i> Caroline interjects.<p>"
					+ "<i>\"Right, with some restrictions. Anyway, if we do make you cum-\"</i><p>"
					+ "<i>\"Don't gloss over that part! We need to clearly establish the terms of the bet before we start. There are a couple "
					+ "things he can't request, even if he wins.\"</i> Caroline takes over the explanation for a moment. <i>\"I have a firm "
					+ "rule against kissing or fucking guys who I'm not romantically involved with. Petting and oral are fine, I just won't "
					+ "do those two things. Also, Sarah is not wagering her virginity on a dumb contest.\"</i><p>"
					+ "Sarah blushes as her sexual experience (or lack there of) is brought up. <i>\"If he actually makes it past all four of us, I m-might "
					+ "consider...\"</i> She doesn't sound particularly convincing.<p>"
					+ "Angel pats the shy girl on the head. <i>\"Hush you. Your first time is important. Save if for someone you really like. "
					+ "I wish I had.\"</i> She glances in your direction. <i>\"There's also a shirt.\"</i><p>"
					+ "<i>\"Oh, right.... I think I know where that is.\"</i> Caroline hops out of her chair and disappears into her room. "
					+ "She returns a moment later holding a T-Shirt.<br> "
					+ "The T-Shirt says �eThe Legendary Pussy Slayer' with an arrow pointing down. <i>\"A prize for the first man to survive "
					+ "the Gauntlet. It's one of a kind... we think. At least we've never seen it anywhere else. I don't even remember where "
					+ "we found it.\"</i><p>"
					+ "<i>\"I saw it at a street vendor in Singapore,\"</i> Mei supplies. <i>\"It doesn't matter. He's not going to win. "
					+ "No boy has gotten past the second person-\"</i><p>"
					+ "<i>\"No, I definitely made a couple boys ejaculate with my special socks.\"</i> Sarah brandishes her sock-covered "
					+ "feet proudly.<p>"
					+ "<i>\"We didn't always stick to the same order,\"</i> Caroline explains to her. <i>\"Sometimes you or Angel went second, "
					+ "but I'm sure no one ever made it to the third person.\"</i><p>"
					+ "<i>\"Anyway!\"</i> Mei raises her voice to get everyone's attention. <i>\"If we make you cum, you stay naked and "
					+ "do whatever we say for an hour. I bet you'd do that if Angel demanded it anyway, so you aren't really losing "
					+ "anything.\"</i><p>"
					+ "She leans toward you with a highly suggestive expression. <i>\"If you want to try it, get naked. I'm going first.\"</i>");
			Global.gui().choose(this,"Do it", "You're no ordinary guy. They probably haven't had to face an experienced sexfighter before.");
			Global.gui().choose(this,"Decline", "Perhaps another day");
		}
		else if(choice == "Do it"){
			Global.gui().message("The four girls watch eagerly as you undress. This shouldn't be anything new to you, given your experience in the Games. "
					+ "However, this is making you a little excited and nervous. Four girls servicing you for three minutes each, and you can't defend "
					+ "yourself? If that happened during a match, you wouldn't last very long.<p>"
					+ "<i>\"Hard already?\"</i> Mei giggles as you remove your underwear. <i>\"This will be easier than I thought.\"</i> Crap. Your "
					+ "imagination is getting to you already. A room full of girls focusing on your nudity, plus the anticipation of what's coming. "
					+ "You're at a disadvantage before anyone even touches you.<p>"
					+ "Mei takes her time admiring your naked body. The clock hasn't started yet, so maybe she's trying to tease you as much as possible "
					+ "in advance. <i>\"Such a delicious looking boy. Shame on you for keeping him to yourself, Angel.\"</i><p>"
					+ "Angel chuckles quietly. <i>\"I don't mind sharing, if you don't mind being punished.\"</i><p>"
					+ "Mei kneels between your legs. You can feel her hot breath on your dick. <i>\"This is allowed at least.\"</i> She nods to Sarah, "
					+ "who is holding a stopwatch.<p>"
					+ "<i>\"Start!\"</i><p>"
					+ "Mei immediately takes your entire length into her mouth. You barely suppress a moan at the heat and wetness of her mouth. "
					+ "She may not have a sex-fighter's experience, but clearly she has plenty of practice giving blowjobs.<p>"
					+ "She almost completely removes your penis from her mouth to focus on the head for a few seconds. Before you can get used to "
					+ "the intense feeling of her lips and tongue on your sensitive glans, she deep throats you again.<p>"
					+ "You're having trouble enduring this sensation before she starts using her hands, but when she massages your balls, you can't "
					+ "help groaning in pleasure. Her eyes turn up to meet yours, locking you in her stare. Those eyes show total confidence in "
					+ "her victory.<p>"
					+ "You close your eyes to try to resist. Suddenly, her intense service stops. Is that it? Did you last for three minutes?<p>"
					+ "The moment your guard drops, Mei attacks the head of your penis again. You let out an embarrassing moan at the sudden pleasure. "
					+ "She took advantage of you closing your eyes to catch you by surprise.<p>"
					+ "You're on your last leg now. Mei holds your head in her mouth and sucks hard while pumping the shaft with her hands. She's "
					+ "clearly ready to finish you off. Time has to be almost up. Can you hold on?");
			player.pleasure(100, Anatomy.genitals);
			if(player.getArousal().max() >= 100){
				Global.gui().choose(this,"Endure","This is a pretty good blowjob, but you've had better. You can resist this.");
			}
			Global.gui().choose(this,"Cum in Mei's mouth","Three minutes of this is too much to handle. You're going to cum!");
		}else if(choice == "Endure"){
			Global.gui().message("<i>\"Time!\"</i> Sarah calls out, holding up the stopwatch.<p>"
					+ "Mei reluctantly stops her blowjob, giving you a sour look. You let out a deep breath of relief as she walks "
					+ "away.<p>"
					+ "Caroline quickly takes her place, not giving you time to recover. <i>\"Not bad. Most guys can't get past Mei. "
					+ "I'll enjoy finishing you off.\"</i> She gives you a friendly smile as she grabs your still-wet dick. <p>"
					+ "<i>\"Start!\"</i><p>"
					+ "You grunt and clench your fists as Caroline starts jerking you off. Within a few seconds, it's clear she's "
					+ "every bit as skilled with her hands as the girls you fight each night. Her hands pump your shaft at just "
					+ "the right speed, making good use of Mei's saliva as lubricant. <p>"
					+ "Her face flushes with arousal as she plays with your penis, but her smile doesn't change. Her grin has none "
					+ "of Mei's sadism, but all of the same confidence. <p>"
					+ "With good reason. She winks as you and twists a hand around the head of your dick, making your hips buck "
					+ "involuntarily. She's playing you like an instrument, and you're helpless to resist.<p>"
					+ "<i>\"It's hard to tell how much of this is spit, and how much is precum.\"</i> Caroline jerks your cock faster "
					+ "to emphasize the wet noise she's making. <i>\"Sounds like you're close to bursting.\"</i><p>"
					+ "You moan pitifully as she fondles your jewels with her other hand. You are dangerously close. Can you last "
					+ "just a little longer?");
			player.pleasure(80, Anatomy.genitals);
			if(player.getArousal().max() >= 180){
				Global.gui().choose(this,"Endure!","You aren't done yet!");
			}
			Global.gui().choose(this,"Cum in Caroline's hands","Caroline's really good at milking guys. You're going to cum!");
		}else if(choice == "Endure!"){
			Global.gui().message("<i>\"Time!\"</i> Sarah sounds genuinely surprised, and more than a little excited. "
					+ "<i>\"He actually endured Caroline's handjob!\"</i><p>"
					+ "<i>\"Aww.\"</i> Caroline sounds disappointed as she releases you. <i>\"I must be losing my touch.\"</i> "
					+ "You emphatically shake your head at that statement. That was way too close.<p>"
					+ "<i>\"Come on, Sarah, finish him off!\"</i> Mei urges the quiet girl forward. <i>\"You're not suppose to "
					+ "give him time to recover.\"</i><p>"
					+ "\"<i>I need his chair!\"</i> Sarah scrambles to prepare for her turn and makes you sit on the floor. "
					+ "At least you get a brief moment of respite. <i>\"Someone get the stopwatch!\"</i><p>"
					+ "<i>\"I got it.\"</i> Caroline takes the stopwatch as Sarah sits down in front of you. <i>\"Start!\"</i><p>"
					+ "Sarah captures your erect dick between her feet, rubbing your length with her soft socks. You wouldn't "
					+ "expect the shy girl to be able to give a good footjob, but she clearly has some practice. <p>"
					+ "<i>\"Don't these socks feel nice? I only wear them for special occasions like this.\"</i> Her face is "
					+ "completely flushed, she was clearly turned on before her turn started. If this were a match, you'd be "
					+ "pretty confident of making her orgasm first, but right now you have to sit still and let her have her "
					+ "way with you.<p>"
					+ "One of her soft feet focuses on the head of your cock, while the other toys with your testicles. This would "
					+ "be a humiliating way to lose the challenge, but it's a definite possibility. Your dick has already been "
					+ "stimulated to its limits, and Sarah's special socks feel really good.");
			player.pleasure(80, Anatomy.genitals);
			if(player.getArousal().max() >= 260){
				Global.gui().choose(this,"ENDURE!!","Like Hell you're going to cum from this footjob!");
			}
			Global.gui().choose(this,"Cum on Sarah's feet","Sarah's special socks are too much. You're going to cum!");
		}else if(choice == "ENDURE!!"){
			Global.gui().message("<i>\"Time!\"</i> Caroline looks at the stopwatch in disbelief. <p>"
					+ "Sarah stops her footjob, looking tired. <i>\"You didn't like my special socks?\"</i> "
					+ "She sounds a little hurt.<p>"
					+ "Mei stomps her foot in frustration. <i>\"Seriously!? Did you two go easy on him? The pride of our "
					+ "undefeated team is on the line. Angel, you got this?\"</i><p>"
					+ "Angel smiles seductively and peels off her top. \"<i>No problem. I know exactly how to make "+player.name()+" "
					+ "submit.\"</i><p>"
					+ "You groan quietly. You barely made it past a trio of amateurs. You almost forgot they had a pro in reserve. "
					+ "Three minutes is plenty of time for Angel to make you cum, even if you hadn't been edged by three "
					+ "other girls.<p>"
					+ "Angel lays down on top of you and wraps her bountiful breasts around your cock. Her dominant expression "
					+ "tells you she's not planning to go easy on you.<p>"
					+ "<i>\"Start!\"</i> Sarah calls out as she reclaims the stopwatch.<p>"
					+ "Angel squeezes her soft boobs together and rubs your shaft between them. Her luxurious chest is a "
					+ "dangerous weapon in the Games, but usually you don't have to just lay there and take it. This will be "
					+ "a problem.<p>"
					+ "<i>\"Does it feel good?\"</i> Angel taunts you seductively. <i>\"I know how much you like these. I've lost count "
					+ "of how many times you've cum all over them.\"</i><p>"
					+ "You want to think up a comeback, but all your focus is currently on trying to keep yourself from ejaculating. "
					+ "Can you scale these final mountains?");
			player.pleasure(80, Anatomy.genitals);
			if(player.getArousal().max() >= 340){
				Global.gui().choose(this,"ENDUUUURE!!!","This is the final stretch! You can do it!");
			}
			Global.gui().choose(this,"Cum on Angel's breasts","This isn't like the other girls. Angel is a pro. You're going to cum!");
		}else if(choice == "ENDUUUURE!!!"){
			Global.gui().message("<i>\"Time!\"</i> Sarah yells, looking completely astonished.<p>"
					+ "Angel stops her titjob for a moment, looking equally surprised. She soon redoubles her efforts. The precum leaking from "
					+ "your cock lubricates her breasts, making it even harder for you to resist. <p>"
					+ "<i>\"That's enough!\"</i> Caroline calls out to Angel, trying to stop her. <i>\"Sarah called time! "+player.name()+
					" won already!\"</i><p>"
					+ "Angel shakes her head and looks you directly in the eye. <i>\"You've been enduring for a long time. Don't hold "
					+ "back. The first part of your reward is a good orgasm.\"</i> She takes the head of your penis in her mouth and "
					+ "starts to suck.<p>"
					+ "You cum with a loud grunt. Your pent-up ejaculation fills Angel's mouth, until she's unable to contain it, "
					+ "letting excess jizz spill out onto her chest.<p>"
					+ "There's a long moment while Angel struggles to swallow your load and the other girls are still stunned to "
					+ "silence. Caroline speaks up first.<p>"
					+ "<i>\"Well... Congrats on being the first guy to finish The Gauntlet. You've won the shirt... and us kinda.\"</i> "
					+ "She glances awkwardly at the other girls. <i>\"So, uh... What do you want to do with us?\"</i><p>"
					+ "...<p>"
					+ "While the girls strip naked, you get dressed and take a seat on the couch. You aren't embarrassed about your nudity "
					+ "anymore, but it's the principle of the thing. This is your victory celebration, so you should be the one dressed "
					+ "for the occassion.<p>"
					+ "Frankly, you're still satisfied and a bit tired after giving Angel a mouthful of cum. You've be pretty comfortable "
					+ "just lounging on the couch and having the girls pamper you. However, with the amount of effort and perserverence you "
					+ "put into winning this thing, it would be an unacceptable waste not to sample the naked beauties in front of you.<p>"
					+ "You may as well start with the two you aren't allowed to fuck. You order Caroline and Sarah to sit on either side of you."
					+ "Caroline looks only a little embarrassed to be naked next to you, but Sarah is beet-red as she tries to cover herself "
					+ "with her hands. However, when you slip a hand into each of their pussies, you find her even wetter than Caroline.<p>"
					+ "Both girls moan in response to your touch, but with your four girl harem, you could use something more than your bare "
					+ "hands. You send Mei to retrieve a couple sex toys, which you had no doubt at least one of them had under their bed. "
					+ "She returns with a pair of small capsule vibrators. You make good use of the toys, dual-wielding them to tease both "
					+ "the girls simultaneously.<p>"
					+ "While you're focused on pleasuring the Caroline and Sarah, you notice that they are both focused on the tent in your pants. "
					+ "Playing with their pussies has helped you recover from your last orgasm and you're rock hard. You ask if they've changed "
					+ "their minds about about the no-sex rule.<p>"
					+ "Caroline shakes her head. <i>\"I just want to see it. I like dicks, ok? It'll help me finish.\"</i> Sarah doesn't say "
					+ "anything, but nods emphatically. You give them permission to take it out of your pants, and they both scramble to do "
					+ "so while enduring the pleasure from the vibes.<p>"
					+ "As your cock is exposed, both girls stare at is fascinated. You can feel them both become more responsive to your touch "
					+ "as the sight fuels their fantasies. You tease Caroline's clit and ask her what she thinks about it.<p>"
					+ "<i>\"Ah! It's a good size and shape. If you were my boyfriend, I'd love to have that in me.\"</i> You stick the vibrator "
					+ "inside her as a reward for answering and repeat the question to Sarah.<p>"
					+ "<i>\"I-I like it!\"</i> She can barely get out the words between moans. It's not a great answer, but she's clearly too "
					+ "close to orgasm to elaborate. You push her vibrator inside as well.<p>"
					+ "You continue to pleasure both of the girls until they climax in your hands. You retrieve the vibrators from inside them as "
					+ "they recover. That's enough foreplay for now, you're ready to go again, and Mei doesn't mind getting fucked, right?<p>"
					+ "Mei eagerly hops onto the couch and spreads her legs for you. She's visibly soaked and ready for you. You're about to "
					+ "plunge in, when Angel stops you and hands you a condom. Oh right. Mei isn't a participant in the Games, so you can't take "
					+ "birth control for granted.<p>"
					+ "Angel speaks up while you're busy putting on the condom. <i>\"Mei can be a bit of a screamer during sex. I recommend covering "
					+ "her mouth with something so that the neighbors don't complain.\"</i> You're sure she has something in mind to cover it with. "
					+ "Angel's slit is visibly wet with arousal. Well, you don't see any reason why not. It should make them both happy.<p>"
					+ "You have Mei lie down on the couch while Angel straddles her face. Mei gives a token protest, but sounds far too eager to be "
					+ "convincing. You wait for Mei to start eating out Angel, before thrusting your cock into her waiting pussy. You hear a muffled "
					+ "moan from under Angel, as you bury yourself up to the hilt.<p>"
					+ "You start to move your hips vigorously, pleasuring yourself and Mei. At the same time, Mei busies her tongue pleasuring Angel. "
					+ "You quickly discover Mei is extremely sensitive, and you feel her shudder in orgasm several times before either you or Angel is "
					+ "close to the edge. Instead, you pace you thrusting to try to synchronize your climax with Angel's. Your experience in the Games "
					+ "has given you both the precise control you need and the ability to judge Angel's progress.<p>"
					+ "As Angel begins riding Mei's face more aggressively, you speed up your thrusts. Mei shrieks in response, but it's mostly muffled "
					+ "by Angel's muff. The three of you cum together in a moaning, trembling heap.<p>"
					+ "You slump on the couch as Angel takes care of the used condom. You still have some time left in the wager, but now you really do "
					+ "just want to relax.");
			player.gain(Clothing.legendshirt);
			Global.gui().choose(this,"Leave");
		}else if(choice == "Cum in Mei's mouth"){
			Global.gui().message("You let out a groan of resignation as you feel the first burst of semen force its way through your urethra. "
					+ "Mei takes the first jet in her mouth, but pulls her head away to let the rest of your load spray into the air. <p>"
					+ "<i>\"Ha! No problem at all!\"</i> Mei continues to jerk you with her hand. Her other hand grabs your balls hard as if to "
					+ "squeeze out as much semen as possible. The other girls grin with delight as they enjoy the show.<p>"
					+ "<i>\"Not even three minutes. You never had a chance. You just wanted me to blow you, didn't you. Well, you better work hard "
					+ "to entertain us as payment.\"</i>");
			player.getArousal().empty();
			Global.gui().choose(this,"Punishment");
		}else if(choice == "Cum in Caroline's hands"){
			Global.gui().message("You clench your first until your knuckles turn white, but you can't resist Caroline's skilled handjob. "
					+ "Your cum erupts like a geyser and hits her directly in the face. She laughs in surprise and victory as she aims the rest of "
					+ "your ejaculation straight into the air. <p>"
					+ "<i>\"Wow. You made a big mess\"</i> She licks some of your semen off her lips. <i>\"Your first job as servant will be cleaning "
					+ "it up. There are tissues over there.\"</i><p>"
					+ "Her calm smile is an oasis of kindness as you notice the other girls making plans for you. <i>\"Don't feel too bad, I think "
					+ "you got pretty close to the time limit. That might even be a record.\"</i>");
			player.getArousal().empty();
			Global.gui().choose(this,"Punishment");
		}else if(choice == "Cum on Sarah's feet"){
			Global.gui().message("You groan as your pent up cum shoots out, staining Sarah's sock-covered feet. <p>"
					+ "<i>\"Yes! Another victim of my special socks!\"</i> Sarah cheers and continues to pump your erupting dick "
					+ "with her feet. She continues to milk you until you're completely spent.<p>"
					+ "You slump to the floor, exhausted. Sarah looks almost equally tired and overstimulated. She wasn't touching "
					+ "herself, but you suspect she may have orgasmed with you. She quickly starts peeling off her knee-socks.<p>"
					+ "<i>\"Ok, you need to wash my special socks quick before they stain. I'm gonna get my camera and we'll have a "
					+ "naked photo shoot when you're back.\"</i>");
			player.getArousal().empty();
			Global.gui().choose(this,"Punishment");
		}else if(choice == "Cum on Angel's breasts"){
			Global.gui().message("You groan in pleasure and resignation as that old familiar feeling hits you. You're going to "
					+ "lose to Angel's breasts. Angel grins as she realizes the same thing.<p>"
					+ "<i>\"Ready to blow? Go ahead, you've earned it.\"</i> She squeezes your cock between her boobs, pushing "
					+ "you past your limits. You erupt like a geyser, spraying her face and chest with your seed.<p>"
					+ "You lie exhausted on the floor as Angel stands up triumphantly, scooping up some semen from her breasts "
					+ "and tasting it. Her predatory smile tells you that she never had any doubt of winning. <p>"
					+ "<i>\"It looks like you enjoyed that. You must really like my breasts. You held out so long for a chance "
					+ "to cum between them.\"</i> She licks up some more semen and savors it slowly. "
					+ "<i>\"Well, I'm satisfied, but I think my teammates have some plans for you.\"</i>");
			player.getArousal().empty();
			Global.gui().choose(this,"Punishment");
		}else if(choice == "Punishment"){
			Global.gui().message("About twenty minutes later, you're still naked and taking orders from the girls. You're not "
					+ "even halfway through the one hour wager, but you're already exhausted.<p>"
					+ "You bring a soft drink to Caroline, who is relaxing in an armchair while watching TV. <i>\"Thank you, "
					+ "butler.\"</i> She grins up at you as she takes the drink. Her eyes drop to your erect penis, roughly "
					+ "level with her eyes.<p>"
					+ "<i>\"You look pretty pent up. Are the other girls giving you a hard time?\"</i> That's an understatement. "
					+ "Mei has been edging you every chance she gets, while Sarah has been making you pose in humiliating "
					+ "positions so she can take pictures.<p>"
					+ "Caroline gives you a sympathetic smile and casually grasps your dick. <i>\"Poor thing. How about "
					+ "I relieve some of the pressure for you?\"</i> She skillfully strokes you with one hand. You moan "
					+ "quietly at her handjob. As close to the edge as you are, it won't take her long to finish you off.<p>"
					+ "<i>\"Oh no you don't!\"</i><p>"
					+ "Just before you hit the point of no return, Mei grabs your balls from behind and squeezes roughly. "
					+ "A grunt escapes you as your orgasm is abruptly halted. <p>"
					+ "<i>\"You got into this situation because of this leaky cock of yours. You should take that as a clue to "
					+ "learn some patience. You can cum when your time is up.\"</i> Mei gives your jewels a cruel squeeze for "
					+ "emphasis.<p>"
					+ "<i>\"Oh! Hold that pose!\"</i> Sarah rushes over with her camera and takes a picture of the two girls "
					+ "holding your genitals. <i>\"The moment when a man's pleasure and lust turns into pain and frustration "
					+ "is such a great image.\"</i><p>"
					+ "Caroline gives up on the handjob with a shrug. <i>\"Sorry. I tried.\"</i> She looks a bit more amused "
					+ "than sympathetic now. <p>"
					+ "Mei thankfully releases your sack and prods you toward the sofa with a light slap on the butt. "
					+ "<i>\"Hey Angel, don't you want to join in? You've just been watching this whole time.\"</i><p>"
					+ "Angel lounges on the sofa contently. <i>\"I'm enjoying the show. Besides, I can have him whenever I want. "
					+ "You three have fun now.\"</i> She smiles at you without pity.<p>"
					+ "Mei gives you another prod and sits next to Angel. <i>\"Sarah, do you have any other poses you want to "
					+ "photograph? I'm thinking I could use a footrest.\"</i><p>"
					+ "<i>\"Ooh, sounds good.\"</i> Sarah checks her camera. <i>\"I've got plenty of memory left.\"</i><p>"
					+ "Still half an hour left. It's going to feel pretty long.");
			Global.gui().choose(this,"Leave");
		}else if(choice == "Decline"){
			Global.gui().message("<i>\"Hmmph! Pussy.\"</i><p>"
					+ "Mei turns away dismissively and sits down. Well, she's a disappointed, but there's plenty of other things you can do.");
			promptScenes();
		}		
		else if(choice == "Leave"){
			Global.setCounter(Flag.AngelDWV, 0);
			Global.gui().showNone();
			done(true);
		}
	}
	private void promptScenes(){
		Global.gui().choose(this,"Sex","Getting clean and dirty in the shower");
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=12){
			Global.gui().choose(this,"Kissing Practice","Angel thinks you need more practice kissing. Mei is happy to help");
		}
		Global.gui().choose(this,"Games","A classic party game with some sexy twists");
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=8){
			Global.gui().choose(this,"Bluffing","If you want to win this stripping game, you'll need to bluff like a pro");
		}
		Global.gui().choose(this,"Sparring","Strip wrestling? It really seems to suit Angel");
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=16){
			Global.gui().choose(this,"Fighting Dirty","You'll need to fight a little dirty to outmatch Angel");
		}
		if(Roster.getAffection(ID.PLAYER,ID.ANGEL)>=6){
			Global.gui().choose(this,"The Gauntlet");
		}
	}
	@Override
	public void shop(Character npc, int budget) {
		npc.gainAffection(angel,1);
	}
}
