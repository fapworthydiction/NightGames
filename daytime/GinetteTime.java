package daytime;

import characters.Character;
import characters.ID;
import global.Flag;
import global.Global;
import global.Roster;

public class GinetteTime extends Activity{
    boolean acted;
    public GinetteTime(Character player) {
        super("Ginette", player);
    }

    @Override
    public boolean known() {
        return Global.checkFlag(Flag.metGinette);
    }

    @Override
    public void visit(String choice) {
        Global.gui().clearText();
        Global.gui().clearCommand();
        if(choice=="Start"){
            acted = false;
        }
        if(choice=="Leave"){
            done(acted);
            return;
        }

        if(Roster.getAffection(ID.PLAYER,ID.GINETTE)<1){
            Global.gui().message("You meet up with Ginette at the Student Union. She’s got a small storage room basically to herself, where she organizes the item cache boxes. It’s a bit cramped, but very private. She seems a bit nervous about your company in such a cramped space.<br>" +
                    "<br>" +
                    "The shelves around you are packed with boxes filled with familiar potions, sexy clothes, and random junk. Who picks out this stuff?<br>" +
                    "<br>" +
                    "<i>\"I do.\"</i> She fiddles with her ponytail and looks away, embarrassed. <i>\"I asked some of the older competitors what stuff they used to buy for the Games. I also look around obscure stores and websites for anything that might be useful to you guys.\"</i><br>" +
                    "<br>" +
                    "That’s a lot of responsibility for a freshman. They must have a lot of faith in her.<br>" +
                    "<br>" +
                    "<i>\"You know my big sister is the supervisor, right? Besides, this isn’t really an established job. When Lilly recruited me, I just ran around cleaning up after the match. I thought scattering some free items in the match area would help spice things up a bit. I mentioned it to Lilly, and she got me a small budget to buy stuff.\"</i><br>" +
                    "<br>" +
                    "Well, it was a good idea she had. Was putting them in puzzle boxes her idea too? <i>\"No, that was Lilly’s idea. She thought giving you guys stuff for free was too easy. She wanted you to earn it in some way.\"</i> <br>" +
                    "<br>" +
                    "That sounds like what you know about Lilly. Well, you can’t complain too much. It is still some extra help during matches.<br>" +
                    "<br>" +
                    "You look through the nearby shelves. Some of this stuff is pretty easy to buy in nearby stores, but some of it you’ve never seen for sale. Frankly, you’d like to see more obscure equipment in the caches.<br>" +
                    "<br>" +
                    "<i>\"Yeah, I figured. Unfortunately the rare stuff is pretty pricey. If I want to keep planting as many caches as I currently do, some of them need to be filled with the cheap stuff. Like I said: small budget.\"</i><br>" +
                    "<br>" +
                    "How small?<br>" +
                    "<br>" +
                    "<i>\"Well, better than the average extracurricular activity budget, but still pretty small compared to what you Fuck Buddies earn.\"</i> She seems to be comfortable calling you that to your face now. <i>\"I don’t suppose you’d consider donating some of your winnings to expand my budget?\"</i> She gives you a pretty persuasive puppy-dog stare.<br>" +
                    "<br>" +
                    "Well, it’s not like you’re just banking your prize money. If you want to stay competitive, there’s a lot of important things you need to spend that money on.<br>" +
                    "<br>" +
                    "<i>\"Improving my budget could be mutually beneficial. I’m pretty sure I have sources that you wouldn’t be able to buy from directly. Just consider it an investment in getting more rare loot.\"</i><br>");
            Roster.gainAffection(ID.PLAYER,ID.GINETTE,1);
            Global.gui().message("<b>You've gained Affection with Ginette</b>");
        }else if(choice.startsWith("More Caches")){
            if(player.money >= 1500){
                player.money -= 1500;
                Global.gui().message("You give Ginette a wad of money so she can put together more item caches each match. <br>" +
                        "<br>" +
                        "<i>\"This is… Wow. This will help my budget a lot. Yeah, I can definitely afford to deploy more caches at night.\"</i> She smiles at you, shyly. <i>\"The guys who help me place them may not appreciate the extra work, but I do. It feels like my item cache idea could actually become more of an important part of the Games.\"</i><br>" +
                        "<br>" +
                        "She puts the money in a small lockbox and fidgets awkwardly for a moment.<br>" +
                        "<br>" +
                        "<i>\"Can I… Can I kiss you? On the cheek, I mean!\"</i> She flushes bright red. <i>\"I should have just done it. It probably wouldn’t be this awkward, right?\"</i><br>" +
                        "<br>" +
                        "While she’s flustered and babbling, you turn your cheek toward her. She hesitates for a moment, but gives you a light peck.<br>" +
                        "<br>" +
                        "<i>\"Thanks.\"</i> She grins at you, still beet red. <i>\"I really appreciate it.\"</i><br>");
                Roster.gainAffection(ID.PLAYER,ID.GINETTE,3);
                Global.gui().message("<b>You've gained Affection with Ginette</b>");
                Global.flag(Flag.MoreCache1);
            }else{
                Global.gui().message("You don't have enough money");
            }
        }else if(choice.startsWith("No Basic Items")) {
            if (player.money >= 2500) {
                player.money -= 2500;
                Global.gui().message("You hand Ginette a modest bundle of money, and her eyes widen when she counts it. Hopefully with this she won’t have to stock so many common items for a while?<p>" +
                        "<i>\"Whoa! Are you serious?! You’re just going to give me this?\"</i> She stares at you with a combination of surprise and gratitude. It shouldn’t be such a shock. As she said, investing in her budget is mutually beneficial.<p>" +
                        "<i>\"I know I said that, but… I mean, yes there will be better stuff available, but your opponents are probably going to get their share too. You could just spend it on yourself...\"</i> She hesitates until it’s clear you aren’t going to ask for the money back, then smiles genuinely.<p>" +
                        "<i>\"I guess I shouldn’t try to talk you out of it. Thanks. No more cheap flasks and potions in the care packages. I’ll sell my current stock and make this money last as long as it needs to.\"</i>");
                Roster.gainAffection(ID.PLAYER, ID.GINETTE, 3);
                Global.gui().message("<b>You've gained Affection with Ginette</b>");
                Global.flag(Flag.CacheNoBasics);
            } else {
                Global.gui().message("You don't have enough money");
            }
        }else if(Roster.getAffection(ID.PLAYER,ID.GINETTE)>=10 && !Global.checkFlag(Flag.CacheFinder)){
            Global.gui().message("When you meet Ginette at the usual location. She seems even more fidgety than usual. She obviously has something to say, but you don't press her on it until she brings it up.<p>" +
                    "<i>\"So... Lilly is very insistent that the Games be fair. Matches are supposed to be pure tests of strategy and skill. But then, she gives you those handicaps, so 'fair' doesn't always mean treating every Fuck Buddy the same.\"</i><p>" +
                    "<i>\"Besides, these item caches aren't even an official part of the Games. They're just something I came up with to spice things up. You've helped a lot to improve these, so I think it's 'fair' if I send you a text to let you know when we place one.\"</i><p>" +
                    "Well, if she wants to show some favoritism, you aren't going to argue. You keep your phone on you during matches anyway, so you'll just keep an eye open for her texts.<p>" +
                    "<i>\"Great! Um... Maybe don't mention this to Lilly or the other competitors, just in case they have a different idea of 'fair'.");
            Global.flag(Flag.CacheFinder);
        }else{
            Global.gui().message("You meet Ginette at the Student Union and help her pack some item caches. She still acts pretty shy, but her body language makes it clear that she enjoys the company. <br>" +
                    "<br>" +
                    "At one point, she loses her balance grabbing something off a high shelf. You catch her before she gets hurt, and she thanks you while blushing furiously.<br>" +
                    "<br>" +
                    "She’s really cute, and it’s kind of refreshing having an opportunity to flirt so innocently. Most of your romantic interests are based on competitive sex. Of course, Ginette here watches your sexfights every night, so maybe it’s not quite so innocent from her perspective.<br>");
            if(!Global.checkFlag(Flag.MoreCache1)){
                Global.gui().choose(this,"More Caches: $1500");
            }
            if(!Global.checkFlag(Flag.CacheNoBasics)){
                Global.gui().choose(this,"No Basic Items: $2500");
            }
            Roster.gainAffection(ID.PLAYER,ID.GINETTE,1);
        }
        Global.gui().choose(this,"Leave");
    }

    @Override
    public void shop(Character npc, int budget) {

    }
}
