package daytime;

import characters.Attribute;
import characters.Character;
import characters.Trait;
import global.Flag;
import global.Global;
import global.Roster;
import items.Attachment;
import items.Toy;

public class PlayerRoom extends Activity {
	private boolean excaliburMenu;
	private boolean acted;

	public PlayerRoom(Character player) {
		super("Your Room", player);
		excaliburMenu = false;
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean known() {
		return true;
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(choice=="Start" || choice =="Back"){
			Global.gui().message("You have a pretty standard single dorm room. It's a little cramped, but at least you don't have to worry about "
					+ "a roommate wondering where you go every night. Your TV is mounted on the wall, leaving you enough room for the bookshelf of "
					+ "board and video games. You try to keep it reasonably tidy in case you have company over, which is "
					+ "a much more likely prospect since you joined the Games.");
			if(player.has(Toy.Excalibur)){
				Global.gui().message("You've moved your computer to one side of your desk so that you have room to tinker with Sexcalibur.");
			}
			if(player.has(Toy.Aersolizer)){
				Global.gui().message("Your multitool is tucked under your couch between matches. Jett would probably not approve, but you don't "
						+ "have a better place to store it.");
			}
			if(player.getPure(Attribute.Dark)>=1){
				Global.gui().message("A small statue sits on your bookshelf, a souvenir of your first ritual with Rin. The curse that once filled "
						+ "the statue is a part of you now.");
			}
			excaliburMenu = false;
			acted = false;
			if(Global.checkFlag(Flag.excalibur)){
				Global.gui().choose(this, "Sexcalibur");
			}
			if(Global.checkFlag(Flag.goldcockstart)){
				Global.gui().choose(this, "Golden Cock");
			}
			Global.gui().choose(this,"Change Clothes");
			Global.gui().choose(this,"Plan Skills");
			Global.gui().choose(this,"Leave");
		}
		else if(choice=="Leave"){
			done(acted);
		}
		else if(excaliburMenu){
			if(choice.startsWith("Order Sexcalibur")){
				if(player.money >= 1000){
					Global.gui().message("This is quite a pricy sex toy. Fortunately, you get free same-day shipping on orders over $900. It'll arrive in "
							+ "time for your match tonight.");
					player.money -= 1000;
					player.gain(Toy.Excalibur);
				}else{
					Global.gui().message("You can't afford this overpriced toy.");
				}
			}
			if(choice.startsWith("Upgrade")){
				if(!player.has(Attachment.Excalibur2)){
					Global.gui().message("Despite the fine build quality, it's pretty easy to disassemble Sexcalibur. You remove the existing motor and "
							+ "replace it with the high-grade model. This new motor is so powerful, you need to limit its maximum output or risk the toy "
							+ "shaking itself to pieces.");
					player.consumeAll(Attachment.Excalibur2.getRecipe());
					player.money -= Attachment.Excalibur2.getPrice();
					player.gain(Attachment.Excalibur2);
				}else if(!player.has(Attachment.Excalibur3)){
					Global.gui().message("Machining this quantity of titanium is no easy feat. Fortunately, the Mechanical Engineering department has some "
							+ "state of the art CNC machining equipment. You get permission to use the equipment with surprising ease. After considerable time "
							+ "and effort, you craft a masterwork metal casing.");
					player.consumeAll(Attachment.Excalibur3.getRecipe());
					player.money -= Attachment.Excalibur3.getPrice();
					player.gain(Attachment.Excalibur3);
				}else if(!player.has(Attachment.Excalibur4)){
					Global.gui().message("This Dragon Bone seems like the ultimate crafting material, but there isn't exactly information on the internet about "
							+ "how to use it. Forging a new Sexcalibur from it requires a combination of lore, science, and divine inspiration, but eventually "
							+ "you succeed. Sexcalibur is now a sex toy of legend, unmatched by anything that has previously been used in the Games.");
					player.consumeAll(Attachment.Excalibur4.getRecipe());
					player.money -= Attachment.Excalibur4.getPrice();
					player.gain(Attachment.Excalibur4);
				}else if(!player.has(Attachment.Excalibur5)){
					Global.gui().message("You've done a lot of advanced customization on Sexcalibur, but the blueprints and components are beyond even your "
							+ "understanding. The technology involved is completely alien. However, even if you can't understand the blueprints, you can "
							+ "still follow them. The resulting sex toy surpasses your wildest expectations.");
					player.consumeAll(Attachment.Excalibur5.getRecipe());
					player.money -= Attachment.Excalibur5.getPrice();
					player.gain(Attachment.Excalibur5);
				}
			}
			if(choice.startsWith("Add")){
				Global.gui().message("With some time and effort, you craft the attachment and add it to Sexcalibur.");
				for(Attachment addon: Attachment.values()){
					if(choice.contains(addon.getFullName(player))){
						player.consumeAll(addon.getRecipe());
						player.money -= addon.getPrice();
						player.gain(addon);
					}
				}
			}
			this.acted = true;
			Global.gui().choose(this, "Leave");
		}
		else if(choice=="Sexcalibur"){
			excaliburMenu = true;
			if(player.has(Toy.Excalibur)){
				if(!player.has(Attachment.Excalibur2)){
					Global.gui().message("Your Sexcalibur is factory standard. It's fine for a sex toy, but the components "
							+ "aren't good enough to justify the price. The best place to start would be replacing the motor with a "
							+ "high end model.");
					if(player.hasAll(Attachment.Excalibur2.getRecipe()) && player.money >= Attachment.Excalibur2.getPrice()){
						Global.gui().choose(this,"Upgrade","Required: High Grade Motor, $"+Attachment.Excalibur2.getPrice());
					}else{
						Global.gui().unchoose(this,"Upgrade","Required: High Grade Motor, $"+Attachment.Excalibur2.getPrice());
					}
				}else if(!player.has(Attachment.Excalibur3)){
					Global.gui().message("This Grand Sexcalibur contains the best small motor on the market. Most of the rest of it "
							+ "is still just plastic. The casing isn't really durable enough to support the maximum vibration the "
							+ "motor is capable of. Improving it further would require rebuilding the whole body out of a stronger "
							+ "material.");
					if(player.hasAll(Attachment.Excalibur3.getRecipe()) && player.money >= Attachment.Excalibur3.getPrice()){
						Global.gui().choose(this,"Upgrade","Required: Titanium, $"+Attachment.Excalibur3.getPrice());
					}else{
						Global.gui().unchoose(this,"Upgrade","Required: Titanium, $"+Attachment.Excalibur3.getPrice());
					}
				}else if(!player.has(Attachment.Excalibur4)){
					Global.gui().message("Sexcalibur is now a genuine masterwork sex toy. Improving it further would require something truly "
							+ "mythical. Before entering the games, that would seem impossible, but your perception of reality and fantasy "
							+ "has been challenged a lot recently.");
					if(player.hasAll(Attachment.Excalibur4.getRecipe()) && player.money >= Attachment.Excalibur4.getPrice()){
						Global.gui().choose(this,"Upgrade","Required: Dragon Bone, $"+Attachment.Excalibur4.getPrice());
					}else{
						Global.gui().unchoose(this,"Upgrade","Required: Dragon Bone, $"+Attachment.Excalibur4.getPrice());
					}
				}else if(!player.has(Attachment.Excalibur5)){
					Global.gui().message("Sexcalibur is now worthy of legend. This toy, crafted out of actual dragon bone, inspires awe "
							+ "in any sexfighter who sees it, let alone feels its potent vibrations. Logic tells you that you've improve "
							+ "this toy to the maximum limit, but your heart whispers that there is still more.");
					if(player.hasAll(Attachment.Excalibur5.getRecipe()) && player.money >= Attachment.Excalibur5.getPrice()){
						Global.gui().choose(this,"Upgrade","Required: Prototype Blueprints, Prototype Vibrator, Prototype Handle, $"+Attachment.Excalibur5.getPrice());
					}else{
						Global.gui().unchoose(this,"Upgrade","Required: Prototype Blueprints, Prototype Vibrator, Prototype Handle, $"+Attachment.Excalibur5.getPrice());
					}
				}else{
					Global.gui().message("Sexcalibur is now the perfect sex toy in every way, surpassing human understanding. If there is "
							+ "any way to improve it further, you dare not even think about it.");
				}if(player.has(Attachment.ExcaliburAnimism)){
					Global.gui().message("The head of the vibrator is covered with soft fox fur and embued with the spirit of a mystical "
							+ "fox creature.");
				}else if(player.getPure(Attribute.Animism) >= 1){
					Global.gui().message("You could probably power up the Sexcalibur using an animal spirit.");
					if(player.getPure(Attribute.Animism) < 12){
						Global.gui().message("You would need to improve your understanding of Animism to attempt it.");
					}else if(!player.hasAll(Attachment.ExcaliburAnimism.getRecipe())){
						Global.gui().message("You would need something with a lot of residual spiritual energy.");
					}else{
						Global.gui().message("The Fox Tail you found could summon a spirit. You'd also need $"+
					Attachment.ExcaliburAnimism.getPrice()+" for additional materials.");
						if(player.money >= Attachment.ExcaliburAnimism.getPrice()){
							Global.gui().choose(this,"Add "+Attachment.ExcaliburAnimism.getFullName(player));
						}
					} 
					   
				}if(player.has(Attachment.ExcaliburArcane)){
					Global.gui().message("The magical enchantment carved into the shaft should draw mana out of the target and into you.");
				}else if(player.getPure(Attribute.Arcane) >= 1){
					Global.gui().message("You could enchant Sexcalibur using Arcane magic.");
					if(player.getPure(Attribute.Arcane) < 12){
						Global.gui().message("You would need to learn more Arcane magic to attempt it.");
					}else if(!player.hasAll(Attachment.ExcaliburArcane.getRecipe())){
						Global.gui().message("You would need help from the faeries. A lot of them");
					}else{
						Global.gui().message("You would need help from the faeries. The summoning scroll could call "
								+ "enough of them to complete the process. You'd also need $"+
					Attachment.ExcaliburArcane.getPrice()+" for additional materials.");
						if(player.money >= Attachment.ExcaliburArcane.getPrice()){
							Global.gui().choose(this,"Add "+Attachment.ExcaliburArcane.getFullName(player));
						}
					}
					Global.gui().message("<br>");
				}if(player.has(Attachment.ExcaliburDark)){
					Global.gui().message("The toys radiates a cursed aura. It should feed off the target's impure thoughts to gain power.");
				}else if(player.getPure(Attribute.Dark) >= 1){
					Global.gui().message("A carefully applied curse could make Sexcalibur more dangerous.");
					if(player.getPure(Attribute.Dark) < 12){
						Global.gui().message("You would need more Dark power to attempt it.");
					}else if(!player.hasAll(Attachment.ExcaliburDark.getRecipe())){
						Global.gui().message("You would need some sort of cursed token.");
					}else{
						Global.gui().message("You could transfer the curse from a Dark Talisman. You'd also need $"+
					Attachment.ExcaliburDark.getPrice()+" for additional materials.");
						if(player.money >= Attachment.ExcaliburDark.getPrice()){
							Global.gui().choose(this,"Add "+Attachment.ExcaliburDark.getFullName(player));
						}
					}
					Global.gui().message("<br>");
				}
				if(player.has(Attachment.ExcaliburFetish)){
					Global.gui().message("A braid of small tentacles are wrapped around the shaft. They should come alive and go wild "
							+ "in response to your arousal.");
				}else if(player.getPure(Attribute.Fetish) >= 1){
					Global.gui().message("This could do with some tentacles. Pretty much everything could.");
					if(player.getPure(Attribute.Fetish) < 12){
						Global.gui().message("You would need to develop your Fetishes more to attempt it.");
					}else if(!player.hasAll(Attachment.ExcaliburFetish.getRecipe())){
						Global.gui().message("You would need something capable of sustaining tentacles.");
					}else{
						Global.gui().message("Your Fetish Totem would work. You'd also need $"+
					Attachment.ExcaliburFetish.getPrice()+" for additional materials.");
						if(player.money >= Attachment.ExcaliburFetish.getPrice()){
							Global.gui().choose(this,"Add "+Attachment.ExcaliburFetish.getFullName(player));
						}
					}
					Global.gui().message("<br>");
				}
				if(player.has(Attachment.ExcaliburKi)){
					Global.gui().message("The handle is specially designed to stimulate your chakras as you hold it. Just using the toy "
							+ "will recover some of your stamina.");
				}else if(player.getPure(Attribute.Ki) >= 1){
					Global.gui().message("With some modification, the handle of Sexcalibur could help open your chakras to improve Ki flow.");
					if(player.getPure(Attribute.Ki) < 12){
						Global.gui().message("You would need to train your Ki more to attempt it.");
					}else if(!player.hasAll(Attachment.ExcaliburKi.getRecipe())){
						Global.gui().message("You would need to embed a powerful item.");
					}else{
						Global.gui().message("You could remake the handle out of this Power Band. You'd also need $"+
					Attachment.ExcaliburKi.getPrice()+" for additional materials.");
						if(player.money >= Attachment.ExcaliburKi.getPrice()){
							Global.gui().choose(this,"Add "+Attachment.ExcaliburKi.getFullName(player));
						}
					}
					Global.gui().message("<br>");
				}
				if(player.has(Attachment.ExcaliburNinjutsu)){
					Global.gui().message("The shaft has been shortened slightly to fit inside a concealed holster. Opponents won't see your "
							+ "attack coming until it's too late to block.");
				}else if(player.getPure(Attribute.Ninjutsu) >= 1){
					Global.gui().message("It would be easier to catch opponents by surprise if they didn't see Sexcalibur attached to your belt. "
							+ "You should craft a hidden holster for it.");
					if(player.getPure(Attribute.Ninjutsu) < 12){
						Global.gui().message("You would need more knowledge of Ninjutsu to attempt it.");
					}else if(!player.hasAll(Attachment.ExcaliburNinjutsu.getRecipe())){
						Global.gui().message("You would need to a very light, but durable material to craft it.");
					}else{
						Global.gui().message("The Serpent Skin you found would be perfect for a holster. You'd also need $"+
					Attachment.ExcaliburNinjutsu.getPrice()+" for additional materials.");
						if(player.money >= Attachment.ExcaliburNinjutsu.getPrice()){
							Global.gui().choose(this,"Add "+Attachment.ExcaliburNinjutsu.getFullName(player));
						}
					}
					Global.gui().message("<br>");
				}
				if(player.has(Attachment.ExcaliburScience)){
					Global.gui().message("A small tube runs up the shaft to the head. It will carry dissolving solution from a tank in the handle "
							+ "to a tiny sprayer.");
				}else if(player.getPure(Attribute.Science) >= 1){
					Global.gui().message("If Sexcalibur could deliver some dissolving solution on contact, you could use it against "
							+ "opponents who still have their clothes on.");
					if(player.getPure(Attribute.Science) < 12){
						Global.gui().message("You would need more Science knowledge to attempt it.");
					}else if(!player.hasAll(Attachment.ExcaliburScience.getRecipe())){
						Global.gui().message("There isn't a lot of space in the head. You'd need a very compact delivery system.");
					}else{
						Global.gui().message("You could fit a Micro Sprayer in the head of the toy and store the solution in the handle. "
								+ "You'd also need $"+Attachment.ExcaliburScience.getPrice()+" for additional materials.");
						if(player.money >= Attachment.ExcaliburScience.getPrice()){
							Global.gui().choose(this,"Add "+Attachment.ExcaliburScience.getFullName(player));
						}
					}
					Global.gui().message("<br>");
				}
			}else{
				Global.gui().message("You eventually locate an online retailer selling the Sexcalibur sex toy. Unfortunately, it's as "
						+ "expensive as Aesop warned you. There aren't a lot of product reviews available.");
				Global.gui().choose(this, "Order Sexcalibur: $1000");
			}
			Global.gui().choose(this, "Back");
		}
		else if(choice=="Golden Cock"){
			int checks = 0;
			if(player.has(Trait.goldcock)){
				Global.gui().message("You have completely mastered the Way of the Golden Cock and achieved sexual enlightenment. There "
						+ "is nothing more to learn from this guide. You feel like you're on the verge of something incredible. You just "
						+ "need to perservere.<p>");
			}else if(player.has(Trait.silvercock)){
				Global.gui().message("You've almost completed the Way. This last group of goals is the hardest as expected. ");
				if(Global.checkFlag(Flag.Mayabeaten)){
					Global.gui().message("Defeat an overpowering opponent (Complete)");
					checks++;
				}else{
					Global.gui().message("Defeat an overpowering opponent");
				}
				if(Global.getValue(Flag.ThreesomeCnt)>=30){
					Global.gui().message("Have 30 threesomes: 30/30 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Have 30 threesomes: %d/30",Math.round(Global.getValue(Flag.ThreesomeCnt))));
				}
				int lovers = countLovers();
				if(lovers >= 4){
					Global.gui().message("Have four girls in love with you: 4/4 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Have four girls in love with you: %d/4",lovers));
				}
				if(player.getStamina().max()>=150){
					Global.gui().message("Raise max Stamina to 150: 150/150 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Raise max Stamina to 150: %d/150",player.getStamina().max()));
				}
				if(player.getArousal().max()>=250){
					Global.gui().message("Raise max Arousal to 250: 250/250 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Raise max Arousal to 250: %d/250",player.getStamina().max()));
				}
				if(checks >= 5){
					Global.gui().choose(this,"Meditate","You've completed the requirements. It's time to look within yourself to see what you've learned");					
				}
				
			}else if(player.has(Trait.bronzecock)){
				Global.gui().message("This new list of goals is primarily focused on your performance in the matches, confirming this "
						+ "guide is targeted at participants in the Games. You're less skeptical of the value of this guide now, but some "
						+ "of these goals are clearly going to take some time.<p>");
				if(Global.getValue(Flag.MatchWins)>=25){
					Global.gui().message("Finish 25 matches in first place: 25/25 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Finish 25 matches in first place: %d/25",Math.round(Global.getValue(Flag.MatchWins))));
				}
				if(Global.getValue(Flag.HandicapMatches)>=20){
					Global.gui().message("Accept 20 match handicaps: 20/20 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Accept 20 match handicaps: %d/20",Math.round(Global.getValue(Flag.HandicapMatches))));
				}
				if(Global.getValue(Flag.IntercourseWins)>=50){
					Global.gui().message("Defeat 50 opponents using sexual penetration: 50/50 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Defeat 50 opponents using sexual penetration: %d/50",Math.round(Global.getValue(Flag.IntercourseWins))));
				}
				if(player.getPure(Attribute.Speed)>=12){
					Global.gui().message("Raise Speed to 12: 12/12 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Raise Speed to 12: %d/12",player.getPure(Attribute.Speed)));
				}
				int max = MaxAdvanced();
				if(max >= 30){
					Global.gui().message("Have 30 points in an advanced Attribute: 30/30 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Have 30 points in an advanced Attribute: %d/30",max));
				}
				if(checks >= 5){
					Global.gui().choose(this,"Meditate","You've completed the requirements. It's time to look within yourself to see what you've learned");					
				}
				
				
			}else{
				Global.gui().message("So far this guide has been a bit short on guidance. There is mostly just a list of goals and "
						+ "a checklist to keep track of your progress. After completing the list, you're suppose to meditate on everything "
						+ "you've learned. It's implied that more goals will become available after finishing these.<p>"
						+ "So far you're having trouble imagining you'll get your money's worth out of this app.<p>");
				int lovers = countLovers();
				if(lovers >= 2){
					Global.gui().message("Have two girls in love with you: 2/2 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Have two girls in love with you: %d/2",lovers));
				}
				if(Global.getValue(Flag.ThreesomeCnt)>0){
					Global.gui().message("Have a threesome (Complete)");
					checks++;
				}else{
					Global.gui().message("Have a threesome");
				}
				
				if(player.getStamina().max()>=80){
					Global.gui().message("Raise max Stamina to 80: 80/80 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Raise max Stamina to 80: %d/80",player.getStamina().max()));
				}
				if(player.getArousal().max()>=125){
					Global.gui().message("Raise max Arousal to 125: 125/125 (Complete)");
					checks++;
				}else{
					Global.gui().message(String.format("Raise max Arousal to 125: %d/125",player.getArousal().max()));
				}
				if(checks >= 4){
					Global.gui().choose(this,"Meditate","You've completed the requirements. It's time to look within yourself to see what you've learned");					
				}
			}
			Global.gui().choose(this, "Back");
		}
		else if(choice=="Meditate"){
			if(player.has(Trait.silvercock)){
				Global.gui().message("You've done it. You've come so far and overcome every obstacle in your path. It's time to complete the Way of "
						+ "the Golden Cock. You've loved many women, sometimes at the same time. You've refined your body to be the ideal sex-fighter. "
						+ "As you center yourself, you can feel spiritual energy flow into your abdomen. You realize your cock has become the perfect "
						+ "vessel for spiritual power. As the energy grows, light begins to leak out of your pants. When you undress, you discover that "
						+ "your cock is radiating a golden light. The is the Golden Cock that the way is named for.");
				player.add(Trait.goldcock);
			}else if(player.has(Trait.bronzecock)){
				Global.gui().message("You sit and reflect on everything that has brought you this far. You've gained quite a lot of experience in the "
						+ "Games. You've fucked your way to victory time and time "
						+ "again and become strong. At this point, your penis may be your single most exercised organ. After overcoming so much pleasure, "
						+ "you realize you've become more resistant to it. ");
				player.add(Trait.silvercock);
			}else{
				Global.gui().message("You're suppose to reflect on what you've learned so far. So far you've mostly just learned how to have a lot of sex. "
						+ "You suppose that is basically the point of the guide though. You think back to all the sex you've had recently, which doesn't "
						+ "seem to give you anything except a boner. You relax to calm down your arousal, but you discover you can regain your erection "
						+ "with minimal effort. If you can get hard at will, you'll never have to worry about not being aroused enough to initiate sex.");
				player.add(Trait.bronzecock);
			}
			Global.gui().choose(this,"Leave");
		}
		else if(choice=="Change Clothes"){
			Global.gui().changeClothes(player,this);
		}
		else if(choice=="Plan Skills"){
			Global.gui().message("Upcoming Skills:");
			for(Attribute att: player.att.keySet()){
				Global.gui().message(Global.getUpcomingSkills(att, player.getPure(att)));
			}
			Global.gui().choose(this, "Back");
		}

	}

	private int countLovers(){
		int i = 0;
		for(Character npc: Roster.getExisting()){
			if(!npc.human()){
				if(npc.getAffection(player)>=25){
					i++;
				}
			}
		}
		return i;
	}
	private int MaxAdvanced(){
		int sum = 0;
		for(Attribute att: Attribute.values()){
			switch(att){
			case Arcane:
			case Animism:
			case Ki:
			case Dark:
			case Fetish:
			case Submissive:
			case Ninjutsu:
			case Science:
				sum += player.getPure(att);
			}
		}
		return sum;
	}
	@Override
	public void shop(Character npc, int budget) {
		// TODO Auto-generated method stub

	}

}
