package daytime;

import characters.*;
import characters.Character;
import global.Flag;
import global.Global;
import global.Roster;

public class KatTime extends Activity {
	private Character kat;
	private Dummy sprite;

	public KatTime(Character player) {
		super("Kat", player);
		kat = Roster.get(ID.KAT);
		sprite = new Dummy("Kat",1,true);
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.Kat);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		sprite.dress();
		sprite.setCostumeLevel(1);
		sprite.setBlush(0);
		sprite.setMood(Emotion.confident);
		if(choice == "Start"){
			if(Roster.getAffection(ID.PLAYER,ID.KAT)>0){
				Global.gui().message("You send Kat a text to see if she's free. Since exchanging numbers with her, you've discovered that she's much more outgoing " +
						"when texting than she is in person. The two of you have chatted quite a bit, you just hope she'll eventually get more used to talking with you in " +
						"person.<p>You quickly receive a reply from Kat. 'i'm free right now. :) do you want to meet up?' You text her back, asking if there's a place you " +
						"can meet without her friends coming after you. 'i'm not with Mel and Emma right now. you can come here' About ten seconds later, she sends you a followup. " +
						"'please don't hate them. they're my best friends and they mean well. they just think i'm still an innocent virgin who needs to be protected.' To be " +
						"fair, she does inspire that sort of protective attitude, even from her opponents. For Kat's sake, you'll do your best to get along with them, but " +
						"they may not be as agreeable, especially if they find out you're having sex with their protectee.<p>"
						+ "On your way to Kat's room, you get another text. " +
						"'i think i'm too excited waiting for you to get here. what are you planning?'");
				promptScenes();
			}
			else if(kat.getAttraction(player)<10){
				Global.gui().message("You decide to look for Kat and see if she's interested in spending some time together. You don't have any way to contact her directly, " +
						"but she apparently spends a lot of time in the campus gardens. That's probably your best hope for running into her.<p>"
						+ "You eventually spot Kat walking " +
						"through the gardens, but you almost don't recognize her. Instead of the light, casual clothes she usually wears during a match, she's currently dressed " +
						"in an excessively baggy outfit. She's still pretty cute, but those clothes make her look like she's trying to hide her small frame. She's probably trying " +
						"to hide something else, though. At night, she doesn't have to worry about people seeing her cat ears and tail, but in her day-to-day life, it's got to be " +
						"a real inconvenience. To her credit, you don't see any obvious bulge in her pants where her tail should be. It would be interesting to see how she " +
						"maneuvers her tail to keep it from showing. It also sounds like a good excuse to get her pants off. As for her ears, she has them hidden under a cloth " +
						"hat... that has cat ears on it. She's wearing a cat ear hat over her actual cat ears. Does that qualify as hiding in plain sight? In her hand, she's " +
						"carrying a paperback book that she's either just been reading or is looking for a place to read.<p>"
						+ "You call out to Kat and for a moment, she looks like " +
						"a small, startled animal. She looks noticeably relieved when she sees you. That's probably a good sign. Before the two of you have a chance to talk, someone " +
						"else calls out to Kat. Two girls insert themselves between you and Kat. Were they with her? You didn't even notice them. One of the girls puts an arm around " +
						"Kat's shoulder and leads her away. <i>\"Sorry, we're in a bit of a hurry.\"</i> Kat looks like she wants to say something, but can't get the words out.<p>" +
						"After they " +
						"leave, the other girl gives you a smile with no goodwill. <i>\"I know. That girl's super cute, isn't she? Unfortunately, you'll need to find another girl to hit " +
						"on. She doesn't have any experience with men and isn't very good at asserting herself, so we can't let anyone take advantage of her.\"</i><p>"
						+ "Ugh. This is inconvenient. " +
						"You can't very well explain that you're already intimately acquainted with Kat. You'll need to find another opportunity to approach her.");
				Roster.gainAttraction(ID.PLAYER,ID.KAT,2);
			}
			else{
				sprite.setBlush(3);
				sprite.setBotInner(false);
				sprite.setBotOuter(false);
				Global.gui().loadPortrait(player, sprite);
				Global.gui().message("You head out to the campus gardens, hoping to find Kat so you can spend some time together. You aren't searching long before you find her reading " +
						"a book in the shade of a tree. She seems pretty absorbed in the book, so you're hesitant to disturb her. Instead of calling out to her, you just sit next to her " +
						"quietly. You don't recognize the book she's reading, but judging by the cover, it appears to be an urban fantasy romance novel. It must pretty engaging, because " +
						"Kat still hasn't noticed that you've been sitting with her for several minutes.<p>When Kat finally perceives your presence, she lets out a startled yelp and jumps " +
						"to her feet. You weren't expecting her to be that surprised. You give her a relaxed smile and a greeting. Before Kat can reply, you hear an angry voice behind you. " +
						"<i>\"What the hell are you doing to my best friend?!\"</i> A girl with curly red hair glares at you and hugs Kat protectively. Another girl, a brunette, looks calmer, but " +
						"also stands between you and Kat. This is bad. You quickly explain that you weren't trying to scare Kat, you just wanted to talk to her. <p><i>\"Our Kat is pretty delicate. " +
						"Maybe you should learn how to approach a girl without scaring her before you try to pick her up.\"</i> The brunette speaks in a reasonable tone, but there's a definite " +
						"edge to her voice. The redhead snorts and starts to lead Kat away. <p>"
						+ "<i>\"I don't want this creep talking to Kat at all.\"</i> Kat tugs on the girl's sleeve to stop her and " +
						"whispers something in her ear. The girl looks back at you shocked. <i>\"This guy? Are you kidding me?\"</i> The brunette joins the two of them and they enter a brief huddle. " +
						"You stand there awkwardly, unable to hear their conversation. There are more than a few glances in your direction, and Kat's face is gradually turning red. <p>Eventually, " +
						"Kat leaves the huddle to stand behind you, as if hiding from her friends. The calmer of the two girls gives you an awkward smile. <i>\"We'll give you two some space.\"</i> " +
						"She has to practically drag away the other girl, who is glaring daggers at you. " +
						"Kat doesn't make eye contact, but shyly touches your hand. <i>\"Will you come to my room for a little while?\"</i> she softly asks.<p>"
						+ "She's quiet all the way to her room " +
						"and that doesn't change after you arrive. You sit next to each other in silence on her bed. She's leaning against you slightly, but is looking away, so you can't see " +
						"her expression. You'll probably have to be the one to initiate conversation, but you aren't sure how to start. To your surprise Kat is the one to break the silence. " +
						"She takes your hand and gently moves it to her groin. <i>\"C-can you touch me? Just a little bit....\"</i> Her face is beet red and she can't look you in the eye, but that " +
						"was way more forward than you were expecting. You don't mind, of course.<p>You pull down Kat's pants and slip your hand into her underwear. Under your skilled touch, " +
						"her nethers quickly start to get wet. You gently lay her down on the bed as she whimpers in pleasure. Her tail twitches uncontrollably the more you finger her. You " +
						"slide her panties down to get better access to her girl parts and she ends up kicking them off completely.<p>Just as you're about to finish Kat off, she grabs your " +
						"wrist to stop you. <i>\"Wait! If you keep that up, I'm gonnya cum and we'll be right back where we started.\"</i> She isn't interesting in climaxing, she just wants you to " +
						"leave her horny and unsatisfied? She squirms noticeably as she sits up on the bed. <i>\"It's really frustrating to stop nyow, but I need to be patient. I'm counting on " +
						"you to reward me when we're done.\"</i> She settles into a comfortable seated position on the bed, apparently not bothered that her naked lower half is visible. <i>\"We've " +
						"fought together a bunch, but since you're myaking an effort to get to know me, I wanted to explain how my animal spirit works. The Girl was too flustered about being " +
						"alone with a boy to talk properly, so I needed your help to bring out the Cat to do the talking.\"</i><p>" +
						"By arousing Kat, you brought out her animal side. So that's who " +
						"you're talking to now? <i>\"The urge to mate is a very primal thing. The more I feel it, the stronger the cat spirit gets, which improves my instincts and my reflexes. It's " +
						"nyat like a Jekyll and Hyde thing though. The Girl and the Cat have the same memories, same intelligence, same personality, and same interests. I'm still Kat, just " +
						"imyagine you've turned up the catliness level on a mixer.\"</i> She said they have the same personality, but she doesn't sound at all like Kat. Her manner of speech is " +
						"completely different and she obviously talks a lot more. <i>\"Nya~. The Cat is a wild animal, so it doesn't add much in the way of myannerisms or speech style. The " +
						"only real difference is that I'm free from a lot of human social inhibitions. I don't get shy talking to others and I don't feel shame, see?\"</i> She spreads her legs " +
						"to flash you a good look at her soaked privates. <i>\"This is how the Girl's inner monologue sounds, or to put it another way, this is how Kat would talk if she wasn't " +
						"shy. Oh, I also do a lot more meowing. That's totally the Cat's fault.\"</i><p>Ok, you get the general idea. You just aren't sure what to call her when she's like this. " +
						"<i>\"Nya! I told you, I'm still Kat. I have all the same thoughts and all the same desires I do normally.\"</i> She seductively pulls you close to her and whispers in your " +
						"ear. <i>\"I want you to fuck me, because the Girl wants you to fuck her. I'm just nyot embarrassed to say it. Speaking of which....\"</i> She lies back on the bed and spreads " +
						"her legs open for you. <i>\"I've been patient for a while. I think I deserve a reward.\"</i> You have some more questions, but you can't simply turn down Kat when she's asking " +
						"in such a lewd pose. You kneel between her legs and start to eat her out. She mews in pleasure and writhes on the bed. She's already very turned on, so don't you need " +
						"to lick her clit very long to make her orgasm. She arches her back and lets out a breathless moan as she hits her peak.<p>You sit next to Kat and gently stroke her " +
						"head while you wait for her to recover. After she catches her breath, she blushes furiously and scrambles to retrieve her discarded panties. You enjoy the view of her " +
						"cute butt until she finishes putting her underwear back on. She sits down on the bed next to you and gives you a light kiss on the cheek. <i>\"Thanks.\"</i> She smiles shyly " +
						"and looks away. <i>\"That felt really good.\"</i> She holds her phone out to you. <i>\"Can we exchange numbers? You know... so we can train together?\"</i> You add yourself to her " +
						"contacts and return the phone. She sends a short text to you. She seems a lot less awkward now, but she still acts so innocent. It's still hard to believe this is the " +
						"same girl that was asking you to fuck her just minutes ago. You look at the text she sent you: 'i meant everything i said &#60;3.'");
				Roster.gainAffection(ID.PLAYER,ID.KAT,1);
				Global.gui().message("<b>You gained affection with Kat.</b>");

			}		
			Global.gui().choose(this,"Leave");
		}
		else if(choice == "Sex"){
			sprite.setBlush(3);
			sprite.undress();
			sprite.setMood(Emotion.horny);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("Kat sits on her bed and looks at you hesitantly, with red cheeks. <i>\"Are we going to... you know?\"</i> Despite her shy appearance, there's definitely " +
					"an eagerness to her voice. You both want the same thing. You give her a quick kiss on the lips and help her remove her shirt. She shyly crosses her arms over her bra " +
					"and smiles weakly. <i>\"It's embarrassing if I'm the only one who is naked. Take off your shirt too.\"</i> You obligingly strip of your own top and she helps you remove " +
					"her bra. Her breasts are quite big and soft looking, considering her petite build. If she didn't cover up her body with baggy clothing during the day, her friends would " +
					"surely need to beat guys off of her left and right. Kat turns even redder when she catches you staring and covers her breasts. <i>\"D-don't stare at my boobs so much. It's " +
					"your turn to undress.\"</i> Of course, she deserves a little eye candy too. You kick off your pants, leaving only your boxers. Kat hesitantly takes her hands off her chest " +
					"so she can remove her pants. <p>" +
					"You're both down to your underwear and Kat stares at your boxers in anticipation. She's obviously ready for some passionate sex, but you " +
					"can't resist teasing her some more. You sit down on the bed next to her and push her gently onto her back. She looks confused about this sudden change in plan. You explain " +
					"that you came here for sexual training, not simply to have sex. Therefore there's no reason to take your boxers off right now. In one smooth motion, you pull down her panties " +
					"and expose her wet flower. She yelps in surprise and embarrassment and covers herself with her hands. You're going to start by training her self control. Kat gets taken over " +
					"by her animal spirit too easily when she's aroused. You're going to help her learn to contain it by fingering her while she tries to maintain her control. <p>" +
					"She looks a bit " +
					"dubious, but accepts your idea. She closes her eyes, takes a deep breath, and moves her hands away from her groin. You lightly move your finger up and down her slit, eliciting a soft whimper. " +
					"If you're going to do this properly, you need better access to her girl parts. She flushes in shame, but obediently spreads her legs at your prompting. You reposition yourself " +
					"to sit between her spread legs and start to slowly rub her lower lips. Kat squirms a bit, but focuses on controlling her breathing. You intensify your fingerwork a bit, but " +
					"she manages to maintain her composure, showing no sign of catliness. You notice that her hands are balled into fists, gripping the bedsheets tightly. Her earnestness in this " +
					"silly exercise you made up is charming. You lean down and lightly lick her inner thigh. She jerks as if shocked and lets out a moan. <i>\"Tongue is unfair! I can barely handle " +
					"the fingers!\"</i> She really is taking this seriously. You were planning to eat her out as a reward, but you decide instead to continue the exercise until she wants to stop. " +
					"<p>");
			Global.gui().displayImage("premium/Kat Tease Fingering.jpg", "Art by AimlessArt");
			Global.gui().message("She's doing a very good job keeping her arousal in check. You've been fingering her long enough that your wrist is getting sore. You give her a brief respite while you move " +
					"to sit behind her. She's a bit startled when you pull her into your lap to lean against your chest. From this new position, you slip your hand between her legs and resume " +
					"fingering her. She lets out breathy moan and shivers in your arms. Within a handful of seconds, you feel her tail twitching against you leg and her moans have a clear mewing " +
					"quality. <p>" +
					"She was doing pretty well up until now. <i>\"I've been in the Games long enyough to endure being fingered if I focus, but being held by a guy... and the " +
					"warmth... and the breath on my neck! How is a girl suppose to handle that?\"</i> She squirms out of your arms and turns to face you.<p>"
					+ "<i>\"I know this 'training' was just an " +
					"excuse to tease me. I didn't complain because I liked what you were doing, but it's your turn nyow. Either get those boxers off, or they're gonnya be shredded.\"</i> You " +
					"quickly strip off your underwear. These are your good boxers, not like the cheap throwaway pairs that you wear during a match. Kat gives an approving purr as she looks over " +
					"your throbbing erection. She straddles your waist and lowers herself onto your member. She was talking about teasing you before, but apparently she's too impatient. She " +
					"rocks her hips, sliding her slick folds up and down your cock. You kiss her passionately and pull her body against yours. You're both extremely horny since you've been fooling " +
					"around for quite awhile. Kat moves her hips energetically as you thrust into her from below. Her tail whips back and forth with excitement and she moans softly into " +
					"your mouth. She has to break the kiss to breathe and buries her face in your neck.<p>");
			Global.gui().displayImage("premium/Kat Tease Sex.jpg", "Art by AimlessArt");
			Global.gui().message("<i>\"It feels really good! Nya! I'm gonnya cum!\"</i> Her timing is pretty good; you're " +
					"about to cum too. She clings to you and digs her nails into your back as she shudders in orgasm. Her pussy clenches your cock and you erupt inside her. <p>" +
					"She slumps completely " +
					"limp into your arms, eyes closed. You hold her and gently stroke her head. You pet her for a little while before she opens her eyes with a sleepy smile. <i>\"You're still inside " +
					"me.... It's a nice feeling.\"</i> You lay her gently onto the bed and she hugs you tightly to make you aren't going anywhere. She quickly falls asleep, cuddling up against you. " +
					"You pull a blanket over your naked bodies and close your eyes. You were supposed to be training, but a quick nap suddenly seems very inviting.");
			Global.gui().choose(this,"Leave");
			Daytime.train(player,kat,Attribute.Seduction);
			Roster.gainAffection(ID.PLAYER,ID.KAT,1);
			Global.gui().message("<b>You gained affection with Kat.</b>");

		}
		else if(choice == "Affectionate Sex"){
			sprite.setBlush(3);
			sprite.undress();
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("Kat sits down next to you on the bed, stealing furtive glances at you.<p>"
					+ "<i>\"S-So...! Um...\"</i><p>"
					+ "Her cheeks are pink, and she shifts uncomfortably. A heavy silence hangs in the air. She seems to be "
					+ "waiting for you to do something. You suppose she wants you to touch her again and call forth the Cat "
					+ "so she has a reason to pounce on you. The Girl is too shy to be so forward. You find it very cute, "
					+ "and you're curious to know what will happen if you're able to keep the Girl around a little while longer.<p>"
					+ "You reach up and brush Kat's short hair off her cheek and gently grip her jaw, turning her head to face you. "
					+ "She looks up at you in surprise, her lips parted. You take the opportunity to lean in and kiss her. She makes "
					+ "a small noise against your mouth, her hand coming up to touch your arm. After a moment, her eyes slide shut "
					+ "and she tilts her head, and then she is kissing you back.<p>"
					+ "You're a little surprised by the eagerness in her soft kisses; she wraps her arms around your neck and you "
					+ "pull her in close, feeling her small, warm body underneath her baggy clothes. You let your free hand trail "
					+ "down her front, massaging her breasts. You're surprised at how large they feel, easily filling your hands. "
					+ "You slip your hand beneath her shirt to feel them better and you delight at the sensation.<p>"
					+ "Kat sighs softly as you rub her stiff nipples between your fingers. You stop kissing her and reach down "
					+ "to pull her baggy shirt up. She lifts her arms so you can slide it over her head. As you toss the shirt aside, "
					+ "she smooths her hair down and shyly covers her chest with her arms, looking at you dubiously. The Girl is a "
					+ "lot less confident than her kitty counterpart. You smile and tell her how beautiful she looks.<p>"
					+ "<i>\"You really think so?\"</i> she says, looking pleased. Her blush is more apparent now. You reposition "
					+ "yourself on the bed so you're facing her and you lean into her again, kissing her deeper, pushing her back "
					+ "onto the bed. She follows your lead, resting back, letting you kiss down her neck and over her collarbone to "
					+ "her breasts to lick her nipples. Her chest rises and falls as her breath quickens at the feeling of your tongue "
					+ "on her skin. You move further down, kissing over her flat stomach. You take the hem of her baggy pants in your "
					+ "fingers and begin to slide them down, careful not to remove her panties as well. You know if you get her too "
					+ "excited, her feline instincts will take over.<p>"
					+ "Kat bends her legs and helps you pull her pants off, sitting up and watching you as you kneel in front of her "
					+ "on the bed. Her tail twitches in anticipation and she looks up at you with lusty eyes as she reaches to pull "
					+ "your shirt up. You gladly take it off. She runs her hands over your chest as you smile down at her. She leans "
					+ "up to kiss you as her hands toy with your fly, and you feel her breath against your lips.<p>"
					+ "<i>\"I want you...\"</i> she whispers. She's undoing your pants and pulling them down, and you feel her hands "
					+ "massaging the bulge that's already poking out from your boxers. She deftly slides her hands inside and begins "
					+ "stroking you as you help tug your boxers down. Her tail sways gently from side to side as she focuses intently "
					+ "on your stiff erection, feeling its size in her small hands, and she looks up at you with desire written across "
					+ "her face. You pull her close and kiss her again, her body pressed up against yours as your cock rubs against her "
					+ "belly. The kiss she gives you back is fiery and hungry; she wraps herself around you, her hips slowly moving up "
					+ "and down against you. You can feel the heat radiating from between her legs.<p>"
					+ "The Cat will take over if you let her continue this way. You slowly take her by the shoulders and lay her back "
					+ "down on the bed. She moves to protest, pouting slightly, but she relents as you resume kissing her stomach.<p>"
					+ "You look up and meet Kat's gaze as she peers down at you. You can see in her expression that the feline part of "
					+ "her brain is a little resentful of your slow place, but something is blossoming beneath the surface; a tenderness "
					+ "that appreciates your gentleness and wants you to take it slow as well. A misty sentiment that is overwhelmingly "
					+ "human swims behind her light blue eyes, and she smiles suddenly.<p>"
					+ "<i>\"I'm so happy you're here,\"</i> she murmurs affectionately. You kiss her hip in agreement and slide her "
					+ "panties down.<p>"
					+ "This time, when you lower yourself over her, your naked bodies press together and entwine, her legs sliding around "
					+ "yours. She pulls you close and kisses you fervently as you position yourself to enter her. The swollen tip of your "
					+ "cock massages the opening as she rubs her hips against yours. You break the kiss, looking deep into her eyes, and "
					+ "for a moment, you swear you can hear her heart beating. Then, you slowly move your hips forward and push yourself "
					+ "into her.<p>"
					+ "Kat takes in a breath as you ease yourself inside her slick, wet pussy. Her nails grip your back and she clings to "
					+ "you, trembling slightly, but as you begin to move, a moan escapes her.<p>"
					+ "<i>\"Oh! Mm... I-I...\"</i> she stammers breathlessly, at a loss for words. The Girl clearly doesn't know how to handle "
					+ "a situation like this. The thought turns you on, and you find yourself thrusting a little faster. Her head falls to "
					+ "the side, her eyes rolling up in delirious pleasure. It seems like a meow might be about to escape her lips, so you "
					+ "lean down and kiss her passionately, hoping to stifle it and let the Girl enjoy this experience.<p>"
					+ "It's getting harder to tell which Kat you're making love to, though. She's biting at your lower lip. You can feel "
					+ "her nails scratching at your back. Her pussy is contracting and squeezing your cock in an instinctive, primal way. "
					+ "She's getting close to the edge, and you're not going to last much longer either. Your pace quickens; you grip one "
					+ "of her bouncing breasts as you thrust in deep. She cries out in pleasure, her body shuddering. You groan loudly "
					+ "as your body tenses up, your cock twitching urgently as you spill your load inside her.<p>"
					+ "Kat smiles up at you with trusting adoration, stroking your sweaty brow as you catch your breath, slowly rolling "
					+ "onto the bed next to her. She cuddles up close to you and you wrap your arms around her.<p>"
					+ "<i>\"That was amazing...\"</i> she purrs contentedly. <i>\"But I'm feeling-\"</i> her words are cut off by a yawn. "
					+ "Her eyes are half closed, and she blinks up at you. <i>\"Kind of.. Sleepy...\"</i> Her head nods to the side and rests "
					+ "against your chest. You hug her tenderly and pull the blanket over you both.");


			Global.gui().choose(this,"Leave");
			Daytime.train(player,kat,Attribute.Seduction);
			Roster.gainAffection(ID.PLAYER,ID.KAT,1);
			if(!player.has(Trait.affectionate)){
				Global.gui().message("<p><b>Your Affectionate charms make you more lovable during simultaneous orgasms.</b>");
				player.add(Trait.affectionate);
				kat.add(Trait.affectionate);
			}
			Global.gui().message("<b>You gained affection with Kat.</b>");
		}
		else if(choice == "Games"){
			sprite.setBlush(3);
			sprite.undress();
			sprite.setMood(Emotion.desperate);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("Despite your shared intimacy, Kat still has trouble speaking normally when you're alone together. You've turned to games as, not just a form of " +
					"strategy training, but also a means of getting her to relax. As you get deep into the game, it seems to be working. It probably helps that she seems to have a " +
					"knack for this game. She's practically bouncing in her seat and shadowboxing each time she crashes a gem. <p>" +
					"<i>\"You have no chance against my kitty cat attacks.\"</i> " +
					"You probably shouldn't point out that her character is suppose to have a fox motif, rather than a cat. Her cute antics are helping you overlook how badly you're " +
					"losing. You've had to focus most of your efforts on defense, so you've been falling behind on buying good chips. On Kat's turn she plays 'Speed of the Fox' (which " +
					"she continues to call 'Speed of the Cat') and several 'Combines', creating a pair of level 4 gems out of the level 2 gems she's been holding onto. That puts an " +
					"end to your defensive strategy, level 4 gems can't be countered.<p>" +
					"Kat sends the entire eight stack of gems crashing towards you and shoots her fist into the air in an enthusiastic pose. <i>\"Super Cat Punch!\"</i> Oh God, she's " +
					"just too damn cute! You can't resist tackling Kat and hugging her tightly, the game completely forgotten. She doesn't protest or try to resist, but you do notice " +
					"a distinctively dissatisfied look on her face. Does she not like being held like this? <i>\"I don't mind this sort of thing. It happens to me a lot. Aisha glomps me like this " +
					"almost every time I see her. It feels especially nice being held by you, but... I was winning....\"</i> Oh right, the game. The odds of you surviving another turn " +
					"after her Super Cat Punch is pretty low. Even if you did recover, she still has a significant advantage. It seems fair to concede defeat now and skip to some more " +
					"intimate fun. Kat pouts, clearly displeased at your lack of motivation. If she has her heart set on finishing the game instead of sexy times, you'll try to be " +
					"patient. She blushes and squirms a bit in your arms. <i>\"I'm not turning down sexy times. Just remember that I won, so you should obey me today. You're not " +
					"allowed to tease me.\"</i> You don't really tease her that much, do you? Well, fair enough. She calls the shots this time. Kat looks you over and smiles. <i>\"First, take off " +
					"your clothes. Today, you get to be naked and embarrassed instead of me.\"</i> You obediently strip naked, but you aren't actually that embarrassed to be naked in front " +
					"of Kat. She cuddles up against you and starts to fondle your dick with both hands. She's pretty good at this, probably due to her experience in the Games. She strokes " +
					"and pleasures you until your precum starts to leak out onto her fingers.<p>" +
					"<i>\"That's all for now,\"</i> Kat says, as she abruptly takes her hands off your penis. <i>\"The rest will be a reward if you service me properly.\"</i> You groan in " +
					"frustration at Kat's uncharacteristic cocktease. Well, you did agree to listen to her for now. You'll surely earn more than a handjob with your expert cunnilingus skills. " +
					"You reach down to remove Kat's pants, but she suddenly covers your eyes with her hands. <p>" +
					"<i>\"I know it's silly after all this time, but I still get embarrassed when someone stares at my privates. No peeking, OK?\"</i> This has become slightly inconvenient. " +
					"You'll have to rely on your sense of touch. You clumsily remove her pants and underwear and quickly locate her vulva with your tongue. Kat shivers as you start to eat her " +
					"out and seems to be having trouble keeping her hands over your eyes. <i>\"Keep-Nya!~ Keep your eyes closed. You're doiNya!~ just fine without looking.\"</i><p>" +
					"You're glad she's enjoying your efforts, but you do wish you could look at her. It's not just so you can see what you're doing, but also to watch her reactions. She said " +
					"'no peeking', so you'll just have to harden your resolve and resist temptation. At least you can enjoy her moans, which are gradually becoming more and more feline. <p><i>\"You're " +
					"still nyat peeking?\"</i> You assure her that your eyes are still tightly shut. To your surprise, she lets out a low sound of annoyance and pushes your head away. <i>\"Why are you " +
					"being so obedient?! You're gonnya make me cum too fast!\"</i> You open your eyes (she's obviously too cat-like to care about being seen) and look at her in confusion.<p>" +
					"<i>\"Can't you take a hint? I obviously only told you nyat to tease me so you'd want to do it more. I even teased you and gave you that dumb 'no peeking' rule to put the " +
					"idea in your head. You shouldn't just blindly do whatever I say.\"</i> She's pouting, but it just looks ridiculous with no pants on. <p>" +
					"If she's giving you the green light to tease her, you're definitely getting the urge to do so right now. You mercilessly tickle her naked inner thighs, paralyzing her with " +
					"squeals of laughter. No matter how cute she is, you're still going to punish her for being so unreasonable. How can she expect you to pick up on reverse psychology when she's " +
					"giving you orders? She can't demand that you obey her as a prize for winning and then demand the opposite of what she wants. You stop tickling her long enough for her to " +
					"answer you. <i>\"I couldn't just tell you that I like it when you tease and deny me. Who would freely admit that? I can only tell you now because I don't have any shame at " +
					"the moment. I'm going to be totally mortified when I turn back to normal.\"</i><p>" +
					"If she's really a masochist, she probably will enjoy being embarrassed. Well, if she wants you to deny her an orgasm, that's easy enough. She'll just have to wait until you " +
					"leave to handle her arousal. <i>\"Nya?! That's going too far! I just want you to play with me a bit, not leave before we're finished!\"</i> She looks pretty desperate. You're " +
					"still horny too, so maybe you'll reconsider her punishment if she begs convincingly enough. Kat shouldn't have any shame right now, but she still flushes at  your demand. " +
					"<i>\"Please! You can do whatever you want to me, just help me cum!\"</i><p>" +
					"You make her get on her hands and knees while you move behind her. You pull her tail out of the way, revealing her soaked pussy. Just as you thought, she's even more drenched " +
					"then when you were eating her out. You lightly stroke her lower lips, making her moan and shiver. When you slide a finger into her, her arms give out and her upper body drops to " +
					"the floor. <i>\"NYA! Nyat just your fingers! Aren't you horny too? Do it for real!\"</i><p>" +
					"You seem to recall her begging you to make her cum, doing whatever you wanted to her. Now she's saying your fingers aren't good enough for her? She should be more clear about " +
					"what she wants. She squirms in embarrassment and looks hesitant to speak. She's in full cat mode, so she can't actually be feeling shame. The blushing innocent bit must be part " +
					"of her libido. <i>\"I want... your... d-dick.... I want you to f-fuck me.\"</i> She can barely get the words out, but you feel a fresh flood of moisture from her entrance.<p>" +
					"She's earned a reward, and so has your unsatisfied cock. You line your rod up with her wet hole and thrust into her before she realizes what you're doing. She moans in pleasure " +
					"at the sensation of being completely filled. She's way beyond teasing, you might as well fuck her properly. You get a good grip on her waist and settle into a steady rhythm thrusting " +
					"in and out of her. Her tail thrashes wildly as she meows with complete abandon. You feel your ejaculation building quickly, but you're sure Kat's about to cum too, if she isn't " +
					"already mid-orgasm. A wave of pleasure washes over you as you shoot your load into her womb.<p>" +
					"As you recover from your orgasm, you notice that Kat is fast asleep, or possibly pretending to be asleep to avoid an embarrassing conversation. You're exhausted, but neither of " +
					"you are going to be comfortable napping on the floor. You pick up Kat's petite body and carry her over to the bed.");					
			Global.gui().choose(this,"Leave");
			Daytime.train(player,kat,Attribute.Cunning);
			Roster.gainAffection(ID.PLAYER,ID.KAT,1);
			Global.gui().message("<b>You gained affection with Kat.</b>");

		}
		else if(choice == "Sparring"){
			sprite.setBlush(3);
			sprite.undress();
			sprite.setMood(Emotion.desperate);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("You and Kat are able to find a private room with a wrestling mat so you can do some sparring without covering up her cat parts. You spend some time stretching and warming up together, " +
					"before you have to figure out how you're going to actually train. You're a lot taller and heavier than her, so an actual sparring match would be problematic. " +
					"Instead you offer to show her some simple wrestling takedowns. Kat looks visibly indignant, which is an expression you're not used to on her. <i>\"I have " +
					"been doing this for a year now. I should be teaching you techniques.\"</i><p>" +
					"Kat demonstrates a simple tackle to take your opponent off her feet. It's not " +
					"quite as smooth as when she's in her cat form, but it seems fairly effective. However, Kat is strong for her size and very quick. A big tackle is probably " +
					"not the most efficient technique against her. On the other hand.... Kat lets out a confused yelp as you grab her under the armpits and lift her off the floor. With her " +
					"small size and light frame, this is probably the simplest method to incapacitate her. She flails in humiliation at being treated like a kid. You forgot that " +
					"she's actually much stronger than she looks. Her thrashing manages to knock you off balance and you both fall to the mat, her landing heavily on top of you. " +
					"You get the wind knocked out of you, but you're more worried about Kat, who has her face buried in your chest and isn't moving. Is she ok? Did she land wrong? " +
					"As you're starting to panic, you see her tail wag lazily and hear a low meow.<p>" +
					"Wait, did she go full feline? You haven't done anything to arouse her. Can that " +
					"be triggered by physical trauma? Kat raises her head and looks at you with flushed cheeks. She certainly doesn't look traumatized. <i>\"Nya, sorry.... You " +
					"smell and feel really masculine. An innyocent girl like me can't help getting a little turned on from lying on top of you.\"</i> She purrs happily, hops " +
					"to her feet, and starts to strip. Why is she undressing? It's a good thing you've got a private room, otherwise you'd have a serious situation. <i>\"Nya! I'm " +
					"nyot stupid. Even if I accidentally get aroused in public, I wouldn't expose myself to just anyone. I may nyot get embarrassed, but I have enough common sense " +
					"to stay out of trouble.\"</i> She tosses her clothes aside and hops around energetically, making her breasts bounce attractively. <i>\"I thought of a fun way " +
					"to spar and get me back to nyormal at the same time. You just have to try to catch me and myake me cum, which won't be that easy with my cat instincts.\"</i> That " +
					"does sound more fun than just showing each other takedowns, though probably less practical. However, Kat is overestimating her ability to avoid you. She may " +
					"be quick, but this will be over soon. <p>" +
					"You take the initiative and lunge at Kat. She nimbly twists out of the way and jumps back to put some space between you. " +
					"That hop put her back to the wall, cutting off her escape vectors. You quickly corner her and rush in to finish her off. She kicks off from the wall and " +
					"manages to hop completely over your head. She's more agile than you realized, but it won't make a difference in the end. Kat dances away to the opposite corner, " +
					"but you take your time and approach her carefully. You slowly advance while she's still waiting for another rush. Soon she's backed away as far as she can and " +
					"can't stall any longer. She dashes past you, coming just slightly too close. You finally spring forward and manage to get an arm around her waist, pulling her " +
					"towards you as you both fall to the mat.");
			Global.gui().displayImage("premium/Kat Sparring.jpg", "Art by AimlessArt");
			Global.gui().message("Kat squirms in your arms, but you easily manage to mount her and finger her pussy. She's already wet and receptive to " +
					"your touch. This is why she couldn't get away. She wanted to be caught just as much as you wanted to catch her. She moans frantically, but doesn't really try " +
					"convincingly to escape. You quickly locate her clitoris and start rapidly rubbing it with your fingertips. Her back arches and she yowls in ecstasy as she orgasms. " +
					"You lightly caress her body as she's recovering from her climax. She giggles softly at the ticklish sensation and looks up at you with a flushed smile. <i>\"You're " +
					"good at this kind of sparring. Can I put my clothes on before we continue? This feels nice, but it's really embarrassing.\"</i>");					
			Global.gui().choose(this,"Leave");
			Daytime.train(player,kat,Attribute.Power);
			Roster.gainAffection(ID.PLAYER,ID.KAT,1);
			Global.gui().message("<b>You gained affection with Kat.</b>");

		}
		else if(choice.startsWith("Animal Spirit")){
			Global.flag(Flag.catspirit);
			Global.gui().loadPortrait(player, sprite);
			Global.gui().message("You know that Kat's power comes from an animal spirit inside her, but she never mentioned how that came to be. It seems like that might be a useful " +
					"ability, so you decide to ask her about it. <i>\"Hmm? My cat spirit? It's something I got from my friend, Aisha. Have you met her?\"</i> If she's referring to " +
					"Aisha Song, the girl who gives magic lessons and temporarily gave you a massive penis, then yes. You've most certainly 'met' her.<p>" +
					"<i>\"Aisha is really nice. When I was doing really badly in the Games, she offered to teach me magic. I was never able to learn it though. When she said she could " +
					"use the magic on me to give me catlike reflexes, I jumped at the opportunity. We did a ritual and I got these ears and this tail.\"</i> She proudly shows off her " +
					"normally hidden cat parts. <i>\"They're a little inconvenient, but they're really cute, aren't they?\"</i><p>" +
					"It's interesting that Aisha never brought up the possibility of giving you a spirit. Maybe you should ask her about it.<p>" +
					"<i>\"You totally should! I'm imagining you with puppy ears now, or maybe a tiger.\"</i> She blushes a bit at her own mental image. <i>\"I think Aisha may " +
					"feel bad about how the ritual turned out, even though I keep telling her I don't regret it. If she refuses to give you a spirit, I'll try to  help you talk her into " +
					"it.\"</i>");
			Global.gui().choose(this,"Leave");
		}
		else if(choice == "Leave"){
			Global.setCounter(Flag.KatDWV, 0);
			done(true);
		}
	}
	private void promptScenes(){
		Global.gui().choose(this,"Sex","Have some fun with her by helping her practice staying calm when teased");
		if(Roster.getAffection(ID.PLAYER,ID.KAT)>=12){
			Global.gui().choose(this,"Affectionate Sex","If you're careful, maybe you can make love to Kat without setting off her animal side");
		}
		Global.gui().choose(this,"Games","Kat beats you in a game and demands the right to be in control, but is that really what she wants?");
		Global.gui().choose(this,"Sparring","Kat's feral side comes out during practice. It shouldn't be too hard to get her back to normal...");
		if(Global.checkFlag(Flag.metAisha)&&!Global.checkFlag(Flag.catspirit)&&kat.getAffection(player)>=5){
			Global.gui().choose(this,"Animal Spirit","Ask Kat about the source of the animal spirit possessing her");
		}

	}
	@Override
	public void shop(Character npc, int budget) {
		Roster.gainAffection(ID.KAT,npc.id(),1);
	}
}
