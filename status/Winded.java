package status;

import java.util.HashSet;

import characters.Anatomy;
import combat.Combat;

import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;

public class Winded extends Status {

	public Winded(Character affected) {
		super("Winded",affected);
		flag(Stsflag.stunned);
		this.duration = 3;
		tooltip = "Unable to act, Stamina recovers quickly, immune to pain damage";
		this.affected = affected;
	}
	public Winded(Character affected, int duration) {
		this(affected);
		this.duration=duration;
	}
	@Override
	public String describe() {
		if(affected.human()){
			return "You need a moment to catch your breath";
		}
		else{
			return affected.name()+" is panting and trying to recover";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a==Attribute.Power||a==Attribute.Speed){
			return -2;
		}
		else{
			return 0;
		}
	}

	@Override
	public int regen() {
		return affected.getStamina().max()/4;
	}

	@Override
	public int damage(int x, Anatomy area) {
		return -x;
	}

	@Override
	public int weakened(int x) {
		return -x;
	}

	@Override
	public int tempted(int x) {
		return -x/2;
	}

	@Override
	public int evade() {
		return -99;
	}

	@Override
	public int counter() {
		return -99;
	}

	@Override
	public Status copy(Character target) {
		return new Winded(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous,15);
		affected.emote(Emotion.angry,10);
		decay(c);
	}
}
