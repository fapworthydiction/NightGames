package status;

import characters.*;
import characters.Character;
import combat.Combat;

public class Shield extends Status {

	public Shield(Character affected, int magnitude) {
		super("Shield",affected);
		flag(Stsflag.shielded);
		this.duration=4;
		this.magnitude = magnitude;
		stacking = true;
		tooltip = "Pain resistance";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=6;
		}
		this.affected = affected;
		
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int damage(int x, Anatomy area) {
		return -magnitude;
	}

	@Override
	public int counter() {
		return 2;
	}

	@Override
	public Status copy(Character target) {
		return new Shield(target, magnitude);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		decay(c);
	}

}
