package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Feral extends Status {

	public Feral(Character affected) {
		super("Feral",affected);
		flag(Stsflag.feral);
		tooltip = "Bonus to all basic Attributes based on Animism";
		this.affected = affected;
		decaying = false;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You feel your animal instincts start to take over, increasing your strength and speed.";
		}
		else{
			return "";
		}
	}

	@Override
	public int mod(Attribute a) {
		switch(a){
		case Power:
			return affected.get(Attribute.Animism)/5;
		case Cunning:
			return affected.get(Attribute.Animism)/4;
		case Seduction:
			return affected.get(Attribute.Animism)/4;
		case Animism:
			return 2;
		case Speed:
			return affected.get(Attribute.Animism)/6;
		}
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Feral(target);
	}
	@Override
	public void turn(Combat c) {
		if(!affected.has(Trait.feral)){
			if(affected.has(Trait.furaffinity)){
				if(affected.getArousal().percent()<25){
					affected.removeStatus(this,c);
				}
			}else{
				if(affected.getArousal().percent()<50){
					affected.removeStatus(this,c);
				}
			}
		}
	}

}
