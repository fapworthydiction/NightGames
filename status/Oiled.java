package status;

import java.util.HashSet;

import characters.Anatomy;
import combat.Combat;

import characters.Attribute;
import characters.Character;

public class Oiled extends Status {

	public Oiled(Character affected) {
		super("Oiled",affected);
		lingering = true;
		flag(Stsflag.oiled);
		tooltip = "-25% pleasure resistance, easier to escape pins";
		this.affected = affected;
		decaying = false;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your skin is slick with oil and kinda feels weird.";
		}
		else{
			return affected.name()+" is shiny with lubricant, making you more tempted to touch and rub her skin.";
		}
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return x/4;
	}

	@Override
	public Status copy(Character target) {
		return new Oiled(target);
	}

}
