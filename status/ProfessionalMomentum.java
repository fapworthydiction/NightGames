package status;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;

public class ProfessionalMomentum extends Status {
	private Anatomy part;
	
	public ProfessionalMomentum(Character affected, int percent) {
		super("Professional Momentum", affected);
		this.magnitude = percent;
		stacking=true;
		lingering=false;
		decaying=false;
		tooltip = "Bonus finger, mouth, and intercourse proficiency";
		
	}

	@Override
	public String describe() {
		return null;
	}
	public float proficiency(Anatomy using){
		if(using == Anatomy.fingers || using == Anatomy.mouth || using == Anatomy.genitals){
			return 1.0f+(magnitude/100f);
		}
		return 1.0f;
	}

	@Override
	public Status copy(Character target) {
		return new ProfessionalMomentum(affected,magnitude);
	}

}
