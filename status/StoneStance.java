package status;

import characters.*;
import characters.Character;
import combat.Combat;

public class StoneStance extends Status {
	
	public StoneStance(Character affected) {
		super("Stone Form", affected);
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=15;
		}
		else{
			this.duration=10;
		}
		flag(Stsflag.form);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your body still feels as impenetrable as rock.";
		}
		else{
			return affected.name()+"'s movements are slow, but rock-solid.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a==Attribute.Speed){
			return -affected.get(Attribute.Ki);
		}
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x, Anatomy area) {
		return -x*(Math.min((affected.get(Attribute.Ki)*3),90)/100);
	}

	@Override
	public Status copy(Character target) {
		return new StoneStance(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		decay(c);
	}

}
