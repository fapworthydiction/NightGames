package status;

import characters.*;
import characters.Character;
import combat.Combat;

import global.Global;

public class Enthralled extends Status {
	public Character master;

	public Enthralled(Character self,Character master) {
		super("Enthralled",self);
		this.master=master;
		flag(Stsflag.enthralled);
		tooltip = "Unable to act, must follow commands from caster";
		duration = Global.random(3) + (self.state == State.combat ? 1 : 3);
	}
	
	@Override
	public String describe() {
		if(affected.human())
		  return "You feel a constant pull on your mind, forcing you to obey"
				+ " your master's every command.";
		else{
			return affected.name()+" looks dazed and compliant, ready to follow your orders.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if (a == Attribute.Perception)
			return -5;
		return -2;
	}

	@Override
	public int pleasure(int paramInt, Anatomy area) {
		return paramInt/4;
	}

	@Override
	public int tempted(int paramInt) {
		return paramInt/4;
	}

	@Override
	public int evade() {
		return -20;
	}

	@Override
	public int escape() {
		return -20;
	}

	@Override
	public int gainmojo(int paramInt) {
		return -paramInt;
	}

	@Override
	public Status copy(Character target) {
		return new Enthralled(target,master);
	}
	@Override
	public void turn(Combat c) {
		affected.spendMojo(5);
		if (duration <= 0|| affected.check(Attribute.Cunning, 10+5*duration)) {
			affected.removeStatus(this,c);
			if (affected.human() && affected.state != State.combat)
				Global.gui().message("Everything around you suddenly seems much clearer,"
						+ " like a lens snapped into focus. You don't really remember why"
						+ " you were heading in the direction you where...");
		}
		affected.emote(Emotion.horny,15);
		decay(c);
	}

}
