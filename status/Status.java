package status;

import java.util.ArrayList;
import java.util.HashSet;

import combat.Combat;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;

public abstract class Status {
	protected String name;
	protected String tooltip;
	protected Character affected;
	protected HashSet<Stsflag> flags;
	protected ArrayList<Trait> traits;
	protected int magnitude;
	protected int duration;
	protected boolean stacking;
	protected boolean lingering;
	protected boolean decaying;

	public Status(){

    }

	public Status(String name, Character affected){
		this.name=name;
		flags = new HashSet<Stsflag>();
		traits = new ArrayList<Trait>();
		this.magnitude = 0;
		this.duration = 0;
		stacking = false;
		lingering = false;
		decaying = true;
		this.affected=affected;
	}
	public String toString(){
		return name;
	}
	public abstract String describe();
	public String getTooltip(){
		String tip = tooltip;
		if(magnitude != 0){
			tip += " ("+mag()+")";
		}
		if(duration > 0){
			tip += "; "+duration+" turns";
		}
		return tip;
	}
	public int mod(Attribute a){
		return 0;
	}
	public int regen(){
		return 0;
	}
	public int damage(int x, Anatomy area){
		return 0;
	}
	public int pleasure(int x, Anatomy area){
		return 0;
	}
	public int weakened(int x){
		return 0;
	}
	public int tempted(int x){
		return 0;
	}
	public int evade(){
		return 0;
	}
	public int escape(){
		return 0;
	}
	public int gainmojo(int x){
		return 0;
	}
	public int spendmojo(int x){
		return 0;
	}
	public int counter(){
		return 0;
	}
	public int value(){
		return 0;
	}
	public boolean tempTraits(Trait bestows){
		return traits.contains(bestows);
	}
	public void turn(Combat c){
	    if(decaying){
	        decay(c);
        }
    }
	public abstract Status copy(Character target);
	public boolean lingering(){
		return lingering;
	}
	public void flag(Stsflag status){
		flags.add(status);
	}
	public HashSet<Stsflag> flags(){
		return this.flags;
	}
	public boolean mindgames(){
		return this.flags().contains(Stsflag.charmed)||this.flags().contains(Stsflag.distracted)||this.flags().contains(Stsflag.enthralled);
	}
	public boolean stacking(){
		return stacking;
	}
	public int mag(){
		return magnitude;
	}
	public int duration(){
		return duration;
	}
	public float proficiency(Anatomy using){
		return 1.0f;
	}
	public float sensitive(Anatomy targeted){
		return 1.0f;
	}
	public float sore(Anatomy targeted){
		return 1.0f;
	}
	public void stack(Status other){
		this.magnitude+=other.mag();
		if(other.duration>this.duration){
			this.duration=other.duration();
		}
		if(this.magnitude == 0){
			affected.removeStatus(this);
		}
	}
	public boolean equals(Status other){
		return this.name == other.name;
	}
	public void decay(Combat c){
        duration--;
        if (duration <= 0) {
            affected.removeStatus(this,c);
            if (mindgames()) {
                affected.addlist.add(new Cynical(affected));
            }
        }
	}
    public void decay(){
        duration--;
        if (duration <= 0) {
            affected.removeStatus(this);
            if (mindgames()) {
                affected.addlist.add(new Cynical(affected));
            }
        }
    }
}
