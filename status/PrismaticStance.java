package status;

import characters.*;
import characters.Character;
import combat.Combat;

public class PrismaticStance extends Status {

	public PrismaticStance(Character affected) {
		super("Prismatic Form",affected);
		duration = 10;
		flag(Stsflag.form);
		tooltip = "Pain and Pleasure resistance, x2 Mojo gain, Half Mojo costs, Evasion and Counter bonus";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 15;
		}
		this.affected = affected;
		
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "combining all the elemental forms, your Ki pushes your body past its limit";
		}
		else{
			return affected.name()+"'s powerful Ki seems to be limitless.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(Attribute.Power==a){
			return -affected.get(Attribute.Ki)/2;
		}
		if(a==Attribute.Speed){
			return -affected.get(Attribute.Ki);
		}
		return 0;
	}

	@Override
	public int regen() {
		return -Math.min(affected.get(Attribute.Ki),30)/5;
	}

	@Override
	public int damage(int x, Anatomy area) {
		return -x*(Math.min((affected.get(Attribute.Ki)*3),90)/100);
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return -x*(Math.min((affected.get(Attribute.Ki)*2),80)/100);
	}

	@Override
	public int evade() {
		return 2*affected.get(Attribute.Ki);
	}

	@Override
	public int gainmojo(int x) {
		return x*Math.min(affected.get(Attribute.Ki),30)/6;
	}

	@Override
	public int spendmojo(int x) {
		return -x/2;
	}

	@Override
	public int counter() {
		return affected.get(Attribute.Ki);
	}

	@Override
	public Status copy(Character target) {
		return new PrismaticStance(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.dominant,20);
		decay(c);
	}

}
