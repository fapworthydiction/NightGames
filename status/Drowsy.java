package status;

import characters.Anatomy;
import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Drowsy extends Status {

	public Drowsy(Character affected, int magnitude) {
		super("Drowsy",affected);
		this.magnitude = magnitude;
		lingering = true;
		stacking = true;
		this.duration = 4;
		flag(Stsflag.drowsy);
		tooltip = "Stamina loss each turn, take reduced pleasure damage";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			this.duration=6;
		}
		this.affected = affected;
		
	}
	
	@Override
	public String describe() {
		if(affected.human()){
			return "You feel lethargic and sluggish. You're struggling to remain standing";
		}
		else{
			return affected.name()+" looks extremely sleepy.";
		}
	}

	@Override
	public int regen() {
		return -magnitude;
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return -(x*magnitude)/12;
	}

	@Override
	public int weakened(int x) {
		return (x*magnitude)/12;
	}

	@Override
	public Status copy(Character target) {
		return new Drowsy(target,magnitude);
	}
	@Override
	public void turn(Combat c) {
		affected.buildMojo(-5*magnitude);
		decay(c);
	}

}
