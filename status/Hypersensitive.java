package status;

import characters.*;
import characters.Character;
import combat.Combat;

public class Hypersensitive extends Status {

	public Hypersensitive(Character affected) {
		super("Hypersensitive",affected);
		lingering = true;
		duration = 20;
		flag(Stsflag.hypersensitive);
		tooltip = "+4 Perception, +30% pleasure";
		if(affected!=null && affected.has(Trait.PersonalInertia)){
			duration = 30;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your skin tingles and feels extremely sensitive to touch.";
		}
		else{
			return "She shivers from the breeze hitting her skin and has goosebumps";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a == Attribute.Perception){
			return 4;
		}
		return 0;
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return x/3;
	}

	@Override
	public Status copy(Character target) {
		return new Hypersensitive(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous,5);
		decay(c);
	}
}
