package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;

public class Bound extends Status {
	private int toughness;
	private String binding;

	public Bound(Character affected, int dc, String binding) {
		super("Bound",affected);
		toughness = dc;
		this.binding = binding;
		flag(Stsflag.bound);
		tooltip = "Unable to move. Escape difficulty: "+dc;
		this.affected = affected;
		decaying = false;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your hands are bound by "+binding+".";
		}
		else{
			return "Her hands are restrained by "+binding+".";
		}
	}

	@Override
	public int evade() {
		return -15;
	}

	@Override
	public int escape() {
		int dc = toughness;
			toughness-=5;
		return -dc;
	}

	@Override
	public int counter() {
		return -10;
	}
	public String toString(){
		return binding;
	}

	@Override
	public Status copy(Character target) {
		return new Bound(target,toughness,binding);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.desperate, 10);
		affected.emote(Emotion.nervous, 10);
	}
}
