package status;

import characters.Character;
import combat.Combat;

public class Beaded extends Status{

    public Beaded(Character affected, int beads) {
        super("Beads Inside", affected);
        flag(Stsflag.beads);
        magnitude = beads;
        stacking = true;
        duration = 999;
        decaying = false;
    }

    @Override
    public String describe() {
        if(affected.human()){
            if(magnitude==1) {
                return "There is a bead in your ass";
            }else{
                return "There are " +magnitude+" beads in your ass";
            }
        }else{
            if(magnitude==1) {
                return "There is a single anal bead inside "+affected.name()+".";
            }else{
                return "There are " +magnitude+" anal beads inside "+affected.name()+".";
            }
        }
    }

    @Override
    public Status copy(Character target) {
        return new Beaded(affected,magnitude);
    }

}
